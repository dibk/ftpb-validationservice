# Dibk.Ftpb.ValidationService

# Lokalt oppsett

For å kjøre ValidationService lokalt må det være installert et sertifikat for dekryptering av data. Utviklerne i teamet har dette tilgjengelig. 

Bruk følgende innstillinger ved installering av sertifikatet:

* Merk denne nøkkelen som eksporterbar
* Lagringssted: Gjeldende bruker
* Velg sertifkatlager automatisk, basert på sertifikattypen

Hent ut certifikat-thumbprint: 

Windows: 
Start "Administrer brukersertifikater" (certmgr)

Gå til Personlig -> Sertifikater
Finn sertifikat utstedt av Arkitektum
Vis detaljert - hent ut verdien i feltet 'Avtrykk' 

Legg thumbrint inn i __appsettings.Development.json__ i Dibk.Ftpb.Validation.Web-prosjektet
```
{
  "EncryptionSettings:CertificateThumbprint": "......"
}
```