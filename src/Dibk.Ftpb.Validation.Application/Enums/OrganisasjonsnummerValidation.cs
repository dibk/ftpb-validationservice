﻿namespace Dibk.Ftpb.Validation.Application.Enums
{
    public enum OrganisasjonsnummerValidation
    {
        Ok,
        Empty,
        Invalid,
        InvalidDigitsControl,
    }
}
