﻿using System;

namespace Dibk.Ftpb.Validation.Application.Enums
{
    [AttributeUsage(AttributeTargets.Field)]
    public class EntityValidatorEnumerationAttribute : Attribute
    {
        public string XmlNode { get; set; }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class SjekklistekravEnumerationAttribute : Attribute
    {
        public string SjekklistepunktVerdi { get; set; }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class FieldNameEnumerationAttribute : Attribute
    {
        public string XmlNode { get; set; }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class CodelistEnumerationAttribute : Attribute
    {
        public string CodelistUrl { get; set; }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class AttachmentEnumerationAttribute : Attribute
    {
        public string XmlNode { get; set; }

    }
}
