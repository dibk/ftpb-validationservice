﻿namespace Dibk.Ftpb.Validation.Application.Enums
{
    public enum FoedselnumerValidation
    {
        Ok,
        Empty,
        Invalid,
        InvalidDigitsControl,
        InvalidEncryption
    }
}
