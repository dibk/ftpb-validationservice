namespace Dibk.Ftpb.Validation.Application.Enums
{
    public enum FtbKodeListeEnum
    {
        Partstype,
        Anleggstype,
        Naeringsgruppe,
        tiltaksformal,
        Bygningstype,
        tiltaktype,
        [CodelistEnumeration(CodelistUrl = "tiltakstyperutenansvarsrett")] tiltakstyperUtenAnsvarsrett,
        funksjon,
        tiltaksklasse,
        [CodelistEnumeration(CodelistUrl = "sluttrapport-med-avfallsplan/avfallklasse")] Avfallklasse,
        [CodelistEnumeration(CodelistUrl = "sluttrapport-med-avfallsplan/disponeringsmate")] Avfalldisponeringsmåte,
        [CodelistEnumeration(CodelistUrl = "sluttrapport-med-avfallsplan/fraksjoner/fraksjoner-eal")] Avfallfraksjoner_EAL,
        [CodelistEnumeration(CodelistUrl = "sluttrapport-med-avfallsplan/fraksjoner/fraksjoner-ns")] Avfallfraksjoner_NS,
        [CodelistEnumeration(CodelistUrl = "vedlegg")] ArbeidstilsynetVedlegg,
        [CodelistEnumeration(CodelistUrl = "tema")] Tema,
        [CodelistEnumeration(CodelistUrl = "vedlegg-og-underskjema/vedlegg")] Vedlegg,
        [CodelistEnumeration(CodelistUrl = "mangel/mangeltekst-bokmal")] Mangeltekst,
        [CodelistEnumeration(CodelistUrl = "tiltakstyper-for-arbeidstilsynet")] tiltakstyper_for_arbeidstilsynet,
        [CodelistEnumeration(CodelistUrl = "utfall")] utfall,
        foretrukketspraak,
        [CodelistEnumeration(CodelistUrl = "ansvar-for-byggesaken")] AnsvarForByggesaken,
        [CodelistEnumeration(CodelistUrl = "status-for-ansvarsomradet")] AnsvarsomraadeStatus,
        [CodelistEnumeration(CodelistUrl = "energiforsyningtype")] Energiforsyningtype,
        [CodelistEnumeration(CodelistUrl = "varmefordeling")] Varmefordeling,
    }
}