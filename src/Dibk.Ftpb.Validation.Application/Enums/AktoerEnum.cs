﻿namespace Dibk.Ftpb.Validation.Application.Enums
{
    public enum AktoerEnum
    {
        tiltakshaver = 1,
        ansvarligSoeker = 2,
        forslagsstiller = 3,
        plankonsulent = 4
    }
}
