﻿namespace Dibk.Ftpb.Validation.Application.Enums
{
    public enum MunicipalityValidationEnum
    {
        Ok,
        Empty,
        Invalid,
        Expired,
        TooSoon,
        unverified
    }
}
