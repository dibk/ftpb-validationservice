﻿namespace Dibk.Ftpb.Validation.Application.Enums
{
    public enum ArbeidstilsynetKodeListeEnum
    {
        [CodelistEnumerationAttribute(CodelistUrl = "arbeidstilsynets-krav")]  Arbeidstilsynets_krav,
    }
}
