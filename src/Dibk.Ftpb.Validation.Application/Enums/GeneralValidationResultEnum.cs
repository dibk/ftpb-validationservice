﻿namespace Dibk.Ftpb.Validation.Application.Enums
{
    public enum GeneralValidationResultEnum
    {
        Empty,
        Invalid,
        Error,
        Warning,
        Ok,
    }
}
