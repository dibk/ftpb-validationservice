﻿namespace Dibk.Ftpb.Validation.Application.Enums
{
    public enum RegistryType
    {
        Byggesoknad,
        Arbeidstilsynet,
        SosiKodelister,
        NabovarselPlan,
        EByggesak,
        plansak
    }
}
