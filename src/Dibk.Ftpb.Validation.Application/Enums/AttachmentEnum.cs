﻿namespace Dibk.Ftpb.Validation.Application.Enums
{
    public enum AttachmentEnum
    {
        TiltakshaverSignatur = 1,
        Arbeidsmiljoutvalg = 2,
        ArbeidstakersRepresentant = 3,
        Verneombud = 4,
        LeietakerArbeidsgiver = 5,
        Bedriftshelsetjeneste = 6,
        TegningNyttSnitt = 7,
        TegningNyPlan = 8,
        TegningNyFasade = 9,
        SluttrapportForAvfallsplan = 10,
        KvitteringForLevertAvfall = 11,
        AndreRedegjoerelser = 12,
        Soknad = 13,
        SoknadUnntasOffentlighet = 14,
        Annet = 15,
        SamtykkeArbeidstilsynet= 16,
        ErklaeringSelvbygger = 17,
        TegningEksisterendePlan = 18,

        //[AttachmentEnumerationAttribute(XmlNode = "/sluttrapport/sortering")] AndreRedegjoerelserSluttrapport = 13,

        //Plan Attachement
        PlanomraadeSosi = 49,
        Planomraade = 50,
        KartDetaljert = 51,
        Planinitiativ = 52,
        Planprogram = 53,
        PlanomraadePdf= 54,
        ReferatOppstartsmoete = 55,

        Gjennomføringsplan = 56,
        SituasjonsplanOgTegning = 57,
    }
}
