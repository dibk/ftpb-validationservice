﻿namespace Dibk.Ftpb.Validation.Application.Enums
{
    public enum SjekklisteAnswerOutcomeEnum
    {
        Dokumentasjon,
        Underpunkt,
        Ingen
    }
}
