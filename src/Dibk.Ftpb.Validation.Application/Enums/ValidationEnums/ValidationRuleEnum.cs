﻿namespace Dibk.Ftpb.Validation.Application.Enums.ValidationEnums
{
    public enum ValidationRuleEnum
    {
        //Generelle test av nye aug-21
        XsdValidationErrors = 0,
        utfylt = 1,
        gyldig = 2, // @Helge forklar hva som er forskjellen mellom gyldig og tillatt  
        validert = 3,
        utgått = 4,
        status = 5,
        tillatt = 6,
        registrert = 7, // F.eks. hvis bygningsnummer finnes i matrikkel register       

        // @Helge forklar hva som er forskjellen mellom gyldig og tillatt
        // // F.eks. et felt for beløp kan valideres med 3 ulike hensyn:
        // 1) utfylt => feltet må være utfylt
        // 2) gyldig => feltverdien må være > 0
        // 3) tillatt => gitt andre data i XML'en, må beløpet være f.eks. 2450

        kontrollsiffer = 17,
        dekryptering = 18,
        kryptert = 19,

        framtidige_eller_eksisterende_utfylt = 21,
        faste_eller_midlertidige_utfylt = 22,
        ehf_eller_papir = 23,
        numerisk = 24,

        // potential for improvement
        sjekklistepunkt_1_18_dokumentasjon_utfylt = 11, // utfylt

        //*midlertidig, finn et smartere navn eller en løsning
        finnes = 30, //kodeverdi fra Plan/avfallklase finnes ikke i *delsum/avfallklase 

        // skjemaspesifikke - avfallsplan (mellom 40 og 49)
        BlandetAvfall = 40, // minst en
        FarligAvfallOgEllerOrdinaertAvfall = 41, // en eller begge må være med
        Stemmer = 42, // kodeverdi fra avfallklase ikke stemmer med avfallsfraksjon kodeverdi

        //Date
        senere = 50,
        tidligere = 51,

        //Vedlegg
        vedlegg = 12,
        størrelse = 13,
        filtyper = 14,
        underskjema = 15,
    }



}
