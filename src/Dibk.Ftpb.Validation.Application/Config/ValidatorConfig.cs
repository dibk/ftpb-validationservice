﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Dibk.Ftpb.Validation.Application.Config;

public static class ValidatorConfig
{
    public static void AddValidators(this IServiceCollection services, Action<ValidatorOptions> options)
    {
        services.Configure(options);
    }
}
