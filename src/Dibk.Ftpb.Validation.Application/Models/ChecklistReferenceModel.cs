﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.Models
{
    public class ChecklistReferenceModel
    {
        public string ProcessCategory { get; set; }
        public string ChecklistReference { get; set; }
        public bool ActionValue { get; set; }
        public List<string> EnterpriseTerms { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public List<ChecklistReferenceModel> SubChecklistReferenceModels { get; set; }
    }
}
