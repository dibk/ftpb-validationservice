﻿namespace Dibk.Ftpb.Validation.Application.Models
{
    public class InputDataConfig
    {
        public string DataFormatId { get; private set; }
        public string DataFormatVersion { get; private set; }

        public InputDataConfig(string dataFormatId, string dataFormatVersion)
        {
            DataFormatId = dataFormatId;
            DataFormatVersion = dataFormatVersion;
        }
    }
}