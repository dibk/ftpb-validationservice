﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Dibk.Ftpb.Validation.Application.Models.Standard
{
    public class RuleModelDocumentation
    {
        [JsonPropertyName("Regelnr")]
        public string RuleId { get; set; }

        [JsonPropertyName("sjkPkt")]
        public string CheckListPt { get; set; }

        [JsonPropertyName("Beskrivelse")]
        public string Description { get; set; }

        [JsonPropertyName("Valideringsresultat")]
        public string RuleType { get; set; }

        [JsonPropertyName("Betingelse")]
        public string Xpath { get; set; }

        [JsonPropertyName("Forutsetning")]
        public string XpathPrecondition { get; set; }

        [System.Text.Json.Serialization.JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        [JsonPropertyName("SkjemaListe")]
        public List<string> FormMetadataList { get; set; }
    }
}