﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.Models.Web
{
    public class RuleComparison
    {
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
        public string Branch { get; set; }
        public string CompareBranch { get; set; }
    }
}
