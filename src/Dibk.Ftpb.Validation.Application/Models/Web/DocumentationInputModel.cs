﻿namespace Dibk.Ftpb.Validation.Application.Models.Web
{
    public class DocumentationInputModel
    {
        public string Branch { get; set; }
        public string[] FormsMetadataList { get; set; }
        public string XmlNode { get; set; }
        public bool? IncludeChecklist { get; set; }
        public bool isXmlNodeStartElement { get; set; }
    }
}