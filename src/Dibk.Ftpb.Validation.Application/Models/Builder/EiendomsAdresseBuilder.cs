﻿using Dibk.Ftpb.Common.Datamodels.Parts;

namespace Dibk.Ftpb.Validation.Application.Models.Builder
{
    public class EiendomsAdresseBuilder
    {
        private readonly Adresse _eiendomsAdresse;

        public EiendomsAdresseBuilder()
        {
            _eiendomsAdresse = new Adresse();
        }

        public Adresse Build()
        {
            return _eiendomsAdresse;
        }

        public EiendomsAdresseBuilder EiendomsAdresse(string adresselinje1, string husnr, string gatenavn)
        {
            _eiendomsAdresse.Adresselinje1 = adresselinje1;
            _eiendomsAdresse.Husnr = husnr;
            _eiendomsAdresse.Gatenavn = gatenavn;
            return this;
        }
    }
}