﻿using Dibk.Ftpb.Common.Datamodels.Parts;

namespace Dibk.Ftpb.Validation.Application.Models.Builder
{
    public class EiendomsidentifikasjonBuilder
    {
        private readonly Eiendomsidentifikasjon _Eiendomsidentifikasjon;

        public EiendomsidentifikasjonBuilder()
        {
            _Eiendomsidentifikasjon = new Eiendomsidentifikasjon();
        }

        public Eiendomsidentifikasjon Build()
        {
            return _Eiendomsidentifikasjon;
        }

        public EiendomsidentifikasjonBuilder Eiendomsidentifikasjon(string kommunenummer, string gaardsnummer, string bruksnummer, string festenummer = null, string seksjonsnummer = null)
        {
            _Eiendomsidentifikasjon.Kommunenummer = kommunenummer;
            _Eiendomsidentifikasjon.Gaardsnummer = gaardsnummer;
            _Eiendomsidentifikasjon.Bruksnummer = bruksnummer;
            _Eiendomsidentifikasjon.Festenummer = festenummer;
            _Eiendomsidentifikasjon.Seksjonsnummer = seksjonsnummer;

            return this;
        }
    }
}