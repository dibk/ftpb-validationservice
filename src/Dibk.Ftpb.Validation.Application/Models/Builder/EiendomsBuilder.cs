﻿using Dibk.Ftpb.Common.Datamodels.Parts;

namespace Dibk.Ftpb.Validation.Application.Models.Builder
{
    public class EiendomsBuilder
    {
        private readonly Eiendom _eiendom;

        public EiendomsBuilder()
        {
            _eiendom = new Eiendom();
        }

        public Eiendom Build()
        {
            return _eiendom;
        }

        public EiendomsBuilder Building(string bygningsnummer, string bolignummer, string kommunenavn)
        {
            _eiendom.Bygningsnummer = bygningsnummer;
            _eiendom.Bolignummer = bolignummer;
            _eiendom.Kommunenavn = kommunenavn;

            return this;
        }

        public EiendomsBuilder BuildingAdresse(string adresselinje1, string husnr, string gatenavn)
        {
            _eiendom.Adresse = new Adresse()
            {
                Adresselinje1 = adresselinje1,
                Husnr = husnr,
                Gatenavn = gatenavn,
            };
            return this;
        }

        public EiendomsBuilder BuildingAdresse(Adresse eiendomsAdresse)
        {
            _eiendom.Adresse = eiendomsAdresse;
            return this;
        }

        public EiendomsBuilder Eiendomsidentifikasjon(string kommunenummer, string gaardsnummer, string bruksnummer, string festenummer = null, string seksjonsnummer = null)
        {
            _eiendom.Eiendomsidentifikasjon = new Eiendomsidentifikasjon()
            {
                Kommunenummer = kommunenummer,
                Gaardsnummer = gaardsnummer,
                Bruksnummer = bruksnummer,
                Festenummer = festenummer,
                Seksjonsnummer = seksjonsnummer
            };
            return this;
        }

        public EiendomsBuilder Eiendomsidentifikasjon(Eiendomsidentifikasjon eiendomsidentifikasjon)
        {
            _eiendom.Eiendomsidentifikasjon = eiendomsidentifikasjon;
            return this;
        }
    }
}