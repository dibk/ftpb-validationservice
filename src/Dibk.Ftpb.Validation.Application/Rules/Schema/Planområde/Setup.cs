﻿using DiBK.RuleValidator.Config;

namespace Dibk.Ftpb.Validation.Application.Rules.Schema.Planområde;

public class Setup : IRuleSetup
{
    public RuleConfig CreateConfig()
    {
        return RuleConfig
            .Create<SkjemavalideringForGmlPlanområde>("Applikasjonsskjema")
            .AddGroup("Applikasjonsskjema", "Applikasjonsskjema", group => group
                .AddRule<SkjemavalideringForGmlPlanområde>()
            )
            .Build();
    }
}
