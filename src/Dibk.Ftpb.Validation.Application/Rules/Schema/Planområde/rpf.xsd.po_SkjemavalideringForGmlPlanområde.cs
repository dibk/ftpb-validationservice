﻿using Dibk.Ftpb.Validation.Application.Models.Rules;

namespace Dibk.Ftpb.Validation.Application.Rules.Schema.Planområde;

public class SkjemavalideringForGmlPlanområde : SchemaRule
{
    public override void Create()
    {
        Id = "gml.xsd.po";
        Name = "Skjemavalidering for GML-plangrense";
        Description = "Datasettet må være i henhold til oppgitt applikasjonsskjema";
    }
}
