﻿using DiBK.RuleValidator.Config;

namespace Dibk.Ftpb.Validation.Application.Rules.Planområde
{
    public class Setup : IRuleSetup
    {
        public RuleConfig CreateConfig()
        {
            return RuleConfig
                .Create<IPlanområdeValidationInput>("Planområde")
                .AddGroup("Planområde", "Planområde", group => group
                    .AddRule<PlanområdeMåInneholdeArealplanOgPlanområde>()
                )
                .Build();
        }
    }
}
