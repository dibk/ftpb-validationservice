﻿using DiBK.RuleValidator.Extensions.Gml;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Rules.Planområde
{
    public interface IPlanområdeValidationInput : IDisposable
    {
        List<GmlDocument> Documents { get; }
    }
}
