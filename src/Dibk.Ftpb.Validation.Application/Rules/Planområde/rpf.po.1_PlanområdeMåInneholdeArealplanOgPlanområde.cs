﻿using DiBK.RuleValidator;
using DiBK.RuleValidator.Extensions;
using DiBK.RuleValidator.Extensions.Gml;
using System;
using System.Linq;
using Wmhelp.XPath2;

namespace Dibk.Ftpb.Validation.Application.Rules.Planområde
{
    public class PlanområdeMåInneholdeArealplanOgPlanområde : Rule<IPlanområdeValidationInput>
    {
        public override void Create()
        {
            Id = "rpf.po.1";
            Name = "Planområde-GML må inneholde én arealplan og ett planområde";
        }

        protected override void Validate(IPlanområdeValidationInput input)
        {
            if (!input.Documents.Any())
                SkipRule();

            input.Documents.ForEach(Validate);
        }

        private void Validate(GmlDocument document)
        {
            var arealplanCount = document.Document
                .XPath2SelectElements("//*:Arealplan")
                .Count();

            var planområdeCount = document.Document
                .XPath2SelectElements("//*:Planområde")
                .Count();

            var root = document.Document.Root;
            var xPath = root.GetXPath();
            var (lineNumber, linePosition) = root.GetLineInfo();

            if (arealplanCount != 1)
            {
                this.AddMessage(
                    "Planområdet må inneholde ett og bare ett Arealplan-element",
                    document.FileName,
                    new[] { xPath },
                    Array.Empty<string>(),
                    lineNumber,
                    linePosition
                );
            }

            if (planområdeCount != 1)
            {
                this.AddMessage(
                    "Planområdet må inneholde ett og ett Planområde-element",
                    document.FileName,
                    new[] { xPath },
                    Array.Empty<string>(),
                    lineNumber,
                    linePosition
                );
            }
        }
    }
}
