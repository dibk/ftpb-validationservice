﻿using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Reporter.DataBase
{
    public interface IValidationMessageDb
    {
        List<ValidationMessageStorageEntry> InitiateMessageRepository();
    }
}