using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Utils;
using Microsoft.FeatureManagement;
using System;
using System.Collections.Generic;
using static Dibk.Ftpb.Validation.Application.Reporter.DataBase.DbHelpers;


namespace Dibk.Ftpb.Validation.Application.Reporter.DataBase
{
    public class ValidationMessageDb : IValidationMessageDb
    {
        private List<ValidationMessageStorageEntry> _validationMessageStorageEntry;

        private readonly bool atil20250217Enabled = false;
        public ValidationMessageDb(IFeatureManager featureManager)
        {
            _validationMessageStorageEntry = new List<ValidationMessageStorageEntry>();

            atil20250217Enabled = AsyncHelper.RunSync(async () => await featureManager.IsEnabledAsync("ATIL20250217"));

        }

        private void AddRuleToValidationMessageStorageEntry(string dataFormatId, string dataFormatVersion, ValidationRuleEnum rule, string xPath, string message, ValidationResultSeverityEnum validationResultSeverity = ValidationResultSeverityEnum.WARNING, string preCondition = null)
        {
            _validationMessageStorageEntry.Add(GetValidationMessageStorageEntry(dataFormatId, dataFormatVersion, rule, xPath, message, validationResultSeverity, preCondition));
        }

        public List<ValidationMessageStorageEntry> InitiateMessageRepository()
        {
            var storage = new List<ValidationMessageStorageEntry>();
            storage.AddRange(InitiateMessageStorageEntries());

            return storage;
        }
        /// <summary>
        /// Eiendom byggested for FTB
        /// </summary>
        /// <returns></returns>
        private List<ValidationMessageStorageEntry> Eiendombyggested()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/eiendomByggested/eiendom{0}", "Du må oppgi hvilken eiendom/hvilke eiendommer byggesøknaden gjelder.", ValidationResultSeverityEnum.ERROR),
                //Adresse
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/eiendomByggested/eiendom{0}/adresse", "Postadresse for eiendom/byggested bør fylles ut.", ValidationResultSeverityEnum.WARNING, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.8" }, { ServiceDomain.ATIL, "1.9" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/eiendomByggested/eiendom{0}/adresse/adresselinje1", "Adresselinje 1 for eiendom/byggested bør fylles ut.", ValidationResultSeverityEnum.WARNING, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.8" }, { ServiceDomain.ATIL, "1.9" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/eiendomByggested/eiendom{0}/adresse/gatenavn", "Du bør oppgi gatenavn for eiendom/byggested slik at adressen kan valideres mot matrikkelen. Du kan sjekke riktig adresse på https://seeiendom.no ", ValidationResultSeverityEnum.WARNING, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.8" }, { ServiceDomain.ATIL, "1.9" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/eiendomByggested/eiendom{0}/adresse/husnr", "Du bør oppgi husnummer og eventuell bokstav for eiendom/byggested slik at adressen kan valideres mot matrikkelen. Du kan sjekke riktig adresse på https://seeiendom.no", ValidationResultSeverityEnum.WARNING, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.8" }, { ServiceDomain.ATIL, "1.9" } }),
                // Adresse validering mot Matrikkel
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.registrert, "/eiendomByggested/eiendom{0}/adresse", "Du har oppgitt feil gateadresse for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", ValidationResultSeverityEnum.WARNING, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.8" }, { ServiceDomain.ATIL, "1.9" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/eiendomByggested/eiendom{0}/adresse", "Gateadressen for eiendom/byggested ble ikke validert mot matrikkelen. Gateadressen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det. Du kan sjekke adressen på https://seeiendom.no", ValidationResultSeverityEnum.WARNING, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.8" }, { ServiceDomain.ATIL, "1.9" } }),
                //eiendomsidentifikasjon
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon", "Du må oppgi eiendomsidentifikasjon for eiendom/byggested.", ValidationResultSeverityEnum.ERROR, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.72" }, { ServiceDomain.ATIL, "1.2" } }),
                // validering mot Matrikkel
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.registrert, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon", "Når eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] er oppgitt for eiendom/byggested, bør den være gyldig i matrikkelen. Du kan sjekke riktig informasjon på https://seeiendom.no", ValidationResultSeverityEnum.WARNING, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.72" }, { ServiceDomain.ATIL, "1.2" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon", "Eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] for eiendom/byggested ble ikke validert mot matrikkelen. Identifikasjonen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.72" }, { ServiceDomain.ATIL, "1.2" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/kommunenummer", "Kommunenummer må fylles ut for eiendom/byggested.", ValidationResultSeverityEnum.ERROR, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.2" }, { ServiceDomain.ATIL, "1.3" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/kommunenummer", "Kommunenummeret '{0}' for eiendom/byggested finnes ikke i kodelisten. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", ValidationResultSeverityEnum.ERROR, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.2" }, { ServiceDomain.ATIL, "1.3" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/kommunenummer", "En teknisk feil gjør at vi ikke kan bekrefte om kommunenummeret du har oppgitt '{0}' er riktig. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", ValidationResultSeverityEnum.WARNING, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.2" }, { ServiceDomain.ATIL, "1.3" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.status, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/kommunenummer", "Kommunenummeret '{0}' for eiendom/byggested har ugyldig status ({1}). Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer ", ValidationResultSeverityEnum.ERROR, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.2" }, { ServiceDomain.ATIL, "1.3" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/gaardsnummer", "Gårdsnummer må fylles ut for eiendom/byggested.", ValidationResultSeverityEnum.ERROR, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.3" }, { ServiceDomain.ATIL, "1.4" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/gaardsnummer", "Gårdsnummer '{0}' for eiendom/byggested må være '0' eller større.", ValidationResultSeverityEnum.ERROR, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.3" }, { ServiceDomain.ATIL, "1.4" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/bruksnummer", "Bruksnummer må fylles ut for eiendom/byggested.", ValidationResultSeverityEnum.ERROR, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.4" }, { ServiceDomain.ATIL, "1.5" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/bruksnummer", "Bruksnummer '{0}' for eiendom/byggested må være '0' eller større.", ValidationResultSeverityEnum.ERROR, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.4" }, { ServiceDomain.ATIL, "1.5" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/eiendomByggested/eiendom{0}/bygningsnummer", "Bygningsnummer for eiendom/byggested må være større enn '0'.", ValidationResultSeverityEnum.ERROR, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.6" }, { ServiceDomain.ATIL, "1.8" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/eiendomByggested/eiendom{0}/bygningsnummer", "Du har oppgitt følgende bygningsnummer for eiendom/byggested: '{0}'. Bygningsnummeret må være et tall.", ValidationResultSeverityEnum.ERROR, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.6" }, { ServiceDomain.ATIL, "1.8" } }),
                // Validering mot Matrikkel
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.registrert, "/eiendomByggested/eiendom{0}/bygningsnummer", "Når bygningsnummer [{0}] er oppgitt for eiendom/byggested, bør det være gyldig i matrikkelen på aktuelt matrikkelnummer. Du kan sjekke riktig bygningsnummer på https://seeiendom.no", ValidationResultSeverityEnum.WARNING, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.6" }, { ServiceDomain.ATIL, "1.8" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/eiendomByggested/eiendom{0}/bygningsnummer", "Bygningsnummer for eiendom/byggested ble ikke validert mot matrikkelen. Bygningsnummeret kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det. Du kan sjekke adressen på https://seeiendom.no", ValidationResultSeverityEnum.WARNING, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.6" }, { ServiceDomain.ATIL, "1.8" } }),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/eiendomByggested/eiendom{0}/bolignummer", "Når bruksenhetsnummer/bolignummer er fylt ut for eiendom/byggested, må det følge riktig format (for eksempel H0101). Se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/ ", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/eiendomByggested/eiendom{0}/kommunenavn", "Navnet på kommunen bør fylles ut for eiendom/byggested.", ValidationResultSeverityEnum.WARNING),
            };
            return validationMessageStorage;
        }


        private List<ValidationMessageStorageEntry> Tiltakshaver()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                //Tiltakshaver
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver", "Informasjon om tiltakshaver må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("10004", "4", ValidationRuleEnum.utfylt, "/tiltakshaver", "Du har valgt '{0}'. Da må du fylle ut informasjon om tiltakshaver.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("10005", "4", ValidationRuleEnum.utfylt, "/tiltakshaver", "Du har valgt '{0}'. Da må du fylle ut informasjon om tiltakshaver.", ValidationResultSeverityEnum.ERROR),

                //Tiltakshavers partstype
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/partstype", "Du må fylle ut partstypen for tiltakshaver.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/partstype/kodeverdi", "Du må oppgi partstypen for tiltakshaver. Du kan sjekke gyldige partstyper på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/tiltakshaver/partstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for partstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/tiltakshaver/partstype/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Partstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/partstype/kodebeskrivelse", "Når partstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR, "/tiltakshaver/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/tiltakshaver/partstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for partstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.WARNING, "/tiltakshaver/partstype/kodeverdi"),

                //telefonnummer ogmobilnummer
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/telefonnummer", "Telefonnummeret eller mobilnummeret til tiltakshaver må fylles ut.", ValidationResultSeverityEnum.ERROR, "/tiltakshaver/mobilnummer"),
                //Avfallsplan
                GetValidationMessageStorageEntry("7063", "47177", ValidationRuleEnum.utfylt, "/tiltakshaver/telefonnummer", "Telefonnummeret eller mobilnummeret til tiltakshaver bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/tiltakshaver/mobilnummer"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/tiltakshaver/telefonnummer", "Telefonnummeret til tiltakshaver må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/tiltakshaver/mobilnummer", "Mobilnummeret til tiltakshaver må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),

                //epost - navn
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/epost", "E-postadressen til tiltakshaver bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/tiltakshaver/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/navn", "Navnet til tiltakshaver må fylles ut.", ValidationResultSeverityEnum.ERROR, "/tiltakshaver/partstype/kodeverdi"),

                //foedselsnummer
                //arbeidstilsynetsSamtykke2
                GetValidationMessageStorageEntry("6821", "45957", ValidationRuleEnum.utfylt, "/tiltakshaver/foedselsnummer", "Fødselsnummer bør angis når tiltakshaver er privatperson.", ValidationResultSeverityEnum.WARNING, "/tiltakshaver/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/foedselsnummer", "Fødselsnummer må angis når tiltakshaver er privatperson.", ValidationResultSeverityEnum.ERROR, "/tiltakshaver/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.dekryptering, "/tiltakshaver/foedselsnummer", "Fødselsnummeret til tiltakshaver kan ikke dekrypteres.", ValidationResultSeverityEnum.ERROR, "/tiltakshaver/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/tiltakshaver/foedselsnummer", "Tiltakshavers fødselsnummer er ikke gyldig.", ValidationResultSeverityEnum.ERROR, "/tiltakshaver/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/tiltakshaver/foedselsnummer", "Fødselsnummeret til tiltakshaver har ikke gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR, "/tiltakshaver/partstype/kodeverdi"),

                //organisasjonsnummer
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/organisasjonsnummer", "Organisasjonsnummeret til tiltakshaver må fylles ut når tiltakshaver er en organisasjon.", ValidationResultSeverityEnum.ERROR, "/tiltakshaver/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/tiltakshaver/organisasjonsnummer", "Organisasjonsnummeret ('{0}') til tiltakshaver er ikke gyldig.", ValidationResultSeverityEnum.ERROR, "/tiltakshaver/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/tiltakshaver/organisasjonsnummer", "Organisasjonsnummeret ('{0}') til tiltakshaver har ikke gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR, "/tiltakshaver/partstype/kodeverdi"),

                //Tiltakshavers adresse
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/adresse", "Adressen til tiltakshaver bør fylles ut."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/adresse/adresselinje1", "Adresselinje 1 bør fylles ut for tiltakshaver."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/adresse/postnr", "Postnummeret til tiltakshaver bør angis."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/tiltakshaver/adresse/postnr", "Postnummeret '{0}' til tiltakshaver er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/tiltakshaver/adresse/landkode", "Landkoden for tiltakshavers adresse er ikke gyldig."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/tiltakshaver/adresse/poststed", "Postnummeret '{0}' til tiltakshaver stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/", ValidationResultSeverityEnum.WARNING, "/tiltakshaver/adresse/postnr"),

                //Tiltakshavers kontaktpersjon
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/kontaktperson", "Kontaktpersonen til tiltakshaver bør fylles ut."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/kontaktperson/navn", "Navnet til kontaktpersonen for tiltakshaver bør fylles ut."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/kontaktperson/telefonnummer", "Telefonnummeret eller mobilnummeret til tiltakshaver kontaktperson bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/tiltakshaver/kontaktperson/mobilnummer"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/tiltakshaver/kontaktperson/telefonnummer", "Telefonnummeret til tiltakshaver kontaktperson må kun inneholde tall og '+'."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/tiltakshaver/kontaktperson/mobilnummer", "Mobilnummeret til tiltakshaver kontaktperson må kun inneholde tall og '+'."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/tiltakshaver/kontaktperson/epost", "E-postadressen til tiltakshaver kontaktperson bør fylles ut.")
            };

            return validationMessageStorage;
        }
        private List<ValidationMessageStorageEntry> AnsvarligSoker()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                //Ansvarlig søker
                GetValidationMessageStorageEntry("10004", "4", ValidationRuleEnum.utfylt, "/ansvarligSoeker", "Du har valgt '{0}'. Da må du fylle ut informasjon om ansvarlig søker.", ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi = 'medAnsvar' || 'selvbygger'"),
                GetValidationMessageStorageEntry("10005", "4", ValidationRuleEnum.utfylt, "/ansvarligSoeker", "Du har valgt '{0}'. Da må du fylle ut informasjon om ansvarlig søker.", ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi = 'medAnsvar' || 'selvbygger'"),
                GetValidationMessageStorageEntry("10004", "4", ValidationRuleEnum.gyldig, "/ansvarligSoeker", "Du har valgt '{0}'. Da skal ikke søknaden ha en ansvarlig søker.", ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll'"),
                GetValidationMessageStorageEntry("10005", "4", ValidationRuleEnum.gyldig, "/ansvarligSoeker", "Du har valgt '{0}'. Da skal ikke søknaden ha en ansvarlig søker.", ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll'"),
                GetValidationMessageStorageEntry("10004", "4", ValidationRuleEnum.validert, "/ansvarligSoeker", "Ansvarlig søker for byggetillatelsen ble ikke validert. For å validere ansvarlig søker må du først fylle ut en gyldig verdi for 'ansvarForByggesaken'.", ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi ugyldig"),
                GetValidationMessageStorageEntry("10005", "4", ValidationRuleEnum.validert, "/ansvarligSoeker", "Ansvarlig søker for byggetillatelsen ble ikke validert. For å validere ansvarlig søker må du først fylle ut en gyldig verdi for 'ansvarForByggesaken'.", ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi ugyldig"),
                GetValidationMessageStorageEntry("6146", "44096", ValidationRuleEnum.utfylt, "/ansvarligSoeker", "Informasjon om søker må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker", "Du må fylle ut informasjon om ansvarlig søker.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("6146", "44096", ValidationRuleEnum.utfylt, "/ansvarligSoeker/navn", "Navnet til søker må fylles ut.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/navn", "Navnet til ansvarlig søker må fylles ut.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/epost", "E-postadressen til ansvarlig søker bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/telefonnummer", "Telefonnummeret eller mobilnummeret til ansvarlig søker bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/ansvarligSoeker/mobilnummer"),                
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoeker/mobilnummer", "Mobilnummeret til ansvarlig søker må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoeker/telefonnummer", "Telefonnummeret til ansvarlig søker må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),

                //Ansvarlig søkers partstype
                //TODO check if is use in other Forms
                GetValidationMessageStorageEntry("6146", "44096", ValidationRuleEnum.utfylt, "/ansvarligSoeker/partstype", "Du må oppgi partstypen for søker. Du kan sjekke gyldige partstyper på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/partstype", "Du må oppgi partstypen for ansvarlig søker. Du kan sjekke gyldige partstyper på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("6146", "44096", ValidationRuleEnum.utfylt, "/ansvarligSoeker/partstype/kodeverdi", "Kodeverdien for 'partstype' til søker må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/partstype/kodeverdi", "Kodeverdien for 'partstype' til ansvarlig søker må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("6146", "44096", ValidationRuleEnum.gyldig, "/ansvarligSoeker/partstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for partstypen til søker. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoeker/partstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for partstypen til ansvarlig søker. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tillatt, "/ansvarligSoeker/partstype/kodeverdi", "Partstypen for ansvarlig søker må være et foretak eller en organisasjon.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/ansvarligSoeker/partstype/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Partstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/partstype/kodebeskrivelse", "Når partstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoeker/partstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for partstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.WARNING, "/ansvarligSoeker/partstype/kodeverdi"),

                //Ansvarlig søkers adresse
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/adresse", "Adressen til ansvarlig søker bør fylles ut."),
                GetValidationMessageStorageEntry("6821", "45957", ValidationRuleEnum.utfylt, "/ansvarligSoeker/adresse", "Adressen til ansvarlig søker må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/adresse/adresselinje1", "Adresselinje 1 bør fylles ut for ansvarlig søker."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoeker/adresse/landkode", "Landkoden for ansvarlig søkers adresse er ikke gyldig."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/adresse/postnr", "Du bør fylle ut postnummeret til ansvarlig søker."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoeker/adresse/postnr", "Postnummeret '{0}' til ansvarlig søker er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoeker/adresse/poststed", "Postnummeret '{0}' til ansvarlig søker stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/adresse/postnr"),

                //Ansvarlig søkers kontaktpersjon
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/kontaktperson", "Kontaktpersonen til ansvarlig søker bør fylles ut.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/kontaktperson/navn", "Navnet til kontaktpersonen for ansvarlig søker bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/kontaktperson/telefonnummer", "Telefonnummeret eller mobilnummeret til ansvarlig søkers kontaktperson bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/ansvarligSoeker/kontaktperson/mobilnummer"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoeker/kontaktperson/telefonnummer", "Telefonnummeret til ansvarlig søkers kontaktperson må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoeker/kontaktperson/mobilnummer", "Mobilnummeret til ansvarlig søkers kontaktperson må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/kontaktperson/epost", "E-postadressen til ansvarlig søkers kontaktperson bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/ansvarligSoeker/partstype/kodeverdi"),

                //Ansvarlig organisasjonsnummer
                GetValidationMessageStorageEntry("6146", "44096", ValidationRuleEnum.utfylt, "/ansvarligSoeker/organisasjonsnummer", "Organisasjonsnummeret til søker må fylles ut.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/organisasjonsnummer", "Organisasjonsnummeret til ansvarlig søker må fylles ut.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry("6146", "44096", ValidationRuleEnum.kontrollsiffer, "/ansvarligSoeker/organisasjonsnummer", "Organisasjonsnummeret ('{0}') til søker har ikke gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/ansvarligSoeker/organisasjonsnummer", "Organisasjonsnummeret ('{0}') til ansvarlig søker har ikke gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry("6146", "44096", ValidationRuleEnum.gyldig, "/ansvarligSoeker/organisasjonsnummer", "Organisasjonsnummeret ('{0}') til søker er ikke gyldig.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoeker/organisasjonsnummer", "Organisasjonsnummeret ('{0}') til ansvarlig søker er ikke gyldig.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),

                //Ansvarlig Fødselnummer
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoeker/foedselsnummer", "Fødselsnummer må fylles ut når ansvarlig søker er en privatperson.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoeker/foedselsnummer", "Fødselsnummeret til ansvarlig søker er ikke gyldig.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/ansvarligSoeker/foedselsnummer", "Fødselsnummeret til ansvarlig søker har ikke gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.dekryptering, "/ansvarligSoeker/foedselsnummer", "Fødselsnummeret til ansvarlig søker kan ikke dekrypteres.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi")
            };

            if (!atil20250217Enabled) // ATIL Feature - SLETTES ETTER 20250217
                validationMessageStorage.Add(GetValidationMessageStorageEntry("6821", "45957", ValidationRuleEnum.utfylt, "/ansvarligSoeker/telefonnummer", "Telefonnummeret eller mobilnummeret til ansvarlig søker må fylles ut.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/mobilnummer"));


            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> AnsvarsrettForetak()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                //Foretak
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak", "Du må fylle ut informasjon om det ansvarlige foretaket.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tillatt, "/ansvarsrett/foretak/partstype/kodeverdi", "Kodeverdien '{0}' stemmer ikke med den gyldige kodeverdien for partstype. Riktig partstype er 'Foretak'.", ValidationResultSeverityEnum.ERROR),
                //foretak partstype
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/partstype", "Du må oppgi 'partstype' for foretak. Riktig partstype er 'Foretak'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/partstype/kodeverdi", "Kodeverdien for 'partstype' til foretaket må fylles ut. Riktig kodeverdi er 'Foretak'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/foretak/partstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for partstypen. Riktig kodeverdi er 'Foretak'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/ansvarsrett/foretak/partstype/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Partstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/partstype/kodebeskrivelse", "Når partstype er valgt, må kodebeskrivelse fylles ut. Riktig kodebeskrivelse er 'Foretak'.", ValidationResultSeverityEnum.ERROR, "/ansvarsrett/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/foretak/partstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for partstype. Riktig kodebeskrivelse er 'Foretak'.", ValidationResultSeverityEnum.WARNING, "/ansvarsrett/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/organisasjonsnummer", "Organisasjonsnummeret til foretaket må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/foretak/organisasjonsnummer", "Organisasjonsnummeret ('{0}') til foretaket er ikke gyldig.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/ansvarsrett/foretak/organisasjonsnummer", "Organisasjonsnummeret ('{0}') til foretaket har ikke gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/navn", "Navnet til foretaket må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/epost", "E-postadressen til foretaket bør fylles ut."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/telefonnummer", "Telefonnummeret eller mobilnummeret til foretaket bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/ansvarsrett/foretak/mobilnummer"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/foretak/telefonnummer", "Telefonnummeret til foretaket må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/foretak/mobilnummer", "Mobilnummeret til foretaket må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/adresse", "Adressen til foretaket bør fylles ut."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/adresse/adresselinje1", "Adresselinje 1 bør fylles ut for foretaket."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/foretak/adresse/landkode", "Ugyldig landkode til foretaket."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/adresse/postnr", "Postnummeret til foretaket bør fylles ut."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/foretak/adresse/postnr", "Postnummeret '{0}' til foretaket er ikke gyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/foretak/adresse/poststed", "Postnummeret '{0}' til foretaket stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/", ValidationResultSeverityEnum.WARNING, "/ansvarsrett/foretak/adresse/postnr"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/kontaktperson", "Kontaktpersonen til foretaket må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/kontaktperson/navn", "Navnet til kontaktpersonen for foretaket må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/kontaktperson/epost", "E-postadressen til foretakets kontaktperson bør fylles ut."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/foretak/kontaktperson/telefonnummer", "Telefonnummeret eller mobilnummeret til foretakets kontaktperson bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/ansvarsrett/foretak/kontaktperson/mobilnummer"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/foretak/kontaktperson/telefonnummer", "Telefonnummeret til foretakets kontaktperson må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/foretak/kontaktperson/mobilnummer", "Mobilnummer til foretakets kontaktperson må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR)
            };

            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> Foretak()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                //foretak
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak", "Du må fylle ut informasjon om det ansvarlige foretaket.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tillatt, "/foretak/partstype/kodeverdi", "Kodeverdien '{0}' stemmer ikke med den gyldige kodeverdien for partstype. Riktig partstype er 'Foretak'.", ValidationResultSeverityEnum.ERROR),
                //foretak partstype
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/partstype", "Du må oppgi 'partstype' for foretak. Riktig partstype er 'Foretak'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/partstype/kodeverdi", "Kodeverdien for 'partstype' til foretaket må fylles ut. Riktig kodeverdi er 'Foretak'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/foretak/partstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for partstypen. Riktig kodeverdi er 'Foretak'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/foretak/partstype/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Partstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/partstype/kodebeskrivelse", "Når partstype er valgt, må kodebeskrivelse fylles ut. Riktig kodebeskrivelse er 'Foretak'.", ValidationResultSeverityEnum.ERROR, "/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/foretak/partstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for partstype. Riktig kodebeskrivelse er 'Foretak'.", ValidationResultSeverityEnum.WARNING, "/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/organisasjonsnummer", "Organisasjonsnummeret til foretaket må fylles ut.", ValidationResultSeverityEnum.ERROR, "/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/foretak/organisasjonsnummer", "Organisasjonsnummeret ('{0}') til foretaket er ikke gyldig.", ValidationResultSeverityEnum.ERROR, "/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/foretak/organisasjonsnummer", "Organisasjonsnummeret ('{0}') til foretaket har ikke gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR, "/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/navn", "Navnet til foretaket må fylles ut.", ValidationResultSeverityEnum.ERROR, "/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/epost", "E-postadressen til foretaket bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/telefonnummer", "Telefonnummeret eller mobilnummeret til foretaket bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/foretak/mobilnummer"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/foretak/telefonnummer", "Telefonnummeret til foretaket må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/foretak/mobilnummer", "Mobilnummeret til foretaket må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/adresse", "Adressen til foretaket bør fylles ut."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/adresse/adresselinje1", "Adresselinje 1 bør fylles ut for foretaket."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/foretak/adresse/landkode", "Ugyldig landkode til foretaket"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/adresse/postnr", "Postnummeret til foretaket bør fylles ut."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/foretak/adresse/postnr", "Postnummeret '{0}' til foretaket er ikke gyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/foretak/adresse/poststed", "Postnummeret '{0}' til foretaket stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/", ValidationResultSeverityEnum.WARNING, "/foretak/adresse/postnr"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/kontaktperson", "Kontaktpersonen til foretaket må fylles ut.", ValidationResultSeverityEnum.ERROR, "/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/kontaktperson/navn", "Navnet til kontaktpersonen for foretaket må fylles ut.", ValidationResultSeverityEnum.ERROR, "/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/kontaktperson/epost", "E-postadressen til foretakets kontaktperson bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/foretak/kontaktperson/telefonnummer", "Telefonnummeret eller mobilnummeret til foretakets kontaktperson bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/foretak/kontaktperson/mobilnummer"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/foretak/kontaktperson/telefonnummer", "Telefonnummeret til foretakets kontaktperson må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/foretak/kontaktperson/mobilnummer", "Mobilnummer til foretakets kontaktperson må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR)
            };

            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> Metadata()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                //Metadata
                GetValidationMessageStorageEntry("6146", "44096", ValidationRuleEnum.utfylt, "/metadata", "Gjennomføringsplanens metadata må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7063", "47177", ValidationRuleEnum.utfylt, "/metadata", "Du må fylle ut metadata for sluttrapport med avfallsplan.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/metadata", "Søknadens metadata må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/metadata/erNorskSvenskDansk", "Informasjon om søknaden og all relevant dokumentasjon er skrevet/oversatt til norsk, svensk eller dansk må være utfylt", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/metadata/erNorskSvenskDansk", "Søknaden og all relevant dokumentasjon må være skrevet/oversatt til norsk, svensk eller dansk", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/metadata/fraSluttbrukersystem", "Systemet må fylle ut samme navn som er brukt i registrering for Altinn API i 'fraSluttbrukersystem'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/metadata/unntattOffentlighet", "Det må besvares om søknad skal unntas offentlighet", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/metadata/prosjektnavn", "Hvis det er et prosjektnavn på byggesøknaden, bør du oppgi dette.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/metadata/prosjektnr", "Hvis det er et prosjektnummer på byggesøknaden, bør du oppgi dette.", ValidationResultSeverityEnum.WARNING),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/metadata/versjonsnummerAvfallsplan", "Du må oppgi versjonsnummeret til sluttrapporten.", ValidationResultSeverityEnum.ERROR),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/metadata/foretrukketSpraak/kodeverdi", "'{0}' er en ugyldig kodeverdi for målform. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/foretrukketspraak"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/metadata/foretrukketSpraak/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Målformen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/metadata/foretrukketSpraak/kodebeskrivelse", "Når målform er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/foretrukketspraak",ValidationResultSeverityEnum.ERROR,"/metadata/foretrukketSpraak/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/metadata/foretrukketSpraak/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for målformen. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/foretrukketspraak",ValidationResultSeverityEnum.ERROR,"/metadata/foretrukketSpraak/kodeverdi"),
            };

            return validationMessageStorage;
        }


        private List<ValidationMessageStorageEntry> AnsvarsrettAnsvarsomraader()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                //ansvarsområde
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}", "Du må definere minst ett ansvarsområde.", ValidationResultSeverityEnum.ERROR),
                //KodeverdiValidatorV2 - funksjon
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/funksjon", "Du må oppgi en funksjon for ansvarsområdet. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/byggesoknad/funksjon", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/funksjon/kodeverdi", "Kodeverdien for 'funksjon' for ansvarsområdet må fylles ut. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/byggesoknad/funksjon", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/funksjon/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Funksjonen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/funksjon/kodeverdi", "'{0}' er en ugyldig kodeverdi for funksjon. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/funksjon", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/funksjon/kodebeskrivelse", "Når funksjon er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/funksjon", ValidationResultSeverityEnum.ERROR, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/funksjon/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/funksjon/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for funksjon. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/funksjon", ValidationResultSeverityEnum.WARNING, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/funksjon/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/beskrivelseAvAnsvarsomraade", "Du må fylle ut en beskrivelse av ansvarsområdet. Ansvarlig foretak kan endre beskrivelsen senere.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/soeknadssystemetsReferanse", "Feltet 'soeknadssystemetsReferanse' må være fylt ut. Ta kontakt med søknadssystemet du bruker.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/soeknadssystemetsReferanse", "Feltet 'soeknadssystemetsReferanse' må være fylt ut. Ta kontakt med søknadssystemet du bruker.", ValidationResultSeverityEnum.ERROR),

                //KodeverdiValidatorV3 - tiltaksklasse
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/tiltaksklasse/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Tiltaksklassen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/tiltaksklasse/kodeverdi", "'{0}' er en ugyldig kodeverdi for tiltaksklasse. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/tiltaksklasse ", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/tiltaksklasse/kodebeskrivelse", "Når tiltaksklasse er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/tiltaksklasse ", ValidationResultSeverityEnum.ERROR, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/tiltaksklasse/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/tiltaksklasse/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tiltaksklasse. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/tiltaksklasse", ValidationResultSeverityEnum.WARNING, "/ansvarsrett/ansvarsomraader/ansvarsomraade{0}/tiltaksklasse/kodeverdi")
            };

            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> Arbeidsplasser()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/arbeidsplasser", "Arbeidsplasser må fylles ut", ValidationResultSeverityEnum.ERROR, null,null,new Dictionary<ServiceDomain, string>{ { ServiceDomain.ATIL ,"1.13"}}),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.framtidige_eller_eksisterende_utfylt, "/arbeidsplasser", "Det må velges enten 'eksisterende' eller 'fremtidige' eller begge deler.", ValidationResultSeverityEnum.ERROR, null,null,new Dictionary<ServiceDomain, string>{ { ServiceDomain.ATIL ,"1.11"}}),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.faste_eller_midlertidige_utfylt, "/arbeidsplasser", "Det må velges enten 'faste' eller 'midlertidige' eller begge deler", ValidationResultSeverityEnum.ERROR, null,null,new Dictionary<ServiceDomain, string>{ { ServiceDomain.ATIL ,"1.13"}}),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/arbeidsplasser/antallVirksomheter", "Er tiltaket knyttet til utleiebygg så skal antall virksomheter angis.", ValidationResultSeverityEnum.ERROR, null,null,new Dictionary<ServiceDomain, string>{ { ServiceDomain.ATIL ,"1.16"}}),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/arbeidsplasser/antallAnsatte", "Det skal angis hvor mange ansatte som bygget dimensjoneres for.", ValidationResultSeverityEnum.ERROR, null,null,new Dictionary<ServiceDomain, string>{ { ServiceDomain.ATIL ,"1.10"}}),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/arbeidsplasser/beskrivelse", "Enten skal arbeidsplasser beskrives i søknaden eller det skal være lagt ved vedlegg 2: 'Beskrivelse av type arbeid / prosesser'.", ValidationResultSeverityEnum.ERROR, null,null,new Dictionary<ServiceDomain, string>{ { ServiceDomain.ATIL ,"1.19"}}),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/arbeidsplasser/beskrivelse", "Beskrivelse kan ikke inneholde mer enn 2000 tegn.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/arbeidsplasser/veiledning", "Veileding bør være utfylt.", ValidationResultSeverityEnum.ERROR)
            };
            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> Betaling()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/betaling", "Betaling må fylles ut", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/betaling/beskrivelse", "Beskrivelse må fylles ut", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/betaling/sum", "Sum må fylles ut", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/betaling/sum", "Sum må være større enn 0", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tillatt, "/betaling/sum", "Gebyrbeløp må være korrekt ('Kr. {0}') gitt søknadstypen", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/betaling/sum", "Sum må være en numerisk verdi", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/betaling/gebyrkategori", "Gebyrkategori må fylles ut", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/betaling/gebyrkategori", "Gebyrkategori må være gyldig", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/betaling/skalFaktureres", "SkalFaktureres må fylles ut", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/betaling/skalFaktureres", "SkalFaktureres må være satt til true", ValidationResultSeverityEnum.ERROR)
            };

            return validationMessageStorage;
        }
        private List<ValidationMessageStorageEntry> Fakturamottaker()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/fakturamottaker", "Fakturainformasjon må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/fakturamottaker/navn", "Fakturamottakers navn må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/fakturamottaker/navn", "Fakturamottakers navn kan ikke inneholde mer enn 70 tegn.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.ehf_eller_papir, "/fakturamottaker", "Det må angis om det ønskes faktura som EHF eller på papir.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/fakturamottaker/bestillerReferanse", "'BestillerReferanse' for fakturamottaker bør fylles ut.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/fakturamottaker/fakturareferanser", "Fakturareferanser for fakturamottaker bør fylles ut.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/fakturamottaker/organisasjonsnummer", "Organisasjonsnummeret ('{0}') for fakturamottaker har ikke gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/fakturamottaker/organisasjonsnummer", "Organisasjonsnummeret ('{0}') for fakturamottaker er ikke gyldig.", ValidationResultSeverityEnum.ERROR),

                //Fakturamottakers adresse
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/fakturamottaker/adresse", "Adresse må fylles ut for fakturamottaker.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/fakturamottaker/adresse/adresselinje1", "Adresselinje 1 bør fylles ut for fakturamottaker."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/fakturamottaker/adresse/postnr", "Postnummeret '{0}' er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/fakturamottaker/adresse/poststed", "Postnummeret '{0}' til fakturamottaker stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/", ValidationResultSeverityEnum.WARNING, "/fakturamottaker/adresse/postnr"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/fakturamottaker/adresse/landkode", "Ugyldig landkode for fakturamottaker.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/fakturamottaker/adresse/postnr", "Postnummer for fakturamottaker må fylles ut.", ValidationResultSeverityEnum.ERROR)
            };

            // ATIL Feature - RYDDES ETTER 20250217 - START
            if (atil20250217Enabled) //Slettes
                validationMessageStorage.Add(GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/fakturamottaker/organisasjonsnummer", "Organisasjonsnummer for fakturamottaker må fylles ut.", ValidationResultSeverityEnum.ERROR));
            else //Slettes og den under
                validationMessageStorage.Add(GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/fakturamottaker/organisasjonsnummer", "Organisasjonsnummer skal fylles ut for fakturamottaker når man velger ehfFaktura.", ValidationResultSeverityEnum.ERROR, "/fakturamottaker/ehfFaktura"));

            return validationMessageStorage;
        }
        private List<ValidationMessageStorageEntry> BeskrivelseAvTiltak()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak", "Du må beskrive tiltaket.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/bruk", "Du må fylle ut informasjon om tiltakets bruk.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/bruk/bygningstype", "Du må oppgi en bygningstype for byggeprosjektet. Du kan sjekke gyldige bygningstyper på https://register.geonorge.no/byggesoknad/bygningstype ", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/bruk/bygningstype/kodeverdi", "Kodeverdien for bygningstype må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/bygningstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beskrivelseAvTiltak/bruk/bygningstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for bygningstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/bygningstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/beskrivelseAvTiltak/bruk/bygningstype/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Bygningstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/bruk/bygningstype/kodebeskrivelse", "Når bygningstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/bygningstype", ValidationResultSeverityEnum.ERROR, "/beskrivelseAvTiltak/bruk/bygningstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beskrivelseAvTiltak/bruk/bygningstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for bygningstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/bygningstype", ValidationResultSeverityEnum.WARNING, "/beskrivelseAvTiltak/bruk/bygningstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/bruk/bygningstype{0}", "Du må oppgi en bygningstype for byggeprosjektet. Du kan sjekke gyldige bygningstyper på https://register.geonorge.no/byggesoknad/bygningstype ", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/bruk/bygningstype{0}/kodeverdi", "Kodeverdien for bygningstype må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/bygningstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beskrivelseAvTiltak/bruk/bygningstype{0}/kodeverdi", "'{0}' er en ugyldig kodeverdi for bygningstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/bygningstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/beskrivelseAvTiltak/bruk/bygningstype{0}/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Bygningstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/bruk/bygningstype{0}/kodebeskrivelse", "Når bygningstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/bygningstype", ValidationResultSeverityEnum.ERROR, "/beskrivelseAvTiltak/bruk/bygningstype{0}/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beskrivelseAvTiltak/bruk/bygningstype{0}/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for bygningstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/bygningstype", ValidationResultSeverityEnum.WARNING, "/beskrivelseAvTiltak/bruk/bygningstype{0}/kodeverdi"),
                GetValidationMessageStorageEntry("7063", "47177", ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/type{0}", "Du må velge en tiltakstype for byggeprosjektet. Du kan sjekke gyldige tiltakstyper på https://register.geonorge.no/byggesoknad/tiltaktype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/beskrivelseAvTiltak/type{0}/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Tiltakstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7063", "47177", ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/type{0}/kodeverdi", "Du må oppgi en tiltakstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/tiltaktype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7063", "47177", ValidationRuleEnum.gyldig, "/beskrivelseAvTiltak/type{0}/kodeverdi", "'{0}' er en ugyldig kodeverdi for tiltakstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/tiltaktype", ValidationResultSeverityEnum.ERROR),

                //TODO check if is use in other Forms
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/type{0}", "Tiltakstype må være utfylt", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/type{0}/kodeverdi", "Kodeverdi for tiltakstype må være utfylt", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beskrivelseAvTiltak/type{0}/kodeverdi", "Kodeverdi for tiltakstype må være gyldig", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/type{0}/kodebeskrivelse", "Når tiltakstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaktype", ValidationResultSeverityEnum.ERROR, "/beskrivelseAvTiltak/type{0}/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beskrivelseAvTiltak/type{0}/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tiltakstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaktype", ValidationResultSeverityEnum.WARNING, "/beskrivelseAvTiltak/type{0}/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beskrivelseAvTiltak/BRA", "Tiltakets bruksareal må være utfylt.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("6821", "45957", ValidationRuleEnum.gyldig, "/beskrivelseAvTiltak/BRA", "Tiltakets bruksareal må være et tall.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("6821", "45957", ValidationRuleEnum.tillatt, "/beskrivelseAvTiltak/BRA", "BRA må være større enn 0 for {0}.", ValidationResultSeverityEnum.ERROR)
            };


            return validationMessageStorage;
        }
        private List<ValidationMessageStorageEntry> AnsvarsrettFunksjon()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/ansvarsrett/funksjon/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Funksjonen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry("10001", "1", ValidationRuleEnum.tillatt, "/ansvarsrett/funksjon/kodeverdi", "Kodeverdien '{0}' stemmer ikke med de gyldige kodeverdiene for funksjon. Gyldige funksjoner er “Ansvarlig prosjekterende” og “Ansvarlig utførende”.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("10002", "1", ValidationRuleEnum.tillatt, "/ansvarsrett/funksjon/kodeverdi", "Kodeverdien '{0}' stemmer ikke med den gyldige kodeverdien for funksjon. Riktig funksjon er “KONTROLL”.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("10002", "1", ValidationRuleEnum.utfylt, "/ansvarsrett/funksjon", "Du må oppgi 'funksjon' for ansvarsområdet. Gyldig ansvarsområde er 'Ansvarlig kontrollerende'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("10002", "1", ValidationRuleEnum.utfylt, "/ansvarsrett/funksjon/kodeverdi", "Kodeverdien for 'funksjon' for ansvarsområdet må fylles ut. Gyldig funksjon er 'KONTROLL'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("10002", "1", ValidationRuleEnum.gyldig, "/ansvarsrett/funksjon/kodeverdi", "'{0}' er en ugyldig kodeverdi for funksjon. Gyldig funksjon er 'KONTROLL'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("10002", "1", ValidationRuleEnum.utfylt, "/ansvarsrett/funksjon/kodebeskrivelse", "Når funksjon er valgt, må kodebeskrivelse fylles ut. Riktig kodebeskrivelse er 'Ansvarlig kontrollerende'.", ValidationResultSeverityEnum.ERROR, "/ansvarsrett/funksjon/kodeverdi"),
                GetValidationMessageStorageEntry("10002", "1", ValidationRuleEnum.gyldig, "/ansvarsrett/funksjon/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for funksjon. Riktig kodebeskrivelse er 'Ansvarlig kontrollerende'.", ValidationResultSeverityEnum.WARNING, "/ansvarsrett/funksjon/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/funksjon", "Du må oppgi en funksjon for ansvarsområdet. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/byggesoknad/funksjon", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/funksjon/kodeverdi", "Kodeverdien for 'funksjon' for ansvarsområdet må fylles ut. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/byggesoknad/funksjon", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/funksjon/kodeverdi", "'{0}' er en ugyldig kodeverdi for funksjon. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/funksjon", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/funksjon/kodebeskrivelse", "Når funksjon er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/funksjon", ValidationResultSeverityEnum.ERROR, "/ansvarsrett/funksjon/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarsrett/funksjon/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for funksjon. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/funksjon", ValidationResultSeverityEnum.WARNING, "/ansvarsrett/funksjon/kodeverdi")
            };


            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> PlanForAvfall()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                //**

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan", "Du bør fylle ut avfallsplanen for byggeprosjektet. Avfallsplanen skal vise hva slags avfall du planlegger å levere, og hvor mye.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/avfall/avfallplan{0}", "Du må fylle ut informasjon om avfallet du planlegger å levere.", ValidationResultSeverityEnum.ERROR),

                // plan/fraksjon
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/avfall/avfallplan{0}/fraksjon", "Du må oppgi en fraksjon for avfallsplanen. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/fraksjoner/fraksjoner-ns", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/avfall/avfallplan{0}/fraksjon/kodeverdi", "Kodeverdien for fraksjon må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/fraksjoner/fraksjoner-ns", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plan/avfall/avfallplan{0}/fraksjon/kodeverdi", "'{0}' er en ugyldig kodeverdi for fraksjonen. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/fraksjoner/fraksjoner-ns", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/plan/avfall/avfallplan{0}/fraksjon/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Fraksjonen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/avfall/avfallplan{0}/fraksjon/kodebeskrivelse", "Når fraksjon er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/fraksjoner/fraksjoner-ns", ValidationResultSeverityEnum.ERROR, "/plan/avfall/avfallplan{0}/fraksjon/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plan/avfall/avfallplan{0}/fraksjon/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for fraksjon. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/fraksjoner/fraksjoner-ns ", ValidationResultSeverityEnum.WARNING, "/plan/avfall/avfallplan{0}/fraksjon/kodeverdi"),

                // plan/mengde
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/avfall/avfallplan{0}/mengdeFraksjon", "Du har lagt inn fraksjonen '{0}' i avfallsplanen. Da må du også fylle ut mengden som er planlagt levert. Mengden skal angis i tonn.", ValidationResultSeverityEnum.ERROR, "/plan/avfall/avfallplan{0}/fraksjon/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/plan/avfall/avfallplan{0}/mengdeFraksjon", "Den planlagte mengden '{0}' må oppgis i tall.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plan/avfall/avfallplan{0}/mengdeFraksjon", "Den planlagte mengden '{0}' kan ikke være et negativt tall.", ValidationResultSeverityEnum.ERROR),

                // plan/avfallsklasse
                GetValidationMessageStorageEntry("7063", "47177", ValidationRuleEnum.utfylt, "/plan/avfall/avfallplan{0}/avfallsklasse", "Du må oppgi en avfallsklasse for fraksjonen '{0}' i avfallplanen.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/avfall/avfallplan{0}/avfallsklasse/kodeverdi", "Kodeverdien for avfallsklasse i avfallplanen må fylles ut. Du kan sjekke gyldige avfallsklasser på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plan/avfall/avfallplan{0}/avfallsklasse/kodeverdi", "'{0}' er en ugyldig kodeverdi for avfallsklassen. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/plan/avfall/avfallplan{0}/avfallsklasse/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Avfallsklassen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/avfall/avfallplan{0}/avfallsklasse/kodebeskrivelse", "Når avfallsklassen er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.ERROR, "/plan/avfall/avfallplan{0}/avfallsklasse/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plan/avfall/avfallplan{0}/avfallsklasse/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for avfallsklasse. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.WARNING, "/plan/avfall/avfallplan{0}/avfallsklasse/kodeverdi"),

                //Ny
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.Stemmer, "/plan/avfall/avfallplan{0}/avfallsklasse/kodeverdi", "Du har valgt feil avfallsklasse for den planlagte fraksjonen. '{0}' regnes som '{1}'.", ValidationResultSeverityEnum.ERROR, "/plan/avfall/avfallplan{0}/fraksjon/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.BlandetAvfall, "/plan/avfall/avfallplan{0}/avfallsklasse/kodeverdi", "Avfallsplanen må inneholde avfallsklassen 'Blandet avfall'. Dersom du ikke planlegger å ha blandet avfall, kan du registrere '0' som mengde.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.FarligAvfallOgEllerOrdinaertAvfall, "/plan/avfall/avfallplan{0}/avfallsklasse/kodeverdi", "Avfallsplanen må inneholde avfallsklassene 'Ordinært avfall' og/eller 'Farlig avfall'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tillatt, "/plan/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodeverdi", "Du har lagt inn {0} avfallsklasse(r) i avfallsplanen ({1}), men det er lagt inn delsum for {2} avfallsklasse(r) ({3}). Hver avfallsklasse skal ha en delsum.", ValidationResultSeverityEnum.ERROR, "/plan/avfall/avfallplan{0}/avfallsklasse/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.finnes, "/plan/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse", "Du har lagt inn {0} i avfallsplanen. De samme avfallsklassene må oppgis i avfallsklassedelsum.", ValidationResultSeverityEnum.ERROR, "/plan/avfall/avfallplan{0}/avfallsklasse"),

                //plan/avfallsklasseDelsum
                GetValidationMessageStorageEntry("7063", "47177", ValidationRuleEnum.utfylt, "/plan/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse", "Du må fylle ut avfallsklassen for hver delsum du planlegger å levere.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodeverdi", "Kodeverdien for avfallsklasse for den planlagte delsummen må fylles ut. Du kan sjekke gyldige kodeverdier på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plan/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodeverdi", "'{0}' er en ugyldig kodeverdi for avfallsklassen. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/plan/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Avfallsklassen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodebeskrivelse", "Når avfallsklasse er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.ERROR, "/plan/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plan/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for fraksjon. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.WARNING, "/plan/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/avfallsklasseDelsum/avfallsklassedelsum{0}/delsum", "Du har oppgitt at du skal levere '{0}'. Du må fylle ut delsummen for de planlagte fraksjonene i avfallsklassen. Delsummen skal angis i tonn.", ValidationResultSeverityEnum.ERROR),

                // plan/sortering
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/sortering", "Når du har lagt inn alt avfallet i avfallsplanen, må du fylle ut sortert mengde, total mengde og planlagt sorteringsgrad.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/sortering/mengdeSortert", "Du må fylle ut summen av det sorterte avfallet i avfallsplanen. Utregning av sortert avfall er 'Ordinært avfall' + 'Farlig avfall'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/plan/sortering/mengdeSortert", "Summen av det sorterte avfallet i avfallsplanen må være et tall.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/sortering/mengdeTotalt", "Du må fylle ut total avfallsmengde for avfallsplanen. Utregning av total avfallsmengde er 'Ordinært avfall' + 'Farlig avfall' + 'Blandet avfall'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/plan/sortering/mengdeTotalt", "Total avfallsmengde i avfallsplanen må være tall.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plan/sortering/sorteringsgrad", "Du må fylle ut planlagt sorteringsgrad i avfallsplanen. Planlagt sorteringsgrad er 'mengdeSortert' / 'mengdeTotalt' * 100. Sorteringsgraden skal rundes av til nærmeste hele prosent.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/plan/sortering/sorteringsgrad", "Planlagt sorteringsgrad må være tall.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plan/sortering/sorteringsgrad", "Kravet til planlagt sorteringsgrad er minst 60 % for søknader sendt før 1. juli 2022. Hvis sorteringsgraden er lavere, må du redegjøre for dette i vedlegget 'Andre redegjørelser'.", ValidationResultSeverityEnum.ERROR, "/overgangsordning/datoForHovedsoeknad før 01.07.2022", null, null, null, new DateTime(2022, 06, 30)),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plan/sortering/sorteringsgrad", "Kravet til planlagt sorteringsgrad er minst 60 % for søknader sendt i perioden 1. juli 2022-30. juni 2023, der overgangsbestemmelsene er tatt i bruk. Hvis sorteringsgraden er lavere, må du redegjøre for dette i vedlegget 'Andre redegjørelser'.", ValidationResultSeverityEnum.ERROR, "/overgangsordning/overgangsbestemmelse true", null, null, new DateTime(2022, 07, 01), new DateTime(2023, 06, 30)),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plan/sortering/sorteringsgrad", "Kravet til planlagt sorteringsgrad er minst 70 % for søknader sendt i perioden 1. juli 2022-30. juni 2023, der overgangsbestemmelsene ikke er tatt i bruk. Hvis sorteringsgraden er lavere, må du redegjøre for dette i vedlegget 'Andre redegjørelser'.", ValidationResultSeverityEnum.ERROR, "/overgangsordning/overgangsbestemmelse false", null, null, new DateTime(2022, 07, 01), new DateTime(2023, 06, 30)),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plan/sortering/sorteringsgrad", "Kravet til planlagt sorteringsgrad er minst 70 % for søknader sendt etter 1. juli 2023. Hvis sorteringsgraden er lavere, må du redegjøre for dette i vedlegget 'Andre redegjørelser'.", ValidationResultSeverityEnum.ERROR, "/overgangsordning/datoForHovedsoeknad etter 30.06.2023", null, null, new DateTime(2023, 07, 01))
            };
            //**
            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> Soeker()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/ansvarligSoeker (tiltakshaver)", "Du har ikke fylt ut informasjon om søker. Hvis du søker med ansvarsrett, må du fylle ut informasjon om ansvarlig søker. For tiltak uten ansvarsrett, må du fylle ut informasjon om tiltakshaver.", ValidationResultSeverityEnum.ERROR),
                //TODO: er linje under feil? SKal det ikke validering for avfallplan
                GetValidationMessageStorageEntry("7063", "47177", ValidationRuleEnum.utfylt, "/ansvarligSoeker (tiltakshaver)", "Du har ikke fylt ut informasjon om søker. Hvis du søker med ansvarsrett, må du fylle ut informasjon om ansvarlig søker. For tiltak uten ansvarsrett, må du fylle ut informasjon om tiltakshaver.", ValidationResultSeverityEnum.ERROR)
            };

            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> SluttrapportForAvfall()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport", "Du må fylle ut sluttrapporten for avfall. Sluttrapporten skal vise hvor mye av hver type avfall som er levert, og til hvilket mottak.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/mengdeFraksjon", "Du har lagt inn fraksjonen '{0}' i sluttrapporten. Da må du også fylle ut mengden som er planlagt levert. Mengden skal angis i tonn.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/sluttrapport/avfall/avfallrapport{0}/mengdeFraksjon", "Den leverte mengden '{0}' må oppgis i tall.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/avfall/avfallrapport{0}/mengdeFraksjon", "Den leverte mengden '{0}' kan ikke være et negativt tall.", ValidationResultSeverityEnum.ERROR),

                // avfall/fraksjon
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/fraksjon", "Du må oppgi en fraksjon i sluttrapporten. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/fraksjoner/fraksjoner-ns", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/fraksjon/kodeverdi", "Kodeverdien for fraksjon må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/fraksjoner/fraksjoner-ns", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/avfall/avfallrapport{0}/fraksjon/kodeverdi", "'{0}' er en ugyldig kodeverdi for fraksjonen. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/fraksjoner/fraksjoner-ns", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/sluttrapport/avfall/avfallrapport{0}/fraksjon/kodeverdi", "Kodeverdien ‘{0}' ble ikke validert. Fraksjonen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/fraksjon/kodebeskrivelse", "Når fraksjon er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/fraksjoner/fraksjoner-ns", ValidationResultSeverityEnum.ERROR, "/sluttrapport/avfall/avfallrapport{0}/fraksjon/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/avfall/avfallrapport{0}/fraksjon/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for fraksjon. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/fraksjoner/fraksjoner-ns", ValidationResultSeverityEnum.WARNING, "/sluttrapport/avfall/avfallrapport{0}/fraksjon/kodeverdi"),

                // avfall/avfallsklasse
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse", "Du må oppgi en avfallsklasse for fraksjonen '{0}' i sluttrapporten.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse/kodeverdi", "Kodeverdien for avfallsklasse i sluttrapporten må fylles ut. Du kan sjekke gyldige avfallsklasser på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse/kodeverdi", "'{0}' er en ugyldig kodeverdi for avfallsklassen. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse/kodeverdi", "Kodeverdien ‘{0}' ble ikke validert. Avfallsklassen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse/kodebeskrivelse", "Når avfallsklassen er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse ", ValidationResultSeverityEnum.ERROR, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for avfallsklasse. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.WARNING, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.Stemmer, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse/kodeverdi", "Du har valgt feil avfallsklasse for den leverte fraksjonen. '{0}' regnes som '{1}'.", ValidationResultSeverityEnum.ERROR, "/sluttrapport/avfall/avfallrapport{0}/fraksjon/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.BlandetAvfall, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse/kodeverdi", "Sluttrapporten må inneholde avfallsklassen 'Blandet avfall'. Dersom du ikke har levert blandet avfall, kan du registrere '0' som mengde.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.FarligAvfallOgEllerOrdinaertAvfall, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse/kodeverdi", "Sluttrapporten må inneholde avfallsklassene 'Ordinært avfall' og/eller 'Farlig avfall'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/disponering", "Du må fylle ut informasjon om hvordan du har levert avfallet i sluttrapporten.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/disponering/disponeringsmaate", "Du har lagt inn fraksjonen '{0}' i sluttrapporten. For ordinært avfall må du fylle ut informasjon om hvordan avfallet er levert. Du kan sjekke gyldige disponeringsmåter på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/disponeringsmate", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/disponering/disponeringsmaate/kodeverdi", "Kodeverdien for disponeringsmåte må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/disponeringsmate ", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/avfall/avfallrapport{0}/disponering/disponeringsmaate/kodeverdi", "'{0}' er en ugyldig kodeverdi for disponeringsmåten. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/disponeringsmate ", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/sluttrapport/avfall/avfallrapport{0}/disponering/disponeringsmaate/kodeverdi", "Kodeverdien ‘{0}' ble ikke validert. Disponeringsmåten kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/disponering/disponeringsmaate/kodebeskrivelse", "Når disponeringsmåte er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/disponeringsmate ", ValidationResultSeverityEnum.ERROR, "/sluttrapport/avfall/avfallrapport{0}/disponering/disponeringsmaate/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/avfall/avfallrapport{0}/disponering/disponeringsmaate/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for disponeringsmåten. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/disponeringsmate ", ValidationResultSeverityEnum.WARNING, "/sluttrapport/avfall/avfallrapport{0}/disponering/disponeringsmaate/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/disponering/leveringssted", "Du har lagt inn fraksjonen '{0}' i sluttrapporten. Da må leveringssted være utfylt.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfall/avfallrapport{0}/disponering/leveringssted/mottaksNavn", "Du har lagt inn fraksjonen '{0}' i sluttrapporten. Da må du også oppgi navnet på mottaket der avfallet er levert.", ValidationResultSeverityEnum.ERROR),

                // sluttrapport/disponeringsmaateDelsum
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/disponeringsmaateDelsum/disponeringsmaatedelsum{0}/disponeringsmaate", "Du må fylle ut informasjon om avfallet du har levert for hver disponeringsmåte.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/disponeringsmaateDelsum/disponeringsmaatedelsum{0}/disponeringsmaate/kodeverdi", "Kodeverdien for disponeringsmåte må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/disponeringsmate", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/disponeringsmaateDelsum/disponeringsmaatedelsum{0}/disponeringsmaate/kodeverdi", "'{0}' er en ugyldig kodeverdi for disponeringsmåte. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/disponeringsmate", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/sluttrapport/disponeringsmaateDelsum/disponeringsmaatedelsum{0}/disponeringsmaate/kodeverdi", "Kodeverdien ‘{0}' ble ikke validert. Disponeringsmåten kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/disponeringsmaateDelsum/disponeringsmaatedelsum{0}/disponeringsmaate/kodebeskrivelse", "Når disponeringsmåte er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/disponeringsmate", ValidationResultSeverityEnum.ERROR, "/sluttrapport/disponeringsmaateDelsum/disponeringsmaatedelsum{0}/disponeringsmaate/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/disponeringsmaateDelsum/disponeringsmaatedelsum{0}/disponeringsmaate/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for disponeringsmåte. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/disponeringsmate", ValidationResultSeverityEnum.WARNING, "/sluttrapport/disponeringsmaateDelsum/disponeringsmaatedelsum{0}/disponeringsmaate/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/disponeringsmaateDelsum/disponeringsmaatedelsum{0}/delsum", "Du har oppgitt at avfallet har blitt '{0}'. Da må du fylle ut den totale mengden du har levert på denne måten.", ValidationResultSeverityEnum.ERROR, "/sluttrapport/disponeringsmaateDelsum/disponeringsmaatedelsum{0}"),

                // sluttrapport/sortering
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/sortering", "Når du har lagt inn alle fraksjonene i sluttrapporten, må du fylle ut sortert mengde, total mengde og faktisk sorteringsgrad.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/sortering/mengdeSortert", "Du må fylle ut summen av det sorterte avfallet i sluttrapporten. Utregning av sortert avfall er 'Ordinært avfall' + 'Farlig avfall'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/sluttrapport/sortering/mengdeSortert", "Summen av det sorterte avfallet i sluttrapporten må være et tall.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/sortering/mengdeTotalt", "Du må fylle ut total avfallsmengde for sluttrapporten. Utregning av total avfallsmengde er 'Ordinært avfall' + 'Farlig avfall' + 'Blandet avfall'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/sluttrapport/sortering/mengdeTotalt", "Total avfallsmengde i sluttrapporten må være tall.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/sortering/sorteringsgrad", "Du må fylle ut faktisk sorteringsgrad i sluttrapporten. Sorteringsgraden er 'mengdeSortert' / 'mengdeTotalt' * 100. Sorteringsgraden skal rundes av til nærmeste hele prosent.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/sluttrapport/sortering/sorteringsgrad", "Faktisk sorteringsgrad må være tall.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/sortering/sorteringsgrad", "Kravet til faktisk sorteringsgrad er minst 60 % for søknader sendt før 1. juli 2022, jfr. nasjonal sjekkliste punkt 4.7. Hvis sorteringsgraden er lavere, må du redegjøre for dette i vedlegget 'Andre redegjørelser'.", ValidationResultSeverityEnum.ERROR, "/overgangsordning/datoForHovedsoeknad før 01.07.2022", null, null, null, new DateTime(2022, 06, 30)),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/sortering/sorteringsgrad", "Kravet til faktisk sorteringsgrad er minst 60 % for søknader sendt i perioden 1. juli 2022-30. juni 2023, der overgangsbestemmelsene er tatt i bruk, jfr. nasjonal sjekkliste punkt 4.17. Hvis sorteringsgraden er lavere, må du redegjøre for dette i vedlegget 'Andre redegjørelser'.", ValidationResultSeverityEnum.ERROR, "/overgangsordning/overgangsbestemmelse true", null, null, new DateTime(2022, 07, 01), new DateTime(2023, 06, 30)),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/sortering/sorteringsgrad", "Kravet til faktisk sorteringsgrad er minst 70 % for søknader sendt i perioden 1. juli 2022-30. juni 2023, der overgangsbestemmelsene ikke er tatt i bruk, jfr. nasjonal sjekkliste punkt 4.17. Hvis sorteringsgraden er lavere, må du redegjøre for dette i vedlegget 'Andre redegjørelser'.", ValidationResultSeverityEnum.ERROR, "/overgangsordning/overgangsbestemmelse false", null, null, new DateTime(2022, 07, 01), new DateTime(2023, 06, 30)),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/sortering/sorteringsgrad", "Kravet til faktisk sorteringsgrad er minst 70 % for søknader sendt etter 1. juli 2023, jfr. nasjonal sjekkliste punkt 4.12. Hvis sorteringsgraden er lavere, må du redegjøre for dette i vedlegget 'Andre redegjørelser'.", ValidationResultSeverityEnum.ERROR, "/overgangsordning/datoForHovedsoeknad etter 30.06.2023", null, null, new DateTime(2023, 07, 01)),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/sortering/mengdeAvfallPerAreal", "Du må fylle ut mengden avfall per m2 bruksareal (BRA). Utregningen er 'mengdeTotalt' / 'bruksareal'. Mengden skal angis i kilo.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/sluttrapport/sortering/mengdeAvfallPerAreal", "Mengden avfall per m2 bruksareal (BRA) må være et tall.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/sluttrapport/sortering/sorteringsgrad", "Kravet til faktisk sorteringsgrad er minst 60 %. Hvis sorteringsgraden er lavere, må du redegjøre for dette i vedlegget 'Andre redegjørelser', jfr. nasjonal sjekkliste punkt 4.7.", ValidationResultSeverityEnum.ERROR, "/Vedlegg"),

                // sluttrapport/avfallsklasseDelsum
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse", "Du må fylle ut avfallsklassen for hver delsum i sluttrapporten.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodeverdi", "Kodeverdien for avfallsklasse for den leverte delsummen må fylles ut. Du kan sjekke gyldige kodeverdier på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodeverdi", "'{0}' er en ugyldig kodeverdi for avfallsklassen. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/sluttrapport/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodeverdi", "Kodeverdien ‘{0}' ble ikke validert. Avfallsklassen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodebeskrivelse", "Når avfallsklasse er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.ERROR, "/sluttrapport/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sluttrapport/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for avfallsklasse. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/sluttrapport-med-avfallsplan/avfallklasse", ValidationResultSeverityEnum.WARNING, "/sluttrapport/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sluttrapport/avfallsklasseDelsum/avfallsklassedelsum{0}/delsum", "Du har oppgitt at du skal levere '{0}'. Du må fylle ut summen av de planlagte fraksjonene i avfallsklassen. Denne delsummen skal angis i tonn.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tillatt, "/sluttrapport/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse/kodeverdi", "Du har lagt inn {0} avfallsklasse(r) i sluttrapporten ({1}), men det er lagt inn delsum for {2} avfallsklasse(r) ({3}). Hver avfallsklasse skal ha en delsum.", ValidationResultSeverityEnum.ERROR, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.finnes, "/sluttrapport/avfallsklasseDelsum/avfallsklassedelsum{0}/avfallsklasse", "Du har lagt inn {0} i sluttrapporten. De samme avfallsklassene må oppgis i avfallsklassedelsum.", ValidationResultSeverityEnum.ERROR, "/sluttrapport/avfall/avfallrapport{0}/avfallsklasse")
            };

            return validationMessageStorage;
        }
        private List<ValidationMessageStorageEntry> kommunensSaksnummer()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                //KommunensSaksnummer
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/kommunensSaksnummer", "Hvis du har mottatt kommunens saksnummer, må du oppgi dette.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry("10003", "4", ValidationRuleEnum.utfylt, "/kommunensSaksnummer", "Du må oppgi kommunens saksnummer med saksår og sekvensnummer.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("10004", "4", ValidationRuleEnum.utfylt, "/kommunensSaksnummer", "Du må oppgi kommunens saksnummer med saksår og sekvensnummer.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("10005", "4", ValidationRuleEnum.utfylt, "/kommunensSaksnummer", "Du må oppgi kommunens saksnummer med saksår og sekvensnummer.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/kommunensSaksnummer/saksaar", "Du må oppgi kommunens saksnummer med saksår.", ValidationResultSeverityEnum.ERROR, "/kommunensSaksnummer/sakssekvensnummer"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/kommunensSaksnummer/saksaar", "Saksår ({0}) for kommunens saksnummer er ikke gyldig. Saksåret må inneholde fire siffer, og ikke være eldre enn 30 år.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/kommunensSaksnummer/sakssekvensnummer", "Du må oppgi kommunens saksnummer med sekvensnummer.", ValidationResultSeverityEnum.ERROR, "/kommunensSaksnummer/saksaar"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/kommunensSaksnummer/saksnummer{0}", "Du må oppgi kommunens saksnummer med saksår og sekvensnummer.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/kommunensSaksnummer/saksnummer{0}/saksaar", "Du må oppgi kommunens saksnummer med saksår.", ValidationResultSeverityEnum.ERROR, "/kommunensSaksnummer/saksnummer{0}/sakssekvensnummer"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/kommunensSaksnummer/saksnummer{0}/saksaar", "Saksår ({0}) for kommunens saksnummer er ikke gyldig. Saksåret må inneholde fire siffer, og ikke være eldre enn 30 år.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/kommunensSaksnummer/saksnummer{0}/sakssekvensnummer", "Du må oppgi kommunens saksnummer med sekvensnummer.", ValidationResultSeverityEnum.ERROR, "/kommunensSaksnummer/saksnummer{0}/saksaar")
            };

            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> Ettersending()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}", "Du har valgt å ettersende informasjon, men informasjon om ettersending mangler.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}/tittel", "Du har valgt å ettersende informasjon. Da må du lage en tittel som beskriver det du ettersender.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}/vedlegg (underskjema) (kommentar)", "Du har valgt å ettersende informasjon. Da må du enten skrive en kommentar eller fylle ut informasjon om hvilke vedlegg/underskjema du skal ettersende.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/versjonsnummer", "Versjonsnummer mangler.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/versjonsdato", "Versjonsdato mangler.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/filnavn", "Filnavn mangler.", ValidationResultSeverityEnum.ERROR),

                //For illustration purposes: in case of ettersending for other forms than Arbeidstilsynet (maybe implemented later...)
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/vedleggstype", "Du må oppgi en vedleggstype for vedlegget i ettersendingen. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/byggesoknad/vedlegg-og-underskjema/vedlegg", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/vedleggstype", "Du må oppgi en vedleggstype for vedlegget i ettersendingen. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/arbeidstilsynet/vedlegg", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi", "Kodeverdien for 'vedleggstype' for vedlegget i ettersendingen må fylles ut. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/arbeidstilsynet/vedlegg", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.gyldig, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for vedleggstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/arbeidstilsynet/vedlegg", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.validert, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Vedleggstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/vedleggstype/kodebeskrivelse", "Når vedleggstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/arbeidstilsynet/vedlegg", ValidationResultSeverityEnum.ERROR, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi"),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.gyldig, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/vedleggstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for vedleggstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/arbeidstilsynet/vedlegg", ValidationResultSeverityEnum.WARNING, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}/tema", "Du må oppgi et tema for ettersendingen. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/ebyggesak/tema", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}/tema/kodeverdi", "Kodeverdien for 'tema' for ettersendingen må fylles ut. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/ebyggesak/tema", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ettersending/ettersending{0}/tema/kodeverdi", "'{0}' er en ugyldig kodeverdi for tema for ettersendingen. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/ebyggesak/tema", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/ettersending/ettersending{0}/tema/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Tema kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ettersending/ettersending{0}/tema/kodebeskrivelse", "Når tema for ettersendingen er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/ebyggesak/tema", ValidationResultSeverityEnum.ERROR, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ettersending/ettersending{0}/tema/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tema for ettersendingen. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/ebyggesak/tema", ValidationResultSeverityEnum.WARNING, "/ettersending/ettersending{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi")
            };

            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> Mangelbesvarelse()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}", "Du har valgt å besvare en mangel fra kommunen, men informasjon om mangelbesvarelse mangler.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/tittel", "Du har valgt å besvare en mangel fra kommunen. Da må du lage en tittel som beskriver det du svarer på.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/beskrivelseFraKommunen", "Du bør legge inn en beskrivelse av mangelen.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/erMangelBesvaresSenere", "Du må velge om mangelen skal besvares senere.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/kommentar", "Du har bedt om å få besvare mangelen senere. Da bør du skrive en kommentar til kommunen.", ValidationResultSeverityEnum.WARNING, "/mangelbesvarelse/mangelbesvarelse{0}/erMangelBesvaresSenere"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg (underskjema) (kommentar)", "Du har valgt å besvare en mangel fra kommunen. Da må du enten skrive en kommentar eller fylle ut informasjon om hvilke vedlegg/underskjema du skal sende inn.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/versjonsnummer", "Versjonsnummer mangler.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/versjonsdato", "Versjonsdato mangler.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/filnavn", "Filnavn mangler.", ValidationResultSeverityEnum.ERROR),

                //For illustration purposes: in case of ettersending for other forms than Arbeidstilsynet (maybe implemented later...)
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/vedleggstype", "Du må oppgi en vedleggstype for vedlegget i mangelbesvarelsen. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/byggesoknad/vedlegg-og-underskjema/vedlegg", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/vedleggstype", "Du må oppgi en vedleggstype for vedlegget i mangelbesvarelsen. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/arbeidstilsynet/vedlegg", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi", "Kodeverdien for 'vedleggstype' for vedlegget i mangelbesvarelsen må fylles ut. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/arbeidstilsynet/vedlegg", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.gyldig, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for vedleggstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/arbeidstilsynet/vedlegg", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.validert, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Vedleggstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/vedleggstype/kodebeskrivelse", "Når vedleggstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/arbeidstilsynet/vedlegg", ValidationResultSeverityEnum.ERROR, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi"),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.gyldig, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/vedleggstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for vedleggstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/arbeidstilsynet/vedlegg", ValidationResultSeverityEnum.WARNING, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/tema", "Du må oppgi et tema for mangelbesvarelsen. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/ebyggesak/tema", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/tema/kodeverdi", "Kodeverdien for 'tema' for mangelbesvarelsen må fylles ut. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/kodelister/ebyggesak/tema", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/mangelbesvarelse/mangelbesvarelse{0}/tema/kodeverdi", "'{0}' er en ugyldig kodeverdi for tema for mangelbesvarelsen. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/ebyggesak/tema", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/mangelbesvarelse/mangelbesvarelse{0}/tema/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Tema kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/tema/kodebeskrivelse", "Når tema for mangelbesvarelsen er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/ebyggesak/tema", ValidationResultSeverityEnum.ERROR, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/mangelbesvarelse/mangelbesvarelse{0}/tema/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tema for mangelbesvarelsen. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/ebyggesak/tema", ValidationResultSeverityEnum.WARNING, "/mangelbesvarelse/mangelbesvarelse{0}/vedlegg/vedlegg{0}/vedleggstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/mangel", "Du må oppgi en mangel for mangelbesvarelsen.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/mangel/mangelId", "Du må oppgi en mangelId for mangelbesvarelsen.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/mangel/mangeltekst", "Du må oppgi en mangeltekst for mangelbesvarelsen. Du kan sjekke gyldige mangler på https://register.geonorge.no/kodelister/ebyggesak/mangel", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/mangel/mangeltekst/kodeverdi", "Kodeverdien for mangelteksten i mangelbesvarelsen må fylles ut. Du kan sjekke gyldige mangler på https://register.geonorge.no/kodelister/ebyggesak/mangel", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/mangelbesvarelse/mangelbesvarelse{0}/mangel/mangeltekst/kodeverdi", "'{0}' er en ugyldig kodeverdi for mangeltekst i mangelbesvarelsen. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/ebyggesak/mangel", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/mangelbesvarelse/mangelbesvarelse{0}/mangel/mangeltekst/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Mangel kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/mangelbesvarelse/mangelbesvarelse{0}/mangel/mangeltekst/kodebeskrivelse", "Når mangeltekst i mangelbesvarelsen er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/ebyggesak/mangel", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/mangelbesvarelse/mangelbesvarelse{0}/mangel/mangeltekst/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for mangeltekst i mangelbesvarelsen. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/ebyggesak/mangel", ValidationResultSeverityEnum.WARNING)
            };

            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> Attachment()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/attachment", "Skjema skal ikke ha noen vedlegg.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/attachment/attachmentTypeName", "Du må fylle ut vedleggstype.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/attachment/attachmentTypeName", "Du må fylle ut vedleggstype. Du kan sjekke gyldige vedleggstyper på https://tt02.altinn.no/api/metadata/formtask/5851/1", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/attachment/attachmentTypeName", "Vedleggstype '{0}' er ikke gyldig i henhold til metadata for skjema.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/attachment/filnavn", "Filen må ha et navn.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/attachment/filnavn", "Ugyldig filnavn. Filnavn skal ikke inneholde '{0}'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/attachment/allowedFileTypes", "Filtype for vedlegg '{0}' må være i henhold til metadata for skjema. Filtype '{1}' er ikke gyldig.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/attachment/maxFileSize", "Filstørrelse for vedleggstype '{0}' må fylles ut", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/attachment/maxFileSize", "Filstørrelse for vedleggsfil '{0}' er større enn '{1}' for vedleggstype '{2}'.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/vedlegg", "Maks antall vedlegg for vedleggstype '{0}' er '{1}' og du har sendt '{2}'.", ValidationResultSeverityEnum.ERROR, "/maxAttachmentCount"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/vedlegg", "Minimum antall vedlegg for vedleggstype '{0}' er '{1}' og du har sendt '{2}'.", ValidationResultSeverityEnum.ERROR, "/minAttachmentCount")
            };

            return validationMessageStorage;
        }
        private List<ValidationMessageStorageEntry> Overgangsordning()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>();
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/overgangsordning/datoForHovedsoeknad", "Du må registrere datoen hovedsøknaden ble sendt til kommunen.", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/overgangsordning/overgangsbestemmelse", "Hovedsøknaden ble sendt mellom 1. juli 2022 og 30. juni 2023. Da må du må velge om du skal følge kravene som gjaldt før eller etter forskriftsendringen 1. juli 2022.", ValidationResultSeverityEnum.ERROR);
            return validationMessageStorage;
        }

        // PLAN

        private List<ValidationMessageStorageEntry> InitiateMessageStorageEntries()
        {

            // Standard
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/xml", "Innhold må være i henhold til informasjonsmodell/XSD for skjema. Ta kontakt med søknadssystemet du bruker. XSD errors: {0}", ValidationResultSeverityEnum.ERROR);

            //Eiendombyggested
            _validationMessageStorageEntry.AddRange(Eiendombyggested());

            //Metadata
            _validationMessageStorageEntry.AddRange(Metadata());

            //Ansvarlig søker
            _validationMessageStorageEntry.AddRange(AnsvarligSoker());

            //Tiltakshaver
            _validationMessageStorageEntry.AddRange(Tiltakshaver());

            //foretak
            _validationMessageStorageEntry.AddRange(Foretak());

            //arbeidsplasser
            _validationMessageStorageEntry.AddRange(Arbeidsplasser());

            //Betaling
            _validationMessageStorageEntry.AddRange(Betaling());

            //fakturamottaker
            _validationMessageStorageEntry.AddRange(Fakturamottaker());

            //BeskrivelseAvTiltak
            _validationMessageStorageEntry.AddRange(BeskrivelseAvTiltak());

            //Ansvarsrett
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/beskrivelseAvAnsvarsomraadet", "Du må fylle ut en beskrivelse av ansvarsområdet. Husk at beskrivelsen må være identisk med beskrivelsen i den signerte ansvarserklæringen.", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett/ansvarsrettErklaert", "Du må fylle ut datoen for når foretaket erklærte ansvar for dette ansvarsområdet.", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarsrett", "Du må definere et ansvarsområde.", ValidationResultSeverityEnum.ERROR);

            //Ansvarsrett foretak
            _validationMessageStorageEntry.AddRange(AnsvarsrettForetak());
            //Ansvarsrett ansvarsomraader
            _validationMessageStorageEntry.AddRange(AnsvarsrettAnsvarsomraader());

            //Ansvarsrett Funksjon
            _validationMessageStorageEntry.AddRange(AnsvarsrettFunksjon());
            _validationMessageStorageEntry.AddRange(PlanForAvfall());
            _validationMessageStorageEntry.AddRange(SluttrapportForAvfall());

            //Soeker
            _validationMessageStorageEntry.AddRange(Soeker());

            //kommunensSaksnummer
            _validationMessageStorageEntry.AddRange(kommunensSaksnummer());

            //sjekklistekrav
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/krav/sjekklistekrav{0}", "Krav '{0}' må være utfylt", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/krav/sjekklistekrav{0}", "Minst ett av punktene i sjekklisten til fysisk arbeidsmiljø må erklæres som planlagt/ivaretatt i tråd med kravene", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/krav/sjekklistekrav{0}/sjekklistepunkt", "Kravet må være utfylt", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/krav/sjekklistekrav{0}/sjekklistepunkt/kodeverdi", "Kodeverdien for sjekklistepunkt '{0}' må være utfylt", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/krav/sjekklistekrav{0}/sjekklistepunkt/kodeverdi", "Kodeverdien for sjekklistepunkt '{0}' må være gyldig", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/krav/sjekklistekrav{0}/sjekklistepunkt/kodeverdi", "Kodeverdien for sjekklistepunkt '{0}' kunne ikke valideres", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/krav/sjekklistekrav{0}/sjekklistepunktsvar", "Sjekklistepunktet '{0}' må være besvart med ja/nei", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/krav/sjekklistekrav{0}/dokumentasjon", "Dokumentasjon er påkrevd for sjekklistekravet '{0}'", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.sjekklistepunkt_1_18_dokumentasjon_utfylt, "/krav/sjekklistekrav{0}/dokumentasjon", "Dokumentasjon ang. utleiebygg er påkrevd for sjekklistekravet '{0}'", ValidationResultSeverityEnum.ERROR);

            //arbeidstilsynetsSaksnummer - begge søknader
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/arbeidstilsynetsSaksnummer/saksaar", "Saksår ({0}) for Arbeidstilsynets saksnummer er ikke gyldig.", ValidationResultSeverityEnum.ERROR);

            //arbeidstilsynetsSaksnummer - søknad om samtykke
            AddRuleToValidationMessageStorageEntry("6821", "45957", ValidationRuleEnum.utfylt, "/arbeidstilsynetsSaksnummer", "Informasjon om 'ArbeidstilsynetsSaksnummer' bør fylles ut.", ValidationResultSeverityEnum.WARNING);
            AddRuleToValidationMessageStorageEntry("6821", "45957", ValidationRuleEnum.utfylt, "/arbeidstilsynetsSaksnummer/saksaar", "Arbeidstilsynets saksår bør fylles ut.", ValidationResultSeverityEnum.WARNING);
            AddRuleToValidationMessageStorageEntry("6821", "45957", ValidationRuleEnum.utfylt, "/arbeidstilsynetsSaksnummer/sakssekvensnummer", "Arbeidstilsynets sakssekvensnummer bør fylles ut.", ValidationResultSeverityEnum.WARNING);

            //arbeidstilsynetsSaksnummer - Arbeidstilsynet supplering
            AddRuleToValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/arbeidstilsynetsSaksnummer", "Informasjon om 'ArbeidstilsynetsSaksnummer' må fylles ut.", ValidationResultSeverityEnum.ERROR, "/mangelbesvarelse");
            AddRuleToValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/arbeidstilsynetsSaksnummer", "Informasjon om 'ArbeidstilsynetsSaksnummer' bør fylles ut.", ValidationResultSeverityEnum.WARNING, "/ettersending");
            AddRuleToValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/arbeidstilsynetsSaksnummer/saksaar", "Du må oppgi Arbeidstilsynets saksnummer med saksår.", ValidationResultSeverityEnum.ERROR, "/mangelbesvarelse");
            AddRuleToValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/arbeidstilsynetsSaksnummer/saksaar", "Du bør oppgi Arbeidstilsynets saksnummer med saksår.", ValidationResultSeverityEnum.WARNING, "/ettersending");
            AddRuleToValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/arbeidstilsynetsSaksnummer/sakssekvensnummer", "Du må oppgi Arbeidstilsynets saksnummer med sekvensnummer.", ValidationResultSeverityEnum.ERROR, "/mangelbesvarelse");
            AddRuleToValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/arbeidstilsynetsSaksnummer/sakssekvensnummer", "Du bør oppgi Arbeidstilsynets saksnummer med sekvensnummer.", ValidationResultSeverityEnum.WARNING, "/ettersending");

            //TODO "ArbeidstilsynetsSamtykke" to "ArbeidstilsynetsSamtykkeV2"/"ArbeidstilsynetsSamtykkeDfv45957"?? rule may need to have dfv in the first "node" in order to connect the text to the correct version and correct schema.

            //**ANSAKO
            // ErklaeringAnsvarsrett
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/prosjektnavn", "Hvis det er et prosjektnavn på byggesøknaden, bør du oppgi dette.", ValidationResultSeverityEnum.WARNING);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/prosjektnr", "Hvis det er et prosjektnummer på byggesøknaden, bør du oppgi dette.", ValidationResultSeverityEnum.WARNING);

            //Vedleggsvalidering
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/TiltakshaverSignatur", "Du må sende med vedlegget TiltakshaverSignatur.", ValidationResultSeverityEnum.ERROR);

            // ATIL Feature - ENDRES ETTER 20250217 - START
            if (atil20250217Enabled) //Slettes
            {
                AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/Arbeidsmiljoutvalg", "Virksomheter med minst 30 arbeidstakere skal ha arbeidsmiljøutvalg (AMU). Når det er krav om AMU, skal det dokumenteres at AMU har behandlet planene. Det gjelder uavhengig av tiltakets størrelse. Vennligst last opp signert dokumentasjon fra AMU", ValidationResultSeverityEnum.ERROR);
                AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/TegningEksisterendePlan", "Plantegning som viser eksisterende bygningsmasse skal legges ved søknaden.", ValidationResultSeverityEnum.ERROR, "/krav/sjekklistekrav");
            }
            else //Slettes og den under
                AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/Arbeidsmiljoutvalg", "Du bør sende med vedlegget som inneholder signatur fra Arbeidsmiljøutvalg.", ValidationResultSeverityEnum.WARNING);

            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/ArbeidstakersRepresentant", "Du bør sende med vedlegget som inneholder signatur fra Arbeidstakernes representant.", ValidationResultSeverityEnum.WARNING);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/Verneombud", "Du bør sende med vedlegget som inneholder signatur fra Verneombud.", ValidationResultSeverityEnum.WARNING);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/LeietakerArbeidsgiver", "Du bør sende med vedlegget som inneholder signatur fra Leietaker - arbeidsgiver.", ValidationResultSeverityEnum.WARNING);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/Bedriftshelsetjeneste", "Du bør sende med vedlegget som inneholder signatur fra Bedriftshelsetjeneste.", ValidationResultSeverityEnum.WARNING);

            AddRuleToValidationMessageStorageEntry("6821", "45957", ValidationRuleEnum.vedlegg, "/vedlegg/Soknad", "Det er ikke tillatt med mer enn ett vedlegg av typen 'Soknad'.", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry("6821", "45957", ValidationRuleEnum.vedlegg, "/vedlegg/SoknadUnntasOffentlighet", "Når søknaden skal unntas offentlighet, må vedlegget 'SoknadUnntasOffentlighet' legges ved.", ValidationResultSeverityEnum.ERROR);

            //Avfallplan
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/bruksarealAvfall", "Du må oppgi samlet bruksareal (BRA) for tiltaket. Husk at BRA for tiltaket ikke er det samme som BRA for bygningen, dersom tiltaket kun omfatter en del av en bygning.", ValidationResultSeverityEnum.ERROR);
            AddRuleToValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/avfallEtterRiving", "Dersom tiltaket medfører riving, må du erklære at alt avfall etter riving er innlevert, jfr. nasjonal sjekkliste punkt 4.11.", ValidationResultSeverityEnum.WARNING);
            //overgangsordning
            _validationMessageStorageEntry.AddRange(Overgangsordning());



            //Ettersending
            _validationMessageStorageEntry.AddRange(Ettersending());

            //Mangelbesvarelse
            _validationMessageStorageEntry.AddRange(Mangelbesvarelse());

            //SuppleringArbeidstilsynetForm
            AddRuleToValidationMessageStorageEntry("7086", "47365", ValidationRuleEnum.utfylt, "/ettersending (mangelbesvarelse)", "Du må fylle ut informasjon om mangelbesvarelse og/eller ettersending.", ValidationResultSeverityEnum.ERROR);

            //Attachments Altinnmetadata
            _validationMessageStorageEntry.AddRange(Attachment());

            //Generelle vilkår
            _validationMessageStorageEntry.AddRange(Generalvilkår());

            //PLAN
            _validationMessageStorageEntry.AddRange(new ValidationMessagePlanDb().InitiateMessageRepository());

            //New FTB
            _validationMessageStorageEntry.AddRange(new ValidationMessageFtbDb().InitiateMessageRepository());
            return _validationMessageStorageEntry;
        }

        private List<ValidationMessageStorageEntry> Generalvilkår()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/generelleVilkaar", "Du må svare på de generelle vilkårene.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null,null,ValidationRuleEnum.utfylt,"/generelleVilkaar/beroererArbeidsplasser","Du må svare på om tiltaket berører eksisterende eller fremtidige arbeidsplasser, jfr. nasjonal sjekkliste punkt 5.1.", ValidationResultSeverityEnum.ERROR,null,null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "5.1" }}),
                GetValidationMessageStorageEntry(null,null,ValidationRuleEnum.utfylt,"/generelleVilkaar/norskSvenskDansk","Du må svare på om søknaden og all relevant dokumentasjon er skrevet på eller oversatt til norsk, svensk eller dansk.",ValidationResultSeverityEnum.ERROR,null,null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "1.1" }}),
                GetValidationMessageStorageEntry(null,null,ValidationRuleEnum.vedlegg,"/vedlegg/SamtykkeArbeidstilsynet","Når en velger at yrkesbygg berører arbeidsplasser, må samtykke fra Arbeidstilsynet være på plass før igangsettingstillatelse kan gis. Det anbefales at vedlegg ‘SamtykkeArbeidstilsynet’ legges ved denne søknaden.",ValidationResultSeverityEnum.WARNING,"/generelleVilkaar/beroererArbeidsplasser = true & soeknadGjelder/delsoeknadsnummer = 1",null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "5.2" }}),
            };

            return validationMessageStorage;
        }

    }
}