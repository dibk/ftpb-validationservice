using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Enums;
using System.Collections.Generic;
using static Dibk.Ftpb.Validation.Application.Reporter.DataBase.DbHelpers;

namespace Dibk.Ftpb.Validation.Application.Reporter.DataBase
{
    internal class ValidationMessagePlanDb : IValidationMessageDb
    {
        private List<ValidationMessageStorageEntry> _validationMessageStorageEntry;

        public ValidationMessagePlanDb()
        {
            _validationMessageStorageEntry = new List<ValidationMessageStorageEntry>();

        }
        public List<ValidationMessageStorageEntry> InitiateMessageRepository()
        {
            var storage = new List<ValidationMessageStorageEntry>();
            storage.AddRange(InitiateMessageStorageEntries());

            return storage;
        }

        private List<ValidationMessageStorageEntry> InitiateMessageStorageEntries()
        {
            _validationMessageStorageEntry.AddRange(Eiendombyggested());
            _validationMessageStorageEntry.AddRange(Beroertpart());
            _validationMessageStorageEntry.AddRange(Forslagsstiller());
            _validationMessageStorageEntry.AddRange(Plankonsulent());
            _validationMessageStorageEntry.AddRange(Planforslag());
            _validationMessageStorageEntry.AddRange(ModelMessageStorageEntries());
            _validationMessageStorageEntry.AddRange(VedleggEntries());
            _validationMessageStorageEntry.AddRange(Avsender());

            return _validationMessageStorageEntry;
        }

        private List<ValidationMessageStorageEntry> Avsender()
        {
            var submitterMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/avsender/forslagsstiller", "Identiteten (organisasjonsnummeret eller fødselsnummeret) til avsender må være lik identiteten som er oppgitt for forslagsstiller.", ValidationResultSeverityEnum.ERROR, "/forslagsstiller/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/avsender/plankonsulent", "Identiteten (organisasjonsnummeret) til avsender må være lik identiteten som er oppgitt for plankonsulent.", ValidationResultSeverityEnum.ERROR, "/plankonsulent/partstype/kodeverdi")
            };

            return submitterMessageStorage;
        }

        private List<ValidationMessageStorageEntry> VedleggEntries()
        {
            var vedleggMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/PlanomraadePdf", "Kart med planavgrensning (‘Planområde’) skal sendes med som PDF-fil.", ValidationResultSeverityEnum.ERROR,"/beroerteParter/beroertpart/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/Planomraade", "Når du har valgt å sende varsel til en høringsmyndighet, må kart med planavgrensning ('Planområde') sendes med som GML- eller SOSI-fil.", ValidationResultSeverityEnum.ERROR,"/beroerteParter/beroertpart/erHoeringsmyndighet"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/KartDetaljert", "Detaljert kart bør sendes med varselet. Husk å markere kartet med gårds- og bruksnummer eller adresse."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/Planinitiativ", "‘Planinitiativ’ bør sendes med varselet hvis planinitiativet ikke ligger på en nettside.",ValidationResultSeverityEnum.WARNING,"/planforslag/hjemmesidePlanprogram"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/Planinitiativ", "‘Planinitiativ’ bør sendes med varselet hvis planinitiativet ikke ligger på en nettside.",ValidationResultSeverityEnum.WARNING,"/planforslag/hjemmesidePlanforslag"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/Planprogram", "Du har valgt at planforslaget utløser krav om konsekvensutredning. Da må du sende med vedlegget ‘Planprogram’.",ValidationResultSeverityEnum.WARNING, "/planforslag/kravKonsekvensUtredning"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/Annet", "Du kan sende med annen informasjon, som for eksempel følgebrev."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/ReferatOppstartsmoete", "Referat fra oppstartsmøtet bør sendes med varselet."),

            };

            return vedleggMessageStorage;
        }

        private List<ValidationMessageStorageEntry> Eiendombyggested()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry("11000", "2.0", ValidationRuleEnum.utfylt, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/kommunenummer", "Du må fylle ut kommunenummeret til kommunen du vil regulere i.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("11000", "2.0", ValidationRuleEnum.gyldig, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/kommunenummer", "Kommunenummeret '{0}' finnes ikke i kodelisten. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("11000", "2.0", ValidationRuleEnum.validert, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/kommunenummer", "En teknisk feil gjør at vi ikke kan bekrefte om kommunenummeret du har oppgitt '{0}' er riktig. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry("11000", "2.0", ValidationRuleEnum.status, "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/kommunenummer", "Kommunenummeret '{0}' har ugyldig status ({1}). Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer ", ValidationResultSeverityEnum.ERROR)
            };

            return validationMessageStorage;
        }
        private List<ValidationMessageStorageEntry> BeroertpartAdresse()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beroerteParter{0}/adresse", "Adresse bør fylles ut for den berørte parten."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beroerteParter{0}/adresse/adresselinje1", "Adresselinje 1 bør fylles ut for den berørte parten."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beroerteParter{0}/adresse/postnr", "Du bør fylle ut postnummeret til den berørte parten."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beroerteParter{0}/adresse/postnr", "Postnummeret '{0}' for den berørte parten er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beroerteParter{0}/adresse/poststed", "Postnummeret '{0}' for den berørte parten stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/", ValidationResultSeverityEnum.WARNING, "/beroerteParter{0}/adresse/postnr"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beroerteParter{0}/adresse/landkode", "Landkode '{0}' for den berørte partens adresse, er ikke gyldig."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tillatt, "/beroerteParter{0}/adresse/landkode", "Landkoden til berørt parts adresse må være norsk.")
            };

            return validationMessageStorage;
        }
        private List<ValidationMessageStorageEntry> BeroertpartPartType()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beroerteParter{0}/partstype", "Du må oppgi partstypen for den berørte parten. Du kan sjekke gyldige partstyper på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beroerteParter{0}/partstype/kodeverdi", "Kodeverdien for 'partstype' til den berørte parten må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beroerteParter{0}/partstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for partstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/beroerteParter{0}/partstype/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Partstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beroerteParter{0}/partstype/kodebeskrivelse", "Når partstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR, "/beroerteParter{0}/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beroerteParter{0}/partstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for partstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR, "/beroerteParter{0}/partstype/kodeverdi")
            };

            return validationMessageStorage;
        }


        private List<ValidationMessageStorageEntry> Beroertpart()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beroerteParter", "Du må legge til minst én berørt part", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beroerteParter{0}", "Du må fylle ut informasjon om den berørte parten.", ValidationResultSeverityEnum.ERROR),
                //Beroertpart organisasjonsnummer
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beroerteParter{0}/organisasjonsnummer", "Organisasjonsnummeret til den berørte parten må fylles ut.", ValidationResultSeverityEnum.ERROR, "/beroerteParter{0}/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beroerteParter{0}/organisasjonsnummer", "Organisasjonsnummeret ('{0}') for den berørte parten er ikke gyldig.", ValidationResultSeverityEnum.ERROR, "/beroerteParter{0}/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/beroerteParter{0}/organisasjonsnummer", "Organisasjonsnummeret ('{0}') for den berørte parten har ikke gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR, "/beroerteParter{0}/partstype/kodeverdi"),
                //Beroertpart Fødselnummer
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beroerteParter{0}/foedselsnummer", "Fødselsnummer må angis når berørt part er privatperson.", ValidationResultSeverityEnum.ERROR, "/beroerteParter{0}/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/beroerteParter{0}/foedselsnummer", "Fødselsnummeret til berørt part må være gyldig", ValidationResultSeverityEnum.ERROR, "/beroerteParter{0}/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/beroerteParter{0}/foedselsnummer", "Berørt parts fødselsnummer må ha gyldig kontrollsiffer", ValidationResultSeverityEnum.ERROR, "/beroerteParter{0}/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.dekryptering, "/beroerteParter{0}/foedselsnummer", "Fødselsnummeret til den berørte parten kan ikke dekrypteres.", ValidationResultSeverityEnum.ERROR, "/beroerteParter{0}/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kryptert, "/beroerteParter{0}/foedselsnummer", "Fødselsnummeret til den berørte parten må være kryptert.", ValidationResultSeverityEnum.ERROR, "/beroerteParter{0}/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beroerteParter{0}/navn", "Navnet til den berørte parten må fylles ut.", ValidationResultSeverityEnum.ERROR, "/beroerteParter{0}/partstype/kodeverdi"),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/beroerteParter{0}/erHoeringsmyndighet", "Du må svare på om den berørte parten er en høringsmyndighet.", ValidationResultSeverityEnum.ERROR, "/beroerteParter{0}/partstype/kodeverdi")
            };


            validationMessageStorage.AddRange(BeroertpartPartType());
            validationMessageStorage.AddRange(BeroertpartAdresse());

            return validationMessageStorage;

        }

        private List<ValidationMessageStorageEntry> Forslagsstiller()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                //forslagsstiller
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/forslagsstiller","Du må fylle ut informasjon om forslagsstiller.", ValidationResultSeverityEnum.ERROR),
                //forslagsstillers partstype
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/forslagsstiller/partstype", "Du må oppgi partstypen for forslagsstiller. Du kan sjekke gyldige partstyper på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/forslagsstiller/partstype/kodeverdi", "Kodeverdien for 'partstype' til forslagsstiller må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/forslagsstiller/partstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for partstypen til forslagsstiller. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/forslagsstiller/partstype/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Partstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/forslagsstiller/partstype/kodebeskrivelse", "Når partstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR, "/forslagsstiller/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/forslagsstiller/partstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for partstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR, "/forslagsstiller/partstype/kodeverdi"),
                //telefonnummer
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/forslagsstiller/telefonnummer", "Telefonnummeret eller mobilnummeret til forslagsstiller bør fylles ut."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/forslagsstiller/telefonnummer", "Telefonnummeret til forslagsstiller må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),
                //epost - navn
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/forslagsstiller/epost", "E-postadressen til forslagsstiller bør fylles ut.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/forslagsstiller/epost", "E-postadresse '{0}' for forslagsstiller er ikke gyldig. Gyldig e-post skrives som navn@domene.no", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/forslagsstiller/navn", "Navnet til forslagsstiller må fylles ut.", ValidationResultSeverityEnum.ERROR),
                //foedselsnummer
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/forslagsstiller/foedselsnummer", "Fødselsnummer må fylles ut når forslagsstiller er en privatperson.", ValidationResultSeverityEnum.ERROR, "/forslagsstiller/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.dekryptering, "/forslagsstiller/foedselsnummer", "Fødselsnummeret til forslagsstiller kan ikke dekrypteres.", ValidationResultSeverityEnum.ERROR, "/forslagsstiller/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/forslagsstiller/foedselsnummer", "Fødselsnummeret til forslagsstiller er ikke gyldig.", ValidationResultSeverityEnum.ERROR, "/forslagsstiller/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/forslagsstiller/foedselsnummer", "Fødselsnummeret til forslagsstiller har ikke gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR, "/forslagsstiller/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kryptert, "/forslagsstiller/foedselsnummer", "Fødselsnummeret til forslagsstiller må være kryptert.", ValidationResultSeverityEnum.ERROR, "/forslagsstiller/partstype/kodeverdi"),
                //organisasjonsnummer
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/forslagsstiller/organisasjonsnummer", "Organisasjonsnummer må fylles ut når forslagsstiller er en organisasjon.", ValidationResultSeverityEnum.ERROR, "/forslagsstiller/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/forslagsstiller/organisasjonsnummer", "Organisasjonsnummeret ('{0}') for forslagsstiller er ikke gyldig.", ValidationResultSeverityEnum.ERROR, "/forslagsstiller/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/forslagsstiller/organisasjonsnummer", "Organisasjonsnummeret ('{0}') for forslagsstiller har ikke gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR, "/forslagsstiller/partstype/kodeverdi"),
                //forslagsstillers adresse                      
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/forslagsstiller/adresse", "Du bør fylle ut adressen til forslagsstiller."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/forslagsstiller/adresse/adresselinje1", "Adresselinje 1 bør fylles ut for forslagsstiller."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/forslagsstiller/adresse/postnr", "Du bør fylle ut postnummeret til forslagsstillers adresse"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/forslagsstiller/adresse/postnr", "Postnummeret '{0}' for forslagsstiller er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/forslagsstiller/adresse/poststed", "Postnummeret '{0}' for forslagsstiller stemmer ikke overens med poststedet '{1}' Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/", ValidationResultSeverityEnum.WARNING, "/forslagsstiller/adresse/postnr"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/forslagsstiller/adresse/landkode", "Landkoden '{0}' for forslagsstillers adresse, er ikke gyldig."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tillatt, "/forslagsstiller/adresse/landkode", "Landkoden til forslagsstillers adresse må være norsk."),
            };


            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> Plankonsulent()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                //plankonsulent
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plankonsulent", "Du må fylle ut informasjon om plankonsulent.", ValidationResultSeverityEnum.ERROR),
                //plankonsulents partstype
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plankonsulent/partstype", "Du må oppgi partstypen for plankonsulent. Du kan sjekke gyldige partstyper på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plankonsulent/partstype/kodeverdi", "Kodeverdien for 'partstype' til plankonsulent må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plankonsulent/partstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for partstypen til plankonsulent. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/plankonsulent/partstype/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Partstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tillatt, "/plankonsulent/partstype/kodeverdi", "Partstypen for plankonsulent må være et foretak eller en organisasjon.", ValidationResultSeverityEnum.ERROR),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plankonsulent/partstype/kodebeskrivelse", "Når partstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR, "/plankonsulent/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plankonsulent/partstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for partstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR, "/plankonsulent/partstype/kodeverdi"),
                //telefonnummer
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plankonsulent/telefonnummer", "Telefonnummeret eller mobilnummeret til plankonsulent bør fylles ut."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plankonsulent/telefonnummer", "Telefonnummeret til plankonsulent må kun inneholde tall og '+'.", ValidationResultSeverityEnum.ERROR),
                //epost - navn
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plankonsulent/epost", "E-postadressen til plankonsulent bør fylles ut."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plankonsulent/epost", "E-postadresse '{0}' for plankonsulent er ikke gyldig. Gyldig e-post skrives som navn@domene.no",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plankonsulent/navn", "Navnet til plankonsulenten må fylles ut.", ValidationResultSeverityEnum.ERROR),
                //organisasjonsnummer
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plankonsulent/organisasjonsnummer", "Organisasjonsnummeret til plankonsulenten må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plankonsulent/organisasjonsnummer", "Organisasjonsnummeret ('{0}') for plankonsulent er ikke gyldig.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/plankonsulent/organisasjonsnummer", "Organisasjonsnummeret til plankonsulent må ha gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR),
                //plankonsulents adresse
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plankonsulent/adresse", "Adresse bør fylles ut for plankonsulent"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plankonsulent/adresse/adresselinje1", "Adresselinje 1 bør fylles ut for plankonsulent."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/plankonsulent/adresse/postnr", "Du bør fylle ut postnummeret til plankonsulent."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plankonsulent/adresse/postnr", "Postnummeret '{0}' for plankonsulent er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plankonsulent/adresse/poststed", "Postnummeret '{0}' for plankonsulent stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/", ValidationResultSeverityEnum.WARNING, "/plankonsulent/adresse/postnr"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/plankonsulent/adresse/landkode", "Landkoden ‘{0}’ for plankonsulentens adresse, er ikke gyldig."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tillatt, "/plankonsulent/adresse/landkode", "Landkoden til plankonsulentens adresse må være norsk.")
            };


            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> Planforslag()
        {
            var validationMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/planforslag", "Du må fylle ut informasjon om planforslaget.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/planforslag/plannavn", "Du må fylle ut navnet på planforslaget.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/planforslag/arealplanId", "Arealplan-ID for planforslaget må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/planforslag/BegrunnelseKU", "Du må begrunne hvorfor planforslaget utløser eller ikke utløser en konsekvensutredning.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/planforslag/Planhensikt", "Du må fylle ut hensikten med planforslaget.", ValidationResultSeverityEnum.ERROR),
                //plantype
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/planforslag/plantype/kodeverdi", "En teknisk feil gjør at vi ikke kan validere informasjon for plantype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/plansak/plantype-plansak"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/planforslag/plantype", "Du må oppgi en plantype for planforslaget. Du kan sjekke gyldige plantyper på https://register.geonorge.no/plansak/plantype-plansak", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/planforslag/plantype/kodeverdi", "Kodeverdien for ‘plantype’ til planforslaget må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/plansak/plantype-plansak ", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/planforslag/plantype/kodeverdi", "'{0}' er en ugyldig kodeverdi for plantype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/plansak/plantype-plansak", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/planforslag/plantype/kodebeskrivelse", "Når plantype for planforslaget er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/plansak/plantype-plansak", ValidationResultSeverityEnum.ERROR, "/planforslag/plantype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/planforslag/plantype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for plantype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/plansak/plantype-plansak", ValidationResultSeverityEnum.ERROR, "/planforslag/plantype/kodeverdi"),
                //planforslag/KommunensSaksnummer
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/planforslag/kommunensSaksnummer", "Du bør oppgi kommunens saksnummer med saksår.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/planforslag/kommunensSaksnummer/saksaar", "Saksår ({0}) for kommunens saksnummer er ikke gyldig. Saksåret må inneholde fire siffer.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/planforslag/kommunensSaksnummer/sakssekvensnummer", "Du bør oppgi kommunens saksnummer med sekvensnummer.", ValidationResultSeverityEnum.WARNING, "/planforslag/KommunensSaksnummer/saksaar"),
                //Datarelationer
                GetValidationMessageStorageEntry(null,null,ValidationRuleEnum.Stemmer,"/planforslag/kravKonsekvensUtredning","Dersom det er krav om konsekvensutredning kan ikke planforslaget behandles som forenklet endring.",ValidationResultSeverityEnum.ERROR,"/planforslag/plantype/kodeverdi"),
                //fristForInnspill
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt,"/planforslag/fristForInnspill", "Du må fylle ut fristen berørte parter har til å komme med innspill til planforslaget.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.senere,"/planforslag/fristForInnspill", "Frist for innspill må være satt til etter dagens dato.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tidligere,"/planforslag/fristForInnspill", "Fristen for innspill kan ikke være lenger enn seks måneder frem i tid.", ValidationResultSeverityEnum.ERROR),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt,"/planforslag/hjemmesidePlanforslag", "Du bør legge med en lenke til nettsiden der planforslaget er publisert."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt,"/planforslag/hjemmesidePlanprogram", "Du bør legge med en lenke til nettsiden der planprogrammet er publisert."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt,"/planforslag/saksgangOgMedvirkning", "Du bør fylle ut informasjon om hvordan berørte parter kan medvirke til planforslaget."),

            };

            return validationMessageStorage;
        }

        private List<ValidationMessageStorageEntry> ModelMessageStorageEntries()
        {
            var modellMessageStorageEntries = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/kommunenavn","Du må fylle ut navnet på kommunen du vil regulere i.", ValidationResultSeverityEnum.ERROR)
            };
            return modellMessageStorageEntries;

        }

    }
}