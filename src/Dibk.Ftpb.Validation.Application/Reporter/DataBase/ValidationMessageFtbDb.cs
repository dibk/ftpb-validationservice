using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using System.Collections.Generic;
using static Dibk.Ftpb.Validation.Application.Reporter.DataBase.DbHelpers;

namespace Dibk.Ftpb.Validation.Application.Reporter.DataBase
{
    public class ValidationMessageFtbDb
    {
        private List<ValidationMessageStorageEntry> _validationMessageStorageEntry;

        public ValidationMessageFtbDb()
        {
            _validationMessageStorageEntry = new List<ValidationMessageStorageEntry>();
        }

        public List<ValidationMessageStorageEntry> InitiateMessageRepository()
        {
            _validationMessageStorageEntry.AddRange(SoeknadGjelder());
            _validationMessageStorageEntry.AddRange(UtfallBesvarelseValidator());
            _validationMessageStorageEntry.AddRange(DelSoeknader());
            _validationMessageStorageEntry.AddRange(AnsvarForByggesaken());
            _validationMessageStorageEntry.AddRange(Vedlegg());
            _validationMessageStorageEntry.AddRange(Avsender());
            _validationMessageStorageEntry.AddRange(DatoFerdigAttest());
            _validationMessageStorageEntry.AddRange(GjenstaaendeInnenfor());
            _validationMessageStorageEntry.AddRange(Sikkerhetsnivaa());
            _validationMessageStorageEntry.AddRange(Versjon());
            _validationMessageStorageEntry.AddRange(AnsvarligSoekerTiltakstype());
            _validationMessageStorageEntry.AddRange(Gjennomfoeringsplan());
            _validationMessageStorageEntry.AddRange(KravFerdigattest());
            _validationMessageStorageEntry.AddRange(IkkeSoeknadspliktigeEndringer());
            _validationMessageStorageEntry.AddRange(Varmesystem());
            _validationMessageStorageEntry.AddRange(ErTilstrekkeligDokumentasjonOverlevertEier());

            return _validationMessageStorageEntry;
        }

        private List<ValidationMessageStorageEntry> Avsender()
        {
            var submitterMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/avsender/tiltakshaver", "Identiteten (organisasjonsnummeret eller fødselsnummeret) til avsender må være lik identiteten som er oppgitt for tiltakshaver.", ValidationResultSeverityEnum.ERROR, "/tiltakshaver/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/avsender/ansvarligSoeker", "Identiteten (organisasjonsnummeret eller fødselsnummeret) til avsender må være lik identiteten som er oppgitt for ansvarlig søker.", ValidationResultSeverityEnum.ERROR, "/ansvarligSoeker/partstype/kodeverdi"),
            };

            return submitterMessageStorage;
        }

        private List<ValidationMessageStorageEntry> SoeknadGjelder()
        {
            var soeknadGjelderMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/soeknadGjelder", "Du må beskrive hva denne søknaden gjelder.",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/soeknadGjelder/gjelderHeleTiltaket", "Du må svare på om denne søknaden gjelder for hele eller deler av tiltaket som byggetillatelsen gjelder.",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/soeknadGjelder/delAvTiltaket", "Du har svart at denne søknaden gjelder for en del av tiltaket. Da må du beskrive hvilken del av tiltaket denne søknaden gjelder for.",ValidationResultSeverityEnum.ERROR,"/soeknadGjelder/gjelderHeleTiltaket = 'false'"),
                //Type
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/soeknadGjelder/type", "Du må oppgi tiltakstypen eller tiltakstypene som gjelder for byggetillatelsen.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/soeknadGjelder/type", "Tiltakstypen(e) for byggetillatelsen ble ikke validert. For å validere tiltakstypen(e), må du først fylle ut en gyldig verdi for 'ansvarForByggesaken'.", ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi ugyldig"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/soeknadGjelder/type/kode{0}/kodeverdi", "Du må oppgi en tiltakstype for byggetillatelsen. Du kan sjekke riktig kodeverdi for tiltak med ansvarsrett på https://register.geonorge.no/byggesoknad/tiltaktype",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/soeknadGjelder/type/kode{0}/kodeverdi", "Du må oppgi en tiltakstype. Du kan sjekke riktig kodeverdi for tiltak med ansvarsrett på https://register.geonorge.no/byggesoknad/tiltaktype",ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/soeknadGjelder/type/kode{0}/kodeverdi", "Du må oppgi en tiltakstype. Du kan sjekke riktig kodeverdi for tiltak uten ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltakstyperutenansvarsrett",ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll'"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/soeknadGjelder/type/kode{0}/kodeverdi", "'{0}' er en ugyldig kodeverdi for tiltakstype. Du kan sjekke riktig kodeverdi for tiltak med ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltaktype",ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/soeknadGjelder/type/kode{0}/kodeverdi", "'{0}' er en ugyldig kodeverdi for tiltakstype. Du kan sjekke riktig kodeverdi for tiltak uten ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltakstyperutenansvarsrett",ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll'"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/soeknadGjelder/type/kode{0}/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Tiltakstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det", ValidationResultSeverityEnum.ERROR, "/soeknadGjelder/type"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/soeknadGjelder/type/kode{0}/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Tiltakstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det", ValidationResultSeverityEnum.ERROR, "/soeknadGjelder/type & (/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll')"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/soeknadGjelder/type/kode{0}/kodebeskrivelse", "Når tiltakstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse for tiltak med ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltaktype",ValidationResultSeverityEnum.ERROR, "/soeknadGjelder/type/kode{0}/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/soeknadGjelder/type/kode{0}/kodebeskrivelse", "Når tiltakstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse for tiltak uten ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltakstyperutenansvarsrett",ValidationResultSeverityEnum.ERROR, "/soeknadGjelder/type/kode{0}/kodeverdi & (/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll')"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/soeknadGjelder/type/kode{0}/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tiltakstypen. Du kan sjekke riktig kodebeskrivelse for tiltak med ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltaktype",ValidationResultSeverityEnum.WARNING, "/soeknadGjelder/type/kode{0}/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/soeknadGjelder/type/kode{0}/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tiltakstypen. Du kan sjekke riktig kodebeskrivelse for tiltak uten ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltakstyperutenansvarsrett",ValidationResultSeverityEnum.WARNING, "/soeknadGjelder/type/kode{0}/kodeverdi & (/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll')"),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/soeknadGjelder/delsoeknadsnummer", "Du må oppgi hvilket nummer denne søknaden om {0} er.",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/soeknadGjelder/delsoeknadsnummer", "Nummeret til søknaden om {0} må være et tall.",ValidationResultSeverityEnum.ERROR),
            };
            return soeknadGjelderMessageStorage;
        }

        private List<ValidationMessageStorageEntry> UtfallBesvarelseValidator()
        {
            var utfallBesvarelseMessageStorage = new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry("10003", "4", ValidationRuleEnum.utfylt, "/utfallBesvarelse", "Dersom det er stilt vilkår eller oppfølgingspunkt for igangsetting i byggetillatelsen, må du opplyse om status for disse."),
                GetValidationMessageStorageEntry("10004", "4", ValidationRuleEnum.utfylt, "/utfallBesvarelse", "Dersom det er stilt vilkår eller oppfølgingspunkt for midlertidig brukstillatelse i byggetillatelsen, må du opplyse om status for disse."),
                GetValidationMessageStorageEntry("10005", "4", ValidationRuleEnum.utfylt, "/utfallBesvarelse", "Dersom det er stilt vilkår eller oppfølgingspunkt for ferdigattest i byggetillatelsen, må du opplyse om status for disse."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}", "Du må oppgi informasjon om vilkåret/oppfølgingspunktet.", ValidationResultSeverityEnum.ERROR),


                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/utfallType", "Du må oppgi hvilken type utfall du besvarer. Hvilken utfallstype som er riktig bestemmes av det aktuelle sjekkpunktet. Du kan sjekke tilgjengelige utfallstyper på https://register.geonorge.no/kodelister/ebyggesak/utfall", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi", "Kodeverdien for 'utfall' må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/ebyggesak/utfall",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi", "‘{0}' er en ugyldig kodeverdi for utfallstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/ebyggesak/utfall.",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("10003", "4", ValidationRuleEnum.tillatt, "/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi", "Utfallstypen '{0}' for utfallSvar er ikke tillatt. Godkjente utfallstyper for søknader om igangsettingstillatelse er begrenset til ‘SVV’ og 'MKV’.",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("10004", "4", ValidationRuleEnum.tillatt, "/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi", "Utfallstypen '{0}' for utfallSvar er ikke tillatt. Godkjente utfallstyper for søknader om midlertidig brukstillatelse er begrenset til ‘SVV’ og 'MKV’.",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry("10005", "4", ValidationRuleEnum.tillatt, "/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi", "Utfallstypen '{0}' for utfallSvar er ikke tillatt. Godkjente utfallstyper for søknad om ferdigattest er begrenset til ‘SVV’ og 'MKV’.",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Utfallstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/utfallType/kodebeskrivelse", "Når utfallstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/ebyggesak/utfall",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/utfallBesvarelse/utfallSvar{0}/utfallType/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for utfallet. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/ebyggesak/utfall",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi"),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/utfallBesvarelse/utfallSvar{0}/utloestFraSjekkpunkt/sjekkpunktId", "SjekkpunktID-en '{0}' er på feil format. Sjekkpunktet skal bestå av siffer og punktum. Du kan finne riktig sjekkpunkt på https://ftb-checklist.dibk.no/",ValidationResultSeverityEnum.ERROR),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/utfallBesvarelse/utfallSvar{0}/tema/kodeverdi", "'{0}' er en ugyldig kodeverdi for tema. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/ebyggesak/tema",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/utfallBesvarelse/utfallSvar{0}/tema/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Temaet kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/tema/kodebeskrivelse", "Når tema er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/ebyggesak/tema",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/tema/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/utfallBesvarelse/utfallSvar{0}/tema/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tema. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/ebyggesak/tema",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/tema/kodeverdi"),
                GetValidationMessageStorageEntry("10004", "4", ValidationRuleEnum.gyldig, "/utfallBesvarelse/utfallSvar{0}/tema/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tema. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/ebyggesak/tema",ValidationResultSeverityEnum.WARNING,"/utfallBesvarelse/utfallSvar{0}/tema/kodeverdi"),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/tittel", "Du har valgt å besvare '{0}' fra kommunen. Da må du lage en tittel som beskriver det du svarer på.",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere", "Du må svare på om '{0}' skal besvares senere.",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/utfallType/kodebeskrivelse"),
                GetValidationMessageStorageEntry("10005", "4", ValidationRuleEnum.gyldig, "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere", "'{0}' kan ikke besvares senere for ferdigattest.",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere != false"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvart", "Du har oppgitt at '{0}' ikke skal besvares senere. Da må du svare på om '{0}' allerede er besvart.",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere = false & /soeknadGjelder/delsoeknadsnummer > 1"),
                GetValidationMessageStorageEntry("10005", "4", ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvart", "Du må svare på om '{0}' allerede er besvart.",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere = false"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/kommentar", "Du har bedt om å få besvare '{0}' senere. Da må du skrive en kommentar til kommunen.",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere"),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/kommentar || vedlegg", "Du har valgt å besvare '{0}' fra kommunen. Da må du enten skrive en kommentar eller fylle ut informasjon om hvilke vedlegg du skal sende inn.",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere = false & /utfallBesvarelse/utfallSvar{0}/erUtfallBesvart = false"),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/filnavn", "Du har valgt å legge ved '{0}'. Da må du også fylle ut et filnavn.",ValidationResultSeverityEnum.WARNING,"/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype"),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype", "Du har lagt ved vedlegget '{0}'. Da må du også velge vedleggstype.",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/filnavn"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi", "Du må oppgi en kodeverdi for vedleggstype. Du kan sjekke gyldige vedlegg på https://register.geonorge.no/kodelister/byggesoknad/vedlegg-og-underskjema/vedlegg",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/filnavn"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for vedleggstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodebeskrivelse", "Når vedleggstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for vedleggstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg",ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi"),
                GetValidationMessageStorageEntry("10004", "4", ValidationRuleEnum.gyldig, "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for vedleggstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg",ValidationResultSeverityEnum.WARNING,"/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Vedleggstype kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det."),

                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/versjonsdato", "Du har lagt ved vedlegget '{0}'. Da bør du også fylle ut vedleggets dato.",ValidationResultSeverityEnum.WARNING,"/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/versjonsnummer", "Du har lagt ved vedlegget '{0}'. Dersom dette er en ny versjon av et tidligere innsendt dokument, bør du fylle ut tegnings-/versjonsnummer.",ValidationResultSeverityEnum.WARNING,"/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi"),
            };
            return utfallBesvarelseMessageStorage;
        }

        private List<ValidationMessageStorageEntry> DelSoeknader()
        {
            var delsoeknaderMessageStorageEntry = new List<ValidationMessageStorageEntry>()
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/delsoeknader", "Du bør oppgi informasjon om de tidligere innsendte søknadene om {0}.", ValidationResultSeverityEnum.WARNING,"/soeknadGjelder/delsoeknadsnummer > 1"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/delsoeknader/delsoeknad{0}", "Du bør oppgi informasjon om den tidligere innsendte delsøknaden.", ValidationResultSeverityEnum.WARNING,"/soeknadGjelder/delsoeknadsnummer > 1"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/delsoeknader/delsoeknad{0}/delAvTiltaket", "Du må beskrive hvilke deler av tiltaket som gjaldt for den tidligere innsendte delsøknaden.", ValidationResultSeverityEnum.ERROR,"/soeknadGjelder/delsoeknadsnummer > 1"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/delsoeknader/delsoeknad{0}/tillatelsedato", "Dersom det er gitt tillatelse for den tidligere søknaden, bør du oppgi datoen den ble gitt.", ValidationResultSeverityEnum.WARNING,"/soeknadGjelder/delsoeknadsnummer > 1"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/delsoeknader/delsoeknad{0}/delsoeknadsnummer", "Du må oppgi hvilket nummer den tidligere innsendte delsøknaden er.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.numerisk, "/delsoeknader/delsoeknad{0}/delsoeknadsnummer", "Nummeret til den tidligere innsendte delsøknaden må være et tall.", ValidationResultSeverityEnum.ERROR),
                //Type
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/delsoeknader/delsoeknad{0}/type/kode{0}/kodeverdi", "'{0}' er en ugyldig kodeverdi for tiltakstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/tiltaktype",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/delsoeknader/delsoeknad{0}/type/kode{0}/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Tiltakstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det."),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/delsoeknader/delsoeknad{0}/type/kode{0}/kodebeskrivelse", "Når tiltakstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaktype",ValidationResultSeverityEnum.ERROR,"/delsoeknader/delsoeknad{0}/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/delsoeknader/delsoeknad{0}/type/kode{0}/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tiltakstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaktype",ValidationResultSeverityEnum.ERROR,"/delsoeknader/delsoeknad{0}/kodeverdi"),
            };
            return delsoeknaderMessageStorageEntry;
        }

        private List<ValidationMessageStorageEntry> AnsvarForByggesaken()
        {
            var ansvarForByggesakenMessageStorage = new List<ValidationMessageStorageEntry>()
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarForByggesaken", "Du må velge hvilket ansvarsforhold som gjelder for byggesaken. Du kan sjekke riktig ansvarsforhold på https://register.geonorge.no/kodelister/byggesoknad/ansvar-for-byggesaken",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarForByggesaken/kodeverdi", "Du må oppgi en kodeverdi for ansvarsforhold. Du kan sjekke riktig ansvarsforhold på https://register.geonorge.no/kodelister/byggesoknad/ansvar-for-byggesaken",ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarForByggesaken/kodeverdi", "'{0}' er en ugyldig kodeverdi for ansvar for byggesaken. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/ansvar-for-byggesaken", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tillatt, "/ansvarForByggesaken/kodeverdi", "Ansvar for byggesaken type '{0}' er ikke tillatt. Godkjente ansvarsforhold for igangsettingstillatelse er 'medAnsvar'og 'selvbygger'", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/ansvarForByggesaken/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Ansvar for byggesaken kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarForByggesaken/kodebeskrivelse", "Når ansvar for byggesaken er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/ansvar-for-byggesaken", ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarForByggesaken/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for ansvar for byggesaken. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/ansvar-for-byggesaken", ValidationResultSeverityEnum.WARNING, "/ansvarForByggesaken/kodeverdi"),
            };
            return ansvarForByggesakenMessageStorage;
        }


        private List<ValidationMessageStorageEntry> Vedlegg()
        {
            var vedleggMessageStorage = new List<ValidationMessageStorageEntry>()
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg", "Du har valgt å besvare '{0}' med vedlegg. Da må du legge ved minst én fil.", ValidationResultSeverityEnum.ERROR,"/utfallBesvarelse/utfallSvar{0}/vedlegg"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/Gjennomføringsplan", "‘Gjennomføringsplan’ skal følge med søknaden.", ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi = 'medAnsvar'",null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "17.11" }}),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/Gjennomføringsplan", "‘Gjennomføringsplan’ skal følge med søknaden.", ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi = 'medAnsvar' || 'utenAnsvarMedKontroll'",null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "17.11" }}),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/ErklaeringSelvbygger", "Du har valgt '{0}'. Da må du legge ved ‘Egenerklæring for selvbygger’ (blankett 5187).", ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi = 'selvbygger' && /soeknadGjelder/gjelderHeleTiltaket = 'true'", checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "17.40" }}),
                GetValidationMessageStorageEntry("10005", "4", ValidationRuleEnum.vedlegg, "/vedlegg/ErklaeringSelvbygger", "Du har valgt '{0}'. Da må du legge ved ‘Egenerklæring for selvbygger’ (blankett 5187).", ValidationResultSeverityEnum.ERROR, "/ansvarForByggesaken/kodeverdi = 'selvbygger'", checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "17.25" }}),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.vedlegg, "/vedlegg/SituasjonsplanOgTegning", "Du har svart at det er foretatt ikke søknadspliktige endringer i forhold til byggetillatelsen. Da må du legge ved oppdatert situasjonsplan og tegninger (“som bygget”-tegninger) og eventuell dokumentasjon om tiltakets plassering slik den er utført.", ValidationResultSeverityEnum.ERROR, "/foretattIkkeSoeknadspliktigeJusteringer = 'true'", checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "18.16" }}),
            };
            return vedleggMessageStorage;
        }

        private List<ValidationMessageStorageEntry> Versjon()
        {
            return new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry("6146", "44096", ValidationRuleEnum.utfylt, "/versjon", "Du må fylle ut versjonsnummeret på gjennomføringsplanen.", ValidationResultSeverityEnum.ERROR, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "17.13" }})
            };
        }
        private List<ValidationMessageStorageEntry> AnsvarligSoekerTiltakstype()
        {
            return new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoekerTiltaksklasse", "Du må velge en tiltaksklasse for søker.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoekerTiltaksklasse/kodeverdi", "Kodeverdien for 'ansvarligSoekerTiltaksklasse' til søker må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/tiltaksklasse", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoekerTiltaksklasse/kodeverdi", "'{0}' er en ugyldig kodeverdi for søkers tiltaksklasse. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/tiltaksklasse", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/ansvarligSoekerTiltaksklasse/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Søkers tiltaksklasse kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/ansvarligSoekerTiltaksklasse/kodebeskrivelse", "Når søkers tiltaksklasse er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaksklasse", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/ansvarligSoekerTiltaksklasse/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for søkers tiltaksklasse. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaksklasse", ValidationResultSeverityEnum.WARNING)
            };
        }

        private List<ValidationMessageStorageEntry> Gjennomfoeringsplan()
        {
            return new List<ValidationMessageStorageEntry>
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan", "Du må fylle ut informasjon om gjennomføringsplanen.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}", "Minst ett ansvarsområde må fylles ut.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/funksjon", "Du må oppgi en funksjon for ansvarsområdet. Du kan sjekke gyldige funksjoner på https://register.geonorge.no/byggesoknad/funksjon", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/funksjon/kodeverdi", "Du må oppgi en kodeverdi for funksjon. Du kan sjekke riktig ansvarsforhold på https://register.geonorge.no/byggesoknad/funksjon", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/gjennomfoeringsplan/ansvarsomraade{0}/funksjon/kodeverdi", "'{0}' er en ugyldig kodeverdi for funksjon for ansvarsområdet. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/funksjon", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/gjennomfoeringsplan/ansvarsomraade{0}/funksjon/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Funksjonen for ansvarsområdet kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/funksjon/kodebeskrivelse", "Når funksjon er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/funksjon", ValidationResultSeverityEnum.ERROR, "/gjennomfoeringsplan/ansvarsomraade{0}/funksjon/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/gjennomfoeringsplan/ansvarsomraade{0}/funksjon/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for funksjon. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/funksjon", ValidationResultSeverityEnum.WARNING, "/gjennomfoeringsplan/ansvarsomraade{0}/funksjon/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/ansvarsomraade", "Du må beskrive ansvarsområdet. Beskrivelsen må være identisk med det som står i erklæringen om ansvarsrett.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/tiltaksklasse", "Du må velge en tiltaksklasse for ansvarsområdet '{0}'. Tiltaksklassen må være identisk med det som står i erklæringen om ansvarsrett.", ValidationResultSeverityEnum.ERROR, "/gjennomfoeringsplan/ansvarsomraade{0}/ansvarsomraade"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/tiltaksklasse/kodeverdi", "Du må oppgi en kodeverdi for tiltaksklasse. Du kan sjekke riktig tiltaksklasse på https://register.geonorge.no/byggesoknad/tiltaksklasse", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/gjennomfoeringsplan/ansvarsomraade{0}/tiltaksklasse/kodeverdi", "'{0}' er en ugyldig kodeverdi for tiltaksklasse. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/tiltaksklasse", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/gjennomfoeringsplan/ansvarsomraade{0}/tiltaksklasse/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Tiltaksklassen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/tiltaksklasse/kodebeskrivelse", "Når tiltaksklasse er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaksklasse", ValidationResultSeverityEnum.ERROR, "/gjennomfoeringsplan/ansvarsomraade{0}/tiltaksklasse/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/gjennomfoeringsplan/ansvarsomraade{0}/tiltaksklasse/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tiltaksklasse. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/tiltaksklasse", ValidationResultSeverityEnum.WARNING, "/gjennomfoeringsplan/ansvarsomraade{0}/tiltaksklasse/kodeverdi"),
                GetValidationMessageStorageEntry("6146", "44096", ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade/foretak", "Du må fylle ut minst ett ansvarlig foretak i gjennomføringsplanen.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/navn", "Du må fylle ut navnet på det ansvarlige foretaket.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/partstype", "Når du har valgt den ansvarlige for ansvarsområdet '{0}', må du også fylle ut partstype. Du kan sjekke gyldige partstyper på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/partstype/kodeverdi", "Kodeverdien for 'partstype' til den ansvarlige for ansvarsområdet '{0}' må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.tillatt, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/partstype/kodeverdi", "Partstypen til den ansvarlige for ansvarsområdet er ikke tillatt. Godkjente partstyper er begrenset til ‘Foretak’ og 'Privatperson’.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/partstype/kodeverdi", "'{0}' er en ugyldig kodeverdi for partstypen til den ansvarlige for ansvarsområdet. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/partstype/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Partstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/partstype/kodebeskrivelse", "Når partstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.ERROR, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/partstype/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for partstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", ValidationResultSeverityEnum.WARNING, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/organisasjonsnummer", "Når et ansvarlig foretak '{0}' er valgt for ansvarsområdet, må du også fylle ut organisasjonsnummeret til foretaket.", ValidationResultSeverityEnum.ERROR, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/organisasjonsnummer", "Organisasjonsnummeret ('{0}') til foretak er ikke gyldig.", ValidationResultSeverityEnum.ERROR, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.kontrollsiffer, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/organisasjonsnummer", "Organisasjonsnummeret ('{0}') til foretak har ikke gyldig kontrollsiffer.", ValidationResultSeverityEnum.ERROR, "/gjennomfoeringsplan/ansvarsomraade{0}/foretak/partstype/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/erklaeringAnsvar", "Du må velge når samsvars-/kontrollerklæring er planlagt eller signert for ansvarsområdet '{0}'. Planlagt tidspunkt skal være identisk med det som står i erklæringen om ansvarsrett. Hvis samsvars- eller kontrollerklæring er avgitt, skal du fylle inn datoen for når den siste erklæringen er signert. Det skal være minst én registrering for hvert ansvarsområde.", ValidationResultSeverityEnum.ERROR, "/gjennomfoeringsplan/ansvarsomraade{0}/ansvarsomraade"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/ansvarsomraadeStatus", "Du må oppgi en status for ansvarsområdet '{0}'. Du kan sjekke gyldige statuser på https://register.geonorge.no/kodelister/byggesoknad/status-for-ansvarsomradet", ValidationResultSeverityEnum.ERROR, "/gjennomfoeringsplan/ansvarsomraade{0}/ansvarsomraade"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/ansvarsomraadeStatus/kodeverdi", "Du må oppgi en kodeverdi for status for ansvarsområdet. Du kan sjekke gyldige statuser på https://register.geonorge.no/kodelister/byggesoknad/status-for-ansvarsomradet", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/gjennomfoeringsplan/ansvarsomraade{0}/ansvarsomraadeStatus/kodeverdi", "'{0}' er en ugyldig kodeverdi for status for ansvarsområdet. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/status-for-ansvarsomradet", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/gjennomfoeringsplan/ansvarsomraade{0}/ansvarsomraadeStatus/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Status for ansvarsområdet kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjennomfoeringsplan/ansvarsomraade{0}/ansvarsomraadeStatus/kodebeskrivelse", "Når status for ansvarsområdet er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/status-for-ansvarsomradet", ValidationResultSeverityEnum.ERROR, "/gjennomfoeringsplan/ansvarsomraade{0}/ansvarsomraadeStatus/kodeverdi"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/gjennomfoeringsplan/ansvarsomraade{0}/ansvarsomraadeStatus/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for status for ansvarsområdet. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/status-for-ansvarsomradet", ValidationResultSeverityEnum.WARNING, "/gjennomfoeringsplan/ansvarsomraade{0}/ansvarsomraadeStatus/kodeverdi"),
            };
        }
        private List<ValidationMessageStorageEntry> DatoFerdigAttest()
        {
            return new List<ValidationMessageStorageEntry>()
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/datoForFerdigattest", "Du må oppgi datoen for når du planlegger å søke om ferdigattest.", ValidationResultSeverityEnum.ERROR, checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "18.23" }})
            };
        }
        private List<ValidationMessageStorageEntry> GjenstaaendeInnenfor()
        {
            return new List<ValidationMessageStorageEntry>()
            {
            GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjenstaaendeArbeider", "Du må oppgi hvilke arbeider som gjenstår.", ValidationResultSeverityEnum.ERROR, checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "18.24" }}),
            GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjenstaaendeArbeider/gjenstaaendeInnenfor", "Du må oppgi hvilke arbeider som gjenstår innenfor den delen av tiltaket det søkes midlertidig brukstillatelse for. Arbeidene skal være av mindre vesentlig betydning.", ValidationResultSeverityEnum.ERROR, checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "18.24" }}),
            GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/gjenstaaendeArbeider/gjenstaaendeUtenfor", "Du må oppgi de resterende delene av tiltaket, som ikke er inkludert i denne søknaden om midlertidig brukstillatelse.", ValidationResultSeverityEnum.ERROR, "/soeknadGjelder/gjelderHeleTiltaket = false")
            };
        }
        private List<ValidationMessageStorageEntry> Sikkerhetsnivaa()
        {
            return new List<ValidationMessageStorageEntry>()
            {
            GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sikkerhetsnivaa", "Du må fylle ut informasjon om sikkerhetsnivået for tiltaket.", ValidationResultSeverityEnum.ERROR, checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "18.19" }}),
            GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sikkerhetsnivaa/harTilstrekkeligSikkerhet", "Du må svare på om tiltaket har tilstrekkelig sikkerhetsnivå til å tas i bruk i dag.", ValidationResultSeverityEnum.ERROR, checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "18.19" }}),
            GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sikkerhetsnivaa/utfoertInnen", "Du har svart at tiltaket ikke har tilstrekkelig sikkerhetsnivå til å tas i bruk. Da må du oppgi datoen for når arbeidet for å tilfredsstille sikkerhetsnivået skal være ferdigstilt.", ValidationResultSeverityEnum.ERROR, "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false", checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "18.21" }}),
            GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sikkerhetsnivaa/utfoertInnen", "Datoen for når arbeidet for å tilfredsstille sikkerhetsnivået skal være ferdigstilt, må være i løpet av de neste 14 dagene.", ValidationResultSeverityEnum.ERROR, "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false", checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "18.21" }}),
            GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sikkerhetsnivaa/typeArbeider", "Du må beskrive hva som gjenstår for at tiltaket skal ha tilstrekkelig sikkerhetsnivå til å kunne tas i bruk.", ValidationResultSeverityEnum.ERROR, "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false", checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "18.20" }}),
            GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/sikkerhetsnivaa/bekreftelseInnen", "Du må oppgi en dato for når bekreftelsen på at tiltaket har tilfredsstillende sikkerhetsnivå, skal være sendt til kommunen.", ValidationResultSeverityEnum.ERROR, "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false", checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "18.21" }}),
            GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/sikkerhetsnivaa/bekreftelseInnen", "Bekreftelsen på at tiltaket har tilfredsstillende sikkerhetsnivå må være sendt til kommunen i løpet av de neste 14 dagene.", ValidationResultSeverityEnum.ERROR, "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false", checklistReferences: new Dictionary<ServiceDomain, string>{{ ServiceDomain.FTB, "18.21" }}),
            };
        }
        private List<ValidationMessageStorageEntry> KravFerdigattest()
        {
            return new List<ValidationMessageStorageEntry>()
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/kravFerdigattest", "Du må fylle ut informasjon om kravene til ferdigattest.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest", "Du må bekrefte om tiltaket tilfredsstiller kravene til ferdigattest.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/kravFerdigattest/typeArbeider", "Du må oppgi hvilke arbeider som gjenstår for at tiltaket skal tilfredsstille kravene til ferdigattest.", ValidationResultSeverityEnum.ERROR, "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/kravFerdigattest/utfoertInnen", "Du må oppgi datoen for når arbeid av mindre vesentlig betydning skal være ferdigstilt.", ValidationResultSeverityEnum.ERROR, "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/kravFerdigattest/utfoertInnen", "Datoen for når arbeidet skal være ferdigstilt, må være i løpet av de neste 14 dagene.", ValidationResultSeverityEnum.ERROR, "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/kravFerdigattest/bekreftelseInnen", "Du må oppgi en dato for nå en bekreftelse på at gjenstående arbeider er ferdigstilt, skal være sendt til kommunen.", ValidationResultSeverityEnum.ERROR, "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false"),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/kravFerdigattest/bekreftelseInnen", "Bekreftelsen på at gjenstående arbeider er ferdigstilt må være sendt til kommunen i løpet av de neste 14 dagene.", ValidationResultSeverityEnum.ERROR, "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false"),
            };
        }

        private List<ValidationMessageStorageEntry> IkkeSoeknadspliktigeEndringer()
        {
            return new List<ValidationMessageStorageEntry>()
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/erForetattIkkeSoeknadspliktigeJusteringer", "Du må svare på om det er foretatt ikke søknadspliktige endringer i forhold til byggetillatelsen.", ValidationResultSeverityEnum.ERROR),
            };
        }

        private List<ValidationMessageStorageEntry> Varmesystem()
        {
            return new List<ValidationMessageStorageEntry>()
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/varmesystem", "Du må oppgi hvilket varmesystem bygningen har.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/varmesystem/energiforsyning{0}", "Du må oppgi hvilken energiforsyning bygningen har.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/varmesystem/energiforsyning{0}/kodeverdi", "Kodeverdien for 'energiforsyning' må fylles ut. Du kan sjekke gyldige energiforsyninger er på https://register.geonorge.no/kodelister/byggesoknad/energiforsyningtype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/varmesystem/energiforsyning{0}/kodeverdi", "'{0}' er en ugyldig kodeverdi for energiforsyning. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/energiforsyningtype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/varmesystem/energiforsyning{0}/kodebeskrivelse", "Når energiforsyning er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/energiforsyningtype", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/varmesystem/energiforsyning{0}/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for energiforsyning. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/energiforsyningtype", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/varmesystem/energiforsyning{0}/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Energiforsyningen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/varmesystem/varmefordeling{0}", "Du må oppgi hvilken varmefordeling bygningen har.", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/varmesystem/varmefordeling{0}/kodeverdi", "Kodeverdien for 'varmefordeling' må fylles ut. Du kan sjekke gyldige varmefordelinger er på https://register.geonorge.no/kodelister/byggesoknad/varmefordeling", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/varmesystem/varmefordeling{0}/kodeverdi", "'{0}' er en ugyldig kodeverdi for varmefordeling. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/varmefordeling", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/varmesystem/varmefordeling{0}/kodebeskrivelse", "Når varmefordeling er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/varmefordeling", ValidationResultSeverityEnum.ERROR),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.gyldig, "/varmesystem/varmefordeling{0}/kodebeskrivelse", "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for varmefordeling. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/varmefordeling", ValidationResultSeverityEnum.WARNING),
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.validert, "/varmesystem/varmefordeling{0}/kodeverdi", "Kodeverdien '{0}' ble ikke validert. Varmefordelingen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", ValidationResultSeverityEnum.WARNING),

            };
        }
        private List<ValidationMessageStorageEntry> ErTilstrekkeligDokumentasjonOverlevertEier()
        {
            return new List<ValidationMessageStorageEntry>()
            {
                GetValidationMessageStorageEntry(null, null, ValidationRuleEnum.utfylt, "/erTilstrekkeligDokumentasjonOverlevertEier", "Du må bekrefte at tilstrekkelig dokumentasjon som grunnlag for driftsfasen er overlevert byggets eier.", ValidationResultSeverityEnum.ERROR, null, null, new Dictionary<ServiceDomain, string> { { ServiceDomain.FTB, "18.15" }}),
            };
        }
    }
}
