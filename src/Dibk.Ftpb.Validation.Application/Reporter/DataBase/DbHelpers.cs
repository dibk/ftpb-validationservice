﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Reporter.DataBase
{
    public class DbHelpers
    {
        public static ValidationMessageStorageEntry GetValidationMessageStorageEntry(string dataFormatId, string dataFormatVersion, ValidationRuleEnum rule, string xPath, string message, ValidationResultSeverityEnum validationResultSeverity = ValidationResultSeverityEnum.WARNING, string precondition = null, string languageCode = null, Dictionary<ServiceDomain, string> checklistReferences = null, DateTime? validFrom = null, DateTime? validTo = null)
        {
            var validationMessageStorageEntry = new ValidationMessageStorageEntry
            {
                Rule = rule.ToString(),
                XPath = xPath,
                Precondition = precondition,
                Message = message,
                Messagetype = validationResultSeverity,
                ValidFrom = validFrom,
                ValidTo = validTo,
                ChecklistReferences = checklistReferences,
                LanguageCode = string.IsNullOrEmpty(languageCode) ? "NO" : languageCode,
                DataFormatId = dataFormatId,
                DataFormatVersion = dataFormatVersion
            };

            return validationMessageStorageEntry;
        }

    }
}
