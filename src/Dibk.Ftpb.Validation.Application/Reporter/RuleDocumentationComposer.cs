﻿using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Logic.GeneralValidations;
using Dibk.Ftpb.Validation.Application.Models.Standard;

namespace Dibk.Ftpb.Validation.Application.Reporter
{
    public class RuleDocumentationComposer
    {

        public static List<RuleModelDocumentation> GetRuleDocumentationModel(List<ValidationRule> validationRules)
        {
            var formRules = new List<RuleModelDocumentation>();

            if (validationRules != null)
            {

                foreach (var rule in validationRules)
                {
                    formRules.Add(new RuleModelDocumentation()
                    {
                        RuleId = rule.Id,
                        CheckListPt = rule.ChecklistReference,
                        Description = rule.Message,
                        Xpath = rule.XpathField,
                        XpathPrecondition = rule.PreCondition,
                        RuleType = NorskStandardValidator.NorskFeilmeldingType(rule.Messagetype)
                    });
                }
            }
            return formRules;
        }
    }
}
