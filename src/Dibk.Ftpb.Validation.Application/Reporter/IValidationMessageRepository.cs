﻿using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums;

namespace Dibk.Ftpb.Validation.Application.Reporter
{
    public interface IValidationMessageRepository
    {
        ValidationRule GetValidationRuleMessage(ValidationRule validationRule, string languageCode, string dataFormatId, string dataFormatVersion, ServiceDomain? serviceDomain);
    }
}