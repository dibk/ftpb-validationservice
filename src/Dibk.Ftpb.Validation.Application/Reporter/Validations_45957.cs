﻿using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;

namespace Dibk.Ftpb.Validation.Application.Reporter
{
    public class Validations_45957
    {
        public Validations Validations { get; set; }
        //public string ArbeidstilsynetsSamtykkeXml { get; set; }
        public ArbeidstilsynetsSamtykkeV2 ArbeidstilsynetsSamtykkeType { get; set; }
    }

}
