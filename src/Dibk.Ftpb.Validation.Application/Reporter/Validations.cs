﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Dibk.Ftpb.Validation.Application.Reporter
{
    public class Validations
    {
        public Validations()
        { }
        [JsonPropertyOrder(1)]
        public int Errors { get; set; }
        [JsonPropertyOrder(2)]
        public int Warnings { get; set; }
        [JsonPropertyOrder(3)]
        public List<string> TiltakstyperISoeknad { get; set; }
        [JsonPropertyOrder(4)]
        public string Soknadtype { get; set; }
        [JsonPropertyOrder(5)]
        public List<ValidationMessage> Messages { get; set; }
        [JsonPropertyOrder(6)]
        public List<ValidationRule> RulesChecked { get; set; }
        [JsonIgnore]
        public DateTime? ValidationDate { get; set; }

        [JsonIgnore]
        public List<ValidationMessage> ValidationMessages { get; set; }
        [JsonIgnore]
        public List<ValidationRule> ValidationRules { get; set; }
    }
}
