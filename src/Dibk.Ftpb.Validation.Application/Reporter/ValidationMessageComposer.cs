﻿using System;
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums;

namespace Dibk.Ftpb.Validation.Application.Reporter
{
    public class ValidationMessageComposer : IValidationMessageComposer
    {
        private readonly IValidationMessageRepository _repo;

        public ValidationMessageComposer(IValidationMessageRepository repo)
        {
            _repo = repo;
        }

        public ValidationRule[] ComposeValidationRules(string xPathRoot, string dataFormatId, string dataFormatVersion, List<ValidationRule> validationRules, string languageCode, ServiceDomain? serviceDomain = null)
        {
            var composedValidationRules = new List<ValidationRule>();

            if (validationRules != null)
            {
                foreach (var validationRule in validationRules)
                {
                    var rule = _repo.GetValidationRuleMessage(validationRule, languageCode, dataFormatId, dataFormatVersion, serviceDomain);

                    string precondition = null;
                    if (!string.IsNullOrEmpty(rule.PreCondition))
                    {
                        precondition = rule.PreCondition.Contains("vedlegg", StringComparison.CurrentCultureIgnoreCase) ? rule.PreCondition : $"{xPathRoot}{rule.PreCondition}";
                    }

                    composedValidationRules.Add(new ValidationRule()
                    {
                        Id = $"{dataFormatId}.{dataFormatVersion}{rule.Id}",
                        Rule = rule.Rule,
                        XpathField = $"{xPathRoot}{rule.XpathField}",
                        Message = rule.Message,
                        Messagetype = rule.Messagetype,
                        PreCondition = precondition,
                        ChecklistReference = rule.ChecklistReference,
                        XmlElement = rule.XmlElement,
                        ValidFrom = rule.ValidFrom,
                        ValidTo = rule.ValidTo
                    });
                }
            }

            return composedValidationRules.ToArray();
        }
    }
}
