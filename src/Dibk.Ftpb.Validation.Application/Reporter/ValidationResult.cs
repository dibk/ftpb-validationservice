﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Dibk.Ftpb.Validation.Application.Reporter
{
    public class ValidationResult : Validations
    {
        [JsonPropertyOrder(7)]
        public List<ChecklistAnswer> PrefillChecklist { get; set; }
    }
}
