using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Reporter.DataBase;

namespace Dibk.Ftpb.Validation.Application.Reporter
{
    public class ValidationMessageRepository : IValidationMessageRepository
    {
        private List<ValidationMessageStorageEntry> _validationMessageStorageEntry;
        private readonly IValidationMessageDb _messageDB;

        public ValidationMessageRepository(IValidationMessageDb messageDB)
        {
            _validationMessageStorageEntry = new List<ValidationMessageStorageEntry>();
            _messageDB = messageDB;
            InitiateMessageRepository();
        }
        public ValidationRule GetValidationRuleMessage(ValidationRule validationRule, string languageCode, string dataFormatId, string dataFormatVersion, ServiceDomain? serviceDomain)
        {
            ValidationRule validationRuleNew;

            // First, filter by specific DataFormatId and DataFormatVersion
            var storageEntries = _validationMessageStorageEntry
                .Where(x =>
                    x.Rule == validationRule.Rule &&
                    x.XPath == validationRule.XpathField &&
                    x.LanguageCode == languageCode &&
                    x.DataFormatId == dataFormatId &&
                    x.DataFormatVersion == dataFormatVersion)
                .ToArray();

            if (!storageEntries.Any())
            {
                storageEntries = _validationMessageStorageEntry
                    .Where(x =>
                        x.Rule == validationRule.Rule &&
                        x.XPath == validationRule.XpathField &&
                        x.LanguageCode == languageCode &&
                        string.IsNullOrEmpty(x.DataFormatId) &&
                        string.IsNullOrEmpty(x.DataFormatVersion))
                    .ToArray();
            }

            if (!storageEntries.Any())
                return CreateErrorValidationRule(validationRule, languageCode);

            var matchingEntries = storageEntries
                .Where(x => string.IsNullOrEmpty(validationRule.PreCondition) || x.Precondition == validationRule.PreCondition)
                .ToArray();

            if (matchingEntries.Length != 1)
            {
                matchingEntries = storageEntries
                    .Where(x => x.ValidFrom == validationRule.ValidFrom && x.ValidTo == validationRule.ValidTo)
                    .ToArray();

                if (matchingEntries.Length != 1)
                {
                    var errorEntry = storageEntries.First();
                    errorEntry.Messagetype = Enums.ValidationResultSeverityEnum.ERROR;
                    errorEntry.Message = $"Found more than one validation message in DB with reference: '{validationRule.Rule}', xpath: '{validationRule.XpathField}' and languageCode:'{languageCode}'. Couldn't determine the specific validation message to use.";
                    return ConsolidateValidationRule(validationRule, errorEntry, serviceDomain);
                }
            }

            validationRuleNew = ConsolidateValidationRule(validationRule, matchingEntries.First(), serviceDomain);

            return validationRuleNew;
        }
        private ValidationRule CreateErrorValidationRule(ValidationRule validationRule, string languageCode)
        {
            return new ValidationRule
            {
                Id = validationRule.Id,
                Rule = validationRule.Rule,
                XpathField = validationRule.XpathField,
                Message = $"Could not find Rule validation message in DB with reference: '{validationRule.Rule}', xpath: '{validationRule.XpathField}' and languageCode:'{languageCode}'.-",
                Messagetype = Enums.ValidationResultSeverityEnum.ERROR,
                PreCondition = null,
                ChecklistReference = validationRule.ChecklistReference,
                XmlElement = validationRule.XmlElement,
                ValidFrom = validationRule.ValidFrom,
                ValidTo = validationRule.ValidTo
            };
        }
        private static ValidationRule ConsolidateValidationRule(ValidationRule validationRule, ValidationMessageStorageEntry messageStorageEntry, ServiceDomain? serviceDomain)
        {
            var validationRuleNew = new ValidationRule()
            {
                Id = validationRule.Id,
                Rule = messageStorageEntry.Rule,
                XpathField = validationRule.XpathField,
                Message = messageStorageEntry.Message,
                Messagetype = messageStorageEntry.Messagetype,
                XmlElement = validationRule.XmlElement,
                ValidFrom = validationRule.ValidFrom,
                ValidTo = validationRule.ValidTo,
                PreCondition = messageStorageEntry.Precondition,
                ChecklistReference = null
            };

            if (validationRule.PreCondition != messageStorageEntry.Precondition)
                validationRuleNew.PreCondition = string.IsNullOrEmpty(validationRule.PreCondition)
                    ? messageStorageEntry.Precondition
                    : $"Couldn't determine precondition validationRule: '{validationRule.PreCondition}' or messageStorageEntry:'{messageStorageEntry.Precondition}'";

            if (serviceDomain.HasValue && (messageStorageEntry.ChecklistReferences != null && messageStorageEntry.ChecklistReferences.TryGetValue(serviceDomain.Value, out var checklistReference)))
                validationRuleNew.ChecklistReference = checklistReference;

            return validationRuleNew;
        }

        private void InitiateMessageRepository()
        {
            _validationMessageStorageEntry = _messageDB.InitiateMessageRepository();
        }
    }
}
