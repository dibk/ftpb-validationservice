﻿using Dibk.Ftpb.Validation.Application.Models;
using Microsoft.AspNetCore.Http;

namespace Dibk.Ftpb.Validation.Application.Services
{
    public interface IInputDataService
    {
        InputDataConfig GetInputDataConfig(string xmlString);
    }
}
