﻿namespace Dibk.Ftpb.Validation.Application.Services
{
    public class FormProperties
    {
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
        public string serviceDomain { get; set; }
        public string ProcessCategory { get; set; }
    }
}
