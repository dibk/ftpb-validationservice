﻿using Dibk.Ftpb.Validation.Application.Logic.FormValidators;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Services
{
    public interface IValidationService
    {
        Validations GetValidationResult(ValidationInput validationInput);

        ValidationResult GetValidationReportIncludingChecklistAnswers(ValidationInput validationInput);

        ValidationRule[] GetFormValidationRules(string dataFormatId, string dataFormVersion);

        Validations ValidateXmlFile(IFormFile xmlFile);

        Validations ValidateXmlFileAndAttachments(IFormFile xmlFile, List<string> attachments);

        Validations ValidateXmlFileAndAttachments(IFormFile xmlFile, IFormFile attachments, IFormFile subFormFile, string authenticatedSubmitter = null);

        FormDataAttribute GetFormDataAttribute(string dataFormatId, string dataFormVersion);
    }
}