using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Checklist;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Reporter;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.Services
{
    public interface IChecklistService
    {
        IEnumerable<Sjekk> GetChecklist(string processCategory, ServiceDomain serviceDomain, string filter);

        IEnumerable<ValidationMessage> FilterValidationResult(string processCategory, ServiceDomain serviceDomain, IEnumerable<ValidationMessage> validationMessages, IEnumerable<string> tiltakstyper, DateTime? validationDate);

        PrefillChecklist GetPrefillChecklist(ValidationResult validationResult, string dataFormatId, string dataFormatVersion, string processCategory, ServiceDomain serviceDomain);

        ValidationRule[] UpdateValidationRulesWithCheckListNumber(string processCategory, ServiceDomain serviceDomain, ValidationRule[] formValidationRules);

        ValidationRule[] GetValidationRulesWithCheckListNumber(string processCategory, ServiceDomain serviceDomain, List<ValidationRule> toArray);

        IEnumerable<String> GetSjekkpunktTiltakstype(string processCategory, ServiceDomain serviceDomain, string sjekkpunktId);

        Task RefreshCachedChecklist(string processCategory, ServiceDomain serviceDomain, string filter = null);
    }
}