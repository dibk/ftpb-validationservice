﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Checklist;
using Dibk.Ftpb.Validation.Application.Models;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Services
{
    public static class SjekkToChecklistReferenceModelMapper
    {
        public static IEnumerable<ChecklistReferenceModel> Map(IEnumerable<Sjekk> checkPoints)
        {
            List<ChecklistReferenceModel> checklistReferenceModels = null;
            if (checkPoints == null) return checklistReferenceModels;

            foreach (var checkPoint in checkPoints)
            {
                checklistReferenceModels ??= new List<ChecklistReferenceModel>();
                var checklistReferenceModel = GetReferenceModel(checkPoint);
                checklistReferenceModels.Add(checklistReferenceModel);
            }
            return checklistReferenceModels;
        }

        private static ChecklistReferenceModel GetReferenceModel(Sjekk checkPoint)
        {
            var checklistReferenceModel = new ChecklistReferenceModel()
            {
                ProcessCategory = checkPoint.Prosesskategori,
                ChecklistReference = checkPoint.Id,
                ValidFrom = checkPoint.GyldigFra,
                // ValidTo = checkPoint.Oppdatert
            };

            if (checkPoint.Undersjekkpunkter != null && checkPoint.Undersjekkpunkter.Count > 0)
                checklistReferenceModel.SubChecklistReferenceModels = GetUndersjekkpunkter(checkPoint.Undersjekkpunkter);

            if (checkPoint.Tiltakstyper != null && checkPoint.Tiltakstyper.Count > 0)
                checklistReferenceModel.EnterpriseTerms = checkPoint.Tiltakstyper.Select(x => x.Kode).ToList();

            if (checkPoint.Utfall != null && checkPoint.Utfall.Count > 0)
            {
                //TODO: Check this is correct
                checklistReferenceModel.ActionValue = checkPoint.Utfall.FirstOrDefault()!.Utfallverdi;
            }

            return checklistReferenceModel;
        }

        private static List<ChecklistReferenceModel> GetUndersjekkpunkter(List<Sjekk> Undersjekkpunkter)
        {
            List<ChecklistReferenceModel> checkListReferenceModels = null;

            foreach (var undersjekkpunkt in Undersjekkpunkter)
            {
                checkListReferenceModels ??= new List<ChecklistReferenceModel>();
                var subCheckListReferenceModel = GetReferenceModel(undersjekkpunkt);
                checkListReferenceModels.Add(subCheckListReferenceModel);
            }
            return checkListReferenceModels;
        }
    }
}