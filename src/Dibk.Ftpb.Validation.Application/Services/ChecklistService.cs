﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Checklist;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Models;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.Services
{
    public class ChecklistService : IChecklistService
    {
        private readonly IMemoryCache _memoryCache;
        private readonly ChecklistApiService _checklistApiService;
        private readonly TimeSpan _timeout;

        private bool TolerateErrors(string dataFormatVersion) // TODO: Unødvendig metode, må fjernes
        { return true; }

        public ChecklistService(ChecklistApiService checklistApiService, IMemoryCache memoryCache, IOptions<ChecklistSettings> options)
        {
            _memoryCache = memoryCache;
            _checklistApiService = checklistApiService;
            _timeout = TimeSpan.TryParse(options.Value.MemoryCacheExpiration, out var timeSpan) ? timeSpan : new TimeSpan(0, 60, 0);
        }

        public ValidationRule[] UpdateValidationRulesWithCheckListNumber(string processCategory, ServiceDomain serviceDomain, ValidationRule[] validationRules)
        {
            var checklistRelatedValidations = GetCheckListValidationRelations(processCategory, serviceDomain);
            foreach (var rule in validationRules)
            {
                var ruleId = Debugging.GetRuleIdNumber(rule.Id);
                var ruleValidFrom = rule.ValidFrom;
                var ruleValidTo = rule.ValidTo;
                var checklistValidationRelations = GetChecklistValidationRelationFromRuleId(ruleId, checklistRelatedValidations, ruleValidFrom, ruleValidTo);
                if (checklistValidationRelations != null)
                    rule.ChecklistReference = checklistValidationRelations?.ChecklistReference;
            }
            return validationRules;
        }

        public ValidationRule[] GetValidationRulesWithCheckListNumber(string processCategory, ServiceDomain serviceDomain, List<ValidationRule> validationRules)
        {
            var checklistRelatedValidations = GetCheckListValidationRelations(processCategory, serviceDomain);

            var validationRulesWithCheckListNumber = new List<ValidationRule>();
            foreach (var rule in validationRules)
            {
                var ruleId = Debugging.GetRuleIdNumber(rule.Id);
                var ruleValidFrom = rule.ValidFrom;
                var ruleValidTo = rule.ValidTo;
                var checklistValidationRelations = GetChecklistValidationRelationFromRuleId(ruleId, checklistRelatedValidations, ruleValidFrom, ruleValidTo);

                if (checklistValidationRelations == null) continue;

                var ruleWithCheckListReference = new ValidationRule()
                {
                    Rule = rule.Rule,
                    Id = rule.Id,
                    Message = rule.Message,
                    Messagetype = rule.Messagetype,
                    ValidFrom = rule.ValidFrom,
                    ValidTo = rule.ValidTo,
                    ChecklistReference = checklistValidationRelations?.ChecklistReference,
                    PreCondition = rule.PreCondition,
                    XmlElement = rule.XmlElement,
                    XpathField = rule.XpathField,
                };
                ruleWithCheckListReference.ChecklistReference = checklistValidationRelations?.ChecklistReference;
                validationRulesWithCheckListNumber.Add(ruleWithCheckListReference);
            }
            return validationRulesWithCheckListNumber.ToArray();
        }

        private ChecklistValidationRelations GetChecklistValidationRelationFromRuleId(string ruleId, IEnumerable<ChecklistValidationRelations> checklistValidationRelationsList, DateTime? validFrom = null, DateTime? validTo = null)
        {
            ChecklistValidationRelations checklistValidationRelationsObject = null;
            ruleId = Debugging.GetRuleIdNumber(ruleId);

            var filteredRelations = checklistValidationRelationsList
                .Where(relation => relation.SupportingDataValidationRuleId
                    .Any(supportingRuleId => supportingRuleId.ValidationRuleIds.Contains(ruleId))).ToList();

            if (validFrom != null || validTo != null)
            {
                filteredRelations = ChecklistValidationRelationsFilteredByValidDate(validFrom, validTo, filteredRelations, true);
            }

            if (!filteredRelations.Any())
                return null;

            var referenceCounts = filteredRelations
                .ToDictionary(
                    relation => relation.ChecklistReference,
                    relation => relation.SupportingDataValidationRuleId
                        .Where(supportingRuleId => supportingRuleId.ValidationRuleIds.Contains(ruleId))
                        .Sum(supportingRuleId => supportingRuleId.ValidationRuleIds.Count));

            var referenceWithMinCount = referenceCounts.Aggregate((x, y) => x.Value < y.Value ? x : y).Key;

            checklistValidationRelationsObject = filteredRelations.FirstOrDefault(relation => relation.ChecklistReference == referenceWithMinCount);
            return checklistValidationRelationsObject;
        }

        private List<ChecklistValidationRelations> ChecklistValidationRelationsFilteredByValidDate(DateTime? validFrom, DateTime? validTo, List<ChecklistValidationRelations> checklistValidationRelations, bool filterByValidDate)
        {
            var checklistValidationRelationsFilteredByValidDate = new List<ChecklistValidationRelations>();

            if (filterByValidDate)
            {
                foreach (var checklistValidationRelation in checklistValidationRelations)
                {
                    bool checklistValidationRelationValidFrom = false;
                    bool checklistValidationRelationValidTo = false;
                    bool validationRuleHasValidDateFromAndTo = validFrom != null && validTo != null;

                    if (validFrom != null)
                        checklistValidationRelationValidFrom = FilterChecklistValidationRelationsByValidDate(checklistValidationRelation, validFrom.Value);

                    if (validTo != null)
                        checklistValidationRelationValidTo = FilterChecklistValidationRelationsByValidDate(checklistValidationRelation, validTo.Value);

                    if ((validationRuleHasValidDateFromAndTo) && (checklistValidationRelationValidFrom == false && checklistValidationRelationValidTo == false))
                    {
                        // 0 > = later than selected date
                        var checklistValueValidFrom = checklistValidationRelation.ValidFrom == null ? 0 : DateTime.Compare(checklistValidationRelation.ValidFrom.Value.Date, validFrom.Value.Date);
                        // 0 < = earlier than selected date
                        var checklistValueValidTo = checklistValidationRelation.ValidTo == null ? 0 : DateTime.Compare(checklistValidationRelation.ValidTo.Value.Date, validTo.Value.Date);

                        if (checklistValueValidFrom < 0 && checklistValueValidTo > 0)
                            checklistValidationRelationsFilteredByValidDate.Add(checklistValidationRelation);
                    }
                    else if (checklistValidationRelationValidFrom || checklistValidationRelationValidTo)
                    {
                        checklistValidationRelationsFilteredByValidDate.Add(checklistValidationRelation);
                    }
                }
            }

            return checklistValidationRelationsFilteredByValidDate;
        }

        private static bool FilterChecklistValidationRelationsByValidDate(ChecklistValidationRelations checklistValidationRelations, DateTime validDate)
        {
            if (checklistValidationRelations.ValidFrom.HasValue && checklistValidationRelations.ValidFrom.Value.Date > validDate.Date)
                return false;

            if (checklistValidationRelations.ValidTo.HasValue && checklistValidationRelations.ValidTo.Value.Date < validDate.Date)
                return false;

            return true;
        }

        public IEnumerable<string> GetSjekkpunktTiltakstype(string processCategory, ServiceDomain serviceDomain, string sjekkpunktId)
        {
            List<string> tiltakstyper = null;
            var checklistReferenceModels = AsyncHelper.RunSync(() => GetCheckListReferenceModel(processCategory, serviceDomain));
            if (checklistReferenceModels != null)
            {
                foreach (ChecklistReferenceModel referenceModel in checklistReferenceModels)
                {
                    tiltakstyper = GetTiltakstypeFromReferenceModel(referenceModel, sjekkpunktId);
                    if (tiltakstyper != null)
                        break;
                }
            }
            return tiltakstyper;
        }

        private List<String> GetTiltakstypeFromReferenceModel(ChecklistReferenceModel checklistReferencemode, string sjekkpunktId)
        {
            List<string> tiltakstyper = null;

            if (checklistReferencemode.ChecklistReference == sjekkpunktId)
                return checklistReferencemode.EnterpriseTerms;
            if (checklistReferencemode.SubChecklistReferenceModels is { Count: > 0 })
            {
                foreach (ChecklistReferenceModel subChecklistReferenceModel in checklistReferencemode.SubChecklistReferenceModels)
                {
                    tiltakstyper = GetTiltakstypeFromReferenceModel(subChecklistReferenceModel, sjekkpunktId);
                    if (tiltakstyper != null)
                        break;
                }
            }

            return tiltakstyper;
        }

        public IEnumerable<ValidationMessage> FilterValidationResult(string processCategory, ServiceDomain serviceDomain, IEnumerable<ValidationMessage> validationMessages, IEnumerable<string> tiltakstyper, DateTime? validationDate)
        {
            var checklistRelatedValidations = GetCheckListValidationRelations(processCategory, serviceDomain);
            var tiltakstypeArray = tiltakstyper.ToArray();

            var filteredMessages = validationMessages.Where(message =>
            {
                var checklistValidationRelation = checklistRelatedValidations.FirstOrDefault(c => c.ChecklistReference == message.ChecklistReference);

                if (checklistValidationRelation == null)
                    return true;

                var checklistPointsMatching = checklistValidationRelation.EnterpriseTerms.Intersect(tiltakstypeArray).Any();
                var checklistPointIsValidOnValidationDate = validationDate != null && FilterChecklistValidationRelationsByValidDate(checklistValidationRelation, validationDate.Value);

                return checklistPointsMatching && checklistPointIsValidOnValidationDate;
            }).ToList();

            return filteredMessages;
        }

        public PrefillChecklist GetPrefillChecklist(ValidationResult validationResult, string dataFormatId, string dataFormatVersion, string processCategory, ServiceDomain serviceDomain)
        {
            var validationResultContainsErrors = validationResult.ValidationMessages.Any(x => x.Messagetype.Equals(ValidationResultSeverityEnum.ERROR));
            if (ValidationResultIsAcceptableForFurtherProcessing(validationResultContainsErrors, dataFormatVersion))
            {
                PrefillChecklistInput prefillChecklistInput = new();
                prefillChecklistInput.ProcessCategory = processCategory;
                prefillChecklistInput.DataFormatId = dataFormatId;
                prefillChecklistInput.DataFormatVersion = dataFormatVersion;

                var errors = validationResult.ValidationMessages.Where(x => x.Messagetype == Enums.ValidationResultSeverityEnum.ERROR).Select(y => y.Reference).Distinct().ToList();
                var warnings = validationResult.ValidationMessages.Where(x => x.Messagetype == Enums.ValidationResultSeverityEnum.WARNING).Select(y => y.Reference).Distinct().ToList();
                prefillChecklistInput.ExecutedValidations = validationResult.ValidationRules.Select(y => y.Id).Distinct();

                //Errors and warnings must be supplemented with all their children
                prefillChecklistInput.Errors = GetRuleChildren(errors, validationResult);
                prefillChecklistInput.Warnings = GetRuleChildren(warnings, validationResult);

                var prefilledChecklist = AsyncHelper.RunSync(() => _checklistApiService.GetPrefillChecklistAnswer(prefillChecklistInput, serviceDomain));

                return new PrefillChecklist() { ChecklistAnswer = prefilledChecklist?.ToList() };
            }
            else
            {
                //Abort sending due to not form data is not complete
                return null;
            }
        }

        public IEnumerable<Sjekk> GetChecklist(string processCategory, ServiceDomain serviceDomain, string filter)
        {
            IEnumerable<Sjekk> checkPoints = null;
            var cacheKeyName = string.IsNullOrEmpty(filter) ? $"{processCategory}-{serviceDomain}" : $"{processCategory}-{serviceDomain}-{filter}";

            if (_memoryCache.TryGetValue(cacheKeyName, out checkPoints))
                return checkPoints;

            checkPoints = AsyncHelper.RunSync(() => _checklistApiService.GetChecklist(processCategory, filter, serviceDomain));

            if (checkPoints != null)
            {
                _memoryCache.Set(cacheKeyName, checkPoints, new MemoryCacheEntryOptions()
                {
                    SlidingExpiration = _timeout
                });
            }

            return checkPoints;
        }

        private List<string> GetRuleChildren(List<string> inputMessages, ValidationResult validationResult)
        {
            List<string> messagesWithChildren = new();
            foreach (var message in inputMessages)
            {
                messagesWithChildren.Add(message);
                if (message.EndsWith(".1"))  //utfylt
                {
                    var prefix = message.Substring(0, message.Length - 1);
                    var childrenList = validationResult.ValidationRules.Where(x => x.Id.Contains(prefix)).Select(y => y.Id).Distinct();
                    messagesWithChildren.AddRange(childrenList);
                }
            }

            return messagesWithChildren.Distinct().ToList();
        }

        private bool ValidationResultIsAcceptableForFurtherProcessing(bool validationResultContainsErrors, string dataFormatVersion)
        {
            var tolerateErrors = TolerateErrors(dataFormatVersion);

            return !validationResultContainsErrors || (tolerateErrors && validationResultContainsErrors);
        }

        private IEnumerable<ChecklistValidationRelations> GetCheckListValidationRelations(string processCategory, ServiceDomain serviceDomain)
        {
            List<ChecklistValidationRelations> checkListValidationRelations;
            var keyName = $"{processCategory}-{serviceDomain}-validationrelations";

            //Get from cache
            if (_memoryCache.TryGetValue(keyName, out checkListValidationRelations))
                return checkListValidationRelations;

            //Get from API
            var checklistRelatedValidations = AsyncHelper.RunSync(() => _checklistApiService.GetChecklistValidationRelations(processCategory, serviceDomain));

            //Add to cache if found
            if (checklistRelatedValidations != null)
            {
                _memoryCache.Set(keyName, checklistRelatedValidations, new MemoryCacheEntryOptions()
                {
                    SlidingExpiration = _timeout
                });
            }

            return checklistRelatedValidations;
        }

        private async Task<IEnumerable<ChecklistReferenceModel>> GetCheckListReferenceModel(string processCategory, ServiceDomain serviceDomain, string filter = null)
        {
            var cacheKeyName = string.IsNullOrEmpty(filter) ? $"{processCategory}-{serviceDomain}" : $"{processCategory}-{serviceDomain}-{filter}";

            var checkPoints = GetChecklist(processCategory, serviceDomain, filter);

            return SjekkToChecklistReferenceModelMapper.Map(checkPoints);
        }

        public async Task RefreshCachedChecklist(string processCategory, ServiceDomain serviceDomain, string filter = null)
        {
            var cacheKeyName = string.IsNullOrEmpty(filter) ? $"{processCategory}-{serviceDomain}" : $"{processCategory}-{serviceDomain}-{filter}";

            var checklistReferenceModels = await _checklistApiService.GetChecklist(processCategory, filter, serviceDomain);

            if (checklistReferenceModels != null)
            {
                _memoryCache.Remove(cacheKeyName);

                _memoryCache.Set(cacheKeyName, checklistReferenceModels, new MemoryCacheEntryOptions()
                {
                    SlidingExpiration = _timeout
                });
            }
        }
    }
}