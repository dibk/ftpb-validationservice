using Dibk.Ftpb.Validation.Application.Config;
using Dibk.Ftpb.Validation.Application.Models.Rules;
using Dibk.Ftpb.Validation.XmlSchemaValidator.Validator;
using DiBK.RuleValidator;
using DiBK.RuleValidator.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Services.Xsd;

public class XsdValidationService : IXsdValidationService
{
    private readonly IXmlSchemaValidator _xmlSchemaValidator;
    private readonly ValidatorOptions _options;
    private readonly ILogger<XsdValidationService> _logger;

    public XsdValidationService(
        IXmlSchemaValidator xmlSchemaValidator,
        IOptions<ValidatorOptions> options,
        ILogger<XsdValidationService> logger)
    {
        _xmlSchemaValidator = xmlSchemaValidator;
        _options = options.Value;
        _logger = logger;
    }

    public List<SchemaRule> Validate(DisposableList<InputData> inputData)
    {
        var rules = new List<SchemaRule>();

        foreach (var data in inputData)
        {
            var rule = GetSchemaRule(data.DataType);

            if (rule == null)
                continue;

            var startTime = DateTime.Now;
            var messages = _xmlSchemaValidator.Validate(data.DataType.ToString(), data.Stream);

            LogInformation(rule, startTime);

            data.IsValid = !messages.Any();
            data.Stream.Position = 0;

            MapSchemaRule(messages, rule, data.FileName);

            rules.Add(rule);
        }

        return rules
            .Where(rule => rule.Executed)
            .ToList();
    }

    private SchemaRule GetSchemaRule(object dataType)
    {
        var type = _options.GetSchemaRuleType(dataType);

        if (type == null)
            return null;

        var rule = Activator.CreateInstance(type) as SchemaRule;
        rule.Create();

        return rule;
    }

    private void LogInformation(SchemaRule schemaRule, DateTime startTime)
    {
        _logger.LogInformation("{@Rule}", new
        {
            schemaRule.Id,
            schemaRule.Name,
            FullName = schemaRule.ToString(),
            schemaRule.Status,
            TimeUsed = DateTime.Now.Subtract(startTime).TotalSeconds,
            MessageCount = schemaRule.Messages.Count
        });
    }

    private static void MapSchemaRule(List<string> xsdValidationResult, SchemaRule schemaRule, string fileName)
    {
        xsdValidationResult
            .Select(message => new RuleMessage { Message = message, Properties = new Dictionary<string, object> { { "FileName", fileName } } })
            .ToList()
            .ForEach(schemaRule.AddMessage);

        schemaRule.Status = !schemaRule.Messages.Any() ? Status.PASSED : Status.FAILED;
    }
}
