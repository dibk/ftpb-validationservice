using Dibk.Ftpb.Validation.Application.Models.Rules;
using DiBK.RuleValidator.Extensions;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Services.Xsd;

public interface IXsdValidationService
{
    List<SchemaRule> Validate(DisposableList<InputData> inputData);
}
