using Dibk.Ftpb.Validation.Application.Reporter;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.Services.Gml.Planområde;

public interface IPlanområdeValidationService
{
    Task<Validations> ValidateAsync(IFormFile file);
}
