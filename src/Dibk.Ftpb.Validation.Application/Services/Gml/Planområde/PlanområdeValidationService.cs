using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Models.Gml;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services.Xsd;
using DiBK.RuleValidator;
using DiBK.RuleValidator.Extensions;
using DiBK.RuleValidator.Extensions.Gml;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ValidationRule = Dibk.Ftpb.Validation.Application.Reporter.ValidationRule;

namespace Dibk.Ftpb.Validation.Application.Services.Gml.Planområde;

public class PlanområdeValidationService : IPlanområdeValidationService
{
    private readonly IXsdValidationService _xsdValidationService;
    private readonly IRuleValidator _ruleValidator;

    private const string DataType = "Planområde";

    public PlanområdeValidationService(
        IXsdValidationService xsdValidationService,
        IRuleValidator ruleValidator)
    {
        _xsdValidationService = xsdValidationService;
        _ruleValidator = ruleValidator;
    }

    public async Task<Validations> ValidateAsync(IFormFile file)
    {
        var startTime = DateTime.Now;

        using var inputData = CreateInputData(file);

        var xsdRule = _xsdValidationService.Validate(inputData);

        var gmlDocuments = CreateGmlDocuments(inputData);
        var gmlValidationInputV1 = GmlValidationInput.Create(gmlDocuments);
        var poValidationInput = PlanområdeValidationInput.Create(gmlDocuments);
        
        await _ruleValidator.Validate(gmlValidationInputV1, options => {});
        await _ruleValidator.Validate(poValidationInput, options => { });

        gmlValidationInputV1.Dispose();
        poValidationInput.Dispose();

        var rules = new List<Rule>(xsdRule);
        rules.AddRange(_ruleValidator.GetAllRules());

        var report = ValidationReport.Create(Guid.NewGuid().ToString(), rules, inputData, new List<string>(), startTime);

        return MapFromValidationReport(report);
    }

    private static List<GmlDocument> CreateGmlDocuments(DisposableList<InputData> inputData)
    {
        var documents = new List<GmlDocument>();

        foreach (var data in inputData)
        {
            if (!data.IsValid)
                continue;

            documents.Add(GmlDocument.Create(data));
        }

        return documents;
    }

    private static DisposableList<InputData> CreateInputData(IFormFile file)
    {
        var inputData = new InputData(file.OpenReadStream(), file.FileName, DataType);
        var inputDataList = new List<InputData> { inputData };

        return inputDataList.ToDisposableList();
    }

    private static Validations MapFromValidationReport(ValidationReport report)
    {
        var validations = new Validations
        {
            Errors = report.Errors,
            Warnings = report.Warnings,
            RulesChecked = new List<ValidationRule>(),
            Messages = new List<ValidationMessage>(),
            ValidationRules = new List<ValidationRule>(),
            ValidationMessages = new List<ValidationMessage>(),
            ValidationDate = DateTime.Now,
        };

        foreach (var rule in report.Rules)
        {
            var messageType = rule.MessageType.ToLower() switch
            {
                "error" => ValidationResultSeverityEnum.ERROR,
                "warning" => ValidationResultSeverityEnum.WARNING,
                _ => ValidationResultSeverityEnum.WARNING,
            };

            foreach (var message in rule.Messages)
            {
                message.TryGetValue(MessagesKeys.Message, out var ruleMessage);
                message.TryGetValue(MessagesKeys.XPaths, out var xPaths);

                var xpathField = (xPaths as string[])?.FirstOrDefault();

                var validationMessage = new ValidationMessage
                {
                    Reference = rule.Id,
                    Rule = rule.Name,
                    Message = ruleMessage as string,
                    Messagetype = messageType,
                    XpathField = xpathField,
                };

                var validationRule = new ValidationRule
                {
                    Id = rule.Id,
                    Rule = rule.Name,
                    Message = ruleMessage as string,
                    Messagetype = messageType,
                    XpathField = xpathField,
                };

                if (!rule.Status.Equals("SKIPPED", StringComparison.OrdinalIgnoreCase))
                {
                    validations.Messages.Add(validationMessage);
                    validations.RulesChecked.Add(validationRule);
                }

                validations.ValidationRules.Add(validationRule);
                validations.ValidationMessages.Add(validationMessage);
            }

            if (!rule.Messages.Any() && !rule.Status.Equals("SKIPPED", StringComparison.OrdinalIgnoreCase))
            {
                validations.RulesChecked.Add(new ValidationRule
                {
                    Id = rule.Id,
                    Rule = rule.Name,
                    Messagetype = messageType,
                    Message = rule.Status,
                });
            }
        }

        return validations;
    }

    private static class MessagesKeys
    {
        public const string Message = "Message";
        public const string FileName = "FileName";
        public const string XPaths = "XPaths";
        public const string GmlIds = "GmlIds";
        public const string LineNumber = "LineNumber";
        public const string LinePosition = "LinePosition";
    }
}
