using Dibk.Ftpb.Validation.Application.Models.Gml;
using Dibk.Ftpb.Validation.Application.Services.Xsd;
using DiBK.RuleValidator;
using DiBK.RuleValidator.Extensions;
using DiBK.RuleValidator.Extensions.Gml;
using DiBK.RuleValidator.Rules.Gml;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.Services.Gml.Generic;

public class GmlValidationService : IGmlValidationService
{
    private readonly IXsdValidationService _xsdValidationService;
    private readonly IRuleValidator _ruleValidator;

    public GmlValidationService(
        IXsdValidationService xsdValidationService,
        IRuleValidator ruleValidator)
    {
        _xsdValidationService = xsdValidationService;
        _ruleValidator = ruleValidator;
    }

    public async Task<ValidationReport> ValidateAsync(IFormFile file, string dataType)
    {
        var startTime = DateTime.Now;

        using var inputData = CreateInputData(file, dataType);

        var xsdRule = _xsdValidationService.Validate(inputData);

        var validationInput = GetGmlValidationInputV1(inputData);

        await _ruleValidator.Validate(validationInput, options => { });

        validationInput.Dispose();

        var rules = new List<Rule>(xsdRule);
        rules.AddRange(_ruleValidator.GetAllRules());

        var report = ValidationReport.Create(Guid.NewGuid().ToString(), rules, inputData, new List<string>(), startTime);

        return report;
    }

    private static IGmlValidationInputV1 GetGmlValidationInputV1(DisposableList<InputData> inputData)
    {
        var documents = new List<GmlDocument>();

        foreach (var data in inputData)
        {
            if (!data.IsValid)
                continue;

            documents.Add(GmlDocument.Create(data));
        }

        return GmlValidationInput.Create(documents);
    }

    private static DisposableList<InputData> CreateInputData(IFormFile file, string dataType)
    {
        var inputData = new InputData(file.OpenReadStream(), file.FileName, dataType);
        var inputDataList = new List<InputData> { inputData };

        return inputDataList.ToDisposableList();
    }
}
