using DiBK.RuleValidator.Extensions;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.Services.Gml.Generic;

public interface IGmlValidationService
{
    Task<ValidationReport> ValidateAsync(IFormFile file, string dataType);
}
