using Dibk.Ftpb.Validation.Application.Reporter;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Models.Web;
using System.Linq;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators;
using Newtonsoft.Json;
using System;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Utils;
using Newtonsoft.Json.Linq;

namespace Dibk.Ftpb.Validation.Application.Services
{
    public class ValidationService : IValidationService
    {
        private readonly IInputDataService _inputDataService;
        private readonly IValidationHandler _validationHandler;
        private readonly IChecklistService _checklistService;

        //private Models.InputData _inputData;
        //private List<string> _errorMessages;
        //private ValidationResult _validationResult;
        private List<ChecklistAnswer> _outputlist = new List<ChecklistAnswer>();
        private IEnumerable<PrefillDemo> _alleSjekklistepunkter;

        public ValidationService(IInputDataService inputDataService, IValidationHandler validationOrchestrator, IChecklistService checklistService)
        {
            _inputDataService = inputDataService;
            _validationHandler = validationOrchestrator;
            _checklistService = checklistService;
        }

        public Validations GetValidationResult(ValidationInput validationInput)
        {
            var result = ValidateForm(validationInput);

            return GetValidations(result);
        }

        public ValidationResult GetValidationReportIncludingChecklistAnswers(ValidationInput validationInput)
        {
            var inputDataConfig = _inputDataService.GetInputDataConfig(validationInput.FormData);
            var validationResult = ValidateForm(validationInput);
            var dataFormatVersion = inputDataConfig?.DataFormatVersion;
            var dataFormatId = inputDataConfig?.DataFormatId;

            if (!string.IsNullOrEmpty(dataFormatVersion) && !string.IsNullOrEmpty(dataFormatId))
            {
                if (validationResult.PrefillChecklist != null)
                {
                    var formDataAttribute = _validationHandler.formDataAttribute();

                    var prefilledAnswersFromChecklist = _checklistService.GetPrefillChecklist(validationResult, formDataAttribute.DataFormatId, formDataAttribute.DataFormatVersion, formDataAttribute.ProcessCategory, formDataAttribute.ServiceDomain);

                    validationResult.PrefillChecklist.AddRange(prefilledAnswersFromChecklist.ChecklistAnswer);

                    //Adding xpath to SupportingDataXpathField if SupportingDataXpathField exists. Relevant for prefilled checklistasnwers where checklist backend holds the truth (of answering true/false)
                    foreach (var answer in validationResult.PrefillChecklist)
                    {
                        if (answer.SupportingDataValidationRuleId != null && answer.SupportingDataXpathField == null)
                        {
                            answer.SupportingDataXpathField = new List<string>();

                            foreach (var ruleId in answer.SupportingDataValidationRuleId)
                            {  
                                var xPathsIfNotAlreadyExisting = validationResult.ValidationRules.Where
                                    (x => ruleId.Equals(x.Id) && !answer.SupportingDataXpathField.Any(y => y.Equals(x.XpathField))).Select(z => z.XpathField).ToList();

                                answer.SupportingDataXpathField.AddRange(xPathsIfNotAlreadyExisting);
                            }
                        }
                    }

                    var PrefillChecklistOrdered = validationResult.PrefillChecklist.OrderBy(x => double.Parse(x.ChecklistReference.Substring(x.ChecklistReference.IndexOf(".")).Length == 2 ?
                                                                                                        x.ChecklistReference.Replace(".", ",0") : x.ChecklistReference.Replace(".", ","))).ToList();
                    validationResult.PrefillChecklist = PrefillChecklistOrdered;
                }
            }
            else
            {
                throw new System.ArgumentOutOfRangeException("Missing DataFormatVersion");
            }

            return validationResult;
        }

        public ValidationRule[] GetFormValidationRules(string dataFormatId, string dataFormVersion)
        {
            var rules = AsyncHelper.RunSync(() => _validationHandler.GetformRulesAsync(dataFormatId, dataFormVersion));
            return rules;
        }

        private ValidationResult ValidateForm(ValidationInput validationInput)
        {
            var inputDataConfig = _inputDataService.GetInputDataConfig(validationInput.FormData);
            var validationResult = new ValidationResult();

            //Verify stuff - functionality as it is in Alpha
            //**************         START             *************************************************************************************

            //_logger.Debug($"{archiveReference} is being validated for structure issues");
            ////Validate structure
            //string formDataAsXml = archivedForm.Forms.First().FormData;
            //string mainDataFormatID = archivedForm.Forms.First().DataFormatID;
            //string mainDataFormatVersionID = archivedForm.Forms.First().DataFormatVersionID.ToString();

            //ftbId = XmlUtil.GetElementValue(formDataAsXml, "ftbid");


            //FormValidationService val = new FormValidationService();
            //if (!val.IsKnownForm(mainDataFormatID, mainDataFormatVersionID))
            //{
            //    _logger.Debug($"{archiveReference} form is not supported");
            //    _logEntryService.Save(new LogEntry(archiveReference, Resources.TextStrings.ShippingErrorDownloadAndDeserialize + " skjema (" + archivedForm.ServiceCode + "/" + archivedForm.ServiceEditionCode + ") støttes ikke", "Error"));

            //    string title = String.Format(Resources.TextStrings.ShippingErrorTitle, archivedForm.ArchiveReference);
            //    string summary = Resources.TextStrings.ShippingErrorSummary;
            //    string body = String.Format(Resources.TextStrings.ShippingErrorBody, archivedForm.ArchiveReference) + Environment.NewLine + Resources.TextStrings.ShippingErrorDownloadAndDeserialize + " skjema (" + archivedForm.ServiceCode + "/" + archivedForm.ServiceEditionCode + ") støttes ikke";
            //    _correspondenceHelper.SendSimpleNotificaitonToReportee(archivedForm.Reportee, title, summary, body, archivedForm.ArchiveReference);
            //    _formMetadataService.UpdateValidationResultToFormMetadata(archiveReference, "Feil", 1, 0);

            //**************         END             *************************************************************************************
            var dataFormatVersion = inputDataConfig?.DataFormatVersion;
            var dataFormatId = inputDataConfig?.DataFormatId;

            if (!string.IsNullOrEmpty(dataFormatId) || !string.IsNullOrEmpty(dataFormatVersion))
            {
                validationResult = _validationHandler.Validate(dataFormatId, dataFormatVersion,validationInput);

                validationResult.Soknadtype = _validationHandler?.formDataAttribute()?.ProcessCategory;
            }
            else
            {
                validationResult.ValidationMessages = new List<ValidationMessage> { new() { Message = "Can't Get DataFormatId, DataFormatVersion from xml data" } };
            }

            validationResult.Errors = validationResult.ValidationMessages.Count(x => x.Messagetype == ValidationResultSeverityEnum.ERROR);
            validationResult.Warnings = validationResult.ValidationMessages.Count(x => x.Messagetype == ValidationResultSeverityEnum.WARNING);

            validationResult.Messages = validationResult.ValidationMessages;
            validationResult.RulesChecked = validationResult.ValidationRules;
            return validationResult;
        }

        public Validations ValidateXmlFile(IFormFile xmlFile)
        {
            ValidationResult result = null;
            //try
            //{
                ValidationInput validationInput;
                using var fileStream = xmlFile.OpenReadStream();
                var memoryStream = new MemoryStream();
                xmlFile.OpenReadStream().CopyTo(memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);

                using var reader = new StreamReader(memoryStream);

                validationInput = new ValidationInput()
                {
                    FormData = reader.ReadToEnd()
                };
                result = ValidateForm(validationInput);

                return GetValidations(result);

            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            return null;
        }

        public Validations ValidateXmlFileAndAttachments(IFormFile xmlFile, List<string> attachments)
        {
            ValidationResult result = null;
            //try
            //{
            ValidationInput validationInput;
            using var fileStream = xmlFile.OpenReadStream();
            var memoryStream = new MemoryStream();
            xmlFile.OpenReadStream().CopyTo(memoryStream);
            memoryStream.Seek(0, SeekOrigin.Begin);

            using var reader = new StreamReader(memoryStream);

            List<AttachmentInfo> atts = new List<AttachmentInfo>();
            foreach (var attachment in attachments)
            {
                atts.Add(new AttachmentInfo() { AttachmentTypeName = attachment, Filename = $"{attachment}.pdf" });
            }

            validationInput = new ValidationInput()
            {
                FormData = reader.ReadToEnd(),
                Attachments = atts.ToArray()
            };
            result = ValidateForm(validationInput);

            return GetValidations(result);
        }

        public Validations ValidateXmlFileAndAttachments(IFormFile xmlFile, IFormFile attachmentsFile, IFormFile subFormFile, string authenticatedSubmitter=null)
        {
            ValidationResult result = null;
            var xmlMemoryStream = new MemoryStream();
            xmlFile.OpenReadStream().CopyTo(xmlMemoryStream);
            xmlMemoryStream.Seek(0, SeekOrigin.Begin);

            using var xmlReader = new StreamReader(xmlMemoryStream);

            

            AttachmentInfo[] attachments = null;
            if (attachmentsFile != null)
            {
                var attachmentsMemoryStream = new MemoryStream();
                attachmentsFile.OpenReadStream().CopyTo(attachmentsMemoryStream);
                attachmentsMemoryStream.Seek(0, SeekOrigin.Begin);

                try
                {
                    using var attachemntsReader = new StreamReader(attachmentsMemoryStream);
                    var jsonString = attachemntsReader.ReadToEnd();
                    var jarray = JArray.Parse(jsonString);
                    attachments = jarray.ToObject<AttachmentInfo[]>();
                }
                catch
                {

                    // TODO do something with can't parse attachments from Json
                }
            }

            SubFormInfo[] subForms = null;

            if (!Helpers.ObjectIsNullOrEmpty(subFormFile))
            {
                var subFormMemoryStream = new MemoryStream();
                subFormFile.OpenReadStream().CopyTo(subFormMemoryStream);
                subFormMemoryStream.Seek(0, SeekOrigin.Begin);

                try
                {
                    using var subformReader = new StreamReader(subFormMemoryStream);
                    var jsonString = subformReader.ReadToEnd();
                    var jarray = JArray.Parse(jsonString);
                    subForms = jarray.ToObject<SubFormInfo[]>();
                }
                catch
                {

                    // TODO do something with can't parse attachments from Json
                }
            }

            var validationInput = new ValidationInput()
            {
                FormData = xmlReader.ReadToEnd(),
                Attachments = attachments,
                AuthenticatedSubmitter = authenticatedSubmitter,
                SubForms = subForms,
            };
            result = ValidateForm(validationInput);

            return GetValidations(result);
        }

        private static Validations GetValidations(ValidationResult result)
        {
            var validations = new Validations();
            validations.Messages = result.ValidationMessages;
            validations.RulesChecked = result.ValidationRules;
            validations.Warnings = result.Warnings;
            validations.Errors = result.Errors;
            validations.Soknadtype = result.Soknadtype;
            validations.TiltakstyperISoeknad = result.TiltakstyperISoeknad;

            return validations;
        }

        public FormDataAttribute GetFormDataAttribute(string dataFormatId, string dataFormVersion)
        {
            var formDataAttribute = _validationHandler.GetFormDataAttribute(dataFormatId, dataFormVersion);
            return formDataAttribute;
        }

        private void LagDings(string checklistReference)
        {
            var outputpt = new ChecklistAnswer();
            var hovedpktSomHarUnderpkt = _alleSjekklistepunkter.FirstOrDefault(x => x.ChecklistReference.Equals(checklistReference) && x.ActionTypeCode.Equals("SU"));
            var hovedpktSomHarDok = _alleSjekklistepunkter.FirstOrDefault(x => x.ChecklistReference.Equals(checklistReference) && x.ActionTypeCode.Equals("DOK"));
            var hovedpkt = _alleSjekklistepunkter.FirstOrDefault(x => x.ChecklistReference.Equals(checklistReference));

            var sjekkpktHaandert = _outputlist.FirstOrDefault(x => x.ChecklistReference.Equals(checklistReference)) != null;
            if (!sjekkpktHaandert)
            {
                outputpt = AddStuff(outputpt);
                if (hovedpktSomHarUnderpkt != null)
                {
                    outputpt.ChecklistReference = hovedpktSomHarUnderpkt.ChecklistReference;
                    outputpt.ChecklistQuestion = hovedpktSomHarUnderpkt.ChecklistQuestion;
                    outputpt.YesNo = hovedpktSomHarUnderpkt.YesNo;
                    _outputlist.Add(outputpt);

                    foreach (var underpkt in _alleSjekklistepunkter.Where(x => hovedpktSomHarUnderpkt.ChecklistReference.Equals(x.ParentActivityAction)))
                    {
                        LagDings(underpkt.ChecklistReference);
                    }
                }
                else if (hovedpktSomHarDok != null)
                {
                    outputpt.ChecklistReference = hovedpktSomHarDok.ChecklistReference;
                    outputpt.ChecklistQuestion = hovedpktSomHarDok.ChecklistQuestion;
                    outputpt.YesNo = hovedpktSomHarDok.YesNo;
                    outputpt.Documentation = $"Dette er dokumentasjon for sjekklistepunkt {hovedpktSomHarDok.ChecklistReference}";
                    _outputlist.Add(outputpt);
                }
                else
                {
                    outputpt.ChecklistReference = hovedpkt.ChecklistReference;
                    outputpt.ChecklistQuestion = hovedpkt.ChecklistQuestion;
                    var randomBool = new Random().Next(2) == 1; // 0 = false, 1 = true;
                    outputpt.YesNo = randomBool;
                    _outputlist.Add(outputpt);
                }
            }
        }

        private string MakeValidationreport(List<ChecklistAnswer> _outputlist)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(List<ChecklistAnswer>));
            var subReq = _outputlist;
            var xml = "";

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, subReq);
                    xml = sww.ToString(); // Your XML
                }
            }

            return xml;
        }
        private ChecklistAnswer AddStuff(ChecklistAnswer input)
        {
            input.SupportingDataValidationRuleId = new List<string>();
            input.SupportingDataValidationRuleId.Add("999.99");
            input.SupportingDataValidationRuleId.Add("999.66");

            input.SupportingDataXpathField = new List<string>();
            input.SupportingDataXpathField.Add("ArbeidstilsynetsSamtykke/aaa");
            input.SupportingDataXpathField.Add("ArbeidstilsynetsSamtykke/bbb");

            return input;
        }

        //public FormProperties GetFormProperties(string dataFormatVersion)
        //{



        //    throw new NotImplementedException();
        //}
    }
}
