﻿
using Dibk.Ftpb.Validation.Application.Models;
using System.Text.RegularExpressions;

namespace Dibk.Ftpb.Validation.Application.Services
{
    public class InputDataService : IInputDataService
    {
        private static readonly Regex _dataFormatRegex = new(@"^(?=.*dataFormatId=\W(?<dataFormatId>\d+)\W)(?=.*dataFormatVersion=\W(?<dataFormatVersion>\d+(\.\d+)?)\W).*$", RegexOptions.Compiled | RegexOptions.Multiline);

        public InputDataConfig GetInputDataConfig(string xmlString)
        {
            var match = _dataFormatRegex.Match(xmlString);

            if (!match.Success)
                return null;

            var dataFormatId = match.Groups["dataFormatId"].Value;
            var dataFormatVersion = match.Groups["dataFormatVersion"].Value;
            if (!string.IsNullOrEmpty(dataFormatVersion) && dataFormatVersion.Contains("."))
                dataFormatVersion = dataFormatVersion.Replace(".", "");

            return new InputDataConfig(dataFormatId, dataFormatVersion);
        }
    }
}
