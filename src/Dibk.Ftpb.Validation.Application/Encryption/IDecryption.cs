﻿namespace Dibk.Ftpb.Validation.Application.Encryption
{
    public interface IDecryption
    {
        string DecryptText(string cipherText);
        string EncryptText(string clearText);

    }
}