﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Dibk.Ftpb.Validation.Application.Encryption
{
    public class Decryption : IDecryption
    {
        private static RSA _privateKey;
        private static RSA _publicKey;

        public Decryption(IOptions<EncryptionSettings> encryptionSettings)
        {
            var certificateThumbprintSetting = encryptionSettings.Value.CertificateThumbprint;
            var cryptoProviders = GetRsaCryptoProviderFromKeyStore(certificateThumbprintSetting);

            _privateKey = cryptoProviders.Item1;
            _publicKey = cryptoProviders.Item2;
        }

        public string DecryptText(string cipherText)
        {
            if (string.IsNullOrEmpty(cipherText)) { return null; }
            
            var retVal = cipherText;
            try
            {
                if (cipherText.Length > 11)
                    retVal = DecryptString(_privateKey, cipherText);
            }
            catch { } //To be able to test decryption

            return retVal;
        }

        private static string DecryptString(RSA rsaCryptoPrivateKey, string cipherB64Text)
        {
            var retVal = string.Empty;
            try
            {
                var cipherBytes = Convert.FromBase64String(cipherB64Text);
                var decryptedBytes = rsaCryptoPrivateKey.Decrypt(cipherBytes, RSAEncryptionPadding.Pkcs1);
                var byteConverter = new UnicodeEncoding();
                retVal = byteConverter.GetString(decryptedBytes);
            }
            catch { }

            return retVal;
        }

        public string EncryptText(string clearText)
        {
            return EncryptString(_publicKey, clearText);
        }

        private static string EncryptString(RSA rsaCryptoPublicKey, string clearText)
        {
            var byteConverter = new UnicodeEncoding();
            var clearTextBytes = byteConverter.GetBytes(clearText);
            var cipherBytes = rsaCryptoPublicKey.Encrypt(clearTextBytes, RSAEncryptionPadding.Pkcs1);
            var clearTextBase64 = Convert.ToBase64String(cipherBytes);

            return clearTextBase64;
        }

        private static Tuple<RSA, RSA> GetRsaCryptoProviderFromKeyStore(string thumbprint)
        {
            X509Certificate2 cert = null;

            var certStores = new List<X509Store>
            {
                new(StoreName.My, StoreLocation.CurrentUser),
                new(StoreName.My, StoreLocation.LocalMachine)
            };

            foreach (var certStore in certStores)
            {
                certStore.Open(OpenFlags.ReadOnly);
                var certCollection = certStore.Certificates.Find(
                    X509FindType.FindByThumbprint,
                    thumbprint,
                    false);
                // Get the first cert with the thumbprint
                if (certCollection.Count > 0)
                {
                    cert = certCollection[0];
                }
                certStore.Close();

                // Exit foreach if already found
                if (cert != null)
                {
                    break;
                }
            }

            if (cert != null)
                return Tuple.Create(cert.GetRSAPrivateKey(), cert.GetRSAPublicKey());
            throw new Exception($"No certificate found for given thumbprint {thumbprint}");
        }
    }
}