﻿namespace Dibk.Ftpb.Validation.Application.Encryption
{
    public class EncryptionSettings
    {
        public string CertificateThumbprint { get; set; }
    }
}