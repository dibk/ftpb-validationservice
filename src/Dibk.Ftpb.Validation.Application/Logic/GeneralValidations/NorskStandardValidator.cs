﻿using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using System;
using System.Text.RegularExpressions;

namespace Dibk.Ftpb.Validation.Application.Logic.GeneralValidations
{
    public class NorskStandardValidator
    {
        public static FoedselnumerValidation ValidateFoedselsnummer(string foedselsnummer, IDecryptionFactory decryptionFactory)
        {
            if (string.IsNullOrEmpty(foedselsnummer))
            {
                return FoedselnumerValidation.Empty;
            }
            foedselsnummer = DecryptFoedselnummer(foedselsnummer, decryptionFactory);

            if (string.IsNullOrEmpty(foedselsnummer))
                return FoedselnumerValidation.InvalidEncryption;

            var numberOfDigitsConstraint = CreateDigitsConstraintCheck(foedselsnummer, "^([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])$");

            if (!numberOfDigitsConstraint.Success)
                return FoedselnumerValidation.Invalid;

            if (!ControlDigitIsValid(numberOfDigitsConstraint))
                return FoedselnumerValidation.InvalidDigitsControl;

            return FoedselnumerValidation.Ok;
        }

        public static OrganisasjonsnummerValidation ValidateOrgnummer(string organisasjonsnummer)
        {
            if (string.IsNullOrEmpty(organisasjonsnummer))
            {
                return OrganisasjonsnummerValidation.Empty;
            }
            else
            {
                // https://www.brreg.no/om-oss/oppgavene-vare/alle-registrene-vare/om-enhetsregisteret/organisasjonsnummeret/
                var orgNumerRegexMatch = CreateDigitsConstraintCheck(organisasjonsnummer, "^([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])([0-9])$");

                if (!orgNumerRegexMatch.Success)
                    return OrganisasjonsnummerValidation.Invalid;

                if (!OrgnummerDigitsControlValidation(orgNumerRegexMatch))
                    return OrganisasjonsnummerValidation.InvalidDigitsControl;
            }
            return OrganisasjonsnummerValidation.Ok;
        }

        private static string DecryptFoedselnummer(string fodselsnummer, IDecryptionFactory decryptionFactory)
        {
            try
            {
                if (fodselsnummer.Length > 11)
                    fodselsnummer = decryptionFactory.GetDecryptor().DecryptText(fodselsnummer);

                return fodselsnummer;
            }
            catch
            {
                //TODO logger - Fødselsnummer kan ikke dekrypteres
                return null;
            }
        }

        private static bool OrgnummerDigitsControlValidation(Match orgnrMatch)
        {
            if (orgnrMatch == null)
                return false;
            int products = Convert.ToInt32(orgnrMatch.Groups[1].Value) * 3 +
                           Convert.ToInt32(orgnrMatch.Groups[2].Value) * 2 +
                           Convert.ToInt32(orgnrMatch.Groups[3].Value) * 7 +
                           Convert.ToInt32(orgnrMatch.Groups[4].Value) * 6 +
                           Convert.ToInt32(orgnrMatch.Groups[5].Value) * 5 +
                           Convert.ToInt32(orgnrMatch.Groups[6].Value) * 4 +
                           Convert.ToInt32(orgnrMatch.Groups[7].Value) * 3 +
                           Convert.ToInt32(orgnrMatch.Groups[8].Value) * 2;

            int controlDigit = 11 - (products % 11);
            if (controlDigit == 11)
            {
                controlDigit = 0;
            }
            return controlDigit == Convert.ToInt32(orgnrMatch.Groups[9].Value);
        }

        private static Match CreateDigitsConstraintCheck(string input, string pattern)
        {
            if (string.IsNullOrWhiteSpace(input))
                return Match.Empty;

            Match match = Regex.Match(input, pattern);

            return match;
        }

        private static bool ControlDigitIsValid(Match fnrMatch)
        {
            int productsFirstControlDigit = ControlDigitCheck(new[] { 3, 7, 6, 1, 8, 9, 4, 5, 2 }, fnrMatch);
            int firstControlDigit = 11 - (productsFirstControlDigit % 11);

            if (firstControlDigit == 11)
                firstControlDigit = 0;

            int productsSecondControlDigit = ControlDigitCheck(new[] { 5, 4, 3, 2, 7, 6, 5, 4, 3 }, fnrMatch, firstControlDigit, 2);
            int secondControlDigit = 11 - (productsSecondControlDigit % 11);

            if (secondControlDigit == 11)
                secondControlDigit = 0;

            if (firstControlDigit == Convert.ToInt32(fnrMatch.Groups[10].Value) && secondControlDigit == Convert.ToInt32(fnrMatch.Groups[11].Value))
                return true;

            return false; //"Fødselsnummeret har ikke gyldig kontrollsiffer"
        }

        private static int ControlDigitCheck(int[] weightCoefficients, Match isNumberValid, int productsFirstControlDigit = 0, int coefficientForFirstControlNumber = 0)
        {
            int products = 0;

            for (int i = 1; i <= weightCoefficients.Length; i++)
            {
                products += Convert.ToInt32(isNumberValid.Groups[i].Value) * weightCoefficients[i - 1];
            }

            if (coefficientForFirstControlNumber != 0)
            {
                products += productsFirstControlDigit * coefficientForFirstControlNumber;
            }
            return products;
        }

        public static GeneralValidationResultEnum BruksenhetsnummerStandardValidator(string bruksenhetsnr)
        {
            if (string.IsNullOrEmpty(bruksenhetsnr))
                return GeneralValidationResultEnum.Empty;

            if (!Regex.Match(bruksenhetsnr, @"^[HKLU]\d{4}$").Success)
                return GeneralValidationResultEnum.Invalid;

            return GeneralValidationResultEnum.Ok;
        }

        public static string NorskFeilmeldingType(ValidationResultSeverityEnum? ruleEnum)
        {
            switch (ruleEnum)
            {
                case ValidationResultSeverityEnum.ERROR:
                    return "Feil";

                case ValidationResultSeverityEnum.WARNING:
                    return "Advarsel";

                default:
                    return "";
            }
        }
    }
}