﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface ISluttrapportForAvfallValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(SluttrapportForAvfall sluttrapportForAvfall = null, Overgangsordning overgangsordning = null, string[] attachmentsTypesName = null);
    }
}
