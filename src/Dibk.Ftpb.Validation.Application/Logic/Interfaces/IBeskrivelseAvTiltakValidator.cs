﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IBeskrivelseAvTiltakValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(BeskrivelseAvTiltak modeldata = null);
        string[] Tiltakstypes { get; }
    }
}