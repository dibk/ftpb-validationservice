﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface ISuppleringSaksnummerValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(Saksnummer saksnummer, bool mangelbesvarelse);
    }
}