﻿using Dibk.Ftpb.Validation.Application.Reporter;
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Models.Web;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IFormValidator
    {
        ValidationResult StartValidation(ValidationInput validationInput);
        ValidationRule[] formValidationRules();
        FormValidators.FormDataAttribute GetFormDataAttribute();
    }
    public interface IFormWithChecklistAnswers
    {
        List<ChecklistAnswer> GetChecklistAnswersFromStaticPartOfDatamodel(ValidationInput validationInput);
    }
}
