﻿using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterPlan;

public interface IAktoerPlanValidator
{
    ValidationResult ValidationResult { get; set; }
    ValidationResult Validate(AktoerPlan aktoer = null);
    string _entityXPath { get; }
}