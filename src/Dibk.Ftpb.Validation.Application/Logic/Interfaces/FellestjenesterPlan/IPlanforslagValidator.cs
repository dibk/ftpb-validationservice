﻿using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterPlan;

public interface IPlanforslagValidator
{
    ValidationResult ValidationResult { get; set; }
    ValidationResult Validate(Planforslag planforslag);
}