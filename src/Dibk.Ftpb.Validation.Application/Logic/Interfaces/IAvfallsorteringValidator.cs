﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IAvfallsorteringValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(Avfallsortering avfallsortering = null, Overgangsordning overgangsordning = null, string[] attachmentsTypesName = null);
        string _entityXPath { get; }

    }
}
