﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IBeskrivelseAvTiltakAtilV2Validator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(List<ValidationRule> validationRules, BeskrivelseAvTiltak modeldata = null);
        string[] Tiltakstypes { get; }
    }
}