﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IPlanForAvfallValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(PlanForAvfall planForAvfall = null, Overgangsordning overgangsordning = null, string[] attachmentsTypesName = null);
    }
}
