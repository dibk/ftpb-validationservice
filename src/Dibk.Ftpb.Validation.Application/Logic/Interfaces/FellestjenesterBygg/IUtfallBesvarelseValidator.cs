﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;

public interface IUtfallBesvarelseValidator
{
    ValidationResult ValidationResult { get; set; }
    string _entityXPath { get; }


    ValidationResult Validate(UtfallSvar utfallSvar, int? delsoeknadsnummer);
}