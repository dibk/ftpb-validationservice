﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using System;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg
{
    public interface IGjenstaaendeArbeiderValidator
    {
        ValidationResult ValidationResult { get; set; }
        string _entityXPath { get; }
        ValidationResult Validate(GjenstaaendeArbeider utfallSvar, bool? gjelderHeleTiltaket);
    }
}