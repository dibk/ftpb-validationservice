﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;

public interface IMetadataByggValidator
{
    ValidationResult ValidationResult { get; set; }
    ValidationResult Validate(Metadata metadata);
}