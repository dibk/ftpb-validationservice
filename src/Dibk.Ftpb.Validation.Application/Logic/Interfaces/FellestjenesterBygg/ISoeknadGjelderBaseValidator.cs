﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;

public interface ISoeknadGjelderBaseValidator
{
    ValidationResult ValidationResult { get; set; }
    string _entityXPath { get; }

    ValidationResult Validate(SoeknadGjelderBase soeknadGjelder, Kodeliste ansvarForByggesaken);

    string[] Tiltakstypes { get; }
}