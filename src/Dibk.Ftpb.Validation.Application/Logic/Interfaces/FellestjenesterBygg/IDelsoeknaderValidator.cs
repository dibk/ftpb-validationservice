﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;

public interface IDelsoeknaderValidator
{
    string _entityXPath { get; }
    ValidationResult ValidationResult { get; set; }
    ValidationResult Validate(Delsoeknad delsoeknad);
}