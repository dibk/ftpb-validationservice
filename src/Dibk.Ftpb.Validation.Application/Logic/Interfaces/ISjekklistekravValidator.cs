﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface ISjekklistekravValidator
    {
        //string ruleXmlElement { get; }
        ValidationResult ValidationResult { get; }
        ValidationResult Validate(string processCategory, ServiceDomain serviceDomain, Sjekklistekrav[] sjekklistekrav, IChecklistService checklistService, List<string> tiltakstyperISkjema);
    }
}
