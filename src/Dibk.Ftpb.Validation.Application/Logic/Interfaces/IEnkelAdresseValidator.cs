﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IEnkelAdresseValidator 
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(EnkelAdresse enkelAdresse = null);
        void ValidateEntityFields(EnkelAdresse adresse);
    }
}