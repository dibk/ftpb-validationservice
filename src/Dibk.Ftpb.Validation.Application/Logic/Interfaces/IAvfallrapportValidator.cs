﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IAvfallrapportValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(Avfallrapport avfallrapport = null);
        string _entityXPath { get; }

    }
}
