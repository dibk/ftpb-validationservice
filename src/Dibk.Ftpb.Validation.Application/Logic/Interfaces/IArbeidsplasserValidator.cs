﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using System.Collections.Generic;
using Dibk.Ftpb.Common.Datamodels.Parts;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IArbeidsplasserValidator
    {
        //string ruleXmlElement { get; }
        ValidationResult ValidationResult { get; set; }
        
        //TODO: Fix this
        ValidationResult Validate(Arbeidsplasser arbeidsplasser, List<string> attachments = null);
        ValidationResult Validate(Arbeidsplasser arbeidsplasser, IEnumerable<Sjekklistekrav> sjekkliste, List<string> attachments = null);
        
        //event EventHandler<ValidationResult> RulesAdded;
    }
}