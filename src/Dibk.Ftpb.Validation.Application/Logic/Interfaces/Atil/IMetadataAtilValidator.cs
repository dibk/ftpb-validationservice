using Dibk.Ftpb.Common.Datamodels.Parts.Atil;
using Dibk.Ftpb.Validation.Application.Reporter;



namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces.Atil
{
    public interface IMetadataAtilValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(MetadataAtilSamtykke metadata);
    }
}