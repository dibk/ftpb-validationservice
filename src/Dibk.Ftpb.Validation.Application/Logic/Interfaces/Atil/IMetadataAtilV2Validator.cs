using Dibk.Ftpb.Common.Datamodels.Parts.Atil;
using Dibk.Ftpb.Validation.Application.Reporter;



namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces.Atil
{
    public interface IMetadataAtilV2Validator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(MetadataAtilSupplering metadata);
    }
}