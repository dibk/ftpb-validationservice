﻿using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IFakturamottakerValidator
    {
        //string ruleXmlElement { get; }
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(ArbeidstilsynetsSamtykkeV2 validationForm, Fakturamottaker fakturamottaker = null);
        void ValidateEntityFields(ArbeidstilsynetsSamtykkeV2 validationForm, Fakturamottaker fakturamottaker);
    }
}