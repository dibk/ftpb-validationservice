﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IAktoerValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(Aktoer tiltakshaver = null, string messageParameter = null);

        string _entityXPath { get; }

    }
}