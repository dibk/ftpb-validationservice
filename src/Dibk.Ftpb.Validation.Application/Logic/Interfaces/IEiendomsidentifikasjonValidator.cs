﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IEiendomsidentifikasjonValidator
    {
        string _entityXPath { get; }
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(Eiendomsidentifikasjon matrikkel);
    }
}