﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface ISubmitterValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(Aktoer tiltakshaver = null, Aktoer ansvarligsoeker = null, string loggedInUser = null, List<ValidationMessage> validationMessages = null);
    }
}
