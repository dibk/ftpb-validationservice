﻿using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IVedleggValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(ValidationInput validationInput, object formdata, string loggedInUser = null, List<ValidationMessage> validationMessages = null);
    }
}
