﻿using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using Dibk.Ftpb.Validation.Application.Reporter;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface ISubmitterPlanValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(AktoerPlan forslagsstiller, AktoerPlan plankonsulent = null, string loggedInUser = null, List<ValidationMessage> validationMessages = null);
    }
}
