﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IAvfalldisponeringValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(Avfalldisponering avfalldisponering = null, string fraksjonKodebeskrivelse = null);
        string _entityXPath { get; }

    }
}
