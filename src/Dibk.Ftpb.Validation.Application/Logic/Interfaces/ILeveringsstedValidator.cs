﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface ILeveringsstedValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(Leveringssted leveringssted, string fraksjonKodebeskrivelse = null);
    }
}