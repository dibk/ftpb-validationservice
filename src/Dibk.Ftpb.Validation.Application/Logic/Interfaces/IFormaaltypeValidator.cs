﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IFormaaltypeValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(Formaaltype formaaltype = null);
    }
}
