﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IAvfallklasseDelsumValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(AvfallsklasseDelsum avfallsKlasseDelSum = null);
        string _entityXPath { get; }

    }
}
