﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.Interfaces
{
    public interface IAvfallplanValidator
    {
        ValidationResult ValidationResult { get; set; }
        ValidationResult Validate(Avfallplan avfallplan = null);
        string _entityXPath { get; }
    }
}
