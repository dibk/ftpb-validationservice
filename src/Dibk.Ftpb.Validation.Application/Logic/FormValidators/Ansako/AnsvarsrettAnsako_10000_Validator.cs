﻿using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Utils;
using MatrikkelWSClient.Services;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.FormValidators.Ansako
{
    [FormData(DataFormatId = "10000", DataFormatVersion = "1", XsdFileName = "ansvarsrettAnsako", ProcessCategory = "EA", ServiceDomain = ServiceDomain.FTB)]
    public class AnsvarsrettAnsako_10000_Validator : FormValidatorBase, IFormValidator, IFormWithChecklistAnswers
    {
        private readonly IValidationMessageComposer _validationMessageComposer;
        private readonly IChecklistService _checklistService;
        private readonly IMatrikkelProvider _matrikkelProvider;
        private readonly IDecryptionFactory _decryptionFactory;
        private readonly ICodeListService _codeListService;
        private readonly IPostalCodeService _postalCodeService;

        private string[] _tiltakstypes;
        private List<EntityValidatorNode> _entitiesNodeList;

        //AnsvarligSoeker
        private IAktoerValidator _ansvarligSoekerValidator;
        private IEnkelAdresseValidator _ansvarligSoekerEnkelAdresseValidator;
        private IKodelisteValidator _ansvarligSoekerPartstypeValidator;
        private IKontaktpersonValidator _ansvarligSoekerKontaktpersonValidator;

        //*Ansvarsrett
        private AnsvarsrettValidator _AnsvarsrettValidator;
        //**foretak
        private IAktoerValidator _foretakValidator;
        private IKontaktpersonValidator _foretakKontaktpersonValidator;
        private IKodelisteValidator _foretakPartstypeValidator;
        private IEnkelAdresseValidator _foretakEnkelAdresseValidator;
        //**Ansavarområde        
        private IAnsvarsomraadeValidator _ansvarsomraadeValidator;
        private IKodelisteValidator _funksjonValidator;
        private IKodelisteValidator _tiltaksklasseValidator;

        //EiendomByggested
        private IEiendomsidentifikasjonValidator _eiendomsidentifikasjonValidator;
        private IEiendomsAdresseValidator _eiendomsAdresseValidator;
        private IEiendomByggestedValidator _eiendomByggestedValidator;

        //KommuneSaksnummer
        private ISaksnummerValidator _kommunensSaksnummerValidator;

        public AnsvarsrettAnsako_10000_Validator(IValidationMessageComposer validationMessageComposer, ICodeListService codeListService
            , IPostalCodeService postalCodeService, IChecklistService checklistService, IMatrikkelProvider matrikkelProvider, IDecryptionFactory decryptionFactory)
            : base(validationMessageComposer, checklistService)
        {
            _validationMessageComposer = validationMessageComposer;
            _checklistService = checklistService;
            _matrikkelProvider = matrikkelProvider;
            _decryptionFactory = decryptionFactory;
            _codeListService = codeListService;
            _postalCodeService = postalCodeService;

            _entitiesNodeList = new List<EntityValidatorNode>();
            _tiltakstypes = new string[] { };

            base.InitializeFormValidator<AnsakoAnsvarsrett>();
        }

        protected override void InitializeValidatorConfig()
        {
            //AnsvarligSoeker
            var ansvarligSoekervalidatorNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 01, EnumId = EntityValidatorEnum.AnsvarligSoekerValidator, ParentID = null},
                new () {NodeId = 02, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 01},
                new () {NodeId = 03, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 01},
                new () {NodeId = 04, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 01}
            };
            _entitiesNodeList.AddRange(ansvarligSoekervalidatorNodeList);

            var soeknadssystemetsReferanseNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 05, EnumId = EntityValidatorEnum.AnsvarsrettValidator, ParentID = null},
                //foretak
                new () {NodeId = 06, EnumId = EntityValidatorEnum.ForetakValidator, ParentID = 05},
                new () {NodeId = 07, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 06},
                new () {NodeId = 08, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 06},
                new () {NodeId = 09, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 06},
                //ansvarsområde
                new () {NodeId = 10, EnumId = EntityValidatorEnum.AnsvarsomraadeValidator, ParentID = 05},
                new () {NodeId = 11, EnumId = EntityValidatorEnum.FunksjonValidator, ParentID = 10},
                new () {NodeId = 12, EnumId = EntityValidatorEnum.TiltaksklasseValidator, ParentID = 10},
            };
            _entitiesNodeList.AddRange(soeknadssystemetsReferanseNodeList);
            // EiendomByggested
            var eiendombyggestedNodeList = new List<EntityValidatorNode>()
            {
                new() { NodeId = 13, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
                new() { NodeId = 14, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 13 },
                new() { NodeId = 15, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 13 },
            };
            _entitiesNodeList.AddRange(eiendombyggestedNodeList);

            //Kommunenes Saksnummer
            var kommunenesSaksnummerValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 16, EnumId = EntityValidatorEnum.KommunensSaksnummerValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(kommunenesSaksnummerValidatorNodeList);
        }


        protected override IEnumerable<string> GetFormTiltakstyper()
        {
            return _tiltakstypes;
        }

        protected override void InstantiateValidators()
        {
            var tree = EntityValidatorTree.BuildTree(_entitiesNodeList);

            //*AnsvarligSoeker
            _ansvarligSoekerKontaktpersonValidator = new KontaktpersonValidator(tree, 02);
            _ansvarligSoekerPartstypeValidator = new PartstypeValidator(tree, 03, _codeListService);
            _ansvarligSoekerEnkelAdresseValidator = new EnkelAdresseValidator(tree, 04, _postalCodeService);
            var ansvarligSoekerPartstypes = new[] { "Foretak", "Organisasjon" };
            _ansvarligSoekerValidator = new AnsvarligSoekerValidator(tree, _ansvarligSoekerEnkelAdresseValidator, _ansvarligSoekerKontaktpersonValidator, _ansvarligSoekerPartstypeValidator, _codeListService, _decryptionFactory, ansvarligSoekerPartstypes);

            //*Ansvarsrett
            //**Ansvarsområde
            _funksjonValidator = new FunksjonValidator(tree, 11, _codeListService);
            _tiltaksklasseValidator = new TiltaksklasseValidator(tree, 12, _codeListService, true);
            _ansvarsomraadeValidator = new AnsvarsomraadeValidator(tree, _funksjonValidator, _tiltaksklasseValidator);
            //**foretak
            _foretakPartstypeValidator = new PartstypeValidator(tree, 07, _codeListService);
            _foretakEnkelAdresseValidator = new EnkelAdresseValidator(tree, 08, _postalCodeService);
            _foretakKontaktpersonValidator = new KontaktpersonValidator(tree, 09);

            var foretakPartstypes = new[] { "Foretak" };
            _foretakValidator = new ForetakValidator(tree, _foretakEnkelAdresseValidator, _foretakKontaktpersonValidator, _foretakPartstypeValidator, _codeListService, _decryptionFactory, foretakPartstypes);

            _AnsvarsrettValidator = new AnsvarsrettValidator(tree, _foretakValidator, _ansvarsomraadeValidator);

            //EiendomByggested
            _eiendomsAdresseValidator = new EiendomsAdresseValidator(tree);
            _eiendomsidentifikasjonValidator = new EiendomsidentifikasjonValidator(tree, _codeListService, _matrikkelProvider);
            _eiendomByggestedValidator = new EiendomByggestedValidator(tree, _eiendomsAdresseValidator, _eiendomsidentifikasjonValidator, _matrikkelProvider);

            //Kommunens saksnummer
            _kommunensSaksnummerValidator = new KommunensSaksnummerValidator(tree);

        }

        protected override void Validate(ValidationInput validationInput)
        {

            //var xmlValidationResult = base.ValidateXmlStructure(validationInput.FormData);
            //if (!string.IsNullOrEmpty(xmlValidationResult))
            //{
            //    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.xml, new[] { xmlValidationResult });
            //    return;
            //}

            var form = SerializeUtil.DeserializeFromString<AnsakoAnsvarsrett>(validationInput.FormData);

            ValidateEntityFields(form);

            var ansvarsrettResult = _AnsvarsrettValidator.Validate(form.Ansvarsretts);
            AccumulateValidationMessages(ansvarsrettResult.ValidationMessages);

            var ansvarligSoekerValidationResult = _ansvarligSoekerValidator.Validate(form.AnsvarligSoeker);
            AccumulateValidationMessages(ansvarligSoekerValidationResult.ValidationMessages);

            var index = GetArrayIndex(form.eiendomByggested);
            for (int i = 0; i < index; i++)
            {
                Eiendom eiendom = Helpers.ObjectIsNullOrEmpty(form.eiendomByggested) ? null : form.eiendomByggested[i];
                var eiendomsResult = _eiendomByggestedValidator.Validate(eiendom);
                AccumulateValidationMessages(eiendomsResult.ValidationMessages, i);
            }

            var kommunensSaksnummerValidatorResult = _kommunensSaksnummerValidator.Validate(form.KommunensSaksnummer);
            AccumulateValidationMessages(kommunensSaksnummerValidatorResult.ValidationMessages);

        }

        public void ValidateEntityFields(AnsakoAnsvarsrett form)
        {
            if (string.IsNullOrEmpty(form.Prosjektnavn))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.prosjektnavn);

            if (string.IsNullOrEmpty(form.Prosjektnr))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.prosjektnr);
        }


        protected override void DefineValidationRules()
        {
            // Local
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.xml);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.prosjektnavn);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.prosjektnr);

            //AnsvarligSoeker
            AccumulateValidationRules(_ansvarligSoekerValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarligSoekerEnkelAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarligSoekerPartstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarligSoekerKontaktpersonValidator.ValidationResult.ValidationRules);
            //*Ansvarsrett
            AccumulateValidationRules(_AnsvarsrettValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_foretakValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_foretakKontaktpersonValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_foretakPartstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_foretakEnkelAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarsomraadeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_funksjonValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_tiltaksklasseValidator.ValidationResult.ValidationRules);

            //EiendomByggested
            AccumulateValidationRules(_eiendomsidentifikasjonValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_eiendomsAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_eiendomByggestedValidator.ValidationResult.ValidationRules);

            //Kommunens saksnummer
            AccumulateValidationRules(_kommunensSaksnummerValidator.ValidationResult.ValidationRules);

        }

        public List<ChecklistAnswer> GetChecklistAnswersFromStaticPartOfDatamodel(ValidationInput validationInput)
        {
            //throw new NotImplementedException();
            return new List<ChecklistAnswer>();

        }

        protected override void CustomFormValidation(ValidationInput validationInput)
        {
            //Not implemented for this form
        }

        protected override DateTime? GetFormValidationDate()
        {
            return DateTime.Now;
        }
    }
}
