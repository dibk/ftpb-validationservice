using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators.Plan;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterPlan;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterPlan;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.FormValidators.FellestjenesterPlan
{
    [FormData(DataFormatId = "11000", DataFormatVersion = "20", XsdFileName = "planvarsel", ProcessCategory = "PL", ServiceDomain = ServiceDomain.PLAN)]
    public class Planvarsel_11000_2_Validator : FormValidatorBase, IFormValidator
    {
        private readonly IValidationMessageComposer _validationMessageComposer;

        private string[] _tiltakstypes;
        private List<EntityValidatorNode> _entitiesNodeList;
        private PlanvarselV2 _validationForm { get; set; }

        //Metadata
        private IMetadataPlanValidator _metadataPlanValidator;

        //Forslagsstiller
        private IAktoerPlanValidator _forslagsstillerValidator;
        private IEnkelAdresseValidator _forslagsstillerEnkelAdresseValidator;
        private IKodelisteValidator _forslagsstillerPartstypeValidator;

        //beroerteParter
        private IKodelisteValidator _beroertPartPartstypeValidator;
        private IEnkelAdresseValidator _beroertParterEnkelAdresseValidator;
        private IBeroertPartValidator _beroerteParterValidator;

        //Planforslag
        private IPlanforslagValidator _planforslagValidator;
        private IKodelisteValidator _plantypeKodelisteValidator;
        private ISaksnummerValidator _saksnummerPlanValidator;

        //EiendomByggested
        private IEiendomsidentifikasjonValidator _eiendomsidentifikasjonPlanValidator;
        private IEiendomByggestedValidator _eiendomByggestedPlanValidator;

        //Plankonsulent
        private IAktoerPlanValidator _plankonsulentValidator;
        private IEnkelAdresseValidator _plankonsulentEnkelAdresseValidator;
        private IKodelisteValidator _plankonsulentPartstypeValidator;

        private readonly ICodeListService _codeListService;
        private readonly IPostalCodeService _postalCodeService;
        private readonly IDecryptionFactory _decryptionFactory;

        //Vedlegg
        private IVedleggValidator _vedleggValidator;

        //Avsender
        private ISubmitterPlanValidator _submitterPlanValidator;


        public Planvarsel_11000_2_Validator(IValidationMessageComposer validationMessageComposer, ICodeListService codeListService, IPostalCodeService postalCodeService, IDecryptionFactory decryptionFactory) : base(validationMessageComposer)
        {
            _validationMessageComposer = validationMessageComposer;
            _codeListService = codeListService;
            _postalCodeService = postalCodeService;
            _decryptionFactory = decryptionFactory;

            _entitiesNodeList = new List<EntityValidatorNode>();
            _tiltakstypes = new string[] { };

            base.InitializeFormValidator<PlanvarselV2>();
        }

        protected override void InitializeValidatorConfig()
        {
            // EiendomByggested
            var eiendombyggestedNodeList = new List<EntityValidatorNode>()
                {
                    new() { NodeId =1, EnumId = EntityValidatorEnum.EiendomByggestedPlanValidator, ParentID = null },
                    new() { NodeId = 3, EnumId = EntityValidatorEnum.EiendomsidentifikasjonPlanValidator, ParentID = 1 },
                };
            _entitiesNodeList.AddRange(eiendombyggestedNodeList);
            //BeroerteParter
            var beroerteParterNodeList = new List<EntityValidatorNode>()
                {
                    new() { NodeId = 4, EnumId = EntityValidatorEnum.BeroerteParterValidator, ParentID = null },
                    new() { NodeId =5, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 4 },
                    new() { NodeId = 6, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 4 },
                };
            _entitiesNodeList.AddRange(beroerteParterNodeList);
            //Metadata
            var metadataNodeList = new List<EntityValidatorNode>()
            {
                new() { NodeId = 7, EnumId = EntityValidatorEnum.MetadataPlanValidator, ParentID = null }
            };
            _entitiesNodeList.AddRange(metadataNodeList);
            //Forslagsstiller
            var forslagsstillerNodeList = new List<EntityValidatorNode>()
            {
                new() { NodeId = 8, EnumId = EntityValidatorEnum.ForslagsstillerValidator, ParentID = null },
                new() { NodeId = 9, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 8 },
                new() { NodeId = 10, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 8 },
            };
            _entitiesNodeList.AddRange(forslagsstillerNodeList);
            //Plankonsulent
            var plankonsulentNodeList = new List<EntityValidatorNode>()
            {
                new() { NodeId = 11, EnumId = EntityValidatorEnum.PlankonsulentValidator, ParentID = null },
                new() { NodeId = 12, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 11 },
                new() { NodeId = 13, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 11 },
            };
            _entitiesNodeList.AddRange(plankonsulentNodeList);
            //Planforslag
            var planforslagNodeList = new List<EntityValidatorNode>()
            {
                new() { NodeId = 14, EnumId = EntityValidatorEnum.PlanforslagValidator, ParentID = null },
                new() { NodeId = 15, EnumId = EntityValidatorEnum.PlantypeValidator, ParentID = 14 },
                new() { NodeId = 16, EnumId = EntityValidatorEnum.SaksnummerPlanValidator, ParentID = 14 },
            };
            _entitiesNodeList.AddRange(planforslagNodeList);
            //Vedlegg
            var vedleggValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 17, EnumId = EntityValidatorEnum.VedleggPlanvarselValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(vedleggValidatorNodeList);

            //Avsender
            var submitterPlanValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 18, EnumId = EntityValidatorEnum.SubmitterPlanValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(submitterPlanValidatorNodeList);
        }

        protected override IEnumerable<string> GetFormTiltakstyper()
        {
            return _tiltakstypes;
        }

        protected override void InstantiateValidators()
        {
            var tree = EntityValidatorTree.BuildTree(_entitiesNodeList);

            //EiendomByggested
            _eiendomsidentifikasjonPlanValidator = new EiendomsidentifikasjonPlanValidator(tree, _codeListService);
            _eiendomByggestedPlanValidator = new EiendomByggestedPlanValidator(tree, _eiendomsidentifikasjonPlanValidator);

            //BeroerteParter
            _beroertPartPartstypeValidator = new PartstypeValidator(tree, 5, _codeListService);
            _beroertParterEnkelAdresseValidator = new EnkelAdresseValidator(tree, 6, _postalCodeService,true);
            _beroerteParterValidator = new BeroerteParterValidator(tree, _beroertPartPartstypeValidator, _beroertParterEnkelAdresseValidator, _decryptionFactory);

            //Metadata
            _metadataPlanValidator = new MetadataPlanValidator(tree);

            //Forslagsstiller
            _forslagsstillerPartstypeValidator = new PartstypeValidator(tree, 9, _codeListService);
            _forslagsstillerEnkelAdresseValidator = new EnkelAdresseValidator(tree, 10, _postalCodeService,true);
            _forslagsstillerValidator = new ForslagsstillerValidator(tree, _forslagsstillerEnkelAdresseValidator, _forslagsstillerPartstypeValidator, _codeListService, _decryptionFactory);

            //Plankonsulent
            _plankonsulentPartstypeValidator = new PartstypeValidator(tree, 12, _codeListService);
            _plankonsulentEnkelAdresseValidator = new EnkelAdresseValidator(tree, 13, _postalCodeService,true);
            var plankonsulentSoekerPartstypes = new[] { "Foretak", "Organisasjon", "Offentlig myndighet" };
            _plankonsulentValidator = new PlankonsulentValidator(tree, _plankonsulentEnkelAdresseValidator, _plankonsulentPartstypeValidator, _codeListService, _decryptionFactory, plankonsulentSoekerPartstypes);

            //Planforslag
            _plantypeKodelisteValidator = new PlantypeValidator(tree, 15, _codeListService);
            _saksnummerPlanValidator = new SaksnummerPlanValidator(tree);
            _planforslagValidator = new PlanforslagValidator(tree, _plantypeKodelisteValidator, _saksnummerPlanValidator);

            //Vedlegg
            _vedleggValidator = new VedleggPlanvarselValidator(tree);

            //Avsender
            _submitterPlanValidator = new SubmitterPlanValidator(tree, _decryptionFactory);
        }

        protected override void Validate(ValidationInput validationInput)
        {
            var form = SerializeUtil.DeserializeFromString<PlanvarselV2>(validationInput.FormData, false);
            ValidateEntityFields(form);

            var index = GetArrayIndex(form.EiendomByggested);
            for (int i = 0; i < index; i++)
            {
                Eiendom eiendom = Helpers.ObjectIsNullOrEmpty(form.EiendomByggested) ? null : form.EiendomByggested[i];
                var eiendomsResult = _eiendomByggestedPlanValidator.Validate(eiendom);
                AccumulateValidationMessages(eiendomsResult.ValidationMessages, i);
            }

            if (form.BeroerteParter.Length > 0)
            {
                var beroerteParterIndex = GetArrayIndex(form.BeroerteParter);
                for (int i = 0; i < beroerteParterIndex; i++)
                {
                    Beroertpart beroertPart = Helpers.ObjectIsNullOrEmpty(form.BeroerteParter) ? null : form.BeroerteParter[i];
                    var beroertPartResult = _beroerteParterValidator.Validate(beroertPart);
                    AccumulateValidationMessages(beroertPartResult.ValidationMessages, i);
                }

            }

            var metadataResult = _metadataPlanValidator.Validate(form.Metadata);
            AccumulateValidationMessages(metadataResult.ValidationMessages);

            var planforslagResult = _planforslagValidator.Validate(form.Planforslag);
            AccumulateValidationMessages(planforslagResult.ValidationMessages);

            var vedleggValidationResult = _vedleggValidator.Validate(validationInput, form, validationInput.AuthenticatedSubmitter);
            AccumulateValidationMessages(vedleggValidationResult.ValidationMessages);

            if (form.Plankonsulent != null)
            {
                var plankonsulentResult = _plankonsulentValidator.Validate(form.Plankonsulent);
                AccumulateValidationMessages(plankonsulentResult.ValidationMessages);
            }

            var forslagsstillerResult = _forslagsstillerValidator.Validate(form.Forslagsstiller);
            AccumulateValidationMessages(forslagsstillerResult.ValidationMessages);

            ValidateDataRelations(form, validationInput);
        }

        private void ValidateDataRelations(PlanvarselV2 form, ValidationInput validationInput)
        {
            if (!Helpers.ObjectIsNullOrEmpty(form.Plankonsulent))
            {
                RemoveNotRelevantMessagesFromValidationResult("/forslagsstiller/foedselsnummer");
            }

            if (!IsAnyValidationMessagesWithXpath("/forslagsstiller/partstype/kodeverdi") || !IsAnyValidationMessagesWithXpath("/plankonsulent/partstype/kodeverdi"))
            {
                var submitterValidationResult = _submitterPlanValidator.Validate(form.Forslagsstiller, form.Plankonsulent, validationInput.AuthenticatedSubmitter);
                AccumulateValidationMessages(submitterValidationResult.ValidationMessages);
            }
        }

        protected override DateTime? GetFormValidationDate()
        {
            return DateTime.Now;
        }

        protected override void DefineValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.xml);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.beroerteParter);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommunenavn);

            //EiendomByggested
            AccumulateValidationRules(_eiendomByggestedPlanValidator.ValidationResult.ValidationRules);

            //BeroerteParter
            AccumulateValidationRules(_beroertPartPartstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_beroertParterEnkelAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_beroerteParterValidator.ValidationResult.ValidationRules);

            //Metadata
            AccumulateValidationRules(_metadataPlanValidator.ValidationResult.ValidationRules);

            //Forslagsstiller
            AccumulateValidationRules(_forslagsstillerEnkelAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_forslagsstillerPartstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_forslagsstillerValidator.ValidationResult.ValidationRules);

            //Plankonsulent
            AccumulateValidationRules(_plankonsulentEnkelAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_plankonsulentPartstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_plankonsulentValidator.ValidationResult.ValidationRules);

            //Planforslag
            AccumulateValidationRules(_planforslagValidator.ValidationResult.ValidationRules);

            //Vedlegg
            AccumulateValidationRules(_vedleggValidator.ValidationResult.ValidationRules);

            //Avsender
            AccumulateValidationRules(_submitterPlanValidator.ValidationResult.ValidationRules);
        }

        public void ValidateEntityFields(PlanvarselV2 form)
        {
            if (form.BeroerteParter?.Length < 1)
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.beroerteParter);

            if (string.IsNullOrEmpty(form.Kommunenavn))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommunenavn);

        }

        protected override void CustomFormValidation(ValidationInput validationInput)
        {
            //throw new NotImplementedException();
        }
    }
}
