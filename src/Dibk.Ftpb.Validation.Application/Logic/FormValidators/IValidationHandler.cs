﻿using Dibk.Ftpb.Validation.Application.Reporter;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Checklist;
using Dibk.Ftpb.Validation.Application.Models.Web;

namespace Dibk.Ftpb.Validation.Application.Logic.FormValidators
{
    public interface IValidationHandler
    {
        ValidationResult Validate(string dataFormatId, string dataFormatVersion, ValidationInput validationInput);
        Task<ValidationRule[]> GetformRulesAsync(string dataFormatId, string dataFormatVersion);
        FormDataAttribute formDataAttribute();
        FormDataAttribute GetFormDataAttribute(string dataFormatId, string dataFormatVersion);
    }
}