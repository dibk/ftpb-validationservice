using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Utils;
using MatrikkelWSClient.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.FormValidators.FellestjenesterBygg
{
    [FormData(DataFormatId = "10005", DataFormatVersion = "4", XsdFileName = "ferdigattestV4", ProcessCategory = "FA", ServiceDomain = ServiceDomain.FTB)]
    public class Ferdigattest_10005_4_Validator : FormValidatorBase, IFormValidator
    {
        private readonly IValidationMessageComposer _validationMessageComposer;
        private readonly ICodeListService _codeListService;
        private readonly IMatrikkelProvider _matrikkelProvider;
        private readonly IChecklistService _checklistService;
        private readonly IPostalCodeService _postalCodeService;
        private readonly IDecryptionFactory _decryptionFactory;
        private readonly string _formName = "ferdigattest";

        private string[] _tiltakstypes;
        private List<EntityValidatorNode> _entitiesNodeList;

        //EiendomByggested
        private IEiendomsidentifikasjonValidator _eiendomsidentifikasjonValidator;
        private IEiendomsAdresseValidator _eiendomsAdresseValidator;
        private IEiendomByggestedValidator _eiendomByggestedValidator;

        //Kommunens Saksnummer
        private ISaksnummerValidator _kommunensSaksnummerValidator;

        //Metadata
        private IMetadataByggValidator _metadataByggValidator;
        private IKodelisteValidator _foretrukketspraakTypeValidator;

        //GenerelleVilkaar
        private IGenerelleVilkaarValidator _generelleVilkaarValidator;

        //Ansvar for byggesaken
        private IKodelisteValidator _ansvarForByggesakenValidator;

        //AnsvarligSoeker
        private AnsvarligSoekerValidator _ansvarligSoekerValidator;
        private IEnkelAdresseValidator _ansvarligSoekerEnkelAdresseValidator;
        private IKodelisteValidator _ansvarligSoekerPartstypeValidator;
        private IKontaktpersonValidator _ansvarligSoekerKontaktpersonValidator;

        //Tiltakshaver
        private TiltakshaverValidator _tiltakshaverValidator;
        private IEnkelAdresseValidator _tiltakshaverEnkelAdresseValidator;
        private IKodelisteValidator _tiltakshaverPartstypeValidator;
        private IKontaktpersonValidator _tiltakshaverKontaktpersonValidator;

        //SoeknadGjelder
        private ISoeknadGjelderBaseValidator _soeknadGjelderValidator;
        private IKodelisteValidator _soeknadTypeMedAnsvarsrettValidator;
        private IKodelisteValidator _soeknadTypeUtenAnsvarsrettValidator;

        //UtfallBesvarelse
        private IUtfallBesvarelseValidator _utfallBesvarelseFAValidator;
        private IKodelisteValidator _utfallTypeValidator;
        private IUtloestFraSjekkpunktValidator _utloestFraSjekkpunktValidator;
        private IKodelisteValidator _temaKodelisteValidator;
        private IKodelisteValidator _vedleggsTypeValidator;
        private IVedleggslisteValidator _vedleggslisteValidator;

        //KravFerdigattest
        private KravFerdigattestValidator _kravFerdigattestValidator;

        //Varmesystem
        private IVarmesystemValidator _varmesystemValidator;
        private IKodelisteValidator _energiforsyningValidator;
        private IKodelisteValidator _varmefordelingValidator;

        //Vedlegg
        private VedleggFAValidator _vedleggValidator;

        public Ferdigattest_10005_4_Validator(
            IValidationMessageComposer validationMessageComposer,
            ICodeListService codeListService,
            IMatrikkelProvider matrikkelProvider,
            IChecklistService checklistService,
            IPostalCodeService postalCodeService,
            IDecryptionFactory decryptionFactory
            ) : base(validationMessageComposer, checklistService)
        {
            _validationMessageComposer = validationMessageComposer;
            _codeListService = codeListService;
            _matrikkelProvider = matrikkelProvider;
            _checklistService = checklistService;
            _postalCodeService = postalCodeService;
            _decryptionFactory = decryptionFactory;

            _entitiesNodeList = new List<EntityValidatorNode>();
            _tiltakstypes = new string[] { };
            base.InitializeFormValidator<Ferdigattest>();
        }

        protected override void InitializeValidatorConfig()
        {
            // EiendomByggested
            List<EntityValidatorNode> eiendombyggestedNodeList = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 1 },
                new() { NodeId = 3, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 1 },
            };
            _entitiesNodeList.AddRange(eiendombyggestedNodeList);

            //Kommunenes Saksnummer
            var kommunenesSaksnummerValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 4, EnumId = EntityValidatorEnum.KommunensSaksnummerValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(kommunenesSaksnummerValidatorNodeList);

            //Metadata
            var metadataByggValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 5, EnumId = EntityValidatorEnum.MetadataByggValidator, ParentID = null},
                new() {NodeId = 6, EnumId = EntityValidatorEnum.ForetrukketspraakTypeValidator, ParentID = 5}
            };
            _entitiesNodeList.AddRange(metadataByggValidatorNodeList);

            //GenerelleVilkaar
            _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 7, EnumId = EntityValidatorEnum.GenerelleVilkaarValidator, ParentID = null}
        });
            //Ansvar for byggesaken
            var ansvarForByggesakenValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 8, EnumId = EntityValidatorEnum.AnsvarForByggesakenValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(ansvarForByggesakenValidatorNodeList);

            //AnsvarligSoeker
            var ansvarligSoekervalidatorNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 9, EnumId = EntityValidatorEnum.AnsvarligSoekerValidator, ParentID = null},
                new () {NodeId = 10, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 9},
                new () {NodeId = 11, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 9},
                new () {NodeId = 12, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 9}
            };
            _entitiesNodeList.AddRange(ansvarligSoekervalidatorNodeList);

            //Tiltakshaver
            _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() { NodeId = 13, EnumId = EntityValidatorEnum.TiltakshaverValidator, ParentID = null },
            new() { NodeId = 14, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 13 },
            new() { NodeId = 15, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 13 },
            new() { NodeId = 16, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 13 }
        });

            //SoeknadGjelder
            _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 17, EnumId = EntityValidatorEnum.SoeknadGjelderValidator, ParentID = null},
            new() {NodeId = 18, EnumId = EntityValidatorEnum.TypeValidator, ParentID = 17},
            new() {NodeId = 19, EnumId = EntityValidatorEnum.SoeknadTypeUtenAnsvarsrettValidator, ParentID = 17},
        });

            //UtfallBesvarelse
            _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 20, EnumId = EntityValidatorEnum.UtfallBesvarelseFAValidator, ParentID = null},
            new() {NodeId = 21, EnumId = EntityValidatorEnum.UtfallTypeValidator, ParentID = 20},
            new() {NodeId = 22, EnumId = EntityValidatorEnum.UtloestFraSjekkpunktValidator, ParentID = 20},
            new() {NodeId = 23, EnumId = EntityValidatorEnum.TemaValidator, ParentID = 20},
            new() {NodeId = 24, EnumId = EntityValidatorEnum.VedleggslisteValidator, ParentID = 20},
            new() {NodeId = 25, EnumId = EntityValidatorEnum.VedleggsTypeValidator, ParentID = 24},
        });
            //KravFerdigattest
            _entitiesNodeList.AddRange(new List<EntityValidatorNode>
                {
                new() {NodeId = 27, EnumId = EntityValidatorEnum.KravFerdigattestValidator, ParentID = null}
            });

            //Varmesystem
            _entitiesNodeList.AddRange(new List<EntityValidatorNode>
            {
                new() {NodeId = 28, EnumId = EntityValidatorEnum.VarmesystemValidator, ParentID = null},
                new() {NodeId = 29, EnumId = EntityValidatorEnum.EnergiforsyningValidator, ParentID = 28},
                new() {NodeId = 30, EnumId = EntityValidatorEnum.VarmefordelingValidator, ParentID = 28},
            });

            //Vedlegg
            var vedleggValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 31, EnumId = EntityValidatorEnum.VedleggFAValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(vedleggValidatorNodeList);
        }

        protected override IEnumerable<string> GetFormTiltakstyper()
        {
            return _tiltakstypes;
        }
        protected override void InstantiateValidators()
        {
            var tree = EntityValidatorTree.BuildTree(_entitiesNodeList);

            //EiendomByggested
            _eiendomsAdresseValidator = new EiendomsAdresseValidator(tree);
            _eiendomsidentifikasjonValidator = new EiendomsidentifikasjonValidator(tree, _codeListService, _matrikkelProvider);
            _eiendomByggestedValidator = new EiendomByggestedValidator(tree, _eiendomsAdresseValidator, _eiendomsidentifikasjonValidator, _matrikkelProvider, true);

            //Kommunens Saksnummer
            _kommunensSaksnummerValidator = new KommunensSaksnummerValidator(tree);

            //Metadata
            _foretrukketspraakTypeValidator = new ForetrukketspraakTypeValidator(tree, 6, _codeListService, true);
            _metadataByggValidator = new MetadataByggValidator(tree, _foretrukketspraakTypeValidator);

            //GenerelleVilkaar
            _generelleVilkaarValidator = new GenerelleVilkaarValidator(tree);

            //Ansvar for byggesaken
            _ansvarForByggesakenValidator = new AnsvarForByggesakenValidator(tree, 8, _codeListService);

            //*AnsvarligSoeker
            _ansvarligSoekerKontaktpersonValidator = new KontaktpersonValidator(tree, 10);
            _ansvarligSoekerPartstypeValidator = new PartstypeValidator(tree, 11, _codeListService);
            _ansvarligSoekerEnkelAdresseValidator = new EnkelAdresseValidator(tree, 12, _postalCodeService);
            _ansvarligSoekerValidator = new AnsvarligSoekerValidator(tree, _ansvarligSoekerEnkelAdresseValidator, _ansvarligSoekerKontaktpersonValidator, _ansvarligSoekerPartstypeValidator, _codeListService, _decryptionFactory, isDependentOnAnsvarForByggesaken: true);

            //Tiltakshaver
            _tiltakshaverKontaktpersonValidator = new KontaktpersonValidator(tree, 14);
            _tiltakshaverPartstypeValidator = new PartstypeValidator(tree, 15, _codeListService);
            _tiltakshaverEnkelAdresseValidator = new EnkelAdresseValidator(tree, 16, _postalCodeService);
            _tiltakshaverValidator = new TiltakshaverValidator(tree, _tiltakshaverEnkelAdresseValidator, _tiltakshaverKontaktpersonValidator, _tiltakshaverPartstypeValidator, _codeListService, _decryptionFactory, isDependentOnAnsvarForByggesaken: true);

            //SoeknadGjelder
            _soeknadTypeMedAnsvarsrettValidator = new TypeValidator(tree, 18, _codeListService,
                preconditionKodeverdi: "/ansvarForByggesaken/kodeverdi",
                preconditionKodebeskrivelse: "/soeknadGjelder/type/kode{0}/kodeverdi",
                preconditionValidert: "/soeknadGjelder/type");
            _soeknadTypeUtenAnsvarsrettValidator = new SoeknadTypeUtenAnsvarsrettValidator(tree, 19, _codeListService);
            _soeknadGjelderValidator = new SoeknadGjelderBaseValidator(tree, _soeknadTypeMedAnsvarsrettValidator, _soeknadTypeUtenAnsvarsrettValidator, _formName, 17);

            //UtfallBesvarelse
            _vedleggsTypeValidator = new VedleggsTypeValidator(tree, 25, _codeListService);
            _vedleggslisteValidator = new VedleggslisteValidator(tree, _vedleggsTypeValidator);
            _temaKodelisteValidator = new TemaValidator(tree, 23, _codeListService, true);
            _utloestFraSjekkpunktValidator = new UtloestFraSjekkpunktValidator(tree);
            _utfallTypeValidator = new UtfallTypeValidator(tree, 21, _codeListService, false, new[] { "SVV", "MKV" });
            _utfallBesvarelseFAValidator = new UtfallBesvarelseFAValidator(tree, _utfallTypeValidator, _utloestFraSjekkpunktValidator, _temaKodelisteValidator, _vedleggslisteValidator);

            //KravFerdigattest
            _kravFerdigattestValidator = new KravFerdigattestValidator(tree);

            //Varmesystem
            _energiforsyningValidator = new EnergiforsyningValidator(tree, 29, _codeListService);
            _varmefordelingValidator = new VarmefordelingValidator(tree, 30, _codeListService);
            _varmesystemValidator = new VarmesystemValidator(tree, _energiforsyningValidator, _varmefordelingValidator);

            //Vedlegg
            _vedleggValidator = new VedleggFAValidator(tree);
        }
        protected override void DefineValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.xml);

            AccumulateValidationRules(_eiendomByggestedValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_kommunensSaksnummerValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_metadataByggValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_generelleVilkaarValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarForByggesakenValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarligSoekerValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_tiltakshaverValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_soeknadGjelderValidator.ValidationResult.ValidationRules);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.utfallBesvarelse);
            AccumulateValidationRules(_utfallBesvarelseFAValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_kravFerdigattestValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_varmesystemValidator.ValidationResult.ValidationRules);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.erForetattIkkeSoeknadspliktigeJusteringer);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.erTilstrekkeligDokumentasjonOverlevertEier);
            AccumulateValidationRules(_vedleggValidator.ValidationResult.ValidationRules);
        }

        protected override DateTime? GetFormValidationDate()
        {
            return DateTime.Now;
        }

        protected override void Validate(ValidationInput validationInput)
        {
            var form = SerializeUtil.DeserializeFromString<Ferdigattest>(validationInput.FormData, false);

            var eiendomIndex = GetArrayIndex(form.EiendomByggested);
            for (int i = 0; i < eiendomIndex; i++)
            {
                var eiendomByggested = Helpers.ObjectIsNullOrEmpty(form.EiendomByggested) ? null : form.EiendomByggested[i];
                var eiendomByggestedResult = _eiendomByggestedValidator.Validate(eiendomByggested);
                AccumulateValidationMessages(eiendomByggestedResult.ValidationMessages, i);
            }

            //Kommunens saksnummer
            var kommunensSaksnummerResult = _kommunensSaksnummerValidator.Validate(form.KommunensSaksnummer);
            AccumulateValidationMessages(kommunensSaksnummerResult.ValidationMessages);

            //Metadata
            var metadataByggResult = _metadataByggValidator.Validate(form.Metadata);
            AccumulateValidationMessages(metadataByggResult.ValidationMessages);

            //GenerelleVilkaar
            var generelleVilkaarResult = _generelleVilkaarValidator.Validate(form.GenerelleVilkaar);
            AccumulateValidationMessages(generelleVilkaarResult.ValidationMessages);

            //Ansvar for byggesaken
            var ansvarForByggesakenResult = _ansvarForByggesakenValidator.Validate(form.AnsvarForByggesaken);
            AccumulateValidationMessages(ansvarForByggesakenResult.ValidationMessages);

            //AnsvarligSoeker
            var ansvarligSoekerResult = _ansvarligSoekerValidator.Validate(form.AnsvarligSoeker, form?.AnsvarForByggesaken);
            AccumulateValidationMessages(ansvarligSoekerResult.ValidationMessages);

            //Tiltakshaver
            var tiltakshaverResult = _tiltakshaverValidator.Validate(form.Tiltakshaver, form?.AnsvarForByggesaken);
            AccumulateValidationMessages(tiltakshaverResult.ValidationMessages);

            //SoeknadGjelder
            var soeknadGjelderResult = _soeknadGjelderValidator.Validate(form.SoeknadGjelder, form.AnsvarForByggesaken);
            AccumulateValidationMessages(soeknadGjelderResult.ValidationMessages);

            _tiltakstypes = _soeknadGjelderValidator.Tiltakstypes.ToArray();

            //UtfallBesvarelse
            if (form.UtfallBesvarelse == null)
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.utfallBesvarelse);
            }
            else
            {
                var index = GetArrayIndex(form.UtfallBesvarelse);
                for (int i = 0; i < index; i++)
                {
                    var utfallSvar = Helpers.ObjectIsNullOrEmpty(form.UtfallBesvarelse) ? null : form.UtfallBesvarelse[i];
                    if (utfallSvar == null)
                        AddMessageFromRule(ValidationRuleEnum.utfylt, "/utfallBesvarelse");
                    var utfallBesvarelseResult = _utfallBesvarelseFAValidator.Validate(utfallSvar, null);
                    AccumulateValidationMessages(utfallBesvarelseResult.ValidationMessages, i);
                }
            }

            ValidateDataRelations(form, validationInput);

            //KravFerdigattest
            var kravFerdigattestResult = _kravFerdigattestValidator.Validate(form.KravFerdigattest);
            AccumulateValidationMessages(kravFerdigattestResult.ValidationMessages);

            if (form.ErForetattIkkeSoeknadspliktigeJusteringer == null)
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.erForetattIkkeSoeknadspliktigeJusteringer);
            }
            if (form.ErTilstrekkeligDokumentasjonOverlevertEier == null)
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.erTilstrekkeligDokumentasjonOverlevertEier);
            }

            //Varmesystem
            var varmesystemResult = _varmesystemValidator.Validate(form.Varmesystem);
            AccumulateValidationMessages(varmesystemResult.ValidationMessages);

            //Vedlegg
            var vedleggResult = _vedleggValidator.Validate(validationInput.Attachments, form);
            AccumulateValidationMessages(vedleggResult.ValidationMessages);
        }

        private void ValidateDataRelations(Ferdigattest form, ValidationInput validationInput)
        {
        }

        protected override void CustomFormValidation(ValidationInput validationInput)
        {
            //throw new NotImplementedException();
        }
    }
}
