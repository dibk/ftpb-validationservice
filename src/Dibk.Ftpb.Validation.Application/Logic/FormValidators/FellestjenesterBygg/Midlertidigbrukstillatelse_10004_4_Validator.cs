using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Utils;
using MatrikkelWSClient.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.FormValidators.FellestjenesterBygg;

[FormData(DataFormatId = "10004", DataFormatVersion = "4", XsdFileName = "midlertidigbrukstillatelseV4", ProcessCategory = "MB", ServiceDomain = ServiceDomain.FTB)]
public class Midlertidigbrukstillatelse_10004_4_Validator : FormValidatorBase, IFormValidator
{
    private readonly ICodeListService _codeListService;
    private readonly IMatrikkelProvider _matrikkelProvider;
    private readonly IPostalCodeService _postalCodeService;
    private readonly IDecryptionFactory _decryptionFactory;

    private string[] _tiltakstyper;
    private List<EntityValidatorNode> _entitiesNodeList;
    private readonly string _formName = "midlertidig brukstillatelse";

    //EiendomByggested
    private IEiendomsidentifikasjonValidator _eiendomsidentifikasjonValidator;
    private IEiendomsAdresseValidator _eiendomsAdresseValidator;
    private IEiendomByggestedValidator _eiendomByggestedValidator;

    //KommunensSaksnummer
    private ISaksnummerValidator _kommunensSaksnummerValidator;

    //Metadata
    private IMetadataByggValidator _metadataByggValidator;
    private IKodelisteValidator _foretrukketspraakTypeValidator;

    //GenerelleVilkaar
    private IGenerelleVilkaarValidator _generelleVilkaarValidator;

    //SoeknadGjelder
    private SoeknadGjelderValidator _soeknadGjelderValidator;
    private IKodelisteValidator _soeknadTypeMedAnsvarsrettValidator;
    private IKodelisteValidator _soeknadTypeUtenAnsvarsrettValidator;

    //Delsøknader
    private IDelsoeknaderValidator _delsoeknaderValidator;

    //UtfallBesvarelse
    private IUtfallBesvarelseValidator _utfallBesvarelseValidator;
    private IKodelisteValidator _utfallTypeValidator;
    private IUtloestFraSjekkpunktValidator _utloestFraSjekkpunktValidator;
    private IKodelisteValidator _temaKodelisteValidator;
    private IKodelisteValidator _vedleggsTypeValidator;
    private IVedleggslisteValidator _vedleggslisteValidator;

    //Tiltakshaver
    private TiltakshaverValidator _tiltakshaverValidator;
    private IEnkelAdresseValidator _tiltakshaverEnkelAdresseValidator;
    private IKodelisteValidator _tiltakshaverPartstypeValidator;
    private IKontaktpersonValidator _tiltakshaverKontaktpersonValidator;

    //AnsvarligSoeker
    private AnsvarligSoekerValidator _ansvarligSoekerValidator;
    private IEnkelAdresseValidator _ansvarligSoekerEnkelAdresseValidator;
    private IKodelisteValidator _ansvarligSoekerPartstypeValidator;
    private IKontaktpersonValidator _ansvarligSoekerKontaktpersonValidator;

    //GjenstaaendeArbeider
    private IGjenstaaendeArbeiderValidator _gjenstaaendeArbeiderValidator;

    //Sikkerhetsnivaa
    private ISikkerhetsnivaaValidator _sikkerhetsnivaaValidator;

    //AnsvarForByggesaken
    private IKodelisteValidator _ansvarForByggesakenValidator;

    //Vedlegg
    private VedleggMBValidator _vedleggValidator;

    //Avsender
    private ISubmitterValidator _submitterValidator;

    public Midlertidigbrukstillatelse_10004_4_Validator
    (
        IValidationMessageComposer validationMessageComposer,
        IChecklistService checklistService,
        ICodeListService codeListService,
        IMatrikkelProvider matrikkelProvider,
        IDecryptionFactory decryptionFactory,
        IPostalCodeService postalCodeService

    ) : base(validationMessageComposer, checklistService)
    {
        _codeListService = codeListService;
        _matrikkelProvider = matrikkelProvider;
        _postalCodeService = postalCodeService;
        _decryptionFactory = decryptionFactory;

        _tiltakstyper = Array.Empty<string>();
        _entitiesNodeList = new List<EntityValidatorNode>();

        base.InitializeFormValidator<Midlertidigbrukstillatelse>();
    }

    protected override void InitializeValidatorConfig()
    {
        //EiendomByggested
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() { NodeId = 1, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
            new() { NodeId = 2, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 1 },
            new() { NodeId = 3, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 1 },
        });

        //KommunensSaksnummer
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 4, EnumId = EntityValidatorEnum.KommunensSaksnummerValidator, ParentID = null}
        });

        //Metadata
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 5, EnumId = EntityValidatorEnum.MetadataByggValidator, ParentID = null},
            new() {NodeId = 6, EnumId = EntityValidatorEnum.ForetrukketspraakTypeValidator, ParentID = 5},
        });

        //GenerelleVilkaar
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 7, EnumId = EntityValidatorEnum.GenerelleVilkaarValidator, ParentID = null}
        });

        //SoeknadGjelder
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 8, EnumId = EntityValidatorEnum.SoeknadGjelderValidator, ParentID = null},
            new() {NodeId = 9, EnumId = EntityValidatorEnum.TypeValidator, ParentID = 8},
            new() {NodeId = 30, EnumId = EntityValidatorEnum.SoeknadTypeUtenAnsvarsrettValidator, ParentID = 8},
        });

        //Delsøknader
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 28, EnumId = EntityValidatorEnum.DelsoeknaderValidator, ParentID = null}
        });

        //UtfallBesvarelse
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 10, EnumId = EntityValidatorEnum.UtfallBesvarelseValidator, ParentID = null},
            new() {NodeId = 11, EnumId = EntityValidatorEnum.UtfallTypeValidator, ParentID = 10},
            new() {NodeId = 12, EnumId = EntityValidatorEnum.UtloestFraSjekkpunktValidator, ParentID = 10},
            new() {NodeId = 13, EnumId = EntityValidatorEnum.TemaValidator, ParentID = 10},
            new() {NodeId = 14, EnumId = EntityValidatorEnum.VedleggslisteValidator, ParentID = 10},
            new() {NodeId = 15, EnumId = EntityValidatorEnum.VedleggsTypeValidator, ParentID = 14},
        });

        //Tiltakshaver
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() { NodeId = 17, EnumId = EntityValidatorEnum.TiltakshaverValidator, ParentID = null },
            new() { NodeId = 18, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 17 },
            new() { NodeId = 19, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 17 },
            new() { NodeId = 20, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 17 }
        });

        //AnsvarligSoeker
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() { NodeId = 21, EnumId = EntityValidatorEnum.AnsvarligSoekerValidator, ParentID = null },
            new() { NodeId = 22, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 21 },
            new() { NodeId = 23, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 21 },
            new() { NodeId = 24, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 21 }
        });

        //GjenstaaendeArbeider
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 25, EnumId = EntityValidatorEnum.GjenstaaendeArbeiderValidator, ParentID = null}
        });

        //Sikkerhetsnivaa
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 26, EnumId = EntityValidatorEnum.SikkerhetsnivaaValidator, ParentID = null}
        });

        //AnsvarForByggesaken
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 27, EnumId = EntityValidatorEnum.AnsvarForByggesakenValidator, ParentID = null}
        });

        //Vedlegg
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 29, EnumId = EntityValidatorEnum.VedleggMBValidator, ParentID = null}
        });

        //Avsender
        _entitiesNodeList.AddRange(new List<EntityValidatorNode>
        {
            new() {NodeId = 31, EnumId = EntityValidatorEnum.SubmitterValidator, ParentID = null}
        });
    }

    protected override IEnumerable<string> GetFormTiltakstyper()
    {
        return _tiltakstyper;
    }

    protected override DateTime? GetFormValidationDate()
    {
        return DateTime.Now;
    }

    protected override void InstantiateValidators()
    {
        var tree = EntityValidatorTree.BuildTree(_entitiesNodeList);

        //EiendomByggested
        _eiendomsAdresseValidator = new EiendomsAdresseValidator(tree);
        _eiendomsidentifikasjonValidator = new EiendomsidentifikasjonValidator(tree, _codeListService, _matrikkelProvider);
        _eiendomByggestedValidator = new EiendomByggestedValidator(tree, _eiendomsAdresseValidator, _eiendomsidentifikasjonValidator, _matrikkelProvider, true);

        //KommunensSaksnummer
        _kommunensSaksnummerValidator = new KommunensSaksnummerValidator(tree);

        //Metadata
        _foretrukketspraakTypeValidator = new ForetrukketspraakTypeValidator(tree, 6, _codeListService, true);
        _metadataByggValidator = new MetadataByggValidator(tree, _foretrukketspraakTypeValidator);

        //GenerelleVilkaar
        _generelleVilkaarValidator = new GenerelleVilkaarValidator(tree);

        //SoeknadGjelder
        _soeknadTypeMedAnsvarsrettValidator = new TypeValidator(tree, 9, _codeListService,
            preconditionKodeverdi: "/ansvarForByggesaken/kodeverdi",
            preconditionKodebeskrivelse: "/soeknadGjelder/type/kode{0}/kodeverdi",
            preconditionValidert: "/soeknadGjelder/type");
        _soeknadTypeUtenAnsvarsrettValidator = new SoeknadTypeUtenAnsvarsrettValidator(tree, 30, _codeListService);
        _soeknadGjelderValidator = new SoeknadGjelderValidator(tree, _soeknadTypeMedAnsvarsrettValidator, _soeknadTypeUtenAnsvarsrettValidator, _formName);

        //Delsøknader
        _delsoeknaderValidator = new DelsoeknaderValidator(tree);

        //UtfallBesvarelse
        _vedleggsTypeValidator = new VedleggsTypeValidator(tree, 15, _codeListService);
        _vedleggslisteValidator = new VedleggslisteValidator(tree, _vedleggsTypeValidator);
        _temaKodelisteValidator = new TemaValidator(tree, 13, _codeListService, true);
        _utloestFraSjekkpunktValidator = new UtloestFraSjekkpunktValidator(tree);
        _utfallTypeValidator = new UtfallTypeValidator(tree, 11, _codeListService, false, new[] { "SVV", "MKV" });
        _utfallBesvarelseValidator = new UtfallBesvarelseValidator(tree, _utfallTypeValidator, _utloestFraSjekkpunktValidator, _temaKodelisteValidator, _vedleggslisteValidator);

        //Tiltakshaver
        _tiltakshaverKontaktpersonValidator = new KontaktpersonValidator(tree, 18);
        _tiltakshaverPartstypeValidator = new PartstypeValidator(tree, 19, _codeListService);
        _tiltakshaverEnkelAdresseValidator = new EnkelAdresseValidator(tree, 20, _postalCodeService);
        _tiltakshaverValidator = new TiltakshaverValidator(tree, _tiltakshaverEnkelAdresseValidator, _tiltakshaverKontaktpersonValidator, _tiltakshaverPartstypeValidator, _codeListService, _decryptionFactory,
            isDependentOnAnsvarForByggesaken: true);

        //*AnsvarligSoeker
        _ansvarligSoekerKontaktpersonValidator = new KontaktpersonValidator(tree, 22);
        _ansvarligSoekerPartstypeValidator = new PartstypeValidator(tree, 23, _codeListService);
        _ansvarligSoekerEnkelAdresseValidator = new EnkelAdresseValidator(tree, 24, _postalCodeService);
        _ansvarligSoekerValidator = new AnsvarligSoekerValidator(tree,_ansvarligSoekerEnkelAdresseValidator,
            _ansvarligSoekerKontaktpersonValidator, _ansvarligSoekerPartstypeValidator, _codeListService,
            _decryptionFactory, isDependentOnAnsvarForByggesaken: true);

        //GjenstaaendeArbeider
        _gjenstaaendeArbeiderValidator = new GjenstaaendeArbeiderValidator(tree);

        //Sikkerhetsnivaa
        _sikkerhetsnivaaValidator = new SikkerhetsnivaaValidator(tree);

        //AnsvarForByggesaken
        _ansvarForByggesakenValidator = new AnsvarForByggesakenValidator(tree, 27, _codeListService);

        //Vedlegg
        _vedleggValidator = new VedleggMBValidator(_entitiesNodeList);

        //Avsender
        _submitterValidator = new SubmitterValidator(tree, _decryptionFactory);
    }

    protected override void Validate(ValidationInput validationInput)
    {
        var form = SerializeUtil.DeserializeFromString<Midlertidigbrukstillatelse>(validationInput.FormData, false);

        //EiendomByggested
        var eiendomIndex = GetArrayIndex(form.EiendomByggested);
        for (int i = 0; i < eiendomIndex; i++)
        {
            var eiendomByggested = Helpers.ObjectIsNullOrEmpty(form.EiendomByggested) ? null : form.EiendomByggested[i];
            var eiendomByggestedResult = _eiendomByggestedValidator.Validate(eiendomByggested);
            AccumulateValidationMessages(eiendomByggestedResult.ValidationMessages, i);
        }

        //KommunensSaksnummer
        var kommunesSaksnummerResult = _kommunensSaksnummerValidator.Validate(form.KommunensSaksnummer);
        AccumulateValidationMessages(kommunesSaksnummerResult.ValidationMessages);

        //Metadata
        var metadataByggResult = _metadataByggValidator.Validate(form.Metadata);
        AccumulateValidationMessages(metadataByggResult.ValidationMessages);

        //GenerelleVilkaar
        var generelleVilkaarResult = _generelleVilkaarValidator.Validate(form.GenerelleVilkaar);
        AccumulateValidationMessages(generelleVilkaarResult.ValidationMessages);

        //SoeknadGjelder
        var soeknadGjelderResult = _soeknadGjelderValidator.Validate(form.SoeknadGjelder, form.AnsvarForByggesaken);
        AccumulateValidationMessages(soeknadGjelderResult.ValidationMessages);
        _tiltakstyper = _soeknadGjelderValidator.Tiltakstypes;

        //Delsøknader
        var delsoeknadsnummer = _soeknadGjelderValidator.Delsoeknadsnummer;
        if (delsoeknadsnummer > 1)
        {
            if (form.Delsoeknader == null)
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.delsoeknader, new[] { _formName });
            }
            else
            {
                var index = GetArrayIndex(form.Delsoeknader);
                for (int i = 0; i < index; i++)
                {
                    var delsoeknad = Helpers.ObjectIsNullOrEmpty(form.Delsoeknader) ? null : form.Delsoeknader[i];
                    var delsoeknadResult = _delsoeknaderValidator.Validate(delsoeknad);
                    AccumulateValidationMessages(delsoeknadResult.ValidationMessages, i);
                }
            }
        }

        //UtfallBesvarelse
        if (form.UtfallBesvarelse == null)
        {
            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.utfallBesvarelse);
        }
        else
        {
            var index = GetArrayIndex(form.UtfallBesvarelse);
            for (int i = 0; i < index; i++)
            {
                var utfallSvar = Helpers.ObjectIsNullOrEmpty(form.UtfallBesvarelse) ? null : form.UtfallBesvarelse[i];
                if (utfallSvar == null)
                    AddMessageFromRule(ValidationRuleEnum.utfylt, "/utfallBesvarelse");
                var utfallBesvarelseResult = _utfallBesvarelseValidator.Validate(utfallSvar, delsoeknadsnummer);
                AccumulateValidationMessages(utfallBesvarelseResult.ValidationMessages, i);
            }
        }

        //Tiltakshaver
        var tiltakshaverValidationResult = _tiltakshaverValidator.Validate(form.Tiltakshaver, form?.AnsvarForByggesaken);
        AccumulateValidationMessages(tiltakshaverValidationResult.ValidationMessages);

        //AnsvarligSoeker
        var ansvarligSoekerValidationResult = _ansvarligSoekerValidator.Validate(form.AnsvarligSoeker, form?.AnsvarForByggesaken);
        AccumulateValidationMessages(ansvarligSoekerValidationResult.ValidationMessages);

        //DatoForFerdigattest
        if (form.DatoFerdigattest is null || form.DatoFerdigattest.Equals(new DateTime()))
        {
            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.datoForFerdigattest);
        }

        //GjenstaaendeArbeider
        var gjenstaaendeArbeiderResult = _gjenstaaendeArbeiderValidator.Validate(form.GjenstaaendeArbeider, form.SoeknadGjelder?.GjelderHeleTiltaket);
        AccumulateValidationMessages(gjenstaaendeArbeiderResult.ValidationMessages);

        //Sikkerhetsnivaa
        var sikkerhetsnivaaResult = _sikkerhetsnivaaValidator.Validate(form.Sikkerhetsnivaa);
        AccumulateValidationMessages(sikkerhetsnivaaResult.ValidationMessages);

        //AnsvarForByggesaken
        var ansvarForByggesakenResult = _ansvarForByggesakenValidator.Validate(form.AnsvarForByggesaken);
        AccumulateValidationMessages(ansvarForByggesakenResult.ValidationMessages);

        //Vedlegg
        var vedleggResult = _vedleggValidator.Validate(validationInput.Attachments, form);
        AccumulateValidationMessages(vedleggResult.ValidationMessages);

        ValidateDataRelations(form, validationInput.AuthenticatedSubmitter);
    }

    protected override void DefineValidationRules()
    {
        AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.xml);

        AccumulateValidationRules(_eiendomByggestedValidator.ValidationResult.ValidationRules);

        AccumulateValidationRules(_kommunensSaksnummerValidator.ValidationResult.ValidationRules);

        AccumulateValidationRules(_metadataByggValidator.ValidationResult.ValidationRules);

        AccumulateValidationRules(_generelleVilkaarValidator.ValidationResult.ValidationRules);

        AccumulateValidationRules(_soeknadGjelderValidator.ValidationResult.ValidationRules);

        AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.delsoeknader, "/soeknadGjelder/delsoeknadsnummer > 1");
        AccumulateValidationRules(_delsoeknaderValidator.ValidationResult.ValidationRules);

        AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.utfallBesvarelse);
        AccumulateValidationRules(_utfallBesvarelseValidator.ValidationResult.ValidationRules);

        AccumulateValidationRules(_tiltakshaverValidator.ValidationResult.ValidationRules);

        AccumulateValidationRules(_ansvarligSoekerValidator.ValidationResult.ValidationRules);

        AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.datoForFerdigattest);

        AccumulateValidationRules(_gjenstaaendeArbeiderValidator.ValidationResult.ValidationRules);

        AccumulateValidationRules(_sikkerhetsnivaaValidator.ValidationResult.ValidationRules);

        AccumulateValidationRules(_ansvarForByggesakenValidator.ValidationResult.ValidationRules);

        AccumulateValidationRules(_vedleggValidator.ValidationResult.ValidationRules);

        AccumulateValidationRules(_submitterValidator.ValidationResult.ValidationRules);
    }

    private void ValidateDataRelations(Midlertidigbrukstillatelse form, string authenticatedSubmitter)
    {
        if (!IsAnyValidationMessagesWithXpath("/tiltakshaver/partstype/kodeverdi") || !IsAnyValidationMessagesWithXpath("/ansvarligSoeker/partstype/kodeverdi"))
        {
            var medAnsvarsrettValues = new[] { "medAnsvar", "selvbygger" };
            var utenAnsvarsrettValues = new[] { "utenAnsvar", "utenAnsvarMedKontroll" };

            var ansvarForByggesakenKodeverdi = form.AnsvarForByggesaken?.Kodeverdi;

            if (medAnsvarsrettValues.Contains(ansvarForByggesakenKodeverdi))
                AccumulateValidationMessages(_submitterValidator.Validate(null, form.AnsvarligSoeker, authenticatedSubmitter).ValidationMessages);

            else if (utenAnsvarsrettValues.Contains(ansvarForByggesakenKodeverdi))
                AccumulateValidationMessages(_submitterValidator.Validate(form.Tiltakshaver, null, authenticatedSubmitter).ValidationMessages);
        }
    }


    protected override void CustomFormValidation(ValidationInput validationInput)
    {
        // Ikke i bruk for denne tjenesten
    }
}