using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Utils;
using MatrikkelWSClient.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.FormValidators.FellestjenesterBygg
{
    [FormData(DataFormatId = "7063", DataFormatVersion = "47177", XsdFileName = "AvfallsplanV2", ProcessCategory = "FA", ServiceDomain = ServiceDomain.FTB)]
    public class Avfallsplan_7063_47177_Validator : FormValidatorBase, IFormValidator
    {
        private List<EntityValidatorNode> _entitiesNodeList;

        private SluttrapportForBygningsAvfall _validationForm { get; set; }

        private readonly ICodeListService _codeListService;
        private readonly IPostalCodeService _postalCodeService;
        private readonly IChecklistService _checklistService;
        private readonly IMatrikkelProvider _matrikkelProvider;
        private readonly IDecryptionFactory _decryptionFactory;

        //EiendomByggested
        private IEiendomsidentifikasjonValidator _eiendomsidentifikasjonValidator;

        private IEiendomsAdresseValidator _eiendomsAdresseValidator;
        private IEiendomByggestedValidator _eiendomByggestedValidator;

        //Saksnummer
        private ISaksnummerValidator _kommunensSaksnummerArrayValidator;

        //AnsvarligSoeker
        private IAktoerValidator _ansvarligSoekerValidator;

        private IEnkelAdresseValidator _ansvarligSoekerEnkelAdresseValidator;
        private IKodelisteValidator _ansvarligSoekerPartstypeValidator;
        private IKontaktpersonValidator _ansvarligSoekerKontaktpersonValidator;

        //Tiltakshaver
        private IAktoerValidator _tiltakshaverValidator;

        private IEnkelAdresseValidator _tiltakshaverEnkelAdresseValidator;
        private IKodelisteValidator _tiltakshaverPartstypeValidator;
        private IKontaktpersonValidator _tiltakshaverKontaktpersonValidator;

        //Metadata
        private IMetadataFtbValidator _metadataValidator;

        //BeskrivelseAvTiltak
        private IKodelisteValidator _bygningstypeValidator;

        private IFormaaltypeValidator _formaaltypeValidator;
        private IKodelisteValidator _tiltakstypeValidator;
        private IBeskrivelseAvTiltakValidator _beskrivelseAvTiltakValidator;

        //PlanForAvfall
        private IPlanForAvfallValidator _planForAvfallValidator;

        private IAvfallplanValidator _avfallplanValidator;

        private IKodelisteValidator _avfallplanAvfallklasseValidator;
        private IKodelisteValidator _avfallplanAvfallfraksjonValidator;

        private IAvfallsorteringValidator _avfallplanSorteringValidator;
        private IKodelisteValidator _avfallplanAvfallklassedelsumAvfallklasseValidator;

        private IAvfallklasseDelsumValidator _avfallplanAvfallklasseDelsumValidator;

        //SluttrapportForAvfall
        private ISluttrapportForAvfallValidator _sluttrapportForAvfallValidator;

        private IKodelisteValidator _avfallrapportAvfallklasseValidator;
        private IKodelisteValidator _avfallrapportAvfallfraksjonValidator;
        private IKodelisteValidator _avfallrapportAvfallklassedelsumAvfallklasseValidator;
        private IAvfallsorteringValidator _avfallsluttrapportSorteringValidator;
        private IKodelisteValidator _disponeringDisponeringsmaateValidator;
        private IKodelisteValidator _disponeringsmaatedelsumDisponeringsmaateValidator;
        private ILeveringsstedValidator _leveringsstedValidator;
        private IAvfallrapportValidator _avfallrapportValidator;
        private IAvfallklasseDelsumValidator _avfallrapportAvfallklasseDelsumValidator;

        private IDisponeringsmaateDelsumValidator _disponeringsmaateDelSumValidator;
        //private IAvfallklasseDelsumValidator _avfallklasseDelsumValidator;

        private IAvfalldisponeringValidator _avfalldisponeringValidator;

        //Overgangsordning
        private OvergangsordningValidator _overgangsordningValidator;

        private string[] _tiltakstypes;
        private DateTime? _validationDate;
        private AttachmentInfo[] _attachments;

        public Avfallsplan_7063_47177_Validator(IValidationMessageComposer validationMessageComposer,
                                                ICodeListService codeListService,
                                                IPostalCodeService postalCodeService,
                                                IChecklistService checklistService,
                                                IMatrikkelProvider matrikkelProvider,
                                                IDecryptionFactory decryptionFactory)
            : base(validationMessageComposer, checklistService)
        {
            _codeListService = codeListService;
            _postalCodeService = postalCodeService;
            _entitiesNodeList = new List<EntityValidatorNode>();
            _tiltakstypes = new string[] { };
            _matrikkelProvider = matrikkelProvider;
            _decryptionFactory = decryptionFactory;
            //SetTitaksTypeIsøknad();

            base.InitializeFormValidator<SluttrapportForBygningsAvfall>();
        }

        protected override void InitializeValidatorConfig()
        {
            // EiendomByggested
            List<EntityValidatorNode> eiendombyggestedNodeList = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 1 },
                new() { NodeId = 3, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 1 },
            };
            _entitiesNodeList.AddRange(eiendombyggestedNodeList);

            //Kommunenes Saksnummer
            var kommunenesSaksnummerValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 4, EnumId = EntityValidatorEnum.KommunensSaksnummerArrayValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(kommunenesSaksnummerValidatorNodeList);

            //** check not implemented in FTB
            //AnsvarligSoeker
            var ansvarligSoekervalidatorNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 5, EnumId = EntityValidatorEnum.AnsvarligSoekerValidator, ParentID = null},
                new () {NodeId = 6, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 5},
                new () {NodeId = 7, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 5},
                new () {NodeId = 8, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 5}
            };
            _entitiesNodeList.AddRange(ansvarligSoekervalidatorNodeList);
            //**

            //Metadata
            var metadataValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 9, EnumId = EntityValidatorEnum.MetadataAvfallsplanV2Validator, ParentID = null}
            };
            _entitiesNodeList.AddRange(metadataValidatorNodeList);

            //beskrivelseAvTiltak
            var beskrivelseAvTiltakNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 10, EnumId = EntityValidatorEnum.BeskrivelseAvTiltakValidator, ParentID = null},
                new () {NodeId = 11, EnumId = EntityValidatorEnum.FormaaltypeValidatorV2, ParentID = 10},
                new () {NodeId = 12, EnumId = EntityValidatorEnum.BygningstypeValidatorV2, ParentID = 11},
                new () {NodeId = 13, EnumId = EntityValidatorEnum.TiltakstypeValidator, ParentID = 10},
            };
            _entitiesNodeList.AddRange(beskrivelseAvTiltakNodeList);

            //Tiltakshaver
            var tiltakshavervalidatorNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 14, EnumId = EntityValidatorEnum.TiltakshaverValidator, ParentID = null},
                new () {NodeId = 15, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 14},
                new () {NodeId = 16, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 14},
                new () {NodeId = 17, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 14}
            };
            _entitiesNodeList.AddRange(tiltakshavervalidatorNodeList);

            //planForAvfall
            var planForAvfallNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 18, EnumId = EntityValidatorEnum.PlanForAvfallValidator, ParentID = null},
                new () {NodeId = 19, EnumId = EntityValidatorEnum.AvfallplanValidator, ParentID = 18},

                new () {NodeId = 20, EnumId = EntityValidatorEnum.AvfallklasseValidator, ParentID = 19},
                new () {NodeId = 21, EnumId = EntityValidatorEnum.AvfallfraksjonValidator, ParentID = 19},

                new () {NodeId = 22, EnumId = EntityValidatorEnum.AvfallsorteringValidator, ParentID = 18},
                new () {NodeId = 23, EnumId = EntityValidatorEnum.AvfallsklasseDelsumValidator, ParentID = 18},

                new () {NodeId = 24, EnumId = EntityValidatorEnum.AvfallklasseValidator, ParentID = 23},
            };
            _entitiesNodeList.AddRange(planForAvfallNodeList);

            //sluttrapportForAvfall
            var sluttrapportForAvfallNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 25, EnumId = EntityValidatorEnum.SluttrapportForAvfallValidator, ParentID = null},
                new () {NodeId = 26, EnumId = EntityValidatorEnum.AvfallrapportValidator, ParentID = 25},

                new () {NodeId = 27, EnumId = EntityValidatorEnum.AvfallklasseValidator, ParentID = 26},
                new () {NodeId = 28, EnumId = EntityValidatorEnum.AvfallfraksjonValidator, ParentID = 26},
                new () {NodeId = 29, EnumId = EntityValidatorEnum.AvfalldisponeringValidator, ParentID = 26},

                new () {NodeId = 30, EnumId = EntityValidatorEnum.LeveringsstedValidator, ParentID = 29},
                new () {NodeId = 31, EnumId = EntityValidatorEnum.DisponeringsmaateValidator, ParentID = 29},

                new () {NodeId = 32, EnumId = EntityValidatorEnum.AvfallsorteringValidator, ParentID = 25},

                new () {NodeId = 33, EnumId = EntityValidatorEnum.DisponeringsmaateDelsumValidator, ParentID = 25},
                new () {NodeId = 34, EnumId = EntityValidatorEnum.DisponeringsmaateValidator, ParentID = 33},

                new () {NodeId = 35, EnumId = EntityValidatorEnum.AvfallsklasseDelsumValidator, ParentID = 25},
                new () {NodeId = 36, EnumId = EntityValidatorEnum.AvfallklasseValidator, ParentID = 35},
            };
            _entitiesNodeList.AddRange(sluttrapportForAvfallNodeList);

            //OvergangsordningValidator
            List<EntityValidatorNode> overgangsordningValidatorNodes = new()
            {
                new() { NodeId = 38, EnumId = EntityValidatorEnum.OvergangsordningValidator, ParentID = null },
            };

            _entitiesNodeList.AddRange(overgangsordningValidatorNodes);
        }

        protected override void InstantiateValidators()
        {
            var tree = EntityValidatorTree.BuildTree(_entitiesNodeList);

            //EiendomByggested
            _eiendomsAdresseValidator = new EiendomsAdresseValidator(tree);
            _eiendomsidentifikasjonValidator = new EiendomsidentifikasjonValidator(tree, _codeListService, _matrikkelProvider);
            _eiendomByggestedValidator = new EiendomByggestedValidator(tree, _eiendomsAdresseValidator, _eiendomsidentifikasjonValidator, _matrikkelProvider);

            //Kommunens saksnummer
            _kommunensSaksnummerArrayValidator = new KommunensSaksnummerArrayValidator(tree);

            //AnsvarligSoeker TODO not applied in FTB v1
            _ansvarligSoekerKontaktpersonValidator = new KontaktpersonValidator(tree, 6);
            _ansvarligSoekerPartstypeValidator = new PartstypeValidator(tree, 7, _codeListService);
            _ansvarligSoekerEnkelAdresseValidator = new EnkelAdresseValidator(tree, 8, _postalCodeService);
            _ansvarligSoekerValidator = new AnsvarligSoekerValidator(tree, _ansvarligSoekerEnkelAdresseValidator, _ansvarligSoekerKontaktpersonValidator, _ansvarligSoekerPartstypeValidator, _codeListService, _decryptionFactory);

            //Tiltakshaver
            _tiltakshaverKontaktpersonValidator = new KontaktpersonValidator(tree, 15);
            _tiltakshaverPartstypeValidator = new PartstypeValidator(tree, 16, _codeListService);
            _tiltakshaverEnkelAdresseValidator = new EnkelAdresseValidator(tree, 17, _postalCodeService);
            _tiltakshaverValidator = new TiltakshaverValidator(tree, _tiltakshaverEnkelAdresseValidator, _tiltakshaverKontaktpersonValidator, _tiltakshaverPartstypeValidator, _codeListService, _decryptionFactory);

            //Metadata
            _metadataValidator = new MetadataAvfallsplanV2Validator(tree);

            //BeskrivelseAvTiltak
            _bygningstypeValidator = new BygningstypeValidatorV2(tree, _codeListService);
            _formaaltypeValidator = new FormaaltypeValidatorV2(tree, _bygningstypeValidator);
            _tiltakstypeValidator = new TiltakstypeValidator(tree, _codeListService);
            _beskrivelseAvTiltakValidator = new BeskrivelseAvTiltakValidator(tree, _formaaltypeValidator, _tiltakstypeValidator);

            //PlanForAvfall
            _avfallplanAvfallklasseValidator = new AvfallklasseValidator(tree, 20, _codeListService);
            _avfallplanAvfallfraksjonValidator = new AvfallfraksjonValidator(tree, 21, _codeListService);
            _avfallplanValidator = new AvfallplanValidator(tree, _avfallplanAvfallklasseValidator, _avfallplanAvfallfraksjonValidator);

            _avfallplanAvfallklassedelsumAvfallklasseValidator = new AvfallklasseValidator(tree, 24, _codeListService);
            _avfallplanAvfallklasseDelsumValidator = new AvfallklasseDelsumValidator(tree, 23, _avfallplanAvfallklassedelsumAvfallklasseValidator);

            _avfallplanSorteringValidator = new AvfallsorteringValidator(tree, 22);
            _planForAvfallValidator = new PlanForAvfallValidator(tree, _avfallplanValidator, _avfallplanSorteringValidator, _avfallplanAvfallklasseDelsumValidator);

            //SluttrapportForAvfall
            _leveringsstedValidator = new LeveringsstedValidator(tree);
            _disponeringDisponeringsmaateValidator = new DisponeringsmaateValidator(tree, 31, _codeListService);
            _avfallrapportAvfallklasseValidator = new AvfallklasseValidator(tree, 27, _codeListService);
            _avfallrapportAvfallfraksjonValidator = new AvfallfraksjonValidator(tree, 28, _codeListService);

            _avfalldisponeringValidator = new AvfalldisponeringValidator(tree, _leveringsstedValidator, _disponeringDisponeringsmaateValidator);
            _avfallrapportValidator = new AvfallrapportValidator(tree, _avfallrapportAvfallklasseValidator, _avfallrapportAvfallfraksjonValidator, _avfalldisponeringValidator);

            _avfallrapportAvfallklassedelsumAvfallklasseValidator = new AvfallklasseValidator(tree, 36, _codeListService);
            _avfallrapportAvfallklasseDelsumValidator = new AvfallklasseDelsumValidator(tree, 35, _avfallrapportAvfallklassedelsumAvfallklasseValidator);

            _avfallsluttrapportSorteringValidator = new AvfallsorteringValidator(tree, 32);

            _disponeringsmaatedelsumDisponeringsmaateValidator = new DisponeringsmaateValidator(tree, 34, _codeListService);
            _disponeringsmaateDelSumValidator = new DisponeringsmaateDelsumValidator(tree, _disponeringsmaatedelsumDisponeringsmaateValidator);

            _sluttrapportForAvfallValidator = new SluttrapportForAvfallValidator(tree, _avfallrapportValidator, _disponeringsmaateDelSumValidator, _avfallrapportAvfallklasseDelsumValidator, _avfallsluttrapportSorteringValidator);

            //overgangsordning
            _overgangsordningValidator = new OvergangsordningValidator(tree);
        }

        protected override void DefineValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.xml);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.Soeker);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.avfallEtterRiving);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.bruksarealAvfall);

            //TODO create a method to do this automatic, potential error
            //EiendomByggested rules
            AccumulateValidationRules(_eiendomByggestedValidator.ValidationResult.ValidationRules);

            //Kommunens saksnummer
            AccumulateValidationRules(_kommunensSaksnummerArrayValidator.ValidationResult.ValidationRules);

            //AnsvarligSoeker
            AccumulateValidationRules(_ansvarligSoekerValidator.ValidationResult.ValidationRules);

            //Tiltakshaver
            AccumulateValidationRules(_tiltakshaverValidator.ValidationResult.ValidationRules);

            //Metadata
            AccumulateValidationRules(_metadataValidator.ValidationResult.ValidationRules);

            //BeskrivelseAvTiltak
            AccumulateValidationRules(_beskrivelseAvTiltakValidator.ValidationResult.ValidationRules);

            //PlanForAvfall
            AccumulateValidationRules(_planForAvfallValidator.ValidationResult.ValidationRules);

            //SluttrapportForAvfall
            AccumulateValidationRules(_sluttrapportForAvfallValidator.ValidationResult.ValidationRules);

            //OvergangsordningValidator
            AccumulateValidationRules(_overgangsordningValidator.ValidationResult.ValidationRules);

            RemoveNotRelevantRules();
        }

        private void RemoveNotRelevantRules()
        {
            var planMengdeAvfallPerArealXpath = Helpers.GetEntityXpath(_avfallplanSorteringValidator._entityXPath, null, FieldNameEnum.mengdeAvfallPerAreal);
            RemoveNotRelevantRulesFromValidationRules(planMengdeAvfallPerArealXpath);

            var tiltakshaverXpath = Helpers.GetEntityXpath(_tiltakshaverValidator._entityXPath);
            RemoveNotRelevantRulesFromValidationRules(tiltakshaverXpath, ValidationRuleEnum.utfylt);
            var ansvarligSoekerXpath = Helpers.GetEntityXpath(_ansvarligSoekerValidator._entityXPath);
            RemoveNotRelevantRulesFromValidationRules(ansvarligSoekerXpath, ValidationRuleEnum.utfylt);
        }

        protected override void Validate(ValidationInput validationInput)
        {
            _validationForm = SerializeUtil.DeserializeFromString<SluttrapportForBygningsAvfall>(validationInput.FormData);
            _attachments = validationInput.Attachments;

            ValidateEntityFields(_validationForm);

            var beskrivelseAvTiltakValidationResult = _beskrivelseAvTiltakValidator.Validate(_validationForm.BeskrivelseAvTiltak);
            AccumulateValidationMessages(beskrivelseAvTiltakValidationResult.ValidationMessages);

            _tiltakstypes = _beskrivelseAvTiltakValidator.Tiltakstypes.ToArray();
            _validationDate = _validationForm.Overgangsordning?.DatoForHovedsoeknad;

            var eiendommer = _validationForm?.EiendomByggested;

            var index = GetArrayIndex(eiendommer);

            for (int i = 0; i < index; i++)
            {
                var eiendom = Helpers.ObjectIsNullOrEmpty(eiendommer) ? null : eiendommer[i];
                var eiendomValidationResult = _eiendomByggestedValidator.Validate(eiendom);
                AccumulateValidationMessages(eiendomValidationResult.ValidationMessages, i);
            }

            var attachments = Helpers.ObjectIsNullOrEmpty(validationInput.Attachments) ? null : validationInput.Attachments.Select(a => a.AttachmentTypeName).ToArray();

            var metadataValidationResult = _metadataValidator.Validate(_validationForm.Metadata);
            AccumulateValidationMessages(metadataValidationResult.ValidationMessages);

            var kommunensSaksnummers = _validationForm?.KommunensSaksnummer;
            var indexKommunensSaksnummers = GetArrayIndex(kommunensSaksnummers);

            for (int i = 0; i < indexKommunensSaksnummers; i++)
            {
                var saksnummer = Helpers.ObjectIsNullOrEmpty(kommunensSaksnummers) ? null : kommunensSaksnummers[i];
                var kommunensSaksnummerValidationResult = _kommunensSaksnummerArrayValidator.Validate(saksnummer);
                AccumulateValidationMessages(kommunensSaksnummerValidationResult.ValidationMessages, i);
            }

            var _attachmentsTypesName = _attachments?.Select(a => a.AttachmentTypeName)?.ToArray();
            var planForAvfallValidationResult = _planForAvfallValidator.Validate(_validationForm.PlanForAvfall, _validationForm.Overgangsordning, _attachmentsTypesName);
            AccumulateValidationMessages(planForAvfallValidationResult.ValidationMessages);

            var sluttrapportForAvfallValidationResult = _sluttrapportForAvfallValidator.Validate(_validationForm.SluttrapportForAvfall, _validationForm.Overgangsordning, _attachmentsTypesName);
            AccumulateValidationMessages(sluttrapportForAvfallValidationResult.ValidationMessages);

            var overgangsordningResult = _overgangsordningValidator.Validate(_validationForm.Overgangsordning);
            AccumulateValidationMessages(overgangsordningResult.ValidationMessages);

            ValidateDataRelations(_validationForm);
        }

        public ValidationResult ValidateEntityFields(SluttrapportForBygningsAvfall form)
        {
            if (form.AvfallEtterRiving == null)
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.avfallEtterRiving);

            if (Helpers.ObjectIsNullOrEmpty(form.BruksarealAvfall))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.bruksarealAvfall);
            return ValidationResult;
        }

        public ValidationResult ValidateDataRelations(SluttrapportForBygningsAvfall form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.AnsvarligSoeker) && Helpers.ObjectIsNullOrEmpty(form.Tiltakshaver))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.Soeker);
            }
            else
            {
                if (!Helpers.ObjectIsNullOrEmpty(form.AnsvarligSoeker))
                {
                    var ansvarligSoekerValidationResult = _ansvarligSoekerValidator.Validate(form.AnsvarligSoeker);
                    AccumulateValidationMessages(ansvarligSoekerValidationResult.ValidationMessages);
                }
                else
                {
                    var tiltakshaverValidationResult = _tiltakshaverValidator.Validate(form.Tiltakshaver);
                    AccumulateValidationMessages(tiltakshaverValidationResult.ValidationMessages);
                }
            }

            return ValidationResult;
        }

        protected override IEnumerable<string> GetFormTiltakstyper()
        { return _tiltakstypes; }

        protected override void CustomFormValidation(ValidationInput validationInput)
        {
            var planMengdeAvfallPerArealXpath = Helpers.GetEntityXpath(_avfallplanSorteringValidator._entityXPath, null, FieldNameEnum.mengdeAvfallPerAreal);
            RemoveNotRelevantMessagesFromValidationResult(planMengdeAvfallPerArealXpath);

            // https://stackoverflow.com/a/10443540
            int[] indexArray = _validationForm?.SluttrapportForAvfall?.Avfallrapport?.Select((b, i) => b.Avfallklasse?.Kodeverdi != "ordinaertAvfall" ? i : -1).Where(i => i != -1).ToArray();

            if (indexArray != null && indexArray.Any())
            {
                foreach (var index in indexArray)
                {
                    var disponeringmaateXpath = Helpers.GetEntityXpath(_avfalldisponeringValidator._entityXPath, index, null, EntityValidatorEnum.DisponeringsmaateValidator);
                    RemoveNotRelevantMessagesFromValidationResult(disponeringmaateXpath, null, true);
                }
            }
        }

        protected override DateTime? GetFormValidationDate()
        {
            return _validationDate ?? DateTime.Now;
        }
    }
}