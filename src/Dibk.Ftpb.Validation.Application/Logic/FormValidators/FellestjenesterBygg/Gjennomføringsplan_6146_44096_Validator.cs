using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Utils;
using MatrikkelWSClient.Services;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.FormValidators.FellestjenesterBygg;

[FormData(DataFormatId = "6146", DataFormatVersion = "44096", XsdFileName = "gjennomforingsplan7", ProcessCategory = "GFP", ServiceDomain = ServiceDomain.FTB)]
public class Gjennomføringsplan_6146_44096_Validator : FormValidatorBase, IFormValidator
{
    private string[] _tiltakstyper;
    private List<EntityValidatorNode> _entitiesNodeList;
    private ICodeListService _codeListService;
    private IMatrikkelProvider _matrikkelProvider;
    private readonly IPostalCodeService _postalCodeService;
    private readonly IDecryptionFactory _decryptionFactory;

    private MetadataGjennomfoeringsplanValidator _metadataValidator;
    private IEiendomByggestedValidator _eiendomByggestedValidator;
    private IEiendomsAdresseValidator _eiendomsAdresseValidator;
    private IEiendomsidentifikasjonValidator _eiendomsidentifikasjonValidator;
    private ISaksnummerValidator _kommunensSaksnummerValidator;
    private IAktoerValidator _ansvarligSoekerValidator;
    private IKodelisteValidator _ansvarligSoekerPartstypeValidator;
    private AnsvarligSoekerTiltaksklasseValidator _ansvarligSoekerTiltaksklasseValidator;
    private IAktoerValidator _foretakValidator;
    private FunksjonValidator _funksjonValidator;
    private TiltaksklasseValidator _tiltaksklasseValidator;
    private AnsvarsomraadeStatusValidator _ansvarsomraadeStatusValidator;
    private IKodelisteValidator _partstypeValidator;
    private AnsvarsomraadeGjennomfoeringsplanValidator _ansvarsomraadeValidator;

    public Gjennomføringsplan_6146_44096_Validator
    (
        IValidationMessageComposer validationMessageComposer,
        IChecklistService checklistService,
        ICodeListService codeListService,
        IMatrikkelProvider matrikkelProvider,
        IPostalCodeService postalCodeService,
        IDecryptionFactory decryptionFactory
    ) : base(validationMessageComposer, checklistService)
    {
        _codeListService = codeListService;
        _matrikkelProvider = matrikkelProvider;
        _postalCodeService = postalCodeService;
        _decryptionFactory = decryptionFactory;
        _tiltakstyper = Array.Empty<string>();
        _entitiesNodeList = new List<EntityValidatorNode>();

        base.InitializeFormValidator<Gjennomfoeringsplan>();

    }

    protected override void InitializeValidatorConfig()
    {
        //Metadata
        var metadataValidatorNodeList = new List<EntityValidatorNode>
        {
            new() { NodeId = 1, EnumId = EntityValidatorEnum.MetadataGjennomfoeringsplanValidator, ParentID = null }
        };
        _entitiesNodeList.AddRange(metadataValidatorNodeList);

        //Eiendombyggested
        var eiendomByggestedValidatorNodeList = new List<EntityValidatorNode>
        {
            new() { NodeId = 2, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
            new() { NodeId = 3, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 2 },
            new() { NodeId = 4, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 2 },
        };
        _entitiesNodeList.AddRange(eiendomByggestedValidatorNodeList);

        //Kommunens Saksnummer
        var kommunensSaksnummerValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 5, EnumId = EntityValidatorEnum.KommunensSaksnummerValidator, ParentID = null}
            };
        _entitiesNodeList.AddRange(kommunensSaksnummerValidatorNodeList);

        //AnsvarligSoeker
        var ansvarligSoekervalidatorNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 6, EnumId = EntityValidatorEnum.AnsvarligSoekerValidator, ParentID = null},
                new () {NodeId = 7, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 6},
            };
        _entitiesNodeList.AddRange(ansvarligSoekervalidatorNodeList);

        //AnsvarligSoekerTiltaksklasse
        var ansvarligSoekerTiltaksklasseValidatorNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 8, EnumId = EntityValidatorEnum.AnsvarligSoekerTiltaksklasseValidator, ParentID = null},
            };
        _entitiesNodeList.AddRange(ansvarligSoekerTiltaksklasseValidatorNodeList);

        //Gjennomføringsplan
        var gjennomfoeringsplanValidatorNodeList = new List<EntityValidatorNode>
        {
            new () {NodeId = 9, EnumId = EntityValidatorEnum.AnsvarsomraadeGjennomfoeringsplanValidator, ParentID = null},
            new () {NodeId = 10, EnumId = EntityValidatorEnum.FunksjonValidator, ParentID = 9},
            new () {NodeId = 11, EnumId = EntityValidatorEnum.TiltaksklasseValidator, ParentID = 9},
            new () {NodeId = 12, EnumId = EntityValidatorEnum.ForetakValidator, ParentID = 9},
            new () {NodeId = 13, EnumId = EntityValidatorEnum.AnsvarsomraadeStatusValidator, ParentID = 9},
            new () {NodeId = 14, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 12},
        };
        _entitiesNodeList.AddRange(gjennomfoeringsplanValidatorNodeList);
    }

    protected override IEnumerable<string> GetFormTiltakstyper()
    {
        return _tiltakstyper;
    }

    protected override DateTime? GetFormValidationDate()
    {
        return DateTime.Now;
    }

    protected override void InstantiateValidators()
    {
        var tree = EntityValidatorTree.BuildTree(_entitiesNodeList);

        //Metadata
        _metadataValidator = new MetadataGjennomfoeringsplanValidator(tree);

        //EiendomByggested
        _eiendomsAdresseValidator = new EiendomsAdresseValidator(tree);
        _eiendomsidentifikasjonValidator = new EiendomsidentifikasjonValidator(tree, _codeListService, _matrikkelProvider);
        _eiendomByggestedValidator = new EiendomByggestedValidator(tree, _eiendomsAdresseValidator, _eiendomsidentifikasjonValidator, _matrikkelProvider, true);

        //Kommunens Saksnummer
        _kommunensSaksnummerValidator = new KommunensSaksnummerValidator(tree);

        //AnsvarligSoeker
        _ansvarligSoekerPartstypeValidator = new PartstypeValidator(tree, 7, _codeListService);
        _ansvarligSoekerValidator = new AnsvarligSoekerValidator(tree, null, null, _ansvarligSoekerPartstypeValidator, _codeListService, _decryptionFactory, null, false, true);

        //AnsvarligSoekerTiltaksklasse
        _ansvarligSoekerTiltaksklasseValidator = new AnsvarligSoekerTiltaksklasseValidator(tree, 8, _codeListService);

        //Gjennomføringsplan
        _partstypeValidator = new PartstypeValidator(tree, 14, _codeListService);
        _foretakValidator = new ForetakValidator(tree, null, null, _partstypeValidator, _codeListService, _decryptionFactory, new[] {"Privatperson", "Foretak"}, true,
            useUtfyltRule: false); // The utfylt rule is handled in this validator, see the //Gjennomføringsplan section in the Validate method
        _funksjonValidator = new FunksjonValidator(tree, 10, _codeListService);
        _tiltaksklasseValidator = new TiltaksklasseValidator(tree, 11, _codeListService);
        _ansvarsomraadeStatusValidator = new AnsvarsomraadeStatusValidator(tree, 13, _codeListService);
        _ansvarsomraadeValidator = new AnsvarsomraadeGjennomfoeringsplanValidator(tree, _funksjonValidator, _tiltaksklasseValidator, _foretakValidator, _ansvarsomraadeStatusValidator);
    }

    protected override void Validate(ValidationInput validationInput)
    {
        var form = SerializeUtil.DeserializeFromString<GjennomfoeringsplanV7>(validationInput.FormData, false);

        // Versjon
        if (string.IsNullOrWhiteSpace(form.Versjon))
        {
            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.versjon);
        }

        // Metadata
        var metadataValidationResult = _metadataValidator.Validate(form.Metadata);
        AccumulateValidationMessages(metadataValidationResult.ValidationMessages);

        // EiendomByggested
        var eiendomIndex = GetArrayIndex(form.EiendomByggested);
        for (int i = 0; i < eiendomIndex; i++)
        {
            var eiendomByggested = Helpers.ObjectIsNullOrEmpty(form.EiendomByggested) ? null : form.EiendomByggested[i];
            var eiendomByggestedResult = _eiendomByggestedValidator.Validate(eiendomByggested);
            AccumulateValidationMessages(eiendomByggestedResult.ValidationMessages, i);
        }

        //Kommunenes Saksnummer
        var kommunesSaksnummerResult = _kommunensSaksnummerValidator.Validate(form.KommunensSaksnummer);
        AccumulateValidationMessages(kommunesSaksnummerResult.ValidationMessages);

        //AnsvarligSoeker
        var ansvarligSoekerResult = _ansvarligSoekerValidator.Validate(form.AnsvarligSoeker);
        AccumulateValidationMessages(ansvarligSoekerResult.ValidationMessages);

        //AnsvarligSoekerTiltaksklasse
        var ansvarligSoekerTiltaksklasseResult = _ansvarligSoekerTiltaksklasseValidator.Validate(form.AnsvarligSoekerTiltaksklasse);
        AccumulateValidationMessages(ansvarligSoekerTiltaksklasseResult.ValidationMessages);

        //Gjennomføringsplan
        if (form.Gjennomfoeringsplan == null)
        {
            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.gjennomfoeringsplan);
        }
        else
        {
            var hasAtLeastOneForetak = false;
            var index = GetArrayIndex(form.Gjennomfoeringsplan);
            for (int i = 0; i < index; i++)
            {
                var ansvarsomraade = Helpers.ObjectIsNullOrEmpty(form.Gjennomfoeringsplan) ? null : form.Gjennomfoeringsplan[i];
                var ansvarsomraadedResult = _ansvarsomraadeValidator.Validate(ansvarsomraade);

                if (ansvarsomraade?.Foretak != null)
                    hasAtLeastOneForetak = true;

                AccumulateValidationMessages(ansvarsomraadedResult.ValidationMessages, i);
            }
            if (!hasAtLeastOneForetak && !Helpers.ObjectIsNullOrEmpty(form.Gjennomfoeringsplan))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.ansvarsomraaderForetak);
        }
    }

    protected override void DefineValidationRules()
    {
        //Skjemavalidering
        AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.xml);

        //Versjon
        AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.versjon);

        //Metadata
        AccumulateValidationRules(_metadataValidator.ValidationResult.ValidationRules);

        //EiendomByggested
        AccumulateValidationRules(_eiendomByggestedValidator.ValidationResult.ValidationRules);

        //Kommunens Saksnummer
        AccumulateValidationRules(_kommunensSaksnummerValidator.ValidationResult.ValidationRules);

        //AnsvarligSoeker
        AccumulateValidationRules(_ansvarligSoekerValidator.ValidationResult.ValidationRules);

        //AnsvarligSoekerTiltaksklasse
        AccumulateValidationRules(_ansvarligSoekerTiltaksklasseValidator.ValidationResult.ValidationRules);

        //Gjennomføringsplan
        AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.gjennomfoeringsplan);
        AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.ansvarsomraaderForetak);
        AccumulateValidationRules(_ansvarsomraadeValidator.ValidationResult.ValidationRules);
    }

    protected override void CustomFormValidation(ValidationInput validationInput)
    {
        // Ikke i bruk for denne tjenesten
    }
}