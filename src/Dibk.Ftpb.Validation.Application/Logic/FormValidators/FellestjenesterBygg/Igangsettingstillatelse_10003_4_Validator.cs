using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Utils;
using MatrikkelWSClient.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.FormValidators.FellestjenesterBygg
{
    [FormData(DataFormatId = "10003", DataFormatVersion = "4", XsdFileName = "igangsettingstillatelseV4", ProcessCategory = "IG", ServiceDomain = ServiceDomain.FTB)]
    public class Igangsettingstillatelse_10003_4_Validator : FormValidatorBase, IFormValidator
    {
        private readonly IValidationMessageComposer _validationMessageComposer;
        private readonly ICodeListService _codeListService;
        private readonly IMatrikkelProvider _matrikkelProvider;
        private readonly IChecklistService _checklistService;
        private readonly IPostalCodeService _postalCodeService;
        private readonly IDecryptionFactory _decryptionFactory;

        private string[] _tiltakstypes;
        private List<EntityValidatorNode> _entitiesNodeList;
        private readonly string _formName = "igangsettingstillatelse";
        private ISaksnummerValidator _kommunensSaksnummerValidator;

        //Generelle vilkår
        private IGenerelleVilkaarValidator _generelleVilkaarValidator;

        //Vedlegg
        private VedleggIGValidator _vedleggValidator;

        //soeknadGjelder
        private SoeknadGjelderValidator _soeknadGjelderValidator;

        private IKodelisteValidator _typeKodelisteValidator;

        //UtfallBesvarelse
        private IUtfallBesvarelseValidator _utfallBesvarelseValidator;

        private IKodelisteValidator _utfallTypeValidator;
        private IKodelisteValidator _vedleggsTypeValidator;
        private IVedleggslisteValidator _vedleggslisteValidator;

        // utloestFraSjekkpunkt
        private IUtloestFraSjekkpunktValidator _utloestFraSjekkpunktValidator;

        //Tema
        private IKodelisteValidator _temaKodelisteValidator;

        //EiendomByggested
        private IEiendomsidentifikasjonValidator _eiendomsidentifikasjonValidator;

        private IEiendomsAdresseValidator _eiendomsAdresseValidator;
        private IEiendomByggestedValidator _eiendomByggestedValidator;

        //Metadata
        private IMetadataByggValidator _metadataByggValidator;

        private IKodelisteValidator _foretrukketspraakTypeValidator;

        //Delsoeknader
        private IDelsoeknaderValidator _delsoeknaderValidator;

        private IKodelisteValidator _delsoeknadstypeValidator;

        //AnsvarligSoeker
        private IAktoerValidator _ansvarligSoekerValidator;

        private IEnkelAdresseValidator _ansvarligSoekerEnkelAdresseValidator;
        private IKodelisteValidator _ansvarligSoekerPartstypeValidator;
        private IKontaktpersonValidator _ansvarligSoekerKontaktpersonValidator;

        //Ansvar for byggesaken
        private IKodelisteValidator _ansvarForByggesakenValidator;

        //Avsender
        private ISubmitterValidator _submitterValidator;

        public Igangsettingstillatelse_10003_4_Validator(
            IValidationMessageComposer validationMessageComposer,
            ICodeListService codeListService,
            IMatrikkelProvider matrikkelProvider,
            IChecklistService checklistService,
            IPostalCodeService postalCodeService,
            IDecryptionFactory decryptionFactory
            ) : base(validationMessageComposer, checklistService)
        {
            _validationMessageComposer = validationMessageComposer;
            _codeListService = codeListService;
            _matrikkelProvider = matrikkelProvider;
            _checklistService = checklistService;
            _postalCodeService = postalCodeService;
            _decryptionFactory = decryptionFactory;
            _entitiesNodeList = new List<EntityValidatorNode>();
            _tiltakstypes = new string[] { };
            base.InitializeFormValidator<Igangsettingstillatelse>();
        }

        protected override void InitializeValidatorConfig()
        {
            //Kommunenes Saksnummer
            var kommunenesSaksnummerValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 1, EnumId = EntityValidatorEnum.KommunensSaksnummerValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(kommunenesSaksnummerValidatorNodeList);

            //Generelle vilkår
            var generelleVilkaarValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 2, EnumId = EntityValidatorEnum.GenerelleVilkaarIgV3Validator, ParentID = null}
            };
            _entitiesNodeList.AddRange(generelleVilkaarValidatorNodeList);

            //Vedlegg
            var vedleggValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 3, EnumId = EntityValidatorEnum.VedleggIGValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(vedleggValidatorNodeList);

            //soeknadGjelder
            var soeknadGjelderValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 4, EnumId = EntityValidatorEnum.SoeknadGjelderValidator, ParentID = null},
                new() {NodeId = 5, EnumId = EntityValidatorEnum.TypeValidator, ParentID = 4}
            };
            _entitiesNodeList.AddRange(soeknadGjelderValidatorNodeList);

            //UtfallBesvarelse
            var utfallBesvarelseValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 6, EnumId = EntityValidatorEnum.UtfallBesvarelseValidator, ParentID = null},
                new() {NodeId = 7, EnumId = EntityValidatorEnum.UtfallTypeValidator, ParentID = 6},
                new() {NodeId = 8, EnumId = EntityValidatorEnum.UtloestFraSjekkpunktValidator, ParentID = 6},
                new() {NodeId = 9, EnumId = EntityValidatorEnum.TemaValidator, ParentID = 6},
                new() {NodeId = 10, EnumId = EntityValidatorEnum.VedleggslisteValidator, ParentID = 6},
                new() {NodeId = 11, EnumId = EntityValidatorEnum.VedleggsTypeValidator, ParentID = 10},
            };
            _entitiesNodeList.AddRange(utfallBesvarelseValidatorNodeList);

            // EiendomByggested
            List<EntityValidatorNode> eiendombyggestedNodeList = new()
            {
                new() { NodeId = 13, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
                new() { NodeId = 14, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 13 },
                new() { NodeId = 15, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 13 },
            };
            _entitiesNodeList.AddRange(eiendombyggestedNodeList);

            //Metadata
            var metadataByggValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 16, EnumId = EntityValidatorEnum.MetadataByggValidator, ParentID = null},
                new() {NodeId = 17, EnumId = EntityValidatorEnum.ForetrukketspraakTypeValidator, ParentID = 16}
            };
            _entitiesNodeList.AddRange(metadataByggValidatorNodeList);

            //Delsoeknader
            var delsoeknaderValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 18, EnumId = EntityValidatorEnum.DelsoeknaderValidator, ParentID = null},
                new() {NodeId = 19, EnumId = EntityValidatorEnum.TypeValidator, ParentID = 18}
            };
            _entitiesNodeList.AddRange(delsoeknaderValidatorNodeList);

            //AnsvarligSoeker
            var ansvarligSoekervalidatorNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 20, EnumId = EntityValidatorEnum.AnsvarligSoekerValidator, ParentID = null},
                new () {NodeId = 21, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 20},
                new () {NodeId = 22, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 20},
                new () {NodeId = 23, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 20}
            };
            _entitiesNodeList.AddRange(ansvarligSoekervalidatorNodeList);

            //Ansvar for byggesaken
            var ansvarForByggesakenValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 24, EnumId = EntityValidatorEnum.AnsvarForByggesakenValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(ansvarForByggesakenValidatorNodeList);

            //Avsender
            var submitterValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 25, EnumId = EntityValidatorEnum.SubmitterValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(submitterValidatorNodeList);
        }

        protected override IEnumerable<string> GetFormTiltakstyper()
        {
            return _tiltakstypes;
        }

        protected override void InstantiateValidators()
        {
            var tree = EntityValidatorTree.BuildTree(_entitiesNodeList);

            _kommunensSaksnummerValidator = new KommunensSaksnummerValidator(tree);
            _generelleVilkaarValidator = new GenerelleVilkaarIgV3Validator(tree);
            _vedleggValidator = new VedleggIGValidator(tree);

            _typeKodelisteValidator = new TypeValidator(tree, 5, _codeListService,
                preconditionKodeverdi: "/ansvarForByggesaken/kodeverdi",
                preconditionKodebeskrivelse: "/soeknadGjelder/type/kode{0}/kodeverdi",
                preconditionValidert: "/soeknadGjelder/type");
            _soeknadGjelderValidator = new SoeknadGjelderValidator(tree, _typeKodelisteValidator, null, _formName);

            var tillatUtfallstype = new[] { "SVV", "MKV" };
            _utfallTypeValidator = new UtfallTypeValidator(tree, 7, _codeListService, false, tillatUtfallstype);
            _utloestFraSjekkpunktValidator = new UtloestFraSjekkpunktValidator(tree);
            _temaKodelisteValidator = new TemaValidator(tree, 9, _codeListService, true);
            _vedleggsTypeValidator = new VedleggsTypeValidator(tree, 11, _codeListService, false);
            _vedleggslisteValidator = new VedleggslisteValidator(tree, _vedleggsTypeValidator);
            _utfallBesvarelseValidator = new UtfallBesvarelseValidator(tree, _utfallTypeValidator, _utloestFraSjekkpunktValidator, _temaKodelisteValidator, _vedleggslisteValidator);

            //EiendomByggested
            _eiendomsAdresseValidator = new EiendomsAdresseValidator(tree);
            _eiendomsidentifikasjonValidator = new EiendomsidentifikasjonValidator(tree, _codeListService, _matrikkelProvider);
            _eiendomByggestedValidator = new EiendomByggestedValidator(tree, _eiendomsAdresseValidator, _eiendomsidentifikasjonValidator, _matrikkelProvider, true);

            //Metadata
            _foretrukketspraakTypeValidator = new ForetrukketspraakTypeValidator(tree, 17, _codeListService, true);
            _metadataByggValidator = new MetadataByggValidator(tree, _foretrukketspraakTypeValidator);

            //Delsoeknader
            _delsoeknadstypeValidator = new TypeValidator(tree, 19, _codeListService, allowedNull: true);
            _delsoeknaderValidator = new DelsoeknaderValidator(tree, _delsoeknadstypeValidator);

            //*AnsvarligSoeker
            _ansvarligSoekerKontaktpersonValidator = new KontaktpersonValidator(tree, 21);
            _ansvarligSoekerPartstypeValidator = new PartstypeValidator(tree, 22, _codeListService);
            _ansvarligSoekerEnkelAdresseValidator = new EnkelAdresseValidator(tree, 23, _postalCodeService);
            _ansvarligSoekerValidator = new AnsvarligSoekerValidator(tree, _ansvarligSoekerEnkelAdresseValidator, _ansvarligSoekerKontaktpersonValidator, _ansvarligSoekerPartstypeValidator, _codeListService, _decryptionFactory);

            //Ansvar for byggesaken
            var ansvarForByggesaken = new[] { "medAnsvar", "selvbygger" };
            _ansvarForByggesakenValidator = new AnsvarForByggesakenValidator(tree, 24, _codeListService, false, ansvarForByggesaken);

            //Avsender
            _submitterValidator = new SubmitterValidator(tree, _decryptionFactory, false);
        }

        protected override void DefineValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.xml);

            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.utfallBesvarelse);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.delsoeknader, "/soeknadGjelder/delsoeknadsnummer > 1");

            AccumulateValidationRules(_kommunensSaksnummerValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_generelleVilkaarValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_vedleggValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_soeknadGjelderValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_utfallBesvarelseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_eiendomByggestedValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_metadataByggValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_delsoeknaderValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarligSoekerValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarForByggesakenValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_submitterValidator.ValidationResult.ValidationRules);
        }

        protected override DateTime? GetFormValidationDate()
        {
            return DateTime.Now;
        }

        protected override void Validate(ValidationInput validationInput)
        {
            var form = SerializeUtil.DeserializeFromString<Igangsettingstillatelse>(validationInput.FormData, false);

            var kommunesSaksnummerResult = _kommunensSaksnummerValidator.Validate(form.KommunensSaksnummer);
            AccumulateValidationMessages(kommunesSaksnummerResult.ValidationMessages);

            var generelleVilkaarResult = _generelleVilkaarValidator.Validate(form.GenerelleVilkaar);
            AccumulateValidationMessages(generelleVilkaarResult.ValidationMessages);

            var vedleggResult = _vedleggValidator.Validate(validationInput.Attachments, form);
            AccumulateValidationMessages(vedleggResult.ValidationMessages);

            var soeknadGjelderResult = _soeknadGjelderValidator.Validate(form.SoeknadGjelder, form.AnsvarForByggesaken);
            AccumulateValidationMessages(soeknadGjelderResult.ValidationMessages);

            _tiltakstypes = _soeknadGjelderValidator.Tiltakstypes.ToArray();

            if (form.UtfallBesvarelse == null)
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.utfallBesvarelse);
            }
            else
            {
                if (!int.TryParse(form.SoeknadGjelder.Delsoeknadsnummer, out var delsoeknadsnummer))
                    delsoeknadsnummer = 0;

                var index = GetArrayIndex(form.UtfallBesvarelse);
                for (int i = 0; i < index; i++)
                {
                    var utfallSvar = Helpers.ObjectIsNullOrEmpty(form.UtfallBesvarelse) ? null : form.UtfallBesvarelse[i];
                    if (utfallSvar == null)
                        AddMessageFromRule(ValidationRuleEnum.utfylt, "/utfallBesvarelse");
                    var utfallBesvarelseResult = _utfallBesvarelseValidator.Validate(utfallSvar, delsoeknadsnummer);
                    AccumulateValidationMessages(utfallBesvarelseResult.ValidationMessages, i);
                }
            }

            var eiendomIndex = GetArrayIndex(form.EiendomByggested);
            for (int i = 0; i < eiendomIndex; i++)
            {
                var eiendomByggested = Helpers.ObjectIsNullOrEmpty(form.EiendomByggested) ? null : form.EiendomByggested[i];
                var eiendomByggestedResult = _eiendomByggestedValidator.Validate(eiendomByggested);
                AccumulateValidationMessages(eiendomByggestedResult.ValidationMessages, i);
            }

            //Metadata
            var metadataByggResult = _metadataByggValidator.Validate(form.Metadata);
            AccumulateValidationMessages(metadataByggResult.ValidationMessages);

            //AnsvarligSoeker
            var ansvarligSoekerResult = _ansvarligSoekerValidator.Validate(form.AnsvarligSoeker);
            AccumulateValidationMessages(ansvarligSoekerResult.ValidationMessages);

            //Ansvar for byggesaken
            var ansvarForByggesakenResult = _ansvarForByggesakenValidator.Validate(form.AnsvarForByggesaken);
            AccumulateValidationMessages(ansvarForByggesakenResult.ValidationMessages);

            ValidateDataRelations(form, validationInput.AuthenticatedSubmitter);
        }

        private void ValidateDataRelations(Igangsettingstillatelse form, string authenticatedSubmitter)
        {
            var delsoeknadnummerXpath = $"{_soeknadGjelderValidator._entityXPath}/{FieldNameEnum.delsoeknadsnummer}";
            int delsoknadsnummer = 0;
            if (!IsAnyValidationMessagesWithXpath(delsoeknadnummerXpath))
                int.TryParse(form.SoeknadGjelder?.Delsoeknadsnummer, out delsoknadsnummer);

            if (delsoknadsnummer != 1)
            {
                if (form.Delsoeknader == null)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.delsoeknader, new[] { _formName });
                }
                else
                {
                    var index = GetArrayIndex(form.Delsoeknader);
                    for (int i = 0; i < index; i++)
                    {
                        var delsoeknad = Helpers.ObjectIsNullOrEmpty(form.Delsoeknader) ? null : form.Delsoeknader[i];
                        var delsoeknadResult = _delsoeknaderValidator.Validate(delsoeknad);
                        AccumulateValidationMessages(delsoeknadResult.ValidationMessages, i);
                    }
                }
            }

            if (IsAnyValidationMessagesWithXpath("/ansvarligSoeker/partstype") || IsAnyValidationMessagesWithXpath("/ansvarligSoeker/partstype/kodeverdi"))
                return;
            else
            {
                var submitterValidationResult = _submitterValidator.Validate(null, form.AnsvarligSoeker, authenticatedSubmitter);
                AccumulateValidationMessages(submitterValidationResult.ValidationMessages);
            }
        }

        protected override void CustomFormValidation(ValidationInput validationInput)
        {
            //throw new NotImplementedException();
        }
    }
}