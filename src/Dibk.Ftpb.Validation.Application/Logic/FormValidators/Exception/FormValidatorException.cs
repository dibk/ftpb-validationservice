﻿using System;
using System.Runtime.Serialization;

namespace Dibk.Ftpb.Validation.Application.Logic.FormValidators
{
    public class FormValidatorException : Exception
    {
        public FormValidatorException()
        {
        }

        public FormValidatorException(string message) : base(message)
        {
        }

        public FormValidatorException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FormValidatorException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
