using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Validation.Application.Logic.FormValidators
{
    public abstract class FormValidatorBase
    {
        private readonly IValidationMessageComposer _validationMessageComposer;
        private readonly IChecklistService _checklistService;

        protected ValidationResult ValidationResult;

        protected FormDataAttribute FormDataAttribute;

        protected string XPathRoot;

        protected abstract void InitializeValidatorConfig();
        protected abstract IEnumerable<string> GetFormTiltakstyper();
        protected abstract DateTime? GetFormValidationDate();
        protected abstract void InstantiateValidators();
        protected abstract void Validate(ValidationInput validationInput);
        protected abstract void DefineValidationRules();
        protected abstract void CustomFormValidation(ValidationInput validationInput);

        public ValidationRule[] FormValidationRules;

        protected FormValidatorBase(IValidationMessageComposer validationMessageComposer, IChecklistService checklistService = null)
        {
            _validationMessageComposer = validationMessageComposer;
            _checklistService = checklistService;
            ValidationResult = new ValidationResult
            {
                ValidationMessages = new List<ValidationMessage>(),
                ValidationRules = new List<ValidationRule>()
            };
        }

        protected void InitializeFormValidator<T>()
        {
            //Get 'DataFormatId' and 'DataFormatVersion' info from validator
            var customAttributes = GetFormDataAttribute();

            //GetRootXmlNode name
            XPathRoot = GetXPathRoot<T>();

            InitializeValidatorConfig();
            InstantiateValidators();
            DefineValidationRules();

            ValidationResult.ValidationRules = _validationMessageComposer.ComposeValidationRules(XPathRoot, customAttributes.DataFormatId, customAttributes.DataFormatVersion, ValidationResult?.ValidationRules, "NO", customAttributes.ServiceDomain).ToList();
            if (_checklistService != null)
            {
                FormValidationRules = _checklistService.UpdateValidationRulesWithCheckListNumber(customAttributes.ProcessCategory, customAttributes.ServiceDomain, ValidationResult?.ValidationRules?.ToArray());
            }
            else
            {
                FormValidationRules = ValidationResult?.ValidationRules?.ToArray();
            }
        }

        public virtual FormDataAttribute GetFormDataAttribute()
        {
            FormDataAttribute = this.GetType().GetCustomAttributes(typeof(FormDataAttribute), true).FirstOrDefault() as FormDataAttribute;
            return FormDataAttribute;
        }

        private string GetXPathRoot<T>()
        {
            var xmlRootElement = typeof(T).GetCustomAttributes(typeof(XmlRootAttribute), true).SingleOrDefault() as XmlRootAttribute;
            return xmlRootElement?.ElementName;
        }

        public virtual ValidationResult StartValidation(ValidationInput validationInput)
        {
            var xmlValidationResult = ValidateXmlStructure(validationInput.FormData);
            if (!string.IsNullOrEmpty(xmlValidationResult))
            {
                AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.xml, new[] { xmlValidationResult });
                ValidationResult = AddValidationMessageFromFormRules();

                return ValidationResult;
            }

            Validate(validationInput);

            CustomFormValidation(validationInput);

            ValidationResult.TiltakstyperISoeknad = GetFormTiltakstyper().ToList();
            ValidationResult.ValidationDate = GetFormValidationDate();
            ValidationResult = AddValidationMessageFromFormRules();

            if (!ValidationResult.TiltakstyperISoeknad.Contains("N/A") && _checklistService != null)
                FilterValidationMessagesOnTiltakstyper();

            return ValidationResult;
        }

        private string ValidateXmlStructure(string xml)
        {
            var validationForm = new XmlValidation().ValidateXml(xml, FormDataAttribute.XsdFileName);

            string errorsInXml = null;

            if (validationForm.ContainsKey("Error"))
            {
                errorsInXml = string.Join("; ", validationForm["Error"]);
            }
            else if (validationForm.ContainsKey("Warning"))
            {
                if (validationForm["Warning"] != null & validationForm["Warning"].Any(e => e.Contains("Could not find schema information for the element", StringComparison.CurrentCultureIgnoreCase)))
                {
                    errorsInXml = "XML have invalid element's";
                }
            }
            return errorsInXml;
        }
        protected void AccumulateValidationRules(List<ValidationRule> validationRules)
        {
            var whereNotAlreadyExists = validationRules
                .Where(x => !ValidationResult.ValidationRules
                    .Any(y =>
                        y.XpathField == x.XpathField &&
                        y.Rule == x.Rule &&
                        x.ValidFrom == y.ValidFrom &&
                        x.ValidTo == y.ValidTo &&
                        x.PreCondition == y.PreCondition
                    ));

            ValidationResult.ValidationRules.AddRange(whereNotAlreadyExists);
        }

        protected void AccumulateValidationMessages(List<ValidationMessage> validationMessages, int? index = null)
        {
            if (index.HasValue)
            {
                foreach (var message in validationMessages)
                {
                    var messageXpathField = Helpers.ReplaceCurlyBracketInXPath(index.Value, message.XpathField);
                    message.XpathField = messageXpathField;

                    var preConditionXpathField = Helpers.ReplaceCurlyBracketInXPath(index.Value, message.PreCondition);
                    message.PreCondition = preConditionXpathField;
                }
            }

            ValidationResult.ValidationMessages.AddRange(validationMessages);
        }

        private void FilterValidationMessagesOnTiltakstyper()
        {
            if (_checklistService == null) return;

            var soeknadTiltakstype = ValidationResult.TiltakstyperISoeknad;
            if(soeknadTiltakstype == null || !soeknadTiltakstype.Any()) return;

            var rulesWithChecklistReference = ValidationResult.ValidationMessages.Where(r => !string.IsNullOrEmpty(r.ChecklistReference)).ToList();
            foreach (var validationMessage in rulesWithChecklistReference)
            {
                var sjekkpunktTiltakstype = _checklistService.GetSjekkpunktTiltakstype(FormDataAttribute.ProcessCategory, FormDataAttribute.ServiceDomain, validationMessage.ChecklistReference);
                if (sjekkpunktTiltakstype != null && !sjekkpunktTiltakstype.Any(soeknadTiltakstype.Contains))
                    ValidationResult.ValidationMessages.Remove(validationMessage);
            }
        }

        protected static int GetArrayIndex(object[] objectArray)
        {
            if (objectArray == null || objectArray.Length == 0)
                return 1;

            return objectArray.Length;
        }

        protected void AddValidationRule(ValidationRuleEnum rule, FieldNameEnum? xmlElement = null, string xpathPrecondition = null, string overrideXpath = null, DateTime? validFrom = null, DateTime? validTo = null)
        {
            //Note: different implementation than in the EntityValidatorBase

            var validationRuleTypeId = EnumHelper.GetRuleNumberFromValidationRuleEmum(rule);
            var fieldNumberString = String.Empty;

            if (xmlElement != null)
            {
                var fieldNameNumber = EnumHelper.GetEnumFieldNameNumber(xmlElement.Value);  //GetEnumEntityValidatorNumber
                fieldNumberString = $".{fieldNameNumber}";

            }

            var elementRuleId = $"{fieldNumberString}.{validationRuleTypeId}";

            //TODO Is this relevant now?
            var separator = xmlElement.HasValue ? "/" : "";

            string xPath = $"{overrideXpath}{separator}{EnumHelper.GetXmlNodeFromFieldNameEnum(xmlElement)}";
            
            ValidationResult.ValidationRules.Add(new ValidationRule() { Rule = rule.ToString(), XpathField = xPath, XmlElement = xmlElement?.ToString(), Id = elementRuleId, PreCondition = xpathPrecondition, ValidFrom = validFrom, ValidTo = validTo });
        }

        protected void AddMessageFromRule(ValidationRuleEnum id, FieldNameEnum? fieldName = null, string[] messageParameters = null, string preConditionField = null, int? index = null)
        {
            var xpathNew = fieldName.HasValue ? $"/{EnumHelper.GetXmlNodeFromFieldNameEnum(fieldName)}" : null;

            AddMessageFromRule(id, xpathNew, messageParameters, preConditionField, index);
        }

        protected void AddMessageFromRule(ValidationRuleEnum id, string xmlElementPathName, string[] messageParameters = null, string preConditionField = null, int? index = null)
        {
            var idSt = id.ToString();
            var rule = RuleToValidate(idSt, xmlElementPathName, preConditionField);

            if (index.HasValue)
            {
                xmlElementPathName = Helpers.ReplaceCurlyBracketInXPath(index.Value, xmlElementPathName);
                preConditionField = Helpers.ReplaceCurlyBracketInXPath(index.Value, preConditionField);
            }
            var validationMessage = new ValidationMessage()
            {
                Rule = idSt,
                Reference = rule.Id,
                XpathField = xmlElementPathName,
                PreCondition = preConditionField,
                MessageParameters = messageParameters,
                Message = rule.Message,
                Messagetype = rule.Messagetype
            };
            ValidationResult.ValidationMessages.Add(validationMessage);
        }

        private ValidationRule RuleToValidate(string rule, string xPath, string preCondition = null)
        {
            //Note: different regex implementation than in the EntityValidatorBase
            xPath = Regex.Replace(xPath, @"\[\d+\]", "{0}");
            preCondition = string.IsNullOrEmpty(preCondition) ? null : Regex.Replace(preCondition, @"\[\d+\]", "{0}");

            ValidationRule validationRule;

            if (preCondition == null)
            {
                validationRule = ValidationResult.ValidationRules.Where(r => !string.IsNullOrEmpty(r.Rule))
                    .FirstOrDefault(r => r.Rule.Equals(rule) && (r.XpathField.EndsWith(xPath)));
            }
            else
            {
                validationRule = ValidationResult.ValidationRules.Where(r => !string.IsNullOrEmpty(r.Rule))
                    .FirstOrDefault(r => r.Rule.Equals(rule) && (r.XpathField.EndsWith(xPath)) && r.PreCondition != null && r.PreCondition.EndsWith(preCondition));
            }

            if (validationRule == null)
            {
                validationRule = new ValidationRule()
                {
                    Rule = rule,
                    Message = $"Can't find rule:'{rule}'.",
                    Messagetype = ValidationResultSeverityEnum.ERROR
                };
            }
            return validationRule;
        }

        private ValidationResult AddValidationMessageFromFormRules()
        {
            var messages = ValidationResult.ValidationMessages;
            foreach (var validationMessage in messages)
            {
                var composedValidationMessage = RuleToValidate(validationMessage.Rule, validationMessage.XpathField, validationMessage.PreCondition);
                validationMessage.Message = composedValidationMessage.Message;
                validationMessage.ChecklistReference = composedValidationMessage.ChecklistReference;
                string pattern = @"\{\d+\}";

                var match = Regex.Matches(composedValidationMessage.Message, pattern);

                string validationMessageText;
                if (match.Count > 0)
                {
                    var numberOfUniqueMatches = match.DistinctBy(m => m.Value).Count();

                    int messageParametersCount = 0;
                    if (validationMessage.MessageParameters != null && validationMessage.MessageParameters.Any())
                        messageParametersCount = validationMessage.MessageParameters.Length;

                    if (!numberOfUniqueMatches.Equals(messageParametersCount))
                    {
                        validationMessageText = $"The rule contains '{numberOfUniqueMatches}' unique parameters and the message has '{messageParametersCount}' parameters.";
                    }
                    else
                    {
                        try
                        {
                            validationMessageText = string.Format(composedValidationMessage.Message, validationMessage.MessageParameters);
                        }
                        catch (Exception e)
                        {
                            validationMessageText = $"Can't replace parameter in text for rule: {validationMessage.Rule} with xpath: {validationMessage.XpathField} .-";
                        }
                    }
                }
                else
                {
                    validationMessageText = composedValidationMessage.Message;
                }

                validationMessage.Message = validationMessageText;
                validationMessage.Messagetype = composedValidationMessage.Messagetype;

                //hvis preCondition er tom, bruk preCondition fra composedValidationMessage preCondition fra data base
                if (string.IsNullOrEmpty(validationMessage.PreCondition))
                    validationMessage.PreCondition = composedValidationMessage.PreCondition;
                
                if (!string.IsNullOrEmpty(validationMessage.PreCondition) && !validationMessage.PreCondition.StartsWith(XPathRoot))
                    validationMessage.PreCondition = $"{XPathRoot}{validationMessage.PreCondition}";

                if (validationMessage.XpathField != null && !validationMessage.XpathField.StartsWith(XPathRoot))
                    validationMessage.XpathField = $"{XPathRoot}{validationMessage.XpathField}";

                var dataFormatId = FormDataAttribute.DataFormatId;
                var dataFormatVersion = FormDataAttribute.DataFormatVersion;

                if (validationMessage.Reference != null && !validationMessage.Reference.StartsWith($"{dataFormatId}.{dataFormatVersion}"))
                    validationMessage.Reference = $"{dataFormatId}.{dataFormatVersion}{validationMessage.Reference}";
            }

            var sortedMessages = messages.OrderBy(m => m.Messagetype).ToList();
            ValidationResult.ValidationMessages = sortedMessages;

            return ValidationResult;
        }

        protected void RemoveNotRelevantRulesFromValidationRules(string ruleXpath, ValidationRuleEnum? validationRule = null, bool? removeAllChildElements = false)
        {
            if (ValidationResult?.ValidationRules == null) return;

            int[] indexArray;
            if (validationRule.HasValue)
                indexArray = ValidationResult?.ValidationRules?.Select((b, i) => b.XpathField.Equals(ruleXpath) && b.Rule.Equals(validationRule.Value.ToString()) ? i : -1).Where(i => i != -1).ToArray();
            else
            {
                if (removeAllChildElements.GetValueOrDefault(false))
                    indexArray = ValidationResult?.ValidationRules?.Select((b, i) => b.XpathField.StartsWith(ruleXpath) ? i : -1).Where(i => i != -1).ToArray();
                else
                    indexArray = ValidationResult?.ValidationRules?.Select((b, i) => b.XpathField.Equals(ruleXpath) ? i : -1).Where(i => i != -1).ToArray();
            }

            var filteredValidationMessages = ValidationResult?.ValidationRules;

            //https://stackoverflow.com/a/9908607
            foreach (var index in indexArray.OrderByDescending(v => v))
            {
                if (filteredValidationMessages != null)
                    filteredValidationMessages.RemoveAt(index);
            }

            ValidationResult.ValidationRules = filteredValidationMessages;
        }

        protected void RemoveNotRelevantMessagesFromValidationResult(string xPath, ValidationRuleEnum? validationRule = null, bool? removeAllChildElements = false)
        {
            if (ValidationResult?.ValidationMessages == null) return;

            int[] indexArray;
            if (validationRule.HasValue)
                indexArray = ValidationResult?.ValidationMessages?.Select((b, i) => b.XpathField.Equals(xPath) && b.Rule.Equals(validationRule.Value.ToString()) ? i : -1).Where(i => i != -1).ToArray();
            else
            {
                if (removeAllChildElements.GetValueOrDefault(false))
                    indexArray = ValidationResult?.ValidationMessages?.Select((b, i) => b.XpathField.StartsWith(xPath) ? i : -1).Where(i => i != -1).ToArray();
                else
                    indexArray = ValidationResult?.ValidationMessages?.Select((b, i) => b.XpathField.Equals(xPath) ? i : -1).Where(i => i != -1).ToArray();
            }

            var filteredValidationMessages = ValidationResult?.ValidationMessages;

            foreach (var index in indexArray.OrderByDescending(v => v))
            {
                if (filteredValidationMessages != null)
                {
                    filteredValidationMessages.RemoveAt(index);
                }
            }

            ValidationResult.ValidationMessages = filteredValidationMessages;
        }

        protected bool IsAnyValidationMessagesWithXpath(string xpath, int? index = null)
        {
            bool ruleFounded = false;

            if (xpath.Contains("{0}") && !index.HasValue)
            {
                foreach (var message in ValidationResult.ValidationMessages)
                {
                    string pattern = @"\[([0-9])+]";
                    var messageXpath = Regex.Replace(message.XpathField, pattern, "{0}");
                    if (xpath.Equals(messageXpath))
                    {
                        return true;
                    }
                }
            }

            var newXpath = index.HasValue ? Helpers.ReplaceCurlyBracketInXPath(index.Value, xpath) : xpath;
            ruleFounded = ValidationResult.ValidationMessages.Any(r => !string.IsNullOrEmpty(r.XpathField) && r.XpathField.EndsWith(newXpath, StringComparison.OrdinalIgnoreCase));
            return ruleFounded;
        }

        public ValidationRule[] formValidationRules()
        {
            return FormValidationRules;
        }
    }

    public class FormDataAttribute : Attribute
    {
        public string DataFormatVersion { get; set; }
        public string DataFormatId { get; set; }
        public string XsdFileName { get; set; }
        public ServiceDomain ServiceDomain { get; set; }
        public string ProcessCategory { get; set; }
    }
}
