using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Altinn;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.AtilFee;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Atil;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.Atil;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Utils;
using MatrikkelWSClient.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.FormValidators.Atil
{
    [FormData(DataFormatId = "7086", DataFormatVersion = "47365", XsdFileName = "suppleringArbeidstilsynet", ProcessCategory = "AT", ServiceDomain = ServiceDomain.ATIL)]
    public class SuppleringArbeidstilsynet_7086_47365_Validator : FormValidatorBase, IFormValidator
    {
        private List<EntityValidatorNode> _entitiesNodeList;

        private ArbeidstilsynetSupplering _validationForm { get; set; }

        private readonly ICodeListService _codeListService;
        private readonly IPostalCodeService _postalCodeService;
        private readonly IChecklistService _checklistService;
        private readonly IAtilFeeCalculatorService _atilFeeCalculatorService;
        private readonly IDecryptionFactory _decryptionFactory;
        private readonly IMatrikkelProvider _matrikkelProvider;

        //Tiltakshaver
        private IAktoerValidator _tiltakshaverValidator;

        private IEnkelAdresseValidator _tiltakshaverEnkelAdresseValidator;
        private IKodelisteValidator _tiltakshaverPartstypeValidator;
        private IKontaktpersonValidator _tiltakshaverKontaktpersonValidator;

        //EiendomByggested
        private IEiendomsidentifikasjonValidator _eiendomsidentifikasjonValidator;

        private IEiendomsAdresseValidator _eiendomsAdresseValidator;
        private IEiendomByggestedValidator _eiendomByggestedValidator;

        //Ettersending
        private IEttersendingValidator _ettersendingValidator;

        private IKodelisteValidator _ettersendingArbeidstilsynetVedleggstypeValidator;
        private ISuppleringVedleggValidator _ettersendingVedleggValidator;
        private IKodelisteValidator _ettersendingTemaValidator;

        //Mangelbesvarelse
        private IMangelbesvarelseValidator _mangelbesvarelseValidator;

        private ISuppleringVedleggValidator _mangelbesvarelseVedleggValidator;
        private IKodelisteValidator _mangelbesvarelseArbeidstilsynetVedleggstypeValidator;
        private IKodelisteValidator _mangelbesvarelseTemaValidator;
        private IMangelValidator _mangelValidator;
        private IKodelisteValidator _mangeltekstValidator;

        //AnsvarligSoeker
        private IAktoerValidator _ansvarligSoekerValidator;

        private IEnkelAdresseValidator _ansvarligSoekerEnkelAdresseValidator;
        private IKodelisteValidator _ansvarligSoekerPartstypeValidator;
        private IKontaktpersonValidator _ansvarligSoekerKontaktpersonValidator;

        //Metadata
        private IMetadataAtilV2Validator _metadataValidator;

        //Saksnummer
        private ISuppleringSaksnummerValidator _arbeidstilsynetsSaksnummerValidator;

        // Altinn metadata
        private IAltinnMetadataServices _altinnMetadataServices;

        private AttachmentValidator _attachmentValidator;

        private string[] _tiltakstypes;

        public SuppleringArbeidstilsynet_7086_47365_Validator(IValidationMessageComposer validationMessageComposer,
                                                             ICodeListService codeListService,
                                                             IPostalCodeService postalCodeService,
                                                             IChecklistService checklistService,
                                                             IAtilFeeCalculatorService atilFeeCalculatorService,
                                                             IDecryptionFactory decryptionFactory,
                                                             IMatrikkelProvider matrikkelProvider,
                                                             IAltinnMetadataServices altinnMetadataServices)

            : base(validationMessageComposer, checklistService)
        {
            _codeListService = codeListService;
            _postalCodeService = postalCodeService;
            _checklistService = checklistService;
            _entitiesNodeList = new List<EntityValidatorNode>();
            _tiltakstypes = new string[] { };
            _atilFeeCalculatorService = atilFeeCalculatorService;
            _matrikkelProvider = matrikkelProvider;
            _altinnMetadataServices = altinnMetadataServices;
            _decryptionFactory = decryptionFactory;

            base.InitializeFormValidator<ArbeidstilsynetSupplering>();
        }

        //public FormDataAttribute GetFormDataAttribute { get => base.FormDataAttribute; }

        protected override void CustomFormValidation(ValidationInput validationInput)
        {
            //throw new NotImplementedException();
        }

        protected override void DefineValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.xml);

            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.ettersendingEllerMangelbesvarelse);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.Soeker);
            //Altinnmetadata
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.vedlegg, "/maxAttachmentCount");
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.vedlegg, "/minAttachmentCount");

            //Tiltakshaver
            AccumulateValidationRules(_tiltakshaverKontaktpersonValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_tiltakshaverPartstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_tiltakshaverEnkelAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_tiltakshaverValidator.ValidationResult.ValidationRules);

            //EiendomByggested rules
            AccumulateValidationRules(_eiendomsidentifikasjonValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_eiendomsAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_eiendomByggestedValidator.ValidationResult.ValidationRules);

            //Ettersending
            AccumulateValidationRules(_ettersendingArbeidstilsynetVedleggstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ettersendingVedleggValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ettersendingTemaValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ettersendingValidator.ValidationResult.ValidationRules);

            //Mangelbesvarelse
            AccumulateValidationRules(_mangelbesvarelseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_mangelbesvarelseVedleggValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_mangelbesvarelseArbeidstilsynetVedleggstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_mangelbesvarelseTemaValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_mangelValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_mangeltekstValidator.ValidationResult.ValidationRules);

            //Arbeidstilsynets saksnummer
            AccumulateValidationRules(_arbeidstilsynetsSaksnummerValidator.ValidationResult.ValidationRules);

            //Metadata
            AccumulateValidationRules(_metadataValidator.ValidationResult.ValidationRules);

            //AnsvarligSoeker
            AccumulateValidationRules(_ansvarligSoekerValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarligSoekerEnkelAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarligSoekerPartstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarligSoekerKontaktpersonValidator.ValidationResult.ValidationRules);

            //attachment
            AccumulateValidationRules(_attachmentValidator.validationResult.ValidationRules);
        }

        protected override IEnumerable<string> GetFormTiltakstyper()
        {
            //throw new NotImplementedException();
            return new List<string> { "" };
        }

        protected override void InitializeValidatorConfig()
        {
            // Tiltakshaver
            var tiltakshaverNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 1, EnumId = EntityValidatorEnum.TiltakshaverValidator, ParentID = null},
                new () {NodeId = 2, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 1},
                new () {NodeId = 3, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 1},
                new () {NodeId = 4, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 1}
            };
            _entitiesNodeList.AddRange(tiltakshaverNodeList);

            // EiendomByggested
            List<EntityValidatorNode> eiendombyggestedNodeList = new()
            {
                new() { NodeId = 5, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
                new() { NodeId = 6, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 5 },
                new() { NodeId = 7, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 5 },
            };
            _entitiesNodeList.AddRange(eiendombyggestedNodeList);

            //Kommunenes Saksnummer
            var kommunenesSaksnummerValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 8, EnumId = EntityValidatorEnum.KommunensSaksnummerValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(kommunenesSaksnummerValidatorNodeList);

            //Ettersending
            var ettersendingValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 9, EnumId = EntityValidatorEnum.EttersendingValidator, ParentID = null},
                new() {NodeId = 10, EnumId = EntityValidatorEnum.SuppleringVedleggValidator, ParentID = 9},
                new() {NodeId = 11, EnumId = EntityValidatorEnum.ArbeidstilsynetVedleggstypeValidator, ParentID = 10},
                new() {NodeId = 12, EnumId = EntityValidatorEnum.TemaValidator, ParentID = 9},
            };
            _entitiesNodeList.AddRange(ettersendingValidatorNodeList);

            //Mangelbesvarelse
            var mangelbesvarelseValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 14, EnumId = EntityValidatorEnum.MangelbesvarelseValidator, ParentID = null},
                new() {NodeId = 15, EnumId = EntityValidatorEnum.SuppleringVedleggValidator, ParentID = 14},
                new() {NodeId = 16, EnumId = EntityValidatorEnum.ArbeidstilsynetVedleggstypeValidator, ParentID = 15},
                new() {NodeId = 17, EnumId = EntityValidatorEnum.TemaValidator, ParentID = 14},
                new() {NodeId = 18, EnumId = EntityValidatorEnum.MangelValidator, ParentID = 14},
                new() {NodeId = 19, EnumId = EntityValidatorEnum.MangeltekstValidator, ParentID = 18},
            };
            _entitiesNodeList.AddRange(mangelbesvarelseValidatorNodeList);

            //Arbeidstilsynets Saksnummer
            var arbeidstilsynetsSaksnummerValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 21, EnumId = EntityValidatorEnum.ArbeidstilsynetsSuppleringSaksnummerValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(arbeidstilsynetsSaksnummerValidatorNodeList);

            //Metadata
            var metadataValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 22, EnumId = EntityValidatorEnum.MetadataSuppleringValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(metadataValidatorNodeList);

            //AnsvarligSoeker
            var ansvarligSoekervalidatorNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 23, EnumId = EntityValidatorEnum.AnsvarligSoekerValidator, ParentID = null},
                new () {NodeId = 24, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 23},
                new () {NodeId = 25, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 23},
                new () {NodeId = 26, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 23}
            };
            _entitiesNodeList.AddRange(ansvarligSoekervalidatorNodeList);

            //Attachment...
            var attachmentValidatorNoeList = new List<EntityValidatorNode>()
            {
                new() { NodeId = 27, EnumId = EntityValidatorEnum.AttachmentValidator, ParentID = null }
            };
            _entitiesNodeList.AddRange(attachmentValidatorNoeList);
        }

        protected override void InstantiateValidators()
        {
            var tree = EntityValidatorTree.BuildTree(_entitiesNodeList);

            //Tiltakshaver
            _tiltakshaverKontaktpersonValidator = new KontaktpersonValidator(tree, 2);
            _tiltakshaverPartstypeValidator = new PartstypeValidator(tree, 3, _codeListService);
            _tiltakshaverEnkelAdresseValidator = new EnkelAdresseValidator(tree, 4, _postalCodeService);
            _tiltakshaverValidator = new TiltakshaverValidator(tree, _tiltakshaverEnkelAdresseValidator, _tiltakshaverKontaktpersonValidator, _tiltakshaverPartstypeValidator, _codeListService, _decryptionFactory);

            //EiendomByggested
            _eiendomsAdresseValidator = new EiendomsAdresseValidator(tree);
            _eiendomsidentifikasjonValidator = new EiendomsidentifikasjonValidator(tree, _codeListService, _matrikkelProvider);
            _eiendomByggestedValidator = new EiendomByggestedValidator(tree, _eiendomsAdresseValidator, _eiendomsidentifikasjonValidator, _matrikkelProvider);

            //Kommunens saksnummer
            //_kommunensSaksnummerValidator = new KommunensSaksnummerValidator(tree);

            //Ettersending
            _ettersendingArbeidstilsynetVedleggstypeValidator = new ArbeidstilsynetVedleggstypeValidator(tree, 11, _codeListService);
            _ettersendingVedleggValidator = new SuppleringVedleggValidator(tree, 10, _ettersendingArbeidstilsynetVedleggstypeValidator);
            _ettersendingTemaValidator = new TemaValidator(tree, 12, _codeListService);
            _ettersendingValidator = new EttersendingValidator(tree, _ettersendingVedleggValidator, _ettersendingTemaValidator);

            //Mangelbesvarelse
            _mangelbesvarelseArbeidstilsynetVedleggstypeValidator = new ArbeidstilsynetVedleggstypeValidator(tree, 16, _codeListService);
            _mangelbesvarelseVedleggValidator = new SuppleringVedleggValidator(tree, 15, _mangelbesvarelseArbeidstilsynetVedleggstypeValidator);
            _mangelbesvarelseTemaValidator = new TemaValidator(tree, 17, _codeListService);
            _mangeltekstValidator = new MangeltekstValidator(tree, 19, _codeListService);
            _mangelValidator = new MangelValidator(tree, 18, _mangeltekstValidator);
            _mangelbesvarelseValidator = new MangelbesvarelseValidator(tree, _mangelbesvarelseVedleggValidator, _mangelbesvarelseTemaValidator, _mangelValidator);

            //Arbeidstilsynets saksnummer
            _arbeidstilsynetsSaksnummerValidator = new ArbeidstilsynetsSuppleringSaksnummerValidator(tree);

            //Metadata
            _metadataValidator = new MetadataSuppleringValidator(tree);

            //AnsvarligSoeker
            _ansvarligSoekerKontaktpersonValidator = new KontaktpersonValidator(tree, 24);
            _ansvarligSoekerPartstypeValidator = new PartstypeValidator(tree, 25, _codeListService);
            _ansvarligSoekerEnkelAdresseValidator = new EnkelAdresseValidator(tree, 26, _postalCodeService);
            _ansvarligSoekerValidator = new AnsvarligSoekerValidator(tree, _ansvarligSoekerEnkelAdresseValidator, _ansvarligSoekerKontaktpersonValidator, _ansvarligSoekerPartstypeValidator, _codeListService, _decryptionFactory);

            //Attachemnts...
            _attachmentValidator = new AttachmentValidator(tree);
        }

        protected override void Validate(ValidationInput validationInput)
        {
            _validationForm = SerializeUtil.DeserializeFromString<ArbeidstilsynetSupplering>(validationInput.FormData);

            if (validationInput.Attachments != null && validationInput.Attachments.Any() || validationInput.SubForms != null && validationInput.SubForms.Any())
            {
                var altinnMetadata = AsyncHelper.RunSync(() => _altinnMetadataServices.GetAltinnMetadataAsync(FormDataAttribute.DataFormatId, FormDataAttribute.DataFormatVersion));

                var formAttachmentRules = altinnMetadata.AttachmentRules;
                if (validationInput.Attachments != null && validationInput.Attachments.Any())
                {
                    ValidateAttachment(validationInput.Attachments, formAttachmentRules);
                }
            }

            var eiendommer = _validationForm?.EiendomByggested;

            var index = GetArrayIndex(eiendommer);

            for (int i = 0; i < index; i++)
            {
                var eiendom = Helpers.ObjectIsNullOrEmpty(eiendommer) ? null : eiendommer[i];
                var eiendomValidationResult = _eiendomByggestedValidator.Validate(eiendom);
                AccumulateValidationMessages(eiendomValidationResult.ValidationMessages, i);
            }

            //Ettersending
            if (Helpers.ObjectIsNullOrEmpty(_validationForm.Ettersending) && Helpers.ObjectIsNullOrEmpty(_validationForm.Mangelbesvarelse))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.ettersendingEllerMangelbesvarelse);
            }
            else
            {
                bool _arbeidstilsynetsSaksnummerValidatorExecuted = false;

                if (!Helpers.ObjectIsNullOrEmpty(_validationForm.Ettersending))
                {
                    var ettersendinger = _validationForm?.Ettersending;
                    var ettersendingerIndex = GetArrayIndex(ettersendinger);

                    for (int i = 0; i < ettersendingerIndex; i++)
                    {
                        var ettersending = Helpers.ObjectIsNullOrEmpty(ettersendinger) ? null : ettersendinger[i];
                        var ettersendingValidationResult = _ettersendingValidator.Validate(ettersending);
                        AccumulateValidationMessages(ettersendingValidationResult.ValidationMessages, i);
                    }

                    var arbeidstilsynetsSaksnummerValidationResultEttersending = _arbeidstilsynetsSaksnummerValidator.Validate(_validationForm.ArbeidstilsynetsSaksnummer, false);
                    AccumulateValidationMessages(arbeidstilsynetsSaksnummerValidationResultEttersending.ValidationMessages);
                    _arbeidstilsynetsSaksnummerValidatorExecuted = true;
                }

                //Mangelbesvarelse
                if (!Helpers.ObjectIsNullOrEmpty(_validationForm.Mangelbesvarelse))
                {
                    var mangelbesvarelser = _validationForm?.Mangelbesvarelse;
                    var mangelbesvarelserIndex = GetArrayIndex(mangelbesvarelser);

                    for (int i = 0; i < mangelbesvarelserIndex; i++)
                    {
                        var mangelbesvarelse = Helpers.ObjectIsNullOrEmpty(mangelbesvarelser) ? null : mangelbesvarelser[i];
                        var mangelbesvarelseValidationResult = _mangelbesvarelseValidator.Validate(mangelbesvarelse);
                        AccumulateValidationMessages(mangelbesvarelseValidationResult.ValidationMessages, i);
                    }

                    if (!_arbeidstilsynetsSaksnummerValidatorExecuted)
                    {
                        var arbeidstilsynetsSaksnummerValidationResultMangelbesvarelse = _arbeidstilsynetsSaksnummerValidator.Validate(_validationForm.ArbeidstilsynetsSaksnummer, true);
                        AccumulateValidationMessages(arbeidstilsynetsSaksnummerValidationResultMangelbesvarelse.ValidationMessages);
                    }
                }
            }

            //Metadata
            var metadataValidationResult = _metadataValidator.Validate(_validationForm?.Metadata);
            AccumulateValidationMessages(metadataValidationResult.ValidationMessages);

            ValidateDataRelations(_validationForm);
        }

        public ValidationResult ValidateAttachment(AttachmentInfo[] attachmentInfos, List<AttachmentRule> altinnFormAttachmentRules)
        {
            if (attachmentInfos == null || altinnFormAttachmentRules == null)
                return ValidationResult;

            foreach (var attachment in attachmentInfos)
            {
                var attachmentFormValidator = _attachmentValidator.Validate(attachment, altinnFormAttachmentRules);
                AccumulateValidationMessages(attachmentFormValidator.ValidationMessages);
            }

            //Validate MaxAttachmentCount
            //Group by AttachmentTypeName and create dictionary
            var groups = attachmentInfos
                .GroupBy(s => s.AttachmentTypeName)
                .Select(s => new
                {
                    s.Key,
                    Count = s.Count()
                });

            var listOfAttachmentsTypeName = altinnFormAttachmentRules.Select(a => a.AttachmentTypeName).ToList();
            // filter dictionary by allowed attachments types
            var filterDictionary = groups.Where(pair => listOfAttachmentsTypeName.Contains(pair.Key))
                .ToDictionary(pair => pair.Key, pair => pair.Count);

            //loop dictionary to control de allowed count of attachments
            foreach (var dictionaryEntry in filterDictionary)
            {
                var attachmentRule = altinnFormAttachmentRules.First(r => r.AttachmentTypeName == dictionaryEntry.Key);
                if (attachmentRule.MaxAttachmentCount < dictionaryEntry.Value)
                {
                    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.vedlegg, new[] { dictionaryEntry.Key, attachmentRule.MaxAttachmentCount.ToString(), dictionaryEntry.Value.ToString() }, "/maxAttachmentCount");
                }
                //Validate MinAttachmentCount
                else if (attachmentRule.MinAttachmentCount > dictionaryEntry.Value)
                {
                    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.vedlegg, new[] { dictionaryEntry.Key, attachmentRule.MinAttachmentCount.ToString(), dictionaryEntry.Value.ToString() }, "/minAttachmentCount");
                }
            }
            return ValidationResult;
        }

        public ValidationResult ValidateDataRelations(ArbeidstilsynetSupplering form)
        {
            if (Helpers.ObjectIsNullOrEmpty(form.AnsvarligSoeker) && Helpers.ObjectIsNullOrEmpty(form.Tiltakshaver))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.Soeker);
                //TODO: sjekk om det er samme tekst som skal brukes som i avfallsplan, i ArbeidstilsyneDB
            }
            else
            {
                if (!Helpers.ObjectIsNullOrEmpty(form.AnsvarligSoeker))
                {
                    var ansvarligSoekerValidationResult = _ansvarligSoekerValidator.Validate(form.AnsvarligSoeker);
                    AccumulateValidationMessages(ansvarligSoekerValidationResult.ValidationMessages);
                }
                else
                {
                    var tiltakshaverValidationResult = _tiltakshaverValidator.Validate(form.Tiltakshaver);
                    AccumulateValidationMessages(tiltakshaverValidationResult.ValidationMessages);
                }
            }

            //TODO: vedleggsvalidering må implementeres
            //var vedleggValidationResults = _vedleggValidator.Validate(_attachments, _validationForm, null, base.ValidationResult.ValidationMessages);
            //AccumulateValidationMessages(vedleggValidationResults.ValidationMessages);

            return ValidationResult;
        }

        protected override DateTime? GetFormValidationDate()
        {
            return DateTime.Now;
        }
    }
}