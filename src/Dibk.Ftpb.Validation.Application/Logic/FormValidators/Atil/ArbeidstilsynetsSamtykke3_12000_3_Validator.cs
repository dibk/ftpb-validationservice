using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.AtilFee;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Checklist;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Atil;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.Atil;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Utils;
using MatrikkelWSClient.Services;
using Microsoft.FeatureManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.FormValidators.Atil
{
    [FormData(DataFormatId = "12000", DataFormatVersion = "3", XsdFileName = "arbeidstilsynetsSamtykke3", ProcessCategory = "AT", ServiceDomain = ServiceDomain.ATIL)]
    public class ArbeidstilsynetsSamtykke3_12000_3_Validator : FormValidatorBase, IFormValidator, IFormWithChecklistAnswers
    {
        private List<EntityValidatorNode> _entitiesNodeList;

        private ArbeidstilsynetsSamtykkeV2 _validationForm { get; set; }

        private readonly ICodeListService _codeListService;
        private readonly IPostalCodeService _postalCodeService;
        private readonly IChecklistService _checklistService;
        private readonly IAtilFeeCalculatorService _atilFeeCalculatorService;
        private readonly IDecryptionFactory _decryptionFactory;
        private readonly IMatrikkelProvider _matrikkelProvider;        

        //EiendomByggested
        private IEiendomsidentifikasjonValidator _eiendomsidentifikasjonValidator;

        private IEiendomsAdresseValidator _eiendomsAdresseValidator;
        private IEiendomByggestedValidator _eiendomByggestedValidator;

        //Tiltakshaver
        private IAktoerValidator _tiltakshaverValidator;

        private IEnkelAdresseValidator _tiltakshaverEnkelAdresseValidator;
        private IKodelisteValidator _tiltakshaverPartstypeValidator;
        private IKontaktpersonValidator _tiltakshaverKontaktpersonValidator;

        //Fakturamotaker
        private IFakturamottakerValidator _fakturamottakerValidator;

        private IEnkelAdresseValidator _fakturamottakerEnkelAdresseValidator;

        //BeskrivelseAvTiltak
        private BygningstypeValidator _bygningstypeValidator;

        private IFormaaltypeValidator _formaaltypeValidator;
        private IKodelisteValidator _tiltakstypeValidator;
        private IBeskrivelseAvTiltakAtilV2Validator _beskrivelseAvTiltakValidator;

        //AnsvarligSoeker
        private IAktoerValidator _ansvarligSoekerValidator;

        private IEnkelAdresseValidator _ansvarligSoekerEnkelAdresseValidator;
        private IKodelisteValidator _ansvarligSoekerPartstypeValidator;
        private IKontaktpersonValidator _ansvarligSoekerKontaktpersonValidator;

        //Sjekklistekrav
        private IKodelisteValidator _sjekklistepunktValidator;

        private ISjekklistekravValidator _sjekklistekravValidator;

        //Metadata
        private IMetadataAtilValidator _metadataValidator;

        //Saksnummer
        private ISaksnummerValidator _arbeidstilsynetsSaksnummerValidator;

        private ISaksnummerValidator _kommunensSaksnummerValidator;

        // Arbeidsplasser
        private ArbeidsplasserValidator _arbeidsplasserValidator;

        // Betaling
        private BetalingValidator _betalingValidator;

        // Vedlegg
        private IVedleggValidator _vedleggValidator;

        private string[] _tiltakstypes;
        private IEnumerable<Sjekk> _allCheckpointsFromAPI;
        private IEnumerable<Sjekk> _allCheckpointsFromAPIFlatList;

        // ATIL Feature - SLETTES ETTER 20250217 - START
        private readonly IFeatureManager _featureManager;
        public ArbeidstilsynetsSamtykke3_12000_3_Validator(IValidationMessageComposer validationMessageComposer,
                                                         ICodeListService codeListService,
                                                         IPostalCodeService postalCodeService,
                                                         IChecklistService checklistService,
                                                         IAtilFeeCalculatorService atilFeeCalculatorService,
                                                         IDecryptionFactory decryptionFactory,
                                                         IMatrikkelProvider matrikkelProvider,
                                                         IFeatureManager featureManager)
            : base(validationMessageComposer, checklistService)
        {
            _codeListService = codeListService;
            _postalCodeService = postalCodeService;
            _checklistService = checklistService;
            _entitiesNodeList = new List<EntityValidatorNode>();
            _tiltakstypes = new string[] { };
            _atilFeeCalculatorService = atilFeeCalculatorService;
            _matrikkelProvider = matrikkelProvider;            
            _decryptionFactory = decryptionFactory;
            //SetTitaksTypeIsøknad();

            _featureManager = featureManager;

            base.InitializeFormValidator<ArbeidstilsynetsSamtykkeV2>();
        }

        public FormDataAttribute GetFormDataAttribute => FormDataAttribute;

        //https://stackoverflow.com/questions/18165460/how-to-search-hierarchical-data-with-linq
        public override ValidationResult StartValidation(ValidationInput validationInput)
        {
            _allCheckpointsFromAPI = _checklistService.GetChecklist(FormDataAttribute.ProcessCategory, FormDataAttribute.ServiceDomain, null);

            List<Sjekk> flatlist = new();
            foreach (var item in _allCheckpointsFromAPI)
            {
                //Create a flat list, from a hierarchical list of all "sjekkpunkter"
                foreach (var sjekkpkt in item.Flatten(y => y.Undersjekkpunkter))
                {
                    flatlist.Add(sjekkpkt);
                }
            }

            _allCheckpointsFromAPIFlatList = flatlist;

            base.StartValidation(validationInput);

            if (ValidationResult.ValidationMessages.Any(x => x.Reference.EndsWith(".122.2"))) //xml.feil
            {
                return ValidationResult;
            }

            //Add ChecklistAnswers to validationresult
            ValidationResult.PrefillChecklist = GetChecklistAnswersFromDynamicPartOfDatamodel();
            var formChecklistAnswers = GetChecklistAnswersFromStaticPartOfDatamodel(validationInput);
            ValidationResult.PrefillChecklist.AddRange(formChecklistAnswers);

            return ValidationResult;
        }

        protected override void InitializeValidatorConfig()
        {
            // EiendomByggested
            List<EntityValidatorNode> eiendombyggestedNodeList = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 1 },
                new() { NodeId = 3, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 1 },
            };
            _entitiesNodeList.AddRange(eiendombyggestedNodeList);

            //** check not implemented in FTB
            //AnsvarligSoeker
            var ansvarligSoekervalidatorNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 18, EnumId = EntityValidatorEnum.AnsvarligSoekerValidator, ParentID = null},
                new () {NodeId = 19, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 18},
                new () {NodeId = 20, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 18},
                new () {NodeId = 21, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 18}
            };
            _entitiesNodeList.AddRange(ansvarligSoekervalidatorNodeList);
            //**

            // Tiltakshaver
            var tiltakshaverNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 4, EnumId = EntityValidatorEnum.TiltakshaverValidator, ParentID = null},
                new () {NodeId = 5, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 4},
                new () {NodeId = 6, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 4},
                new () {NodeId = 7, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 4}
            };
            _entitiesNodeList.AddRange(tiltakshaverNodeList);

            //fakturamottake
            var fakturamottakeNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 8, EnumId = EntityValidatorEnum.FakturamottakerValidator, ParentID = null},
                new () {NodeId = 9, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 8}
            };
            _entitiesNodeList.AddRange(fakturamottakeNodeList);

            //beskrivelseAvTiltak
            var beskrivelseAvTiltakNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 10, EnumId = EntityValidatorEnum.BeskrivelseAvTiltakForAtilV2Validator, ParentID = null},
                new () {NodeId = 11, EnumId = EntityValidatorEnum.FormaaltypeValidator, ParentID = 10},
                new () {NodeId = 14, EnumId = EntityValidatorEnum.BygningstypeValidator, ParentID = 11},
                new () {NodeId = 16, EnumId = EntityValidatorEnum.TiltakstypeAtilValidator, ParentID = 10},
            };
            _entitiesNodeList.AddRange(beskrivelseAvTiltakNodeList);

            //Arbeidsplasser
            var arbeidsplasserValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 17, EnumId = EntityValidatorEnum.ArbeidsplasserValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(arbeidsplasserValidatorNodeList);

            //Sjekklistekrav
            var sjekklistekravValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 22, EnumId = EntityValidatorEnum.SjekklistekravValidator, ParentID = null},
                new() {NodeId = 23, EnumId = EntityValidatorEnum.SjekklistepunktValidator, ParentID = 22}
            };
            _entitiesNodeList.AddRange(sjekklistekravValidatorNodeList);

            //Metadata
            var metadataValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 24, EnumId = EntityValidatorEnum.MetadataAtilValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(metadataValidatorNodeList);

            //Arbeidstilsynets Saksnummer
            var arbeidstilsynetsSaksnummerValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 25, EnumId = EntityValidatorEnum.ArbeidstilsynetsSaksnummerValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(arbeidstilsynetsSaksnummerValidatorNodeList);

            //Kommunenes Saksnummer
            var kommunenesSaksnummerValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 26, EnumId = EntityValidatorEnum.KommunensSaksnummerValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(kommunenesSaksnummerValidatorNodeList);

            //Betaling
            var betalingValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 27, EnumId = EntityValidatorEnum.BetalingValidator, ParentID = null}
            };
            _entitiesNodeList.AddRange(betalingValidatorNodeList);

            //Vedlegg
            var vedleggValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 28, EnumId = EntityValidatorEnum.VedleggAtilV2Validator, ParentID = null}
            };
            _entitiesNodeList.AddRange(vedleggValidatorNodeList);
        }

        protected override void InstantiateValidators()
        {
            var tree = EntityValidatorTree.BuildTree(_entitiesNodeList);
            //Sjekklistekrav
            _sjekklistepunktValidator = new SjekklistepunktValidator(tree, _codeListService);
            _sjekklistekravValidator = new SjekklistekravValidator(tree, _sjekklistepunktValidator, _codeListService);

            //AnsvarligSoeker TODO not applied in FTB v1
            _ansvarligSoekerKontaktpersonValidator = new KontaktpersonValidator(tree, 19);
            _ansvarligSoekerPartstypeValidator = new PartstypeValidator(tree, 20, _codeListService);
            _ansvarligSoekerEnkelAdresseValidator = new EnkelAdresseValidator(tree, 21, _postalCodeService);
            _ansvarligSoekerValidator = new AnsvarligSoekerValidator(tree, _ansvarligSoekerEnkelAdresseValidator, _ansvarligSoekerKontaktpersonValidator, _ansvarligSoekerPartstypeValidator, _codeListService, _decryptionFactory);

            //Tiltakshaver
            _tiltakshaverKontaktpersonValidator = new KontaktpersonValidator(tree, 5);
            _tiltakshaverPartstypeValidator = new PartstypeValidator(tree, 6, _codeListService);
            _tiltakshaverEnkelAdresseValidator = new EnkelAdresseValidator(tree, 7, _postalCodeService);
            _tiltakshaverValidator = new TiltakshaverValidator(tree, _tiltakshaverEnkelAdresseValidator, _tiltakshaverKontaktpersonValidator, _tiltakshaverPartstypeValidator, _codeListService, _decryptionFactory);

            //fakturamottaker
            _fakturamottakerEnkelAdresseValidator = new EnkelAdresseValidator(tree, 9, _postalCodeService);
            _fakturamottakerValidator = new FakturamottakerValidator(tree, _fakturamottakerEnkelAdresseValidator, _featureManager);

            _arbeidsplasserValidator = new ArbeidsplasserValidator(tree);
            _betalingValidator = new BetalingValidator(tree, _atilFeeCalculatorService);

            //BeskrivelseAvTiltak
            _bygningstypeValidator = new BygningstypeValidator(tree, _codeListService);
            _formaaltypeValidator = new FormaaltypeValidator(tree, _bygningstypeValidator);
            _tiltakstypeValidator = new TiltakstypeAtilValidator(tree, _codeListService);
            _beskrivelseAvTiltakValidator = new BeskrivelseAvTiltakForAtilV2Validator(tree, _formaaltypeValidator, _tiltakstypeValidator, _atilFeeCalculatorService);

            //EiendomByggested
            _eiendomsAdresseValidator = new EiendomsAdresseValidator(tree);
            _eiendomsidentifikasjonValidator = new EiendomsidentifikasjonValidator(tree, _codeListService, _matrikkelProvider);
            _eiendomByggestedValidator = new EiendomByggestedValidator(tree, _eiendomsAdresseValidator, _eiendomsidentifikasjonValidator, _matrikkelProvider);

            //Metadata
            _metadataValidator = new MetadataAtilValidator(tree);
            //Arbeidstilsynets saksnummer
            _arbeidstilsynetsSaksnummerValidator = new ArbeidstilsynetsSaksnummerValidator(tree);
            //Kommunens saksnummer
            _kommunensSaksnummerValidator = new KommunensSaksnummerValidator(tree);
            //Vedlegg
            _vedleggValidator = new VedleggAtilV2Validator(tree);
        }

        protected override void DefineValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.xml);

            AddValidationRule(ValidationRuleEnum.sjekklistepunkt_1_18_dokumentasjon_utfylt, null, null, "/krav/sjekklistekrav{0}/dokumentasjon");

            //TODO create a method to do this automatic, potential error
            //Sjekklistekrav
            AccumulateValidationRules(_sjekklistepunktValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_sjekklistekravValidator.ValidationResult.ValidationRules);
            //EiendomByggested rules
            AccumulateValidationRules(_eiendomsidentifikasjonValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_eiendomsAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_eiendomByggestedValidator.ValidationResult.ValidationRules);
            //Tiltashaver
            AccumulateValidationRules(_tiltakshaverKontaktpersonValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_tiltakshaverPartstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_tiltakshaverEnkelAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_tiltakshaverValidator.ValidationResult.ValidationRules);
            //fakturamottaker
            AccumulateValidationRules(_fakturamottakerEnkelAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_fakturamottakerValidator.ValidationResult.ValidationRules);
            //BeskrivelseAvTiltak
            AccumulateValidationRules(_bygningstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_formaaltypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_tiltakstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_beskrivelseAvTiltakValidator.ValidationResult.ValidationRules);
            //Arbeidsplasser
            AccumulateValidationRules(_arbeidsplasserValidator.ValidationResult.ValidationRules);
            //Arbeidsplasser
            AccumulateValidationRules(_betalingValidator.ValidationResult.ValidationRules);
            //AnsvarligSoeker
            AccumulateValidationRules(_ansvarligSoekerValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarligSoekerEnkelAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarligSoekerPartstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarligSoekerKontaktpersonValidator.ValidationResult.ValidationRules);
            //Metadata
            AccumulateValidationRules(_metadataValidator.ValidationResult.ValidationRules);
            //Arbeidstilsynets saksnummer
            AccumulateValidationRules(_arbeidstilsynetsSaksnummerValidator.ValidationResult.ValidationRules);
            //Kommunens saksnummer
            AccumulateValidationRules(_kommunensSaksnummerValidator.ValidationResult.ValidationRules);
            //Vedlegg
            AccumulateValidationRules(_vedleggValidator.ValidationResult.ValidationRules);
        }

        protected override void Validate(ValidationInput validationInput)
        {
            _validationForm = SerializeUtil.DeserializeFromString<ArbeidstilsynetsSamtykkeV2>(validationInput.FormData);

            var beskrivelseAvTiltakValidationResult = _beskrivelseAvTiltakValidator.Validate(ValidationResult.ValidationRules, _validationForm.BeskrivelseAvTiltak);
            AccumulateValidationMessages(beskrivelseAvTiltakValidationResult.ValidationMessages);

            var errorInBRA = beskrivelseAvTiltakValidationResult.ValidationMessages.Any(x => x.XpathField.ToLower().Equals("/beskrivelseavtiltak/bra") && x.Rule.ToLower().Equals("tillatt"));

            _tiltakstypes = _beskrivelseAvTiltakValidator.Tiltakstypes.ToArray();

            var fakturamottakerValidationResult = _fakturamottakerValidator.Validate(_validationForm, _validationForm.Fakturamottaker);
            AccumulateValidationMessages(fakturamottakerValidationResult.ValidationMessages);

            var eiendoms = _validationForm?.EiendomByggested;

            var index = GetArrayIndex(eiendoms);

            for (int i = 0; i < index; i++)
            {
                var eiendom = Helpers.ObjectIsNullOrEmpty(eiendoms) ? null : eiendoms[i];
                var eiendomValidationResult = _eiendomByggestedValidator.Validate(eiendom);
                AccumulateValidationMessages(eiendomValidationResult.ValidationMessages, i);
            }

            var attachments = Helpers.ObjectIsNullOrEmpty(validationInput.Attachments) ? null : validationInput.Attachments.Select(a => a.AttachmentTypeName).ToArray();

            var arbeidsplasserValidationResult = _arbeidsplasserValidator.Validate(_validationForm?.Arbeidsplasser, attachments);
            AccumulateValidationMessages(arbeidsplasserValidationResult.ValidationMessages);

            var tiltakshaverValidationResult = _tiltakshaverValidator.Validate(_validationForm.Tiltakshaver);
            AccumulateValidationMessages(tiltakshaverValidationResult.ValidationMessages);

            var ansvarligSoekerValidationResult = _ansvarligSoekerValidator.Validate(_validationForm.AnsvarligSoeker);
            AccumulateValidationMessages(ansvarligSoekerValidationResult.ValidationMessages);

            var sjekklistekravValidationResult = _sjekklistekravValidator.Validate(FormDataAttribute.ProcessCategory, FormDataAttribute.ServiceDomain, _validationForm?.Sjekklistekrav, _checklistService, _tiltakstypes.ToList());
            AccumulateValidationMessages(sjekklistekravValidationResult.ValidationMessages);

            var formaaltype = _validationForm.BeskrivelseAvTiltak?.Formaaltype;

            var bygningstype = _validationForm.BeskrivelseAvTiltak != null
                && _validationForm.BeskrivelseAvTiltak.Formaaltype != null
                && formaaltype?.Bygningstype != null ? formaaltype.Bygningstype[0].Kodeverdi : null;

            var BRA = _validationForm.BeskrivelseAvTiltak != null ? _validationForm.BeskrivelseAvTiltak.BRA : null;

            //Only validate payment information if no error in BRA
            if (!errorInBRA)
            {
                var betalingValidationResult = _betalingValidator.Validate(
                    _validationForm.Betaling,
                    _tiltakstypes,
                    bygningstype,
                    BRA);

                AccumulateValidationMessages(betalingValidationResult.ValidationMessages);
            }

            var metadataValidationResult = _metadataValidator.Validate(_validationForm.Metadata);
            AccumulateValidationMessages(metadataValidationResult.ValidationMessages);

            var arbeidstilsynetsSaksnummerValidationResult = _arbeidstilsynetsSaksnummerValidator.Validate(_validationForm.ArbeidstilsynetsSaksnummer);
            AccumulateValidationMessages(arbeidstilsynetsSaksnummerValidationResult.ValidationMessages);

            var kommunensSaksnummerValidationResult = _kommunensSaksnummerValidator.Validate(_validationForm.KommunensSaksnummer);
            AccumulateValidationMessages(kommunensSaksnummerValidationResult.ValidationMessages);

            var vedleggValidationResult = _vedleggValidator.Validate(validationInput, _validationForm, validationInput.AuthenticatedSubmitter);
            AccumulateValidationMessages(vedleggValidationResult.ValidationMessages);
        }

        protected override IEnumerable<string> GetFormTiltakstyper()
        {
            return _tiltakstypes;
        }

        private void CustomSjekklisteValidations(Arbeidsplasser arbeidsplasser, Sjekklistekrav[] sjekklistekrav)
        {
            if (arbeidsplasser != null && sjekklistekrav != null && sjekklistekrav.Length > 0)
            {
                var chkPoint_1_18 = _allCheckpointsFromAPIFlatList.First(x => x.Id.Equals("1.18"));
                var tiltakstyperInSjekkpunkt_1_18 = chkPoint_1_18.Tiltakstyper.Select(x => x.Kode).ToList();
                var checkpoint_1_18_IsRelevant = _tiltakstypes.ToList().Intersect(tiltakstyperInSjekkpunkt_1_18).Count() > 0;

                if (checkpoint_1_18_IsRelevant)
                {
                    if (arbeidsplasser.UtleieBygg.GetValueOrDefault(false))
                    {
                        var pt_1_18 = sjekklistekrav.FirstOrDefault(x => x.Sjekklistepunkt != null && x.Sjekklistepunkt.Kodeverdi != null && x.Sjekklistepunkt.Kodeverdi.Equals("1.18"));

                        if (pt_1_18 == null)
                        {
                            AddMessageFromRule(ValidationRuleEnum.sjekklistepunkt_1_18_dokumentasjon_utfylt, "/krav/sjekklistekrav{0}/dokumentasjon", new string[] { "1.18" });
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(pt_1_18.Dokumentasjon))
                            {
                                //Find element number
                                var numberInArray = Array.IndexOf(sjekklistekrav, pt_1_18);
                                AddMessageFromRule(ValidationRuleEnum.sjekklistepunkt_1_18_dokumentasjon_utfylt, $"/krav/sjekklistekrav[{numberInArray}]/dokumentasjon", new string[] { "1.18" });
                            }
                        }
                    }
                }
            }
        }

        private List<ChecklistAnswer> GetChecklistAnswersFromDynamicPartOfDatamodel()
        {
            if (!Helpers.ObjectIsNullOrEmpty(_validationForm?.Sjekklistekrav))
            {
                List<ChecklistAnswer> list = new List<ChecklistAnswer>();
                foreach (var sjekklistepkt in _validationForm.Sjekklistekrav)
                {
                    var checklistAnswer = new ChecklistAnswer()
                    {
                        ChecklistQuestion = sjekklistepkt.Sjekklistepunkt.Kodebeskrivelse,
                        ChecklistReference = sjekklistepkt.Sjekklistepunkt.Kodeverdi,
                        YesNo = (bool)sjekklistepkt.Sjekklistepunktsvar,
                        Documentation = sjekklistepkt.Dokumentasjon ?? null,
                        SupportingDataValidationRuleId = new List<string> { $"{FormDataAttribute.DataFormatId}.{FormDataAttribute.DataFormatVersion}.{EnumHelper.GetEnumEntityValidatorNumber(EntityValidatorEnum.SjekklistekravValidator)}.{EnumHelper.GetEnumFieldNameNumber(FieldNameEnum.sjekklistepunktsvar)}.1" },
                        SupportingDataXpathField = new List<string>() { XPathRoot + "/krav/sjekklistekrav{0}/sjekklistepunktsvar" }
                    };

                    list.Add(checklistAnswer);
                }

                return list;
            }

            return new List<ChecklistAnswer>();
        }

        public List<ChecklistAnswer> GetChecklistAnswersFromStaticPartOfDatamodel(ValidationInput validationInput)
        {
            //Add prefilled checklist answers for data not part of the validation errors and warnings
            _validationForm = SerializeUtil.DeserializeFromString<ArbeidstilsynetsSamtykkeV2>(validationInput.FormData);

            var chkPoint_1_1 = _allCheckpointsFromAPIFlatList.First(x => x.Id.Equals("1.1"));
            var chkPoint_1_17 = _allCheckpointsFromAPIFlatList.First(x => x.Id.Equals("1.17"));
            var chkPoint_1_20 = _allCheckpointsFromAPIFlatList.First(x => x.Id.Equals("1.20"));
            var chkPoint_1_21 = _allCheckpointsFromAPIFlatList.First(x => x.Id.Equals("1.21"));
            var chkPoint_1_22 = _allCheckpointsFromAPIFlatList.First(x => x.Id.Equals("1.22"));

            var tiltakstyperInSjekkpunkt_1_1 = chkPoint_1_1.Tiltakstyper.Select(x => x.Kode).ToList();
            var checkpoint_1_1_IsRelevant = _tiltakstypes.ToList().Intersect(tiltakstyperInSjekkpunkt_1_1).Count() > 0;

            var tiltakstyperInSjekkpunkt_1_17 = chkPoint_1_17.Tiltakstyper.Select(x => x.Kode).ToList();
            var checkpoint_1_17_IsRelevant = _tiltakstypes.ToList().Intersect(tiltakstyperInSjekkpunkt_1_17).Count() > 0;

            var tiltakstyperInSjekkpunkt_1_21 = chkPoint_1_21.Tiltakstyper.Select(x => x.Kode).ToList();
            var checkpoint_1_21_IsRelevant = _tiltakstypes.ToList().Intersect(tiltakstyperInSjekkpunkt_1_21).Count() > 0;

            var tiltakstyperInSjekkpunkt_1_22 = chkPoint_1_22.Tiltakstyper.Select(x => x.Kode).ToList();
            var checkpoint_1_22_IsRelevant = _tiltakstypes.ToList().Intersect(tiltakstyperInSjekkpunkt_1_22).Count() > 0;

            List<ChecklistAnswer> list = new List<ChecklistAnswer>();

            if (_validationForm.Metadata != null && _validationForm.Metadata.ErNorskSvenskDansk != null && checkpoint_1_1_IsRelevant)
            {
                var checklistAnswer1_1 = new ChecklistAnswer()
                {
                    ChecklistQuestion = chkPoint_1_1.Navn,
                    ChecklistReference = chkPoint_1_1.Id,
                    YesNo = _validationForm.Metadata.ErNorskSvenskDansk.GetValueOrDefault(false),
                    SupportingDataValidationRuleId = new List<string>() { $"{FormDataAttribute.DataFormatId}.{FormDataAttribute.DataFormatVersion}.{EnumHelper.GetEnumEntityValidatorNumber(EntityValidatorEnum.MetadataAtilValidator)}.{EnumHelper.GetEnumFieldNameNumber(FieldNameEnum.erNorskSvenskDansk)}.1" },
                    SupportingDataXpathField = new List<string>() { XPathRoot + $"/{EnumHelper.GetEnumXmlNodeFromEntityValidatorEnum(EntityValidatorEnum.MetadataAtilValidator)}/{FieldNameEnum.erNorskSvenskDansk.ToString()}" }
                };
                list.Add(checklistAnswer1_1);
            }

            if (_validationForm.Arbeidsplasser != null && _validationForm.Arbeidsplasser.UtleieBygg != null && checkpoint_1_17_IsRelevant)
            {
                var checklistAnswer1_17 = new ChecklistAnswer()
                {
                    ChecklistQuestion = chkPoint_1_17.Navn,
                    ChecklistReference = chkPoint_1_17.Id,
                    YesNo = _validationForm.Arbeidsplasser.UtleieBygg.GetValueOrDefault(false),
                    SupportingDataValidationRuleId = new List<string>() { $"{FormDataAttribute.DataFormatId}.{FormDataAttribute.DataFormatVersion}.{EnumHelper.GetEnumEntityValidatorNumber(EntityValidatorEnum.ArbeidsplasserValidator)}.{EnumHelper.GetEnumFieldNameNumber(FieldNameEnum.utleieBygg)}.1" },
                    SupportingDataXpathField = new List<string>() { XPathRoot + $"/{EnumHelper.GetEnumXmlNodeFromEntityValidatorEnum(EntityValidatorEnum.ArbeidsplasserValidator)}/{FieldNameEnum.utleieBygg.ToString()}" }
                };
                list.Add(checklistAnswer1_17);
            }

            var checklistAnswer1_20 = new ChecklistAnswer()
            {
                ChecklistQuestion = chkPoint_1_20.Navn,
                ChecklistReference = chkPoint_1_20.Id,
                YesNo = !ValidationResult.ValidationMessages.Any(x => x.XpathField.Contains($"vedlegg/{AttachmentEnum.TiltakshaverSignatur.ToString()}")),
                SupportingDataValidationRuleId = new List<string>() { $"{FormDataAttribute.DataFormatId}.{FormDataAttribute.DataFormatVersion}.{EnumHelper.GetEnumEntityValidatorNumber(EntityValidatorEnum.VedleggAtilV2Validator)}.{EnumHelper.GetEnumAttachmentNumber(AttachmentEnum.TiltakshaverSignatur)}.1" },
                SupportingDataXpathField = new List<string>() { XPathRoot + $"/{EnumHelper.GetEnumXmlNodeFromEntityValidatorEnum(EntityValidatorEnum.VedleggAtilV2Validator)}/{AttachmentEnum.TiltakshaverSignatur.ToString()}" }
            };
            list.Add(checklistAnswer1_20);

            if (_validationForm.Arbeidsplasser != null && _validationForm.Arbeidsplasser.Veiledning != null && checkpoint_1_21_IsRelevant)
            {
                var checklistAnswer1_21 = new ChecklistAnswer()
                {
                    ChecklistQuestion = chkPoint_1_21.Navn,
                    ChecklistReference = chkPoint_1_21.Id,
                    YesNo = _validationForm.Arbeidsplasser.Veiledning.GetValueOrDefault(false),
                    SupportingDataValidationRuleId = new List<string>() { $"{FormDataAttribute.DataFormatId}.{FormDataAttribute.DataFormatVersion}.{EnumHelper.GetEnumEntityValidatorNumber(EntityValidatorEnum.ArbeidsplasserValidator)}.{EnumHelper.GetEnumFieldNameNumber(FieldNameEnum.veiledning)}.1" },
                    SupportingDataXpathField = new List<string>() { XPathRoot + $"/{EnumHelper.GetEnumXmlNodeFromEntityValidatorEnum(EntityValidatorEnum.ArbeidsplasserValidator)}/{FieldNameEnum.veiledning.ToString()}" }
                };

                list.Add(checklistAnswer1_21);
            }

            if (_validationForm.Metadata != null && _validationForm.Metadata.UnntattOffentlighet != null && checkpoint_1_22_IsRelevant)
            {
                var checklistAnswer1_22 = new ChecklistAnswer()
                {
                    ChecklistQuestion = chkPoint_1_22.Navn,
                    ChecklistReference = chkPoint_1_22.Id,
                    YesNo = _validationForm.Metadata.UnntattOffentlighet.GetValueOrDefault(false),
                    SupportingDataValidationRuleId = new List<string>() { $"{FormDataAttribute.DataFormatId}.{FormDataAttribute.DataFormatVersion}.{EnumHelper.GetEnumEntityValidatorNumber(EntityValidatorEnum.MetadataAtilValidator)}.{EnumHelper.GetEnumFieldNameNumber(FieldNameEnum.unntattOffentlighet)}.1" },
                    SupportingDataXpathField = new List<string>() { XPathRoot + $"/{EnumHelper.GetEnumXmlNodeFromEntityValidatorEnum(EntityValidatorEnum.MetadataAtilValidator)}/{FieldNameEnum.unntattOffentlighet.ToString()}" }
                };
                list.Add(checklistAnswer1_22);
            }

            return list;
        }

        protected override void CustomFormValidation(ValidationInput validationInput)
        {
            CustomSjekklisteValidations(_validationForm?.Arbeidsplasser, _validationForm?.Sjekklistekrav);
        }

        protected override DateTime? GetFormValidationDate()
        {
            return DateTime.Now;
        }
    }
}