﻿using System;
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class AvfalldisponeringValidator : EntityValidatorBase, IAvfalldisponeringValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        private readonly ILeveringsstedValidator _leveringsstedValidator;
        private readonly IKodelisteValidator _avfalldisponeringsmaateValidator;
        public string _entityXPath { get => base.EntityXPath; }


        public AvfalldisponeringValidator(IList<EntityValidatorNode> entityValidatorTree, ILeveringsstedValidator leveringsstedValidator, IKodelisteValidator avfalldisponeringsmaateValidator)
            : base(entityValidatorTree)
        {
            _leveringsstedValidator = leveringsstedValidator;
            _avfalldisponeringsmaateValidator = avfalldisponeringsmaateValidator;
            AddValidateDataRelationsRules();
        }

        private void AddValidateDataRelationsRules()
        {
            AccumulateValidationRules(_leveringsstedValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_avfalldisponeringsmaateValidator.ValidationResult.ValidationRules);

            AddValidationRule(ValidationRuleEnum.utfylt, null, _avfalldisponeringsmaateValidator._entityXPath);
        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
        }

        public ValidationResult Validate(Avfalldisponering avfalldisponering = null, string fraksjonKodebeskrivelse = null)
        {
            base.ResetValidationMessages();

            ValidateEntityFields(avfalldisponering);

            if (!Helpers.ObjectIsNullOrEmpty(avfalldisponering))
            {
                var leveringsstedValidationResult = _leveringsstedValidator.Validate(avfalldisponering?.Leveringssted, fraksjonKodebeskrivelse);
                UpdateValidationResultWithSubValidations(leveringsstedValidationResult);

                if (Helpers.ObjectIsNullOrEmpty(avfalldisponering?.Disponeringsmaate))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, _avfalldisponeringsmaateValidator._entityXPath, new[] { fraksjonKodebeskrivelse });
                }
                else
                {
                    var avfalldisponeringsmaateValidationResult = _avfalldisponeringsmaateValidator.Validate(avfalldisponering?.Disponeringsmaate);
                    UpdateValidationResultWithSubValidations(avfalldisponeringsmaateValidationResult);
                }
            }

            return _validationResult;
        }
        public void ValidateEntityFields(Avfalldisponering avfalldisponering)
        {
            if (Helpers.ObjectIsNullOrEmpty(avfalldisponering))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
        }
    }
}
