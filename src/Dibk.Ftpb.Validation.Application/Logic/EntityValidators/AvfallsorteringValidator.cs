﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class AvfallsorteringValidator : EntityValidatorBase, IAvfallsorteringValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        public string _entityXPath { get => base.EntityXPath; }

        public AvfallsorteringValidator(IList<EntityValidatorNode> entityValidatorTree, int? nodeId)
            : base(entityValidatorTree, nodeId)
        {
        }

        protected override void InitializeValidationRules()
        {            
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.mengdeSortert);
            AddValidationRule(ValidationRuleEnum.numerisk, FieldNameEnum.mengdeSortert);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.mengdeTotalt);
            AddValidationRule(ValidationRuleEnum.numerisk, FieldNameEnum.mengdeTotalt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.sorteringsgrad);
            AddValidationRule(ValidationRuleEnum.numerisk, FieldNameEnum.sorteringsgrad);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.sorteringsgrad, null, "/overgangsordning/datoForHovedsoeknad før 01.07.2022", null, new DateTime(2022, 06, 30));
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.sorteringsgrad, null, "/overgangsordning/overgangsbestemmelse true", new DateTime(2022, 07, 01), new DateTime(2023, 06, 30));
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.sorteringsgrad, null, "/overgangsordning/overgangsbestemmelse false", new DateTime(2022, 07, 01), new DateTime(2023, 06, 30));
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.sorteringsgrad, null, "/overgangsordning/datoForHovedsoeknad etter 30.06.2023", new DateTime(2023, 07, 01));
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.mengdeAvfallPerAreal);
            AddValidationRule(ValidationRuleEnum.numerisk, FieldNameEnum.mengdeAvfallPerAreal);
        }

        public ValidationResult Validate(Avfallsortering avfallsortering = null, Overgangsordning overgangsordning = null, string[] attachmentsTypesName = null)
        {
            ValidateEntityFields(avfallsortering, overgangsordning, attachmentsTypesName);
            ValidateDataRelations(avfallsortering, overgangsordning, attachmentsTypesName);

            return _validationResult;
        }

        public void ValidateDataRelations(Avfallsortering avfallsortering = null, Overgangsordning overgangsordning = null, string[] attachmentsTypesName = null)
        {
            if (!Helpers.ObjectIsNullOrEmpty(avfallsortering.Sorteringsgrad) || decimal.TryParse(avfallsortering?.Sorteringsgrad, out var sorteringsgradResult))
                ValidateSorteringsgrad(avfallsortering, overgangsordning, attachmentsTypesName);
        }

        public ValidationResult ValidateEntityFields(Avfallsortering avfallsortering, Overgangsordning overgangsordning, string[] attachmentsTypesName = null)
        {
            if (Helpers.ObjectIsNullOrEmpty(avfallsortering))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (string.IsNullOrEmpty(avfallsortering?.MengdeSortert))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.mengdeSortert);
                else if (!decimal.TryParse(avfallsortering?.MengdeSortert, out var mengdeSortertResult))
                    AddMessageFromRule(ValidationRuleEnum.numerisk, FieldNameEnum.mengdeSortert);

                if (string.IsNullOrEmpty(avfallsortering?.MengdeTotalt))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.mengdeTotalt);
                else if (!decimal.TryParse(avfallsortering?.MengdeTotalt, out var mengdeTotaltResult))
                    AddMessageFromRule(ValidationRuleEnum.numerisk, FieldNameEnum.mengdeTotalt);

                if (string.IsNullOrEmpty(avfallsortering?.Sorteringsgrad))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.sorteringsgrad);
                else if (!decimal.TryParse(avfallsortering?.Sorteringsgrad, out var sorteringsgradResult))
                    AddMessageFromRule(ValidationRuleEnum.numerisk, FieldNameEnum.sorteringsgrad);

                if (string.IsNullOrEmpty(avfallsortering?.MengdeAvfallPerAreal))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.mengdeAvfallPerAreal);
                else if (!decimal.TryParse(avfallsortering?.MengdeAvfallPerAreal, out var mengdeAvfallPerArealResult))
                    AddMessageFromRule(ValidationRuleEnum.numerisk, FieldNameEnum.mengdeAvfallPerAreal);
            }

            return _validationResult;
        }

        public void ValidateSorteringsgrad(Avfallsortering avfallsortering, Overgangsordning overgangsordning, string[] attachmentsTypesName = null)
        {
            if (attachmentsTypesName == null || !attachmentsTypesName.Any(a => a.Equals("AndreRedegjoerelser")))
            {
                //gammel <60% og før 30.06.2022
                if (overgangsordning?.DatoForHovedsoeknad <= new DateTime(2022, 7, 1))
                {
                    if (decimal.Parse(avfallsortering.Sorteringsgrad) < 60)
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.sorteringsgrad, null, "/overgangsordning/datoForHovedsoeknad før 01.07.2022");
                }

                //mellom 01.07.2022 og 30.06.2023
                if (new DateTime(2022, 7, 1) <= overgangsordning?.DatoForHovedsoeknad &&
                    overgangsordning?.DatoForHovedsoeknad <= new DateTime(2023, 6, 30))
                {
                    if (overgangsordning.Overgangsbestemmelse != null)
                    {
                        if (overgangsordning.Overgangsbestemmelse.GetValueOrDefault())
                        {
                            if (decimal.Parse(avfallsortering.Sorteringsgrad) < 60)
                            {
                                // 60% Overgangsbestemmelse = true
                                AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.sorteringsgrad, null, "/overgangsordning/overgangsbestemmelse true");
                            }
                        }
                        else
                        {
                            if (decimal.Parse(avfallsortering.Sorteringsgrad) < 70)
                            {
                                // 70% Overgangsbestemmelse = false
                                AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.sorteringsgrad, null, "/overgangsordning/overgangsbestemmelse false");
                            }
                        }
                    }
                }

                // Etter 30.06.2023
                if (overgangsordning?.DatoForHovedsoeknad >= new DateTime(2023, 7, 1))
                {
                    if (decimal.Parse(avfallsortering.Sorteringsgrad) < 70)
                    {
                        // 70% syste ny regler   
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.sorteringsgrad, null, "/overgangsordning/datoForHovedsoeknad etter 30.06.2023");
                    }
                }
            }
        }
    }
}


