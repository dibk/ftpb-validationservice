﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class UtloestFraSjekkpunktValidator : EntityValidatorBase, IUtloestFraSjekkpunktValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }

        public UtloestFraSjekkpunktValidator(IList<EntityValidatorNode> entityValidatorTree, int? nodeId = null) : base(entityValidatorTree, nodeId)
        {
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.sjekkpunktId);
        }

        public ValidationResult Validate(Sjekkpunkt sjekkpunkt)
        {
            ResetValidationMessages();

            if (!string.IsNullOrEmpty(sjekkpunkt?.SjekkpunktId))
                if (!Regex.IsMatch(sjekkpunkt.SjekkpunktId, @"\d+\.\d+"))
                    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.sjekkpunktId, new[] { sjekkpunkt.SjekkpunktId });

            return _validationResult;
        }
    }
}
