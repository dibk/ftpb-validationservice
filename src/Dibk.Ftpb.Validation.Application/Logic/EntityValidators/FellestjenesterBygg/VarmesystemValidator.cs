﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class VarmesystemValidator : EntityValidatorBase, IVarmesystemValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }

        IKodelisteValidator _energiforsyningValidator;
        IKodelisteValidator _varmefordelingValidator;

        public VarmesystemValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator energiforsyningValidator, IKodelisteValidator varmefordelingValidator)
            : base(entityValidatorTree)
        {
            _energiforsyningValidator = energiforsyningValidator;
            _varmefordelingValidator = varmefordelingValidator;
            InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            if (_energiforsyningValidator != null)
            {
                AccumulateValidationRules(_energiforsyningValidator.ValidationResult.ValidationRules);
            }
            if (_varmefordelingValidator != null)
            {
                AccumulateValidationRules(_varmefordelingValidator.ValidationResult.ValidationRules);
            }
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
        }

        public ValidationResult Validate(Varmesystem varmesystem)
        {
            ResetValidationMessages();

            if (Helpers.ObjectIsNullOrEmpty(varmesystem))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (varmesystem.ErRelevant != false)
                {
                    if (_energiforsyningValidator != null)
                    {
                        var index = GetArrayIndex(varmesystem.Energiforsyning);
                        for (int i = 0; i < index; i++)
                        {
                            var energiforsyning = Helpers.ObjectIsNullOrEmpty(varmesystem.Energiforsyning) ? null : varmesystem.Energiforsyning[i];

                            var energiforsyningTypeValidatorResult = _energiforsyningValidator.Validate(energiforsyning);
                            AccumulateValidationMessages(energiforsyningTypeValidatorResult.ValidationMessages, i);
                        }
                    }
                    if (_varmefordelingValidator != null)
                    {
                        var index = GetArrayIndex(varmesystem.Varmefordeling);
                        for (int i = 0; i < index; i++)
                        {
                            var varmefordeling = Helpers.ObjectIsNullOrEmpty(varmesystem.Varmefordeling) ? null : varmesystem.Varmefordeling[i];

                            var varmefordelingTypeValidatorResult = _varmefordelingValidator.Validate(varmefordeling);
                            AccumulateValidationMessages(varmefordelingTypeValidatorResult.ValidationMessages, i);
                        }
                    }
                }
            }



            return _validationResult;
        }


    }
}