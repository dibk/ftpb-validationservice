﻿using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class TemaValidator : KodelisteValidatorV2
    {
        public TemaValidator(IList<EntityValidatorNode> entityValidatorTree, int nodeId, ICodeListService codeListService, bool allowedNull = false)
            : base(entityValidatorTree, nodeId, FtbKodeListeEnum.Tema, RegistryType.EByggesak, codeListService, allowedNull)
        {
        }
    }
}
