﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg;

public class GenerelleVilkaarValidator: EntityValidatorBase, IGenerelleVilkaarValidator
{

    public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }
        
    public GenerelleVilkaarValidator(IList<EntityValidatorNode> entityValidatorTree, int? nodeId = null) : base(entityValidatorTree, nodeId)
    {
    }

    protected override void InitializeValidationRules()
    {
        AddValidationRule(ValidationRuleEnum.utfylt);
        AddValidationRule(ValidationRuleEnum.utfylt,FieldNameEnum.norskSvenskDansk);
    }

    public ValidationResult Validate(GenerelleVilkaar generelleVilkaarValidationEntity)
    {
        if (generelleVilkaarValidationEntity == null)
        {
            AddMessageFromRule(ValidationRuleEnum.utfylt);
        }
        else
        {
            AddMessagesFromRules(generelleVilkaarValidationEntity);
        }
        return _validationResult;
    }

    protected virtual void AddMessagesFromRules(GenerelleVilkaar generelleVilkaarValidationEntity)
    {
        if (Helpers.ObjectIsNullOrEmpty(generelleVilkaarValidationEntity.NorskSvenskDansk))
        {
            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.norskSvenskDansk);
        }
    }
}