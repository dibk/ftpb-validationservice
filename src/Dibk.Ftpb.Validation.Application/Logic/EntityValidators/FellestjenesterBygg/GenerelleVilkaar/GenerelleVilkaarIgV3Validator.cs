﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg;

public class GenerelleVilkaarIgV3Validator : GenerelleVilkaarValidator
{
    public GenerelleVilkaarIgV3Validator(IList<EntityValidatorNode> entityValidatorTree, int? nodeId = null) : base(entityValidatorTree, nodeId)
    {
    }

    protected override void InitializeValidationRules()
    {
        base.InitializeValidationRules();

        AddValidationRule(ValidationRuleEnum.utfylt,FieldNameEnum.beroererArbeidsplasser);
    }

    protected override void AddMessagesFromRules(GenerelleVilkaar generelleVilkaarValidationEntity)
    {
        if (generelleVilkaarValidationEntity is not GenerelleVilkaarIgV3 generelleVilkaarIgV3)
            throw new ArgumentException("GenerelleVilkaarIgV3Validator can only validate GenerelleVilkaarIgV3", "generelleVilkaarValidationEntity");

        if (Helpers.ObjectIsNullOrEmpty(generelleVilkaarIgV3.BeroererArbeidsplasser))
        {
            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.beroererArbeidsplasser);
        }

        base.AddMessagesFromRules(generelleVilkaarValidationEntity);
    }
}