﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class EnergiforsyningValidator : KodelisteValidatorV2
    {
        public EnergiforsyningValidator
           (IList<EntityValidatorNode> entityValidatorTree, int nodeId, ICodeListService codeListService, bool allowedNull = false, string[] allowedKodeverdi = null)
           : base(entityValidatorTree, nodeId, FtbKodeListeEnum.Energiforsyningtype, RegistryType.Byggesoknad, codeListService, allowedNull, allowedKodeverdi)
        {
        }
    }
}