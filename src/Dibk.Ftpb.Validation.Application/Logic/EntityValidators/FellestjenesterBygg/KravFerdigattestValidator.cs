﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class KravFerdigattestValidator : EntityValidatorBase
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }

        public KravFerdigattestValidator(IList<EntityValidatorNode> entityValidatorTree, int? nodeId = null) : base(entityValidatorTree, nodeId)
        {
        }

        protected override void InitializeValidationRules()
        {

            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.tilfredsstillerTiltaketKraveneFerdigattest);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.typeArbeider, preCondition: "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false");
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.utfoertInnen, preCondition: "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false");
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.utfoertInnen, preCondition: "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false");
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.bekreftelseInnen, preCondition: "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false");
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.bekreftelseInnen, preCondition: "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false");
        }

        public ValidationResult Validate(KravFerdigattest kravFerdigattest)
        {
            if (Helpers.ObjectIsNullOrEmpty(kravFerdigattest))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (!kravFerdigattest.TilfredsstillerTiltaketKraveneFerdigattest.HasValue)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.tilfredsstillerTiltaketKraveneFerdigattest);
                }

                if (kravFerdigattest.TilfredsstillerTiltaketKraveneFerdigattest == false)
                {
                    if (string.IsNullOrEmpty(kravFerdigattest.TypeArbeider))
                    {
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.typeArbeider, preConditionField: "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false");
                    }
                    if (kravFerdigattest.UtfoertInnen is null)
                    {
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.utfoertInnen, preConditionField: "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false");
                    }
                    else if (kravFerdigattest.UtfoertInnen < DateTime.Today || kravFerdigattest.UtfoertInnen > DateTime.Today.AddDays(14))
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.utfoertInnen, preConditionField: "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false");
                    }
                    if (kravFerdigattest.BekreftelseInnen is null)
                    {
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.bekreftelseInnen, preConditionField: "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false");
                    }
                    else if (kravFerdigattest.BekreftelseInnen < DateTime.Today || kravFerdigattest.BekreftelseInnen > DateTime.Today.AddDays(14))
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.bekreftelseInnen, preConditionField: "/kravFerdigattest/tilfredsstillerTiltaketKraveneFerdigattest = false");
                    }
                }


            }

            return _validationResult;
        }
    }
}