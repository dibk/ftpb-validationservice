﻿using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class UtfallTypeValidator : KodelisteValidatorV2
    {
        public UtfallTypeValidator(IList<EntityValidatorNode> entityValidatorTree, int nodeId, ICodeListService codeListService,
            bool allowedNull = false,
            string[] allowedKodeverdi = null)
            : base(entityValidatorTree, nodeId, FtbKodeListeEnum.utfall, RegistryType.EByggesak, codeListService,allowedNull,allowedKodeverdi)
        {
        }
    }
}
