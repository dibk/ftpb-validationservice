﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class GjenstaaendeArbeiderValidator : EntityValidatorBase, IGjenstaaendeArbeiderValidator
    {
        public string _entityXPath { get => base.EntityXPath; }
        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public GjenstaaendeArbeiderValidator(IList<EntityValidatorNode> entityValidatorTree, int? nodeId = null) : base(entityValidatorTree, nodeId)
        {
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.gjenstaaendeInnenfor);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.gjenstaaendeUtenfor, preCondition: "/soeknadGjelder/gjelderHeleTiltaket = false");
        }

        public ValidationResult Validate(GjenstaaendeArbeider gjenstaaendeArbeider, bool? gjelderHeleTiltaket)
        {
            if (Helpers.ObjectIsNullOrEmpty(gjenstaaendeArbeider))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (string.IsNullOrEmpty(gjenstaaendeArbeider.GjenstaaendeInnenfor))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.gjenstaaendeInnenfor);
                }
                if (gjelderHeleTiltaket.HasValue && !gjelderHeleTiltaket.Value)
                {
                    if (string.IsNullOrEmpty(gjenstaaendeArbeider.GjenstaaendeUtenfor))
                    {
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.gjenstaaendeUtenfor, preConditionField: "/soeknadGjelder/gjelderHeleTiltaket = false");
                    }
                }
            }
            return _validationResult;
        }
    }
}
