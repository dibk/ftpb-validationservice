
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class MetadataAvfallsplanV2Validator : EntityValidatorBase, IMetadataFtbValidator
    {
        ValidationResult IMetadataFtbValidator.ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public MetadataAvfallsplanV2Validator(IList<EntityValidatorNode> entityValidatorTree)
            : base(entityValidatorTree)
        {
        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.fraSluttbrukersystem);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.prosjektnavn);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.prosjektnr);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.versjonsnummerAvfallsplan);
        }

        public ValidationResult Validate(MetadataSluttrapport metadataValidationEntity)
        {
            ResetValidationMessages();
            if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                ValidateEntityFields(metadataValidationEntity);
            }

            return _validationResult;
        }

        public void ValidateEntityFields(MetadataSluttrapport metadataValidationEntity)
        {
            if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity.FraSluttbrukersystem))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.fraSluttbrukersystem);

            if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity.Prosjektnavn))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.prosjektnavn);

            if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity.Prosjektnr))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.prosjektnr);

            if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity.VersjonsnummerAvfallsplan))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.versjonsnummerAvfallsplan);

        }
    }
}
