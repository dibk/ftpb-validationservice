﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class SoeknadGjelderBaseValidator : EntityValidatorBase, ISoeknadGjelderBaseValidator
    {
        public readonly IKodelisteValidator _soeknadTypeMedAnsvarsrettValidator;
        public readonly IKodelisteValidator _soeknadTypeUtenAnsvarsrettValidator;

        public bool _soeknadCanBeWithOrWithoutAnsvarsrett;

        public string _entityXPath { get => base.EntityXPath; }

        public List<string> _Tiltakstypes;
        public string[] Tiltakstypes { get => _Tiltakstypes?.ToArray(); }

        public readonly string _formName;

        public SoeknadGjelderBaseValidator
        (
            IList<EntityValidatorNode> entityValidatorTree,
            IKodelisteValidator soeknadTypeMedAnsvarsrettValidator,
            IKodelisteValidator soeknadTypeUtenAnsvarsrettValidator,
            string formName,
            int? nodeId = null
        ) : base(entityValidatorTree, nodeId)
        {
            _formName = formName;
            _soeknadTypeMedAnsvarsrettValidator = soeknadTypeMedAnsvarsrettValidator;
            _soeknadTypeUtenAnsvarsrettValidator = soeknadTypeUtenAnsvarsrettValidator;
            _soeknadCanBeWithOrWithoutAnsvarsrett = soeknadTypeMedAnsvarsrettValidator != null && soeknadTypeUtenAnsvarsrettValidator != null;
            _Tiltakstypes = new List<string>();
            InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_soeknadTypeMedAnsvarsrettValidator.ValidationResult.ValidationRules);

            if (_soeknadTypeUtenAnsvarsrettValidator != null)
                AccumulateValidationRules(_soeknadTypeUtenAnsvarsrettValidator.ValidationResult.ValidationRules);

            if (_soeknadCanBeWithOrWithoutAnsvarsrett)
                AddValidationRule(ValidationRuleEnum.validert, FieldNameEnum.type, preCondition: "/ansvarForByggesaken/kodeverdi ugyldig");
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.type);
        }

        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }


        public ValidationResult Validate(SoeknadGjelderBase soeknadGjelder, Kodeliste ansvarForByggesaken)
        {
            if (Helpers.ObjectIsNullOrEmpty(soeknadGjelder))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                var typeValidator = ResolveSoeknadTypeValidator(ansvarForByggesaken?.Kodeverdi);

                if (typeValidator == null)
                {
                    AddMessageFromRule(ValidationRuleEnum.validert, FieldNameEnum.type, preConditionField: "/ansvarForByggesaken/kodeverdi ugyldig");
                }
                else if (Helpers.ObjectIsNullOrEmpty(soeknadGjelder.Type))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.type);
                }
                else
                {
                    var index = GetArrayIndex(soeknadGjelder.Type);
                    for (int i = 0; i < index; i++)
                    {
                        var type = soeknadGjelder.Type[i];
                        var typeValidatorResult = typeValidator.Validate(type);
                        AccumulateValidationMessages(typeValidatorResult.ValidationMessages, i);

                        if (!IsAnyValidationMessagesWithXpath($"{_soeknadTypeMedAnsvarsrettValidator._entityXPath}/{FieldNameEnum.kodeverdi}", i))
                            _Tiltakstypes.Add(type.Kodeverdi);
                    }
                }

            }
            return _validationResult;
        }

        private IKodelisteValidator ResolveSoeknadTypeValidator(string ansvarForByggesaken)
        {
            // Hvis søknaden kan være med eller uten ansvarsrett, velger vi validator basert på 'ansvarForByggesaken'.
            // Dersom ansvarForByggesaken ikke har en gyldig verdi, returnerer vi null, noe som indikerer at vi ikke
            // får validert søknadstypen.
            if (_soeknadCanBeWithOrWithoutAnsvarsrett)
                return ansvarForByggesaken switch
                {
                    "medAnsvar" or "selvbygger" => _soeknadTypeMedAnsvarsrettValidator,
                    "utenAnsvar" or "utenAnsvarMedKontroll" => _soeknadTypeUtenAnsvarsrettValidator,
                    _ => null
                };

            // Hvis søknaden kun kan være med eller uten ansvarsrett, trenger vi ikke å sjekke 'ansvarForByggesaken'
            // for å velge validator, dermed returnerer vi den validatoren som er satt
            return _soeknadTypeMedAnsvarsrettValidator ?? _soeknadTypeUtenAnsvarsrettValidator;
        }
    }
}
