﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class SikkerhetsnivaaValidator : EntityValidatorBase, ISikkerhetsnivaaValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public string _entityXPath { get => base.EntityXPath; }

        public SikkerhetsnivaaValidator(IList<EntityValidatorNode> entityValidatorTree, int? nodeId = null) : base(entityValidatorTree, nodeId)
        {
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.harTilstrekkeligSikkerhet);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.utfoertInnen, preCondition: "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false");
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.utfoertInnen, preCondition: "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false");
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.typeArbeider, preCondition: "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false");
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.bekreftelseInnen, preCondition: "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false");
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.bekreftelseInnen, preCondition: "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false");
        }

        public ValidationResult Validate(Sikkerhetsnivaa sikkerhetsnivaa)
        {
            if (Helpers.ObjectIsNullOrEmpty(sikkerhetsnivaa))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (!sikkerhetsnivaa.HarTilstrekkeligSikkerhet.HasValue)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.harTilstrekkeligSikkerhet);
                }
                else if (!sikkerhetsnivaa.HarTilstrekkeligSikkerhet.Value)
                {
                    if (!sikkerhetsnivaa.UtfoertInnen.HasValue)
                    {
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.utfoertInnen, preConditionField: "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false");
                    }
                    else if (sikkerhetsnivaa.UtfoertInnen.Value <= DateTime.Now || sikkerhetsnivaa.UtfoertInnen.Value >= DateTime.Now.AddDays(14))
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.utfoertInnen, preConditionField: "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false");
                    }
                    if (string.IsNullOrEmpty(sikkerhetsnivaa.TypeArbeider))
                    {
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.typeArbeider, preConditionField: "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false");
                    }
                    if (!sikkerhetsnivaa.BekreftelseInnen.HasValue)
                    {
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.bekreftelseInnen, preConditionField: "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false");
                    }
                    else if (sikkerhetsnivaa.BekreftelseInnen.Value <= DateTime.Now || sikkerhetsnivaa.BekreftelseInnen.Value >= DateTime.Now.AddDays(14))
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.bekreftelseInnen, preConditionField: "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false");
                    }
                }
            }

            return _validationResult;
        }
    }
}