﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class DelsoeknaderValidator: EntityValidatorBase, IDelsoeknaderValidator
    {
        private readonly IKodelisteValidator _typeValidator;
        public string _entityXPath { get => base.EntityXPath; }
        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public DelsoeknaderValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator typeValidator = null, int? nodeId = null) : base(entityValidatorTree, nodeId)
        {
            _typeValidator = typeValidator;

            if (_typeValidator != null)
                InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_typeValidator.ValidationResult.ValidationRules);
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.delAvTiltaket);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.tillatelsedato);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.delsoeknadsnummer);
            AddValidationRule(ValidationRuleEnum.numerisk, FieldNameEnum.delsoeknadsnummer);
        }

        public ValidationResult Validate(Delsoeknad delsoeknad)
        {
            ResetValidationMessages();

            if (Helpers.ObjectIsNullOrEmpty(delsoeknad))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (string.IsNullOrEmpty(delsoeknad.DelAvTiltaket))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.delAvTiltaket);

                if (!delsoeknad.Tillatelsedato.HasValue)
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.tillatelsedato);

                if (_typeValidator != null)
                {
                    var index = GetArrayIndex(delsoeknad.Type);
                    for (int i = 0; i < index; i++)
                    {
                        Kodeliste type = Helpers.ObjectIsNullOrEmpty(delsoeknad.Type) ? null : delsoeknad.Type[i];
                        var typeValidatorResult = _typeValidator.Validate(type);
                        AccumulateValidationMessages(typeValidatorResult.ValidationMessages, i);
                    }
                }
                if (string.IsNullOrEmpty(delsoeknad.Delsoeknadsnummer))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.delsoeknadsnummer);
                else if (!int.TryParse(delsoeknad.Delsoeknadsnummer, out var Delsoeknadsnummer))
                    AddMessageFromRule(ValidationRuleEnum.numerisk, FieldNameEnum.delsoeknadsnummer);

            }
            return _validationResult;
        }     
    }
}
