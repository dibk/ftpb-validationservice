﻿using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class VedleggFAValidator : VedleggByggValidatorBase<Ferdigattest>
    {
        public VedleggFAValidator(IList<EntityValidatorNode> entityValidatorTree) : base(entityValidatorTree)
        {
        }

        protected override void InitializeValidationRules()
        {
            base.InitializeValidationRules();

            AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.ErklaeringSelvbygger, "/ansvarForByggesaken/kodeverdi = 'selvbygger'");
            AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Gjennomføringsplan,
                "/ansvarForByggesaken/kodeverdi = 'medAnsvar' || 'utenAnsvarMedKontroll'",
                $"{XPathFilterForAttachments}");
            AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.SituasjonsplanOgTegning, "/foretattIkkeSoeknadspliktigeJusteringer = 'true'",
                $"{XPathFilterForAttachments}");
        }

        public override ValidationResult Validate(AttachmentInfo[] attachmentsInApplication, Ferdigattest form)
        {
            base.Validate(attachmentsInApplication, form);

            ValidateSituasjonsplanAndTegningIsAttached(form);

            return _validationResult;
        }

        protected override UtfallSvar[] GetUtfallBesvarelse(Ferdigattest form)
        {
            return form?.UtfallBesvarelse;
        }

        protected override void RunValidationsRelatedToAnsvarForByggesaken(Ferdigattest form)
        {
            var ansvarForByggesaken = form?.AnsvarForByggesaken?.Kodeverdi;

            if (ansvarForByggesaken == "selvbygger")
            {
                ValidateAttachmentsForSelvbygger(ansvarForByggesaken);
            }
            else if (ansvarForByggesaken is "medAnsvar" or "utenAnsvarMedKontroll")
            {
                ValidateGjennomfoeringsplanIsAttached("/ansvarForByggesaken/kodeverdi = 'medAnsvar' || 'utenAnsvarMedKontroll'");
            }
        }

        private void ValidateAttachmentsForSelvbygger(string ansvarForByggesaken)
        {
            var egenerklaeringExists = AttachmentsInApplication?.FirstOrDefault(x =>
                x.AttachmentTypeName == AttachmentEnum.ErklaeringSelvbygger.ToString()) != null;

            if (!egenerklaeringExists)
                AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.ErklaeringSelvbygger}", new[] { ansvarForByggesaken },
                    preConditionField: "/ansvarForByggesaken/kodeverdi = 'selvbygger'");
        }

        private void ValidateSituasjonsplanAndTegningIsAttached(Ferdigattest form)
        {
            if (form.ErForetattIkkeSoeknadspliktigeJusteringer != null && form.ErForetattIkkeSoeknadspliktigeJusteringer == true)
            {
                var situasjonsplanExists = AttachmentsInApplication?.FirstOrDefault(x =>
                    x.AttachmentTypeName == "Situasjonsplan") != null;

                var attachmentNames = new[] { "TegningNyPlan", "TegningNyttSnitt", "TegningNyFasade" };

                var tegningIsAttached = AttachmentsInApplication?.FirstOrDefault(x =>
                        attachmentNames.Contains(x.AttachmentTypeName)) != null;

                if (!situasjonsplanExists || !tegningIsAttached)
                    AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.SituasjonsplanOgTegning}",
                        preConditionField: "/foretattIkkeSoeknadspliktigeJusteringer = 'true'");
            }
        }
    }
}
