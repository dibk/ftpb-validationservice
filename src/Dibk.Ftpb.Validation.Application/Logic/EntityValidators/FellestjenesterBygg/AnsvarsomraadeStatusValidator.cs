﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class AnsvarsomraadeStatusValidator : KodelisteValidatorV2
    {
        public AnsvarsomraadeStatusValidator(IList<EntityValidatorNode> entityValidatorTree, int nodeId, ICodeListService codeListService)
            : base(entityValidatorTree, nodeId, FtbKodeListeEnum.AnsvarsomraadeStatus, RegistryType.Byggesoknad, codeListService)
        {
            _codeListService = codeListService;
        }
    }
}