using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class UtfallBesvarelseFAValidator : UtfallBesvarelseValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        public string _entityXPath { get => base.EntityXPath; }

        public UtfallBesvarelseFAValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator utfalltypeValidator, IUtloestFraSjekkpunktValidator utloestFraSjekkpunktValidator, IKodelisteValidator temaValidator,
            IVedleggslisteValidator vedleggslisteValidator) : base(entityValidatorTree, utfalltypeValidator, utloestFraSjekkpunktValidator, temaValidator, vedleggslisteValidator)
        {
        }

        protected override void InitializeBesvartStatusRules()
        {
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.erUtfallBesvaresSenere, preCondition: "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere != false");
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.erUtfallBesvart, preCondition: "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere = false");
        }

        protected override void ValidateBesvartStatus(UtfallSvar utfallSvar, int? delsoeknadsnummer, string utfallKodebeskrivelse, string utfallTypeKoebeskrivelseXpath)
        {
            if (!utfallSvar.ErUtfallBesvaresSenere.HasValue || utfallSvar.ErUtfallBesvaresSenere.Value)
                AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.erUtfallBesvaresSenere, new[] { utfallKodebeskrivelse }, $"{_entityXPath}/erUtfallBesvaresSenere != false");
            else
            {
                if (!utfallSvar.ErUtfallBesvart.HasValue)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.erUtfallBesvart, new[] { utfallKodebeskrivelse }, $"{_entityXPath}/erUtfallBesvaresSenere = false");
                }
                else if (!utfallSvar.ErUtfallBesvart.Value)
                {
                    var vedlegg = utfallSvar.Vedleggsliste?.Where(u => !Helpers.ObjectIsNullOrEmpty(u)).ToArray();

                    var isAnyAttachment = !Helpers.ObjectIsNullOrEmpty(vedlegg);

                    if (!isAnyAttachment && string.IsNullOrEmpty(utfallSvar.Kommentar))
                        AddMessageFromRule(
                            ValidationRuleEnum.utfylt,
                            FieldNameEnum.kommentarEllerVedlegg,
                            new[] { utfallKodebeskrivelse },
                            "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere = false & /utfallBesvarelse/utfallSvar{0}/erUtfallBesvart = false");
                }
            }
        }
    }
}
