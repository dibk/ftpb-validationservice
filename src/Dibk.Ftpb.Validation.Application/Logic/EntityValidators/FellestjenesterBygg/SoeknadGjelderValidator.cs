﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class SoeknadGjelderValidator : SoeknadGjelderBaseValidator
    {
        public int? Delsoeknadsnummer { get; private set; }

        public SoeknadGjelderValidator
        (
            IList<EntityValidatorNode> entityValidatorTree,
            IKodelisteValidator soeknadTypeMedAnsvarsrettValidator,
            IKodelisteValidator soeknadTypeUtenAnsvarsrettValidator,
            string formName,
            int? nodeId = null
        ) : base(entityValidatorTree, soeknadTypeMedAnsvarsrettValidator, soeknadTypeUtenAnsvarsrettValidator, formName, nodeId)
        {
            InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            base.InitializeValidationRules();
        }

        protected override void InitializeValidationRules()
        {
            base.InitializeValidationRules();
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.gjelderHeleTiltaket);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.delAvTiltaket, null, "/soeknadGjelder/gjelderHeleTiltaket = 'false'");
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.delsoeknadsnummer);
            AddValidationRule(ValidationRuleEnum.numerisk, FieldNameEnum.delsoeknadsnummer);
        }

        public ValidationResult Validate(SoeknadGjelder soeknadGjelder, Kodeliste ansvarForByggesaken)
        {
            base.Validate(soeknadGjelder, ansvarForByggesaken);
            if (!Helpers.ObjectIsNullOrEmpty(soeknadGjelder))
            {
                ValidateEntityFields(soeknadGjelder);
            }
            return _validationResult;
        }

        private void ValidateEntityFields(SoeknadGjelder soeknadGjelder)
        {
            if (!soeknadGjelder.GjelderHeleTiltaket.HasValue)
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.gjelderHeleTiltaket);
            }
            else if (!soeknadGjelder.GjelderHeleTiltaket.GetValueOrDefault())
            {
                if (string.IsNullOrEmpty(soeknadGjelder.DelAvTiltaket))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.delAvTiltaket, null, "/soeknadGjelder/gjelderHeleTiltaket = 'false'");
            }

            if (string.IsNullOrEmpty(soeknadGjelder.Delsoeknadsnummer))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.delsoeknadsnummer, new[] { _formName });
            else if (!int.TryParse(soeknadGjelder.Delsoeknadsnummer, out var delsoeknadsnummer))
                AddMessageFromRule(ValidationRuleEnum.numerisk, FieldNameEnum.delsoeknadsnummer, new[] { _formName });
            else
                Delsoeknadsnummer = delsoeknadsnummer;
        }
    }
}
