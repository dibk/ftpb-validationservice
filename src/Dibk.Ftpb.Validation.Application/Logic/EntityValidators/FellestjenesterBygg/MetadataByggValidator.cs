﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class MetadataByggValidator : EntityValidatorBase, IMetadataByggValidator
    {
        private readonly IKodelisteValidator _foretrukketspraakValidator;
        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }


        public MetadataByggValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator foretrukketspraakValidator, int? nodeId = null) : base(
            entityValidatorTree, nodeId)
        {
            _foretrukketspraakValidator = foretrukketspraakValidator;

            InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_foretrukketspraakValidator.ValidationResult.ValidationRules);
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.fraSluttbrukersystem);

        }

        public ValidationResult Validate(Metadata metadata)
        {

            if (Helpers.ObjectIsNullOrEmpty(metadata))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(metadata.FraSluttbrukersystem))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.fraSluttbrukersystem);

                var foretrukketspraakValidationResult = _foretrukketspraakValidator.Validate(metadata?.ForetrukketSpraak);
                AccumulateValidationMessages(foretrukketspraakValidationResult.ValidationMessages);
            }


            return _validationResult;
        }
    }

}