﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg;

public class SoeknadTypeUtenAnsvarsrettValidator : TypeValidator
{
    private const string Precondition = "/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll'";

    public SoeknadTypeUtenAnsvarsrettValidator(
        IList<EntityValidatorNode> entityValidatorTree,
        int nodeId,
        ICodeListService codeListService
    ) : base(
        entityValidatorTree,
        nodeId,
        codeListService,
        FtbKodeListeEnum.tiltakstyperUtenAnsvarsrett,
        Precondition,
        $"/soeknadGjelder/type/kode{{0}}/kodeverdi & ({Precondition})",
        $"/soeknadGjelder/type & ({Precondition})"
    )
    {
    }
}