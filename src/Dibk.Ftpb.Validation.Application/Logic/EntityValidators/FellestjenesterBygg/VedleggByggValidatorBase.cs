﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg;

public abstract class VedleggByggValidatorBase<T> : VedleggValidatorBase where T : class
{
    protected AttachmentInfo[] AttachmentsInApplication;

    protected VedleggByggValidatorBase(IList<EntityValidatorNode> entityValidatorTree)
        : base(entityValidatorTree)
    {
    }

    protected override void InitializeValidationRules()
    {
        AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, null, "/utfallBesvarelse/utfallSvar{0}/vedlegg", XPathFilterForAttachments);
    }

    public virtual ValidationResult Validate(AttachmentInfo[] attachmentsInApplication, T form)
    {
        AttachmentsInApplication = attachmentsInApplication;

        ResetValidationMessages();

        RunValidationsRelatedToUtfallBesvarelse(form);

        RunValidationsRelatedToAnsvarForByggesaken(form);

        return ValidationResult;
    }

    protected abstract UtfallSvar[] GetUtfallBesvarelse(T form);

    private void RunValidationsRelatedToUtfallBesvarelse(T form)
    {
        var utfallBesvarelse = GetUtfallBesvarelse(form);

        var index = GetArrayIndex(utfallBesvarelse);
        for (int i = 0; i < index; i++)
        {
            var utfallSvar = Helpers.ObjectIsNullOrEmpty(utfallBesvarelse) ? null : utfallBesvarelse[i];
            if (utfallSvar == null)
                continue;

            var vedlegg = utfallSvar.Vedleggsliste?.Where(u => !Helpers.ObjectIsNullOrEmpty(u)).ToArray();
            if (!Helpers.ObjectIsNullOrEmpty(vedlegg))
            {
                if (!Helpers.ObjectIsNullOrEmpty(AttachmentsInApplication))
                    continue;
                
                AddMessageFromRule(ValidationRuleEnum.vedlegg, XPathFilterForAttachments,
                    new[] { utfallSvar.Utfalltype?.Kodebeskrivelse }, "/utfallBesvarelse/utfallSvar{0}/vedlegg");
            }
        }
    }

    protected abstract void RunValidationsRelatedToAnsvarForByggesaken(T form);

    protected void ValidateGjennomfoeringsplanIsAttached(string precondition)
    {
        var gjennomfoeringsplanNames = new[] { "Gjennomføringsplan", "Gjennomfoeringsplan" };

        var gjennomfoeringsplanIsAttached = AttachmentsInApplication?.FirstOrDefault(x =>
                gjennomfoeringsplanNames.Contains(x.AttachmentTypeName)) != null;

        if (!gjennomfoeringsplanIsAttached)
            AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.Gjennomføringsplan}",
                preConditionField: precondition);
    }
}