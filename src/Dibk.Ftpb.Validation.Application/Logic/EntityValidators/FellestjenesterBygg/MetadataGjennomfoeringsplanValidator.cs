using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class MetadataGjennomfoeringsplanValidator : EntityValidatorBase
    {
        public ValidationResult ValidationResult => _validationResult;

        public MetadataGjennomfoeringsplanValidator(IList<EntityValidatorNode> entityValidatorTree)
            : base(entityValidatorTree)
        {
        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.fraSluttbrukersystem);
        }

        public ValidationResult Validate(MetadataGjennomfoeringsplan metadataValidationEntity)
        {
            ResetValidationMessages();
            if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                ValidateEntityFields(metadataValidationEntity);
            }

            return _validationResult;
        }

        private void ValidateEntityFields(MetadataAltinn2Base metadataValidationEntity)
        {
            if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity.FraSluttbrukersystem))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.fraSluttbrukersystem);
        }
    }
}
