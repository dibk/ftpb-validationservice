using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{

    public class SubmitterValidator : EntityValidatorBase, ISubmitterValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        public List<ValidationMessage> _validationMessages;
        protected const string XPathFilterForSubmitter = "/avsender";
        private readonly IDecryptionFactory _decryptionFactory;
        private bool _useTiltakshaver;

        public SubmitterValidator(IList<EntityValidatorNode> entityValidatorTree, IDecryptionFactory decryptionFactory, bool useTiltakshaver = true) : base(entityValidatorTree)
        {
            _decryptionFactory = decryptionFactory;
            if (useTiltakshaver)
                AddValidationRuleSubmitter(ValidationRuleEnum.gyldig, AktoerEnum.tiltakshaver, "/tiltakshaver/partstype/kodeverdi");
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRuleSubmitter(ValidationRuleEnum.gyldig, AktoerEnum.ansvarligSoeker, "/ansvarligSoeker/partstype/kodeverdi");
        }


        public ValidationResult Validate(Aktoer tiltakshaver = null, Aktoer ansvarligsoeker = null, string loggedInUser = null, List<ValidationMessage> validationMessages = null)
        {
            base.ResetValidationMessages();
            _validationMessages = validationMessages;

            var tiltakshaverIdentifier = GetAktoerIdentifier(tiltakshaver);
            var ansvarligsoekerIdentifier = GetAktoerIdentifier(ansvarligsoeker);

            if (Helpers.ObjectIsNullOrEmpty(ansvarligsoeker))
            {
                if(loggedInUser != null && !loggedInUser.Equals(tiltakshaverIdentifier))
                AddMessageFromRule(ValidationRuleEnum.gyldig, $"{XPathFilterForSubmitter}/{AktoerEnum.tiltakshaver}", null, "/tiltakshaver/partstype/kodeverdi");
            } 
            else if (loggedInUser != null && !loggedInUser.Equals(ansvarligsoekerIdentifier))
            {
                AddMessageFromRule(ValidationRuleEnum.gyldig, $"{XPathFilterForSubmitter}/{AktoerEnum.ansvarligSoeker}", null, "/ansvarligSoeker/partstype/kodeverdi");
            }

            return ValidationResult;
        }

        protected string GetAktoerIdentifier(Aktoer aktoer)
        {
            var identifier = string.Empty;
            if (Helpers.ObjectIsNullOrEmpty(aktoer)
                || Helpers.ObjectIsNullOrEmpty(aktoer.Partstype)
                )
            {
                identifier = null;
            }   
            else if (aktoer.Partstype.Kodeverdi != null && aktoer.Partstype.Kodeverdi.ToUpper().Equals("PRIVATPERSON"))
            {
                identifier = _decryptionFactory.GetDecryptor().DecryptText(aktoer.Foedselsnummer);
            }
            else
            {
                identifier = aktoer.Organisasjonsnummer;
            }
            return identifier;
        }
    }    
}
