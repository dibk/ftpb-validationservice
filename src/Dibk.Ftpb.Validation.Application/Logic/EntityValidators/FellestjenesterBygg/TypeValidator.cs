﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class TypeValidator : KodelisteValidatorV2
    {
        private readonly string _preconditionKodeverdi;
        private readonly string _preconditionKodebeskrivelse;
        private readonly string _preconditionValidert;

        public TypeValidator(
            IList<EntityValidatorNode> entityValidatorTree,
            int nodeId,
            ICodeListService codeListService,
            FtbKodeListeEnum codeListName = FtbKodeListeEnum.tiltaktype,
            string preconditionKodeverdi = null,
            string preconditionKodebeskrivelse = null,
            string preconditionValidert = null,
            bool allowedNull = false
        ) : base(
            entityValidatorTree,
            nodeId,
            codeListName,
            RegistryType.Byggesoknad,
            codeListService,
            allowedNull,
            useBaseConditionalRules: false
        )
        {
            _preconditionKodeverdi = preconditionKodeverdi;
            _preconditionKodebeskrivelse = preconditionKodebeskrivelse;
            _preconditionValidert = preconditionValidert;

            AddCustomValidationRules(allowedNull);
        }

        private void AddCustomValidationRules(bool allowedNull)
        {
            if (!allowedNull)
            {
                AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.kodeverdi, preCondition: _preconditionKodeverdi);
            }
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.kodeverdi, preCondition: _preconditionKodeverdi);
            AddValidationRule(ValidationRuleEnum.validert, FieldNameEnum.kodeverdi, preCondition: _preconditionValidert);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.kodebeskrivelse, preCondition: _preconditionKodebeskrivelse);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.kodebeskrivelse, preCondition: _preconditionKodebeskrivelse);
        }

        protected override void InitializeValidationRules()
        {
            // do not use base rules
        }

        public override ValidationResult Validate(Kodeliste kodeEntry, string messageParameter = null)
        {
            ResetValidationMessages();

            if (Helpers.ObjectIsNullOrEmpty(kodeEntry) && AllowedNull)
                return ValidationResult;

            if (Helpers.ObjectIsNullOrEmpty(kodeEntry) && !AllowedNull)
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.kodeverdi, preConditionField: _preconditionKodeverdi);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(kodeEntry.Kodeverdi) && !AllowedNull)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.kodeverdi, preConditionField: _preconditionKodeverdi);
                }
                else
                {
                    var isCodeValid = _codeListService.IsCodeValueValid(CodeListName, kodeEntry.Kodeverdi, RegistryType);
                    if (!isCodeValid.HasValue)
                    {
                        AddMessageFromRule(ValidationRuleEnum.validert, FieldNameEnum.kodeverdi, new[] { kodeEntry.Kodeverdi }, _preconditionValidert);
                    }
                    else
                    {
                        if (!isCodeValid.GetValueOrDefault())
                        {
                            AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.kodeverdi, new[] { kodeEntry.Kodeverdi }, _preconditionKodeverdi);
                        }
                        else
                        {
                            if (AllowedKodeverdi != null && !AllowedKodeverdi.Contains(kodeEntry.Kodeverdi, StringComparer.CurrentCultureIgnoreCase))
                            {
                                AddMessageFromRule(ValidationRuleEnum.tillatt, FieldNameEnum.kodeverdi, new[] { kodeEntry.Kodeverdi }, _preconditionKodeverdi);
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(kodeEntry.Kodebeskrivelse))
                    {
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.kodebeskrivelse, preConditionField: _preconditionKodebeskrivelse);
                    }
                    else
                    {
                        if (isCodeValid.GetValueOrDefault())
                        {
                            var isCodelistLabelValid = _codeListService.IsCodeLabelValid(CodeListName, kodeEntry.Kodeverdi, kodeEntry.Kodebeskrivelse, RegistryType);
                            if (!isCodelistLabelValid.GetValueOrDefault())
                            {
                                AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.kodebeskrivelse, new[] { kodeEntry.Kodebeskrivelse }, _preconditionKodebeskrivelse);
                            }
                        }
                    }
                }
            }

            return ValidationResult;
        }
    }
}
