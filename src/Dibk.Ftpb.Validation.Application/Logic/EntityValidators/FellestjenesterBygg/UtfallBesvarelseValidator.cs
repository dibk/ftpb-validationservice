using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class UtfallBesvarelseValidator : EntityValidatorBase, IUtfallBesvarelseValidator
    {
        private readonly IKodelisteValidator _utfalltypeValidator;
        private readonly IUtloestFraSjekkpunktValidator _utloestFraSjekkpunktValidator;
        private readonly IKodelisteValidator _temaValidator;
        private readonly IVedleggslisteValidator _vedleggslisteValidator;
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        public string _entityXPath { get => base.EntityXPath; }

        public UtfallBesvarelseValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator utfalltypeValidator, IUtloestFraSjekkpunktValidator utloestFraSjekkpunktValidator, IKodelisteValidator temaValidator,
            IVedleggslisteValidator vedleggslisteValidator) : base(entityValidatorTree)
        {
            _utfalltypeValidator = utfalltypeValidator;
            _utloestFraSjekkpunktValidator = utloestFraSjekkpunktValidator;
            _temaValidator = temaValidator;
            _vedleggslisteValidator = vedleggslisteValidator;
            InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_utfalltypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_utloestFraSjekkpunktValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_temaValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_vedleggslisteValidator.ValidationResult.ValidationRules);
            InitializeBesvartStatusRules();
        }

        protected virtual void InitializeBesvartStatusRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.erUtfallBesvart, preCondition: $"{_entityXPath}/erUtfallBesvaresSenere = false & /soeknadGjelder/delsoeknadsnummer > 1");
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.erUtfallBesvaresSenere);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommentar, null, "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere");
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.tittel, preCondition: "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere");            
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommentarEllerVedlegg, preCondition: "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere = false & /utfallBesvarelse/utfallSvar{0}/erUtfallBesvart = false");
        }

        public virtual ValidationResult Validate(UtfallSvar utfallSvar, int? delsoeknadsnummer)
        {
            ResetValidationMessages();

            if (Helpers.ObjectIsNullOrEmpty(utfallSvar))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                var utfallTypeValidatorResult = _utfalltypeValidator.Validate(utfallSvar.Utfalltype);
                AccumulateValidationMessages(utfallTypeValidatorResult.ValidationMessages);

                var utloestFraSjekkpunktValidatorResult = _utloestFraSjekkpunktValidator.Validate(utfallSvar.UtloestFraSjekkpunkt);
                AccumulateValidationMessages(utloestFraSjekkpunktValidatorResult.ValidationMessages);

                var temaValidatorResult = _temaValidator.Validate(utfallSvar.Tema);
                AccumulateValidationMessages(temaValidatorResult.ValidationMessages);

                var index = GetArrayIndex(utfallSvar.Vedleggsliste);
                for (int i = 0; i < index; i++)
                {
                    var vedlegg = Helpers.ObjectIsNullOrEmpty(utfallSvar.Vedleggsliste) ? null : utfallSvar.Vedleggsliste[i];

                    var vedleggslisteValidatorResult = _vedleggslisteValidator.Validate(vedlegg);
                    AccumulateValidationMessages(vedleggslisteValidatorResult.ValidationMessages, i);
                }

                ValidateDataRelations(utfallSvar, delsoeknadsnummer);
            }

            return _validationResult;
        }

        private void ValidateDataRelations(UtfallSvar utfallSvar, int? delsoeknadsnummer)
        {
            // Construct XPaths for validation messages
            var utfallTypeKoebeskrivelseXpath = _utfalltypeValidator._entityXPath + "/" + FieldNameEnum.kodebeskrivelse;

            // Check if there are any validation messages for utfallTypeKoebeskrivelseXpath
            if (IsAnyValidationMessagesWithXpath(utfallTypeKoebeskrivelseXpath))
                return;

            var utfallKodebeskrivelse = utfallSvar.Utfalltype?.Kodebeskrivelse;

            if (string.IsNullOrEmpty(utfallSvar.Tittel))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.tittel, new[] { utfallKodebeskrivelse }, "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere");

            ValidateBesvartStatus(utfallSvar, delsoeknadsnummer, utfallKodebeskrivelse, utfallTypeKoebeskrivelseXpath);
        }

        protected virtual void ValidateBesvartStatus(UtfallSvar utfallSvar, int? delsoeknadsnummer, string utfallKodebeskrivelse, string utfallTypeKoebeskrivelseXpath)
        {
            if (!utfallSvar.ErUtfallBesvaresSenere.HasValue)
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.erUtfallBesvaresSenere, new[] { utfallKodebeskrivelse }, utfallTypeKoebeskrivelseXpath);
            else
            {
                if (utfallSvar.ErUtfallBesvaresSenere.Value && string.IsNullOrEmpty(utfallSvar.Kommentar))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommentar, new[] { utfallKodebeskrivelse }, "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere");
                else if (!utfallSvar.ErUtfallBesvaresSenere.Value)
                {
                    if (!utfallSvar.ErUtfallBesvart.HasValue)
                    {
                        if (delsoeknadsnummer > 1)
                            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.erUtfallBesvart, new[] { utfallKodebeskrivelse }, $"{_entityXPath}/erUtfallBesvaresSenere = false & /soeknadGjelder/delsoeknadsnummer > 1");
                    }
                    else if (!utfallSvar.ErUtfallBesvart.Value)
                    {
                        var vedlegg = utfallSvar.Vedleggsliste?.Where(u => !Helpers.ObjectIsNullOrEmpty(u)).ToArray();

                        var isAnyAttachment = !Helpers.ObjectIsNullOrEmpty(vedlegg);

                        if (!isAnyAttachment && string.IsNullOrEmpty(utfallSvar.Kommentar))
                            AddMessageFromRule(
                                ValidationRuleEnum.utfylt,
                                FieldNameEnum.kommentarEllerVedlegg,
                                new[] { utfallKodebeskrivelse },
                                "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere = false & /utfallBesvarelse/utfallSvar{0}/erUtfallBesvart = false");
                    }
                }
            }
        }       
    }
}

