﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class VarmefordelingValidator : KodelisteValidatorV2
    {
        public VarmefordelingValidator
            (IList<EntityValidatorNode> entityValidatorTree, int nodeId, ICodeListService codeListService, bool allowedNull = false, string[] allowedKodeverdi = null)
            : base(entityValidatorTree, nodeId, FtbKodeListeEnum.Varmefordeling, RegistryType.Byggesoknad, codeListService, allowedNull, allowedKodeverdi)
        {
        }
    }
}