﻿using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class VedleggIGValidator : VedleggByggValidatorBase<Igangsettingstillatelse>
    {
        public VedleggIGValidator(IList<EntityValidatorNode> entityValidatorTree) : base(entityValidatorTree)
        {
        }

        protected override void InitializeValidationRules()
        {
            base.InitializeValidationRules();

            AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Gjennomføringsplan,
                "/ansvarForByggesaken/kodeverdi = 'medAnsvar'",
                $"{XPathFilterForAttachments}");
            AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.SamtykkeArbeidstilsynet,
                "/generelleVilkaar/beroererArbeidsplasser = true & soeknadGjelder/delsoeknadsnummer = 1");
        }

        public override ValidationResult Validate(AttachmentInfo[] attachmentsInApplication, Igangsettingstillatelse form)
        {
            base.Validate(attachmentsInApplication, form);

            if (form.GenerelleVilkaar.BeroererArbeidsplasser.HasValue && form.SoeknadGjelder.Delsoeknadsnummer == "1")
            {
                if (form.GenerelleVilkaar.BeroererArbeidsplasser.GetValueOrDefault())
                    if (attachmentsInApplication == null || attachmentsInApplication.All(x => x.AttachmentTypeName != AttachmentEnum.SamtykkeArbeidstilsynet.ToString()))
                        AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.SamtykkeArbeidstilsynet}");
            }

            return _validationResult;
        }

        protected override UtfallSvar[] GetUtfallBesvarelse(Igangsettingstillatelse form)
        {
            return form?.UtfallBesvarelse;
        }

        protected override void RunValidationsRelatedToAnsvarForByggesaken(Igangsettingstillatelse form)
        {
            var ansvarForByggesaken = form?.AnsvarForByggesaken?.Kodeverdi;

            if (ansvarForByggesaken == "medAnsvar")
            {
                ValidateGjennomfoeringsplanIsAttached("/ansvarForByggesaken/kodeverdi = 'medAnsvar'");
            }
        }
    }
}
