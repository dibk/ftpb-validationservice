using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class AnsvarsomraadeGjennomfoeringsplanValidator : EntityValidatorBase
    {
        private readonly IKodelisteValidator _funksjonValidator;
        private readonly IKodelisteValidator _tilttaksklasseValidator;
        private readonly IAktoerValidator _foretaksValidator;
        private readonly IKodelisteValidator _ansvarsomraadeStatusValidator;
        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public AnsvarsomraadeGjennomfoeringsplanValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator funksjonValidator, IKodelisteValidator tilttaksklasseValidator, IAktoerValidator foretaksValidator, AnsvarsomraadeStatusValidator ansvarsomraadeStatusValidator) : base(entityValidatorTree)
        {
            _funksjonValidator = funksjonValidator;
            _tilttaksklasseValidator = tilttaksklasseValidator;
            _foretaksValidator = foretaksValidator;
            _ansvarsomraadeStatusValidator = ansvarsomraadeStatusValidator;
            InitializeConditionalValidationRules();
        }
        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_funksjonValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_tilttaksklasseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_foretaksValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_ansvarsomraadeStatusValidator.ValidationResult.ValidationRules);
        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.ansvarsomraade);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.erklaeringAnsvar);
        }

        public ValidationResult Validate(AnsvarsomraadeGjennomfoeringsplan ansvarsomraade = null)
        {
            ResetValidationMessages();

            ValidateEntityFields(ansvarsomraade);
            if (!Helpers.ObjectIsNullOrEmpty(ansvarsomraade))
            {
                if (_funksjonValidator != null)
                {
                    var funksjonResult = _funksjonValidator.Validate(ansvarsomraade?.Funksjon);
                    UpdateValidationResultWithSubValidations(funksjonResult);
                }
                if (_tilttaksklasseValidator != null && !string.IsNullOrEmpty(ansvarsomraade.Ansvarsomraade))
                {
                    var tiltaksklasseResult = _tilttaksklasseValidator.Validate(ansvarsomraade?.Tiltaksklasse, ansvarsomraade.Ansvarsomraade);
                    UpdateValidationResultWithSubValidations(tiltaksklasseResult);
                }
                if (_foretaksValidator != null)
                {
                    var foretaksValidatorResult = _foretaksValidator.Validate(ansvarsomraade?.Foretak, ansvarsomraade.Ansvarsomraade);
                    UpdateValidationResultWithSubValidations(foretaksValidatorResult);
                    //Reset messages for foretaksValidator
                    _foretaksValidator.ValidationResult.ValidationMessages.Clear();
                }
                if (_ansvarsomraadeStatusValidator != null && !string.IsNullOrEmpty(ansvarsomraade.Ansvarsomraade))
                {
                    var ansvarsomraadeStatusResult = _ansvarsomraadeStatusValidator.Validate(ansvarsomraade?.AnsvarsomraadeStatus, ansvarsomraade.Ansvarsomraade);
                    UpdateValidationResultWithSubValidations(ansvarsomraadeStatusResult);
                }
            }
            return ValidationResult;
        }

        public void ValidateEntityFields(AnsvarsomraadeGjennomfoeringsplan ansvarsomraade = null)
        {
            if (Helpers.ObjectIsNullOrEmpty(ansvarsomraade))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            } else if (Helpers.ObjectIsNullOrEmpty(ansvarsomraade?.Ansvarsomraade))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.ansvarsomraade);
            }else
            {
                if (!SamsvarsKontrollerklaeringIsPresent(ansvarsomraade))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.erklaeringAnsvar, new string[] { ansvarsomraade.Ansvarsomraade });
                }
            }
        }

        private bool SamsvarsKontrollerklaeringIsPresent(AnsvarsomraadeGjennomfoeringsplan ansvarsomraade)
        {
            return ansvarsomraade.SamsvarKontrollForeliggerVedFerdigattest.HasValue ||
                ansvarsomraade.SamsvarKontrollForeliggerVedIgangsettingstillatelse.HasValue ||
                ansvarsomraade.SamsvarKontrollForeliggerVedMidlertidigBrukstillatelse.HasValue ||
                ansvarsomraade.SamsvarKontrollForeliggerVedRammetillatelse.HasValue ||
                ansvarsomraade.SamsvarKontrollPlanlagtVedFerdigattest.GetValueOrDefault() ||
                ansvarsomraade.SamsvarKontrollPlanlagtVedIgangsettingstillatelse.GetValueOrDefault() ||
                ansvarsomraade.SamsvarKontrollPlanlagtVedMidlertidigBrukstillatelse.GetValueOrDefault() ||
                ansvarsomraade.SamsvarKontrollPlanlagtVedRammetillatelse.GetValueOrDefault();
        }
    }
}
