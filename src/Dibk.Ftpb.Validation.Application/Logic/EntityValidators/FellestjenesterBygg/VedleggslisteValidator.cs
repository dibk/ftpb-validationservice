﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg
{
    public class VedleggslisteValidator : EntityValidatorBase, IVedleggslisteValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }

        private readonly IKodelisteValidator _vedleggsTypeValidator;

        public VedleggslisteValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator vedleggsTypeValidator, int? nodeId = null) : base(entityValidatorTree, nodeId)
        {
            _vedleggsTypeValidator = vedleggsTypeValidator;
            InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_vedleggsTypeValidator.ValidationResult.ValidationRules);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.filnavn, null, _vedleggsTypeValidator._entityXPath);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.versjonsdato, null, $"{_vedleggsTypeValidator._entityXPath}/{FieldNameEnum.kodeverdi}");
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.versjonsnummer, null, $"{_vedleggsTypeValidator._entityXPath}/{FieldNameEnum.kodeverdi}");

        }

        protected override void InitializeValidationRules()
        {

        }

        public ValidationResult Validate(Vedlegg vedlegg)
        {
            ResetValidationMessages();
            if (!Helpers.ObjectIsNullOrEmpty(vedlegg))
            {
                if (!string.IsNullOrEmpty(vedlegg.Filnavn) )
                {
                    var vedleggsTypeValidationResult = _vedleggsTypeValidator.Validate(vedlegg.Vedleggstype,vedlegg.Filnavn);
                    AccumulateValidationMessages(vedleggsTypeValidationResult.ValidationMessages);

                }

                if (string.IsNullOrEmpty(vedlegg.Filnavn) && !Helpers.ObjectIsNullOrEmpty(vedlegg.Vedleggstype))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.filnavn, new[] { vedlegg.Vedleggstype?.Kodebeskrivelse }, _vedleggsTypeValidator._entityXPath);


                ValidateDataRelations(vedlegg); 
            }

            return _validationResult;
        }

        private void ValidateDataRelations(Vedlegg vedlegg)
        {

            string[] VedleggsArray ={
                "Situasjonsplan",
                "TegningNyPlan",
                "TegningNyttSnitt",
                "TegningNyFasade",
                "Kart",
                "Avkjoerselsplan",
                "RedegjoerelseAndreNaturMiljoeforhold",
                "RedegjoerelseEstetikk",
                "RedegjoerelseUnntakTEK",
                "RedegjoerelseForurensetGrunn",
                "RedegjoerelseGrunnforhold",
                "RedegjoerelseSkredOgFlom",
                "UnderlagUtnytting",
                "Utomhusplan",
                "ByggesaksBIM"
            };

            if (!IsAnyValidationMessagesWithXpath(_vedleggsTypeValidator._entityXPath) && !Helpers.ObjectIsNullOrEmpty(vedlegg.Vedleggstype))
            {
                var vedleggsKodeverdi = vedlegg.Vedleggstype?.Kodeverdi;
                if (!string.IsNullOrEmpty(vedleggsKodeverdi) && VedleggsArray.Contains(vedleggsKodeverdi))
                {
                    if (!vedlegg.Versjonsdato.HasValue)
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.versjonsdato, new[] { vedleggsKodeverdi }, $"{_vedleggsTypeValidator._entityXPath}/{FieldNameEnum.kodeverdi}");
                    
                    if (string.IsNullOrEmpty(vedlegg.Versjonsnummer))
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.versjonsnummer, new[] { vedleggsKodeverdi }, $"{_vedleggsTypeValidator._entityXPath}/{FieldNameEnum.kodeverdi}");
                }
            }
        }
    }
}
