﻿using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg;

public class VedleggMBValidator : VedleggByggValidatorBase<Midlertidigbrukstillatelse>    
{   
    public VedleggMBValidator(IList<EntityValidatorNode> entityValidatorTree) : base(entityValidatorTree)
    {
    }

    protected override void InitializeValidationRules()
    {
        base.InitializeValidationRules();

        AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.ErklaeringSelvbygger, "/ansvarForByggesaken/kodeverdi = 'selvbygger' && /soeknadGjelder/gjelderHeleTiltaket = 'true'");
        AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Gjennomføringsplan,
            "/ansvarForByggesaken/kodeverdi = 'medAnsvar' || 'utenAnsvarMedKontroll'",
            $"{XPathFilterForAttachments}");
    }

    protected override UtfallSvar[] GetUtfallBesvarelse(Midlertidigbrukstillatelse form)
    {
        return form?.UtfallBesvarelse;
    }

    protected override void RunValidationsRelatedToAnsvarForByggesaken(Midlertidigbrukstillatelse form)   
    {
        var ansvarForByggesaken = form?.AnsvarForByggesaken?.Kodeverdi;
        var gjelderHeleTiltaket = form?.SoeknadGjelder.GjelderHeleTiltaket;

        if (ansvarForByggesaken == "selvbygger" && gjelderHeleTiltaket.HasValue && gjelderHeleTiltaket.Value)
        {
            ValidateAttachmentsForSelvbygger(ansvarForByggesaken);
        }
        else if (ansvarForByggesaken is "medAnsvar" or "utenAnsvarMedKontroll")
        {
            ValidateGjennomfoeringsplanIsAttached("/ansvarForByggesaken/kodeverdi = 'medAnsvar' || 'utenAnsvarMedKontroll'");
        }
    }

    private void ValidateAttachmentsForSelvbygger(string ansvarForByggesaken)
    {
        var egenerklaeringExists = AttachmentsInApplication?.FirstOrDefault(x => 
            x.AttachmentTypeName == AttachmentEnum.ErklaeringSelvbygger.ToString()) != null;

        if (!egenerklaeringExists)
            AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.ErklaeringSelvbygger}", new[] { ansvarForByggesaken }, 
                preConditionField: "/ansvarForByggesaken/kodeverdi = 'selvbygger' && /soeknadGjelder/gjelderHeleTiltaket = 'true'");
    }

    
}