﻿
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class LeveringsstedValidator : EntityValidatorBase, ILeveringsstedValidator
    {
        ValidationResult ILeveringsstedValidator.ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public LeveringsstedValidator(IList<EntityValidatorNode> entityValidatorTree)
            : base(entityValidatorTree)
        {
        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.mottaksNavn);
        }

        public ValidationResult Validate(Leveringssted leveringssted, string fraksjonKodebeskrivelse = null)
        {
            base.ResetValidationMessages();
            ValidateEntityFields(leveringssted, new []{fraksjonKodebeskrivelse});

            return _validationResult;
        }

        public void ValidateEntityFields(Leveringssted leveringssted, string[] messageParameters = null)
        {
            if (Helpers.ObjectIsNullOrEmpty(leveringssted))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, this.EntityXPath, messageParameters);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(leveringssted.Mottaksnavn))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.mottaksNavn, messageParameters);
            }
        }
    }
}
