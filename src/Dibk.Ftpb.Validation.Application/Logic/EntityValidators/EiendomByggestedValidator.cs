﻿using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.GeneralValidations;
using MatrikkelWSClient.Services;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class EiendomByggestedValidator : EntityValidatorBase, IEiendomByggestedValidator
    {
        private IEiendomsAdresseValidator _eiendomsAdresseValidator;
        private readonly IEiendomsidentifikasjonValidator _eiendomsidentifikasjonValidator;
        private readonly IMatrikkelProvider _matrikkelProvider;

        private readonly bool _constructUniqueRuleIds = false;

        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public EiendomByggestedValidator(
            IList<EntityValidatorNode> entityValidatorTree,
            IEiendomsAdresseValidator eiendomsAdresseValidator,
            IEiendomsidentifikasjonValidator eiendomsidentifikasjonValidator,
            IMatrikkelProvider matrikkelProvider,
            bool constructUniqueRuleIds = false)
            : base(entityValidatorTree)
        {
            _constructUniqueRuleIds = constructUniqueRuleIds;

            _eiendomsAdresseValidator = eiendomsAdresseValidator;
            _eiendomsidentifikasjonValidator = eiendomsidentifikasjonValidator;
            InitializeConditionalValidationRules();
            _matrikkelProvider = matrikkelProvider;
        }

        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_eiendomsAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_eiendomsidentifikasjonValidator.ValidationResult.ValidationRules);

            if (_constructUniqueRuleIds)
            {
                AddValidationRule(ValidationRuleEnum.registrert, FieldNameEnum.adresse);
                AddValidationRule(ValidationRuleEnum.validert, FieldNameEnum.adresse);
            }
            else
            {
                AddValidationRule(ValidationRuleEnum.registrert, null, "/eiendomByggested/eiendom{0}/adresse");
                AddValidationRule(ValidationRuleEnum.validert, null, "/eiendomByggested/eiendom{0}/adresse");
            }
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.numerisk, FieldNameEnum.bygningsnummer);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.bygningsnummer);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.bolignummer);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommunenavn);

            AddValidationRule(ValidationRuleEnum.registrert, FieldNameEnum.bygningsnummer);
            AddValidationRule(ValidationRuleEnum.validert, FieldNameEnum.bygningsnummer);

        }

        public ValidationResult Validate(Eiendom eiendom)
        {

            base.ResetValidationMessages();


            if (!Helpers.ObjectIsNullOrEmpty(eiendom))
            {

                var eiendomsidentifikasjonValidationResult = _eiendomsidentifikasjonValidator.Validate(eiendom.Eiendomsidentifikasjon);
                _validationResult.ValidationMessages.AddRange(eiendomsidentifikasjonValidationResult.ValidationMessages);

                var eiendomsAdresseValidationResult = _eiendomsAdresseValidator.Validate(eiendom.Adresse);
                _validationResult.ValidationMessages.AddRange(eiendomsAdresseValidationResult.ValidationMessages);

                ValidateDataRelations(eiendom);
            }
            ValidateEntityFields(eiendom);

            return _validationResult;
        }

        private void ValidateDataRelations(Eiendom eiendom)
        {
            if (!AnyValidationMessagesConteinXpath($"{_eiendomsidentifikasjonValidator._entityXPath}"))
            {
                var gatenavnXpath = $"{_eiendomsAdresseValidator._entityXPath}/{FieldNameEnum.gatenavn}";
                var husnrXpath = $"{_eiendomsAdresseValidator._entityXPath}/{FieldNameEnum.husnr}";

                if (!IsAnyValidationMessagesWithXpath(gatenavnXpath) && !IsAnyValidationMessagesWithXpath(husnrXpath) && !Helpers.ObjectIsNullOrEmpty(eiendom?.Adresse))
                {
                    bool? adresseValidation = null;

                    if (!Helpers.ObjectIsNullOrEmpty(eiendom.Adresse))
                    {
                       // bruksenhetsnummer = null, vi validerer ikke fordi Oslo bruker en annen registreringsform
                       adresseValidation = _matrikkelProvider.ByggEksistererPåAdresseIMatrikkelen(
                           eiendom.Eiendomsidentifikasjon.Kommunenummer,
                           eiendom.Adresse?.Gatenavn, eiendom.Adresse?.Husnr, eiendom.Adresse?.Bokstav, null,
                           eiendom.Eiendomsidentifikasjon.Gaardsnummer, eiendom.Eiendomsidentifikasjon.Bruksnummer,
                           eiendom.Eiendomsidentifikasjon.Festenummer, eiendom.Eiendomsidentifikasjon.Seksjonsnummer);
                    }

                    if (!adresseValidation.HasValue)
                    {
                        if (_constructUniqueRuleIds)
                            AddMessageFromRule(ValidationRuleEnum.validert, FieldNameEnum.adresse);
                        else
                            AddMessageFromRule(ValidationRuleEnum.validert, _eiendomsAdresseValidator._entityXPath);
                    }
                    else if (!adresseValidation.GetValueOrDefault())
                    {
                        if (_constructUniqueRuleIds)
                            AddMessageFromRule(ValidationRuleEnum.registrert, FieldNameEnum.adresse);
                        else
                            AddMessageFromRule(ValidationRuleEnum.registrert, _eiendomsAdresseValidator._entityXPath); 
                    }
                }

            }
        }

        public void ValidateEntityFields(Eiendom eiendom)
        {
            if (Helpers.ObjectIsNullOrEmpty(eiendom))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (!string.IsNullOrEmpty(eiendom?.Bygningsnummer))
                {
                    long bygningsnrLong = 0;
                    if (!long.TryParse(eiendom.Bygningsnummer, out bygningsnrLong))
                    {
                        AddMessageFromRule(ValidationRuleEnum.numerisk, FieldNameEnum.bygningsnummer, new[] { eiendom.Bygningsnummer });
                    }
                    else
                    {
                        if (bygningsnrLong <= 0)
                        {
                            AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.bygningsnummer, new[] { bygningsnrLong.ToString("N") });
                        }
                        else
                        {
                            if (!AnyValidationMessagesConteinXpath($"{_eiendomsidentifikasjonValidator._entityXPath}"))
                            {
                                var bygningsnummerValidation = _matrikkelProvider.ByggMedBygningsnummerEksistererIMatrikkelen(
                            eiendom.Eiendomsidentifikasjon.Kommunenummer,
                            eiendom.Bygningsnummer,
                            eiendom.Eiendomsidentifikasjon.Gaardsnummer, eiendom.Eiendomsidentifikasjon.Bruksnummer,
                            eiendom.Eiendomsidentifikasjon.Festenummer,
                            eiendom.Eiendomsidentifikasjon.Seksjonsnummer);
                                if (!bygningsnummerValidation.HasValue)
                                {
                                    AddMessageFromRule(ValidationRuleEnum.validert, FieldNameEnum.bygningsnummer);
                                }
                                else if (!bygningsnummerValidation.GetValueOrDefault())
                                {
                                    AddMessageFromRule(ValidationRuleEnum.registrert, FieldNameEnum.bygningsnummer, new[] { eiendom.Bygningsnummer });
                                }

                            }
                        }
                    }
                }

                var bolignummerStandardValidation = NorskStandardValidator.BruksenhetsnummerStandardValidator(eiendom?.Bolignummer);
                if (bolignummerStandardValidation == GeneralValidationResultEnum.Invalid)
                    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.bolignummer);

                if (string.IsNullOrEmpty(eiendom?.Kommunenavn))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommunenavn);

            }
        }
    }
}
