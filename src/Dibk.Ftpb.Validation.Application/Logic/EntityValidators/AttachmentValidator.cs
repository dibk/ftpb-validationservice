﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Altinn;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Microsoft.Extensions.Caching.Memory;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class AttachmentValidator : EntityValidatorBase
    {
        public string _entityXPath { get => base.EntityXPath; }

        public AttachmentValidator(IList<EntityValidatorNode> entityValidatorTree) : base(entityValidatorTree)
        {

        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.gyldig);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.filnavn);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.filnavn);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.attachmentTypeName);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.attachmentTypeName);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.allowedFileTypes);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.maxFileSize);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.maxFileSize);
        }

        public ValidationResult validationResult { get => _validationResult; set => throw new NotImplementedException(); }



        public ValidationResult Validate(AttachmentInfo attachmentInfo, List<AttachmentRule> altinAttachmentRules = null)
        {
            base.ResetValidationMessages();

            if (!Helpers.ObjectIsNullOrEmpty(attachmentInfo) && (altinAttachmentRules == null || !altinAttachmentRules.Any()))
            {
                AddMessageFromRule(ValidationRuleEnum.gyldig);
            }
            else
            {
                //Check attachment type
                if (string.IsNullOrEmpty(attachmentInfo.AttachmentTypeName))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.attachmentTypeName, new[] { attachmentInfo.AttachmentTypeName, attachmentInfo.Filename });
                }
                else
                {
                    var attachmentRule = altinAttachmentRules?.FirstOrDefault(
                        r => r.AttachmentTypeName == attachmentInfo.AttachmentTypeName);
                    if (attachmentRule == null)
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.attachmentTypeName, new[] { attachmentInfo.AttachmentTypeName });
                    }
                    else
                    {
                        //check file name
                        var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(attachmentInfo.Filename);
                        if (string.IsNullOrWhiteSpace(fileNameWithoutExtension))
                        {
                            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.filnavn);
                        }
                        else
                        {
                            //file name with invalid characters 
                            var invalidCharacters = Path.GetInvalidFileNameChars();
                            var fileNameArray = fileNameWithoutExtension.ToCharArray();
                            var invalidsCharacters = fileNameArray?.Where(st => invalidCharacters.Contains(st)).ToArray();

                            if (invalidsCharacters.Any())
                                AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.filnavn, new[] { String.Join(",", invalidsCharacters.Select(chr => chr.ToString())) });

                            //Check file extension
                            var allowedExtensions = attachmentRule.AllowedFileTypes.Split(",");
                            var extensionWithPoint = Path.GetExtension(attachmentInfo.Filename);
                            var attachmentInfoExtension = extensionWithPoint?.Replace(".", "");

                            if (!allowedExtensions.Any(r => r.Equals(attachmentInfoExtension, StringComparison.InvariantCultureIgnoreCase)))
                                AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.allowedFileTypes, new[] { attachmentInfo.AttachmentTypeName, attachmentInfoExtension });
                        }

                        //Check file size
                        if (attachmentInfo.FileSize == 0)
                        {
                            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.maxFileSize, new []{ attachmentInfo.AttachmentTypeName});
                        }
                        else
                        {
                            if (attachmentRule.MaxFileSize < attachmentInfo.FileSize)
                                AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.maxFileSize, new[] { attachmentInfo.Filename, attachmentRule.MaxFileSize.ToString(), attachmentInfo.AttachmentTypeName });
                        }
                    }
                }
            }
            return validationResult;
        }
    }
}