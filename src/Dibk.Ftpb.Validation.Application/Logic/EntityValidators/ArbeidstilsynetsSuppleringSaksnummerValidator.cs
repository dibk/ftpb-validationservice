﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;
using Dibk.Ftpb.Common.Datamodels.Parts;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class ArbeidstilsynetsSuppleringSaksnummerValidator : EntityValidatorBase, ISuppleringSaksnummerValidator
    {
        private readonly object _codeListName;
        private readonly RegistryType _registryType;
       // protected ICodeListService _codeListService;

        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }

        public ArbeidstilsynetsSuppleringSaksnummerValidator(IList<EntityValidatorNode> entityValidatorTree)
            : base(entityValidatorTree)
        {
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt, null, null, "/mangelbesvarelse");
            AddValidationRule(ValidationRuleEnum.utfylt, null, null, "/ettersending");
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.saksaar);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.saksaar, null, "/ettersending");
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.saksaar, null, "/mangelbesvarelse");
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.sakssekvensnummer, null, "/ettersending");
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.sakssekvensnummer, null, "/mangelbesvarelse");
        }


        public ValidationResult Validate(Saksnummer saksnummer, bool mangelbesvarelse)
        {
            base.ResetValidationMessages();

            if (Helpers.ObjectIsNullOrEmpty(saksnummer))
            {
                if (mangelbesvarelse)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, "/mangelbesvarelse");
                }
                else if (!mangelbesvarelse)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, "/ettersending");
                }
            }
            else
            {
                if (!saksnummer.Saksaar.HasValue && saksnummer.Sakssekvensnummer.HasValue && mangelbesvarelse)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.saksaar, null, "/mangelbesvarelse");
                }
                else if (!saksnummer.Saksaar.HasValue && saksnummer.Sakssekvensnummer.HasValue && !mangelbesvarelse)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.saksaar, null, "/ettersending");
                }
                else
                {
                    var thisYear = DateTime.Now.Year;
                    if (saksnummer.Saksaar < thisYear - 30 || saksnummer.Saksaar > thisYear)
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.saksaar, new string[] { saksnummer.Saksaar.ToString() });
                    }
                }

                if (saksnummer.Saksaar.HasValue && !saksnummer.Sakssekvensnummer.HasValue && mangelbesvarelse)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.sakssekvensnummer, null, "/mangelbesvarelse");
                }
                else if (saksnummer.Saksaar.HasValue && !saksnummer.Sakssekvensnummer.HasValue && !mangelbesvarelse)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.sakssekvensnummer, null, "/ettersending");
                }
            }
            return ValidationResult;
        }
    }
}