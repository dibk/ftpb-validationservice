using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Dibk.Ftpb.Validation.Application.Enums;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class BeskrivelseAvTiltakValidator : EntityValidatorBase, IBeskrivelseAvTiltakValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        protected readonly IFormaaltypeValidator _formaaltypeValidator;
        protected readonly IKodelisteValidator _tiltakstypeValidator;
        public List<string> _Tiltakstypes;
        public string[] Tiltakstypes { get => _Tiltakstypes?.ToArray(); }

        public BeskrivelseAvTiltakValidator(IList<EntityValidatorNode> entityValidatorTree, IFormaaltypeValidator formaaltypeValidator, IKodelisteValidator tiltakstypeValidator)
            : base(entityValidatorTree)
        {
            _formaaltypeValidator = formaaltypeValidator;
            _tiltakstypeValidator = tiltakstypeValidator;
            _Tiltakstypes = new List<string>();
            InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_formaaltypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_tiltakstypeValidator.ValidationResult.ValidationRules);

        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
        }

        public ValidationResult Validate(BeskrivelseAvTiltak beskrivelseAvTiltakValidationEntity = null)
        {
            ValidateEntityFields(beskrivelseAvTiltakValidationEntity);

            if (!Helpers.ObjectIsNullOrEmpty(beskrivelseAvTiltakValidationEntity))
            {
                var tiltakstypes = beskrivelseAvTiltakValidationEntity?.Tiltakstype;
                var index = GetArrayIndex(tiltakstypes);

                for (int i = 0; i < index; i++)
                {
                    var tiltakstype = Helpers.ObjectIsNullOrEmpty(tiltakstypes) ? null : tiltakstypes[i];

                    var tiltakstypeValidationResult = _tiltakstypeValidator.Validate(tiltakstype);
                    UpdateValidationResultWithSubValidations(tiltakstypeValidationResult, i);

                    if (tiltakstypes != null && !IsAnyValidationMessagesWithXpath($"{_tiltakstypeValidator._entityXPath}/{FieldNameEnum.kodeverdi}", i))
                    {
                        _Tiltakstypes.Add(tiltakstypes[i].Kodeverdi);
                    }
                }

                if (!Helpers.ObjectIsNullOrEmpty(tiltakstypes))
                {
                    var formaalTypeValidationResult = _formaaltypeValidator.Validate(beskrivelseAvTiltakValidationEntity?.Formaaltype);
                    UpdateValidationResultWithSubValidations(formaalTypeValidationResult);
                }
            }

            return ValidationResult;
        }

        public void ValidateEntityFields(BeskrivelseAvTiltak beskrivelseAvTiltak)
        {
            if (Helpers.ObjectIsNullOrEmpty(beskrivelseAvTiltak))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
        }
    }
}
