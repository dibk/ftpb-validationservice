﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Validation.Application.Logic.GeneralValidations;
using Microsoft.FeatureManagement;
using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class FakturamottakerValidator : EntityValidatorBase, IFakturamottakerValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        private readonly IEnkelAdresseValidator _enkelAdresseValidator;

        // ATIL Feature - SLETTES ETTER 20250217 - START
        private readonly bool atil20250217Enabled = false;

        private const int MAX_LENGTH = 70;

        public FakturamottakerValidator(IList<EntityValidatorNode> entityValidatorTree, IEnkelAdresseValidator enkelAdresseValidator, IFeatureManager featureManager)
            : base(entityValidatorTree)
        {
            _enkelAdresseValidator = enkelAdresseValidator;

            // ATIL Feature - SLETTES ETTER 20250217 - START
            atil20250217Enabled = AsyncHelper.RunSync(async () => await featureManager.IsEnabledAsync("ATIL20250217"));
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.ehf_eller_papir);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.organisasjonsnummer);
            AddValidationRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.organisasjonsnummer);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.organisasjonsnummer);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.navn);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.navn);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.bestillerReferanse);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.fakturareferanser);
        }

        public ValidationResult Validate(ArbeidstilsynetsSamtykkeV2 validationForm, Fakturamottaker fakturamottaker = null)
        {
            if (Helpers.ObjectIsNullOrEmpty(fakturamottaker))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                ValidateEntityFields(validationForm, fakturamottaker);

                if (RuleIsValid(ValidationRuleEnum.ehf_eller_papir))
                {
                    if (fakturamottaker.FakturaPapir.GetValueOrDefault())
                    {
                        var adresseValidationResult = _enkelAdresseValidator.Validate(fakturamottaker.Adresse);
                        UpdateValidationResultWithSubValidations(adresseValidationResult);
                    }
                }
            }
            return ValidationResult;
        }

        private void ValidateFakturamottaker(Fakturamottaker fakturamottaker)
        {
            var organisasjonsnummerValidation = NorskStandardValidator.ValidateOrgnummer(fakturamottaker.Organisasjonsnummer);
            switch (organisasjonsnummerValidation)
            {
                case OrganisasjonsnummerValidation.Empty:
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.organisasjonsnummer);
                    break;

                case OrganisasjonsnummerValidation.InvalidDigitsControl:
                    AddMessageFromRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.organisasjonsnummer, new string[] { fakturamottaker.Organisasjonsnummer });
                    break;

                case OrganisasjonsnummerValidation.Invalid:
                    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.organisasjonsnummer, new string[] { fakturamottaker.Organisasjonsnummer });
                    break;
            }
        }

        public void ValidateEntityFields(ArbeidstilsynetsSamtykkeV2 validationForm, Fakturamottaker fakturamottaker = null)
        {
            var booleans = new[]
            {
                fakturamottaker.EhfFaktura.GetValueOrDefault(),
                fakturamottaker.FakturaPapir.GetValueOrDefault(),
            };
            var trueCount = booleans.Count(c => c);
            if (trueCount != 1)
                AddMessageFromRule(ValidationRuleEnum.ehf_eller_papir);
            else
            {
                // ATIL Feature - RYDDES ETTER 20250217 - START                
                if (atil20250217Enabled) // Slettes
                {
                    if (validationForm.Tiltakshaver?.Partstype?.Kodeverdi != "Privatperson" || validationForm.AnsvarligSoeker?.Partstype?.Kodeverdi != "Privatperson")
                        ValidateFakturamottaker(fakturamottaker);
                }
                else if (fakturamottaker.EhfFaktura.GetValueOrDefault()) //Slettes
                {
                        ValidateFakturamottaker(fakturamottaker); // Slettes
                }
                

                if (string.IsNullOrEmpty(fakturamottaker.Navn))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.navn);
                }
                else
                {
                    var navn = fakturamottaker.Navn.ToCharArray();
                    if (navn.Length > MAX_LENGTH)
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.navn);
                    }
                }

                if (string.IsNullOrEmpty(fakturamottaker.Bestillerreferanse))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.bestillerReferanse);

                if (string.IsNullOrEmpty(fakturamottaker.Fakturareferanser))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.fakturareferanser);
            }
        }
    }
}