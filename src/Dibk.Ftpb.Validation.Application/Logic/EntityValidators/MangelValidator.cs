﻿
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class MangelValidator : EntityValidatorBase, IMangelValidator
    {
        private readonly IKodelisteValidator _mangeltekstValidator;

        ValidationResult IMangelValidator.ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public MangelValidator(IList<EntityValidatorNode> entityValidatorTree, int? nodeId, IKodelisteValidator mangeltekstValidator)
            : base(entityValidatorTree, nodeId)
        {
            _mangeltekstValidator = mangeltekstValidator;
        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);            
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.mangelId);
        }

        public ValidationResult Validate(Mangel mangel)
        {
            base.ResetValidationMessages();
            ValidateEntityFields(mangel);
            var vedleggstypeValidationResult = _mangeltekstValidator.Validate(mangel?.Mangeltekst);
            UpdateValidationResultWithSubValidations(vedleggstypeValidationResult);
            return _validationResult;
        }

        public void ValidateEntityFields(Mangel mangel)
        {
            if (Helpers.ObjectIsNullOrEmpty(mangel))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(mangel.MangelId))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.mangelId);
            }
        }
    }
}
