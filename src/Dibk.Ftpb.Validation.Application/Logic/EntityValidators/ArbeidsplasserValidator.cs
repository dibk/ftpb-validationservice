﻿using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Enums;
using System.Linq;
using Dibk.Ftpb.Validation.Application.Models.Web;
using System.IO;
using System;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class ArbeidsplasserValidator : EntityValidatorBase
    {
        private List<string> _attachmentList;
        private const int MAX_LENGTH = 2000;

        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public ArbeidsplasserValidator(IList<EntityValidatorNode> entityValidatorTree)
            : base(entityValidatorTree)
        { }

        public ValidationResult Validate(Arbeidsplasser arbeidsplasser, string[] attachments = null)
        {
            _attachmentList = attachments != null ? attachments.ToList() : null;
            ValidateEntityFields(arbeidsplasser);

            return _validationResult;
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.framtidige_eller_eksisterende_utfylt);
            AddValidationRule(ValidationRuleEnum.faste_eller_midlertidige_utfylt);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.antallAnsatte);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.antallVirksomheter);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.beskrivelse);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.beskrivelse);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.veiledning);
        }

        public void ValidateEntityFields(Arbeidsplasser arbeidsplasserValEntity)
        {
            if (Helpers.ObjectIsNullOrEmpty(arbeidsplasserValEntity))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (!arbeidsplasserValEntity.Eksisterende.GetValueOrDefault(false) && !arbeidsplasserValEntity.Framtidige.GetValueOrDefault(false))
                {
                    AddMessageFromRule(ValidationRuleEnum.framtidige_eller_eksisterende_utfylt);
                }

                if (!arbeidsplasserValEntity.Faste.GetValueOrDefault(false) && !arbeidsplasserValEntity.Midlertidige.GetValueOrDefault(false))
                {
                    AddMessageFromRule(ValidationRuleEnum.faste_eller_midlertidige_utfylt);
                }

                int antallAnsatte;
                int.TryParse(arbeidsplasserValEntity.AntallAnsatte, out antallAnsatte);
                if (antallAnsatte <= 0)
                {
                    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.antallAnsatte);
                }

                if (arbeidsplasserValEntity.UtleieBygg.GetValueOrDefault(false))
                {
                    int antallVirksomheter;
                    int.TryParse(arbeidsplasserValEntity.AntallVirksomheter, out antallVirksomheter);
                    if (antallVirksomheter <= 0)
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.antallVirksomheter);
                    }
                }

                if (string.IsNullOrEmpty(arbeidsplasserValEntity.Beskrivelse))
                {
                    if (_attachmentList == null || _attachmentList.All(i => !i.Equals("BeskrivelseTypeArbeidProsess")))
                    {
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.beskrivelse);
                    }
                }
                else
                {
                    
                    var beskrivelse = arbeidsplasserValEntity.Beskrivelse.ToCharArray();
                    if (beskrivelse.Length > MAX_LENGTH)
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.beskrivelse);
                    }
                }

                if (arbeidsplasserValEntity.Veiledning == null)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.veiledning);
                }

            }
        }
    }
}