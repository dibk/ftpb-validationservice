﻿
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class SuppleringVedleggValidator : EntityValidatorBase, ISuppleringVedleggValidator
    {
        private readonly IKodelisteValidator _vedleggstypevalidator;

        ValidationResult ISuppleringVedleggValidator.ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public SuppleringVedleggValidator(IList<EntityValidatorNode> entityValidatorTree, int? nodeId, IKodelisteValidator vedleggstypevalidator)
            : base(entityValidatorTree, nodeId)
        {
            _vedleggstypevalidator = vedleggstypevalidator;
        }
        protected override void InitializeValidationRules()
        {
            //AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.versjonsnummer);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.versjonsdato);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.filnavn);
        }

        public ValidationResult Validate(Vedlegg vedlegg)
        {
            base.ResetValidationMessages();
            ValidateEntityFields(vedlegg);
            if (!Helpers.ObjectIsNullOrEmpty(vedlegg))
            {
                var vedleggstypeValidationResult = _vedleggstypevalidator.Validate(vedlegg.Vedleggstype);
                UpdateValidationResultWithSubValidations(vedleggstypeValidationResult);
            }
            
            return _validationResult;
        }

        public void ValidateEntityFields(Vedlegg vedlegg)
        {
            if (Helpers.ObjectIsNullOrEmpty(vedlegg))
            {
                //AddMessageFromRule(ValidationRuleEnum.utfylt, this.EntityXPath);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(vedlegg.Versjonsnummer))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.versjonsnummer);
                if (Helpers.ObjectIsNullOrEmpty(vedlegg.Versjonsdato))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.versjonsdato);
                if (Helpers.ObjectIsNullOrEmpty(vedlegg.Filnavn))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.filnavn);
            }
        }
    }
}
