﻿using System;
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class DisponeringsmaateDelsumValidator : EntityValidatorBase, IDisponeringsmaateDelsumValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        private readonly IKodelisteValidator _disponeringsmaateValidator;

        public DisponeringsmaateDelsumValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator disponeringsmaateValidator)
            : base(entityValidatorTree)
        {
            _disponeringsmaateValidator = disponeringsmaateValidator;
            InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_disponeringsmaateValidator.ValidationResult.ValidationRules);

        }
        protected override void InitializeValidationRules()
        {
            var disponerigsmaateXpath = Helpers.GetEntityXpath(EntityXPath, null, null, EntityValidatorEnum.DisponeringsmaateValidator);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.delsum, null);
        }

        public ValidationResult Validate(AvfalldisponeringsmaateDelsum disponeringsmaateDelsum = null)
        {
            base.ResetValidationMessages();

            ValidateEntityFields(disponeringsmaateDelsum);

            if (!IsAnyValidationMessagesWithXpath(this.EntityXPath))
            {
                var disponeringsmaateValidationResult = _disponeringsmaateValidator.Validate(disponeringsmaateDelsum?.Disponeringsmaate);
                UpdateValidationResultWithSubValidations(disponeringsmaateValidationResult);

                if (!IsAnyValidationMessagesWithXpath(_disponeringsmaateValidator._entityXPath) && !IsAnyValidationMessagesWithXpath(Helpers.GetEntityXpath(_disponeringsmaateValidator._entityXPath, null, FieldNameEnum.kodeverdi)))
                    ValidateDataRelations(disponeringsmaateDelsum);
            }

            return _validationResult;
        }

        public ValidationResult ValidateDataRelations(AvfalldisponeringsmaateDelsum disponeringsmaateDelSum)
        {
            if (string.IsNullOrEmpty(disponeringsmaateDelSum?.Delsum))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.delsum, new[] { disponeringsmaateDelSum?.Disponeringsmaate?.Kodebeskrivelse });


            return _validationResult;
        }
        public ValidationResult ValidateEntityFields(AvfalldisponeringsmaateDelsum disponeringsmaateDelSum)
        {
            return _validationResult;
        }
    }
}
