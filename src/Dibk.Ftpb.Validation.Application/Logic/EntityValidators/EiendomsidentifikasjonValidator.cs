﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using MatrikkelWSClient.Services;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class EiendomsidentifikasjonValidator : EntityValidatorBase, IEiendomsidentifikasjonValidator
    {
        private readonly ICodeListService _codeListService;
        private readonly IMatrikkelProvider _matrikkelService;
        public string _entityXPath { get => base.EntityXPath; }
        ValidationResult IEiendomsidentifikasjonValidator.ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public EiendomsidentifikasjonValidator(IList<EntityValidatorNode> entityValidatorTree, ICodeListService codeListService, IMatrikkelProvider matrikkelService)
            : base(entityValidatorTree)
        {
            _codeListService = codeListService;
            _matrikkelService = matrikkelService;
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommunenummer);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.kommunenummer);
            AddValidationRule(ValidationRuleEnum.status, FieldNameEnum.kommunenummer);
            AddValidationRule(ValidationRuleEnum.validert, FieldNameEnum.kommunenummer);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.gaardsnummer);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.gaardsnummer);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.bruksnummer);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.bruksnummer);

            //Matrikkel
            AddValidationRule(ValidationRuleEnum.validert);
            AddValidationRule(ValidationRuleEnum.registrert);
        }

        public ValidationResult Validate(Eiendomsidentifikasjon matrikkel)
        {
            base.ResetValidationMessages();

            if (Helpers.ObjectIsNullOrEmpty(matrikkel))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                var kommunenummer = matrikkel?.Kommunenummer;

                var kommunenummerMatrikkelInfo = _codeListService.GetCodelistTagValue(SosiKodelisterEnum.kommunenummer, kommunenummer, RegistryType.SosiKodelister);

                switch (kommunenummerMatrikkelInfo?.Status?.ToLower())
                {
                    case "gyldig":
                        break;

                    case "utfylt":
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommunenummer);
                        break;

                    case "ugyldig":
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.kommunenummer, new[] { kommunenummer });
                        break;

                    case "ikkevalidert":
                        AddMessageFromRule(ValidationRuleEnum.validert, FieldNameEnum.kommunenummer, new[] { kommunenummer });
                        break;

                    default:
                        AddMessageFromRule(ValidationRuleEnum.status, FieldNameEnum.kommunenummer, new[] { kommunenummer, kommunenummerMatrikkelInfo?.Status });
                        break;
                }

                if (Helpers.ObjectIsNullOrEmpty(matrikkel.Gaardsnummer))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.gaardsnummer);
                }
                else
                {
                    if (int.TryParse(matrikkel.Gaardsnummer, out var gaardsnummer))
                    {
                        if (gaardsnummer < 0)
                            AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.gaardsnummer, new[] { matrikkel.Gaardsnummer });
                    }
                }

                if (Helpers.ObjectIsNullOrEmpty(matrikkel.Bruksnummer))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.bruksnummer);
                }
                else
                {
                    if (int.TryParse(matrikkel.Bruksnummer, out var bruksnummer))
                    {
                        if (bruksnummer < 0)
                            AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.bruksnummer, new[] { matrikkel.Bruksnummer });
                    }
                }

                if (kommunenummerMatrikkelInfo?.Status?.ToLower() == "gyldig" || kommunenummerMatrikkelInfo?.Status?.ToLower() == "ikkevalidert")
                {
                    var gaardsnummerXpath = $"{_entityXPath}/{FieldNameEnum.gaardsnummer}";
                    var bruksnummerXpath = $"{_entityXPath}/{FieldNameEnum.bruksnummer}";

                    if (!IsAnyValidationMessagesWithXpath(gaardsnummerXpath) && !IsAnyValidationMessagesWithXpath(bruksnummerXpath))
                    {
                        var matrikkelValid = _matrikkelService.ByggEksistererIMatrikkelen(matrikkel.Kommunenummer, matrikkel.Gaardsnummer, matrikkel.Bruksnummer, matrikkel.Festenummer, matrikkel.Seksjonsnummer);
                        if (!matrikkelValid.HasValue)
                        {
                            AddMessageFromRule(ValidationRuleEnum.validert, _entityXPath, new[] { matrikkel.Kommunenummer, matrikkel.Gaardsnummer, matrikkel.Bruksnummer, matrikkel.Festenummer, matrikkel.Seksjonsnummer });
                        }
                        else if (!matrikkelValid.GetValueOrDefault())
                        {
                            AddMessageFromRule(ValidationRuleEnum.registrert, _entityXPath, new[] { matrikkel.Kommunenummer, matrikkel.Gaardsnummer, matrikkel.Bruksnummer, matrikkel.Festenummer, matrikkel.Seksjonsnummer });
                        }
                    }
                }
            }
            return _validationResult;
        }
    }
}