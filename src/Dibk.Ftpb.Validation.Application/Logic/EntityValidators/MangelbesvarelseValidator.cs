﻿
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class MangelbesvarelseValidator : EntityValidatorBase, IMangelbesvarelseValidator
    {
        private readonly ISuppleringVedleggValidator _suppleringVedleggValidator;
        private readonly IKodelisteValidator _temaValidator;
        private readonly IMangelValidator _mangelValidator;

        ValidationResult IMangelbesvarelseValidator.ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public MangelbesvarelseValidator(IList<EntityValidatorNode> entityValidatorTree, ISuppleringVedleggValidator suppleringVedleggValidator,
                                        IKodelisteValidator temaValidator, IMangelValidator mangelValidator)
            : base(entityValidatorTree)
        {
            _suppleringVedleggValidator = suppleringVedleggValidator;
            _temaValidator = temaValidator;
            _mangelValidator = mangelValidator;
        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.tittel);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.beskrivelseFraKommunen);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.erMangelBesvaresSenere);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommentar);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.vedleggEllerUnderskjemaEllerKommentar);
        }

        public ValidationResult Validate(Mangelbesvarelse mangelbesvarelse)
        {
            base.ResetValidationMessages();
            ValidateEntityFields(mangelbesvarelse);

            if (!Helpers.ObjectIsNullOrEmpty(mangelbesvarelse.Vedlegg))
            {
                var vedleggArray = mangelbesvarelse.Vedlegg;
                var index = GetArrayIndex(vedleggArray);
                for (int i = 0; i < index; i++)
                {
                    var vedlegg = Helpers.ObjectIsNullOrEmpty(vedleggArray) ? null : vedleggArray[i];
                    var suppleringVedleggValidationResult = _suppleringVedleggValidator.Validate(vedlegg);
                    UpdateValidationResultWithSubValidations(suppleringVedleggValidationResult, i);
                }
            }

            var temaValidationResult = _temaValidator.Validate(mangelbesvarelse.Tema);
            UpdateValidationResultWithSubValidations(temaValidationResult);

            var mangelValidationResult = _mangelValidator.Validate(mangelbesvarelse.Mangel);
            UpdateValidationResultWithSubValidations(mangelValidationResult);

            return _validationResult;
        }

        public void ValidateEntityFields(Mangelbesvarelse mangelbesvarelse)
        {
            if (Helpers.ObjectIsNullOrEmpty(mangelbesvarelse))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, this.EntityXPath);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(mangelbesvarelse.Tittel))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.tittel);
                
                if (Helpers.ObjectIsNullOrEmpty(mangelbesvarelse.BeskrivelseFraKommunen))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.beskrivelseFraKommunen);

                if (!mangelbesvarelse.ErMangelBesvaresSenere.HasValue)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.erMangelBesvaresSenere);
                }
                else
                {
                    if (mangelbesvarelse.ErMangelBesvaresSenere.GetValueOrDefault())
                    {
                        if (string.IsNullOrEmpty(mangelbesvarelse.Kommentar))
                        {
                            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommentar);
                        }
                    }
                    else
                    {
                        if (Helpers.ObjectIsNullOrEmpty(mangelbesvarelse.Vedlegg) && Helpers.ObjectIsNullOrEmpty(mangelbesvarelse.Underskjema) && string.IsNullOrEmpty(mangelbesvarelse.Kommentar))
                        {
                            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.vedleggEllerUnderskjemaEllerKommentar);
                        }
                    }
                }
            }
        }
    }
}
