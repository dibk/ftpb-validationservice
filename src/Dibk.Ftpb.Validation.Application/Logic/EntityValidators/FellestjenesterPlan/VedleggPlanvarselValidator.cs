﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Common.Datamodels;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterPlan
{
    public class VedleggPlanvarselValidator : VedleggValidatorBase
    {
        public VedleggPlanvarselValidator(IList<EntityValidatorNode> entityValidatorTree) : base(entityValidatorTree)
        {
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.PlanomraadePdf, "/beroerteParter/beroertpart/partstype/kodeverdi");
            AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Planomraade, "/beroerteParter/beroertpart/erHoeringsmyndighet");
            AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.KartDetaljert);
            AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Planinitiativ, "/planforslag/hjemmesidePlanprogram");
            AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Planprogram, "/planforslag/kravKonsekvensUtredning");
            AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Annet);
            AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.ReferatOppstartsmoete);

        }

        public override ValidationResult Validate(ValidationInput validationInput, object formdata, string loggedInUser, List<ValidationMessage> validationMessages = null)
        {
            base.ResetValidationMessages();
            _validationMessages = validationMessages;

            var planvarselV2 = formdata as PlanvarselV2;

            if (!Helpers.ObjectIsNullOrEmpty(planvarselV2?.BeroerteParter))
            {
                var attachmentOfTypePlanomraadePdf = validationInput.Attachments?.Any(attachment => attachment.AttachmentTypeName.Equals(AttachmentEnum.PlanomraadePdf.ToString()));
                var attachmentOfTypePlanomraade = validationInput.Attachments?.Any(attachment => attachment.AttachmentTypeName.Equals(AttachmentEnum.Planomraade.ToString()));
                var attachmentOfTypePlanomraadeSosi = validationInput.Attachments?.Any(attachment => attachment.AttachmentTypeName.Equals(AttachmentEnum.PlanomraadeSosi.ToString()));

                if (attachmentOfTypePlanomraadePdf == false && planvarselV2?.BeroerteParter?.Any(br => br.ErHoeringsmyndighet == false) == true)
                {
                    AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.PlanomraadePdf}", null, "/beroerteParter/beroertpart/partstype/kodeverdi");
                }

                if (attachmentOfTypePlanomraade == false && attachmentOfTypePlanomraadeSosi == false && planvarselV2?.BeroerteParter?.Any(br => br.ErHoeringsmyndighet == true) == true)
                {
                    AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.Planomraade}", null, "/beroerteParter/beroertpart/erHoeringsmyndighet");
                }
            }

            var attachmentOfTypeKartDetaljert = validationInput.Attachments?.Any(attachment => attachment.AttachmentTypeName.Equals(AttachmentEnum.KartDetaljert.ToString()));
            if (!attachmentOfTypeKartDetaljert.GetValueOrDefault())
                AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.KartDetaljert}");

            var attachmentOfTypePlaninitiativ = validationInput.Attachments?.Any(attachment => attachment.AttachmentTypeName.Equals(AttachmentEnum.Planinitiativ.ToString()));

            if (!attachmentOfTypePlaninitiativ.GetValueOrDefault())
            {
                if (string.IsNullOrEmpty(planvarselV2?.Planforslag?.HjemmesidePlanprogram) && string.IsNullOrEmpty(planvarselV2?.Planforslag?.HjemmesidePlanforslag))
                    AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.Planinitiativ}", null, "/planforslag/hjemmesidePlanprogram");
            }

            var attachmentOfTypePlanprogram = validationInput.Attachments?.Any(attachment => attachment.AttachmentTypeName.Equals(AttachmentEnum.Planprogram.ToString()));
            var kravKonsekvensutredning = planvarselV2?.Planforslag?.KravKonsekvensutredning.GetValueOrDefault();

            if (!attachmentOfTypePlanprogram.GetValueOrDefault() && kravKonsekvensutredning.GetValueOrDefault())
                AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.Planprogram}", null, "/planforslag/kravKonsekvensUtredning");

            var attachementAnnet = validationInput.Attachments?.Any(attachment => attachment.AttachmentTypeName.Equals(AttachmentEnum.Annet.ToString()));
            if (!attachementAnnet.GetValueOrDefault())
                AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.Annet}");

            var attachmentOfTypeReferatOppstartsmoete = validationInput.Attachments?.Any(attachment => attachment.AttachmentTypeName.Equals(AttachmentEnum.ReferatOppstartsmoete.ToString()));
            if (!attachmentOfTypeReferatOppstartsmoete.GetValueOrDefault())
                AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.ReferatOppstartsmoete}");

            return ValidationResult;
        }
    }
}
