﻿using System.Collections.Generic;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using MatrikkelWSClient.Services;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterPlan
{
    public class EiendomsidentifikasjonPlanValidator : EntityValidatorBase, IEiendomsidentifikasjonValidator
    {
        private readonly ICodeListService _codeListService;
        public string _entityXPath { get => base.EntityXPath; }
        ValidationResult IEiendomsidentifikasjonValidator.ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public EiendomsidentifikasjonPlanValidator(IList<EntityValidatorNode> entityValidatorTree, ICodeListService codeListService)
            : base(entityValidatorTree)
        {
            _codeListService = codeListService;
        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommunenummer);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.kommunenummer);
            AddValidationRule(ValidationRuleEnum.status, FieldNameEnum.kommunenummer);
            AddValidationRule(ValidationRuleEnum.validert, FieldNameEnum.kommunenummer);
        }

        public ValidationResult Validate(Eiendomsidentifikasjon matrikkel)
        {
            base.ResetValidationMessages();

            if (!Helpers.ObjectIsNullOrEmpty(matrikkel))
            {
                var kommunenummer = matrikkel?.Kommunenummer;

                var codelistTagValue = _codeListService.GetCodelistTagValue(SosiKodelisterEnum.kommunenummer, kommunenummer, RegistryType.SosiKodelister);

                switch (codelistTagValue?.Status?.ToLower())
                {
                    case "gyldig":
                        break;
                    case "utfylt":
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.kommunenummer);
                        break;
                    case "ugyldig":
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.kommunenummer, new[] { kommunenummer });
                        break;
                    case "ikkevalidert":
                        AddMessageFromRule(ValidationRuleEnum.validert, FieldNameEnum.kommunenummer, new[] { kommunenummer });
                        break;
                    default:
                        AddMessageFromRule(ValidationRuleEnum.status, FieldNameEnum.kommunenummer, new[] { kommunenummer, codelistTagValue?.Status });
                        break;
                }
            }
            return _validationResult;
        }
    }
}
