﻿using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.GeneralValidations;
using MatrikkelWSClient.Services;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterPlan
{
    public class EiendomByggestedPlanValidator : EntityValidatorBase, IEiendomByggestedValidator
    {
        private readonly IEiendomsidentifikasjonValidator _eiendomsidentifikasjonValidator;

        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public EiendomByggestedPlanValidator(IList<EntityValidatorNode> entityValidatorTree, IEiendomsidentifikasjonValidator eiendomsidentifikasjonValidator)
            : base(entityValidatorTree)
        {
            _eiendomsidentifikasjonValidator = eiendomsidentifikasjonValidator;
            InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_eiendomsidentifikasjonValidator.ValidationResult.ValidationRules);
        }
        protected override void InitializeValidationRules()
        {
        }

        public ValidationResult Validate(Eiendom eiendom)
        {

            base.ResetValidationMessages();

            ValidateEntityFields(eiendom);

            if (!Helpers.ObjectIsNullOrEmpty(eiendom))
            {

                var eiendomsidentifikasjonValidationResult = _eiendomsidentifikasjonValidator.Validate(eiendom.Eiendomsidentifikasjon);
                _validationResult.ValidationMessages.AddRange(eiendomsidentifikasjonValidationResult.ValidationMessages);

                ValidateDataRelations(eiendom);
            }

            return _validationResult;
        }

        private void ValidateDataRelations(Eiendom eiendom)
        {
        }

        public void ValidateEntityFields(Eiendom eiendom)
        {
        }
    }
}
