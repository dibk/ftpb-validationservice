﻿using System;
using System.Collections.Generic;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.GeneralValidations;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterPlan;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterPlan
{
    public class BeroerteParterValidator : EntityValidatorBase, IBeroertPartValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }

        private readonly IList<EntityValidatorNode> _entityValidatorTree;
        private readonly IKodelisteValidator _partstypeValidator;
        private readonly IEnkelAdresseValidator _enkelAdresseValidator;
        private readonly IDecryptionFactory _decryptionFactory;

        public BeroerteParterValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator partstypeValidator, IEnkelAdresseValidator enkelAdresseValidator, IDecryptionFactory decryptionFactory) : base(entityValidatorTree)
        {
            _entityValidatorTree = entityValidatorTree;
            _partstypeValidator = partstypeValidator;
            _enkelAdresseValidator = enkelAdresseValidator;
            _decryptionFactory = decryptionFactory;

            AddValidateDataRelationsRules();
        }

        private void AddValidateDataRelationsRules()
        {
            AccumulateValidationRules(_partstypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_enkelAdresseValidator.ValidationResult.ValidationRules);
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.navn);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.erHoeringsmyndighet);

            //fødselnummer
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.foedselsnummer);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.foedselsnummer);
            AddValidationRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.foedselsnummer);
            AddValidationRule(ValidationRuleEnum.dekryptering, FieldNameEnum.foedselsnummer);
            AddValidationRule(ValidationRuleEnum.kryptert, FieldNameEnum.foedselsnummer);

            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.organisasjonsnummer);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.organisasjonsnummer);
            AddValidationRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.organisasjonsnummer);

        }

        public ValidationResult Validate(Beroertpart beroertPart = null)
        {
            base.ResetValidationMessages();

            ValidateEntityFields(beroertPart);

            if (!Helpers.ObjectIsNullOrEmpty(beroertPart))
            {
                var partstypeResult = _partstypeValidator.Validate(beroertPart.Partstype);
                _validationResult.ValidationMessages.AddRange(partstypeResult.ValidationMessages);

                var codeValueHaveError = IsAnyValidationMessagesWithXpath($"{_partstypeValidator._entityXPath}/{FieldNameEnum.kodeverdi}");
                var partypeIsNullOrEmpty = IsAnyValidationMessagesWithXpath(_partstypeValidator._entityXPath);

                if (!codeValueHaveError && !partypeIsNullOrEmpty)
                {
                    ValidateDataRelations(beroertPart);

                    var adresseResult = _enkelAdresseValidator.Validate(beroertPart.Adresse);
                    UpdateValidationResultWithSubValidations(adresseResult);
                }

            }


            return _validationResult;
        }


        public void ValidateEntityFields(Beroertpart beroertPart)
        {
            if (Helpers.ObjectIsNullOrEmpty(beroertPart))
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            else
            {
                if (string.IsNullOrEmpty(beroertPart.Navn))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.navn);

                if (!beroertPart.ErHoeringsmyndighet.HasValue)
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.erHoeringsmyndighet);
            }

        }

        public ValidationResult ValidateDataRelations(Beroertpart beroertpart)
        {
            if (beroertpart.Partstype?.Kodeverdi == "Privatperson")
            {
                if (!string.IsNullOrEmpty(beroertpart.Foedselsnummer) && beroertpart.Foedselsnummer.Length <= 11)
                    AddMessageFromRule(ValidationRuleEnum.kryptert, FieldNameEnum.foedselsnummer);
                else
                {
                    var foedselsnummerValidation =
                        NorskStandardValidator.ValidateFoedselsnummer(beroertpart.Foedselsnummer, _decryptionFactory);
                    switch (foedselsnummerValidation)
                    {
                        case FoedselnumerValidation.Empty:
                            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.foedselsnummer);
                            break;
                        case FoedselnumerValidation.InvalidEncryption:
                            AddMessageFromRule(ValidationRuleEnum.dekryptering, FieldNameEnum.foedselsnummer);
                            break;
                        case FoedselnumerValidation.InvalidDigitsControl:
                            AddMessageFromRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.foedselsnummer);
                            break;
                        case FoedselnumerValidation.Invalid:
                            AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.foedselsnummer);
                            break;
                    }
                }
            }
            else
            {
                var organisasjonsnummerValidation =
                  NorskStandardValidator.ValidateOrgnummer(beroertpart.Organisasjonsnummer);
                switch (organisasjonsnummerValidation)
                {
                    case OrganisasjonsnummerValidation.Empty:
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.organisasjonsnummer);
                        break;
                    case OrganisasjonsnummerValidation.InvalidDigitsControl:
                        AddMessageFromRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.organisasjonsnummer, new[] { beroertpart.Organisasjonsnummer });
                        break;
                    case OrganisasjonsnummerValidation.Invalid:
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.organisasjonsnummer, new[] { beroertpart.Organisasjonsnummer });
                        break;
                }
            }

            return _validationResult;
        }
    }
}
