using System;
using System.Collections.Generic;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterPlan;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterPlan
{
    public class PlanforslagValidator : EntityValidatorBase, IPlanforslagValidator
    {
        private readonly IKodelisteValidator _plantypeValidator;
        private readonly ISaksnummerValidator _kommunensSaksnummerValidator;
        public string _entityXPath { get => base.EntityXPath; }

        private string plantypeKodeverdiXpath;
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        public PlanforslagValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator plantypeValidator, ISaksnummerValidator kommunensSaksnummerValidator) : base(entityValidatorTree)
        {
            _plantypeValidator = plantypeValidator;
            _kommunensSaksnummerValidator = kommunensSaksnummerValidator;
            InitializeConditionalValidationRules();

        }

        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_plantypeValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_kommunensSaksnummerValidator.ValidationResult.ValidationRules);
            AddValidateDataRelationsRules();

        }
        private void AddValidateDataRelationsRules()
        {

            plantypeKodeverdiXpath = Helpers.GetEntityXpath(_plantypeValidator._entityXPath, null, FieldNameEnum.kodeverdi);
            AddValidationRule(ValidationRuleEnum.Stemmer, FieldNameEnum.kravKonsekvensUtredning, null, plantypeKodeverdiXpath);

        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.plannavn);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.arealplanId);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.BegrunnelseKU);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.Planhensikt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.fristForInnspill);
            AddValidationRule(ValidationRuleEnum.senere, FieldNameEnum.fristForInnspill);
            AddValidationRule(ValidationRuleEnum.tidligere, FieldNameEnum.fristForInnspill);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.hjemmesidePlanforslag);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.hjemmesidePlanprogram);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.saksgangOgMedvirkning);

        }

        public ValidationResult Validate(Planforslag planforslag)
        {
            ValidateEntityFields(planforslag);

            if (Helpers.ObjectIsNullOrEmpty(planforslag)) return _validationResult;

            var plantypeValidationResult = _plantypeValidator.Validate(planforslag?.Plantype);
            UpdateValidationResultWithSubValidations(plantypeValidationResult);

            var kommunensSaksnummerValidationResult = _kommunensSaksnummerValidator.Validate(planforslag?.KommunensSaksnummer);
            UpdateValidationResultWithSubValidations(kommunensSaksnummerValidationResult);

            ValidateDataRelations(planforslag);

            return _validationResult;
        }
        public void ValidateEntityFields(Planforslag planforslag)
        {
            if (Helpers.ObjectIsNullOrEmpty(planforslag))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (string.IsNullOrEmpty(planforslag?.Plannavn))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.plannavn);

                if (string.IsNullOrEmpty(planforslag?.ArealplanId))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.arealplanId);

                if (string.IsNullOrEmpty(planforslag?.BegrunnelseKU))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.BegrunnelseKU);

                if (string.IsNullOrEmpty(planforslag?.Planhensikt))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.Planhensikt);

                if (planforslag?.FristForInnspill == null)
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.fristForInnspill);
                else
                {
                    if (planforslag.FristForInnspill <= (DateTime.Now))
                        AddMessageFromRule(ValidationRuleEnum.senere, FieldNameEnum.fristForInnspill);
                    else
                    {
                        if (planforslag.FristForInnspill >= (DateTime.Now.AddMonths(6)))
                            AddMessageFromRule(ValidationRuleEnum.tidligere, FieldNameEnum.fristForInnspill);
                    }
                }

                if (string.IsNullOrEmpty(planforslag?.HjemmesidePlanforslag))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.hjemmesidePlanforslag);
                
                if (string.IsNullOrEmpty(planforslag?.HjemmesidePlanprogram))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.hjemmesidePlanprogram);
                
                if (string.IsNullOrEmpty(planforslag?.SaksgangOgMedvirkning))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.saksgangOgMedvirkning);

            }
        }

        public ValidationResult ValidateDataRelations(Planforslag planforslag)
        {
            //if (!IsAnyValidationMessagesWithXpath(_plantypeValidator._entityXPath) || !IsAnyValidationMessagesWithXpath(plantypeKodeverdiXpath))
            if (!AnyValidationMessagesConteinXpath(_plantypeValidator._entityXPath))
            {
                if (planforslag?.Plantype?.Kodeverdi == "36" && planforslag.KravKonsekvensutredning.GetValueOrDefault(false))
                    AddMessageFromRule(ValidationRuleEnum.Stemmer, FieldNameEnum.kravKonsekvensUtredning, null, plantypeKodeverdiXpath);
            }
            return _validationResult;
        }
    }
}
