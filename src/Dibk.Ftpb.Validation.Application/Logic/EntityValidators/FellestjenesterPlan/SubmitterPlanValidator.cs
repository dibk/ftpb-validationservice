using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterPlan
{

    public class SubmitterPlanValidator : EntityValidatorBase, ISubmitterPlanValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        public List<ValidationMessage> _validationMessages;
        protected const string XPathFilterForSubmitter = "/avsender";
        private readonly IDecryptionFactory _decryptionFactory;

        public SubmitterPlanValidator(IList<EntityValidatorNode> entityValidatorTree, IDecryptionFactory decryptionFactory) : base(entityValidatorTree)
        {
            _decryptionFactory = decryptionFactory;
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRuleSubmitter(ValidationRuleEnum.gyldig, AktoerEnum.forslagsstiller, "/forslagsstiller/partstype/kodeverdi");
            AddValidationRuleSubmitter(ValidationRuleEnum.gyldig, AktoerEnum.plankonsulent, "/plankonsulent/partstype/kodeverdi");
        }


        public ValidationResult Validate(AktoerPlan forslagsstiller, AktoerPlan plankonsulent = null, string loggedInUser = null, List<ValidationMessage> validationMessages = null)
        {
            base.ResetValidationMessages();
            _validationMessages = validationMessages;

            var forslagsstillerIdentifier = GetAktoerIdentifier(forslagsstiller);
            var plankonsulentIdentifier = GetAktoerIdentifier(plankonsulent);

            if (Helpers.ObjectIsNullOrEmpty(plankonsulent))
            {
                if(loggedInUser != null && !loggedInUser.Equals(forslagsstillerIdentifier))
                AddMessageFromRule(ValidationRuleEnum.gyldig, $"{XPathFilterForSubmitter}/{AktoerEnum.forslagsstiller}", null, "/forslagsstiller/partstype/kodeverdi");
            }
            else if (loggedInUser != null && !loggedInUser.Equals(plankonsulentIdentifier))
            {
                AddMessageFromRule(ValidationRuleEnum.gyldig, $"{XPathFilterForSubmitter}/{AktoerEnum.plankonsulent}", null, "/plankonsulent/partstype/kodeverdi");
            }

            return ValidationResult;
        }

        protected string GetAktoerIdentifier(AktoerPlan aktoer)
        {
            var identifier = string.Empty;
            if (Helpers.ObjectIsNullOrEmpty(aktoer))
            {
                identifier = null;
            }   
            else if (aktoer.Partstype.Kodeverdi != null && aktoer.Partstype.Kodeverdi.ToUpper().Equals("PRIVATPERSON"))
            {
                identifier = _decryptionFactory.GetDecryptor().DecryptText(aktoer.Foedselsnummer);
            }
            else
            {
                identifier = aktoer.Organisasjonsnummer;
            }
            return identifier;
        }
    }
    
}
