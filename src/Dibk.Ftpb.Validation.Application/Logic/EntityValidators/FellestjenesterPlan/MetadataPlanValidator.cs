﻿using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterPlan;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterPlan
{
    public class MetadataPlanValidator : EntityValidatorBase, IMetadataPlanValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }

        private readonly IList<EntityValidatorNode> _entityValidatorTree;

        public MetadataPlanValidator(IList<EntityValidatorNode> entityValidatorTree) : base(entityValidatorTree)
        {
            _entityValidatorTree = entityValidatorTree;
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.fraSluttbrukersystem);
        }

        public ValidationResult Validate(MetadataPlan metadata)
        {
            if (string.IsNullOrEmpty(metadata?.FraSluttbrukersystem))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.fraSluttbrukersystem);
            }
            return _validationResult;
        }
    }
}
