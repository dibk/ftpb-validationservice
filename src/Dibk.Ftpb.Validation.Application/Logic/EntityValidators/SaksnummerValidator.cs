﻿using System;
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class SaksnummerValidator : EntityValidatorBase, ISaksnummerValidator
    {
        private readonly object _codeListName;
        private readonly RegistryType _registryType;
        protected ICodeListService _codeListService;

        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }

        public SaksnummerValidator(IList<EntityValidatorNode> entityValidatorTree)
                   : base(entityValidatorTree)
        {
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.saksaar);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.saksaar);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.sakssekvensnummer);
        }

        public ValidationResult Validate(Saksnummer saksnummerStuff)
        {
            base.ResetValidationMessages();

            if (Helpers.ObjectIsNullOrEmpty(saksnummerStuff))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (!saksnummerStuff.Saksaar.HasValue && saksnummerStuff.Sakssekvensnummer.HasValue)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.saksaar);
                }
                else
                {
                    var thisYear = DateTime.Now.Year;
                    if (saksnummerStuff.Saksaar < thisYear - 30 || saksnummerStuff.Saksaar > thisYear)
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.saksaar, new string[] { saksnummerStuff.Saksaar.ToString() });
                    }
                }

                if (!saksnummerStuff.Sakssekvensnummer.HasValue && saksnummerStuff.Saksaar.HasValue)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.sakssekvensnummer);
                }

            }
            return ValidationResult;
        }
    }
}
