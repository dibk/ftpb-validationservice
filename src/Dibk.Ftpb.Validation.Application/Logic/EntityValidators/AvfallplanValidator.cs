﻿using System;
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class AvfallplanValidator : EntityValidatorBase, IAvfallplanValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        private readonly IKodelisteValidator _avfallsklasseValidator;
        public readonly IKodelisteValidator _avfallsfraksjonValidator;
        public string _entityXPath { get => base.EntityXPath; }

        public AvfallplanValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator avfallsklasseValidator, IKodelisteValidator avfallsfraksjonValidator)
            : base(entityValidatorTree)
        {
            _avfallsklasseValidator = avfallsklasseValidator;
            _avfallsfraksjonValidator = avfallsfraksjonValidator;
            AddValidateDataRelationsRules();
        }

        private void AddValidateDataRelationsRules()
        {
            AccumulateValidationRules(_avfallsklasseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_avfallsfraksjonValidator.ValidationResult.ValidationRules);

            var avfallsklasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallsklasseValidator._entityXPath, null, FieldNameEnum.kodeverdi);
            var fraksjonKodeverdiXpath = Helpers.GetEntityXpath(_avfallsfraksjonValidator._entityXPath, null, FieldNameEnum.kodeverdi);

            AddValidationRule(ValidationRuleEnum.utfylt, null, _avfallsklasseValidator._entityXPath);
            AddValidationRule(ValidationRuleEnum.Stemmer, null, avfallsklasseKodeverdiXpath);
        }
        protected override void InitializeValidationRules()
        {

            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.mengdeFraksjon);
            AddValidationRule(ValidationRuleEnum.numerisk, FieldNameEnum.mengdeFraksjon);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.mengdeFraksjon);
        }

        public ValidationResult Validate(Avfallplan avfallplan = null)
        {
            base.ResetValidationMessages();

            ValidateEntityFields(avfallplan);

            if (!Helpers.ObjectIsNullOrEmpty(avfallplan))
            {
                var avfallsfraksjonValidationResult = _avfallsfraksjonValidator.Validate(avfallplan?.Avfallfraksjon);
                UpdateValidationResultWithSubValidations(avfallsfraksjonValidationResult);

                if (!IsAnyValidationMessagesWithXpath(_avfallsfraksjonValidator._entityXPath) && !IsAnyValidationMessagesWithXpath(Helpers.GetEntityXpath(_avfallsfraksjonValidator._entityXPath, null, FieldNameEnum.kodeverdi)))
                    ValidateDataRelations(avfallplan);
            }

            return _validationResult;
        }

        public ValidationResult ValidateDataRelations(Avfallplan avfallplan)
        {
            if (string.IsNullOrEmpty(avfallplan.MengdeFraksjon))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.mengdeFraksjon, new[] { avfallplan?.Avfallfraksjon.Kodebeskrivelse });
            else
            {
                if (decimal.TryParse(avfallplan.MengdeFraksjon, out var mengdeResult))
                {
                    if (mengdeResult < 0)
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.mengdeFraksjon, new[] { avfallplan?.Avfallfraksjon.Kodebeskrivelse });
                    }
                }
                else
                {
                    AddMessageFromRule(ValidationRuleEnum.numerisk, FieldNameEnum.mengdeFraksjon, new[] { avfallplan?.Avfallfraksjon.Kodebeskrivelse });
                }
            }

            // Avfallklasse
            if (Helpers.ObjectIsNullOrEmpty(avfallplan.Avfallklasse))
            {
                //Avfallsplan/plan/avfall/avfallplan{0}/avfallsklasse
                AddMessageFromRule(ValidationRuleEnum.utfylt, _avfallsklasseValidator._entityXPath, new[] { avfallplan?.Avfallfraksjon.Kodebeskrivelse });

            }
            else
            {
                var avfallsklasseValidationResult = _avfallsklasseValidator.Validate(avfallplan?.Avfallklasse);
                UpdateValidationResultWithSubValidations(avfallsklasseValidationResult);

                var avfallsklasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallsklasseValidator._entityXPath, null, FieldNameEnum.kodeverdi);
                if (!IsAnyValidationMessagesWithXpath(avfallsklasseKodeverdiXpath) && !IsAnyValidationMessagesWithXpath(_avfallsklasseValidator._entityXPath))
                {
                    if (avfallplan.Avfallfraksjon.Kodeverdi.StartsWith("1") && !avfallplan.Avfallklasse.Kodeverdi.Equals("ordinaertAvfall", StringComparison.CurrentCultureIgnoreCase))
                        AddMessageFromRule(ValidationRuleEnum.Stemmer, avfallsklasseKodeverdiXpath, new[] { avfallplan.Avfallfraksjon.Kodebeskrivelse, "'Ordinært avfall'" });
                    if (avfallplan.Avfallfraksjon.Kodeverdi.StartsWith("7") && !avfallplan.Avfallklasse.Kodeverdi.Equals("farligAvfall", StringComparison.CurrentCultureIgnoreCase))
                        AddMessageFromRule(ValidationRuleEnum.Stemmer, avfallsklasseKodeverdiXpath, new[] { avfallplan.Avfallfraksjon.Kodebeskrivelse, "'Farlig avfall'" });
                    if (avfallplan.Avfallfraksjon.Kodeverdi.Equals("9912") && !avfallplan.Avfallklasse.Kodeverdi.Equals("blandetAvfall", StringComparison.CurrentCultureIgnoreCase))
                        AddMessageFromRule(ValidationRuleEnum.Stemmer, avfallsklasseKodeverdiXpath, new[] { avfallplan.Avfallfraksjon.Kodebeskrivelse, "'Blandet avfall'" });
                }

            }
            return _validationResult;
        }
        public void ValidateEntityFields(Avfallplan avfallplan = null)
        {
            if (Helpers.ObjectIsNullOrEmpty(avfallplan))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
        }
    }
}
