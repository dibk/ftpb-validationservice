using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.AtilFee;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class BeskrivelseAvTiltakForAtilV2Validator : EntityValidatorBase, IBeskrivelseAvTiltakAtilV2Validator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        private readonly IFormaaltypeValidator _formaaltypeValidator;
        private readonly IKodelisteValidator _tiltakstypeValidator;
        private readonly IAtilFeeCalculatorService _atilFeeCalculatorService;
        public List<string> _Tiltakstypes;

        public BeskrivelseAvTiltakForAtilV2Validator(IList<EntityValidatorNode> entityValidatorTree
                                                    , IFormaaltypeValidator formaaltypeValidator
                                                    , IKodelisteValidator tiltakstypeValidator
                                                    , IAtilFeeCalculatorService atilFeeCalculatorService)
            : base(entityValidatorTree)
        {
            _formaaltypeValidator = formaaltypeValidator;
            _tiltakstypeValidator = tiltakstypeValidator;
            _atilFeeCalculatorService = atilFeeCalculatorService;
            _Tiltakstypes = new List<string>();
        }

        public string[] Tiltakstypes { get => _Tiltakstypes?.ToArray(); }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.BRA);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.BRA);
            AddValidationRule(ValidationRuleEnum.tillatt, FieldNameEnum.BRA);
        }

        public ValidationResult Validate(List<ValidationRule> validationRules, BeskrivelseAvTiltak beskrivelseAvTiltakValidationEntity = null)
        {
            ValidateEntityFields(beskrivelseAvTiltakValidationEntity);

            if (!Helpers.ObjectIsNullOrEmpty(beskrivelseAvTiltakValidationEntity))
            {
                var tiltakstypes = beskrivelseAvTiltakValidationEntity?.Tiltakstype;
                var index = GetArrayIndex(tiltakstypes);
                bool atLeastOneTiltakstypeIsValid = false;

                for (int i = 0; i < index; i++)
                {
                    var tiltakstype = Helpers.ObjectIsNullOrEmpty(tiltakstypes) ? null : tiltakstypes[i];

                    var tiltakstypeValidationResult = _tiltakstypeValidator.Validate(tiltakstype);
                    UpdateValidationResultWithSubValidations(tiltakstypeValidationResult, i);

                    if (tiltakstypeValidationResult.ValidationMessages.Count == 0)
                    {
                        atLeastOneTiltakstypeIsValid = true;
                    }
                    else
                    {
                        bool oneOfTheMessagesAreWarning = false;
                        foreach (var valMessage in tiltakstypeValidationResult?.ValidationMessages)
                        {
                            if (validationRules != null)
                            {
                                var referencedRule = validationRules.First(x => x.Id.EndsWith(valMessage.Reference));
                                if (referencedRule.Messagetype == ValidationResultSeverityEnum.WARNING)
                                {
                                    oneOfTheMessagesAreWarning = true;
                                }
                            }
                        }

                        if (oneOfTheMessagesAreWarning)
                        {
                            atLeastOneTiltakstypeIsValid = true;
                        }
                    }

                    if (tiltakstypes != null && !IsAnyValidationMessagesWithXpath($"{_tiltakstypeValidator._entityXPath}/{FieldNameEnum.kodeverdi}", i))
                    {
                        _Tiltakstypes.Add(tiltakstypes[i].Kodeverdi);
                    }
                }

                if (atLeastOneTiltakstypeIsValid)
                {
                    if (_tiltakstypeValidator._entityXPath != null)
                    {
                        var length = _tiltakstypeValidator._entityXPath.Length - 3;
                        RemoveNotRelevantMessagesFromValidationReport(_tiltakstypeValidator._entityXPath.Substring(0, length));
                    }
                }

                if (!Helpers.ObjectIsNullOrEmpty(beskrivelseAvTiltakValidationEntity))
                {
                    var formaalTypeValidationResult = _formaaltypeValidator.Validate(beskrivelseAvTiltakValidationEntity?.Formaaltype);
                    UpdateValidationResultWithSubValidations(formaalTypeValidationResult);
                }

                var atilTiltakstyper = AsyncHelper.RunSync(() => _atilFeeCalculatorService.GetTiltakstyperAsync());

                if (beskrivelseAvTiltakValidationEntity.BRA == "0")
                {
                    var tiltakstyperNotAllowingZeroInBRA = atilTiltakstyper.Where(x => x.AllowZeroInBRA.ToLower().Equals("false")).ToList();

                    // Compare lists and find common values
                    if (tiltakstypes != null)
                    {
                        var tiltakstyperISoknadenNotAllowingZeroInBRA = from a in tiltakstyperNotAllowingZeroInBRA
                                                                        join k in tiltakstypes?.ToList() on a.Code equals k.Kodeverdi
                                                                        select new { a.Code, k.Kodeverdi };

                        if (tiltakstyperISoknadenNotAllowingZeroInBRA.Any())
                        {
                            var prefix = tiltakstyperISoknadenNotAllowingZeroInBRA.Count() > 1 ? "tiltakstypene" : "tiltakstype";
                            var tiltakstyperAsCommaSepList = string.Join("','", tiltakstyperISoknadenNotAllowingZeroInBRA.Select(x => x.Code));
                            var stringParam = prefix + " '" + tiltakstyperAsCommaSepList + "'";
                            AddMessageFromRule(ValidationRuleEnum.tillatt, FieldNameEnum.BRA, new[] { stringParam });
                        }
                    }
                }
            }

            return ValidationResult;
        }

        public void ValidateEntityFields(BeskrivelseAvTiltak beskrivelseAvTiltakValidationEntity = null)
        {
            if (Helpers.ObjectIsNullOrEmpty(beskrivelseAvTiltakValidationEntity))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                var isNumeric = int.TryParse(beskrivelseAvTiltakValidationEntity?.BRA, out int numericBRA);
                var isFloat = float.TryParse(beskrivelseAvTiltakValidationEntity?.BRA, out float floatBRA);

                if (string.IsNullOrEmpty(beskrivelseAvTiltakValidationEntity?.BRA))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.BRA);
                }
                else if (!isNumeric && !isFloat)
                {
                    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.BRA);
                }
            }
        }
    }
}