﻿using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class AnsvarsrettValidatorV2 : EntityValidatorBase
    {
        private readonly IKodelisteValidator _funksjonValidator;
        private readonly string[] _allowdFunksjontypes;

        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }


        public AnsvarsrettValidatorV2(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator funksjonValidator, string[] funksjontypes = null) : base(entityValidatorTree)
        {
            _funksjonValidator = funksjonValidator;
            _allowdFunksjontypes = funksjontypes;
            InitializeConditionalValidationRules();

        }

        private void InitializeConditionalValidationRules()
        {
            if (_allowdFunksjontypes != null && _allowdFunksjontypes.Any())
            {
                AddValidationRule(ValidationRuleEnum.tillatt, FieldNameEnum.kodeverdi, $"{_funksjonValidator._entityXPath}");
            }
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.soeknadssystemetsReferanse);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.ansvarsrettErklaert);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.beskrivelseAvAnsvarsomraadet);
        }

        public ValidationResult Validate(AnsvarsrettV2 ansvarsrett)
        {
            ValidateEntityFields(ansvarsrett);

            if (!Helpers.ObjectIsNullOrEmpty(ansvarsrett))
            {
                var funksjonValidationResult = _funksjonValidator.Validate(ansvarsrett.Funksjon);
                UpdateValidationResultWithSubValidations(funksjonValidationResult);

                var kodeverdiXpath = $"{_funksjonValidator._entityXPath}/{FieldNameEnum.kodeverdi}";
                var codeValueHaveError = IsAnyValidationMessagesWithXpath(kodeverdiXpath);
                var partypeIsNullOrEmpty = IsAnyValidationMessagesWithXpath(_funksjonValidator._entityXPath);

                if (!codeValueHaveError && !partypeIsNullOrEmpty)
                    if (_allowdFunksjontypes != null && _allowdFunksjontypes.All(e => !e.Equals(ansvarsrett?.Funksjon.Kodeverdi)))
                    {
                        AddMessageFromRule(ValidationRuleEnum.tillatt, kodeverdiXpath, new[] { ansvarsrett?.Funksjon.Kodeverdi });
                    }
            }
            return _validationResult;
        }

        public void ValidateEntityFields(AnsvarsrettV2 ansvarsrett)
        {
            if (Helpers.ObjectIsNullOrEmpty(ansvarsrett))
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            else
            {
                if (string.IsNullOrEmpty(ansvarsrett.SoeknadssystemetsReferanse))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.soeknadssystemetsReferanse);

                if (string.IsNullOrEmpty(ansvarsrett.BeskrivelseAvAnsvarsomraadet))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.beskrivelseAvAnsvarsomraadet);

                if (!ansvarsrett.AnsvarsrettErklaert.HasValue)
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.ansvarsrettErklaert);
            }
        }
    }
}
