using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class KodelisteValidatorV2 : EntityValidatorBase, IKodelisteValidator
    {
        protected readonly Enum CodeListName;
        protected readonly RegistryType RegistryType;
        protected ICodeListService _codeListService;
        protected readonly bool AllowedNull;
        protected readonly string[] AllowedKodeverdi;

        public string _entityXPath { get => base.EntityXPath; }

        public KodelisteValidatorV2(IList<EntityValidatorNode> entityValidatorTree,
            int? nodeId, Enum codeListName, RegistryType registryType, ICodeListService codeListService,
            bool allowedNull = false,
            string[] allowedKodeverdi = null,
            bool useBaseConditionalRules = true
            )
            : base(entityValidatorTree, nodeId)
        {
            CodeListName = codeListName;
            RegistryType = registryType;
            _codeListService = codeListService;
            AllowedNull = allowedNull;
            AllowedKodeverdi = allowedKodeverdi;

            if (useBaseConditionalRules)
                InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            if (!AllowedNull)
            {
                AddValidationRule(ValidationRuleEnum.utfylt);
                AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.kodeverdi);
            }

            if (AllowedKodeverdi != null)
            {
                AddValidationRule(ValidationRuleEnum.tillatt, FieldNameEnum.kodeverdi);
            }
        }

        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.validert, FieldNameEnum.kodeverdi);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.kodeverdi);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.kodebeskrivelse);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.kodebeskrivelse);
        }

        public virtual ValidationResult Validate(Kodeliste kodeEntry, string messageParameter = null)
        {
            base.ResetValidationMessages();

            if (Helpers.ObjectIsNullOrEmpty(kodeEntry) && AllowedNull)
                return ValidationResult;

            if (Helpers.ObjectIsNullOrEmpty(kodeEntry) && !AllowedNull)
            {
                if (string.IsNullOrEmpty(messageParameter))
                    AddMessageFromRule(ValidationRuleEnum.utfylt);
                else
                    AddMessageFromRule(ValidationRuleEnum.utfylt, _entityXPath, new[] { messageParameter });
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(kodeEntry.Kodeverdi) && !AllowedNull)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.kodeverdi, new[] { messageParameter });
                }
                else
                {


                    var isCodeValid = _codeListService.IsCodeValueValid(CodeListName, kodeEntry.Kodeverdi, RegistryType);
                    if (!isCodeValid.HasValue)
                    {
                        AddMessageFromRule(ValidationRuleEnum.validert, FieldNameEnum.kodeverdi, new[] { kodeEntry.Kodeverdi });
                    }
                    else
                    {
                        if (!isCodeValid.GetValueOrDefault())
                        {
                            AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.kodeverdi, new[] { kodeEntry.Kodeverdi });
                        }
                        else
                        {
                            if (AllowedKodeverdi != null && !AllowedKodeverdi.Contains(kodeEntry.Kodeverdi,StringComparer.CurrentCultureIgnoreCase))
                            {
                                AddMessageFromRule(ValidationRuleEnum.tillatt, FieldNameEnum.kodeverdi, new[] { kodeEntry.Kodeverdi });
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(kodeEntry.Kodebeskrivelse))
                    {
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.kodebeskrivelse);
                    }
                    else
                    {
                        if (isCodeValid.GetValueOrDefault())
                        {
                            var isCodelistLabelValid = _codeListService.IsCodeLabelValid(CodeListName, kodeEntry.Kodeverdi, kodeEntry.Kodebeskrivelse, RegistryType);
                            if (!isCodelistLabelValid.GetValueOrDefault())
                            {
                                AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.kodebeskrivelse, new[] { kodeEntry.Kodebeskrivelse });
                            }
                        }
                    }
                }
            }
            return ValidationResult;
        }
    }
}
