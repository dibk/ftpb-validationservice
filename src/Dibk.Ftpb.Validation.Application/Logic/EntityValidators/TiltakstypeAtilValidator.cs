﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class TiltakstypeAtilValidator : KodelisteValidatorV2
    {
        public TiltakstypeAtilValidator(IList<EntityValidatorNode> entityValidatorTree, ICodeListService codeListService)
            : base(entityValidatorTree, null, FtbKodeListeEnum.tiltakstyper_for_arbeidstilsynet, RegistryType.Arbeidstilsynet, codeListService)
        {
            _codeListService = codeListService;
        }
    }
}
