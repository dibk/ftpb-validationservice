
using System.Collections.Generic;
using Dibk.Ftpb.Common.Datamodels.Parts.Atil;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.Atil;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Atil
{
    public class MetadataSuppleringValidator : EntityValidatorBase, IMetadataAtilV2Validator
    {
        ValidationResult IMetadataAtilV2Validator.ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public MetadataSuppleringValidator(IList<EntityValidatorNode> entityValidatorTree)
            : base(entityValidatorTree)
        {
        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.fraSluttbrukersystem);
        }

        public ValidationResult Validate(MetadataAtilSupplering metadataValidationEntity)
        {
            if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity.FraSluttbrukersystem))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.fraSluttbrukersystem);
            }
            return _validationResult;
        }
    }
}
