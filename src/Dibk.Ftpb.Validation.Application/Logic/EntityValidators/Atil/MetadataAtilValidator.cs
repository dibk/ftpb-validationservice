using System.Collections.Generic;
using Dibk.Ftpb.Common.Datamodels.Parts.Atil;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.Atil;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Atil
{
    public class MetadataAtilValidator : EntityValidatorBase, IMetadataAtilValidator
    {
        ValidationResult IMetadataAtilValidator.ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public MetadataAtilValidator(IList<EntityValidatorNode> entityValidatorTree)
            : base(entityValidatorTree)
        {
        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.fraSluttbrukersystem);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.erNorskSvenskDansk);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.erNorskSvenskDansk);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.unntattOffentlighet);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.prosjektnavn);
        }

        public ValidationResult Validate(MetadataAtilSamtykke metadataValidationEntity)
        {
            ResetValidationMessages();

            ValidateEntityFields(metadataValidationEntity);

            return _validationResult;
        }

        public void ValidateEntityFields(MetadataAtilSamtykke metadataValidationEntity)
        {
            if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity.ErNorskSvenskDansk))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.erNorskSvenskDansk);
                }
                else if (!metadataValidationEntity.ErNorskSvenskDansk.GetValueOrDefault(false))
                {
                    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.erNorskSvenskDansk);
                }

                if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity.UnntattOffentlighet))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.unntattOffentlighet);

                if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity.FraSluttbrukersystem))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.fraSluttbrukersystem);

                if (Helpers.ObjectIsNullOrEmpty(metadataValidationEntity.Prosjektnavn))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.prosjektnavn);
            }
        }
    }
}
