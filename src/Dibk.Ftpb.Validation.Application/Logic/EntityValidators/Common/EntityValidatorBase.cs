using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using static System.Enum;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common
{
    public abstract class EntityValidatorBase : IEntityValidator
    {
        //public event EventHandler<ValidationResult> RulesAdded;

        //protected virtual void OnRulesAdded(ValidationResult validationResult)
        //{
        //    RulesAdded?.Invoke(this, validationResult);
        //}

        protected string EntityName;
        private string _validatorIdPath;
        public string EntityXPath;
        //public abstract string ruleXmlElement { get; set; }
        protected ValidationResult _validationResult;
        private EntityValidatorNode _entity;
        private static IList<EntityValidatorNode> _entityValidatorTree;
        ValidationResult IEntityValidator.ValidationResult { get => _validationResult; set => _validationResult = value; }

        public EntityValidatorBase(IList<EntityValidatorNode> entityValidatorTree, int? nodeId = null)
        {
            _entityValidatorTree = entityValidatorTree;
            _validationResult = new ValidationResult();
            _validationResult.ValidationRules = new List<ValidationRule>();
            _validationResult.ValidationMessages = new List<ValidationMessage>();

            var validatorName = this.GetType().Name;

            var node = GetEntityValidatorNode(entityValidatorTree, nodeId, validatorName);
            _entity = node;
            if (node != null)
            {
                _validatorIdPath = node.IdPath;
                EntityXPath = node.EntityXPath;
            }
            else
            {
                _validatorIdPath = "Can't find the node";
                EntityXPath = nodeId.HasValue ? $"Can't find Entity validator enum:'{validatorName}' with nodeId:'{nodeId}'" : $"Can't find Entity validator enum:'{validatorName}'.";
            }

            InitializeValidationRules();
        }

        private static EntityValidatorNode GetEntityValidatorNode(IList<EntityValidatorNode> entityValidationTree, int? treeNodeId, string validatorName)
        {
            EntityValidatorNode entityValidationNode;
            if (treeNodeId.HasValue)
                entityValidationNode = entityValidationTree.FirstOrDefault(e => e.NodeId.Equals(treeNodeId.Value));
            else
                entityValidationNode = entityValidationTree.FirstOrDefault(e => GetName(typeof(EntityValidatorEnum), e.EnumId).Equals(validatorName));

            if (entityValidationNode == null)
            {
                foreach (EntityValidatorNode validationGroup in entityValidationTree)
                {
                    entityValidationNode = GetEntityValidatorNode(validationGroup.Children, treeNodeId, validatorName);
                    if (entityValidationNode != null)
                        break;
                }
            }
            return entityValidationNode;
        }
        private static EntityValidatorNode GetValidatorNodeByXpath(string xpath, IList<EntityValidatorNode> ChildrenNode = null)
        {
            EntityValidatorNode entityValidationNode = null;
            IList<EntityValidatorNode> entityValidatorNodes = ChildrenNode ?? _entityValidatorTree;
            foreach (EntityValidatorNode validationGroup in entityValidatorNodes)
            {
                var isValidatorNode = validationGroup.EntityXPath.Equals(xpath);

                if (isValidatorNode)
                    entityValidationNode = validationGroup;
                else
                    entityValidationNode = GetValidatorNodeByXpath(xpath, validationGroup.Children);


                if (entityValidationNode != null)
                    return entityValidationNode;
            }
            return entityValidationNode;
        }

        protected abstract void InitializeValidationRules();

        public ValidationResult ResetValidationMessages()
        {
            _validationResult.ValidationMessages = new List<ValidationMessage>();
            return _validationResult;
        }
        protected void UpdateValidationResultWithSubValidations(ValidationResult newValudationResult, int? index = null)
        {
            _validationResult.ValidationRules.AddRange(newValudationResult.ValidationRules);
            if (newValudationResult.ValidationMessages != null && index.HasValue)
            {
                foreach (var message in newValudationResult.ValidationMessages)
                {
                    var messageXpathField = Helpers.ReplaceCurlyBracketInXPath(index.Value, message.XpathField);
                    message.XpathField = messageXpathField;

                    var preConditionXpathField = Helpers.ReplaceCurlyBracketInXPath(index.Value, message.PreCondition);
                    message.PreCondition = preConditionXpathField;

                    _validationResult.ValidationMessages.Add(message);
                }
            }
            else
            {
                _validationResult.ValidationMessages.AddRange(newValudationResult.ValidationMessages);
            }
        }

        protected void AddValidationRule(ValidationRuleEnum rule, FieldNameEnum? xmlElement = null, string overrideXpath = null, string preCondition = null, DateTime? validFrom = null, DateTime? validTo = null)
        {
            var fieldNumberString = String.Empty;

            if (xmlElement != null)
            {
                var fieldNameNumber = EnumHelper.GetEnumFieldNameNumber(xmlElement.Value);
                fieldNumberString = $".{fieldNameNumber}";
            }

            var xmlElementString = EnumHelper.GetXmlNodeFromFieldNameEnum(xmlElement);

            AddValidationRule(rule, xmlElementString, fieldNumberString, overrideXpath, preCondition, validFrom, validTo);
        }



        protected void AddValidationRuleAttachment(ValidationRuleEnum rule, AttachmentEnum? xmlElement = null, string xpathPrecondition = null, string overrideXpath = null)
        {
            var fieldNumberString = String.Empty;
            if (xmlElement != null)
            {
                var fieldNameNumber = EnumHelper.GetEnumAttachmentNumber(xmlElement.Value);
                fieldNumberString = $".{fieldNameNumber}";
            }
            var xmlElementString = xmlElement?.ToString();

            AddValidationRule(rule, xmlElementString, fieldNumberString, overrideXpath, xpathPrecondition);
        }

        protected void AddValidationRuleSubmitter(ValidationRuleEnum rule, AktoerEnum? xmlElement = null, string xpathPrecondition = null, string overrideXpath = null)
        {
            var fieldNumberString = String.Empty;
            if (xmlElement != null)
            {
                var fieldNameNumber = EnumHelper.GetEnumAktoerNumber(xmlElement.Value);
                fieldNumberString = $".{fieldNameNumber}";
            }
            var xmlElementString = xmlElement?.ToString();

            AddValidationRule(rule, xmlElementString, fieldNumberString, overrideXpath, xpathPrecondition);
        }

        private void AddValidationRule(ValidationRuleEnum rule, string element, string fieldNumber, string overrideXpath = null, string preCondition = null, DateTime? validFrom = null, DateTime? validTo = null)
        {
            var separator = "";
            ValidationRuleEnum validationRule = (ValidationRuleEnum)rule;
            var validationRuleTypeId = EnumHelper.GetRuleNumberFromValidationRuleEmum(validationRule);

            var elementRuleId = $"{_validatorIdPath}{fieldNumber}.{validationRuleTypeId}";

            var ruleXpath = overrideXpath ?? EntityXPath;

            if (!string.IsNullOrEmpty(element))
            {
                separator = "/";
            }
            string xPath = $"{ruleXpath}{separator}{element}";
             
            _validationResult.ValidationRules.Add(new ValidationRule()
            {
                Rule = rule.ToString(),
                XpathField = xPath,
                XmlElement = element,
                Id = elementRuleId,
                PreCondition = preCondition,
                ValidFrom = validFrom,
                ValidTo = validTo,
            });

        }

        /// <summary>
        /// To use with validation of xml fields
        /// </summary>
        protected void AddMessageFromRule(ValidationRuleEnum id, FieldNameEnum? fieldName = null, string[] messageParameters = null, string preConditionField = null, int? elementNumber = null)
        {
            var xpathNew = fieldName.HasValue ? $"{_entity?.EntityXPath}/{EnumHelper.GetXmlNodeFromFieldNameEnum(fieldName)}" : null;
            AddMessageFromRule(id, xpathNew, messageParameters, preConditionField, null);
        }

        protected void AccumulateValidationMessages(List<ValidationMessage> validationMessages, int? index = null)
        {
            if (index.HasValue)
            {
                foreach (var message in validationMessages)
                {
                    var messageXpathField = Helpers.ReplaceCurlyBracketInXPath(index.Value, message.XpathField);
                    message.XpathField = messageXpathField;

                    var preConditionXpathField = Helpers.ReplaceCurlyBracketInXPath(index.Value, message.PreCondition);
                    message.PreCondition = preConditionXpathField;
                }
            }
            _validationResult.ValidationMessages.AddRange(validationMessages);
        }

        /// <summary>
        /// To use with validation of attachments
        /// </summary>
        protected void AddMessageFromRuleAttachment(ValidationRuleEnum id, AttachmentEnum? attachmentName = null, string[] messageParameters = null, string preConditionField = null, int? elementNumber = null)
        {
            var xpathNew = attachmentName.HasValue ? $"{_entity?.EntityXPath}/{attachmentName.ToString()}" : null;
            AddMessageFromRule(id, xpathNew, messageParameters, preConditionField, null);
        }

        //Add this method to override xPath if the validation value is not from the same 'entity'
        public void AddMessageFromRule(ValidationRuleEnum id, string overrideXpath, string[] messageParameters = null, string preConditionField = null, int? elementNumber = null)
        {
            var idSt = id.ToString();
            string xpathNew;

            if (string.IsNullOrEmpty(overrideXpath))
            {
                xpathNew = _entity?.EntityXPath;
            }
            else
            {
                xpathNew = overrideXpath;
            }


            var rule = RuleToValidate(idSt, xpathNew, preConditionField);


            if (elementNumber != null)
            {
                xpathNew = xpathNew?.Replace("{0}", "[0]");

                var toBeReplaced = "[0]";
                var replaceWith = $"[{elementNumber}]";
                int position = xpathNew.LastIndexOf(toBeReplaced);
                if (position == -1)
                {
                    throw new ArgumentException($"Illegal argument. Xpath must contain array element. Element no. {elementNumber} is given as input.");
                }
                else
                {
                    xpathNew = xpathNew.Remove(position, toBeReplaced.Length).Insert(position, replaceWith);
                }
            }

            var validationMessage = new ValidationMessage()
            {
                Rule = idSt,
                Reference = rule?.Id ?? _validatorIdPath,
                XpathField = xpathNew,
                PreCondition = preConditionField ?? rule?.PreCondition,
                MessageParameters = messageParameters,
                Message = rule?.Message
            };

            _validationResult.ValidationMessages.Add(validationMessage);
        }
        //**
        //Todo: Should we rename method for a more intuitive understanding???

        public bool IsAnyValidationMessagesWithXpath(string xpath, int? index = null, List<ValidationMessage> validationMessages = null)
        {
            bool ruleFounded = false;
            if (validationMessages == null)
                validationMessages = _validationResult.ValidationMessages;

            if (xpath != null && xpath.Contains("{0}") && !index.HasValue)
            {
               
                foreach (var message in validationMessages)
                {
                    string pattern = @"\[([0-9])+]";
                    var messageXpath = Regex.Replace(message.XpathField, pattern, "{0}");
                    if (xpath.Equals(messageXpath))
                    {
                        return true;
                    }
                }
            }

            var newXpath = index.HasValue ? Helpers.ReplaceCurlyBracketInXPath(index.Value, xpath) : xpath;
            ruleFounded = validationMessages.Any(r => !string.IsNullOrEmpty(r.XpathField) && r.XpathField.EndsWith(newXpath, StringComparison.OrdinalIgnoreCase));
            return ruleFounded;
        }
        public bool AnyValidationMessagesConteinXpath(string xpath, ValidationRuleEnum[] rule = null, int? index = null)
        {
            var newXpath = index.HasValue ? Helpers.ReplaceCurlyBracketInXPath(index.Value, xpath) : xpath;
            bool ruleFounded;
            var rules = _validationResult.ValidationMessages.Where(r => !string.IsNullOrEmpty(r.XpathField) && r.XpathField.Contains(newXpath, StringComparison.OrdinalIgnoreCase)).ToArray();
            if (rule != null)
            {
                string[] validationRuleEnumString = Array.ConvertAll(rule, ruleEnum => ruleEnum.ToString());
                ruleFounded = rules.Any(rule => validationRuleEnumString.Any(t => t == rule.Rule));
            }
            else
            {
                ruleFounded = rules.Any();
            }

            return ruleFounded;
        }
        public bool RuleIsValid(ValidationRuleEnum ruleEnum, string xPath = null)
        {
            var rule = ruleEnum.ToString();
            xPath ??= EntityXPath;

            var validationMessage = _validationResult.ValidationMessages.Where(r => !string.IsNullOrEmpty(r.Rule))
                .FirstOrDefault(r => r.Rule.Equals(rule) && (r.XpathField.EndsWith(xPath)));

            return validationMessage == null;
        }

        public static int GetArrayIndex(object[] objectArray)
        {
            if (objectArray == null || objectArray.Length == 0)
                return 1;

            return objectArray.Length;
        }

        //**Add rules with dynamic enum 
        public ValidationRule RuleToValidate(string rule, string xPath, string preCondition = null)
        {
            ValidationRule validationRule;
            try
            {
                if (preCondition == null)
                {
                    validationRule = _validationResult.ValidationRules.Where(r => !string.IsNullOrEmpty(r.Rule))
                        .FirstOrDefault(r => r.Rule.Equals(rule) && (r.XpathField.EndsWith(xPath)));
                }
                else
                {
                    validationRule = _validationResult.ValidationRules.Where(r => !string.IsNullOrEmpty(r.Rule))
                        .FirstOrDefault(r => r.Rule.Equals(rule) && (r.XpathField.EndsWith(xPath)) && (r.PreCondition != null && r.PreCondition.EndsWith(preCondition)));
                }

                if (validationRule == null)
                {
                    validationRule = new ValidationRule()
                    {
                        Rule = rule,
                        Message = $"Can't find rule:'{rule}'.-",
                        PreCondition = preCondition

                    };
                }

            }
            catch (Exception)
            {
                validationRule = new ValidationRule()
                {
                    Rule = rule,
                    Message = $"Something went wrong Can't find rule:'{rule}'.-"
                };
            }
            return validationRule;

        }
        public void RemoveNotRelevantMessagesFromValidationReport(string xPath, ValidationRuleEnum? validationRule = null)
        {
            var filteredValidationMessages = new List<ValidationMessage>();

            if (validationRule.HasValue)
                filteredValidationMessages = _validationResult.ValidationMessages?.Where(r => !r.XpathField.StartsWith(xPath) && r.Rule.Equals(validationRule.Value.ToString()))?.ToList();
            else
                filteredValidationMessages = _validationResult.ValidationMessages?.Where(r => !r.XpathField.StartsWith(xPath))?.ToList();

            _validationResult.ValidationMessages = filteredValidationMessages;
        }
        public ValidationResult RemoveNotRelevantMessagesFromValidationReport(ValidationResult validationResult, string xPath, ValidationRuleEnum? validationRule = null)
        {
            var filteredValidationMessages = new List<ValidationMessage>();

            if (validationRule.HasValue)
                filteredValidationMessages = _validationResult.ValidationMessages?.Where(r => !r.XpathField.StartsWith(xPath) && r.Rule.Equals(validationRule.Value.ToString()))?.ToList();
            else
                filteredValidationMessages = _validationResult.ValidationMessages?.Where(r => !r.XpathField.StartsWith(xPath))?.ToList();

            validationResult.ValidationMessages = filteredValidationMessages;
            validationResult.RulesChecked = null; //TODO clean


            return validationResult;
        }
        //**
        public void AccumulateValidationRules(List<ValidationRule> validationRules)
        {
            var whereNotAlreadyExists = validationRules
                .Where(x => !_validationResult.ValidationRules
                        .Any(y =>
                            y.XpathField == x.XpathField &&
                            y.Rule == x.Rule &&
                            y.ValidFrom == x.ValidFrom &&
                            y.ValidTo == x.ValidTo &&
                            y.PreCondition == x.PreCondition
                        ));

            _validationResult.ValidationRules.AddRange(whereNotAlreadyExists);
        }

    }
}
