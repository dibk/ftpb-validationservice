using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Microsoft.FeatureManagement;
using System;
using System.Collections.Generic;
using System.Linq;

//********************************************************************
// OBS OBS OBS OBS OBS OBS OBS OBS OBS OBS OBS OBS OBS OBS OBS OBS OBS
// ATIL Feature - SLETTES ETTER 20250217
//--------------------------------------------------------------------

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public sealed class AtilFeature
    {
        public bool Atil20250217Enabled { get; set; }
        private AtilFeature() { }
        private static AtilFeature _instance;

        public static AtilFeature Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AtilFeature();
                }
                return _instance;
            }
        }

        public void LoadFeature(IFeatureManager featureManager)
        {
            Instance.Atil20250217Enabled = AsyncHelper.RunSync(async () => await featureManager.IsEnabledAsync("ATIL20250217"));
        }
    }

    public class VedleggAtilV2Validator : VedleggValidatorBase
    {
        public VedleggAtilV2Validator(IList<EntityValidatorNode> entityValidatorTree)
            : base(entityValidatorTree)
        {
        }


        protected override void InitializeValidationRules()
        {
            // ATIL Feature - SLETTES ETTER 20250217 - START
            if (AtilFeature.Instance.Atil20250217Enabled)
            {
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.TiltakshaverSignatur);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Arbeidsmiljoutvalg);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.ArbeidstakersRepresentant);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Verneombud);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.LeietakerArbeidsgiver);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Bedriftshelsetjeneste);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Soknad);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.SoknadUnntasOffentlighet);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.TegningEksisterendePlan, "/krav/sjekklistekrav");
            }
            else
            {
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.TiltakshaverSignatur);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Arbeidsmiljoutvalg);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.ArbeidstakersRepresentant);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Verneombud);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.LeietakerArbeidsgiver);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Bedriftshelsetjeneste);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.Soknad);
                AddValidationRuleAttachment(ValidationRuleEnum.vedlegg, AttachmentEnum.SoknadUnntasOffentlighet);
            }
            //                                       - SLUTT
        }

        private IEnumerable<string> GetExpectedAttachments()
        {
            // ATIL Feature - SLETTES ETTER 20250217 - START
            if (AtilFeature.Instance.Atil20250217Enabled)
            {
                return new List<string>() { AttachmentEnum.TiltakshaverSignatur.ToString()
                                      , AttachmentEnum.SoknadUnntasOffentlighet.ToString()
                                      , AttachmentEnum.Arbeidsmiljoutvalg.ToString()
                                      , AttachmentEnum.TegningEksisterendePlan.ToString()
                                      , AttachmentEnum.ArbeidstakersRepresentant.ToString()
                                      , AttachmentEnum.Verneombud.ToString()
                                      , AttachmentEnum.LeietakerArbeidsgiver.ToString()
                                      , AttachmentEnum.Bedriftshelsetjeneste.ToString()
                };
            }
            else
            {
                return new List<string>() { AttachmentEnum.TiltakshaverSignatur.ToString()
                                      , AttachmentEnum.Arbeidsmiljoutvalg.ToString()
                                      , AttachmentEnum.ArbeidstakersRepresentant.ToString()
                                      , AttachmentEnum.Verneombud.ToString()
                                      , AttachmentEnum.LeietakerArbeidsgiver.ToString()
                                      , AttachmentEnum.Bedriftshelsetjeneste.ToString()
                };
            }
            //                                       - SLUTT
        }

        private List<string> GetRelevantSjekklistepunkt()
        {
            return new List<string> {
                "11.2",
                "11.6",
                "11.8",
                "11.13"
            };
        }


        public override ValidationResult Validate(ValidationInput validationInput, object formdata, string loggedInUser, List<ValidationMessage> validationMessages = null)
        {
            // ATIL Feature - SLETTES ETTER 20250217 - START
            if (AtilFeature.Instance.Atil20250217Enabled)
            {
                base.ResetValidationMessages();
                _validationMessages = validationMessages;

                ArbeidstilsynetsSamtykkeV2 ATILForm = formdata as ArbeidstilsynetsSamtykkeV2;
                if (ATILForm == null)
                {
                    throw new ArgumentException("Expected form ArbeidstilsynetsSamtykke2_45957_Form");
                }

                var attachmentsExpected = GetExpectedAttachments();
                IEnumerable<string> attachmentsInForm = null;

                if (validationInput.Attachments != null)
                {
                    attachmentsInForm = validationInput.Attachments.Select(x => x.AttachmentTypeName);

                    List<AttachmentInfo> attachmentOfTypeSoknad = validationInput.Attachments.Where(attachments => attachments.AttachmentTypeName.Equals(AttachmentEnum.Soknad.ToString())).ToList();
                    if (attachmentOfTypeSoknad != null && attachmentOfTypeSoknad.Count > 1)
                        AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.Soknad}");
                }
                else
                {
                    attachmentsInForm = new List<string>();
                }

                foreach (var requiredAttachment in attachmentsExpected)
                {
                    if (attachmentsInForm.Contains(requiredAttachment))
                        continue;

                    ValidateSpecificAttachment(requiredAttachment, ATILForm, loggedInUser);
                }

                return ValidationResult;
            }
            else
            {
                base.ResetValidationMessages();
                _validationMessages = validationMessages;

                ArbeidstilsynetsSamtykkeV2 ATILForm = formdata as ArbeidstilsynetsSamtykkeV2;
                if (ATILForm == null)
                {
                    throw new ArgumentException("Expected form ArbeidstilsynetsSamtykke2_45957_Form");
                }

                var signeringsAnsvarlig = ATILForm.Tiltakshaver;

                //var attachmentsExpected_orig = GetExpectedAttachments(XPathFilterForAttachments).Where(vedlegg => !vedlegg.Equals(AttachmentEnum.Soknad.ToString()));
                var attachmentsExpected = GetExpectedAttachments();

                IEnumerable<string> attachmentsInForm = null;
                if (validationInput.Attachments != null)
                {
                    attachmentsInForm = validationInput.Attachments.Select(x => x.AttachmentTypeName);

                    List<AttachmentInfo> attachmentOfTypeSoknad = validationInput.Attachments.Where(attachments => attachments.AttachmentTypeName.Equals(AttachmentEnum.Soknad.ToString())).ToList();
                    if (attachmentOfTypeSoknad != null && attachmentOfTypeSoknad.Count > 1)
                    {
                        AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.Soknad}");
                    }
                    if (ATILForm.Metadata?.UnntattOffentlighet == true && !attachmentsInForm.Contains(AttachmentEnum.SoknadUnntasOffentlighet.ToString()))
                        AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.SoknadUnntasOffentlighet}");
                }
                else
                {
                    if (ATILForm.Metadata != null && ATILForm.Metadata.UnntattOffentlighet == true)
                        AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{AttachmentEnum.SoknadUnntasOffentlighet}");
                }

                foreach (var requiredAttachment in attachmentsExpected)
                {
                    if (attachmentsInForm == null || !attachmentsInForm.Contains(requiredAttachment))
                    {
                        if (requiredAttachment.Equals(AttachmentEnum.TiltakshaverSignatur.ToString())
                            && loggedInUser != null
                            && !Helpers.ObjectIsNullOrEmpty(signeringsAnsvarlig)
                            && loggedInUser.Equals(GetTiltakshaverIdentifier(signeringsAnsvarlig))
                            )
                        {

                        }

                        else
                            AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{requiredAttachment}");
                    }
                }

                return ValidationResult;
            }
            //                                       - SLUTT
        }

        private void ValidateSpecificAttachment(string requiredAttachment, ArbeidstilsynetsSamtykkeV2 form, string loggedInUser)
        {
            switch (requiredAttachment)
            {
                case nameof(AttachmentEnum.SoknadUnntasOffentlighet):
                    if (form.Metadata?.UnntattOffentlighet == true)
                        AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{requiredAttachment}");
                    break;

                case nameof(AttachmentEnum.TegningEksisterendePlan):
                    ValidateTegningEksisterendePlan(form, requiredAttachment);
                    break;

                case nameof(AttachmentEnum.Arbeidsmiljoutvalg):
                    ValidateArbeidsmiljoutvalg(form, requiredAttachment);
                    break;

                case nameof(AttachmentEnum.TiltakshaverSignatur):
                    ValidateTiltakshaverSignatur(form, loggedInUser, requiredAttachment);
                    break;

                default:
                    AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{requiredAttachment}");
                    break;
            }
        }

        private void ValidateTegningEksisterendePlan(ArbeidstilsynetsSamtykkeV2 form, string requiredAttachment)
        {
            if (form.Sjekklistekrav == null)
                return;

            var relevantPoints = GetRelevantSjekklistepunkt();
            if (form.Sjekklistekrav.Any(sjekklistepkt =>
                    relevantPoints.Contains(sjekklistepkt.Sjekklistepunkt.Kodeverdi) &&
                    sjekklistepkt.Sjekklistepunktsvar == true))
            {
                AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{requiredAttachment}");
            }
        }

        private void ValidateArbeidsmiljoutvalg(ArbeidstilsynetsSamtykkeV2 form, string requiredAttachment)
        {
            if (!int.TryParse(form.Arbeidsplasser?.AntallVirksomheter, out var antallVirksomheter) ||
                !int.TryParse(form.Arbeidsplasser?.AntallAnsatte, out var antallAnsatte))
                return;

            if (antallVirksomheter == 1 && antallAnsatte >= 30)
                AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{requiredAttachment}");
        }

        private void ValidateTiltakshaverSignatur(ArbeidstilsynetsSamtykkeV2 form, string loggedInUser, string requiredAttachment)
        {
            var tiltakshaverIdentifier = GetTiltakshaverIdentifier(form.Tiltakshaver);

            if (loggedInUser != null && !Helpers.ObjectIsNullOrEmpty(form.Tiltakshaver) && loggedInUser == tiltakshaverIdentifier)
                return;

            AddMessageFromRule(ValidationRuleEnum.vedlegg, $"{XPathFilterForAttachments}/{requiredAttachment}");
        }
    }
}
