﻿
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class EttersendingValidator : EntityValidatorBase, IEttersendingValidator
    {
        private readonly ISuppleringVedleggValidator _ettersendingVedleggValidator;
        private readonly IKodelisteValidator _temaValidator;

        ValidationResult IEttersendingValidator.ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public EttersendingValidator(IList<EntityValidatorNode> entityValidatorTree, 
                                     ISuppleringVedleggValidator ettersendingVedleggValidator,
                                     IKodelisteValidator temaValidator)
            : base(entityValidatorTree)
        {
            _ettersendingVedleggValidator = ettersendingVedleggValidator;
            _temaValidator = temaValidator;
        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.tittel); //ERROR "Du har valgt å ettersende informasjon. Da må du lage en tittel som beskriver det du ettersender."
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.vedleggEllerUnderskjemaEllerKommentar);
        }

        public ValidationResult Validate(Ettersending ettersending)
        {
            base.ResetValidationMessages();
            ValidateEntityFields(ettersending);

            var vedleggsArray = ettersending.Vedlegg;
            var index = GetArrayIndex(vedleggsArray);

            for (int i = 0; i < index; i++)
            {
                var vedlegg = Helpers.ObjectIsNullOrEmpty(vedleggsArray) ? null : vedleggsArray[i];
                var vedleggsValidationResult = _ettersendingVedleggValidator.Validate(vedlegg);
                UpdateValidationResultWithSubValidations(vedleggsValidationResult, i);
            }

            var temaValidationResult = _temaValidator.Validate(ettersending.Tema);
            UpdateValidationResultWithSubValidations(temaValidationResult);

            return _validationResult;
        }

        public void ValidateEntityFields(Ettersending ettersending)
        {
            if (Helpers.ObjectIsNullOrEmpty(ettersending))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, this.EntityXPath);
            }
            else
            {
                if (Helpers.ObjectIsNullOrEmpty(ettersending.Tittel))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.tittel);

                if (Helpers.ObjectIsNullOrEmpty(ettersending.Kommentar) && Helpers.ObjectIsNullOrEmpty(ettersending.Vedlegg) && Helpers.ObjectIsNullOrEmpty(ettersending.Underskjema))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.vedleggEllerUnderskjemaEllerKommentar);
                }
            }
        }
    }
}
