﻿using System;
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class AvfallklasseDelsumValidator : EntityValidatorBase, IAvfallklasseDelsumValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        public string _entityXPath { get => base.EntityXPath; }

        private readonly IKodelisteValidator _avfallsklasseValidator;

        public AvfallklasseDelsumValidator(IList<EntityValidatorNode> entityValidatorTree, int? nodeId, IKodelisteValidator avfallsklasseValidator)
            : base(entityValidatorTree, nodeId)
        {
            _avfallsklasseValidator = avfallsklasseValidator;
            AddValidateDataRelationsRules();
        }

        private void AddValidateDataRelationsRules()
        {
            AccumulateValidationRules(_avfallsklasseValidator.ValidationResult.ValidationRules);

        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.delsum);
        }

        public ValidationResult Validate(AvfallsklasseDelsum avfallsKlasseDelSum = null)
        {
            base.ResetValidationMessages();

            ValidateEntityFields(avfallsKlasseDelSum);

            var avfallsklasseValidationResult = _avfallsklasseValidator.Validate(avfallsKlasseDelSum?.Avfallsklasse);
            UpdateValidationResultWithSubValidations(avfallsklasseValidationResult);

            if (!IsAnyValidationMessagesWithXpath(Helpers.GetEntityXpath(_avfallsklasseValidator._entityXPath)) && !IsAnyValidationMessagesWithXpath(Helpers.GetEntityXpath(_avfallsklasseValidator._entityXPath, null, FieldNameEnum.kodeverdi)))
                ValidateDataRelations(avfallsKlasseDelSum);


            return _validationResult;
        }

        public ValidationResult ValidateDataRelations(AvfallsklasseDelsum avfallsKlasseDelSum)
        {
            if (string.IsNullOrEmpty(avfallsKlasseDelSum.Delsum))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.delsum, new[] { avfallsKlasseDelSum.Avfallsklasse.Kodebeskrivelse });

            return _validationResult;
        }
        public ValidationResult ValidateEntityFields(AvfallsklasseDelsum avfallsKlasseDelSum = null)
        {

            return _validationResult;
        }
    }
}
