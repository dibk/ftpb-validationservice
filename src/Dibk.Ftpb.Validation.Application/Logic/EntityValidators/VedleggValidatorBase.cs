﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public abstract class VedleggValidatorBase : EntityValidatorBase, IVedleggValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        public List<ValidationMessage> _validationMessages;
        protected const string XPathFilterForAttachments = "/vedlegg";

        public VedleggValidatorBase(IList<EntityValidatorNode> entityValidatorTree)
            : base(entityValidatorTree)
        {
        }

        public virtual ValidationResult Validate(ValidationInput validationInput, object formdata, string loggedInUser, List<ValidationMessage> validationMessages = null)
        {
            return ValidationResult;
        }

        //protected IEnumerable<string> GetExpectedAttachments(string xPathFilter)
        //{
        //    return this.ValidationResult.ValidationRules.Where(x => x.XpathField.Contains(xPathFilter))
        //                .Select(z => z.XpathField.Substring(z.XpathField.IndexOf(xPathFilter) + xPathFilter.Length + 1));
        //}

        protected string GetTiltakshaverIdentifier(Aktoer aktoer)
        {
            if (aktoer?.Partstype?.Kodeverdi == null)
                return null;

            if (aktoer.Partstype.Kodeverdi.ToUpper().Equals("PRIVATPERSON"))
            {
                return aktoer.Foedselsnummer;
            }
            else
            {
                return aktoer.Organisasjonsnummer;
            }
        }
    }
}
