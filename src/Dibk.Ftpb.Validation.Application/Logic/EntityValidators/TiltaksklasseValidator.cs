﻿using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class TiltaksklasseValidator : KodelisteValidatorV2
    {
        public TiltaksklasseValidator(IList<EntityValidatorNode> entityValidatorTree, int nodeId, ICodeListService codeListService, bool allowedNull = false)
            : base(entityValidatorTree, nodeId, FtbKodeListeEnum.tiltaksklasse, RegistryType.Byggesoknad, codeListService, allowedNull)
        {
        }
    }
}
