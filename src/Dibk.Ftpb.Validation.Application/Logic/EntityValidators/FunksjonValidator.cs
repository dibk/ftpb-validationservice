﻿using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class FunksjonValidator : KodelisteValidatorV2
    {
        public FunksjonValidator(IList<EntityValidatorNode> entityValidatorTree, int nodeId, ICodeListService codeListService, bool allowedNull = false)
            : base(entityValidatorTree, nodeId, FtbKodeListeEnum.funksjon, RegistryType.Byggesoknad, codeListService, allowedNull)
        {
            _codeListService = codeListService;
        }
    }
}
