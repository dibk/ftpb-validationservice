﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.GeneralValidations;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators
{
    public abstract class AktoerValidator : EntityValidatorBase, IAktoerValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }

        private ICodeListService _codeListService;
        private readonly IDecryptionFactory _decryptionFactory;
        private string[] _allowedPartstypes; 
        private IEnkelAdresseValidator _enkelAdresseValidator;
        private IKontaktpersonValidator _kontaktpersonValidator;
        private IKodelisteValidator _partstypeValidator;

        //Partial validation is used when we want skip certain validations. Used for IG.
        private bool PartialValidation;
        private bool _useUtfyltRule;
        public string _entityXPath { get => base.EntityXPath; }


        public AktoerValidator(IList<EntityValidatorNode> entityValidatorTree, IEnkelAdresseValidator enkelAdresseValidator,
            IKontaktpersonValidator kontaktpersonValidator, IKodelisteValidator partstypeValidator, ICodeListService codeListService,
            IDecryptionFactory decryptionFactory, string[] allowedPartstypes = null, bool partialValidation = false, bool useUtfyltRule = true)
            : base(entityValidatorTree)
        {
            _codeListService = codeListService;
            _decryptionFactory = decryptionFactory;
            _enkelAdresseValidator = enkelAdresseValidator;
            _kontaktpersonValidator = kontaktpersonValidator;
            _partstypeValidator = partstypeValidator;
            _allowedPartstypes = allowedPartstypes;
            PartialValidation = partialValidation;
            _useUtfyltRule = useUtfyltRule;
            InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            if (_useUtfyltRule)
                AddValidationRule(ValidationRuleEnum.utfylt);

            if (_enkelAdresseValidator != null)
            {
                AccumulateValidationRules(_enkelAdresseValidator.ValidationResult.ValidationRules);
            }
            if (_kontaktpersonValidator != null)
            {
                AccumulateValidationRules(_kontaktpersonValidator.ValidationResult.ValidationRules);
            }
            AccumulateValidationRules(_partstypeValidator.ValidationResult.ValidationRules);

            var partstypeKodeverdi = Helpers.GetEntityXpath(this._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.PartstypeValidator);

            if (_allowedPartstypes != null && _allowedPartstypes.Any())
                AddValidationRule(ValidationRuleEnum.tillatt, null, partstypeKodeverdi);

            if (!PartialValidation)
            {
                AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.epost);

                AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.telefonnummer, null, $"{_entityXPath}/{FieldNameEnum.mobilnummer}");
                AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.telefonnummer);
                AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.mobilnummer);
            }

            if (IncludePrivatperson())
            {
                AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.foedselsnummer);
                AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.foedselsnummer);
                AddValidationRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.foedselsnummer);
                AddValidationRule(ValidationRuleEnum.dekryptering, FieldNameEnum.foedselsnummer);
            }
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.organisasjonsnummer);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.organisasjonsnummer);
            AddValidationRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.organisasjonsnummer);

            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.navn);
        }

        public ValidationResult Validate(Aktoer aktoer = null, string messageParameter = null)
        {
            if (Helpers.ObjectIsNullOrEmpty(aktoer))
                HandleUtfyltRuleError();

            else
            {
                var aktoerPartsType = aktoer.Partstype;

                var partstypeValidatinResults = _partstypeValidator.Validate(aktoerPartsType, messageParameter);
                UpdateValidationResultWithSubValidations(partstypeValidatinResults);

                var codeValueHaveError = IsAnyValidationMessagesWithXpath($"{_partstypeValidator._entityXPath}/{FieldNameEnum.kodeverdi}");
                var partypeIsNullOrEmpty = IsAnyValidationMessagesWithXpath(_partstypeValidator._entityXPath);

                if (!codeValueHaveError && !partypeIsNullOrEmpty)
                {
                    ValidateDataRelations(aktoer);

                    if (_enkelAdresseValidator != null)
                    {
                        var enkeladressResult = _enkelAdresseValidator.Validate(aktoer.Adresse);
                        UpdateValidationResultWithSubValidations(enkeladressResult);
                    }
                }

            }
            return _validationResult;
        }

        protected virtual void HandleUtfyltRuleError()
        {
            if (_useUtfyltRule)
                AddMessageFromRule(ValidationRuleEnum.utfylt);
        }

        public ValidationResult ValidateDataRelations(Aktoer aktoer)
        {
            if (_allowedPartstypes != null && _allowedPartstypes.All(p => !p.Equals(aktoer.Partstype.Kodeverdi)))
            {
                AddMessageFromRule(ValidationRuleEnum.tillatt, $"{_entityXPath}/partstype/{FieldNameEnum.kodeverdi}", new[] { aktoer.Partstype.Kodeverdi });
                return _validationResult;
            }

            if (aktoer.Partstype?.Kodeverdi == "Privatperson" && IncludePrivatperson())
            {
                var foedselsnummerValidation = NorskStandardValidator.ValidateFoedselsnummer(aktoer.Foedselsnummer, _decryptionFactory);
                switch (foedselsnummerValidation)
                {
                    case FoedselnumerValidation.Empty:
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.foedselsnummer);
                        break;
                    case FoedselnumerValidation.InvalidEncryption:
                        AddMessageFromRule(ValidationRuleEnum.dekryptering, FieldNameEnum.foedselsnummer);
                        break;
                    case FoedselnumerValidation.InvalidDigitsControl:
                        AddMessageFromRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.foedselsnummer);
                        break;
                    case FoedselnumerValidation.Invalid:
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.foedselsnummer);
                        break;
                }
            }
            else if (aktoer.Partstype?.Kodeverdi != "Privatperson")
            {
                var organisasjonsnummerValidation =
                    NorskStandardValidator.ValidateOrgnummer(aktoer.Organisasjonsnummer);
                switch (organisasjonsnummerValidation)
                {
                    case OrganisasjonsnummerValidation.Empty:
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.organisasjonsnummer, new[] { aktoer.Navn });
                        break;
                    case OrganisasjonsnummerValidation.InvalidDigitsControl:
                        AddMessageFromRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.organisasjonsnummer, new[] { aktoer.Organisasjonsnummer });
                        break;
                    case OrganisasjonsnummerValidation.Invalid:
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.organisasjonsnummer, new[] { aktoer.Organisasjonsnummer });
                        break;
                }

                if (_kontaktpersonValidator != null)
                {
                    var kontaktpersonValidationResult = _kontaktpersonValidator.Validate(aktoer.Kontaktperson);
                    UpdateValidationResultWithSubValidations(kontaktpersonValidationResult);
                }
            }

            if (string.IsNullOrEmpty(aktoer.Navn))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.navn);

            if (!PartialValidation)
            {
                if (string.IsNullOrEmpty(aktoer.Epost))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.epost);

                if (string.IsNullOrEmpty(aktoer.Telefonnummer) && string.IsNullOrEmpty(aktoer.Mobilnummer))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.telefonnummer, null, $"{_entityXPath}/{FieldNameEnum.mobilnummer}");
                }
                else
                {
                    if (!string.IsNullOrEmpty(aktoer.Telefonnummer))
                    {
                        var telefonNumber = aktoer?.Telefonnummer;
                        var isValidTelefonNumber = telefonNumber.All(c => "+0123456789".Contains(c));
                        if (!isValidTelefonNumber)
                        {
                            AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.telefonnummer);
                        }
                    }
                    if (!string.IsNullOrEmpty(aktoer.Mobilnummer))
                    {
                        var mobilNummer = aktoer.Mobilnummer;
                        var isValidmobilnummer = mobilNummer.All(c => "+0123456789".Contains(c));
                        if (!isValidmobilnummer)
                        {
                            AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.mobilnummer);
                        }
                    }
                }
            }

            return _validationResult;
        }

        private bool IncludePrivatperson()
        {
            if (PartialValidation)
            {
                return false;
            }
            if (_allowedPartstypes == null || !_allowedPartstypes.Any())
                return true;

            if (_allowedPartstypes.Any(p => p.Equals("Privatperson")))
                return true;

            return false;
        }

    }
}
