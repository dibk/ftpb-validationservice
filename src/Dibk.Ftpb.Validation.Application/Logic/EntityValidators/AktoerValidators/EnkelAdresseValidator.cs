using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.GeneralValidations;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators
{
    public class EnkelAdresseValidator : EntityValidatorBase, IEnkelAdresseValidator
    {
        private readonly IPostalCodeService _postalCodeService;
        private readonly bool _tillatKunNorge;

        ValidationResult IEnkelAdresseValidator.ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public EnkelAdresseValidator(IList<EntityValidatorNode> entityValidatorTree, int nodeId, IPostalCodeService postalCodeService, bool tillatKunNorge = false)
            : base(entityValidatorTree, nodeId)
        {
            _postalCodeService = postalCodeService;
            _tillatKunNorge = tillatKunNorge;

            if (tillatKunNorge)
            {
                InitializeConditionalValidationRules();
            }
        }

        private void InitializeConditionalValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.tillatt, FieldNameEnum.landkode);
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.adresselinje1);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.landkode);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.postnr);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.postnr);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.poststed);
        }

        public ValidationResult Validate(EnkelAdresse enkelAdresse)
        {
            base.ResetValidationMessages();

            ValidateEntityFields(enkelAdresse);

            return _validationResult;
        }

        public void ValidateEntityFields(EnkelAdresse enkelAdresse)
        {
            if (Helpers.ObjectIsNullOrEmpty(enkelAdresse))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (string.IsNullOrEmpty(enkelAdresse.Adresselinje1))
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.adresselinje1);

                if (!CountryCodeHandler.IsCountryNorway(enkelAdresse.Landkode))
                {
                    if (!CountryCodeHandler.VerifyCountryCode(enkelAdresse.Landkode))
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.landkode, new[] { enkelAdresse.Landkode });
                    else if (_tillatKunNorge)
                        AddMessageFromRule(ValidationRuleEnum.tillatt, FieldNameEnum.landkode);
                }
                else
                {
                    var postNr = enkelAdresse.Postnr;

                    if (string.IsNullOrEmpty(postNr))
                    {
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.postnr);
                    }
                    else
                    {
                        var postnrValidationResult = _postalCodeService.ValidatePostnr(postNr);

                        if (!postnrValidationResult.Gyldig)
                        {
                            AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.postnr, new[] { postNr });
                        }
                        else
                        {
                            //Mangler god håndtering i situasjoner der valideringsresultatet sitt poststed er null
                            // samt enkelAdresse.Poststed er null. Dette burde vel håndteres bedre.
                            // Følgende nullsjekk er implementert for at ikke tjenesten skal eksplodere i nullref-exceptions
                            if (!string.IsNullOrEmpty(postnrValidationResult.Poststed) && !string.IsNullOrEmpty(enkelAdresse.Poststed))
                                if (!postnrValidationResult.Poststed.Equals(enkelAdresse.Poststed, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.poststed, new[] { postNr, enkelAdresse.Poststed, postnrValidationResult.Poststed });
                                }
                        }
                    }
                }
            }
        }
    }
}