﻿using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators
{
    public class TiltakshaverValidator : AktoerValidator
    {
        private bool _isDependentOnAnsvarForByggesaken;
        private string _ansvarForByggesakenKodeverdi;
        private string _ansvarForByggesakenKodebeskrivelse;

        public TiltakshaverValidator(IList<EntityValidatorNode> entityValidatorTree, IEnkelAdresseValidator enkelAdresseValidator,
               IKontaktpersonValidator kontaktpersonValidator, IKodelisteValidator partstypeValidator, ICodeListService codeListService,
               IDecryptionFactory decryptionFactory, string[] allowedPartstypes = null, bool isDependentOnAnsvarForByggesaken = false)
            : base(entityValidatorTree, enkelAdresseValidator, kontaktpersonValidator, partstypeValidator, codeListService, decryptionFactory, allowedPartstypes)
        {
            _isDependentOnAnsvarForByggesaken = isDependentOnAnsvarForByggesaken;
        }

        public ValidationResult Validate(Aktoer aktoer, Kodeliste ansvarForByggesaken)
        {
            _ansvarForByggesakenKodeverdi = ansvarForByggesaken?.Kodeverdi;
            _ansvarForByggesakenKodebeskrivelse = ansvarForByggesaken?.Kodebeskrivelse;

            var conditionalKodeverdiValues = new string[] { "utenAnsvar", "utenAnsvarMedKontroll"};

            if (conditionalKodeverdiValues.Any(kodeverdi => kodeverdi.Equals(_ansvarForByggesakenKodeverdi, System.StringComparison.OrdinalIgnoreCase)))
                return base.Validate(aktoer);

            return ValidationResult;
        }

        protected override void HandleUtfyltRuleError()
        {
            if (_isDependentOnAnsvarForByggesaken)
                AddMessageFromRule(ValidationRuleEnum.utfylt, "/tiltakshaver", [_ansvarForByggesakenKodebeskrivelse]);
            else
                base.HandleUtfyltRuleError();
        }
    }
}
