﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Plan;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.GeneralValidations;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces.FellestjenesterPlan;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators.Plan
{
    public abstract class AktoerPlanValidator : EntityValidatorBase, IAktoerPlanValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }

        private ICodeListService _codeListService;
        private readonly IDecryptionFactory _decryptionFactory;
        private string[] _allowedPartstypes;
        private IEnkelAdresseValidator _enkelAdresseValidator;
        private IKodelisteValidator _partstypeValidator;
        public string _entityXPath { get => base.EntityXPath; }


        public AktoerPlanValidator(IList<EntityValidatorNode> entityValidatorTree, IEnkelAdresseValidator enkelAdresseValidator, IKodelisteValidator partstypeValidator, ICodeListService codeListService,
            IDecryptionFactory decryptionFactory, string[] allowedPartstypes = null)
            : base(entityValidatorTree)
        {
            _codeListService = codeListService;
            _decryptionFactory = decryptionFactory;
            _enkelAdresseValidator = enkelAdresseValidator;
            _partstypeValidator = partstypeValidator;
            _allowedPartstypes = allowedPartstypes;
            InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_enkelAdresseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_partstypeValidator.ValidationResult.ValidationRules);

            var partstypeKodeverdi = Helpers.GetEntityXpath(this._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.PartstypeValidator);

            if (_allowedPartstypes != null && _allowedPartstypes.Any())
                AddValidationRule(ValidationRuleEnum.tillatt, null, partstypeKodeverdi);

            if (IncludePrivatperson())
            {
                AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.foedselsnummer);
                AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.foedselsnummer);
                AddValidationRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.foedselsnummer);
                AddValidationRule(ValidationRuleEnum.dekryptering, FieldNameEnum.foedselsnummer);
                AddValidationRule(ValidationRuleEnum.kryptert, FieldNameEnum.foedselsnummer);
            }
        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);

            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.organisasjonsnummer);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.organisasjonsnummer);
            AddValidationRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.organisasjonsnummer);

            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.navn);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.epost);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.epost);

            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.telefonnummer);
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.telefonnummer);
        }

        public ValidationResult Validate(AktoerPlan aktoer = null)
        {
            ValidateEntityFields(aktoer);
            if (!Helpers.ObjectIsNullOrEmpty(aktoer))
            {
                var aktoerPartsType = aktoer.Partstype;

                var partstypeValidatinResults = _partstypeValidator.Validate(aktoerPartsType);
                UpdateValidationResultWithSubValidations(partstypeValidatinResults);

                var codeValueHaveError = IsAnyValidationMessagesWithXpath($"{_partstypeValidator._entityXPath}/{FieldNameEnum.kodeverdi}");
                var partypeIsNullOrEmpty = IsAnyValidationMessagesWithXpath(_partstypeValidator._entityXPath);

                if (!codeValueHaveError && !partypeIsNullOrEmpty)
                {
                    ValidateDataRelations(aktoer);

                    var enkeladressResult = _enkelAdresseValidator.Validate(aktoer.Adresse);
                    UpdateValidationResultWithSubValidations(enkeladressResult);
                }

            }
            return _validationResult;
        }

        public ValidationResult ValidateDataRelations(AktoerPlan aktoer)
        {
            if (_allowedPartstypes != null && _allowedPartstypes.All(p => !p.Equals(aktoer.Partstype.Kodeverdi)))
            {
                AddMessageFromRule(ValidationRuleEnum.tillatt, $"{_entityXPath}/partstype/{FieldNameEnum.kodeverdi}", new[] { aktoer.Partstype.Kodeverdi });
                return _validationResult;
            }

            if (aktoer.Partstype?.Kodeverdi == "Privatperson" && IncludePrivatperson())
            {

                if (!string.IsNullOrEmpty(aktoer.Foedselsnummer) && aktoer.Foedselsnummer.Length <= 11)
                    AddMessageFromRule(ValidationRuleEnum.kryptert, FieldNameEnum.foedselsnummer);
                else
                {
                    var foedselsnummerValidation = NorskStandardValidator.ValidateFoedselsnummer(aktoer.Foedselsnummer, _decryptionFactory);
                    switch (foedselsnummerValidation)
                    {
                        case FoedselnumerValidation.Empty:
                            AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.foedselsnummer);
                            break;
                        case FoedselnumerValidation.InvalidEncryption:
                            AddMessageFromRule(ValidationRuleEnum.dekryptering, FieldNameEnum.foedselsnummer);
                            break;
                        case FoedselnumerValidation.InvalidDigitsControl:
                            AddMessageFromRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.foedselsnummer);
                            break;
                        case FoedselnumerValidation.Invalid:
                            AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.foedselsnummer);
                            break;
                    }
                }
            }
            else
            {
                var organisasjonsnummerValidation =
                    NorskStandardValidator.ValidateOrgnummer(aktoer.Organisasjonsnummer);
                switch (organisasjonsnummerValidation)
                {
                    case OrganisasjonsnummerValidation.Empty:
                        AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.organisasjonsnummer);
                        break;
                    case OrganisasjonsnummerValidation.InvalidDigitsControl:
                        AddMessageFromRule(ValidationRuleEnum.kontrollsiffer, FieldNameEnum.organisasjonsnummer, new[] { aktoer.Organisasjonsnummer });
                        break;
                    case OrganisasjonsnummerValidation.Invalid:
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.organisasjonsnummer, new[] { aktoer.Organisasjonsnummer });
                        break;
                }
            }

            if (string.IsNullOrEmpty(aktoer.Navn))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.navn);

            if (string.IsNullOrEmpty(aktoer.Epost))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.epost);
            else
            {
                var validEmail = Helpers.IsValidEmail.Match(aktoer.Epost).Success;
                if (!validEmail)
                    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.epost, new[] { aktoer.Epost });
            }

            if (string.IsNullOrEmpty(aktoer.Telefon))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.telefonnummer);
            }
            else
            {
                var telefonNumber = aktoer?.Telefon;
                var isValidTelefonNumber = telefonNumber.All(c => "+0123456789".Contains(c));
                if (!isValidTelefonNumber)
                {
                    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.telefonnummer);
                }
            }

            return _validationResult;
        }

        public ValidationResult ValidateEntityFields(AktoerPlan aktoer)
        {
            if (Helpers.ObjectIsNullOrEmpty(aktoer))
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            return _validationResult;
        }

        private bool IncludePrivatperson()
        {
            if (_allowedPartstypes == null || !_allowedPartstypes.Any())
                return true;

            if (_allowedPartstypes.Any(p => p.Equals("Privatperson")))
                return true;

            return false;
        }
    }
}
