﻿using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators.Plan
{
    public class PlankonsulentValidator : AktoerPlanValidator
    {
        public PlankonsulentValidator(IList<EntityValidatorNode> entityValidatorTree, IEnkelAdresseValidator enkelAdresseValidator, IKodelisteValidator partstypeValidator, ICodeListService codeListService,
               IDecryptionFactory decryptionFactory, string[] allowedPartstypes = null)
            : base(entityValidatorTree, enkelAdresseValidator, partstypeValidator, codeListService, decryptionFactory, allowedPartstypes)
        {
           
        }
    }
}
