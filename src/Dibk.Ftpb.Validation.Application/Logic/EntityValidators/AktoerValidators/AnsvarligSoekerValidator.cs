using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators
{
    public class AnsvarligSoekerValidator : AktoerValidator
    {
        private readonly bool _isDependentOnAnsvarForByggesaken;

        public AnsvarligSoekerValidator(IList<EntityValidatorNode> entityValidatorTree, IEnkelAdresseValidator enkelAdresseValidator,
                IKontaktpersonValidator kontaktpersonValidator, IKodelisteValidator partstypeValidator, ICodeListService codeListService,
                IDecryptionFactory decryptionFactory, string[] allowedPartstypes = null, bool isDependentOnAnsvarForByggesaken = false, bool partialValidation = false)
            : base(entityValidatorTree, enkelAdresseValidator, kontaktpersonValidator, partstypeValidator, codeListService, decryptionFactory, allowedPartstypes, partialValidation)
        {
            _isDependentOnAnsvarForByggesaken = isDependentOnAnsvarForByggesaken;

            if (_isDependentOnAnsvarForByggesaken)
            {
                AddValidationRule(ValidationRuleEnum.gyldig);
                AddValidationRule(ValidationRuleEnum.validert);
            }
        }

        public ValidationResult Validate(Aktoer aktoer, Kodeliste ansvarForByggesaken)
        {
            var medAnsvarsrettValues = new[] { "medAnsvar", "selvbygger" };
            var utenAnsvarsrettValues = new[] { "utenAnsvar", "utenAnsvarMedKontroll" };

            var ansvarForByggesakenKodeverdi = ansvarForByggesaken?.Kodeverdi;
            var ansvarForByggesakenKodebeskrivelse = ansvarForByggesaken?.Kodebeskrivelse;

            if (medAnsvarsrettValues.Any(kodeverdi => kodeverdi.Equals(ansvarForByggesakenKodeverdi, System.StringComparison.OrdinalIgnoreCase)))
            {
                if (Helpers.ObjectIsNullOrEmpty(aktoer))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, messageParameters: new[] { ansvarForByggesakenKodebeskrivelse });
                }

                return base.Validate(aktoer);
            }
            if (utenAnsvarsrettValues.Any(kodeverdi => kodeverdi.Equals(ansvarForByggesakenKodeverdi, System.StringComparison.OrdinalIgnoreCase)))
            {
                if (!Helpers.ObjectIsNullOrEmpty(aktoer))
                    AddMessageFromRule(ValidationRuleEnum.gyldig, messageParameters: new[] { ansvarForByggesakenKodebeskrivelse });
            }
            else
            {
                AddMessageFromRule(ValidationRuleEnum.validert);
            }

            return ValidationResult;
        }

        protected override void HandleUtfyltRuleError()
        {
            if (!_isDependentOnAnsvarForByggesaken)
                base.HandleUtfyltRuleError();
            //else - Do nothing, as the error is handled in the Validate method
        }
    }
}