﻿using System;
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class FormaaltypeValidatorV2 : EntityValidatorBase, IFormaaltypeValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        private readonly IKodelisteValidator _bygningstypeValidator;

        public FormaaltypeValidatorV2(IList<EntityValidatorNode> entityValidatorTree,IKodelisteValidator bygningstypeValidator)
            : base(entityValidatorTree)
        {
            _bygningstypeValidator = bygningstypeValidator;
            InitializeConditionalValidationRules();
        }

        private void InitializeConditionalValidationRules()
        {
            AccumulateValidationRules(_bygningstypeValidator.ValidationResult.ValidationRules);

        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
        }

        public ValidationResult Validate(Formaaltype formaaltypeValEntity = null)
        {
            ValidateEntityFields(formaaltypeValEntity);
            
            if (!Helpers.ObjectIsNullOrEmpty(formaaltypeValEntity))
            {
                var bygningstyper = formaaltypeValEntity?.Bygningstype;
                var indexBygningstyper = GetArrayIndex(bygningstyper);

                for (int i = 0; i < indexBygningstyper; i++)
                {
                    var bygningstype = Helpers.ObjectIsNullOrEmpty(bygningstyper) ? null : bygningstyper[i];
                    var bygningstypeValidationResult = _bygningstypeValidator.Validate(bygningstype);
                    UpdateValidationResultWithSubValidations(bygningstypeValidationResult, i);
                }
            }

            return _validationResult;
        }
        public void ValidateEntityFields(Formaaltype formaaltypeValEntity)
        {
            if (Helpers.ObjectIsNullOrEmpty(formaaltypeValEntity))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
        }
    }
}
