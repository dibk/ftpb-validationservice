﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class SluttrapportForAvfallValidator : EntityValidatorBase, ISluttrapportForAvfallValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        private readonly IAvfallrapportValidator _avfallrapportValidator;
        private readonly IDisponeringsmaateDelsumValidator _disponeringsmaateDelSumValidator;
        private readonly IAvfallklasseDelsumValidator _avfallklasseDelsumValidator;
        private readonly IAvfallsorteringValidator _avfallsorteringValidator;

        public SluttrapportForAvfallValidator(IList<EntityValidatorNode> entityValidatorTree, IAvfallrapportValidator avfallrapportValidator, IDisponeringsmaateDelsumValidator disponeringsmaateDelsumValidator, IAvfallklasseDelsumValidator avfallklasseDelsumValidator,
                                            IAvfallsorteringValidator avfallsorteringValidator)
            : base(entityValidatorTree)
        {
            _avfallrapportValidator = avfallrapportValidator;
            _disponeringsmaateDelSumValidator = disponeringsmaateDelsumValidator;
            _avfallklasseDelsumValidator = avfallklasseDelsumValidator;
            _avfallsorteringValidator = avfallsorteringValidator;
            AddValidateDataRelationsRules();
        }

        private void AddValidateDataRelationsRules()
        {
            AccumulateValidationRules(_avfallrapportValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_disponeringsmaateDelSumValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_avfallklasseDelsumValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_avfallsorteringValidator.ValidationResult.ValidationRules);

            var rapportAvfallsklasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallrapportValidator._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.AvfallklasseValidator);

            var delsumAvfallsklasseXpath = Helpers.GetEntityXpath(_avfallklasseDelsumValidator._entityXPath, null, null, EntityValidatorEnum.AvfallklasseValidator);
            var delsumAvfallsklasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallklasseDelsumValidator._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.AvfallklasseValidator);


            AddValidationRule(ValidationRuleEnum.tillatt, null, delsumAvfallsklasseKodeverdiXpath);
            AddValidationRule(ValidationRuleEnum.finnes, null, delsumAvfallsklasseXpath);

            AddValidationRule(ValidationRuleEnum.BlandetAvfall, null, rapportAvfallsklasseKodeverdiXpath);
            AddValidationRule(ValidationRuleEnum.FarligAvfallOgEllerOrdinaertAvfall, null, rapportAvfallsklasseKodeverdiXpath);



        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
        }

        public ValidationResult Validate(SluttrapportForAvfall sluttrapportForAvfall = null, Overgangsordning overgangsordning = null, string[] attachmentsTypesName = null)
        {
            ValidateEntityFields(sluttrapportForAvfall);

            if (!IsAnyValidationMessagesWithXpath(this.EntityXPath))
            {
                var avfallrapportArray = sluttrapportForAvfall?.Avfallrapport;
                var index = GetArrayIndex(avfallrapportArray);
                for (int i = 0; i < index; i++)
                {
                    var avfallrapport = Helpers.ObjectIsNullOrEmpty(avfallrapportArray) ? null : avfallrapportArray[i];
                    var avfallrapportValidationResult = _avfallrapportValidator.Validate(avfallrapport);
                    UpdateValidationResultWithSubValidations(avfallrapportValidationResult, i);
                }
                ValidateDataRelations(sluttrapportForAvfall);

                // /sluttrapport/disponeringsmaateDelsum
                var disponeringsmaateDelsumArray = sluttrapportForAvfall?.AvfalldisponeringsmaateDelsum;
                index = GetArrayIndex(disponeringsmaateDelsumArray);
                for (int i = 0; i < index; i++)
                {
                    var disponeringsmaateDelsum = Helpers.ObjectIsNullOrEmpty(disponeringsmaateDelsumArray) ? null : disponeringsmaateDelsumArray[i];
                    var disponeringsmaateDelsumValidationResult = _disponeringsmaateDelSumValidator.Validate(disponeringsmaateDelsum);
                    UpdateValidationResultWithSubValidations(disponeringsmaateDelsumValidationResult, i);
                }

                // sluttrapport/sortering
                var avfallsorteringValidationResult = _avfallsorteringValidator.Validate(sluttrapportForAvfall?.Avfallsortering, overgangsordning, attachmentsTypesName);
                UpdateValidationResultWithSubValidations(avfallsorteringValidationResult);
            }

            return _validationResult;
        }

        public ValidationResult ValidateDataRelations(SluttrapportForAvfall sluttrapportForAvfall)
        {

            var planAvfallsklasseXpath = Helpers.GetEntityXpath(_avfallrapportValidator._entityXPath, null, null, EntityValidatorEnum.AvfallklasseValidator);
            var avfallrapportKlasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallrapportValidator._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.AvfallklasseValidator); ;

            var planFraksjonXpath = Helpers.GetEntityXpath(_avfallrapportValidator._entityXPath, null, null, EntityValidatorEnum.AvfallfraksjonValidator);
            var planFraksjonKodeverdiXpath = Helpers.GetEntityXpath(_avfallrapportValidator._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.AvfallfraksjonValidator);

            var isAnyErrorForAvfallklasse = IsAnyValidationMessagesWithXpath(planAvfallsklasseXpath) || IsAnyValidationMessagesWithXpath(avfallrapportKlasseKodeverdiXpath);
            var isAnyErrorForFraksjon = IsAnyValidationMessagesWithXpath(planFraksjonXpath) || IsAnyValidationMessagesWithXpath(planFraksjonKodeverdiXpath);


            if (!isAnyErrorForFraksjon && !isAnyErrorForAvfallklasse && !IsAnyValidationMessagesWithXpath(_avfallrapportValidator._entityXPath))
            {
                var avfallrapportKlasseArray = sluttrapportForAvfall.Avfallrapport.Where(k => !string.IsNullOrEmpty(k.Avfallklasse.Kodeverdi))
                    .Select(k => k.Avfallklasse.Kodeverdi)?.Distinct()?.ToArray();

                if (!avfallrapportKlasseArray.Contains("blandetAvfall"))
                {
                    AddMessageFromRule(ValidationRuleEnum.BlandetAvfall, avfallrapportKlasseKodeverdiXpath);
                }
                else if (!avfallrapportKlasseArray.Contains("ordinaertAvfall") && !avfallrapportKlasseArray.Contains("farligAvfall"))
                {
                    AddMessageFromRule(ValidationRuleEnum.FarligAvfallOgEllerOrdinaertAvfall, avfallrapportKlasseKodeverdiXpath);
                }
                else
                {
                    var avfallsklasseDelsumArray = sluttrapportForAvfall?.AvfallsklasseDelsum;
                    var index = GetArrayIndex(avfallsklasseDelsumArray);
                    for (int i = 0; i < index; i++)
                    {
                        var avfallsklasseDelsum = Helpers.ObjectIsNullOrEmpty(avfallsklasseDelsumArray) ? null : avfallsklasseDelsumArray[i];
                        var avfallsklasseDelsumValidationResult = _avfallklasseDelsumValidator.Validate(avfallsklasseDelsum);
                        UpdateValidationResultWithSubValidations(avfallsklasseDelsumValidationResult, i);
                    }
                    var avfallsklassedelsumAvfallKlasse = Helpers.GetEntityXpath(_avfallklasseDelsumValidator._entityXPath, null, null, EntityValidatorEnum.AvfallklasseValidator);
                    var avfallsklassedelsumAvfallKlasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallklasseDelsumValidator._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.AvfallklasseValidator); ;

                    var isAnyErrorForDelsumAvfallklasse = IsAnyValidationMessagesWithXpath(avfallsklassedelsumAvfallKlasse) || IsAnyValidationMessagesWithXpath(avfallsklassedelsumAvfallKlasseKodeverdiXpath);

                    if (!isAnyErrorForDelsumAvfallklasse && !IsAnyValidationMessagesWithXpath(_avfallklasseDelsumValidator._entityXPath))
                    {
                        var delsumAvfallKlasseArray = sluttrapportForAvfall.AvfallsklasseDelsum.Where(k => !string.IsNullOrEmpty(k.Avfallsklasse.Kodeverdi))
                            .Select(k => k.Avfallsklasse.Kodeverdi)?.Distinct()?.ToArray();

                        var delsumAvfallKlasseCount = delsumAvfallKlasseArray.Length;
                        var avfallrapportKlasseCount = avfallrapportKlasseArray.Length;

                        var delsumAvfallsklasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallklasseDelsumValidator._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.AvfallklasseValidator);

                        if (delsumAvfallKlasseCount != avfallrapportKlasseCount)
                        {
                            AddMessageFromRule(ValidationRuleEnum.tillatt, delsumAvfallsklasseKodeverdiXpath, new string[] { avfallrapportKlasseCount.ToString(), string.Join(", ", avfallrapportKlasseArray), delsumAvfallKlasseCount.ToString(), string.Join(", ", delsumAvfallKlasseArray) });
                        }
                        else
                        {
                            var avfallKlasseExcept = avfallrapportKlasseArray.Except(delsumAvfallKlasseArray);

                            if (avfallKlasseExcept.Any())
                            {
                                foreach (var avfallKlasseKodeverdi in avfallKlasseExcept)
                                {
                                    var avfallrapportKlasse = sluttrapportForAvfall.Avfallrapport.FirstOrDefault(r => r.Avfallklasse?.Kodeverdi == avfallKlasseKodeverdi);
                                    AddMessageFromRule(ValidationRuleEnum.finnes, avfallsklassedelsumAvfallKlasse, new[] { avfallrapportKlasse?.Avfallklasse?.Kodebeskrivelse });
                                }
                            }
                        }
                    }

                }
            }
            return _validationResult;
        }
        public void ValidateEntityFields(SluttrapportForAvfall sluttrapportForAvfall)
        {
            if (Helpers.ObjectIsNullOrEmpty(sluttrapportForAvfall))
                AddMessageFromRule(ValidationRuleEnum.utfylt);
        }
    }
}
