﻿using System;
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class AvfallrapportValidator : EntityValidatorBase, IAvfallrapportValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        private readonly IKodelisteValidator _avfallsklasseValidator;
        private readonly IKodelisteValidator _avfallsfraksjonValidator;
        private readonly IAvfalldisponeringValidator _avfalldisponeringValidator;
        public string _entityXPath { get => base.EntityXPath; }

        public AvfallrapportValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator avfallsklasseValidator,
                                        IKodelisteValidator avfallsfraksjonValidator, IAvfalldisponeringValidator avfalldisponeringValidator)
            : base(entityValidatorTree)
        {
            _avfallsklasseValidator = avfallsklasseValidator;
            _avfallsfraksjonValidator = avfallsfraksjonValidator;
            _avfalldisponeringValidator = avfalldisponeringValidator;
            AddValidateDataRelationsRules();
        }

        private void AddValidateDataRelationsRules()
        {
            AccumulateValidationRules(_avfallsklasseValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_avfallsfraksjonValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_avfalldisponeringValidator.ValidationResult.ValidationRules);

            var avfallsklasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallsklasseValidator._entityXPath, null, FieldNameEnum.kodeverdi);
            var fraksjonKodeverdiXpath = Helpers.GetEntityXpath(_avfallsfraksjonValidator._entityXPath, null, FieldNameEnum.kodeverdi);

            AddValidationRule(ValidationRuleEnum.Stemmer, null, avfallsklasseKodeverdiXpath);
        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.mengdeFraksjon);
            AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.mengdeFraksjon);
            AddValidationRule(ValidationRuleEnum.numerisk, FieldNameEnum.mengdeFraksjon);
        }

        public ValidationResult Validate(Avfallrapport avfallrapport = null)
        {
            base.ResetValidationMessages();

            ValidateEntityFields(avfallrapport);

            if (!IsAnyValidationMessagesWithXpath(this._entityXPath))
            {
                var avfallsfraksjonValidationResult = _avfallsfraksjonValidator.Validate(avfallrapport?.Avfallfraksjon);
                UpdateValidationResultWithSubValidations(avfallsfraksjonValidationResult);

                if (!IsAnyValidationMessagesWithXpath(_avfallsfraksjonValidator._entityXPath) && !IsAnyValidationMessagesWithXpath(Helpers.GetEntityXpath(_avfallsfraksjonValidator._entityXPath, null, FieldNameEnum.kodeverdi)))
                    ValidateDataRelations(avfallrapport);
            }

            return _validationResult;
        }

        public ValidationResult ValidateDataRelations(Avfallrapport avfallrapport)
        {
            if (string.IsNullOrEmpty(avfallrapport?.MengdeFraksjon))
                AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.mengdeFraksjon, new[] { avfallrapport?.Avfallfraksjon.Kodebeskrivelse });
            else
            {
                if (decimal.TryParse(avfallrapport.MengdeFraksjon, out var mengdeResult))
                {
                    if (mengdeResult < 0)
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.mengdeFraksjon, new[] { avfallrapport?.Avfallfraksjon.Kodebeskrivelse });
                    }
                }
                else
                {
                    AddMessageFromRule(ValidationRuleEnum.numerisk, FieldNameEnum.mengdeFraksjon, new[] { avfallrapport?.Avfallfraksjon.Kodebeskrivelse });
                }
            }

            // Avfallklasse
            var avfallsklasseValidationResult = _avfallsklasseValidator.Validate(avfallrapport?.Avfallklasse, avfallrapport?.Avfallfraksjon.Kodebeskrivelse);
            UpdateValidationResultWithSubValidations(avfallsklasseValidationResult);

            var avfallsklasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallsklasseValidator._entityXPath, null, FieldNameEnum.kodeverdi);
            if (!IsAnyValidationMessagesWithXpath(avfallsklasseKodeverdiXpath) && !IsAnyValidationMessagesWithXpath(_avfallsklasseValidator._entityXPath))
            {
                if (avfallrapport.Avfallfraksjon.Kodeverdi.StartsWith("1") && !avfallrapport.Avfallklasse.Kodeverdi.Equals("ordinaertAvfall", StringComparison.CurrentCultureIgnoreCase))
                    AddMessageFromRule(ValidationRuleEnum.Stemmer, avfallsklasseKodeverdiXpath, new[] { avfallrapport.Avfallfraksjon.Kodebeskrivelse, "'Ordinært avfall'" });
                if (avfallrapport.Avfallfraksjon.Kodeverdi.StartsWith("7") && !avfallrapport.Avfallklasse.Kodeverdi.Equals("farligAvfall", StringComparison.CurrentCultureIgnoreCase))
                    AddMessageFromRule(ValidationRuleEnum.Stemmer, avfallsklasseKodeverdiXpath, new[] { avfallrapport.Avfallfraksjon.Kodebeskrivelse, "'Farlig avfall'" });
                if (avfallrapport.Avfallfraksjon.Kodeverdi.Equals("9912") && !avfallrapport.Avfallklasse.Kodeverdi.Equals("blandetAvfall", StringComparison.CurrentCultureIgnoreCase))
                    AddMessageFromRule(ValidationRuleEnum.Stemmer, avfallsklasseKodeverdiXpath, new[] { avfallrapport.Avfallfraksjon.Kodebeskrivelse, "'Blandet avfall'" });

                if (!IsAnyValidationMessagesWithXpath(avfallsklasseKodeverdiXpath))
                {
                    //disponeringsmaate
                    var avfalldisponeringValidationResult = _avfalldisponeringValidator.Validate(avfallrapport?.Avfalldisponering, avfallrapport?.Avfallfraksjon?.Kodebeskrivelse);
                    UpdateValidationResultWithSubValidations(avfalldisponeringValidationResult);
                }

            }
            return _validationResult;
        }
        public ValidationResult ValidateEntityFields(Avfallrapport avfallrapport)
        {

            return _validationResult;
        }
    }
}
