﻿using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using System;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Checklist;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class SjekklistekravValidator : EntityValidatorBase, ISjekklistekravValidator
    {
        private readonly ICodeListService _codeListService;

        private readonly IKodelisteValidator _sjekklistepunktValidator;

        public ValidationResult ValidationResult { get => _validationResult; }

        public SjekklistekravValidator(IList<EntityValidatorNode> entityValidatorTree, IKodelisteValidator sjekklistepunktValidator, ICodeListService codeListService)
            : base(entityValidatorTree)
        {
            _codeListService = codeListService;
            _sjekklistepunktValidator = sjekklistepunktValidator;
        }

        protected override void InitializeValidationRules()
        {
            this.AddValidationRule(ValidationRuleEnum.utfylt);
            this.AddValidationRule(ValidationRuleEnum.gyldig);
            this.AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.sjekklistepunktsvar);
            this.AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.dokumentasjon);
        }

        public ValidationResult Validate(string processCategory, ServiceDomain serviceDomain, Sjekklistekrav[] formsSjekkliste, IChecklistService checklistService, List<string> tiltakstyperISkjema)
        {
            try
            {
                if (Helpers.ObjectIsNullOrEmpty(formsSjekkliste) || formsSjekkliste.Count() == 0)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, "", new string[] { "fra sjekklisten" });
                }
                else
                {
                    var index = GetArrayIndex(formsSjekkliste);

                    for (int i = 0; i < index; i++)
                    {
                        var sjekkliste = Helpers.ObjectIsNullOrEmpty(formsSjekkliste) ? null : formsSjekkliste[i];
                        var sjekklistepunktValidationResult = _sjekklistepunktValidator.Validate(sjekkliste?.Sjekklistepunkt);
                        UpdateValidationResultWithSubValidations(sjekklistepunktValidationResult, i);
                    }

                    //Note: Validates 1.18 in Form validator
                    //Validate if all checkpoints in Sjekklisten are present in form

                    var AtilSpesificCheckpointsFromAPI = checklistService.GetChecklist(processCategory, serviceDomain, "metadataid=1");
                    //var checkpointsFromAPI = checklistService.GetChecklist(dataFormatVersion);

                    if (!AtLeastOneKravIsRelevant(AtilSpesificCheckpointsFromAPI, formsSjekkliste))
                    {
                        AddMessageFromRule(ValidationRuleEnum.gyldig);
                        //throw new ArgumentNullException($"Add validationmessage for not any relevant checklistanswers");
                    }

                    if (AtilSpesificCheckpointsFromAPI.Count() == 0)
                    {
                        throw new ArgumentNullException($"Zero (0) checkpoints from Checklist service");
                    }
                    else
                    {
                        foreach (var checkpoint in AtilSpesificCheckpointsFromAPI)
                        {
                            var tiltakstyperISjekkpunkt = checkpoint.Tiltakstyper.Select(x => x.Kode).ToList();
                            var tiltakstypeIsRelevant = tiltakstyperISkjema.Intersect(tiltakstyperISjekkpunkt).Count() > 0;

                            if (tiltakstypeIsRelevant)
                            {
                                ValidateCheckpoint(formsSjekkliste, checkpoint, tiltakstyperISkjema);
                            }
                        }
                    }
                }

                return _validationResult;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool AtLeastOneKravIsRelevant(IEnumerable<Sjekk> alleArbeidstilsynetskrav, Sjekklistekrav[] formsSjekkliste)
        {
            foreach (var krav in alleArbeidstilsynetskrav)
            {
                if (KravIsRelevant(krav, formsSjekkliste))
                {
                    return true;
                }
            }

            return false;
        }

        private bool KravIsRelevant(Sjekk ettAvArbeidstilsynetsKrav, Sjekklistekrav[] formsSjekkliste)
        {
            if (ettAvArbeidstilsynetsKrav.Utfall == null)
            {
                return false;
            }
            var utfallTypeDOKWithUtfallsTekst = ettAvArbeidstilsynetsKrav.Utfall.Where(x => (x.Utfalltypekode.Equals("DOK") && x.Utfalltekst.Tittel != null) || x.Utfalltypekode.Equals("VAS"));
            if (utfallTypeDOKWithUtfallsTekst.ToList().Count == 0)
            {
                return false;
            }
            else
            {
                var kravInXML = formsSjekkliste.FirstOrDefault(x => x.Sjekklistepunkt.Kodeverdi.Equals(ettAvArbeidstilsynetsKrav.Id));
                if (kravInXML == null)
                {
                    return false;
                }
                else
                {
                    // Ja, det er et krav... KravIsRelevant burde vært satt til true?

                    // og så sjekker vi hva som er svart...
                    var answerTrueFalse = kravInXML.Sjekklistepunktsvar.HasValue && (bool)kravInXML.Sjekklistepunktsvar;

                    foreach (var utfall in utfallTypeDOKWithUtfallsTekst)
                    {
                        if (utfall.Utfallverdi == answerTrueFalse)
                        {
                            return true;
                        }
                        //Else - answerTrueFalse er false, dvs at det ikke er svar i søknaden? 
                        // Mangler det en Feilmelding på at spørsmålet ikke har blitt besvart?
                    }
                }
            }

            foreach (var krav in ettAvArbeidstilsynetsKrav.Undersjekkpunkter)
            {
                var kravIsRelevant = KravIsRelevant(krav, formsSjekkliste);
                if (kravIsRelevant)
                {
                    return true;
                }
            }

            return false;
        }

        private void ValidateCheckpoint(Sjekklistekrav[] formsSjekkliste, Sjekk sjekklistepunkt, List<string> tiltakstyperISkjema)
        {
            if (sjekklistepunkt.Tiltakstyper.Any(x => tiltakstyperISkjema.Contains(x.Kode)))
            {
                if (sjekklistepunkt.Metadata.Count > 0
                    && sjekklistepunkt.Utfall != null
                    && SjekklistepunktFinnesOgErBesvart(formsSjekkliste, sjekklistepunkt.Id)
                   )
                {
                    string yesOutcome = "";
                    string noOutcome = "";
                    Utfall yesAction;
                    Utfall noAction;

                    string outcome = "";
                    Utfall action;

                    try
                    {
                        yesAction = sjekklistepunkt.Utfall.Where(x => x.Utfallverdi.Equals(true)).FirstOrDefault();
                        if (yesAction != null)
                        {
                            yesOutcome = yesAction.Utfalltypekode;
                        }

                        noAction = sjekklistepunkt.Utfall.Where(x => x.Utfallverdi.Equals(false)).FirstOrDefault();
                        if (noAction != null)
                        {
                            noOutcome = noAction.Utfalltypekode;
                        }
                    }
                    catch (Exception ie)
                    {
                        throw new ApplicationException($"Missing Utfall in checklist number {sjekklistepunkt.Id}", ie);
                    }


                    if (SjekklistepunktBesvartMedJa(formsSjekkliste, sjekklistepunkt.Id))
                    {
                        action = yesAction;
                        outcome = yesOutcome;
                    }
                    else
                    {
                        action = noAction;
                        outcome = noOutcome;
                    }

                    if (action != null && outcome.Equals("DOK"))
                    {
                        SjekklistepunktDokumentasjonFinnes(formsSjekkliste, sjekklistepunkt.Id);
                    }
                    else if (action != null && outcome.Equals("SU"))
                    {
                        foreach (var undersjekkpunkt in sjekklistepunkt.Undersjekkpunkter)
                        {
                            ValidateCheckpoint(formsSjekkliste, undersjekkpunkt, tiltakstyperISkjema);
                        }
                    }
                }
            }
        }

        private bool SjekklistepunktBesvartMedJa(Sjekklistekrav[] kravliste, string sjekklistepunktnr)
        {
            var kravEntity = kravliste.FirstOrDefault(x => x.Sjekklistepunkt.Kodeverdi.Equals(sjekklistepunktnr));
            //var xPath = kravEntity.DataModelXpath;
            if (kravEntity != null)
            {
                return (bool)kravEntity.Sjekklistepunktsvar;
            }
            else
            {
                throw new ArgumentNullException($"Checklist number {sjekklistepunktnr} doesn't exist.");
            }
        }

        private bool SjekklistepunktDokumentasjonFinnes(Sjekklistekrav[] kravliste, string sjekklistepunktnr)
        {
            var kravEntity = kravliste.FirstOrDefault(x => x.Sjekklistepunkt.Kodeverdi.Equals(sjekklistepunktnr));
            if (kravEntity != null)
            {
                int arrayElementId = Array.IndexOf(kravliste, kravEntity);

                if (string.IsNullOrEmpty(kravEntity.Dokumentasjon))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.dokumentasjon, new string[] { sjekklistepunktnr }, null, arrayElementId);

                    return false;
                }
                return true;
            }
            else
            {
                throw new ArgumentNullException($"Checklist number {sjekklistepunktnr} doesn't exist.");
            }
        }


        private bool SjekklistepunktFinnesOgErBesvart(Sjekklistekrav[] formsKravliste, string sjekklistepunktnr)
        {
            var kravEntity = formsKravliste.FirstOrDefault(x => x.Sjekklistepunkt.Kodeverdi.Equals(sjekklistepunktnr));
            if (kravEntity == null)
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, EntityXPath, new string[] { sjekklistepunktnr });
                return false;
            }
            else
            {
                var kravet = kravEntity;
                if (kravet.Sjekklistepunktsvar == null)
                {
                    int arrayElementId = Array.IndexOf(formsKravliste, kravEntity);
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.sjekklistepunktsvar, new string[] { sjekklistepunktnr }, null, arrayElementId);

                    return false;
                }

                //TODO: Maybe not: Use sjekklistepunktValidator to verfy correct description from GeoNorge ?? Necessary ???
                //Here......
            }

            return true;
        }
    }
}
