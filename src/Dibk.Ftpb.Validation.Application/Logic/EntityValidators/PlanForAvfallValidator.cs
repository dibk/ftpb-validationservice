﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class PlanForAvfallValidator : EntityValidatorBase, IPlanForAvfallValidator
    {
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }
        private readonly IAvfallplanValidator _avfallplanValidator;
        private readonly IAvfallsorteringValidator _avfallsorteringValidator;
        private readonly IAvfallklasseDelsumValidator _avfallklasseDelsumValidator;

        public PlanForAvfallValidator(IList<EntityValidatorNode> entityValidatorTree,
                                        IAvfallplanValidator avfallplanValidator,
                                        IAvfallsorteringValidator avfallsorteringValidator,
                                        IAvfallklasseDelsumValidator avfallklasseDelsumValidator)
            : base(entityValidatorTree)
        {
            _avfallplanValidator = avfallplanValidator;
            _avfallsorteringValidator = avfallsorteringValidator;
            _avfallklasseDelsumValidator = avfallklasseDelsumValidator;
            AddValidateDataRelationsRules();
        }

        private void AddValidateDataRelationsRules()
        {
            AccumulateValidationRules(_avfallplanValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_avfallsorteringValidator.ValidationResult.ValidationRules);
            AccumulateValidationRules(_avfallklasseDelsumValidator.ValidationResult.ValidationRules);

            var planAvfallsklasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallplanValidator._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.AvfallklasseValidator);

            var delsumAvfallsklasseXpath = Helpers.GetEntityXpath(_avfallklasseDelsumValidator._entityXPath, null, null, EntityValidatorEnum.AvfallklasseValidator);
            var delsumAvfallsklasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallklasseDelsumValidator._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.AvfallklasseValidator);

            AddValidationRule(ValidationRuleEnum.tillatt, null, delsumAvfallsklasseKodeverdiXpath);
            AddValidationRule(ValidationRuleEnum.finnes, null, delsumAvfallsklasseXpath);

            AddValidationRule(ValidationRuleEnum.BlandetAvfall, null, planAvfallsklasseKodeverdiXpath);
            AddValidationRule(ValidationRuleEnum.FarligAvfallOgEllerOrdinaertAvfall, null, planAvfallsklasseKodeverdiXpath);

        }
        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt);
        }

        public ValidationResult Validate(PlanForAvfall planForAvfall = null, Overgangsordning overgangsordning = null, string[] attachmentsTypesName = null)
        {
            ValidateEntityFields(planForAvfall);
            if (!Helpers.ObjectIsNullOrEmpty(planForAvfall))
            {
                var avfallplanArray = planForAvfall?.Avfall;
                var index = GetArrayIndex(avfallplanArray);
                for (int i = 0; i < index; i++)
                {
                    var avfallplan = Helpers.ObjectIsNullOrEmpty(avfallplanArray) ? null : avfallplanArray[i];
                    var avfallplanValidationResult = _avfallplanValidator.Validate(avfallplan);
                    UpdateValidationResultWithSubValidations(avfallplanValidationResult, i);
                }

                ValidateDataRelations(planForAvfall, overgangsordning, attachmentsTypesName);
            }

            return _validationResult;
        }
        public ValidationResult ValidateDataRelations(PlanForAvfall planForAvfall, Overgangsordning overgangsordning = null, string[] attachmentsTypesName = null)
        {
            var planAvfallsklasseXpath = Helpers.GetEntityXpath(_avfallplanValidator._entityXPath, null, null, EntityValidatorEnum.AvfallklasseValidator);
            var planAvfallsklasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallplanValidator._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.AvfallklasseValidator); ;

            var planFraksjonXpath = Helpers.GetEntityXpath(_avfallplanValidator._entityXPath, null, null, EntityValidatorEnum.AvfallfraksjonValidator);
            var planFraksjonKodeverdiXpath = Helpers.GetEntityXpath(_avfallplanValidator._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.AvfallfraksjonValidator);

            var isAnyErrorForAvfallklasse = IsAnyValidationMessagesWithXpath(planAvfallsklasseXpath) || IsAnyValidationMessagesWithXpath(planAvfallsklasseKodeverdiXpath);
            var isAnyErrorForFraksjon = IsAnyValidationMessagesWithXpath(planFraksjonXpath) || IsAnyValidationMessagesWithXpath(planFraksjonKodeverdiXpath);


            if (!isAnyErrorForFraksjon && !isAnyErrorForAvfallklasse && !IsAnyValidationMessagesWithXpath(_avfallplanValidator._entityXPath))
            {
                var avfallPlanAvfallKlasseArray = planForAvfall.Avfall.Where(k => !string.IsNullOrEmpty(k.Avfallklasse.Kodeverdi))
                    .Select(k => k.Avfallklasse.Kodeverdi)?.Distinct()?.ToArray();

                if (!avfallPlanAvfallKlasseArray.Contains("blandetAvfall"))
                {
                    AddMessageFromRule(ValidationRuleEnum.BlandetAvfall, planAvfallsklasseKodeverdiXpath);
                }
                else if (!avfallPlanAvfallKlasseArray.Contains("ordinaertAvfall") && !avfallPlanAvfallKlasseArray.Contains("farligAvfall"))
                {
                    AddMessageFromRule(ValidationRuleEnum.FarligAvfallOgEllerOrdinaertAvfall, planAvfallsklasseKodeverdiXpath);
                }
                else
                {
                    //Delsum AvfallsKlasse validerering
                    var avfallsklasseDelsumArray = planForAvfall?.AvfallsklasseDelsum;
                    var index = GetArrayIndex(avfallsklasseDelsumArray);
                    for (int i = 0; i < index; i++)
                    {
                        var avfallsklasseDelsum = Helpers.ObjectIsNullOrEmpty(avfallsklasseDelsumArray) ? null : avfallsklasseDelsumArray[i];
                        var avfallsklasseDelsumValidationResult = _avfallklasseDelsumValidator.Validate(avfallsklasseDelsum);
                        UpdateValidationResultWithSubValidations(avfallsklasseDelsumValidationResult, i);
                    }

                    var avfallsklassedelsumAvfallKlasse = Helpers.GetEntityXpath(_avfallklasseDelsumValidator._entityXPath, null, null, EntityValidatorEnum.AvfallklasseValidator);
                    var avfallsklassedelsumAvfallKlasseKodeverdiXpath = Helpers.GetEntityXpath(_avfallklasseDelsumValidator._entityXPath, null, FieldNameEnum.kodeverdi, EntityValidatorEnum.AvfallklasseValidator); ;

                    var isAnyErrorForDelsumAvfallklasse = IsAnyValidationMessagesWithXpath(avfallsklassedelsumAvfallKlasse) || IsAnyValidationMessagesWithXpath(avfallsklassedelsumAvfallKlasseKodeverdiXpath);


                    if (!isAnyErrorForDelsumAvfallklasse && !IsAnyValidationMessagesWithXpath(_avfallklasseDelsumValidator._entityXPath))
                    {
                        var delsumAvfallKlasseArray = planForAvfall.AvfallsklasseDelsum.Where(k => !string.IsNullOrEmpty(k.Avfallsklasse.Kodeverdi))
                            .Select(k => k.Avfallsklasse.Kodeverdi)?.Distinct()?.ToArray();

                        var delsumAvfallKlasseCount = delsumAvfallKlasseArray.Length;
                        var planAvfallKlasseCount = avfallPlanAvfallKlasseArray.Length;
                        if (delsumAvfallKlasseCount != planAvfallKlasseCount)
                        {
                            AddMessageFromRule(ValidationRuleEnum.tillatt, planAvfallsklasseKodeverdiXpath, new string[] { planAvfallKlasseCount.ToString(), string.Join(", ", avfallPlanAvfallKlasseArray), delsumAvfallKlasseCount.ToString(), string.Join(", ", delsumAvfallKlasseArray) });
                        }
                        else
                        {
                            var avfallKlasseExcept = avfallPlanAvfallKlasseArray.Except(delsumAvfallKlasseArray);

                            if (avfallKlasseExcept.Any())
                            {
                                foreach (var avfallKlasse in avfallKlasseExcept)
                                {
                                    var avfallrapportKlasse = planForAvfall.Avfall.FirstOrDefault(r => r.Avfallklasse?.Kodeverdi == avfallKlasse);
                                    AddMessageFromRule(ValidationRuleEnum.finnes, avfallsklassedelsumAvfallKlasse, new[] { avfallrapportKlasse?.Avfallklasse?.Kodebeskrivelse });
                                }
                            }
                        }
                    }
                }

                //plan/sortering
                var avfallsorteringValidationResult = _avfallsorteringValidator.Validate(planForAvfall?.Avfallsortering, overgangsordning, attachmentsTypesName);
                UpdateValidationResultWithSubValidations(avfallsorteringValidationResult);
            }

            return _validationResult;
        }
        public ValidationResult ValidateEntityFields(PlanForAvfall planForAvfall = null)
        {
            if (Helpers.ObjectIsNullOrEmpty(planForAvfall))
                AddMessageFromRule(ValidationRuleEnum.utfylt);

            return _validationResult;
        }
    }
}
