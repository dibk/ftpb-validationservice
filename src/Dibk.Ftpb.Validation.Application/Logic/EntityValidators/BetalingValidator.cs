﻿using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.AtilFee;
using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class BetalingValidator : EntityValidatorBase
    {
        private readonly IAtilFeeCalculatorService _atilFeeCalculatorService;
        public ValidationResult ValidationResult { get => _validationResult; set => throw new System.NotImplementedException(); }

        public BetalingValidator(IList<EntityValidatorNode> entityValidatorTree, IAtilFeeCalculatorService atilFeeCalculatorService )
            : base(entityValidatorTree)
        {
            _atilFeeCalculatorService = atilFeeCalculatorService;
        }

        public ValidationResult Validate(Betaling betaling, string[] tiltakstyper, string bygningstype, string BRA)
        {
            ValidateEntityFields(betaling);
            if (!Helpers.ObjectIsNullOrEmpty(betaling) && !Helpers.ObjectIsNullOrEmpty(tiltakstyper) && !Helpers.ObjectIsNullOrEmpty(bygningstype) && !Helpers.ObjectIsNullOrEmpty(BRA))
            {

                var isNumeric = int.TryParse(BRA, out int numericBRA);
                var isFloat = float.TryParse(BRA, out float floatBRA);
                if (isNumeric || isFloat)
                {
                    FeeValidation(betaling, tiltakstyper, bygningstype, BRA);
                }
            }
            return _validationResult;
        }

        protected override void InitializeValidationRules()
        {
            this.AddValidationRule(ValidationRuleEnum.utfylt);
            this.AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.beskrivelse);
            this.AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.sum);
            this.AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.sum);
            this.AddValidationRule(ValidationRuleEnum.numerisk, FieldNameEnum.sum);
            this.AddValidationRule(ValidationRuleEnum.tillatt, FieldNameEnum.sum);
            this.AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.gebyrkategori);
            this.AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.gebyrkategori);
            this.AddValidationRule(ValidationRuleEnum.utfylt, FieldNameEnum.skalFaktureres);
            this.AddValidationRule(ValidationRuleEnum.gyldig, FieldNameEnum.skalFaktureres);
        }

        public void ValidateEntityFields(Betaling betaling)
        {
            if (Helpers.ObjectIsNullOrEmpty(betaling))
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt);
            }
            else
            {
                if (string.IsNullOrEmpty(betaling.Beskrivelse))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.beskrivelse);
                }

                if (betaling.SkalFaktureres == null)
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.skalFaktureres);
                }
                else if (!(bool)betaling.SkalFaktureres)
                {
                    AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.skalFaktureres);
                }


                if (string.IsNullOrEmpty(betaling.Gebyrkategori))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.gebyrkategori);
                }

                if (string.IsNullOrEmpty(betaling.Sum))
                {
                    AddMessageFromRule(ValidationRuleEnum.utfylt, FieldNameEnum.sum);
                }
                else
                {
                    var isNumeric = int.TryParse(betaling.Sum, out int numericAmount);
                    if (!isNumeric)
                    {
                        AddMessageFromRule(ValidationRuleEnum.numerisk, FieldNameEnum.sum);
                    }
                    else
                    {
                        if (numericAmount <= 0)
                        {
                            AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.sum);
                        }
                    }
                }
            }
        }

        public void FeeValidation(Betaling betaling, string[] tiltakstyper, string bygningstype, string BRA)
        {
            if (!Helpers.ObjectIsNullOrEmpty(betaling))
            {
                if (!string.IsNullOrEmpty(betaling.Gebyrkategori) && !string.IsNullOrEmpty(betaling.Sum))
                {
                    var isNumeric = int.TryParse(betaling.Sum, out int numericFeeFromApplication);
                    if (isNumeric)
                    {
                        AtilFeeCalculationBasis largestFeeType = new() { FeeAmount = 0 };
                        foreach (var tiltakstype in tiltakstyper)
                        {
                            if (tiltakstype != null)
                            {
                                var feeInformation = AsyncHelper.RunSync(() => _atilFeeCalculatorService.GetFeeInformationAsync(tiltakstype, bygningstype, BRA));

                                if (feeInformation != null && feeInformation.FeeAmount >= largestFeeType.FeeAmount)
                                {
                                    largestFeeType.FeeAmount = feeInformation.FeeAmount;
                                    largestFeeType.FeeCategory = feeInformation.FeeCategory;
                                }
                            }
                        }

                        if (numericFeeFromApplication != largestFeeType.FeeAmount)
                        {
                            AddMessageFromRule(ValidationRuleEnum.tillatt, FieldNameEnum.sum, new string[] { largestFeeType.FeeAmount.ToString() });
                        }
                        if (betaling.Gebyrkategori != largestFeeType.FeeCategory)
                        {
                            AddMessageFromRule(ValidationRuleEnum.gyldig, FieldNameEnum.gebyrkategori, new string[] { largestFeeType.FeeCategory });
                        }
                    }
                }
            }
        }

    }
}
