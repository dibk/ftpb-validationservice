﻿using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.Logic.EntityValidators
{
    public class OvergangsordningValidator : EntityValidatorBase
    {
        public string _entityXPath { get => base.EntityXPath; }
        public ValidationResult ValidationResult { get => _validationResult; set => throw new NotImplementedException(); }

        private DateTime foerOvergangsordning = new DateTime(2022, 7, 1);
        private DateTime etterOvergangsordning = new DateTime(2023, 6, 30);

        public OvergangsordningValidator(IList<EntityValidatorNode> entityValidatorTree) : base(entityValidatorTree)
        {

        }

        protected override void InitializeValidationRules()
        {
            AddValidationRule(ValidationRuleEnum.utfylt, Enums.FieldNameEnum.datoForHovedsoeknad);
            AddValidationRule(ValidationRuleEnum.utfylt, Enums.FieldNameEnum.overgangsbestemmelse);
        }

        public ValidationResult Validate(Overgangsordning overgangsordning)
        {
            
            if (Helpers.ObjectIsNullOrEmpty(overgangsordning) || !overgangsordning.DatoForHovedsoeknad.HasValue)
            {
                AddMessageFromRule(ValidationRuleEnum.utfylt, Enums.FieldNameEnum.datoForHovedsoeknad);
            }
            else
            {
                if (foerOvergangsordning <= overgangsordning.DatoForHovedsoeknad && overgangsordning.DatoForHovedsoeknad <= etterOvergangsordning)
                {
                    if (!overgangsordning.Overgangsbestemmelse.HasValue)
                    {
                        AddMessageFromRule(ValidationRuleEnum.utfylt, Enums.FieldNameEnum.overgangsbestemmelse);
                    }
                }
            }

            return _validationResult;
        }

    }
}
