﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dibk.Ftpb.Validation.Application.Enums;

namespace Dibk.Ftpb.Validation.Application.Utils
{
    public class Debugging
    {
        public static string GetRuleXpathWithoutFirstNode(string reference)
        {
            var xpath = GetRuleXpathWithoutFirstNodeArray(reference) != null ? string.Join("/", GetRuleXpathWithoutFirstNodeArray(reference)) : null;
            return xpath;
        }
        public static string[] GetRuleXpathWithoutFirstNodeArray(string reference)
        {
            string[] ruleIdNumber = new string[] { };
            var validationEnumsNumber = reference?.Split("/");

            return validationEnumsNumber?.Skip(1).ToArray();
        }
        public static string[] GetRuleIdNumberArray(string reference)
        {
            string[] ruleIdNumber = new string[] { };

            var validationEnumsNumber = reference.Split(".");
            if (int.TryParse(validationEnumsNumber[0], out int number))
            {
                ruleIdNumber = number >= 1000 ? validationEnumsNumber.Skip(2).ToArray() : validationEnumsNumber;
            }

            return ruleIdNumber;
        }

        public static string GetRuleIdNumber(string reference)
        {
            var ruleIdNumber = string.Join(".", GetRuleIdNumberArray(reference));
            return ruleIdNumber;
        }

        public static string DebugValidatorFormReference(string reference)
        {
            var validatorPath = string.Empty;

            if (reference.Contains("."))
            {
                var ruleIdNumber = Debugging.GetRuleIdNumberArray(reference);

                var index = ruleIdNumber.Length;

                for (int i = 0; i < index; i++)
                {
                    if (int.TryParse(ruleIdNumber[i], out int enumNumber))
                    {
                        string validatorText;
                        string stringValue;

                        if (index == i + 2)
                        {
                            var validatorEnum = EnumHelper.GetEnumFromFieldNameId(enumNumber.ToString());
                            stringValue = validatorEnum.ToString();
                        }
                        else if (index == i + 1)
                        {
                            var validatorEnum = EnumHelper.GetEnumFromValidationRuleId(enumNumber.ToString());
                            stringValue = validatorEnum.ToString();
                        }
                        else
                        {
                            var validatorEnum = EnumHelper.GetEnumFromValidationId(enumNumber.ToString());
                            stringValue = validatorEnum.ToString();
                        }

                        if (!int.TryParse(stringValue, out int theNumber))
                        {
                            validatorText = ruleIdNumber.Length == i + 1 ? $" rule: {stringValue}" : $"/{stringValue}";
                        }
                        else
                        {
                            validatorText = $"/Enum:'{theNumber}'";
                        }

                        validatorPath = $"{validatorPath}{validatorText}";
                    }
                }
            }
            return validatorPath;
        }

    }
}
