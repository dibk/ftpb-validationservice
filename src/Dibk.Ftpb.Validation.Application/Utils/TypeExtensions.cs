﻿using System;
using System.Collections;

namespace Dibk.Ftpb.Validation.Application.Utils
{
    public static class TypeExtensions
    {
        public static bool IsEnumerableType(this Type type)
        {
            return type.GetInterface(nameof(IEnumerable)) != null;
        }

        public static bool IsListType(this Type type)
        {
            return type.GetInterface(nameof(IList)) != null;
        }
    }
}