﻿using Dibk.Ftpb.Validation.Application.Enums;
using System;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;

namespace Dibk.Ftpb.Validation.Application.Utils
{
    public class Helpers
    {
        /// <summary>
        /// check if the object and properties are empty
        /// it doesn't work with the new mapping structure because the "ModelData" and "DataModelXpath" properties always have data
        /// </summary>
        /// <param name="mainObject"></param>
        /// <returns></returns>
        public static bool ObjectIsNullOrEmpty(object mainObject)
        {
            if (mainObject == null) return true;

            // Simple types
            if (IsSimple(mainObject.GetType()))
            {
                var stringValue = mainObject.ToString();
                return string.IsNullOrEmpty(stringValue);
            }

            //Complex types - Arrays
            if (mainObject.GetType().IsArray || mainObject.GetType().IsEnumerableType() || mainObject.GetType().IsListType())
            {
                foreach (dynamic objectItem in (IEnumerable)mainObject)
                {
                    var isNullOrEmpty = ObjectIsNullOrEmpty(objectItem);
                    if (!isNullOrEmpty)
                        return false;
                }
                return true;
            }

            //Complex type - Child properties
            if (mainObject.GetType().GetProperties().Any())
            {
                // TODO test this, returns false when in xml all nodes are "xsi:nil="true" "
                var props = mainObject.GetType().GetProperties().Where(p => !p.Name.EndsWith("Specified", StringComparison.CurrentCultureIgnoreCase) && !p.Name.Equals("DataModelXpath", StringComparison.CurrentCultureIgnoreCase));

                foreach (var propertyInfo in props)
                {
                    var isNullOrEmpty = ObjectIsNullOrEmpty(propertyInfo.GetValue(mainObject, null));
                    if (!isNullOrEmpty)
                        return false;
                }
            }

            return true;
        }

        private static bool IsSimple(Type type)
        {
            // https://stackoverflow.com/questions/863881/how-do-i-tell-if-a-type-is-a-simple-type-i-e-holds-a-single-value
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                // nullable type, check if the nested type is simple.
                return IsSimple(type.GetGenericArguments()[0]);
            }
            return type.IsPrimitive
                   || type.IsEnum
                   || type == typeof(string)
                   || type == typeof(decimal)
                   // The date and time is not simple but is added to the method
                   || type == typeof(DateTime);
        }

        public static string ReplaceCurlyBracketInXPath(int index, string xPath)
        {
            string newXPath = null;
            if (!string.IsNullOrEmpty(xPath))
            {
                //Match sText = Regex.Match(xPath, "{([0-9])+}");

                var searchText = "{0}";
                int lastIndex = xPath.LastIndexOf(searchText);
                if (lastIndex >= 0)
                {
                    newXPath = xPath.Remove(lastIndex, searchText.Length).Insert(lastIndex, $"[{index}]");
                }
                else
                {
                    newXPath = xPath;
                }
            }
            return newXPath;
        }

        public static string GetEntityXpath(string rootXpath, int? index = null, FieldNameEnum? fieldName = null, EntityValidatorEnum? subValidatorEnum = null)
        {
            var newXpath = rootXpath;
            newXpath = subValidatorEnum.HasValue ? newXpath + $"/{EnumHelper.GetEnumXmlNodeFromEntityValidatorEnum(subValidatorEnum.Value)}" : newXpath;
            newXpath = fieldName.HasValue ? newXpath + $"/{EnumHelper.GetXmlNodeFromFieldNameEnum(fieldName)}" : newXpath;
            newXpath = index.HasValue ? Helpers.ReplaceCurlyBracketInXPath(index.Value, newXpath) : newXpath;

            return newXpath;
        }

        public static readonly Regex IsValidEmail = new(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", RegexOptions.Compiled);
    }
}