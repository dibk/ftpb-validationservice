using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using System;
using System.Linq;
using System.Reflection;

namespace Dibk.Ftpb.Validation.Application.Utils
{
    public class EnumHelper
    {
        public static ValidationRuleEnum GetEnumFromValidationRuleId(string validationRuleTypeId)
        {

            ValidationRuleEnum myEnum;
            Enum.TryParse(validationRuleTypeId, out myEnum);

            return myEnum;
        }

        public static string GetRuleNumberFromValidationRuleEmum(ValidationRuleEnum value)
        {
            int enumIndex = (int)value;

            return enumIndex.ToString();
        }

        public static FieldNameEnum GetEnumFromFieldNameId(string FieldNameId)
        {
            FieldNameEnum myEnum;
            Enum.TryParse(FieldNameId, out myEnum);

            return myEnum;
        }
        public static string GetXmlNodeFromFieldNameEnum(FieldNameEnum? fieldName)
        {
            var xmlNode = string.Empty;
            if (!fieldName.HasValue) return xmlNode;

            FieldInfo fieldInfo = fieldName.GetType().GetField(fieldName?.ToString());

            if (fieldInfo?.GetCustomAttributes(typeof(FieldNameEnumerationAttribute)) is FieldNameEnumerationAttribute[] enumerationAttributes && enumerationAttributes.Any())
                xmlNode = enumerationAttributes.FirstOrDefault()?.XmlNode;
            else
                xmlNode = fieldName.ToString();

            return xmlNode;
        }

        public static string GetEnumFieldNameNumber(FieldNameEnum fieldNameEnum)
        {
            int fieldNameId = (int)fieldNameEnum;
            return fieldNameId.ToString();
        }

        public static string GetEnumAttachmentNumber(AttachmentEnum attachmentEnum)
        {
            int fieldNameId = (int)attachmentEnum;
            return fieldNameId.ToString();
        }

        public static string GetEnumAktoerNumber(AktoerEnum aktoerEnum)
        {
            int fieldNameId = (int)aktoerEnum;
            return fieldNameId.ToString();
        }

        //https://stackoverflow.com/a/4367868
        public static EntityValidatorEnum GetEnumFromValidationId(string validatorId)
        {

            EntityValidatorEnum entityValidator;
            Enum.TryParse(validatorId, out entityValidator);
            return entityValidator;
        }

        public static string GetEnumXmlNodeFromEntityValidatorEnum(EntityValidatorEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            var enumerationAttributes = fi.GetCustomAttributes(typeof(EntityValidatorEnumerationAttribute), false) as EntityValidatorEnumerationAttribute[];

            var xmlNode = string.Empty;
            if (enumerationAttributes != null && enumerationAttributes.Any())
            {
                xmlNode = enumerationAttributes.First().XmlNode;
            }

            return xmlNode;
        }

        public static string GetEnumEntityValidatorNumber(EntityValidatorEnum value)
        {
            int entityValidatorNumber = (int)value;
            return entityValidatorNumber.ToString();
        }

        public static string GetCodelistUrl(Enum value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
            string codelistUrl;

            if (fieldInfo?.GetCustomAttributes(typeof(CodelistEnumerationAttribute), false) is CodelistEnumerationAttribute[] enumerationAttributes && enumerationAttributes.Any())
            {
                codelistUrl = enumerationAttributes.First().CodelistUrl;
            }
            else
            {
                codelistUrl = value.ToString();
            }
            return codelistUrl;
        }
    }
}
