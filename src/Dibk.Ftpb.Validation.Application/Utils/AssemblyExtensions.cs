﻿using System.IO;
using System.Linq;
using System.Reflection;

namespace Dibk.Ftpb.Validation.Application.Utils
{
    public static class AssemblyExtensions
    {
        public static Stream GetResourceStream(this Assembly assembly, string fileName)
        {
            var name = assembly.GetManifestResourceNames().SingleOrDefault(n => n.EndsWith(fileName));

            return name != null ? assembly.GetManifestResourceStream(name) : null;
        }

        public static Stream GetResourceStream(this Assembly[] assemblies, string fileName)
        {
            Stream resourceStream = null;
            foreach (var assembly in assemblies)
            {
                var name = assembly.GetManifestResourceNames().SingleOrDefault(n => n.EndsWith(fileName));
                if (name != null)
                {
                    resourceStream = assembly.GetManifestResourceStream(name);
                    break;
                }
            }

            return resourceStream;
        }
    }
}