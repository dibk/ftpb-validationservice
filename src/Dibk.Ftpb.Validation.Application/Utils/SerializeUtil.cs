using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace Dibk.Ftpb.Validation.Application.Utils
{
    public class SerializeUtil
    {
        public static T DeserializeFromString<T>(string objectData, bool removeNamespaces = true)
        {
            return (T)DeserializeFromString(objectData, typeof(T), removeNamespaces);
        }
        private static object DeserializeFromString(string objectData, Type type, bool removeNamespaces)
        {
            object result;

            TextReader reader = null;
            try
            {
                var xmlString = removeNamespaces? RemoveAllNamespaces(objectData) : objectData;
                using var stringReader = new StringReader(xmlString);
                var serializer = new XmlSerializer(type);

                result = serializer.Deserialize(stringReader);
            }
            //catch (Exception ex)
            //{
            //    //throw new Exception("An error occurred", ex);
            //    return null;
            //}
            finally
            {
                reader?.Close();
            }

            return result;
        }
        public static string Serialize(object form)
        {
            var serializer = new System.Xml.Serialization.XmlSerializer(form.GetType());
            var stringWriter = new Utf8StringWriter();
            serializer.Serialize(stringWriter, form);
            return stringWriter.ToString();
        }


        //** Remove name spaceses 
        private static string RemoveAllNamespaces(string xmlDocument)
        {
            XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

            return xmlDocumentWithoutNs.ToString();
        }

        //Core recursion function
        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;

                foreach (XAttribute attribute in xmlDocument.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }

            var complexElement = new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));

            if (xmlDocument.Attributes().Any(a => a.Name == "dataFormatProvider"))
            {
                complexElement.Add(new XAttribute("dataFormatProvider", xmlDocument.Attribute("dataFormatProvider").Value));
                complexElement.Add(new XAttribute("dataFormatId", xmlDocument.Attribute("dataFormatId").Value));
                complexElement.Add(new XAttribute("dataFormatVersion", xmlDocument.Attribute("dataFormatVersion").Value));
            }

            return complexElement;
        }

        public static string RemoveNamespaces(string xmlString)
        {
            try
            {
                XDocument document = XDocument.Parse(RemoveAllNamespaces(xmlString));
                return document.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
