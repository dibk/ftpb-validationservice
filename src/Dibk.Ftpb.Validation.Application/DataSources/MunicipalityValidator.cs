﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Municipality;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Utils;
using System;

namespace Dibk.Ftpb.Validation.Application.DataSources
{
    public interface IMunicipalityValidator
    {
        MunicipalityStatus GetMunicipalityStatus(string kommunenummer);
    }

    public class MunicipalityStatus
    {
        public MunicipalityValidationEnum Status { get; set; }
        public string Message { get; set; }
    }

    public class MunicipalityValidator : IMunicipalityValidator
    {
        private readonly IMunicipalityApiService _municipalityApiService;

        public MunicipalityValidator(IMunicipalityApiService municipalityApiService)
        {
            _municipalityApiService = municipalityApiService;
        }

        public MunicipalityStatus GetMunicipalityStatus(string kommunenummer)
        {
            var result = new MunicipalityStatus();

            if (string.IsNullOrEmpty(kommunenummer))
            {
                result.Status = MunicipalityValidationEnum.Empty;
                result.Message = string.Empty;
            }
            else
            {
                var municipality = AsyncHelper.RunSync(() => _municipalityApiService.GetMunicipality(kommunenummer));

                if (municipality == null)
                {
                    result.Status = MunicipalityValidationEnum.Invalid;
                    result.Message = string.Empty;
                }
                else if (municipality.ValidTo.HasValue && DateTime.Now > municipality.ValidTo.Value)
                {
                    result.Status = MunicipalityValidationEnum.Expired;
                    result.Message = municipality.NewMunicipalityCode;
                }
                else if (municipality.ValidFrom.HasValue && DateTime.Now < municipality.ValidFrom.Value)
                {
                    result.Status = MunicipalityValidationEnum.TooSoon;
                    result.Message = municipality.ValidFrom.Value.ToShortDateString();
                }
                else
                {
                    result.Status = MunicipalityValidationEnum.Ok;
                    result.Message = "ok";
                }
            }
            return result;
        }
    }
}