﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.Models
{
    public class AtilFeeCalculationBasis
    {
        [JsonProperty("gebyrbeloep")]
        public int FeeAmount { get; set; }
        [JsonProperty("gebyrkategori")]
        public string FeeCategory { get; set; }
    }

    public class AtilTiltakstype
    {
        [JsonProperty("kode")]
        public string Code { get; set; }
        [JsonProperty("navn")]
        public string Name { get; set; }
        [JsonProperty("kategori")]
        public string Category { get; set; }
        [JsonProperty("tillaterZeroIAreal")]
        public string AllowZeroInBRA { get; set; }
    }
}
