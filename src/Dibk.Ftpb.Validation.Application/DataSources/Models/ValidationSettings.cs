﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.Models
{
    public class ValidationSettings
    {
        public string DevUrl { get; set; }
        public string TestUrl { get; set; }
        public string ProdUrl { get; set; }
        public string LocalhostUrl { get; set; }
    }
}
