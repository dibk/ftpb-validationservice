﻿namespace Dibk.Ftpb.Validation.Application.DataSources.Models
{
    public class CodeListItem
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }

        public CodeListItem(string name, string description, string status)
        {
            Name = name;
            Description = description;
            Status = status;
        }
    }
}
