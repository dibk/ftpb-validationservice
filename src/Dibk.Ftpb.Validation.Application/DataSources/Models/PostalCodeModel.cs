﻿namespace Dibk.Ftpb.Validation.Application.DataSources.Models
{
    public class PostalCodeModel
    {
        public PostalCodeModel(string postnummer, string poststed)
        {
            Postnummer = postnummer;
            Poststed = poststed;
        }

        public string Postnummer { get; set; }
        public string Poststed { get; set; }
    }
}