﻿namespace Dibk.Ftpb.Validation.Application.DataSources.Models
{
    public class PostalCodeValidationResult
    {
        public PostalCodeValidationResult()
        {
        }

        public PostalCodeValidationResult(string postnr, string poststed, bool gyldig)
        {
            Postnr = postnr;
            Poststed = poststed;
            Gyldig = gyldig;
        }

        public string Postnr { get; set; }

        public string Poststed { get; set; }

        public bool Gyldig { get; set; }
    }
}