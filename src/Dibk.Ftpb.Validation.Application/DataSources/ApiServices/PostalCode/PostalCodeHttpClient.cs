﻿using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode
{
    public class PostalCodeHttpClient
    {
        public HttpClient _httpClient;
        private readonly ILogger<PostalCodeHttpClient> _logger;

        public PostalCodeHttpClient(ILogger<PostalCodeHttpClient> logger, HttpClient httpClient)
        {
            _httpClient = httpClient;
            _logger = logger;
        }

        private async Task<string> FetchData()
        {
            try
            {
                using var request = new HttpRequestMessage(HttpMethod.Get, string.Empty);
                request.Headers.Add("Accept", "text/plain");

                using var response = await _httpClient.SendAsync(request);
                response.EnsureSuccessStatusCode();

                Stream stream = response.Content.ReadAsStream();

                return PostalCodeHelpers.GetUtf8StringFromStream(stream);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occured while requesting bring for postal code list");
                throw;
            }
        }

        public async Task<IEnumerable<PostalCodeModel>> GetPostalcodeList()
        {
            var postalCodes = new List<PostalCodeModel>();

            string stringData = await FetchData();

            try
            {
                using var stringReader = new StringReader(stringData);
                string line;

                while ((line = stringReader.ReadLine()) != null)
                {
                    var strings = line.Split('\t');
                    postalCodes.Add(new PostalCodeModel(strings[0], strings[1]));
                }

                return postalCodes;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An unexpected error occured while retreiving and mapping postal codes");
                throw;
            }
        }
    }
}