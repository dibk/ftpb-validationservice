﻿using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Dibk.Ftpb.Validation.Application.Utils;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode
{
    public class PostalCodeService : IPostalCodeService
    {
        private readonly ILogger<PostalCodeService> _logger;
        private readonly PostalCodeHttpClient _postalCodeHttpClient;
        private List<PostalCodeModel> _postalCodes;

        public PostalCodeService(ILogger<PostalCodeService> logger, PostalCodeHttpClient postalCodeHttpClient)
        {
            _logger = logger;
            _postalCodeHttpClient = postalCodeHttpClient;
            _postalCodes = new List<PostalCodeModel>();
        }

        public PostalCodeValidationResult ValidatePostnr(string pnr)
        {
            return AsyncHelper.RunSync(async () => await ValidatePostnrAsync(pnr));
        }

        public async Task<PostalCodeValidationResult> ValidatePostnrAsync(string pnr)
        {
            if (_postalCodes == null || !_postalCodes.Any())
            {
                _logger.LogDebug("Postal codes not loaded into cache. Loading..");
                await LoadPostalCodesIntoCacheAsync();
            }

            var pnrCacheValue = _postalCodes.FirstOrDefault(p => p.Postnummer.Equals(pnr));
            if (pnrCacheValue != null)
                return new PostalCodeValidationResult(pnrCacheValue.Postnummer, pnrCacheValue.Poststed, true);
            else
                return new PostalCodeValidationResult(pnr, null, false);
        }

        public async Task LoadPostalCodesIntoCacheAsync()
        {
            _logger.LogDebug("Loading postal codes into cache..");

            var postalCodes = new List<PostalCodeModel>();
            try
            {
                var response = await _postalCodeHttpClient.GetPostalcodeList();
                postalCodes = response.ToList();
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, "An error occured while requesting bring for postal code list, uses fallback resource file");
                postalCodes = LoadFromFallbackFile();
            }

            if (postalCodes != null && postalCodes.Any())
            {
                _logger.LogDebug("Retreived postal codes. Resets and adds to cache..");
                _postalCodes.Clear();
                _postalCodes.AddRange(postalCodes);
            }
            else
                _logger.LogWarning("Could not retreive postal codes from API..");
        }

        private List<PostalCodeModel> LoadFromFallbackFile()
        {
            var postalCodes = new List<PostalCodeModel>();
            var fileStream = Assembly.GetExecutingAssembly().GetResourceStream("Fallback-postnummerregister-ansi.txt");

            using var reader = new StringReader(PostalCodeHelpers.GetUtf8StringFromStream(fileStream));
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                var strings = line.Split('\t');
                postalCodes.Add(new PostalCodeModel(strings[0], strings[1]));
            }

            return postalCodes;
        }
    }
}