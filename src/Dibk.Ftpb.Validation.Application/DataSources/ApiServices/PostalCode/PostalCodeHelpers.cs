﻿using System.IO;
using System.Text;

internal static class PostalCodeHelpers
{
    public static string GetUtf8StringFromStream(Stream stream)
    {
        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        // Les dataene fra streamen i Windows-1252-koding
        using var streamReader = new StreamReader(stream, Encoding.GetEncoding(1252));
        var windows1252Data = streamReader.ReadToEnd();

        // Konverter dataene til UTF-8
        var utf8Bytes = Encoding.Convert(Encoding.GetEncoding(1252), Encoding.UTF8, Encoding.GetEncoding(1252).GetBytes(windows1252Data));
        var utf8Data = Encoding.UTF8.GetString(utf8Bytes);

        return utf8Data;
    }
}
