﻿using Dibk.Ftpb.Validation.Application.DataSources.Models;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode
{
    public interface IPostalCodeService
    {
        PostalCodeValidationResult ValidatePostnr(string pnr);

        Task<PostalCodeValidationResult> ValidatePostnrAsync(string pnr);

        Task LoadPostalCodesIntoCacheAsync();
    }
}