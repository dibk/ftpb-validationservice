﻿using Dibk.Ftpb.Validation.Application.Models.Standard;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Altinn
{
    public class AltinnMetadataServices : IAltinnMetadataServices
    {
        private readonly AltinnHttpClient _altinnHttpClient;
        private readonly IMemoryCache _memoryCache;


        public AltinnMetadataServices(AltinnHttpClient altinnHttpClient, IMemoryCache memoryCache)
        {
            _altinnHttpClient = altinnHttpClient;
            _memoryCache = memoryCache;
        }

        public async Task<AltinnMetadata> GetAltinnMetadataAsync(string dataFormatId, string dataFormatVersion)
        {
            // try to get Metadata from cache
            if (_memoryCache.TryGetValue<AltinnMetadata>($"{dataFormatId}/{dataFormatVersion}", out var altinnMetadata))
                return altinnMetadata;

            //Get from API
            var allDibkServicesMetadata = await _altinnHttpClient.GetAllDibkServicesMetadata();

            //Add to cache if found
            altinnMetadata = new ();
            if (allDibkServicesMetadata != null)
            {
                foreach (var altinFormService in allDibkServicesMetadata)
                {
                    _memoryCache.Set(altinFormService.Key, altinFormService.Value, DateTime.Now.AddMinutes(60));
                }
                allDibkServicesMetadata.TryGetValue($"{dataFormatId}/{dataFormatVersion}", out altinnMetadata);
            }
            return altinnMetadata;
        }

    }
}
