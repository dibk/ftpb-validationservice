﻿using Dibk.Ftpb.Validation.Application.Models.Standard;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Altinn
{
    public class AltinnHttpClient
    {
        private readonly HttpClient _httpClient;

        public AltinnHttpClient(HttpClient httpClient, IOptions<AltinnSettings> options)
        {
            _httpClient = httpClient;
            var url = options.Value.AltinnServerUrl;
            _httpClient.BaseAddress = new Uri(url);
        }

        public async Task<Dictionary<string, AltinnMetadata>> GetAllDibkServicesMetadata()
        {
            Dictionary<string, AltinnMetadata> altinnMetadatas = null;
            try
            {
                // Url to get all Altinn services "https://altinn.no/api/metadata?$filter=ServiceOwnerCode%20eq%20%27DIBK%27%20or%20ServiceOwnerCode%20eq%20%27DAT%27 eq 'DIBK'"
                var response = await _httpClient.GetAsync("/api/metadata?$filter=ServiceOwnerCode%20eq%20%27DIBK%27%20or%20ServiceOwnerCode%20eq%20%27DAT%27").ConfigureAwait(false);

                string altinnMetadataString = String.Empty;
                if (response.IsSuccessStatusCode)
                {
                    altinnMetadataString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    altinnMetadatas = await SerializeDibkServicesMetadata(altinnMetadataString);
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex, "Feil ved tilgangen til Altinn metadata for skjema");
            }
            return altinnMetadatas;
        }

        internal async Task<string> GetMatadataForAltinnServices(string servicecode, string servicecodeEdition)
        {
            string altinnMetadataString = String.Empty;

            try
            {
                string urlAltinMetadata = $"/api/metadata/formtask/{servicecode}/{servicecodeEdition}";
                var response = await _httpClient.GetAsync(urlAltinMetadata).ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    altinnMetadataString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex, "Feil ved tilgangen til Altinn metadata for skjema");
            }

            return altinnMetadataString;
        }

        internal async Task<Dictionary<string, AltinnMetadata>> SerializeDibkServicesMetadata(string allDibkServicesMetadata)
        {
            Dictionary<string, AltinnMetadata> altinMetadataDictionary = new Dictionary<string, AltinnMetadata>();

            if (string.IsNullOrEmpty(allDibkServicesMetadata))
                return altinMetadataDictionary;

            var jsonArray = JArray.Parse(allDibkServicesMetadata);
            if (jsonArray.GetType() == typeof(JArray))
            {
                // "i" and "j" are used for internal control for duplicate keys and services not found
                var i = 0;
                var j = 0;
                // loop throw all services
                foreach (var jsonObject in jsonArray)
                {
                    // get Services info from Json object
                    var serviceCode1 = jsonObject["ServiceCode"];
                    string serviceCode = (string)jsonObject["ServiceCode"];
                    string serviceCodeEdition = (string)jsonObject["ServiceEditionCode"];

                    // control if service exist
                    if (!string.IsNullOrEmpty(serviceCode) && !string.IsNullOrEmpty(serviceCodeEdition))
                    {
                        // add parameters to altinmetadata model
                        var altinMetadata = new AltinnMetadata();
                        altinMetadata.ServiceName = (string)jsonObject["ServiceName"];
                        altinMetadata.ServiceCode = serviceCode;
                        altinMetadata.ServiceEditionCode = (string)jsonObject["ServiceEditionCode"];
                        altinMetadata.ServiceType = (string)jsonObject["ServiceType"];
                        if (altinMetadata.ServiceType.ToLower() == "formtask")
                        {
                            var altinnMetadataString = await GetMatadataForAltinnServices(serviceCode, serviceCodeEdition);

                            // this is to avoid the problem if the service is not found
                            if (!string.IsNullOrEmpty(altinnMetadataString))
                            {
                                var metadataForm = JObject.Parse(altinnMetadataString);

                                var attachmentRules = (JArray)metadataForm["AttachmentRules"];

                                var mainFormsMetaData = metadataForm["FormsMetaData"]
                                    .FirstOrDefault(f => f["FormType"].Value<string>() == "MainForm");
                                var subFormsMetaData = metadataForm["FormsMetaData"]
                                    .Where(f => f["FormType"].Value<string>() != "MainForm");
                                altinMetadata.SubFormsMetaData = SerializeSubFormsMetadata(subFormsMetaData);
                                altinMetadata.AttachmentRules = SerializeAttachmentRules(attachmentRules);

                                string dataFormatId = (string)mainFormsMetaData["DataFormatID"];
                                string dataFormatVersion = (string)mainFormsMetaData["DataFormatVersion"];

                                if (!altinMetadataDictionary.ContainsKey($"{dataFormatId}/{dataFormatVersion}"))
                                {
                                    altinMetadataDictionary.Add($"{dataFormatId}/{dataFormatVersion}", altinMetadata);
                                }
                            }
                            else
                            {
                                // for services not found
                                j++;
                                altinMetadataDictionary.Add($"{serviceCode}/{serviceCodeEdition} -{i}/{j}", altinMetadata);
                            }
                        }
                        // for duplicate keys
                        i++;
                    }
                }
            }
            return altinMetadataDictionary;
        }

        private List<FormMetaData> SerializeSubFormsMetadata(IEnumerable<JToken> subFormsMetaData)
        {
            var subFormsList = new List<FormMetaData>();
            foreach (var subFormMetadata in subFormsMetaData)
            {
                var formMetaData = new FormMetaData();
                formMetaData.FormName = (string)subFormMetadata["FormName"];
                formMetaData.DataFormatID = (string)subFormMetadata["DataFormatID"];
                formMetaData.DataFormatVersion = (string)subFormMetadata["DataFormatVersion"];
                subFormsList.Add(formMetaData);
            }
            return subFormsList;
        }

        private List<AttachmentRule> SerializeAttachmentRules(JArray attachmentRules)
        {
            var subFormsList = new List<AttachmentRule>();
            if (attachmentRules == null || !attachmentRules.Any()) return subFormsList;
            foreach (var attachRule in attachmentRules)
            {
                var attachmentRule = new AttachmentRule();
                attachmentRule.AttachmentTypeName = (string)attachRule["AttachmentTypeName"];
                attachmentRule.AllowedFileTypes = (string)attachRule["AllowedFileTypes"];
                attachmentRule.MaxAttachmentCount = (int)attachRule["MaxAttachmentCount"];
                attachmentRule.MaxFileSize = (int)attachRule["MaxFileSize"];
                attachmentRule.MinAttachmentCount = (int)attachRule["MinAttachmentCount"];
                subFormsList.Add(attachmentRule);
            }
            return subFormsList;
        }
    }
}