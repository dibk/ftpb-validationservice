﻿using System.Threading.Tasks;
using Dibk.Ftpb.Validation.Application.Models.Standard;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Altinn;

public interface IAltinnMetadataServices
{
    Task<AltinnMetadata> GetAltinnMetadataAsync(string dataFormatId, string dataFormatVersion);
}