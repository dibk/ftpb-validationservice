﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Altinn
{
    public class AltinnSettings
    {
        public string AltinnServerUrl { get; set; }
    }
}
