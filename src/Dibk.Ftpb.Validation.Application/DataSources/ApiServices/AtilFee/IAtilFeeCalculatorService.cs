﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dibk.Ftpb.Validation.Application.DataSources.Models;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.AtilFee
{
    public interface IAtilFeeCalculatorService
    {
        Task<AtilFeeCalculationBasis> GetFeeInformationAsync(string tiltakstype, string bygningstype, string BRA);
        Task<List<AtilTiltakstype>> GetTiltakstyperAsync();
    }
}
