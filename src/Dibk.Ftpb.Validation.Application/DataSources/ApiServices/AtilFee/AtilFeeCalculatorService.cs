﻿using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.AtilFee
{
    public class AtilFeeCalculatorService : IAtilFeeCalculatorService
    {
        private readonly AtilFeeCalculatorApiHttpClient _atilFeeCalculatorApiHttpClient;
        private readonly IMemoryCache _memoryCache;

        public AtilFeeCalculatorService(AtilFeeCalculatorApiHttpClient atilFeeCalculatorApiHttpClient, IMemoryCache memoryCache)
        {
            _atilFeeCalculatorApiHttpClient = atilFeeCalculatorApiHttpClient;
            _memoryCache = memoryCache;
        }

        public async Task<AtilFeeCalculationBasis> GetFeeInformationAsync(string tiltakstype, string bygningstype, string BRA)
        {
            if (tiltakstype == null)
            {
                throw new ArgumentNullException("tiltakstype cannot be null");
            }
            if (bygningstype == null)
            {
                throw new ArgumentNullException("bygningstype cannot be null");
            }
            if (BRA == null)
            {
                throw new ArgumentNullException("BRA cannot be null");
            }
            else
            {
                var isNumeric = int.TryParse(BRA, out int numericBRA);
                var isFloat = float.TryParse(BRA, out float floatBRA);
                if (!isNumeric && !isFloat)
                {
                    throw new ArgumentNullException("BRA must be a numeric value");
                }
                if (isFloat)
                {
                    BRA = Math.Ceiling(floatBRA).ToString();
                }
            }
            AtilFeeCalculationBasis retValue = await _atilFeeCalculatorApiHttpClient.GetTiltakstypeBygningFeeInfoAsync(tiltakstype, bygningstype, BRA);

            return retValue;
        }

        public async Task<List<AtilTiltakstype>> GetTiltakstyperAsync()
        {
            var retValue = await _atilFeeCalculatorApiHttpClient.GetTiltakstyperAsync();

            return retValue;
        }
    }
}