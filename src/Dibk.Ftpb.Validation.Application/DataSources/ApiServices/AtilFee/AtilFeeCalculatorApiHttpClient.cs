﻿using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.AtilFee
{
    public class AtilFeeCalculatorApiHttpClient
    {
        public HttpClient _httpClient;
        private string _calculationBasisUrl;
        private string _tiltakstyperUrl;

        public AtilFeeCalculatorApiHttpClient(HttpClient httpClient, IOptions<AtilFeeCalculatorSettings> options)
        {
            _httpClient = httpClient;
            var url = options.Value.BaseAddress;
            _httpClient.BaseAddress = new Uri(url);
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _calculationBasisUrl = options.Value.FeeCalculatorUrl;
            _tiltakstyperUrl = options.Value.TiltakstyperUrl;
        }

        public async Task<AtilFeeCalculationBasis> GetTiltakstypeBygningFeeInfoAsync(string tiltakstype, string bygningstype, string BRA)
        {
            string queryParams = string.Empty;
            queryParams = $"?tiltakstype={tiltakstype}";
            queryParams = $"{queryParams}&bygningstype={bygningstype}";
            queryParams = $"{queryParams}&areal={BRA}";

            var response = await _httpClient.GetAsync($"{_calculationBasisUrl}{queryParams}").ConfigureAwait(false);
            AtilFeeCalculationBasis retValue = null;
            string json = string.Empty;
            if (response.IsSuccessStatusCode)
            {
                json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                retValue = JsonConvert.DeserializeObject<AtilFeeCalculationBasis>(json);
            }

            return retValue;
        }

        public async Task<List<AtilTiltakstype>> GetTiltakstyperAsync()
        {
            var response = await _httpClient.GetAsync($"{_tiltakstyperUrl}").ConfigureAwait(false);
            List<AtilTiltakstype> retValue = null;
            string json = string.Empty;
            if (response.IsSuccessStatusCode)
            {
                json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                retValue = JsonConvert.DeserializeObject<List<AtilTiltakstype>>(json);
            }

            return retValue;
        }
    }
}