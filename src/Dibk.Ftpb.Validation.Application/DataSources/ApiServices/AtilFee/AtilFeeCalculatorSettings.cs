﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.AtilFee
{
    public class AtilFeeCalculatorSettings : IAtilFeeCalculatorSettings
    {
        public string BaseAddress { get; set; }
        public string FeeCalculatorUrl { get; set; }
        public string TiltakstyperUrl { get; set; }
    }

    public interface IAtilFeeCalculatorSettings
    {
        string BaseAddress { get; set; }
        string FeeCalculatorUrl { get; set; }
        string TiltakstyperUrl { get; set; }
    }
}
