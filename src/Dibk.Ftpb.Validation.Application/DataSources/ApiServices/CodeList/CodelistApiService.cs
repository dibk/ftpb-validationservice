﻿using Dibk.Ftpb.Validation.Application.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList
{
    public class CodeListApiService
    {
        private readonly CodeListApiSettings _options;
        private readonly ILogger<CodeListApiService> _logger;
        private readonly IHttpClientFactory _httpClientFactory;

        public CodeListApiService(ILogger<CodeListApiService> logger, IOptions<CodeListApiSettings> options, IHttpClientFactory httpClientFactory)
        {
            _options = options.Value;
            _logger = logger;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<string> GetCodeList(string codeListPath, RegistryType registryType)
        {
            var jsonCodeList = string.Empty;
            var url = GetUrlForCodeList(registryType);
            var requestPath = $"{codeListPath}";

            try
            {
                using var httpClient = _httpClientFactory.CreateClient();

                httpClient.BaseAddress = new Uri(url);
                httpClient.DefaultRequestHeaders.Accept
                    .Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using var response = await httpClient.GetAsync(requestPath);

                response.EnsureSuccessStatusCode();

                jsonCodeList = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Can not get codeList :'{CodeListName}' for '{CodeListRegistryName}' by request url: '{CodeListUrl}/{CodeListPath}'", codeListPath, registryType.ToString(), url, requestPath);
            }
            return jsonCodeList;
        }

        private string GetUrlForCodeList(RegistryType registryType)
        {
            switch (registryType)
            {
                case RegistryType.Arbeidstilsynet:
                    return _options.ArbeidstilsynetUrl;

                case RegistryType.Byggesoknad:
                    return _options.ByggesoknadUrl;

                case RegistryType.EByggesak:
                    return _options.EByggesakUrl;

                case RegistryType.SosiKodelister:
                    return _options.SosiKodelisterUrl;

                case RegistryType.plansak:
                    return _options.PlansakUrl;

                default:
                    throw new ArgumentException($"No URL registered for '{registryType}'.");
            }
        }
    }
}