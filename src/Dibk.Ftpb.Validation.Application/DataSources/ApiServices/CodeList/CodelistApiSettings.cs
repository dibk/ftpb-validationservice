﻿using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList
{
    public class CodeListApiSettings
    {
        public string ArbeidstilsynetUrl { get; set; }
        public string ByggesoknadUrl { get; set; }
        public string EByggesakUrl { get; set; }
        public string SosiKodelisterUrl { get; set; }
        public string PlansakUrl { get; set; }
        public AdditionalCodes AdditionalCodes { get; set; }
        public List<string> StatusesConsideredValid { get; set; }
    }

    public class AdditionalCodes
    {
        public string Kommunenummer { get; set; }
    }
}
