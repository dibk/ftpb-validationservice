using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Utils;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList
{
    public class CodeListService : ICodeListService
    {
        private readonly CodeListApiService _codeListApiService;
        private readonly IMemoryCache _memoryCache;
        private readonly CodeListApiSettings _options;

        public CodeListService(IMemoryCache memoryCache, IOptions<CodeListApiSettings> options, CodeListApiService codeListApiService)
        {
            _codeListApiService = codeListApiService;
            _memoryCache = memoryCache;
            _options = options.Value;
        }

        public Dictionary<string, CodeListItem> GetCodeList(Enum codeListName, RegistryType registryType)
        {
            var codeListPath = EnumHelper.GetCodelistUrl(codeListName);
            return GetCodeList(codeListPath, registryType);
        }

        public bool? IsCodeValueValid(Enum codeListName, string codeValue, RegistryType registryType)
        {
            if (string.IsNullOrEmpty(codeValue)) return false;

            Dictionary<string, CodeListItem> codelist = GetCodeList(codeListName, registryType);

            if (codelist == null)
                return null;

            CodeListItem result;
            if (codelist.TryGetValue(codeValue, out result))
                return _options.StatusesConsideredValid.Exists(c => c.Equals(result.Status, StringComparison.CurrentCultureIgnoreCase));

            return false;
        }

        public bool? IsCodeLabelValid(Enum codeListName, string codeValue, string codeLabel, RegistryType registryType)
        {
            if (string.IsNullOrEmpty(codeValue) || string.IsNullOrEmpty(codeLabel)) return false;

            Dictionary<string, CodeListItem> codelist = GetCodeList(codeListName, registryType);

            if (codelist == null)
                return null;

            CodeListItem result;
            if (codelist.TryGetValue(codeValue, out result))
                if (result.Name.Equals(codeLabel))
                    return true;

            return false;
        }

        public CodeListItem GetCodelistTagValue(Enum codeListName, string codeValue, RegistryType registryType)
        {
            if (string.IsNullOrEmpty(codeValue))
                return new CodeListItem(codeListName.ToString(), null, "Utfylt");

            Dictionary<string, CodeListItem> codelist = GetCodeList(codeListName, registryType);
            if (codelist == null)
                return new CodeListItem(codeListName.ToString(), null, "IkkeValidert");

            CodeListItem result;
            if (!codelist.TryGetValue(codeValue, out result))
                return new CodeListItem(codeListName.ToString(), null, "ugyldig");

            return result;
        }

        public async Task RefreshCodeListCache(Enum codeListName, RegistryType registryType)
        {
            var codeListPath = EnumHelper.GetCodelistUrl(codeListName);

            var jsonResponce = await _codeListApiService.GetCodeList(codeListPath, registryType);

            if (!string.IsNullOrEmpty(jsonResponce))
            {
                _memoryCache.Remove(codeListPath);

                AddToCache(codeListPath, jsonResponce);
            }
        }

        private Dictionary<string, CodeListItem> GetCodeList(string codelistPath, RegistryType registryType)
        {
            Dictionary<string, CodeListItem> codeList = null;
            //Get from cache
            if (_memoryCache.TryGetValue<Dictionary<string, CodeListItem>>(codelistPath, out codeList))
                return codeList;

            //Get from API
            var jsonResponce = AsyncHelper.RunSync(() => _codeListApiService.GetCodeList(codelistPath, registryType));

            //Add to cache if found
            if (!string.IsNullOrEmpty(jsonResponce))
                codeList = AddToCache(codelistPath, jsonResponce);

            return codeList;
        }

        private Dictionary<string, CodeListItem> AddToCache(string codeListPath, string jsonResponce)
        {
            var codeList = ParseCodeList(jsonResponce);
            var extraCodes = GetCodeListFromConfiguration(codeListPath);
            if (!Helpers.ObjectIsNullOrEmpty(extraCodes))
            {
                var resultCodelist = codeList.Concat(extraCodes).ToDictionary(x => x.Key, x => x.Value);
                codeList = resultCodelist;
            }

            _memoryCache.Set(codeListPath, codeList, new MemoryCacheEntryOptions()
            {
                SlidingExpiration = new TimeSpan(6, 0, 0)
            });

            return codeList;
        }

        private Dictionary<string, CodeListItem> GetCodeListFromConfiguration(string codelistName)
        {
            Dictionary<string, CodeListItem> codeList = null;
            switch (codelistName.ToLower())
            {
                case "kommunenummer":
                    var kommunenummerList = _options.AdditionalCodes.Kommunenummer;
                    codeList = ParseCodeListFromConfiguration(kommunenummerList);
                    break;

                default:
                    break;
            }
            return codeList;
        }

        private Dictionary<string, CodeListItem> ParseCodeListFromConfiguration(string codelistValuesString)
        {
            var codeList = new Dictionary<string, CodeListItem>(StringComparer.CurrentCultureIgnoreCase);
            if (string.IsNullOrEmpty(codelistValuesString))
                return null;
            var codelistValues = codelistValuesString.Split(",");
            try
            {
                foreach (var code in codelistValues)
                {
                    string codevalue = code;
                    string name = code;
                    string status = "gyldig";
                    string description = "test data";

                    if (!string.IsNullOrWhiteSpace(codevalue) && !codeList.ContainsKey(codevalue))
                    {
                        codeList.Add(codevalue, new CodeListItem(name, description, status));
                    }
                }
            }
            catch (Exception e)
            {
                //TODO Add logg and skip 'Throw'
                throw new ArgumentException($"Can not parse Codelist from configuration :'{codelistValuesString}'");
            }
            return codeList;
        }

        private Dictionary<string, CodeListItem> ParseCodeList(string jsonString)
        {
            var codeList = new Dictionary<string, CodeListItem>(StringComparer.CurrentCultureIgnoreCase);

            try
            {
                var response = JObject.Parse(jsonString);

                foreach (var code in response["containeditems"])
                {
                    string codevalue = code["codevalue"].ToString();
                    string name = code["label"].ToString();
                    string status = code["status"].ToString();
                    string description = code["description"]?.ToString();

                    if (!string.IsNullOrWhiteSpace(codevalue) && !codeList.ContainsKey(codevalue))
                    {
                        codeList.Add(codevalue, new CodeListItem(name, description, status));
                    }
                }
            }
            catch (Exception e)
            {
                //TODO Add logg and skip 'Throw'
                throw new ArgumentException($"Can not parse jsonResponse :'{jsonString}'");
            }
            return codeList;
        }
    }
}