﻿using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Dibk.Ftpb.Validation.Application.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList
{
    public interface ICodeListService
    {
        /// <summary>
        /// Henter en kodeliste
        /// </summary>
        /// <param name="codeListName">Navn på kodelisten</param>
        /// <param name="registryType">Typen register</param>
        /// <returns></returns>
        Dictionary<string, CodeListItem> GetCodeList(Enum codeListName, RegistryType registryType);        

        /// <summary>
        /// Sjekker om en kodeverdi er gyldig
        /// </summary>
        /// <param name="codeListName">Navn på kodelisten</param>
        /// <param name="codeValue">Kode verdi</param>
        /// <param name="registryType">Typen register</param>
        /// <returns></returns>
        bool? IsCodeValueValid(Enum codeListName, string codeValue, RegistryType registryType);

        /// <summary>
        /// Sjekker om en kodebeskrivelse er gyldig i henhold til registeret
        /// </summary>
        /// <param name="codeListName">Navn på kodelisten</param>
        /// <param name="codeValue">Kode-verdi</param>
        /// <param name="codeLabel">Kode-beskrivelse</param>
        /// <param name="registryType">Typen register</param>
        /// <returns></returns>
        bool? IsCodeLabelValid(Enum codeListName, string codeValue, string codeLabel, RegistryType registryType);

        CodeListItem GetCodelistTagValue(Enum codeListName, string codeValue, RegistryType registryType);

        /// <summary>
        /// Oppdaterer kodelisten i cache
        /// </summary>
        /// <param name="codeListName">Navn på kodelisten</param>
        /// <param name="registryType">Typen register</param>
        /// <returns></returns>
        Task RefreshCodeListCache(Enum codeListName, RegistryType registryType);
    }
}