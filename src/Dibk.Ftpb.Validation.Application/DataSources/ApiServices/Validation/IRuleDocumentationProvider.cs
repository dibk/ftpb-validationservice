﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dibk.Ftpb.Validation.Application.Models.Standard;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Validation
{
    public interface IRuleDocumentationProvider
    {
        Dictionary<string, List<RuleModelDocumentation>> GetRuleWithoutMessageText(string dataFormatId, string dataFormatVersion, string[] branchNames);
        Dictionary<string, List<RuleModelDocumentation>> GetRulesWithDifferentMessageText(string dataFormatId, string dataFormatVersion, string branch, string compareBranch);
        Dictionary<string, List<RuleModelDocumentation>> GetRulesWithDifferenceSeverityLevel(string dataFormatId, string dataFormatVersion, string branch, string compareBranch);
        Dictionary<string, List<RuleModelDocumentation>> GetDifferentRuleIdInRule(string dataFormatId, string dataFormatVersion, string branch, string compareBranch);
        Dictionary<string, List<RuleModelDocumentation>> GetRulesNotImplemented(string dataFormatId, string dataFormatVersion, string branch, string compareBranch);

        Dictionary<string, List<RuleModelDocumentation>> GetRuleForXmlNodeDocumentation(string[] formMetadaList, string xmlNode, string branch, bool isXmlNodeStartElement = true);
        List<RuleModelDocumentation> RuleInfoUse(Dictionary<string, List<RuleModelDocumentation>> formRuleForXmlNode, bool? includCheckListId);
        List<RuleModelDocumentation> UpdateRuleModelDocumentationWithFormName(List<RuleModelDocumentation> formRuleForXmlNode);

    }
}