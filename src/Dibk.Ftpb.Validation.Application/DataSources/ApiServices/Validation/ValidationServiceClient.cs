﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Microsoft.Extensions.Options;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Validation
{
    public class ValidationServiceClient : IValidationServiceClient
    {
        private readonly IOptions<ValidationSettings> _options;

        public ValidationServiceClient(IOptions<ValidationSettings> options)
        {
            _options = options;
        }

        public async Task<string> GetValidationRules(string dataFormatId, string dataFormatVersion, string branch)
        {
            string uri = GetUrlFormBranch(branch);
            string jsonCodeList = String.Empty;

            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(uri);
                httpClient.DefaultRequestHeaders.Accept
                    .Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var requestPath = $"/api/formrules/{dataFormatId}/{dataFormatVersion}";

                var response = await httpClient.GetAsync(requestPath);

                if (response.IsSuccessStatusCode)
                    jsonCodeList = await response.Content.ReadAsStringAsync();
                else
                {
                    //TODO do something 
                }
            }

            return jsonCodeList;
        }

        private string GetUrlFormBranch(string branch)
        {
            var branch2Lower = branch.ToLower();
            switch (branch2Lower)
            {
                case "dev":
                    return _options.Value.DevUrl;
                case "test":
                    return _options.Value.TestUrl;
                case "prod":
                    return _options.Value.ProdUrl;     
                case "localhost":
                    return _options.Value.LocalhostUrl;
                default:
                    return _options.Value.DevUrl;
            }
        }
    }
}
