﻿using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Validation;

public interface IValidationServiceClient
{
    Task<string> GetValidationRules(string dataFormatId, string dataFormatVersion, string branch);
}