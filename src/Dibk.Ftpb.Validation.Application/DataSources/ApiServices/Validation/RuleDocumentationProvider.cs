﻿using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Dibk.Ftpb.Validation.Application.Utils;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Validation
{
    public class RuleDocumentationProvider : IRuleDocumentationProvider
    {
        private readonly IOptions<ValidationSettings> _options;
        private readonly IMemoryCache _memoryCache;
        private readonly ValidationServiceClient _validationClient;

        public RuleDocumentationProvider(IOptions<ValidationSettings> options, IMemoryCache memoryCache)
        {
            _options = options;
            _memoryCache = memoryCache;
            _validationClient = new ValidationServiceClient(options);
        }
        // Documentation Without Text
        public Dictionary<string, List<RuleModelDocumentation>> GetRuleWithoutMessageText(string dataFormatId, string dataFormatVersion, string[] branchNames)
        {
            if (!branchNames.Any()) return null;
            var result = new Dictionary<string, List<RuleModelDocumentation>>();

            foreach (var branch in branchNames)
            {
                var rules = AsyncHelper.RunSync(() => GetValidationRules(dataFormatId, dataFormatVersion, branch));
                List<RuleModelDocumentation> ruleDocumentationModels = rules?.Where(r => r.Description.Contains("could", StringComparison.CurrentCultureIgnoreCase)).ToList();

                if (!Helpers.ObjectIsNullOrEmpty(ruleDocumentationModels))
                    result.Add(branch, ruleDocumentationModels);
            }
            return RenameDictionaryKeyWithRulesCount(result);
        }

        //Different Message Text
        public Dictionary<string, List<RuleModelDocumentation>> GetRulesWithDifferentMessageText(string dataFormatId, string dataFormatVersion, string branch, string compareBranch)
        {
            var dictionary = AsyncHelper.RunSync(() => GetRulesFromBothBranches(dataFormatId, dataFormatVersion, branch, compareBranch));
            var result = GetRulesWithDifferentMessageText(dictionary);
            return RenameDictionaryKeyWithRulesCount(result);
        }
        public Dictionary<string, List<RuleModelDocumentation>> GetRulesWithDifferentMessageText(Dictionary<string, List<RuleModelDocumentation>> dictionary)
        {
            var result = new Dictionary<string, List<RuleModelDocumentation>>();

            var branches = dictionary.Keys.ToList();
            var rulesLists = dictionary.Values.ToList();

            if (rulesLists.All(l => l == null))
                return null;

            if (rulesLists.Any(l => l == null))
                return result;

            foreach (var (branch1, rules) in dictionary)
            {
                var otherBranch = branches.FirstOrDefault(b => b != branch1);
                var differingRules = rules?.Where(r => otherBranch != null && dictionary[otherBranch] != null && dictionary[otherBranch].Any(r2 =>
                    r2.RuleId == r.RuleId
                    && r2.Xpath == r.Xpath
                    && r2.XpathPrecondition == r.XpathPrecondition
                    && r2.Description != r.Description
                    )).ToList();

                if (differingRules != null && differingRules.Any())
                    result.Add(branch1, differingRules);
            }
            return result;

        }

        // Difference Severity Level
        public Dictionary<string, List<RuleModelDocumentation>> GetRulesWithDifferenceSeverityLevel(string dataFormatId, string dataFormatVersion, string branch, string compareBranch)
        {
            var dictionary = AsyncHelper.RunSync(() => GetRulesFromBothBranches(dataFormatId, dataFormatVersion, branch, compareBranch));
            var result = GetRulesWithDifferenceSeverityLevel(dictionary);
            return RenameDictionaryKeyWithRulesCount(result);
        }

        public Dictionary<string, List<RuleModelDocumentation>> GetRulesWithDifferenceSeverityLevel(Dictionary<string, List<RuleModelDocumentation>> dictionary)
        {

            var result = new Dictionary<string, List<RuleModelDocumentation>>();

            var branches = dictionary.Keys.ToList();
            var rulesLists = dictionary.Values.ToList();

            if (rulesLists.All(l => l == null))
                return null;

            if (rulesLists.Any(l => l == null))
                return result;

            foreach (var (branch1, rules) in dictionary)
            {
                var otherBranch = branches.FirstOrDefault(b => b != branch1);
                var differingRules = rules?.Where(r => otherBranch != null && dictionary[otherBranch] != null && dictionary[otherBranch].Any(r2 =>
                    r2.RuleId == r.RuleId
                    && r2.Xpath == r.Xpath
                    && r2.XpathPrecondition == r.XpathPrecondition
                    && r2.RuleType != r.RuleType
                    )).ToList();

                if (differingRules != null && differingRules.Any())
                    result.Add(branch1, differingRules);
            }
            return result;

        }
        // Different RuleId In Rule
        public Dictionary<string, List<RuleModelDocumentation>> GetDifferentRuleIdInRule(string dataFormatId, string dataFormatVersion, string branch, string compareBranch)
        {
            var dictionary = AsyncHelper.RunSync(() => GetRulesFromBothBranches(dataFormatId, dataFormatVersion, branch, compareBranch));
            var result = GetDifferentRuleIdInRule(dictionary);
            return RenameDictionaryKeyWithRulesCount(result);
        }
        public Dictionary<string, List<RuleModelDocumentation>> GetDifferentRuleIdInRule(Dictionary<string, List<RuleModelDocumentation>> dictionary)
        {
            var result = new Dictionary<string, List<RuleModelDocumentation>>();

            var branches = dictionary.Keys.ToList();
            var rulesLists = dictionary.Values.ToList();

            if (rulesLists.All(l => l == null))
                return null;

            if (rulesLists.Any(l => l == null))
                return result;

            foreach (var (branch1, rules) in dictionary)
            {
                var otherBranch = branches.FirstOrDefault(b => b != branch1);
                var differingRules = rules?.Where(r => otherBranch != null && dictionary[otherBranch] != null && dictionary[otherBranch].Any(r2 =>
                    r2.Description == r.Description
                    && r2.Xpath == r.Xpath
                    && r2.XpathPrecondition == r.XpathPrecondition
                    && r2.RuleId != r.RuleId
                )).ToList();

                if (differingRules != null && differingRules.Any())
                    result.Add(branch1, differingRules);
            }
            return result;
        }

        // Rules Not Implemented
        public Dictionary<string, List<RuleModelDocumentation>> GetRulesNotImplemented(Dictionary<string, List<RuleModelDocumentation>> dictionary)
        {
            var result = new Dictionary<string, List<RuleModelDocumentation>>();

            var branches = dictionary.Keys.ToList();
            var rulesLists = dictionary.Values.ToList();
            if (rulesLists.All(l => l == null))
                return null;

            if (rulesLists.Any(l => l == null))
            {
                result.Add(branches[0], rulesLists[0] ?? new List<RuleModelDocumentation>());
                result.Add(branches[1], rulesLists[1] ?? new List<RuleModelDocumentation>());
                return result;
            }

            foreach (var (branch1, rules) in dictionary)
            {
                var otherBranch = branches.FirstOrDefault(b => b != branch1);

                var differingRules = rules?.Where(r => otherBranch != null && dictionary[otherBranch] != null && !dictionary[otherBranch].Any(r2 => r2.RuleId.Equals(r.RuleId))).ToList();
                if (differingRules != null && differingRules.Any())
                    result.Add(branch1, differingRules);
            }

            return result;
        }
        public Dictionary<string, List<RuleModelDocumentation>> GetRulesNotImplemented(string dataFormatId, string dataFormatVersion, string branch, string compareBranch)
        {
            var dictionary = AsyncHelper.RunSync(() => GetRulesFromBothBranches(dataFormatId, dataFormatVersion, branch, compareBranch));
            var result = GetRulesNotImplemented(dictionary);
            return RenameDictionaryKeyWithRulesCount(result);
        }


        // XmlNode Documentation
        public Dictionary<string, List<RuleModelDocumentation>> GetRuleForXmlNodeDocumentation(string[] formMetadaList, string xmlNode, string branch, bool isXmlNodeStartElement = true)
        {
            var formRuleForXmlNode = new Dictionary<string, List<RuleModelDocumentation>>();

            foreach (var formMetadata in formMetadaList)
            {
                string[] formData = formMetadata.Split(".");
                var dataFormatId = formData[0];
                var dataFormatVersion = formData[1];
                var ruleForXmlNode = GetRuleForXmlNodeDocumentation(dataFormatId, dataFormatVersion, xmlNode, branch, isXmlNodeStartElement);
                formRuleForXmlNode.Add($"{formMetadata}-{xmlNode}", ruleForXmlNode);
            }

            return formRuleForXmlNode;
        }
        public List<RuleModelDocumentation> GetRuleForXmlNodeDocumentation(string dataFormatId, string dataFormatVersion, string xmlNode, string branch, bool isXmlNodeStartElement = true)
        {
            if (string.IsNullOrEmpty(branch)) return null;


            var rules = AsyncHelper.RunSync(() => GetValidationRules(dataFormatId, dataFormatVersion, branch));

            //Delete formName from xpath
            //rules.ForEach(r=>r.Xpath=r.Xpath.Substring(r.Xpath.IndexOf("/")));
            //ruleDocumentationModels.ForEach(r => r.RuleId = Debugging.GetRuleIdNumber(r.RuleId));

            List<RuleModelDocumentation> ruleDocumentationModels = rules?.Where(r => RuleWithXmlNodeSkippingMainNode(r.Xpath, xmlNode,isXmlNodeStartElement)).ToList();

            return ruleDocumentationModels;
        }

        public List<RuleModelDocumentation> UpdateRuleModelDocumentationWithFormName(List<RuleModelDocumentation> formRuleForXmlNode)
        {
            if (Helpers.ObjectIsNullOrEmpty(formRuleForXmlNode))
                return formRuleForXmlNode;

            foreach (var rule in formRuleForXmlNode)
            {
                List<string> formNames = new List<string>();
                foreach (var formId in rule.FormMetadataList)
                {
                    switch (formId)
                    {
                        case "10000.1":
                            formNames.Add("Erklaering Ansvarsrett");
                            break;
                        case "10001.1":
                            formNames.Add("Samsvarserklaering");
                            break;
                        case "10002.1":
                            formNames.Add("Kontrollerklaering");
                            break;
                        case "7063.47177":
                            formNames.Add("Avfallsplan v2");
                            break;
                        case "6821.45957":
                            formNames.Add("Arbeidstilsynets Samtykke v2");
                            break;
                        case "7086.47365":
                            formNames.Add("Supplering Arbeidstilsynet");
                            break;
                        case "10003.30":
                            formNames.Add("Igangsettingstillatelse v3.0");
                            break;
                        case "11000.20":
                            formNames.Add("planvarsel");
                            break;
                        default:
                            formNames.Add(formId);
                            break;
                    }
                }
                rule.FormMetadataList = formNames;
            }
            return formRuleForXmlNode;
        }
        public List<RuleModelDocumentation> RuleInfoUse(Dictionary<string, List<RuleModelDocumentation>> formRuleForXmlNode, bool? includCheckListId = null)
        {
            var ruleSummaryForXmlNode = new List<RuleModelDocumentation>();

            foreach (var ruleList in formRuleForXmlNode.Select(formRule => formRule.Value))
            {
                ruleSummaryForXmlNode = UnifyRules(ruleSummaryForXmlNode, ruleList, includCheckListId);
            }
            return ruleSummaryForXmlNode;
        }
        public List<RuleModelDocumentation> UnifyRules(List<RuleModelDocumentation> mainList, List<RuleModelDocumentation> compareList, bool? includCheckListId = null)
        {
            List<RuleModelDocumentation> newList = mainList;
            if (compareList == null || !compareList.Any())
                return newList;

            foreach (var rule in compareList)
            {
                var ruleXpath = Debugging.GetRuleXpathWithoutFirstNode(rule.Xpath);
                var ruleId = Debugging.GetRuleIdNumber(rule.RuleId);
                bool anyRule;

                if (ruleId.Length != rule.RuleId.Length)
                {

                    if (includCheckListId.GetValueOrDefault())
                    {
                        anyRule = mainList.Any(r =>
                            r.Xpath.Equals(Debugging.GetRuleXpathWithoutFirstNode(rule.Xpath)) &&
                            r.RuleId.Equals(Debugging.GetRuleIdNumber(rule.RuleId)) &&
                            r.RuleType == rule.RuleType &&
                            r.Description == rule.Description &&
                            r.CheckListPt == rule.CheckListPt
                        );
                    }
                    else
                    {
                        anyRule = mainList.Any(r =>
                            r.Xpath.Equals(Debugging.GetRuleXpathWithoutFirstNode(rule.Xpath)) &&
                            r.RuleId.Equals(Debugging.GetRuleIdNumber(rule.RuleId)) &&
                            r.RuleType == rule.RuleType &&
                            r.Description == rule.Description
                        );
                    }
                }
                else
                {
                    anyRule = mainList.Any(r =>
                        Debugging.GetRuleXpathWithoutFirstNode(r.Xpath).Equals(Debugging.GetRuleXpathWithoutFirstNode(rule.Xpath)) &&
                        Debugging.GetRuleIdNumber(r.RuleId).Equals(Debugging.GetRuleIdNumber(rule.RuleId)) &&
                        r.RuleType == rule.RuleType &&
                        r.Description == rule.Description &&
                        r.CheckListPt == rule.CheckListPt
                    );

                }

                int index = rule.RuleId.IndexOf('.', rule.RuleId.IndexOf('.') + 1);
                var formMetadaId = rule.RuleId.Substring(0, index);
                if (!anyRule)
                {
                    var ruleModel = new RuleModelDocumentation()
                    {
                        RuleId = ruleId,
                        CheckListPt = rule.CheckListPt,
                        Description = rule.Description,
                        RuleType = rule.RuleType,
                        Xpath = ruleXpath,
                        XpathPrecondition = Debugging.GetRuleXpathWithoutFirstNode(rule.XpathPrecondition),
                        FormMetadataList = new List<string>() { formMetadaId }
                    };

                    newList.Add(ruleModel);

                }
                else
                {
                    RuleModelDocumentation defaultRule;
                    if (includCheckListId.GetValueOrDefault())
                    {
                        defaultRule = newList.FirstOrDefault(r =>
                            r.Xpath.Equals(Debugging.GetRuleXpathWithoutFirstNode(rule.Xpath)) &&
                            r.RuleId.Equals(Debugging.GetRuleIdNumber(rule.RuleId)) &&
                            r.RuleType == rule.RuleType &&
                            r.Description == rule.Description &&
                            r.CheckListPt == rule.CheckListPt
                        );
                    }
                    else
                    {
                        defaultRule = newList.FirstOrDefault(r =>
                            r.Xpath.Equals(Debugging.GetRuleXpathWithoutFirstNode(rule.Xpath)) &&
                            r.RuleId.Equals(Debugging.GetRuleIdNumber(rule.RuleId)) &&
                            r.RuleType == rule.RuleType &&
                            r.Description == rule.Description
                        );
                    }
                    defaultRule?.FormMetadataList.Add(formMetadaId);

                }
            }
            return newList;
        }

        // Commune Methods
        private static bool RuleWithXmlNodeSkippingMainNode(string xpath, string xpathTocompare, bool isXmlNodeStartElement = true)
        {
            bool result = false;
            if (string.IsNullOrEmpty(xpath) || string.IsNullOrEmpty(xpathTocompare))
                return result;

            var xpathWithoutMainNode = $"/{Debugging.GetRuleXpathWithoutFirstNode(xpath)}";
            if (isXmlNodeStartElement)
                result = xpathWithoutMainNode.StartsWith(xpathTocompare, StringComparison.CurrentCultureIgnoreCase);
            else
                result = xpathWithoutMainNode.Contains(xpathTocompare, StringComparison.CurrentCultureIgnoreCase);
            return result;
        }

        private static Dictionary<string, List<RuleModelDocumentation>> RenameDictionaryKeyWithRulesCount(Dictionary<string, List<RuleModelDocumentation>> ruleDictionary)
        {
            if (Helpers.ObjectIsNullOrEmpty(ruleDictionary) || !ruleDictionary.Any())
                return ruleDictionary;

            var updateDictionary = new Dictionary<string, List<RuleModelDocumentation>>();
            foreach (var ruleDict in ruleDictionary)
            {
                updateDictionary.Add($"{ruleDict.Key} ({ruleDict.Value?.Count})", ruleDict.Value);
            }

            return updateDictionary;
        }
        private async Task<Dictionary<string, List<RuleModelDocumentation>>> GetRulesFromBothBranches(string dataFormatId, string dataFormatVersion, string branch, string compareBranch)
        {
            var rulesFromBranch = await GetValidationRules(dataFormatId, dataFormatVersion, branch);
            var rulesFromCompareBranch = await GetValidationRules(dataFormatId, dataFormatVersion, compareBranch);
            var dictionary = new Dictionary<string, List<RuleModelDocumentation>>
            {
                { branch, rulesFromBranch },
                { compareBranch, rulesFromCompareBranch }
            };
            return dictionary;
        }

        // Used in Unit tests to check localhost
        public async Task<List<RuleModelDocumentation>> GetValidationRules(string dataFormatId, string dataFormatVersion, string branch)
        {
            var dictionaryKey = $"{branch}-{dataFormatId}/{dataFormatVersion}";

            //Get from cache
            if (_memoryCache.TryGetValue(dictionaryKey, out List<RuleModelDocumentation> ruleDocumentationModels))
                return ruleDocumentationModels;

            //Get from API
            var jsonResponce = await _validationClient.GetValidationRules(dataFormatId, dataFormatVersion, branch).ConfigureAwait(false);

            //Add to cache if found
            if (!string.IsNullOrEmpty(jsonResponce))
            {
                var noko = JArray.Parse(jsonResponce);
                ruleDocumentationModels = System.Text.Json.JsonSerializer.Deserialize<List<RuleModelDocumentation>>(jsonResponce);

                _memoryCache.Set(dictionaryKey, ruleDocumentationModels, new MemoryCacheEntryOptions()
                {
                    SlidingExpiration = new TimeSpan(0, 0, 30)
                });
            }
            return ruleDocumentationModels;
        }


    }
}
