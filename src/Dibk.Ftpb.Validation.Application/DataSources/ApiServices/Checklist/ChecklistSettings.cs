﻿namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Checklist
{
    public class ChecklistSettings
    {
        public string ChecklistUrl { get; set; }
        public string AtilUrl { get; set; }
        public string DibkUrl { get; set; }
        public string MemoryCacheExpiration { get; set; }
    }
}
