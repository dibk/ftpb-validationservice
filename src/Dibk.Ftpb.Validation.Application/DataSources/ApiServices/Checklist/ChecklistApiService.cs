using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Reporter;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Checklist
{
    public class ChecklistApiService
    {
        private readonly ILogger<ChecklistApiService> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private string _requestUrl;

        public ChecklistApiService(ILogger<ChecklistApiService> logger, IOptions<ChecklistSettings> options, IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
            _requestUrl = options.Value.ChecklistUrl;
        }

        private HttpClient GetHttpClient(ServiceDomain serviceDomain)
        {
            if (serviceDomain == ServiceDomain.ATIL)
            {
                return _httpClientFactory.CreateClient(ChecklistConstants.AtilClientName);
            }
            else
                return _httpClientFactory.CreateClient(ChecklistConstants.DibkClientName);
        }

        public async Task<IEnumerable<Sjekk>> GetChecklist(string category, string filter, ServiceDomain serviceDomain)
        {
            IEnumerable<Sjekk> liste = null;
            try
            {
                var httpClient = GetHttpClient(serviceDomain);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (category == null)
                {
                    throw new ArgumentNullException("Category cannot be null");
                }

                var requestUri = $"/{category}";
                if (!string.IsNullOrEmpty(filter))
                {
                    requestUri += $"?{filter}";
                }
                using var response = await httpClient.GetAsync($"{_requestUrl}{requestUri}");
                string json = String.Empty;
                if (response.IsSuccessStatusCode)
                {
                    json = await response.Content.ReadAsStringAsync();
                    liste = JsonConvert.DeserializeObject<IEnumerable<Sjekk>>(json);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error getting checklist");
            }
            return liste;
        }

        public async Task<IEnumerable<ChecklistValidationRelations>> GetChecklistValidationRelations(string processCategory, ServiceDomain serviceDomain)
        {
            var liste = new List<ChecklistValidationRelations>();

            try
            {
                var httpClient = GetHttpClient(serviceDomain);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var requestUri = $"/valideringsregler";
                using var response = await httpClient.GetAsync($"{_requestUrl}{requestUri}/{processCategory}");

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    liste = (JsonConvert.DeserializeObject<IEnumerable<ChecklistValidationRelations>>(json) ?? Array.Empty<ChecklistValidationRelations>()).ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error getting checklist validation relations");
            }
            return liste;
        }

        public async Task<IEnumerable<ChecklistAnswer>> GetPrefillChecklistAnswer(PrefillChecklistInput prefillChecklist, ServiceDomain serviceDomain)
        {
            IEnumerable<ChecklistAnswer> liste = null;

            try
            {
                var httpClient = GetHttpClient(serviceDomain);

                var requestUri = $"/prefill";
                var jsonPostRequest = System.Text.Json.JsonSerializer.Serialize(prefillChecklist);

                var stringContent = new StringContent(jsonPostRequest, Encoding.UTF8, "application/json");
                using var response = await httpClient.PostAsync($"{_requestUrl}{requestUri}", stringContent);

                if (response.IsSuccessStatusCode)
                {
                    var jsonResponse = await response.Content.ReadAsStringAsync();
                    liste = JsonConvert.DeserializeObject<IEnumerable<ChecklistAnswer>>(jsonResponse);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error getting prefill checklist answer");
            }
            return liste;
        }
    }
}