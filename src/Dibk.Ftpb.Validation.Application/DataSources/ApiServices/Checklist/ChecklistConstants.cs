﻿namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Checklist
{
    public class ChecklistConstants
    {
        public const string DibkClientName = "DibkChecklistHttpClient";
        public const string AtilClientName = "AtilChecklistHttpClient";
    }
}