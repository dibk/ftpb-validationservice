﻿using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Checklist
{
    public class Sjekk
{       /// <summary>
        /// Intern id
        /// </summary>
        public int SjekkId { get; set; }

        /// <summary>
        /// Referanse Id til felles Id for sjekkpunkt som gjelder flere søknadstyper
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Kategorisering av sjekk. Samme som sjekkpunkttype i eByggesak
        /// </summary>
        public string Sjekkpunkttype { get; set; }

        /// <summary>
        /// Sjekkpunkt
        /// </summary>
        public string Navn { get; set; }

        /// <summary>
        /// Tema
        /// </summary>
        public string Tema { get; set; }

        /// <summary>
        /// Prosesskategori/Søknadstype
        /// </summary>
        public string Prosesskategori { get; set; }

        /// <summary>
        /// Hvis regel/spørsmål/behandling=true/false så angis det her mulige utfall på behandlingen
        /// </summary>
        public List<Utfall> Utfall { get; set; }
        public List<Sjekk> Undersjekkpunkter { get; set; }

        /// <summary>
        /// Rekkefølge på sjekk - samme som radnr i excelarket
        /// </summary>
        public int Rekkefolge { get; set; }

        public List<Tiltakstype> Tiltakstyper { get; set; }
        public List<Metadata> Metadata { get; set; }

        public DateTime? GyldigFra { get; set; }
        public DateTime? Oppdatert { get; set; }
    }
    public class Tiltakstype
    {
        public string Kode { get; set; }
    }

    public class Metadata
    {
        public int TypeId { get; set; }
        public int MetadataId { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }

    }


}
