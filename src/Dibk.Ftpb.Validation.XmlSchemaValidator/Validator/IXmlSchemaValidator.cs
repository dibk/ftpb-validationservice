﻿namespace Dibk.Ftpb.Validation.XmlSchemaValidator.Validator
{
    public interface IXmlSchemaValidator
    {
        public List<string> Validate(string key, Stream xmlStream);
    }
}
