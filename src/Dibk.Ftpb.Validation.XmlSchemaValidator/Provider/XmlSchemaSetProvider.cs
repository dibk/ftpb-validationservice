﻿using Dibk.Ftpb.Validation.XmlSchemaValidator.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Collections;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;

namespace Dibk.Ftpb.Validation.XmlSchemaValidator.Provider
{
    public class XmlSchemaSetProvider : IXmlSchemaSetProvider
    {
        private readonly object _schemaSetLock = new();
        private readonly IDictionary<string, XmlSchemaSet> _schemaSets = new Dictionary<string, XmlSchemaSet>();
        private readonly XsdValidatorOptions _options;
        private readonly ILogger<XmlSchemaSetProvider> _logger;
        private readonly IMemoryCache _memoryCache;

        public XmlSchemaSetProvider(
            IOptions<XsdValidatorOptions> options,
            IMemoryCache memoryCache,
            ILogger<XmlSchemaSetProvider> logger)
        {
            _options = options.Value;
            _memoryCache = memoryCache;
            _logger = logger;
        }

        public void CreateSchemaSets()
        {
            _logger.LogInformation("Laster og kompilerer XmlSchemaSets...");

            foreach (var kvp in _options.SchemaStreams)
            {
                var schemaSet = CreateSchemaSet(kvp.Key, kvp.Value);
                _schemaSets.TryAdd(kvp.Key, schemaSet);
            }

            foreach (var kvp in _options.SchemaUris)
            {
                var schemaSet = CreateSchemaSet(kvp.Key, kvp.Value.TargetNamespace, kvp.Value.SchemaUri);
                _schemaSets.TryAdd(kvp.Key, schemaSet);
            }
        }

        public XmlSchemaSet GetXmlSchemaSet(object key)
        {
            return _schemaSets.TryGetValue(key.ToString(), out var schemaSet) ? schemaSet : null;
        }

        public void RebuildSchemaSets()
        {
            _logger.LogInformation("Gjenoppbygger XmlSchemaSets...");

            lock (_schemaSetLock)
            {
                DeleteSchemaSets();
                CreateSchemaSets();
            }
        }

        public void RebuildSchemaSet(object key)
        {
            var keyStr = key.ToString();
            _logger.LogInformation("Gjenoppbygger XmlSchemaSet for '{keyStr}'...", keyStr);

            lock (_schemaSetLock)
            {
                DeleteSchemaSets(keyStr);

                if (_options.SchemaStreams.TryGetValue(keyStr, out var stream))
                {
                    _options.SchemaStreams.Remove(keyStr);
                    _schemaSets.TryAdd(keyStr, CreateSchemaSet(keyStr, stream));
                }
                else if (_options.SchemaUris.TryGetValue(keyStr, out var namespaceAndUri))
                {
                    _options.SchemaUris.Remove(keyStr);
                    _schemaSets.TryAdd(keyStr, CreateSchemaSet(keyStr, namespaceAndUri.TargetNamespace, namespaceAndUri.SchemaUri));
                }
            }
        }

        private void DeleteSchemaSets(string key = null)
        {
            if (!_options.CacheFiles)
                return;

            var _key = key != null ? $"xsd_{key}_" : "xsd_";

            GetMemoryCacheKeys()
                .Where(cacheKey => cacheKey.StartsWith(_key))
                .ToList()
                .ForEach(_memoryCache.Remove);

            _schemaSets.Clear();
        }

        private XmlSchemaSet CreateSchemaSet(string key, Stream stream)
        {
            var xmlSchemaSet = new XmlSchemaSet { XmlResolver = GetXmlResolver(key) };
            var xmlSchema = XmlSchema.Read(stream, null);
            stream.Position = 0;

            xmlSchemaSet.Add(xmlSchema);

            return CompileSchemaSet(xmlSchemaSet);
        }

        private XmlSchemaSet CreateSchemaSet(string key, string targetNamespace, string schemaUri)
        {
            var xmlSchemaSet = new XmlSchemaSet { XmlResolver = GetXmlResolver(key) };

            xmlSchemaSet.Add(targetNamespace, schemaUri);

            return CompileSchemaSet(xmlSchemaSet);
        }

        private XmlResolver GetXmlResolver(string key)
        {
            if (_options.CacheFiles)
                return new XmlMemoryCacheResolver(key, _options.CacheDurationDays, new HttpClient(), _memoryCache);

            return new XmlUrlResolver();
        }

        private XmlSchemaSet CompileSchemaSet(XmlSchemaSet xmlSchemaSet)
        {
            try
            {
                xmlSchemaSet.Compile();
                return xmlSchemaSet;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Kunne ikke kompilere XmlSchemaSet!");
                return null;
            }
        }

        private IEnumerable<string> GetMemoryCacheKeys()
        {
            var field = typeof(MemoryCache).GetProperty("EntriesCollection", BindingFlags.NonPublic | BindingFlags.Instance);
            var keys = new List<string>();

            if (field.GetValue(_memoryCache) is ICollection collection)
            {
                foreach (var item in collection)
                {
                    var methodInfo = item.GetType().GetProperty("Key");
                    var val = methodInfo.GetValue(item);
                    keys.Add(val.ToString());
                }
            }

            return keys;
        }
    }
}
