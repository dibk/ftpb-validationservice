﻿using System.Xml.Schema;

namespace Dibk.Ftpb.Validation.XmlSchemaValidator.Provider
{
    public interface IXmlSchemaSetProvider
    {
        void CreateSchemaSets();
        void RebuildSchemaSets();
        void RebuildSchemaSet(object key);
        XmlSchemaSet GetXmlSchemaSet(object key);
    }
}
