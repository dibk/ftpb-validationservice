﻿using Microsoft.Extensions.Caching.Memory;
using System.Xml;

namespace Dibk.Ftpb.Validation.XmlSchemaValidator.Provider
{
    internal class XmlMemoryCacheResolver : XmlUrlResolver
    {
        private readonly string _key;
        private readonly int _cacheDurationDays;
        private readonly HttpClient _httpClient;
        private readonly IMemoryCache _memoryCache;

        public XmlMemoryCacheResolver(
            string key,
            int cacheDurationDays,
            HttpClient httpClient,
            IMemoryCache memoryCache)
        {
            _key = key;
            _memoryCache = memoryCache;
            _httpClient = httpClient;
            _cacheDurationDays = cacheDurationDays;
        }

        public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
        {
            if (absoluteUri == null)
                throw new ArgumentNullException(nameof(absoluteUri));

            if ((absoluteUri.Scheme == "http" || absoluteUri.Scheme == "https") && (ofObjectToReturn == null || ofObjectToReturn == typeof(Stream)))
            {
                var byteArray = GetXmlSchemaFromCache(absoluteUri);
                return new MemoryStream(byteArray);
            }
            else
            {
                return base.GetEntity(absoluteUri, role, ofObjectToReturn);
            }
        }

        private byte[] GetXmlSchemaFromCache(Uri absoluteUri)
        {
            var key = $"xsd_{_key}_{absoluteUri}";

            return _memoryCache.GetOrCreate(key, cacheEntry =>
            {
                cacheEntry.SlidingExpiration = TimeSpan.FromDays(_cacheDurationDays);

                using var response = _httpClient.GetAsync(absoluteUri).Result;
                using var stream = response.Content.ReadAsStream();

                using var memoryStream = new MemoryStream();
                stream.CopyTo(memoryStream);
                memoryStream.Position = 0;

                return memoryStream.ToArray();
            });
        }
    }
}
