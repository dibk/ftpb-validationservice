﻿using Dibk.Ftpb.Validation.XmlSchemaValidator.Models;
using Dibk.Ftpb.Validation.XmlSchemaValidator.Provider;
using Dibk.Ftpb.Validation.XmlSchemaValidator.Validator;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using SchemaValidator = Dibk.Ftpb.Validation.XmlSchemaValidator.Validator.XmlSchemaValidator;

namespace Dibk.Ftpb.Validation.XmlSchemaValidator.Config
{
    public static class ValidatorConfig
    {
        public static void AddXmlSchemaValidator(this IServiceCollection services, Action<XsdValidatorOptions> options)
        {
            services.Configure(options);
            services.AddSingleton<IXmlSchemaSetProvider, XmlSchemaSetProvider>();
            services.AddTransient<IXmlSchemaValidator, SchemaValidator>();
        }

        public static void UseXmlSchemaValidator(this IApplicationBuilder app)
        {
            var provider = app.ApplicationServices.GetService<IXmlSchemaSetProvider>();
            provider.CreateSchemaSets();
        }
    }
}
