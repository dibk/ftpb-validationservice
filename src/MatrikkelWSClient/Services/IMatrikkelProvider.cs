﻿using BygningServiceWS;

namespace MatrikkelWSClient.Services
{
    public interface IMatrikkelProvider
    {
        bool? MatrikkelExists(ByggsokModel byggsokModel);

        bool? ByggMedBygningsnummerEksistererIMatrikkelen(string kommunenummer, string bygningsnummer, string gardsnummer, string bruksnummer, string festenummer = null, string seksjonsnummer = null);

        bool? ByggEksistererPåAdresseIMatrikkelen(string kommunenummer, string adressenavn, string husnummer, string bokstav, string bruksenhetsnummer, string gardsnummer, string bruksnummer, string festenummer = null, string seksjonsnummer = null);

        bool? ByggEksistererIMatrikkelen(string kommunenummer, string gardsnummer, string bruksnummer, string festenummer = null, string seksjonsnummer = null);
    }
}