﻿using BygningServiceWS;

namespace MatrikkelWSClient.Services
{
    public interface IMatrikkelServiceClient
    {
        Task<findByggResponse> FindByggAsync(ByggsokModel byggsokModel);
    }
}