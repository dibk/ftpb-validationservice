﻿using BygningServiceWS;
using MatrikkelWSClient.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace MatrikkelWSClient.Services
{
    public class MatrikkelServiceClient : IMatrikkelServiceClient
    {
        private readonly MatrikkelSettings _settings;
        private readonly BygningServiceClient _bygningServiceClient;
        private readonly MatrikkelContext _matrikkelContext;
        private readonly ILogger<MatrikkelServiceClient> _logger;
        private readonly int _timeoutSeconds;

        public MatrikkelServiceClient(ILogger<MatrikkelServiceClient> logger, IOptions<MatrikkelSettings> options)
        {
            _settings = options.Value;

            if (_settings.TimeoutSeconds.HasValue)
                _timeoutSeconds = _settings.TimeoutSeconds.Value;
            else
                _timeoutSeconds = 60;

            _bygningServiceClient = CreateBygningServiceClient();
            _matrikkelContext = CreateMatrikkelContextObject();
            _logger = logger;


        }

        public async Task<findByggResponse> FindByggAsync(ByggsokModel byggsokModel)
        {
            try
            {
                var byggIds = await _bygningServiceClient.findByggAsync(byggsokModel, _matrikkelContext);
                return byggIds;
            }
            catch (FaultException<BygningServiceWS.ServiceFaultInfo> ex)
            { 
                _logger.LogError(ex, "A fault occurred while requesting matrikkel for bygg. See error details for further info.");
                throw;
            }
            catch (FaultException ex)
            {
                _logger.LogError(ex, "An error occurred while requesting matrikkel for bygg. See error details for further info.");
                throw;
            }
            catch (CommunicationException ex)
            {
                _logger.LogError(ex, "A communication error occurred while requesting matrikkel for bygg");
                throw;
            }
            catch (TimeoutException ex)
            {
                _logger.LogError(ex, "Timeout occurred while requesting matrikkel for bygg");
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An unexpected error occurred while requesting matrikkel for bygg");
                throw;
            }
        }

        //**
        private BygningServiceClient CreateBygningServiceClient()
        {
            var client = new BygningServiceClient(GetBasicHttpBinding(), new EndpointAddress(_settings.BaseAddress + "BygningServiceWS"));
            SetCredentialsFromConfig(client.ClientCredentials);

            return client;
        }

        private BasicHttpBinding GetBasicHttpBinding()
        {
            return new BasicHttpBinding
            {
                SendTimeout = new TimeSpan(0, 0, 0, _timeoutSeconds, 0),
                Security =
                {
                    Mode = BasicHttpSecurityMode.Transport,
                    Transport = {ClientCredentialType = HttpClientCredentialType.Basic}
                },
                MaxReceivedMessageSize = long.Parse(_settings.MaxMessageSize)
            };
        }

        private void SetCredentialsFromConfig(ClientCredentials clientCredentials)
        {
            clientCredentials.UserName.UserName = _settings.Username;
            clientCredentials.UserName.Password = _settings.Password;
        }

        private MatrikkelContext CreateMatrikkelContextObject()
        {
            return new MatrikkelContext
            {
                locale = _settings.Locale,
                brukOriginaleKoordinater = false,
                koordinatsystemKodeId = new KoordinatsystemKodeId(),
                klientIdentifikasjon = _settings.KlientIdentifikasjon,
                systemVersion = _settings.SystemVersion,
                snapshotVersion = new Timestamp { timestamp = new DateTime(9999, 1, 1, 0, 0, 0) }
            };
        }
    }
}