﻿using BygningServiceWS;
using Microsoft.Extensions.Logging;

namespace MatrikkelWSClient.Services
{
    public class MatrikkelProvider : IMatrikkelProvider
    {
        private readonly ILogger<MatrikkelProvider> _logger;
        private readonly IMatrikkelServiceClient _matrikkelServiceHttpClient;

        private static readonly TaskFactory _taskFactory = new TaskFactory(CancellationToken.None,
                                                                           TaskCreationOptions.None,
                                                                           TaskContinuationOptions.None,
                                                                           TaskScheduler.Default);

        public static TResult RunSync<TResult>(Func<Task<TResult>> func) => _taskFactory.StartNew(func)
                                                                                        .Unwrap()
                                                                                        .GetAwaiter()
                                                                                        .GetResult();

        public MatrikkelProvider(ILogger<MatrikkelProvider> logger, IMatrikkelServiceClient matrikkelServiceClient)
        {
            _logger = logger;
            _matrikkelServiceHttpClient = matrikkelServiceClient;
        }

        public bool? MatrikkelExists(ByggsokModel byggsokModel)
        {
            try
            {
                //Sjekk om payload er gyldig før request gjøres mot matrikkel.
                //Apiet krever at gårds og bruksnummer er større enn 0.
                if (!IsPayloadValidForRequest(byggsokModel))
                {
                    _logger.LogInformation("Payload er ikke gydlig for request mot matrikkel. Knr: {kommunenummer} Gnr: {gardsnummer} Bnr: {bruksnummer}", byggsokModel.kommunenummer, byggsokModel.gardsnummer, byggsokModel.bruksnummer);
                    return null;
                }

                var byggResponse = RunSync(() => { return _matrikkelServiceHttpClient.FindByggAsync(byggsokModel); });

                var byggIds = byggResponse.@return;

                return byggIds.Any();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occured while requesting matrikkel for bygg");
                return null;
            }
        }

        public bool? ByggEksistererIMatrikkelen(string kommunenummer, string gardsnummer, string bruksnummer, string? festenummer = null, string? seksjonsnummer = null)
        {
            var byggsokModel = CreatePayload(kommunenummer, gardsnummer, bruksnummer, festenummer, seksjonsnummer);

            return MatrikkelExists(byggsokModel);
        }

        public bool? ByggMedBygningsnummerEksistererIMatrikkelen(string kommunenummer, string bygningsnummer, string gardsnummer, string bruksnummer, string? festenummer = null, string? seksjonsnummer = null)
        {
            var byggsokModel = CreatePayload(kommunenummer, gardsnummer, bruksnummer, festenummer, seksjonsnummer);

            if (!long.TryParse(bygningsnummer, out var bygningsnummerLong))
                bygningsnummerLong = 0;

            byggsokModel.bygningsnummer = bygningsnummerLong;
            byggsokModel.bygningsnummerSpecified = true;

            return MatrikkelExists(byggsokModel);
        }

        public bool? ByggEksistererPåAdresseIMatrikkelen(string kommunenummer, string adressenavn, string husnummer, string bokstav, string bruksenhetsnummer, string gardsnummer, string bruksnummer, string festenummer, string seksjonsnummer)
        {
            var byggsokModel = CreatePayload(kommunenummer, gardsnummer, bruksnummer, festenummer, seksjonsnummer);

            if (!int.TryParse(husnummer, out var husnummerInt))
                husnummerInt = 0;

            byggsokModel.adressenavn = adressenavn;
            byggsokModel.husnummer = husnummerInt;
            byggsokModel.husnummerSpecified = true;
            byggsokModel.bruksenhetsnummer = bruksenhetsnummer;
            byggsokModel.bokstav = bokstav;
            byggsokModel.utenBokstavSpecified = true;
            byggsokModel.utenBokstav = string.IsNullOrEmpty(bokstav);

            return MatrikkelExists(byggsokModel);
        }

        private bool IsPayloadValidForRequest(ByggsokModel byggsokModel)
        {
            if(!int.TryParse(byggsokModel.kommunenummer, out var kommunenummerInt))
            { return false; }

            if (!int.TryParse(byggsokModel.gardsnummer, out var gardsnummerInt))
            { return false; }

            if (!int.TryParse(byggsokModel.bruksnummer, out var bruksnummerInt))
            { return false; }

            if (kommunenummerInt == 0 || gardsnummerInt <= 0 || bruksnummerInt <= 0)
            { return false; }

            return true;
        }

        private ByggsokModel CreatePayload(string kommunenummer, string gardsnummer, string bruksnummer, string? festenummer = null, string? seksjonsnummer = null)
        {
            if (!int.TryParse(festenummer, out var festenummerInt))
                festenummerInt = 0;

            if (!int.TryParse(seksjonsnummer, out var seksjonsnummerInt))
                seksjonsnummerInt = 0;

            return new ByggsokModel()
            {
                kommunenummer = kommunenummer,
                gardsnummer = gardsnummer,
                bruksnummer = bruksnummer,
                festenummer = festenummerInt,
                festenummerSpecified = true,
                seksjonsnummer = seksjonsnummerInt,
                seksjonsnummerSpecified = true
            };
        }
    }
}