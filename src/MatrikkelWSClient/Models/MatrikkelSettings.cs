﻿namespace MatrikkelWSClient.Models
{
    public class MatrikkelSettings
    {
        public static string SectionName => "MatrikkelApi";
        public string BaseAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Locale { get; set; }
        public string KlientIdentifikasjon { get; set; }
        public string SystemVersion { get; set; }
        public string SnapshotVersion { get; set; }
        public string MaxMessageSize { get; set; }
        public int? TimeoutSeconds { get; set; }
    }
}