﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrikkelWSClient.Models
{
    public class MatrikkelValidationModel
    {
        public string kommunenummer { get; set; }
        public string gaardsnummer { get; set; }
        public string bruksnummer { get; set; }
        public string festenummer { get; set; }
        public string seksjonsnummer { get; set; }
        public List<string> bruksenhetsnummer { get; set; }
        public List<string> bygningsnummer { get; set; }
        public List<Adresse> ByggAdresses { get; set; }
    }

    public class Adresse
    {
        public string gatenavn { get; set; }
        public string husnr { get; set; }
        public string bokstav { get; set; }
        public string adresselinje1 { get; set; }
    }
}
