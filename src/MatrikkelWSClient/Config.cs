﻿using MatrikkelWSClient.Models;
using MatrikkelWSClient.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MatrikkelWSClient
{
    public static class Config
    {
        public static IServiceCollection AddMatrikkelClient(this IServiceCollection services, IConfiguration configuration)
        { 
            services.AddScoped<IMatrikkelProvider, MatrikkelProvider>();
            services.AddScoped<IMatrikkelServiceClient, MatrikkelServiceClient>();            
            services.Configure<MatrikkelSettings>(configuration.GetSection(MatrikkelSettings.SectionName));

            return services;
        }
    }
}
