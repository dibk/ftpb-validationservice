﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Validation;
using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Xunit;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators.FellestjenesterPlan;

namespace Dibk.Ftpb.Validation.Application.Tests.ReporterTests.DocumentationTests
{
    public class Planvarsel_11000_2_ValidatorTests
    {
        private List<RuleModelDocumentation> _rulesLocalhost;
        //https://stackoverflow.com/a/14845729
        const string skip = "internal tests";

        private readonly IOptions<ValidationSettings> _options;
        private readonly RuleDocumentationProvider _ruleDocumentation;
        private string dataFormatId = "11000";
        private string dataFormatVersion = "2.0";

        private Planvarsel_11000_2_Validator _formValidator;

        public Planvarsel_11000_2_ValidatorTests()
        {
            var services = new ServiceCollection();
            services.AddMemoryCache();
            var serviceProvider = services.BuildServiceProvider();

            var memoryCache = serviceProvider.GetService<IMemoryCache>();

            var initConfiguration = TestHelper.InitConfiguration();
            var ValidationApiSettings = initConfiguration.GetSection("ValidationApi").Get<ValidationSettings>();
            var validationMessageComposer = MockDataSource.GetValidationMessageComposer();
            var codeListService = MockDataSource.CodeListService(true, true, "gyldig");
            var postalCodeService = MockDataSource.ValidatePostnr(true, "Bø i Telemark", "true");
            var decryptionFactory = MockDataSource.DecryptText("09107300292");

            _options = Options.Create(ValidationApiSettings);
            _ruleDocumentation = new RuleDocumentationProvider(_options, memoryCache);
            _formValidator = new Planvarsel_11000_2_Validator(validationMessageComposer,codeListService, postalCodeService, decryptionFactory);
        }


        //TODO Implement LocalHost Tests
        [Fact(Skip = skip, DisplayName = "Rule Without Text In localhost")]
        [Trait("Category", "Documentation")]
        public void RuleWithoutTextInLocalhost()
        {
            bool anyRuleWithoutText = true;

            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            _rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());
            var fomRulesLocalhostJsonString = JsonConvert.SerializeObject(_rulesLocalhost);


            newValidationReport.Should().NotBeNull();

            RuleModelDocumentation[] whiteoutText = { };
            if (_rulesLocalhost != null && _rulesLocalhost.Any())
            {
                whiteoutText = _rulesLocalhost.Where(r => r.Description.Contains("could", StringComparison.CurrentCultureIgnoreCase)).ToArray();
            }
            var validatorRulesJson = JArray.FromObject(whiteoutText);
            anyRuleWithoutText = whiteoutText.Any();

            anyRuleWithoutText.Should().Be(false);
        }
    }
}
