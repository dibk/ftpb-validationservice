using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Validation;
using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators.Ansako;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.ReporterTests.DocumentationTests
{
    public class KontrollerklaeringAnsako_10002_1_MessageStorageTests
    {
        private List<RuleModelDocumentation> _rulesLocalhost;
        private readonly RuleDocumentationProvider _ruleDocumentation;
        private string dataFormatId = "10002";
        private string dataFormatVersion = "1";
        private readonly KontrollerklaeringAnsako_10002_1_Validator _formValidator;
        private readonly IOptions<ValidationSettings> _options;


        //https://stackoverflow.com/a/14845729
        const string skip = "internal tests";

        public KontrollerklaeringAnsako_10002_1_MessageStorageTests()
        {

            //

            var services = new ServiceCollection();
            services.AddMemoryCache();
            var serviceProvider = services.BuildServiceProvider();

            var memoryCache = serviceProvider.GetService<IMemoryCache>();

            var initConfiguration = TestHelper.InitConfiguration();
            var ValidationApiSettings = initConfiguration.GetSection("ValidationApi").Get<ValidationSettings>();

            _options = Options.Create(ValidationApiSettings);
            _ruleDocumentation = new RuleDocumentationProvider(_options, memoryCache);


            var validationMessageComposer = MockDataSource.GetValidationMessageComposer();

            var checklistService = MockDataSource.GetCheckpoints("AT");
            var decryptionFactory = MockDataSource.DecryptText("09107300292");

            //_codeListService = MockDataSource.CodeListService(FtbKodeListeEnum.Partstype, true);
            var codeListService = MockDataSource.CodeListService(true, true, "gyldig");
            var postalCodeService = MockDataSource.ValidatePostnr(true, "Bø i Telemark", "true");

            var matrikkelService = MockDataSource.AnyByggWithEiendomsidentifikasjonInMatrikkel(true);

            _formValidator = new KontrollerklaeringAnsako_10002_1_Validator(validationMessageComposer, codeListService, postalCodeService, checklistService, matrikkelService, decryptionFactory);

        }

        [Fact(Skip = skip, DisplayName = "Rule Without Text In Dev")]
        [Trait("Category", "Documentation")]
        public void RuleWithoutTextInDev()
        {
            var branch = "Dev";
            var rulesInBranch = _ruleDocumentation.GetRuleWithoutMessageText(dataFormatId, dataFormatVersion, new[] { branch });
            rulesInBranch.First().Value.Count.Should().Be(0);

        }
        [Fact(Skip = skip, DisplayName = "Rule Without Text In Test")]
        [Trait("Category", "Documentation")]
        public void RuleWithoutTextInTest()
        {
            var branch = "Test";
            var rulesInBranch = _ruleDocumentation.GetRuleWithoutMessageText(dataFormatId, dataFormatVersion, new[] { branch });
            rulesInBranch.First().Value.Count.Should().Be(0);
        }
        [Fact(Skip = skip, DisplayName = "Rule Without Text In Prod")]
        [Trait("Category", "Documentation")]
        public void RuleWithoutTextInProd()
        {
            var branch = "Prod";
            var rulesInBranch = _ruleDocumentation.GetRuleWithoutMessageText(dataFormatId, dataFormatVersion, new[] { branch });
            rulesInBranch.First().Value.Count.Should().Be(0);
        }

        [Fact(Skip = skip, DisplayName = "Different rules In Dev&Test")]
        [Trait("Category", "Documentation")]
        public void DifferentCountOfRuler_DevAndTest()
        {
            var result = _ruleDocumentation.GetRulesNotImplemented(dataFormatId, dataFormatVersion, "Dev", "Test");
            bool difference = false;
            if (result == null)
            {
                difference = true;
            }
            else
            {
                foreach (var validationRule in result)
                {
                    if (validationRule.Value.Count > 0)
                    {
                        difference = true;
                        break;
                    }
                }
            }

            var JsonDictionary = JObject.FromObject(result);

            difference.Should().Be(false);
        }
        [Fact(Skip = skip, DisplayName = "Different Count In Test&Prod")]
        [Trait("Category", "Documentation")]
        public void DifferentCountOfRuler_TestAndProd()
        {
            var result = _ruleDocumentation.GetRulesNotImplemented(dataFormatId, dataFormatVersion, "Test", "Prod");
            bool difference = false;
            if (result == null)
            {
                difference = true;
            }
            else
            {
                foreach (var validationRule in result)
                {
                    if (validationRule.Value.Count > 0)
                    {
                        difference = true;
                        break;
                    }
                }
            }

            var JsonDictionary = JObject.FromObject(result);

            difference.Should().Be(false);
        }

        [Fact(Skip = skip, DisplayName = "Different messagetext In Dev&Test")]
        [Trait("Category", "Documentation")]
        public void DifferentTextInRule_DevAndTest()
        {
            var result = _ruleDocumentation.GetRulesWithDifferentMessageText(dataFormatId, dataFormatVersion, "Dev", "Test");
            bool difference = false;
            if (result == null)
            {
                difference = true;
            }
            else
            {
                foreach (var validationRule in result)
                {
                    if (validationRule.Value.Count > 0)
                    {
                        difference = true;
                        break;
                    }
                }
            }

            var JsonDictionary = JObject.FromObject(result);

            difference.Should().Be(false);
        }
        [Fact(Skip = skip, DisplayName = "Different messagetext In Test&Prod")]
        [Trait("Category", "Documentation")]
        public void DifferentTextInRule_TestAndProd()
        {
            var result = _ruleDocumentation.GetRulesWithDifferentMessageText(dataFormatId, dataFormatVersion, "Test", "Prod");
            bool difference = false;
            if (result == null)
            {
                difference = true;
            }
            else
            {
                foreach (var validationRule in result)
                {
                    if (validationRule.Value.Count > 0)
                    {
                        difference = true;
                        break;
                    }
                }

            }
            var JsonDictionary = JObject.FromObject(result);
            difference.Should().Be(false);
        }
        [Fact(Skip = skip, DisplayName = "Different Severity In Dev&Test")]
        [Trait("Category", "Documentation")]
        public void DifferentSeverityInRule_DevAndTest()
        {
            var result = _ruleDocumentation.GetRulesWithDifferenceSeverityLevel(dataFormatId, dataFormatVersion, "Dev", "Test");
            bool difference = false;
            if (result == null)
            {
                difference = true;
            }
            else
            {
                foreach (var validationRule in result)
                {
                    if (validationRule.Value.Count > 0)
                    {
                        difference = true;
                        break;
                    }
                }
                var JsonDictionary = JObject.FromObject(result);

                difference.Should().Be(false);
            }
        }

        [Fact(Skip = skip, DisplayName = "Different severity In Test&Prod")]
        [Trait("Category", "Documentation")]
        public void DifferentSeverityInRule_TestAndProd()
        {
            var result = _ruleDocumentation.GetRulesWithDifferenceSeverityLevel(dataFormatId, dataFormatVersion, "Test", "Prod");
            bool difference = false;
            if (result == null)
            {
                difference = true;
            }
            else
            {
                foreach (var validationRule in result)
                {
                    if (validationRule.Value.Count > 0)
                    {
                        difference = true;
                        break;
                    }
                }
            }
            var JsonDictionary = JObject.FromObject(result);

            difference.Should().Be(false);
        }
        [Fact(Skip = skip, DisplayName = "Different RuleId In Dev&Test")]
        [Trait("Category", "Documentation")]
        public void DifferentRuleIdInRule_DevAndTest()
        {
            var result = _ruleDocumentation.GetDifferentRuleIdInRule(dataFormatId, dataFormatVersion, "Dev", "Test");
            bool difference = false;
            if (result == null)
            {
                difference = true;
            }
            else
            {
                foreach (var validationRule in result)
                {
                    if (validationRule.Value.Count > 0)
                    {
                        difference = true;
                        break;
                    }
                }
            }

            var JsonDictionary = JObject.FromObject(result);

            difference.Should().Be(false);
        }

        [Fact(Skip = skip, DisplayName = "Different RuleId In Test&Prod")]
        [Trait("Category", "Documentation")]
        public void DifferentRuleIdInRule_TestAndProd()
        {
            var result = _ruleDocumentation.GetDifferentRuleIdInRule(dataFormatId, dataFormatVersion, "Test", "Prod");
            bool difference = false;
            if (result == null)
            {
                difference = true;
            }
            else
            {
                foreach (var validationRule in result)
                {
                    if (validationRule.Value.Count > 0)
                    {
                        difference = true;
                        break;
                    }
                }
            }

            var JsonDictionary = JObject.FromObject(result);

            difference.Should().Be(false);
        }
        //TODO Implement LocalHost Tests
        [Fact(Skip = skip, DisplayName = "Rule Without Text In localhost")]
        [Trait("Category", "Documentation")]
        [Trait("Implementation", "LocalHost")]
        public void RuleWithoutTextInLocalhost()
        {
            bool anyRuleWithoutText = true;

            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            _rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            newValidationReport.Should().NotBeNull();
            RuleModelDocumentation[] whiteoutText = { };
            if (_rulesLocalhost != null && _rulesLocalhost.Any())
            {
                whiteoutText = _rulesLocalhost.Where(r => r.Description.Contains("could", StringComparison.CurrentCultureIgnoreCase)).ToArray();
            }
            anyRuleWithoutText = whiteoutText.Any();

            anyRuleWithoutText.Should().Be(false);
        }

        [Fact(Skip = skip, DisplayName = "Different rules Message Text In LocalHost&Dev")]
        [Trait("Category", "Documentation")]
        [Trait("Implementation", "LocalHost")]
        public void DifferentMessageText_LocalhostAndDev()
        {

            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            var rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            var compareBranch = "Dev";
            var ruleCompareBranch = _ruleDocumentation.GetValidationRules(dataFormatId, dataFormatVersion, compareBranch).Result;
            var dictionary = new Dictionary<string, List<RuleModelDocumentation>>();
            dictionary.Add("LocalHost", rulesLocalhost);
            dictionary.Add(compareBranch, ruleCompareBranch);

            var result = _ruleDocumentation.GetRulesWithDifferentMessageText(dictionary);

            result.Should().BeEmpty();
        }

        [Fact(Skip = skip, DisplayName = "Different rules Severity Level In LocalHost&Dev")]
        [Trait("Category", "Documentation")]
        [Trait("Implementation", "LocalHost")]
        public void GetRulesWithDifferenceSeverityLevel_LocalhostAndDev()
        {

            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            var rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            var compareBranch = "Dev";
            var ruleCompareBranch = _ruleDocumentation.GetValidationRules(dataFormatId, dataFormatVersion, compareBranch).Result;
            var dictionary = new Dictionary<string, List<RuleModelDocumentation>>();
            dictionary.Add("LocalHost", rulesLocalhost);
            dictionary.Add(compareBranch, ruleCompareBranch);

            var result = _ruleDocumentation.GetRulesWithDifferenceSeverityLevel(dictionary);

            result.Should().BeEmpty();
        }

        [Fact(Skip = skip, DisplayName = "Different rulesId In LocalHost&Dev")]
        [Trait("Category", "Documentation")]
        [Trait("Implementation", "LocalHost")]
        public void GetDifferentRuleIdInRule_LocalhostAndDev()
        {

            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            var rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            var compareBranch = "Dev";
            var ruleCompareBranch = _ruleDocumentation.GetValidationRules(dataFormatId, dataFormatVersion, compareBranch).Result;
            var dictionary = new Dictionary<string, List<RuleModelDocumentation>>();
            dictionary.Add("LocalHost", rulesLocalhost);
            dictionary.Add(compareBranch, ruleCompareBranch);

            var result = _ruleDocumentation.GetDifferentRuleIdInRule(dictionary);
            
            var JsonDictionary = JObject.FromObject(result);


            result.Should().BeEmpty();
        }

        [Fact(Skip = skip, DisplayName = "Different rules In LocalHost&Dev")]
        [Trait("Category", "Documentation")]
        [Trait("Implementation", "LocalHost")]
        public void GetRulesNotImplemented_LocalhostAndDev()
        {

            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            var rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            var compareBranch = "Dev";
            var ruleCompareBranch = _ruleDocumentation.GetValidationRules(dataFormatId, dataFormatVersion, compareBranch).Result;
            var dictionary = new Dictionary<string, List<RuleModelDocumentation>>();
            dictionary.Add("LocalHost", rulesLocalhost);
            dictionary.Add(compareBranch, ruleCompareBranch);

            var result = _ruleDocumentation.GetRulesNotImplemented(dictionary);

            result.Should().BeEmpty();
        }

}
}
