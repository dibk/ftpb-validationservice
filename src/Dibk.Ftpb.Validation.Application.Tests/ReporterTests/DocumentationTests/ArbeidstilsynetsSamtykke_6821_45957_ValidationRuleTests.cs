using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Validation;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators.Atil;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.ReporterTests.DocumentationTests
{
    public class ArbeidstilsynetsSamtykke_6821_45957_ValidationRuleTests
    {
        private string dataFormatId = "7063";
        private string dataFormatVersion = "47177";

        private readonly ArbeidstilsynetsSamtykke2_45957_Validator _formValidator;
        private readonly RuleDocumentationProvider _ruleDocumentation;
        private List<RuleModelDocumentation> _rulesLocalhost;
        private const string skip = "internal use";

        public ArbeidstilsynetsSamtykke_6821_45957_ValidationRuleTests()
        {
            var codeListService = MockDataSource.CodeListService(true);
            var postalCodeService = MockDataSource.ValidatePostnr(true, "Bø i Telemark", "true");
            FormValidatorConfiguration formValidatorConfiguration = new FormValidatorConfiguration();
            var checklistService = MockDataSource.GetCheckpoints("AT");
            var atilFeeCalculatorApiHttpClient = MockDataSource.GetFeeInformation(2240, "3a");
            var matrikkelService = MockDataSource.AnyByggWithEiendomsidentifikasjonInMatrikkel(true);
            var decryptionFactory = MockDataSource.DecryptText("09107300292");

            var validationMessageComposer = MockDataSource.GetValidationMessageComposer();

            // ATIL Feature - Endres etter 20250217
            _formValidator = new ArbeidstilsynetsSamtykke2_45957_Validator(validationMessageComposer, codeListService, postalCodeService, checklistService, atilFeeCalculatorApiHttpClient, decryptionFactory, matrikkelService, FeatureManagerMocks.AtilFeatureManager());
        }

        //TODO Implement LocalHost Tests
        [Fact(Skip = skip, DisplayName = "Rule Without Text In localhost")]
        [Trait("Category", "Documentation")]
        [Trait("Implementation", "LocalHost")]
        public void RuleWithoutTextInLocalhost()
        {
            bool anyRuleWithoutText = true;

            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            _rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            newValidationReport.Should().NotBeNull();

            if (_rulesLocalhost != null && _rulesLocalhost.Any())
            {
                var whiteoutText = _rulesLocalhost.Where(r => r.Description.Contains("could", StringComparison.CurrentCultureIgnoreCase)).ToArray();
                anyRuleWithoutText = whiteoutText.Any();
            }

            anyRuleWithoutText.Should().Be(false);
        }

        [Fact(Skip = skip, DisplayName = "Different rules Message Text In LocalHost&Dev")]
        [Trait("Category", "Documentation")]
        public void DifferentMessageText_LocalhostAndDev()
        {
            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            var rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            var compareBranch = "Dev";
            var ruleCompareBranch = _ruleDocumentation.GetValidationRules(dataFormatId, dataFormatVersion, compareBranch).Result;
            var dictionary = new Dictionary<string, List<RuleModelDocumentation>>();
            dictionary.Add("LocalHost", rulesLocalhost);
            dictionary.Add(compareBranch, ruleCompareBranch);

            var result = _ruleDocumentation.GetRulesWithDifferentMessageText(dictionary);

            result.Should().BeEmpty();
        }

        [Fact(Skip = skip, DisplayName = "Different rules Severity Level In LocalHost&Dev")]
        [Trait("Category", "Documentation")]
        public void GetRulesWithDifferenceSeverityLevel_LocalhostAndDev()
        {
            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            var rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            var compareBranch = "Dev";
            var ruleCompareBranch = _ruleDocumentation.GetValidationRules(dataFormatId, dataFormatVersion, compareBranch).Result;
            var dictionary = new Dictionary<string, List<RuleModelDocumentation>>();
            dictionary.Add("LocalHost", rulesLocalhost);
            dictionary.Add(compareBranch, ruleCompareBranch);

            var result = _ruleDocumentation.GetRulesWithDifferenceSeverityLevel(dictionary);

            result.Should().BeEmpty();
        }

        [Fact(Skip = skip, DisplayName = "Different rulesId In LocalHost&Dev")]
        [Trait("Category", "Documentation")]
        public void GetDifferentRuleIdInRule_LocalhostAndDev()
        {
            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            var rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            var compareBranch = "Dev";
            var ruleCompareBranch = _ruleDocumentation.GetValidationRules(dataFormatId, dataFormatVersion, compareBranch).Result;
            var dictionary = new Dictionary<string, List<RuleModelDocumentation>>();
            dictionary.Add("LocalHost", rulesLocalhost);
            dictionary.Add(compareBranch, ruleCompareBranch);

            var result = _ruleDocumentation.GetDifferentRuleIdInRule(dictionary);

            result.Should().BeEmpty();
        }

        [Fact(Skip = skip, DisplayName = "Different rules In LocalHost&Dev")]
        [Trait("Category", "Documentation")]
        public void GetRulesNotImplemented_LocalhostAndDev()
        {
            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            var rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            var compareBranch = "Dev";
            var ruleCompareBranch = _ruleDocumentation.GetValidationRules(dataFormatId, dataFormatVersion, compareBranch).Result;
            var dictionary = new Dictionary<string, List<RuleModelDocumentation>>();
            dictionary.Add("LocalHost", rulesLocalhost);
            dictionary.Add(compareBranch, ruleCompareBranch);

            var result = _ruleDocumentation.GetRulesNotImplemented(dictionary);

            result.Should().BeEmpty();
        }
    }
}