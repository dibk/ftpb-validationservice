using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Validation;
using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Reporter.DataBase;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.ReporterTests.DocumentationTests
{
    public class DbDocumentationTests
    {
        private const string skip = "internal use";

        private List<string> _formList;
        private readonly RuleDocumentationProvider _ruleDocumentation;
        private readonly List<ValidationMessageStorageEntry> _validationMessageStorageEntry;

        public DbDocumentationTests()
        {
            _formList = new List<string>()
            {
                "10000.1","10001.1","10002.1",
                "7063.47177",
                "6821.45957","5547.41999"
            };
            var services = new ServiceCollection();
            services.AddMemoryCache();
            var serviceProvider = services.BuildServiceProvider();

            var memoryCache = serviceProvider.GetService<IMemoryCache>();
            var initConfiguration = TestHelper.InitConfiguration();
            var ValidationApiSettings = initConfiguration.GetSection("ValidationApi").Get<ValidationSettings>();

            var options = Options.Create(ValidationApiSettings);
            _ruleDocumentation = new RuleDocumentationProvider(options, memoryCache);

            // ATIL Feature - Endres etter 20250217 - START
            _validationMessageStorageEntry = new ValidationMessageDb(FeatureManagerMocks.AtilFeatureManager()).InitiateMessageRepository();

        }

        [Fact(Skip = skip, DisplayName = "Duplicate Rules In DB")]
        [Trait("Category", "Documentation")]
        public void DuplicateMessageInDb()
        {
            List<ValidationMessageStorageEntry> messageList = new List<ValidationMessageStorageEntry>();
            var duplicateRule = new Dictionary<string, List<ValidationMessageStorageEntry>>();
            foreach (var messageStorageEntry in _validationMessageStorageEntry)
            {
                var duplicateMessages = _validationMessageStorageEntry.Where(m =>
                    m.Rule == messageStorageEntry.Rule
                    && m.XPath == messageStorageEntry.XPath
                    && m.Message == messageStorageEntry.Message
                    && m.Messagetype == messageStorageEntry.Messagetype
                    //&& m.ChecklistReference == messageStorageEntry.ChecklistReference
                    ).ToArray();

                if (duplicateMessages.Length > 1)
                {
                    if (duplicateRule.ContainsKey(messageStorageEntry.Message))
                    {
                        duplicateRule[messageStorageEntry.Message].Concat(duplicateMessages);
                    }
                    else
                    {
                        duplicateRule.Add(messageStorageEntry.Message, duplicateMessages.ToList());
                    }
                }
            }
            var jsonDictionary = JObject.FromObject(duplicateRule);

            duplicateRule.Should().BeEmpty();

        }
        [Fact(Skip = skip, DisplayName = "Unused rules  in DB - All branches")]
        [Trait("Category", "Documentation")]
        public void ExtraRulesInAllBranches()
        {
            var branchs = new string[] { "Dev", "Test", "Prod" };
            var messageStorageDictionary = new Dictionary<string, List<ValidationMessageStorageEntry>>();

            foreach (var formData in _formList)
            {
                var dataformatIdAndDataFormatVersion = GetDataformatIdAndDataFormatVersion(formData);

                var dataFormatId = dataformatIdAndDataFormatVersion[0];
                var dataFormatVersion = dataformatIdAndDataFormatVersion[1];
                foreach (var branch in branchs)
                {
                    var rulesInBranch = _ruleDocumentation.GetValidationRules(dataFormatId, dataFormatVersion, branch).Result;
                    var messageStorage = GetMessageStoragefromRuleList(rulesInBranch, dataFormatId, dataFormatVersion);
                    messageStorageDictionary.Add($"{branch}-{formData}", messageStorage);
                }
            }

            IEnumerable<ValidationMessageStorageEntry> messagesUseInRules = _validationMessageStorageEntry;
            foreach (var messagesInBranch in messageStorageDictionary)
            {
                var branchMessage = messagesInBranch.Value;
                if (branchMessage == null || !branchMessage.Any()) continue;

                messagesUseInRules = messagesUseInRules.Except(branchMessage);
            }
            var jsonDictionary = JArray.FromObject(messagesUseInRules);

            messagesUseInRules.Count().Should().Be(0);

        }
        [Theory(Skip = skip, DisplayName = "Unused Rules in DB")]
        [Trait("Category", "Documentation")]
        [InlineData("Dev")]
        [InlineData("Test")]
        [InlineData("Prod")]
        public void ExtraRulesInDev(string branch)
        {
            var messageStorageDictionary = new Dictionary<string, List<ValidationMessageStorageEntry>>();

            foreach (var formData in _formList)
            {
                var dataformatIdAndDataFormatVersion = GetDataformatIdAndDataFormatVersion(formData);

                var dataFormatId = dataformatIdAndDataFormatVersion[0];
                var dataFormatVersion = dataformatIdAndDataFormatVersion[1];
                var rulesInBranch = _ruleDocumentation.GetValidationRules(dataFormatId, dataFormatVersion, branch).Result;
                var messageStorage = GetMessageStoragefromRuleList(rulesInBranch, dataFormatId, dataFormatVersion);
                messageStorageDictionary.Add(formData, messageStorage);
            }

            IEnumerable<ValidationMessageStorageEntry> messagesUseInRules = _validationMessageStorageEntry;
            foreach (var messagesInBranch in messageStorageDictionary)
            {
                var branchMessage = messagesInBranch.Value;
                if (branchMessage == null || !branchMessage.Any()) continue;

                messagesUseInRules = messagesUseInRules.Except(branchMessage);
            }
            var JsonDictionary = JArray.FromObject(messagesUseInRules);

            messagesUseInRules.Count().Should().Be(0);

        }
        
        [Fact(Skip = skip, DisplayName = "kommunensSaksnummer rules count in PROD")]
        [Trait("Category", "Documentation")]
        public void kommunensSaksnummerRulesCountInProd()
        {
            var branch = "Prod";
            var ruleSummaryDictionary = new Dictionary<string, List<RuleModelDocumentation>>();
            var xmlNode = "/kommunensSaksnummer";
            var formRuleForXmlNode = _ruleDocumentation.GetRuleForXmlNodeDocumentation(_formList.ToArray(), xmlNode, branch);

            var ruleSummaryForXmlNode = _ruleDocumentation.RuleInfoUse(formRuleForXmlNode);
            ruleSummaryForXmlNode.Count.Should().Be(8);
            ruleSummaryDictionary.Add(branch, ruleSummaryForXmlNode);
        }

        // common methods
        private List<ValidationMessageStorageEntry> GetMessageStoragefromRuleList(List<RuleModelDocumentation> ruleList, string dataFormatId, string dataFormatVersion)
        {
            if (ruleList == null || !ruleList.Any()) return null;

            List<ValidationMessageStorageEntry> messageList = new List<ValidationMessageStorageEntry>();
            foreach (var rule in ruleList)
            {
                var ruleXpath = GetRuleXpathWithoutFormName(rule.Xpath);
                var messageStorageEntries = _validationMessageStorageEntry
                    .Where(m => m.Message == rule.Description && m.XPath == ruleXpath).ToArray();

                ValidationMessageStorageEntry messageStorageEntry = null;
                if (messageStorageEntries.Length > 1)
                {
                    var specificRule = messageStorageEntries.Where(m => m.DataFormatId == dataFormatId && m.DataFormatVersion == dataFormatVersion).ToArray();
                    messageStorageEntry = specificRule.Length == 1 ? specificRule.First() : messageStorageEntries.First();
                }
                else
                {
                    messageStorageEntry = messageStorageEntries.FirstOrDefault();
                }
                messageList.Add(messageStorageEntry);
            }
            return messageList;
        }
        private string GetRuleXpathWithoutFormName(string ruleXpath)
        {
            var indexOfNoko = ruleXpath.IndexOf("/");
            var xpath = ruleXpath.Substring(indexOfNoko);
            return xpath;
        }
        private string[] GetDataformatIdAndDataFormatVersion(string dfiDfv)
        {
            string[] formData = dfiDfv.Split(".");
            return formData;
        }
    }
}
