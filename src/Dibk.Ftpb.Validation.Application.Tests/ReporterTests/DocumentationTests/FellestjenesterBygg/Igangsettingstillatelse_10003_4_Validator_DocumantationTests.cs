﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Validation;
using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using FluentAssertions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.ReporterTests.DocumentationTests.FellestjenesterBygg
{
    public class Igangsettingstillatelse_10003_4_Validator_DocumantationTests
    {
        private List<RuleModelDocumentation> _rulesLocalhost;
        //https://stackoverflow.com/a/14845729
        //const string skip = "internal tests";
        const string skip = null;

        private readonly IOptions<ValidationSettings> _options;
        private readonly RuleDocumentationProvider _ruleDocumentation;
        private string dataFormatId = "10003";
        private string dataFormatVersion = "4";

        private Igangsettingstillatelse_10003_4_Validator _formValidator;

        public Igangsettingstillatelse_10003_4_Validator_DocumantationTests()
        {
            var services = new ServiceCollection();
            services.AddMemoryCache();
            var serviceProvider = services.BuildServiceProvider();

            var memoryCache = serviceProvider.GetService<IMemoryCache>();

            var initConfiguration = TestHelper.InitConfiguration();
            var codeListService = MockDataSource.CodeListService(true, true, "gyldig");
            var validationMessageComposer = MockDataSource.GetValidationMessageComposer();
            var matrikkelService = MockDataSource.AnyByggWithEiendomsidentifikasjonInMatrikkel(true);
            var checklistService = MockDataSource.GetCheckpoints("IG");
            var postcodeService = MockDataSource.ValidatePostnr(true,"Bø i Telemark");
            var decryptionFactory = MockDataSource.DecryptText("12345678");

            _formValidator = new Igangsettingstillatelse_10003_4_Validator(validationMessageComposer, codeListService, matrikkelService, checklistService, postcodeService, decryptionFactory);
        }

        [Fact(Skip = skip, DisplayName = "Rule Without Text In localhost")]
        [Trait("Category", "Documentation")]
        public void RuleWithoutTextInLocalhost()
        {
            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            _rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());
            var fomRulesLocalhostJsonString = JsonConvert.SerializeObject(_rulesLocalhost);


            newValidationReport.Should().NotBeNull();

            RuleModelDocumentation[] rulesWithoutText = { };
            if (_rulesLocalhost != null && _rulesLocalhost.Any())
            {
                rulesWithoutText = _rulesLocalhost.Where(r => r.Description.Contains("could", StringComparison.CurrentCultureIgnoreCase)).ToArray();
            }
            var validatorRulesJson = JArray.FromObject(rulesWithoutText);
            var rulesWithoutTextCount = rulesWithoutText.Length;

            rulesWithoutTextCount.Should().Be(0);
        }

        [Theory(Skip = null, DisplayName = "Entity rules Text counts")]
        [Trait("Category", "Documentation")]
        [InlineData("/utfallBesvarelse/utfallSvar{0}/utfallType",7)]
        [InlineData("/utfallBesvarelse/utfallSvar{0}/utloestFraSjekkpunkt", 1)]
        [InlineData("/utfallBesvarelse/utfallSvar{0}/tema", 4)]
        [InlineData("/utfallBesvarelse/utfallSvar{0}/tittel", 1)]
        [InlineData("/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere", 1)]
        [InlineData("/utfallBesvarelse/utfallSvar{0}/erUtfallBesvart", 1)]
        [InlineData("/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype", 6)]
        [InlineData("/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/versjonsdato", 1)]
        [InlineData("/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/versjonsnummer", 1)]
        [InlineData("/utfallBesvarelse", 28)]
        public void UtfallBesvarelse(string xmlNode,int rulesCount)
        {
            bool anyRuleWithoutText = true;

            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            _rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());


            newValidationReport.Should().NotBeNull();

            RuleModelDocumentation[] xmlNodeRules = { };
            if (_rulesLocalhost != null && _rulesLocalhost.Any())
            {
                xmlNodeRules = _rulesLocalhost.Where(r => r.Xpath.Contains(xmlNode, StringComparison.CurrentCultureIgnoreCase)).ToArray();
            }
            var validatorRulesJson = JArray.FromObject(xmlNodeRules);
            var xmlNodeRulesCount = xmlNodeRules.Length;
            xmlNodeRulesCount.Should().Be(rulesCount);
        } 
        
        [Theory(Skip = null, DisplayName = "Entity rules Text counts")]
        [Trait("Category", "Documentation")]
        [InlineData("/metadata/fraSluttbrukersystem", 1)]
        [InlineData("/metadata/foretrukketSpraak/", 4)]
        public void Metadata(string xmlNode,int rulesCount)
        {
            bool anyRuleWithoutText = true;

            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            _rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            newValidationReport.Should().NotBeNull();

            RuleModelDocumentation[] xmlNodeRules = { };
            if (_rulesLocalhost != null && _rulesLocalhost.Any())
            {
                xmlNodeRules = _rulesLocalhost.Where(r => r.Xpath.Contains(xmlNode, StringComparison.CurrentCultureIgnoreCase)).ToArray();
            }
            var validatorRulesJson = JArray.FromObject(xmlNodeRules);
            var xmlNodeRulesCount = xmlNodeRules.Length;
            xmlNodeRulesCount.Should().Be(rulesCount);
        }
        
        [Theory(Skip = null, DisplayName = "Entity rules Text counts")]
        [Trait("Category", "Documentation")]
        [InlineData("delsoeknader", 10)]
        public void Delsoknader(string xmlNode,int rulesCount)
        {
            bool anyRuleWithoutText = true;

            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            _rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            newValidationReport.Should().NotBeNull();

            RuleModelDocumentation[] xmlNodeRules = { };
            if (_rulesLocalhost != null && _rulesLocalhost.Any())
            {
                xmlNodeRules = _rulesLocalhost.Where(r => r.Xpath.Contains(xmlNode, StringComparison.CurrentCultureIgnoreCase)).ToArray();
            }
            var validatorRulesJson = JArray.FromObject(xmlNodeRules);
            var xmlNodeRulesCount = xmlNodeRules.Length;
            xmlNodeRulesCount.Should().Be(rulesCount);
        }
        [Theory(Skip = null, DisplayName = "Entity rules Text counts")]
        [Trait("Category", "Documentation")]
        [InlineData("vedlegg", 13)]
        public void Delsoknader1(string xmlNode,int rulesCount)
        {
            bool anyRuleWithoutText = true;

            var newValidationReport = _formValidator.StartValidation(new ValidationInput());
            var validationRules = newValidationReport.ValidationRules;
            _rulesLocalhost = RuleDocumentationComposer.GetRuleDocumentationModel(validationRules.ToList());

            newValidationReport.Should().NotBeNull();

            RuleModelDocumentation[] xmlNodeRules = { };
            if (_rulesLocalhost != null && _rulesLocalhost.Any())
            {
                xmlNodeRules = _rulesLocalhost.Where(r => r.Xpath.Contains(xmlNode, StringComparison.CurrentCultureIgnoreCase)).ToArray();
            }
            var validatorRulesJson = JArray.FromObject(xmlNodeRules);
            var xmlNodeRulesCount = xmlNodeRules.Length;
            xmlNodeRulesCount.Should().Be(rulesCount);
        }
    }
}

