﻿using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.IO;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.DeserializeTests
{
    public  class AnsvarsrettAnsako_10000_DeserializeTests
    {
        [Fact]
        public void FormTest()
        {
            var xmlData_10000 = File.ReadAllText(@"Data\Ansako\ErklaeringAnsvarsrett_1.xml");

            var xmlData_10000_Common = SerializeUtil.DeserializeFromString<AnsakoAnsvarsrett>(xmlData_10000);
            xmlData_10000_Common.Should().NotBeNull();
        }
        [Fact]
        public void ForetakTest()
        {
            var xmlData_10000 = File.ReadAllText(@"Data\Ansako\ErklaeringAnsvarsrett_1.xml");

            var xmlData_10000_Common = SerializeUtil.DeserializeFromString<AnsakoAnsvarsrett>(xmlData_10000);
            var foretak = xmlData_10000_Common.Ansvarsretts.Foretak;
            foretak.Should().NotBeNull();
        }
        [Fact]
        public void AnsvarsomraadesTest()
        {
            var xmlData_10000 = File.ReadAllText(@"Data\Ansako\ErklaeringAnsvarsrett_1.xml");

            var xmlData_10000_Common = SerializeUtil.DeserializeFromString<AnsakoAnsvarsrett>(xmlData_10000);
            var ansvarsomraades = xmlData_10000_Common.Ansvarsretts.Ansvarsomraader;
            ansvarsomraades.Should().NotBeNull();
        }
        [Fact]
        public void EiendomByggestedTest()
        {
            var xmlData_10000 = File.ReadAllText(@"Data\Ansako\ErklaeringAnsvarsrett_1.xml");

            var xmlData_10000_Common = SerializeUtil.DeserializeFromString<AnsakoAnsvarsrett>(xmlData_10000);
            var eiendomByggested = xmlData_10000_Common.eiendomByggested;
            eiendomByggested.Should().NotBeNull();
        }
    }
}
