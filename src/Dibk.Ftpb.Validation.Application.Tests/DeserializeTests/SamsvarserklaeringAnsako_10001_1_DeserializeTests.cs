﻿using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.IO;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.DeserializeTests
{
    public class SamsvarserklaeringAnsako_10001_1_DeserializeTests
    {
        private readonly AnsakoSamsvarserklaering _form;

        public SamsvarserklaeringAnsako_10001_1_DeserializeTests()
        {
           var xmlData = File.ReadAllText(@"Data\Ansako\Samsvarserklaering_1.xml");
            _form = SerializeUtil.DeserializeFromString<AnsakoSamsvarserklaering>(xmlData);
        }

        [Fact]
        public void FormTest()
        {
            _form.Should().NotBeNull();
        }
        [Fact]
        public void KommunensSaksnummer_Saksaar_Test()
        {
            _form?.KommunensSaksnummer?.Saksaar.Should().NotBeNull();
        }
        [Fact]
        public void KommunensSaksnummer_Sakssekvensnummer_Test()
        {
            _form.KommunensSaksnummer?.Sakssekvensnummer.Should().NotBeNull();
        }
        [Fact]
        public void KommunensSaksnummer_Prosjektnavn_Test()
        {
           _form.Prosjektnavn.Should().NotBeNull();
        }
        [Fact]
        public void KommunensSaksnummer_Prosjektnr_Test()
        {
           _form.Prosjektnr.Should().NotBeNull();
        }
        [Fact]
        public void ansvarligSoeker()
        {
           _form.AnsvarligSoeker.Should().NotBeNull();
        }

    }
}
