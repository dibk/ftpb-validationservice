using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.DataSources.ApiServices.CodeList
{
    public class CodeListTests : IClassFixture<CodeListConfiguration>
    {
        private const string skip = "ExternalApiCall";
        private readonly ICodeListService _codeListService;

        public CodeListTests(CodeListConfiguration codeListConfiguration)
        {
            this._codeListService = codeListConfiguration.CodeListService;
        }

        [Theory(Skip = skip, DisplayName = "Byggesoknad test")]
        [Trait("Category", "ExternalApiCall")]
        [InlineData(FtbKodeListeEnum.Partstype, "Privatperson", "Privatperson")]
        [InlineData(FtbKodeListeEnum.Anleggstype, "brygger", "Brygger")]
        [InlineData(FtbKodeListeEnum.Naeringsgruppe, "X", "Bolig")]
        [InlineData(FtbKodeListeEnum.tiltaksformal, "Fritidsbolig", "Fritidsbolig")]
        [InlineData(FtbKodeListeEnum.Bygningstype, "829", "Annen beredskapsbygning")]
        [InlineData(FtbKodeListeEnum.tiltaktype, "bruksendring", "Bruksendring")]
        [InlineData(FtbKodeListeEnum.funksjon, "KONTROLL", "Ansvarlig kontrollerende")]
        [InlineData(FtbKodeListeEnum.tiltaksklasse, "3", "3")]
        [InlineData(FtbKodeListeEnum.Avfallklasse, "blandetAvfall", "Blandet avfall")]
        [InlineData(FtbKodeListeEnum.Avfalldisponeringsmåte, "direkteFormaal", "Levert direkte til gjenvinning til utfyllingsformål/teknisk formål, eks. betong/tegl")]
        [InlineData(FtbKodeListeEnum.Avfallfraksjoner_NS, "1619", "Asfalt")]
        public void ByggesoknadTests(Enum codeListEnum, string codeValue, string codeName, RegistryType registryType = RegistryType.Byggesoknad)
        {
            var isCodelistLabelValid = _codeListService.IsCodeLabelValid(codeListEnum, codeValue, codeName, registryType);
            isCodelistLabelValid.Should().BeTrue();
        }

        [Theory(Skip = skip, DisplayName = "SosiKodelister test")]
        [Trait("Category", "ExternalApiCall")]
        [InlineData(SosiKodelisterEnum.kommunenummer, "3817", "Midt-Telemark", RegistryType.SosiKodelister)]
        [InlineData(SosiKodelisterEnum.kommunenummer, "9999", "9999", RegistryType.SosiKodelister)]
        public void SosiKodelisterTests(Enum codeListEnum, string codeValue, string codeName, RegistryType registryType = RegistryType.SosiKodelister)
        {
            var isCodelistLabelValid = _codeListService.IsCodeLabelValid(codeListEnum, codeValue, codeName, registryType);
            isCodelistLabelValid.Should().BeTrue();
        }

        [Theory(Skip = skip, DisplayName = "Arbeidstilsynet test")]
        [Trait("Category", "ExternalApiCall")]
        [InlineData(ArbeidstilsynetKodeListeEnum.Arbeidstilsynets_krav, "11.18", "Er pauserom et tema i det søknadspliktige tiltaket?", RegistryType.Arbeidstilsynet)]
        public void ArbeidstilsynetTests(Enum codeListEnum, string codeValue, string codeName, RegistryType registryType = RegistryType.Arbeidstilsynet)
        {
            var isCodelistLabelValid = _codeListService.IsCodeLabelValid(codeListEnum, codeValue, codeName, registryType);
            isCodelistLabelValid.Should().BeTrue();
        }
    }

    public class CodeListConfiguration
    {
        public ICodeListService CodeListService;

        public CodeListConfiguration()
        {
            var services = new ServiceCollection();
            services.AddMemoryCache();
            services.AddHttpClient();
            var serviceProvider = services.BuildServiceProvider();
            var memoryCache = serviceProvider.GetService<IMemoryCache>();
            var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();

            var config = TestHelper.InitConfiguration();
            var options = Options.Create(new CodeListApiSettings()
            {
                SosiKodelisterUrl = config["CodeListApi:SosiKodelisterUrl"],
                ByggesoknadUrl = config["CodeListApi:ByggesoknadUrl"],
                ArbeidstilsynetUrl = config["CodeListApi:ArbeidstilsynetUrl"],
                AdditionalCodes = new AdditionalCodes()
                {
                    Kommunenummer = config["CodeListApi:AdditionalCodes:Kommunenummer"]
                }
            });

            var mockLogger = new Moq.Mock<ILogger<CodeListApiService>>();

            CodeListService = new CodeListService(memoryCache, options, new CodeListApiService(mockLogger.Object, options, httpClientFactory));
        }
    }
}