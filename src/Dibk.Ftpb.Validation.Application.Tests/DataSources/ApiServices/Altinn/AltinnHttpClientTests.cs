﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Altinn;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.DataSources.ApiServices.Altinn
{
    public class AltinnHttpClientTests
    {
        private const string skip = null;


        [Theory(DisplayName = "AltinnHttpClient.GetAllDibkServicesMetadata", Skip = skip)]
        [MemberData(nameof(EiendomsidentifikasjonFormsTestData))]
        [Trait("Category", "ExternalApiCall")]

        public void GetAllDibkServicesMetadata(IMemoryCache memoryCache, IOptions<AltinnSettings> options)
        {
            var htt = new HttpClient();
            var altinnHttpClient = new AltinnHttpClient(htt, options);
            var altinnMetadatas = altinnHttpClient.GetAllDibkServicesMetadata().Result;
            
            var SuppleringArbeidstilsynet = altinnMetadatas["7086/47365"];

            altinnMetadatas.Should().NotBeNull();
        }


        public static IEnumerable<object[]> EiendomsidentifikasjonFormsTestData()
        {

            var services = new ServiceCollection();
            services.AddMemoryCache();
            var serviceProvider = services.BuildServiceProvider();
            var memoryCache = serviceProvider.GetService<IMemoryCache>();

            var config = TestHelper.InitConfiguration();
            var options = Options.Create(new AltinnSettings()
            {
                AltinnServerUrl = config["Altinn:AltinnServerUrl"]
            });

            yield return new object[] { memoryCache, options };
        }
    }
}
