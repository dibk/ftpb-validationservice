﻿using BygningServiceWS;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using MatrikkelWSClient.Models;
using MatrikkelWSClient.Services;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.MatrikkelWSClient
{
    public class MatrikkelServiceTests : IClassFixture<MatrikkelProviderConfiguration>
    {
        private const string skip = "ExternalApiCall";

        private readonly IMatrikkelProvider _matrikkelProvider;

        public MatrikkelServiceTests(MatrikkelProviderConfiguration matrikkelProviderConfiguration)
        {
            _matrikkelProvider = matrikkelProviderConfiguration.MatrikkelProvider;
        }

        [Fact(Skip = skip, DisplayName = "Any Building With ByggsokModel Information")]
        [Trait("Category", "ExternalApiCall")]
        public void GetCodeList()
        {
            var byggsokModel = new ByggsokModel()
            {
                kommunenummer = "3817",
                gardsnummer = "53",
                bruksnummer = "319",
                festenummer = 0,
                seksjonsnummer = 2,

                adressenavn = "Bøgata",
                husnummer = 55,
                husnummerSpecified = true,
                bokstav = "A",
                utenBokstav = false,
                utenBokstavSpecified = true,
                bruksenhetsnummer = "U0101",

                bygningsnummer = 300482001,
                bygningsnummerSpecified = true

            };

            var matrikkelResult = _matrikkelProvider.MatrikkelExists(byggsokModel);

            matrikkelResult.Should().NotBeNull();
        }

        [Theory(Skip = skip, DisplayName = "Any Bygg With Bygningsnummer In Matrikkel")]
        [Trait("Category", "ExternalApiCall")]
        [InlineData("3817", "111111", "55", "40")]
        [InlineData("3817", "8625743", "55", "40")]
        public void BygningsnummerFeil(string kommunenummer, string bygningsnummer, string gardsnummer,
            string bruksnummer, string festenummer = null, string seksjonsnummer = null)
        {
            var matrikkelResult = _matrikkelProvider.ByggMedBygningsnummerEksistererIMatrikkelen("3817", "111111", "55", "40");
            matrikkelResult.Should().BeFalse();
        }

        [Theory(Skip = skip, DisplayName = "Any Bygg With Eiendoms Adresse In Matrikkel")]
        [Trait("Category", "ExternalApiCall")]
        [InlineData("3817", "Bøgata", "55", "A", "U0101", "53", "319", "0", "1")]
        [InlineData("3817", null, "5", null, "U0101", "53", "319", "0", "2")]
        [InlineData("3817", "Bøgata", "55", "C", "U0101", "53", "319", "0", "1")]
        public void EiendomsAdresseOK(string kommunenummer, string adressenavn, string husnummer, string bokstav,
            string bruksenhetsnummer, string gardsnummer, string bruksnummer, string festenummer, string seksjonsnummer)
        {
            var matrikkelResult = _matrikkelProvider.ByggEksistererPåAdresseIMatrikkelen(kommunenummer, adressenavn,
                husnummer, bokstav, bruksenhetsnummer, gardsnummer, bruksnummer, festenummer, seksjonsnummer);
            matrikkelResult.Should().BeTrue();
        }

        [Fact(Skip = skip)]
        public void EiendomsAdresseFeil11()
        {
            var matrikkelResult = _matrikkelProvider.ByggEksistererIMatrikkelen("3817", "55", "40");
            matrikkelResult.Should().BeFalse();
        }
    }
    public class MatrikkelProviderConfiguration
    {
        public IMatrikkelProvider MatrikkelProvider;

        public MatrikkelProviderConfiguration()
        {
            
            var loggerMP = new Mock<ILogger<MatrikkelProvider>>().Object;
            var loggerMSC = new Mock<ILogger<MatrikkelServiceClient>>().Object;

            var initConfiguration = TestHelper.InitConfiguration();
            var matrikkelApiSettings = initConfiguration.GetSection("MatrikkelApi").Get<MatrikkelSettings>();
            var options = Options.Create(matrikkelApiSettings);

            MatrikkelProvider = new MatrikkelProvider(loggerMP, new MatrikkelServiceClient(loggerMSC, options));
        }
    }
}