using Dibk.Ftpb.Validation.Application.DataSources;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Altinn;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.AtilFee;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Checklist;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Reporter.DataBase;
using Dibk.Ftpb.Validation.Application.Services;
using MatrikkelWSClient.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;


namespace Dibk.Ftpb.Validation.Application.Tests.Utils
{
    public class MockDataSource
    {
        public static IMunicipalityValidator MunicipalityValidatorResult(MunicipalityValidationEnum validationStatus)
        {

            var municipalityValidator = new Mock<IMunicipalityValidator>();
            municipalityValidator.Setup(s => s.GetMunicipalityStatus(It.IsAny<string>())).Returns(new MunicipalityStatus() { Status = validationStatus, Message = $"kommunenumer er : '{validationStatus.ToString()}'." });
            return municipalityValidator.Object;

        }

        public static ICodeListService GetCodeListService(ArbeidstilsynetKodeListeEnum codeValue, string lable = null)
        {
            var codelistClient = new Mock<ICodeListService>();
            var dictionary = new Dictionary<string, CodeListItem>()
            {
                {codeValue.ToString(), new CodeListItem(lable,"","")}
            };

            codelistClient.Setup(a => a.GetCodeList(It.IsAny<ArbeidstilsynetKodeListeEnum>(), It.IsAny<RegistryType>())).Returns(dictionary);

            return codelistClient.Object;
        }
        public static ICodeListService CodeListService(bool? isCodelistValid = true, bool? isCodelistLabelValid = true, string getCodelistTagValue = "gyldig")
        {
            var codelistClient = new Mock<ICodeListService>();
            codelistClient.Setup(a => a.IsCodeValueValid(It.IsAny<Enum>(), It.IsAny<string>(), It.IsAny<RegistryType>())).Returns(isCodelistValid);
            codelistClient.Setup(a => a.IsCodeLabelValid(It.IsAny<Enum>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<RegistryType>())).Returns(isCodelistLabelValid);
            codelistClient.Setup(a => a.GetCodelistTagValue(It.IsAny<Enum>(), It.IsAny<string>(), It.IsAny<RegistryType>())).Returns(new CodeListItem("","", getCodelistTagValue));
            return codelistClient.Object;
        }

        public static IPostalCodeService ValidatePostnr(bool valid, string result, string postalCodeType = null)
        {
            var postalCodeService = new Mock<IPostalCodeService>();
            postalCodeService.Setup((a => a.ValidatePostnr(It.IsAny<string>())))
                .Returns(new PostalCodeValidationResult() { Poststed = result, Gyldig = valid });

            return postalCodeService.Object;
        }
        public static IDecryptionFactory DecryptText(string fnr)
        {
            var decryptionFactory = new Mock<IDecryptionFactory>();
            decryptionFactory.Setup((d => d.GetDecryptor().DecryptText(It.IsAny<string>()))).Returns(fnr);

            return decryptionFactory.Object;
        }
        public static IAtilFeeCalculatorService GetFeeInformation(int feeAmount, string feeCategory)
        {
            var calculationBasisResult = new AtilFeeCalculationBasis() { FeeAmount = feeAmount, FeeCategory = feeCategory };
            var feeCalcService = new Mock<IAtilFeeCalculatorService>();
            feeCalcService.Setup(a => a.GetFeeInformationAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(calculationBasisResult));

            return feeCalcService.Object;
        }
        public static IAtilFeeCalculatorService GetIFeeInformation(int feeAmount, string feeCategory)
        {
            var calculationBasisResult = new AtilFeeCalculationBasis() { FeeAmount = feeAmount, FeeCategory = feeCategory };
            var feeCalcService = new Mock<IAtilFeeCalculatorService>();
            feeCalcService.Setup(a => a.GetFeeInformationAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(calculationBasisResult));

            return feeCalcService.Object;
        }

        public static IAtilFeeCalculatorService GetAtilTiltakstyperAsync()
        {
            var tiltakstyper = new AtilTiltakstype() {  Code="", Name="", Category="", AllowZeroInBRA="" };
            var tiltakstyperResult = new List<AtilTiltakstype>();
            tiltakstyperResult.Add(tiltakstyper);
            var tiltakstyperService = new Mock<IAtilFeeCalculatorService>();
            tiltakstyperService.Setup(a => a.GetTiltakstyperAsync())
                .Returns(Task.FromResult(tiltakstyperResult));

            return tiltakstyperService.Object;
        }

        public static IChecklistService GetCheckpoints(string category)
        {
            var checklistService = new Mock<IChecklistService>();
            checklistService.Setup((a => a.GetChecklist(It.IsAny<string>(), It.IsAny<ServiceDomain>(), It.IsAny<string>())))
                .Returns(new List<Sjekk> { new Sjekk() { Id = "1.21", SjekkId = 2644, Navn = "Skal søknaden unntas offentilghet?", Prosesskategori = category, Rekkefolge = 26 } });
            return checklistService.Object;

        }

        public static IFormaaltypeValidator formaaltypeValidator()
        {
            var formallTypeValidator = new Mock<IFormaaltypeValidator>();
            var validationResult = new ValidationResult()
            {
                Errors = 0,
                Messages = new List<ValidationMessage>(),
                PrefillChecklist = new List<ChecklistAnswer>(),
                RulesChecked = new List<ValidationRule>(),
                Soknadtype = "Soknadtype",
                TiltakstyperISoeknad = new List<string>(),
                ValidationMessages = new List<ValidationMessage>(),
                ValidationRules = new List<ValidationRule>(),
                Warnings = 0


            };

            formallTypeValidator.Setup(a => a.Validate(It.IsAny<Formaaltype>())).Returns(validationResult);
            formallTypeValidator.SetupGet(e => e.ValidationResult).Returns(validationResult); ;

            return formallTypeValidator.Object;
        }

        //Validator
        public static IKodelisteValidator KodelisteValidator(string xpath = null, ValidationRuleEnum? validationRule = null, string entityXPath = null)
        {
            var kodelisteValidator = new Mock<IKodelisteValidator>();
            kodelisteValidator.Setup((a => a.Validate(It.IsAny<Kodeliste>(), It.IsAny<string>())))
                .Returns(ValidationResult(xpath, validationRule));
            kodelisteValidator.SetupGet(e => e._entityXPath).Returns(entityXPath);
            var validationResult = new ValidationResult()
            {
                Errors = 0,
                Messages = new List<ValidationMessage>(),
                PrefillChecklist = new List<ChecklistAnswer>(),
                RulesChecked = new List<ValidationRule>(),
                Soknadtype = "Soknadtype",
                TiltakstyperISoeknad = new List<string>(),
                ValidationMessages = new List<ValidationMessage>(),
                ValidationRules = new List<ValidationRule>(),
                Warnings = 0


            };

            kodelisteValidator.SetupGet(e => e.ValidationResult).Returns(validationResult);
            return kodelisteValidator.Object;
        }

        public static IEnkelAdresseValidator enkelAdresseValidator(string xpath = null, ValidationRuleEnum? validationRule = null)
        {
            var validationResult = new ValidationResult()
            {
                Errors = 0,
                Messages = new List<ValidationMessage>(),
                PrefillChecklist = new List<ChecklistAnswer>(),
                RulesChecked = new List<ValidationRule>(),
                Soknadtype = "Soknadtype",
                TiltakstyperISoeknad = new List<string>(),
                ValidationMessages = new List<ValidationMessage>(),
                ValidationRules = new List<ValidationRule>(),
                Warnings = 0


            };
            var adressValidator = new Mock<IEnkelAdresseValidator>();

            adressValidator.Setup(a => a.Validate(It.IsAny<Dibk.Ftpb.Common.Datamodels.Parts.EnkelAdresse>())).Returns(ValidationResult(xpath, validationRule));
            adressValidator.SetupGet(e => e.ValidationResult).Returns(validationResult);
            return adressValidator.Object;
        }
        public static IKontaktpersonValidator KontaktpersonValidator(string xpath = null, ValidationRuleEnum? validationRule = null)
        {
            var validationResult = new ValidationResult()
            {
                Errors = 0,
                Messages = new List<ValidationMessage>(),
                PrefillChecklist = new List<ChecklistAnswer>(),
                RulesChecked = new List<ValidationRule>(),
                Soknadtype = "Soknadtype",
                TiltakstyperISoeknad = new List<string>(),
                ValidationMessages = new List<ValidationMessage>(),
                ValidationRules = new List<ValidationRule>(),
                Warnings = 0


            };
            var kontaktpersjon = new Mock<IKontaktpersonValidator>();

            kontaktpersjon.Setup(a => a.Validate(It.IsAny<Kontaktperson>())).Returns(ValidationResult(xpath, validationRule));
            kontaktpersjon.SetupGet(e => e.ValidationResult).Returns(validationResult);
            return kontaktpersjon.Object;
        }

        public static IEiendomsidentifikasjonValidator EiendomsidentifikasjonValidator(string xpath = null, ValidationRuleEnum? validationRule = null)
        {
            var validationResult = new ValidationResult()
            {
                Errors = 0,
                Messages = new List<ValidationMessage>(),
                PrefillChecklist = new List<ChecklistAnswer>(),
                RulesChecked = new List<ValidationRule>(),
                Soknadtype = "Soknadtype",
                TiltakstyperISoeknad = new List<string>(),
                ValidationMessages = new List<ValidationMessage>(),
                ValidationRules = new List<ValidationRule>(),
                Warnings = 0


            };
            var validator = new Mock<IEiendomsidentifikasjonValidator>();
            validator.Setup(a => a.Validate(It.IsAny<Eiendomsidentifikasjon>())).Returns(ValidationResult(xpath, validationRule));
            validator.SetupGet(e => e.ValidationResult).Returns(validationResult);
            return validator.Object;
        }
        public static IEiendomsAdresseValidator EiendomsAdresseValidator(string xpath = null, ValidationRuleEnum? validationRule = null)
        {
            var validator = new Mock<IEiendomsAdresseValidator>();
            var validationResult = new ValidationResult()
            {
                Errors = 0,
                Messages = new List<ValidationMessage>(),
                PrefillChecklist = new List<ChecklistAnswer>(),
                RulesChecked = new List<ValidationRule>(),
                Soknadtype = "Soknadtype",
                TiltakstyperISoeknad = new List<string>(),
                ValidationMessages = new List<ValidationMessage>(),
                ValidationRules = new List<ValidationRule>(),
                Warnings = 0


            };
            validator.Setup(a => a.Validate(It.IsAny<Adresse>())).Returns(ValidationResult(xpath, validationRule));
            validator.SetupGet(e => e.ValidationResult).Returns(validationResult);
            return validator.Object;
        }

        public static IAltinnMetadataServices AttachmentValidatorSuppleringATIL(string xpath = null, ValidationRuleEnum? validationRule = null)
        {
            var validator = new Mock<IAltinnMetadataServices>();
            var attachmentRules = new List<AttachmentRule>()
            {
                new ()
                {
                    AllowedFileTypes = "pdf, doc, docx, jpg, png, xls, xlsx, rtf",
                    AttachmentTypeName = "Annet",
                    MaxAttachmentCount = 10,
                    MaxFileSize = 20,
                    MinAttachmentCount = 0
                },
                new ()
                {
                    AllowedFileTypes = "pdf, jpeg, jpg, tif, tiff, png",
                    AttachmentTypeName = "Situasjonsplan",
                    MaxAttachmentCount = 99,
                    MaxFileSize = 30,
                    MinAttachmentCount = 0
                },
                new ()
                {
                    AllowedFileTypes = "pdf, jpeg, jpg, tif, tiff, png",
                    AttachmentTypeName = "TegningNyFasade",
                    MaxAttachmentCount = 99,
                    MaxFileSize = 30,
                    MinAttachmentCount = 0
                }
            };
            var subFormsMetadata = new List<FormMetaData>()
            {
                new (){ DataFormatVersion = "47365",DataFormatID = "7086",FormName = "Supplering av søknad til Arbeidstilsynets samtykke"},
                //new (){ DataFormatVersion = "",DataFormatID = "",FormName = ""},
                //new (){ DataFormatVersion = "",DataFormatID = "",FormName = ""},
            };
            var validationResult = new AltinnMetadata()
            {
                ServiceName = "",
                ServiceCode = "",ServiceType = "",ServiceEditionCode = "",
                SubFormsMetaData = subFormsMetadata,
                AttachmentRules = attachmentRules,
            };
            validator.Setup(a => a.GetAltinnMetadataAsync(It.IsAny<string>(),It.IsAny<string>())).Returns(Task.FromResult(validationResult));

            return validator.Object;
        }

        //Matrikkel
        public static IMatrikkelProvider AnyByggWithEiendomsidentifikasjonInMatrikkel(bool? valid)
        {
            var matrikkelProvider = new Mock<IMatrikkelProvider>();
            matrikkelProvider.Setup((a => a.ByggEksistererIMatrikkelen(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))).Returns(valid);

            return matrikkelProvider.Object;
        }
        public static IMatrikkelProvider ImatrikkelServices(bool? AnyByggWithEiendomsAdresseInMatrikkel, bool? AnyByggWithEiendomsidentifikasjonInMatrikkel = null, bool? AnyByggWithBygningsnummerInMatrikkel = null)
        {
            var matrikkelProvider = new Mock<IMatrikkelProvider>();
            matrikkelProvider.Setup((a => a.ByggEksistererPåAdresseIMatrikkelen(It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))).Returns(AnyByggWithEiendomsAdresseInMatrikkel);

            matrikkelProvider.Setup(p => p.ByggEksistererIMatrikkelen(It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(AnyByggWithEiendomsidentifikasjonInMatrikkel);

            matrikkelProvider.Setup(p => p.ByggMedBygningsnummerEksistererIMatrikkelen(It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(AnyByggWithBygningsnummerInMatrikkel);
            return matrikkelProvider.Object;
        }


        private static ValidationResult ValidationResult(string xpath = null, ValidationRuleEnum? validationRule = null)
        {
            var validationResult = new ValidationResult()
            {
                ValidationRules = new List<ValidationRule>(),
                ValidationMessages = new List<ValidationMessage>()
            };

            if (!string.IsNullOrEmpty(xpath) || validationRule.HasValue)
            {
                validationResult.ValidationMessages.Add(new ValidationMessage()
                {
                    XpathField = xpath,
                    Rule = validationRule?.ToString() ?? null,
                });
            }
            return validationResult;
        }

        // ATIL Feature - Endres etter 20250217
        internal static ValidationMessageComposer GetValidationMessageComposer()
            => new ValidationMessageComposer(new ValidationMessageRepository(new ValidationMessageDb(FeatureManagerMocks.AtilFeatureManager())));
    }
}
