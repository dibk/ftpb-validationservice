﻿using Microsoft.FeatureManagement;
using Moq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Application.Tests.Utils
{
    internal class FeatureManagerMocks
    {

        public static IFeatureManager AtilFeatureManager()
        {
            var featureManageMock = new Mock<IFeatureManager>();
            featureManageMock
                .Setup(m => m.IsEnabledAsync("ATIL20250217"))
                .Returns(Task.FromResult(true));

            return featureManageMock.Object;
        }
    }
}
