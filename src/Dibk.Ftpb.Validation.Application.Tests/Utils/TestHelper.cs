using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Utils;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Debugging = Dibk.Ftpb.Validation.Application.Utils.Debugging;

namespace Dibk.Ftpb.Validation.Application.Tests.Utils
{
    public class TestHelper
    {
        public static IConfiguration InitConfiguration()
        {
            string basePath = System.AppContext.BaseDirectory;
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "test"}.json", optional: true)
                .Build();
            return configuration;
        }

        public static string GetXmlWithoutSpaces(string formAsXml)
        {
            if (string.IsNullOrEmpty(formAsXml))
                return null;

            Regex Parser = new Regex(@">\s*<");
            var xml = Parser.Replace(formAsXml, "><");
            xml.Trim();
            return xml;
        }

        public static Dictionary<string, string> DebugValidatorFormReference(List<ValidationRule> reference)
        {
            var validatorXpathList = new Dictionary<string, string>();
            foreach (var validationRule in reference)
            {
                var validtorXpath = Debugging.DebugValidatorFormReference(validationRule.Id);
                validatorXpathList.Add(validationRule.Id, $"{validtorXpath} -'XmlElement':{validationRule.XmlElement}, 'XpathField':{validationRule.XpathField}, 'Rule':{validationRule.Rule}");
            }
            return validatorXpathList;
        }


        //https://stackoverflow.com/a/4367868
        public static T GetEnumFromValidationId<T>(string validatorId) where T : Enum
        {
            //foreach (var field in typeof(T).GetFields())
            //{
            //    if (Attribute.GetCustomAttribute(field,
            //        typeof(EntityValidatorEnumerationAttribute)) is EntityValidatorEnumerationAttribute attribute)
            //    {
            //        if (attribute.ValidatorId == validatorId)
            //            return (T)field.GetValue(null);
            //    }
            //    else
            //    {
            //        if (field.Name == validatorId)
            //            return (T)field.GetValue(null);
            //    }
            //}

            //throw new ArgumentException("Not found.", nameof(validatorId));
            return default(T);
        }

        //TODO how to convert new entities to main XML/parse to the main class?
        public static JObject GetJsonForPostman(string xmlData, object form = null, List<string> attachemntsNameList = null)
        {
            string data = xmlData;
            if (form != null)
                data = SerializeUtil.Serialize(form);
            ValidationInput validationInput = new ValidationInput()
            {
                FormData = GetXmlWithoutSpaces(data),

            };

            var attachmentInfo = new List<AttachmentInfo>();
            if (attachemntsNameList != null)
            {
                foreach (var attachment in attachemntsNameList)
                {
                    attachmentInfo.Add(
                        new AttachmentInfo()
                        {
                            AttachmentTypeName = attachment,
                            Filename = $"{attachment}.pdf",
                            FileSize = 10
                        });
                }
            }
            else
            {
                attachmentInfo.Add(
                        new AttachmentInfo()
                        {
                            AttachmentTypeName = "nokoType",
                            Filename = "noko.pdf",
                            FileSize = 10
                        });
            }
            validationInput.Attachments= attachmentInfo.ToArray();

            var validateFormv2JObject = JObject.FromObject(validationInput);
            return validateFormv2JObject;
        }

        public static IMemoryCache GetMemoryCache()
        {
            var services = new ServiceCollection();
            services.AddMemoryCache();
            var serviceProvider = services.BuildServiceProvider();
            var memoryCache = serviceProvider.GetService<IMemoryCache>();
            return memoryCache;
        }

        public static IOptions<CodeListApiSettings> GetCodeListApiSettins()
        {
            var config = InitConfiguration();

            var validStatuses = new List<string>();

            for (int i = 0; i < 4; i++)
            {
                var status = config[$"CodeListApi:StatusesConsideredValid:{i}"];
                if (status == null)
                    break;

                validStatuses.Add(config[$"CodeListApi:StatusesConsideredValid:{i}"]);
            }

            var options = Options.Create(new CodeListApiSettings()
            {
                SosiKodelisterUrl = config["CodeListApi:SosiKodelisterUrl"],
                ByggesoknadUrl = config["CodeListApi:ByggesoknadUrl"],
                ArbeidstilsynetUrl = config["CodeListApi:ArbeidstilsynetUrl"],
                AdditionalCodes = new AdditionalCodes()
                {
                    Kommunenummer = config["CodeListApi:AdditionalCodes:Kommunenummer"]
                },
                StatusesConsideredValid = validStatuses
            });
            return options;
        }
    }
}
