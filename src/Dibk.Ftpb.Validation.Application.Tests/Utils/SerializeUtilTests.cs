﻿using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.IO;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.Utils
{
    public class SerializeUtilTests
    {
        [Fact]
        public void FilterValidationResultTest()
        {
            var xmlData_45957 = File.ReadAllText(@"Data\ArbeidstilsynetsSamtykke_v2_dfv45957.xml");
            var xmlData_41999 = File.ReadAllText(@"Data\ArbeidstilsynetsSamtykke_dfv41999.xml");

            var xmlData_45957_Common = SerializeUtil.DeserializeFromString<ArbeidstilsynetsSamtykkeV2>(xmlData_45957);

            xmlData_45957_Common.Should().NotBeNull();
        }
    }
}
