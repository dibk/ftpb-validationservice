﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Dibk.Ftpb.Validation.Application.Models.Web;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class AttachmentValidatorTest
    {
        private const string skip = null;


        [Theory(DisplayName = ".67 - Attachemnt - OK", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AttachementTestData))]
        public void Attachment(IList<EntityValidatorNode> tree, AttachmentInfo attachment, List<AttachmentRule> altinAttachmentRules)
        {
            var attachmentValidator = new AttachmentValidator(tree);
            var result = attachmentValidator.Validate(attachment, altinAttachmentRules);
            result.ValidationMessages.Count.Should().Be(0);
        }
        [Theory(DisplayName = ".67.2 - Attachment.gyldig", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AttachementTestData))]
        public void AttachmentGyldig(IList<EntityValidatorNode> tree, AttachmentInfo attachment, List<AttachmentRule> altinAttachmentRules)
        {
            var attachmentValidator = new AttachmentValidator(tree);
            
            altinAttachmentRules = null;
            var result = attachmentValidator.Validate(attachment, altinAttachmentRules);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.First().Reference.Should().Be(".67.2");
        }
        [Theory(DisplayName = ".67.160.1 - attachmentTypeName.utfylt", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AttachementTestData))]
        public void attachmentTypeNameUtfylt(IList<EntityValidatorNode> tree, AttachmentInfo attachment, List<AttachmentRule> altinAttachmentRules)
        {
            var attachmentValidator = new AttachmentValidator(tree);

            attachment.AttachmentTypeName = null;
            var result = attachmentValidator.Validate(attachment, altinAttachmentRules);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.First().Reference.Should().Be(".67.160.1");
        }

        [Theory(DisplayName = ".67.160.1 - attachmentTypeName.gyldig", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AttachementTestData))]
        public void attachmentTypeNameGyldig(IList<EntityValidatorNode> tree, AttachmentInfo attachment, List<AttachmentRule> altinAttachmentRules)
        {
            var attachmentValidator = new AttachmentValidator(tree);

            attachment.AttachmentTypeName = "Annet*";
            var result = attachmentValidator.Validate(attachment, altinAttachmentRules);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.First().Reference.Should().Be(".67.160.2");
        }

        [Theory(DisplayName = ".67.160.1 - allowedFileTypes.gyldig", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AttachementTestData))]
        public void allowedFileTypesUtfylt(IList<EntityValidatorNode> tree, AttachmentInfo attachment, List<AttachmentRule> altinAttachmentRules)
        {
            var attachmentValidator = new AttachmentValidator(tree);

            attachment.Filename = "noko";
            var result = attachmentValidator.Validate(attachment, altinAttachmentRules);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.First().Reference.Should().Be(".67.161.2");
        }

        [Theory(DisplayName = ".67.163.1 - maxFilesize.utfylt", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AttachementTestData))]
        public void maxFilesizeUtfylt(IList<EntityValidatorNode> tree, AttachmentInfo attachment, List<AttachmentRule> altinAttachmentRules)
        {
            var attachmentValidator = new AttachmentValidator(tree);

            attachment.FileSize = 0;

            var result = attachmentValidator.Validate(attachment, altinAttachmentRules);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.First().Reference.Should().Be(".67.163.1");
        }

        [Theory(DisplayName = ".67.151.2 - filnavn.gyldig", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AttachementTestData))]
        public void filnavnGyldig(IList<EntityValidatorNode> tree, AttachmentInfo attachment, List<AttachmentRule> altinAttachmentRules)
        {
            var attachmentValidator = new AttachmentValidator(tree);

            attachment.Filename = "Mari?.pdf";

            var result = attachmentValidator.Validate(attachment, altinAttachmentRules);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.First().Reference.Should().Be(".67.151.2");
        }



        public static IEnumerable<object[]> AttachementTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.AttachmentValidator, ParentID = null },
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            AttachmentInfo attachmentInfo = new AttachmentInfo() { AttachmentTypeName = "Situasjonsplan", Filename = "Situasjonsplan.pdf", FileSize = 30 };
            var attachmentRules = new List<AttachmentRule>()
            {
                new ()
                {
                    AllowedFileTypes = "pdf, doc, docx, jpg, png, xls, xlsx, rtf",
                    AttachmentTypeName = "Annet",
                    MaxAttachmentCount = 10,
                    MaxFileSize = 20,
                    MinAttachmentCount = 0
                },
                new ()
                {
                    AllowedFileTypes = "pdf, jpeg, jpg, tif, tiff, png",
                    AttachmentTypeName = "Situasjonsplan",
                    MaxAttachmentCount = 99,
                    MaxFileSize = 30,
                    MinAttachmentCount = 0
                },
                new ()
                {
                    AllowedFileTypes = "pdf, jpeg, jpg, tif, tiff, png",
                    AttachmentTypeName = "TegningNyFasade",
                    MaxAttachmentCount = 99,
                    MaxFileSize = 30,
                    MinAttachmentCount = 0
                }
            };

            yield return new object[] { entityValidationTree, attachmentInfo, attachmentRules };

        }

    }
}
