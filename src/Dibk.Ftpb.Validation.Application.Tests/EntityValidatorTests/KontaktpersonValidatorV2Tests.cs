﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.IO;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Xunit;
using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class KontaktpersonValidatorV2Tests
    {
        private readonly KontaktpersonValidator _foretakKontaktpersonValidator;
        private AnsakoAnsvarsrett _form;

        public KontaktpersonValidatorV2Tests()
        {
            var xmlData = File.ReadAllText(@"Data\Ansako\ErklaeringAnsvarsrett_1.xml");
            _form = SerializeUtil.DeserializeFromString<AnsakoAnsvarsrett>(xmlData);
            var foretakNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 01, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = null},
            };
            var tree = EntityValidatorTree.BuildTree(foretakNodeList);


            _foretakKontaktpersonValidator = new KontaktpersonValidator(tree,01);


        }
        [Fact]
        public void KontaktpersonRule_Count()
        {
            var result = _foretakKontaktpersonValidator.Validate(_form.Ansvarsretts.Foretak.Kontaktperson);
            var rules = result?.ValidationRules;
            rules.Count.Should().Be(6);
        }
        [Fact]
        public void Kontaktperson_navn_utfylt()
        {
            _form.Ansvarsretts.Foretak.Kontaktperson.Navn = null;
            var result = _foretakKontaktpersonValidator.Validate(_form.Ansvarsretts.Foretak.Kontaktperson);
            var validationMessages = result?.ValidationMessages;
            validationMessages?.Count.Should().Be(1);
        }
        [Fact]
        public void Kontaktperson_telmob_utfylt_Utfylt()
        {
            _form.Ansvarsretts.Foretak.Kontaktperson.Mobilnummer = null;
            _form.Ansvarsretts.Foretak.Kontaktperson.Telefonnummer = null;
            var result = _foretakKontaktpersonValidator.Validate(_form.Ansvarsretts.Foretak.Kontaktperson);
            var validationMessages = result?.ValidationMessages;
            validationMessages?.Count.Should().Be(1);
        }
        [Fact]
        public void Kontaktperson_telefonnummer_gyldig()
        {
            _form.Ansvarsretts.Foretak.Kontaktperson.Telefonnummer = "-4445sdf";
            var result = _foretakKontaktpersonValidator.Validate(_form.Ansvarsretts.Foretak.Kontaktperson);
            var validationMessages = result?.ValidationMessages;
            validationMessages?.Count.Should().Be(1);
        }
        [Fact]
        public void Kontaktperson_mobilnummer_gyldig()
        {
            _form.Ansvarsretts.Foretak.Kontaktperson.Mobilnummer = "-4445sdf";
            var result = _foretakKontaktpersonValidator.Validate(_form.Ansvarsretts.Foretak.Kontaktperson);
            var validationMessages = result?.ValidationMessages;
            validationMessages?.Count.Should().Be(1);
        }
        [Fact]
        public void Kontaktperson_epost_utfylt()
        {
            _form.Ansvarsretts.Foretak.Kontaktperson.Epost = null;
            var result = _foretakKontaktpersonValidator.Validate(_form.Ansvarsretts.Foretak.Kontaktperson);
            var validationMessages = result?.ValidationMessages;
            validationMessages?.Count.Should().Be(1);
        }
    }
}
