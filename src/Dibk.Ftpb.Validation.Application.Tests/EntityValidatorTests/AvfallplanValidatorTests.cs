﻿using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class AvfallplanValidatorTests
    {


        private SluttrapportForBygningsAvfall _form;
        private IList<EntityValidatorNode> _planForAvfalltree;
        private IKodelisteValidator _avfallplanAvfallklasseValidator;
        private IKodelisteValidator _avfallplanAvfallfraksjonValidator;
        private AvfallplanValidator _avfallplanValidator;
        private IKodelisteValidator _avfallplanAvfallklassedelsumAvfallklasseValidator;
        private AvfallklasseDelsumValidator _avfallplanAvfallklasseDelsumValidator;
        private AvfallsorteringValidator _avfallplanSorteringValidator;
        private PlanForAvfallValidator _planForAvfallValidator;
        private readonly IList<EntityValidatorNode> _tree;

        public AvfallplanValidatorTests()
        {
            var xmlData = File.ReadAllText(@"Data\Avfallsplan TM 20220405 korrigert.xml");
            _form = SerializeUtil.DeserializeFromString<SluttrapportForBygningsAvfall>(xmlData);
            var planForAvfallNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 18, EnumId = EntityValidatorEnum.PlanForAvfallValidator, ParentID = null},
                new () {NodeId = 19, EnumId = EntityValidatorEnum.AvfallplanValidator, ParentID = 18},

                new () {NodeId = 20, EnumId = EntityValidatorEnum.AvfallklasseValidator, ParentID = 19},
                new () {NodeId = 21, EnumId = EntityValidatorEnum.AvfallfraksjonValidator, ParentID = 19},

                new () {NodeId = 22, EnumId = EntityValidatorEnum.AvfallsorteringValidator, ParentID = 18},
                new () {NodeId = 23, EnumId = EntityValidatorEnum.AvfallsklasseDelsumValidator, ParentID = 18},

                new () {NodeId = 24, EnumId = EntityValidatorEnum.AvfallklasseValidator, ParentID = 23},
            };
            _planForAvfalltree = EntityValidatorTree.BuildTree(planForAvfallNodeList);

            var _planAvfallklasseCodeListService = MockDataSource.CodeListService( true);
            //var _planFraksjonCodeListService = MockDataSource.CodeListService(FtbKodeListeEnum.Avfallfraksjoner_NS, true);
            var _planFraksjonCodeListService = MockDataSource.CodeListService( true);

            var _avfallsklasseDelsumAvfallsklasseCodeListService = MockDataSource.CodeListService(true);

            _tree = _planForAvfalltree;
            //PlanForAvfall
            //_avfallplanAvfallklasseValidator = new AvfallklasseValidator(tree, 20, _planAvfallklasseCodeListService);
            _avfallplanAvfallklasseValidator = MockDataSource.KodelisteValidator(null,null, "/plan/avfall/avfallplan{0}/avfallsklasse");
            //_avfallplanAvfallfraksjonValidator = new AvfallfraksjonValidator(tree, 21, _planFraksjonCodeListService);
            _avfallplanAvfallfraksjonValidator = MockDataSource.KodelisteValidator(null,null, "/plan/avfall/avfallplan{0}/fraksjon");
            _avfallplanValidator = new AvfallplanValidator(_tree, _avfallplanAvfallklasseValidator, _avfallplanAvfallfraksjonValidator);
        }

        [Fact]
        public void TestPartstype()
        {
            var fraksjonKodeverdiXpath = "/plan/avfall/avfallplan{0}/fraksjon/kodeverdi";

            //_avfallplanAvfallfraksjonValidator = MockDataSource.KodelisteValidator(fraksjonKodeverdiXpath,ValidationRuleEnum.gyldig, "/plan/avfall/avfallplan{0}/fraksjon");
            //_avfallplanValidator = new AvfallplanValidator(_tree, _avfallplanAvfallklasseValidator, _avfallplanAvfallfraksjonValidator);

            var result = _avfallplanValidator.Validate(_form.PlanForAvfall.Avfall.First());
            result.Should().NotBeNull();
        }
    }
}
