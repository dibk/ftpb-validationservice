﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class ArbeidstilsynetVedleggstypeValidatorTests
    {
        private const string skip = "test";

        [Theory (DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(VedleggstypeTestData))]
        public void VedleggstypeTestOk(IList<EntityValidatorNode> tree, Kodeliste vedleggstype)
        {
            var codeListService = MockDataSource.CodeListService();
            var vedleggstypeValidator = new ArbeidstilsynetVedleggstypeValidator(tree, 1, codeListService);
      
            var result = vedleggstypeValidator.Validate(vedleggstype);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory (DisplayName = "60.1 - vedleggstype.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(VedleggstypeTestData))]
        public void VedleggstypeUtfylt(IList<EntityValidatorNode> tree, Kodeliste vedleggstype)
        {
            var codeListService = MockDataSource.CodeListService();
            var vedleggstypeValidator = new ArbeidstilsynetVedleggstypeValidator(tree, 1, codeListService);

            vedleggstype = null;
            var result = vedleggstypeValidator.Validate(vedleggstype);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".60.1");
        }

        [Theory(DisplayName = ".60.38.1 - vedleggstype.kodeverdi.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(VedleggstypeTestData))]
        public void VedleggstypeKodeverdiUtfylt(IList<EntityValidatorNode> tree, Kodeliste vedleggstype)
        {
            var codeListService = MockDataSource.CodeListService();
            var vedleggstypeValidator = new ArbeidstilsynetVedleggstypeValidator(tree, 1, codeListService);

            vedleggstype.Kodeverdi = null;
            var result = vedleggstypeValidator.Validate(vedleggstype);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".60.38.1");
        }

        [Theory(DisplayName = ".60.38.2 - vedleggstype.kodeverdi.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(VedleggstypeTestData))]
        public void VedleggstypeKodeverdiGyldig(IList<EntityValidatorNode> tree, Kodeliste vedleggstype)
        {
            var codeListService = MockDataSource.CodeListService(false);
            var vedleggstypeValidator = new ArbeidstilsynetVedleggstypeValidator(tree, 1, codeListService);

            var result = vedleggstypeValidator.Validate(vedleggstype);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".60.38.2");
        }

        [Theory(DisplayName = ".60.39.1 - vedleggstype.kodebeskrivelse.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(VedleggstypeTestData))]
        public void VedleggstypeKodebeskrivelseUtfylt(IList<EntityValidatorNode> tree, Kodeliste vedleggstype)
        {
            var codeListService = MockDataSource.CodeListService();
            var vedleggstypeValidator = new ArbeidstilsynetVedleggstypeValidator(tree, 1, codeListService);

            vedleggstype.Kodebeskrivelse = null;
            var result = vedleggstypeValidator.Validate(vedleggstype);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".60.39.1");
        }

        [Theory(DisplayName = ".60.39.2 - vedleggstype.kodebeskrivelse.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(VedleggstypeTestData))]
        public void VedleggstypeKodebeskrivelseGyldig(IList<EntityValidatorNode> tree, Kodeliste vedleggstype)
        {
            var codeListService = MockDataSource.CodeListService(true, false);
            var vedleggstypeValidator = new ArbeidstilsynetVedleggstypeValidator(tree, 1, codeListService);

            var result = vedleggstypeValidator.Validate(vedleggstype);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".60.39.2");
        }

        public static IEnumerable<object[]> VedleggstypeTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.ArbeidstilsynetVedleggstypeValidator, ParentID = null },              
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            Kodeliste vedleggstype = new Kodeliste() { Kodeverdi = "kodeverdi", Kodebeskrivelse = "kodebeskrivelse"};           

            yield return new object[] { entityValidationTree, vedleggstype };

        }
    }
}
