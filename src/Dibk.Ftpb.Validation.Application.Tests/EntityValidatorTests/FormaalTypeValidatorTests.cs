﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Enums.ValidationEnums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;
using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class FormaalTypeValidatorTests
    {
        private Formaaltype _beskrivelseAvTiltak;
        private FormaaltypeValidator _formaaltypeValidator;
        private readonly IKodelisteValidator _bygningstypeValidator;
        private readonly IList<EntityValidatorNode> _tree;


        public FormaalTypeValidatorTests()
        {
            var xmlData = File.ReadAllText(@"Data\ArbeidstilsynetsSamtykke_v2_dfv45957.xml");
            var form = SerializeUtil.DeserializeFromString<ArbeidstilsynetsSamtykkeV2>(xmlData);
            _beskrivelseAvTiltak = (Formaaltype)form.BeskrivelseAvTiltak.Formaaltype;

            var beskrivelseAvTiltakNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 1, EnumId = EntityValidatorEnum.FormaaltypeValidator, ParentID = null},
            };
            _tree = EntityValidatorTree.BuildTree(beskrivelseAvTiltakNodeList);

            _bygningstypeValidator = MockDataSource.KodelisteValidator();

            _formaaltypeValidator = new FormaaltypeValidator(_tree, _bygningstypeValidator);
        }

        [Fact]
        public void RuleCountTest()
        {
            _formaaltypeValidator.ValidateEntityFields(_beskrivelseAvTiltak);
            _formaaltypeValidator.ValidationResult.Should().NotBeNull();
        }
        [Fact]
        public void Test_Error()
        {
            _beskrivelseAvTiltak = null;
            var result = _formaaltypeValidator.Validate(_beskrivelseAvTiltak);
            result.ValidationMessages?.FirstOrDefault()?.Rule.Should().Be(ValidationRuleEnum.utfylt.ToString());
        }

    }
}
