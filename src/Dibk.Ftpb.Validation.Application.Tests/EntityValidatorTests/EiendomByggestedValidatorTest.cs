﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Models.Builder;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Xunit;
using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;


namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class EiendomByggestedValidatorTest //: IClassFixture<CodeListConfiguration>
    {
        private const string skip = "test";

        [Fact()]
        public void testAvBygningsnummer()
        {
            List<EntityValidatorNode> eiendombyggestedNodeList = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 1 },
                new() { NodeId = 3, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 1 },
            };
            IList<EntityValidatorNode> tree = EntityValidatorTree.BuildTree(eiendombyggestedNodeList);

            Eiendom eiendom = new Eiendom()
            {
                Bolignummer = "H0102",
                Bygningsnummer = "123456789",
                Kommunenavn = "Midt telemark",
                Eiendomsidentifikasjon = new Eiendomsidentifikasjon()
                {
                    Bruksnummer = "1515",
                    Festenummer = "2",
                    Gaardsnummer = "2",
                    Kommunenummer = "3801",
                    Seksjonsnummer = "1"
                },
                Adresse = new Adresse()
                {
                    Adresselinje1 = "Adresselinje1",
                    Gatenavn = "Gatenavn_",
                    Husnr = "123",
                    Bokstav = "B"
                }
            };
            var eiendomsAdresseValidator = MockDataSource.EiendomsAdresseValidator();
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(true, true);

            var eiendomByggestedValidator = new EiendomByggestedValidator(tree, eiendomsAdresseValidator,
                eiendomsidentifikasjonValidator, matrikkelService);

            eiendom.Bygningsnummer = "a";

            var result = eiendomByggestedValidator.Validate(eiendom);

            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.15.24");
        }

        [Fact()]
        public void testAvBygningsnummer1()
        {
            List<EntityValidatorNode> eiendombyggestedNodeList = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 1 },
                new() { NodeId = 3, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 1 },
            };
            IList<EntityValidatorNode> tree = EntityValidatorTree.BuildTree(eiendombyggestedNodeList);

            Eiendom eiendom = new Eiendom()
            {
                Bolignummer = "H0102",
                Bygningsnummer = "123456789",
                Kommunenavn = "Midt telemark",
                Eiendomsidentifikasjon = new Eiendomsidentifikasjon()
                {
                    Bruksnummer = "1515",
                    Festenummer = "2",
                    Gaardsnummer = "2",
                    Kommunenummer = "3801",
                    Seksjonsnummer = "1"
                },
                Adresse = new Adresse()
                {
                    Adresselinje1 = "Adresselinje1",
                    Gatenavn = "Gatenavn_",
                    Husnr = "123",
                    Bokstav = "B"
                }
            };
            var eiendomsAdresseValidator = MockDataSource.EiendomsAdresseValidator();
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(true, true);

            var eiendomByggestedValidator = new EiendomByggestedValidator(tree, eiendomsAdresseValidator,
                eiendomsidentifikasjonValidator, matrikkelService);

            eiendom.Bygningsnummer = "-1";

            var result = eiendomByggestedValidator.Validate(eiendom);

            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.15.2");
        }

        [Theory(Skip = null, DisplayName = "Eiendom - OK")]
        [Trait("Category", "Validator")]
        [ClassData(typeof(EiendomByggestedTestData))]
        public void Eiendom(IList<EntityValidatorNode> tree, Eiendom eiendom)
        {
            var eiendomsAdresseValidator = MockDataSource.EiendomsAdresseValidator();
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(true, true, true);

            var eiendomByggestedValidator = new EiendomByggestedValidator(tree, eiendomsAdresseValidator,
                eiendomsidentifikasjonValidator, matrikkelService);

            var result = eiendomByggestedValidator.Validate(eiendom);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory(DisplayName = ".7.15.24 - Bygningsnummer.numerisk", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomTestData))]
        public void Bygningsnummer_Numerisk(IList<EntityValidatorNode> tree, Eiendom eiendom)
        {
            var eiendomsAdresseValidator = MockDataSource.EiendomsAdresseValidator();
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(true, true);
        }

        [Theory(DisplayName = ".7.1 -  Eiendom.Utfilt", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomTestData))]
        public void Eiendom_Utfylt(IList<EntityValidatorNode> tree, Eiendom eiendom)
        {
            var eiendomsAdresseValidator = MockDataSource.EiendomsAdresseValidator();
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(true, true);

            var eiendomByggestedValidator = new EiendomByggestedValidator(tree, eiendomsAdresseValidator,
                eiendomsidentifikasjonValidator, matrikkelService);

            eiendom = null;

            var result = eiendomByggestedValidator.Validate(eiendom);

            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.1");

        }

        [Theory(DisplayName = ".7.15.24 - Bygningsnummer.numerisk", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomTestData))]
        public void Bygningsnummer_Numerisk1(IList<EntityValidatorNode> tree, Eiendom eiendom)
        {
            var eiendomsAdresseValidator = MockDataSource.EiendomsAdresseValidator();
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(true, true);

            var eiendomByggestedValidator = new EiendomByggestedValidator(tree, eiendomsAdresseValidator,
                eiendomsidentifikasjonValidator, matrikkelService);

            eiendom.Bygningsnummer = "a";

            var result = eiendomByggestedValidator.Validate(eiendom);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.15.24");
        }

        [Theory(DisplayName = ".7.15.2 - Bygningsnummer.gyldig", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomTestData))]
        public void Bygningsnummer_Gyldig(IList<EntityValidatorNode> tree, Eiendom eiendom)
        {
            var eiendomsAdresseValidator = MockDataSource.EiendomsAdresseValidator();
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(true, true);

            var eiendomByggestedValidator = new EiendomByggestedValidator(tree, eiendomsAdresseValidator,
                eiendomsidentifikasjonValidator, matrikkelService);

            eiendom.Bygningsnummer = "-1";

            var result = eiendomByggestedValidator.Validate(eiendom);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.15.2");
        }

        [Theory(DisplayName = ".7.15.3 - Bygningsnummer.validert", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomTestData))]
        public void Bygningsnummer_validert(IList<EntityValidatorNode> tree, Eiendom eiendom)
        {
            var eiendomsAdresseValidator = MockDataSource.EiendomsAdresseValidator();
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(true, true, null);

            var eiendomByggestedValidator = new EiendomByggestedValidator(tree, eiendomsAdresseValidator,
                eiendomsidentifikasjonValidator, matrikkelService);

            var result = eiendomByggestedValidator.Validate(eiendom);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.15.3");
        }

        [Theory(DisplayName = ".7.15.7 - Bygningsnummer.registrert", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomTestData))]
        public void Bygningsnummer_registrert(IList<EntityValidatorNode> tree, Eiendom eiendom)
        {
            var eiendomsAdresseValidator = MockDataSource.EiendomsAdresseValidator();
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(true, true, false);

            var eiendomByggestedValidator = new EiendomByggestedValidator(tree, eiendomsAdresseValidator,
                eiendomsidentifikasjonValidator, matrikkelService);

            var result = eiendomByggestedValidator.Validate(eiendom);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.15.7");
        }

        [Theory(DisplayName = ".7.16.2 - bolignummer.gyldig", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomTestData))]
        public void bolignummer_gyldig(IList<EntityValidatorNode> tree, Eiendom eiendom)
        {
            var eiendomsAdresseValidator = MockDataSource.EiendomsAdresseValidator();
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(true, true, true);

            var eiendomByggestedValidator = new EiendomByggestedValidator(tree, eiendomsAdresseValidator,
                eiendomsidentifikasjonValidator, matrikkelService);

            eiendom.Bolignummer = "noko";

            var result = eiendomByggestedValidator.Validate(eiendom);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.16.2");
        }

        [Theory(DisplayName = ".7.17.1 - kommunenavn.utfylt", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomTestData))]
        public void bolignummer_utfylt(IList<EntityValidatorNode> tree, Eiendom eiendom)
        {
            var eiendomsAdresseValidator = MockDataSource.EiendomsAdresseValidator();
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(true, true, true);

            var eiendomByggestedValidator = new EiendomByggestedValidator(tree, eiendomsAdresseValidator,
                eiendomsidentifikasjonValidator, matrikkelService);

            eiendom.Kommunenavn = null;

            var result = eiendomByggestedValidator.Validate(eiendom);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.17.1");
        }

        [Theory(DisplayName = ".7 - EiendomByggested.validert", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomTestData))]
        public void EiendomByggested_validert(IList<EntityValidatorNode> tree, Eiendom eiendom)
        {
            var eiendomsAdresseValidator = MockDataSource.EiendomsAdresseValidator();
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(null, true, true);

            var eiendomByggestedValidator = new EiendomByggestedValidator(tree, eiendomsAdresseValidator,
                eiendomsidentifikasjonValidator, matrikkelService);


            var result = eiendomByggestedValidator.Validate(eiendom);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7");
        }

        [Theory(DisplayName = ".7 - EiendomByggested.registrert", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomTestData))]
        public void EiendomByggested_registrert(IList<EntityValidatorNode> tree, Eiendom eiendom)
        {
            var eiendomsAdresseValidator =
                MockDataSource.EiendomsAdresseValidator("/eiendomByggested/eiendom{0}/adresse");
            var eiendomsidentifikasjonValidator = MockDataSource.EiendomsidentifikasjonValidator();
            var matrikkelService = MockDataSource.ImatrikkelServices(false, true, true);

            var eiendomByggestedValidator = new EiendomByggestedValidator(tree, eiendomsAdresseValidator,
                eiendomsidentifikasjonValidator, matrikkelService);


            var result = eiendomByggestedValidator.Validate(eiendom);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7");
        }
    

// MemberData for unit test
        public static IEnumerable<object[]> EiendomTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 1 },
                new() { NodeId = 3, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 1 },
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            var eiendom = EiendomByggestedTestData.GetEiendom();
            var avfallsplanEiendom = EiendomByggestedTestData.GetAvfallsplanEiendom();
            var arbeidstilsynetsSamtykkeEiendom = EiendomByggestedTestData.GetArbeidstilsynetsSamtykkeEiendom();

            yield return new object[] { entityValidationTree, eiendom };
            yield return new object[] { entityValidationTree, avfallsplanEiendom };
            yield return new object[] { entityValidationTree, arbeidstilsynetsSamtykkeEiendom };
        }

    }


    public class EiendomByggestedTestData : TheoryData<IList<EntityValidatorNode>, Eiendom>
    {
        public EiendomByggestedTestData()
        {
            Add(GetTree(), GetEiendom());
            Add(GetTree(), GetArbeidstilsynetsSamtykkeEiendom());
            Add(GetTree(), GetAvfallsplanEiendom());
        }
        internal static IList<EntityValidatorNode> GetTree()
        {
            List<EntityValidatorNode> eiendombyggestedNodeList = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 1 },
                new() { NodeId = 3, EnumId = EntityValidatorEnum.MetadataAvfallsplanV2Validator, ParentID = 1 },
            };
            return EntityValidatorTree.BuildTree(eiendombyggestedNodeList);
        }

        internal static Eiendom GetEiendom()
        {
            var eiendom = new EiendomsBuilder()
                .Eiendomsidentifikasjon("5001", "411", "13", "0", "0")
                .BuildingAdresse("Klingenbergs vei 3", "3", "Klingenbergs vei")
                .Building("123456789", "H0201", "Trondheim");

            return eiendom.Build();
        }
        internal static Eiendom GetArbeidstilsynetsSamtykkeEiendom()
        {
            var xmlData = File.ReadAllText(@"Data\ArbeidstilsynetsSamtykke_v2_dfv45957_Test.xml");
            var form = SerializeUtil.DeserializeFromString<ArbeidstilsynetsSamtykkeV2>(xmlData);
            return form.EiendomByggested.FirstOrDefault();
        }
        internal static Eiendom GetAvfallsplanEiendom()
        {
            var xmlData = File.ReadAllText(@"Data\Avfallsplan TM 20220405 korrigert.xml");
            var form = SerializeUtil.DeserializeFromString<SluttrapportForBygningsAvfall>(xmlData);
            return form.EiendomByggested.FirstOrDefault();
        }

    }
}
