﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.FellestjenesterBygg;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class TemaValidatorTests
    {
        private const string skip = "test";

        [Theory (DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TemaTestData))]
        public void TemaTestOk(IList<EntityValidatorNode> tree, Kodeliste tema)
        {
            var codeListService = MockDataSource.CodeListService();
            var temaValidator = new TemaValidator(tree, 1, codeListService);
      
            var result = temaValidator.Validate(tema);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory (DisplayName = "65.1 - tema.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TemaTestData))]
        public void TemaUtfylt(IList<EntityValidatorNode> tree, Kodeliste tema)
        {
            var codeListService = MockDataSource.CodeListService();
            var temaValidator = new TemaValidator(tree, 1, codeListService);

            tema = null;
            var result = temaValidator.Validate(tema);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".65.1");
        }

        [Theory(DisplayName = ".65.38.1 - tema.kodeverdi.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TemaTestData))]
        public void TemaKodeverdiUtfylt(IList<EntityValidatorNode> tree, Kodeliste tema)
        {
            var codeListService = MockDataSource.CodeListService();
            var temaValidator = new TemaValidator(tree, 1, codeListService);

            tema.Kodeverdi = null;
            var result = temaValidator.Validate(tema);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".65.38.1");
        }

        [Theory(DisplayName = ".65.38.2 - tema.kodeverdi.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TemaTestData))]
        public void TemaKodeverdiGyldig(IList<EntityValidatorNode> tree, Kodeliste tema)
        {
            var codeListService = MockDataSource.CodeListService(false);
            var temaValidator = new TemaValidator(tree, 1, codeListService);

            var result = temaValidator.Validate(tema);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".65.38.2");
        }

        [Theory(DisplayName = ".65.39.1 - tema.kodebeskrivelse.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TemaTestData))]
        public void TemaKodebeskrivelseUtfylt(IList<EntityValidatorNode> tree, Kodeliste tema)
        {
            var codeListService = MockDataSource.CodeListService();
            var temaValidator = new TemaValidator(tree, 1, codeListService);

            tema.Kodebeskrivelse = null;
            var result = temaValidator.Validate(tema);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".65.39.1");
        }

        [Theory(DisplayName = ".65.39.2 - tema.kodebeskrivelse.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TemaTestData))]
        public void TemaKodebeskrivelseGyldig(IList<EntityValidatorNode> tree, Kodeliste tema)
        {
            var codeListService = MockDataSource.CodeListService(true, false);
            var temaValidator = new TemaValidator(tree, 1, codeListService);

            var result = temaValidator.Validate(tema);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".65.39.2");
        }

        public static IEnumerable<object[]> TemaTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.TemaValidator, ParentID = null },              
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            Kodeliste tema = new Kodeliste() { Kodeverdi = "kodeverdi", Kodebeskrivelse = "kodebeskrivelse"};           

            yield return new object[] { entityValidationTree, tema };

        }
    }
}
