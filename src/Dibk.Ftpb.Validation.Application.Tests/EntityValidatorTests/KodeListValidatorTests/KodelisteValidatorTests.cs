﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System.Collections.Generic;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests.KodeListValidatorTests
{
    public class KodelisteValidatorTests
    {
        private Kodeliste _kodeliste;
        private KodelisteValidatorV2 _kodelisteValidator;
        private ICodeListService _codeListService;

        public KodelisteValidatorTests()
        {
            _codeListService = MockDataSource.CodeListService(true);

            _kodeliste = new Kodeliste()
            {
                Kodeverdi = "kodeverdi Value",
                Kodebeskrivelse = "kodebeskrivelse value"
            };

            var entityValidatorNodes = new List<EntityValidatorNode>()
            {
                new()
                {
                    NodeId = 1,
                    EnumId = EntityValidatorEnum.PartstypeValidator,
                    ParentID = null,

                }, //root node
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);
            _kodelisteValidator = new PartstypeValidator(entityValidationTree, 1, _codeListService);

        }
        [Fact(DisplayName = "kodelist utfylt - Error")]
        public void KodelistyeUtfylt()
        {
            _kodeliste = null;
            var result = _kodelisteValidator.Validate(_kodeliste);
            result.ValidationMessages.Count.Should().Be(1);
        }
    }
}
