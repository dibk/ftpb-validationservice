﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Xunit;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;
using Microsoft.Extensions.Logging;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests.KodeListValidatorTests
{
    public class KodelisteValidatorV2Tests
    {
        private const string skip = null;

        [Theory(Skip = skip, DisplayName = "kodelist allowedNull - false")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(KodelisteTestData))]
        public void KodelistyeallowedNullFalse(IList<EntityValidatorNode> tree, Enum codeListName, RegistryType registryType, Kodeliste kodeliste)
        {
            var codeListService = MockDataSource.CodeListService( true);
            var kodeListeValidator = new KodelisteValidatorV2(tree, 1, codeListName, registryType, codeListService, false);

            var result = kodeListeValidator.Validate(kodeliste);
            result.ValidationRules.Count.Should().Be(6);

        }
        [Theory(Skip = skip, DisplayName = "kodelist allowedNull - true")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(KodelisteTestData))]
        public void KodelistyeallowedNullTrue(IList<EntityValidatorNode> tree, Enum codeListName, RegistryType registryType, Kodeliste kodeliste)
        {
            var codeListService = MockDataSource.CodeListService( true);
            var kodeListeValidator = new Logic.EntityValidators.KodelisteValidatorV2(tree, 1, codeListName, registryType, codeListService, true);

            var result = kodeListeValidator.Validate(kodeliste);
            result.ValidationRules.Count.Should().Be(4);
        }
        [Theory(Skip = skip, DisplayName = ".16.1 kodelist.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(KodelisteTestData))]
        public void Kodelistutfylt(IList<EntityValidatorNode> tree, Enum codeListName, RegistryType registryType, Kodeliste kodeliste)
        {
            var codeListService = MockDataSource.CodeListService(true);
            var kodeListeValidator = new Logic.EntityValidators.KodelisteValidatorV2(tree, 1, codeListName, registryType, codeListService, false);

            kodeliste = null;
            var result = kodeListeValidator.Validate(kodeliste);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".16.1");
        }
        [Theory(Skip = skip, DisplayName = ".16.38.1 Kodeverdi.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(KodelisteTestData))]
        public void Kodeverdiutfylt(IList<EntityValidatorNode> tree, Enum codeListName, RegistryType registryType, Kodeliste kodeliste)
        {
            var codeListService = MockDataSource.CodeListService( true);
            var kodeListeValidator = new KodelisteValidatorV2(tree, 1, codeListName, registryType, codeListService, false);

            kodeliste.Kodeverdi = null;
            var result = kodeListeValidator.Validate(kodeliste);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".16.38.1");
        }
        [Theory(Skip = skip, DisplayName = ".16.38.3 Kodeverdi.validert")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(KodelisteTestData))]
        public void Kodeverdivalidert(IList<EntityValidatorNode> tree, Enum codeListName, RegistryType registryType, Kodeliste kodeliste)
        {
            var codeListService = MockDataSource.CodeListService( null);
            var kodeListeValidator = new KodelisteValidatorV2(tree, 1, codeListName, registryType, codeListService, false);

            var result = kodeListeValidator.Validate(kodeliste);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".16.38.3");
        }
        [Theory(Skip = skip, DisplayName = ".16.38.2 Kodeverdi.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(KodelisteTestData))]
        public void Kodeverdigyldig(IList<EntityValidatorNode> tree, Enum codeListName, RegistryType registryType, Kodeliste kodeliste)
        {
            var codeListService = MockDataSource.CodeListService( false);
            var kodeListeValidator = new KodelisteValidatorV2(tree, 1, codeListName, registryType, codeListService, false);

            var result = kodeListeValidator.Validate(kodeliste);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".16.38.2");
        }
        [Theory(Skip = skip, DisplayName = ".16.39.1 Kodebeskrivelse.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(KodelisteTestData))]
        public void KodebeskrivelseUtfylt(IList<EntityValidatorNode> tree, Enum codeListName, RegistryType registryType, Kodeliste kodeliste)
        {
            var codeListService = MockDataSource.CodeListService();
            var kodeListeValidator = new KodelisteValidatorV2(tree, 1, codeListName, registryType, codeListService, false);

            kodeliste.Kodebeskrivelse = null;
            var result = kodeListeValidator.Validate(kodeliste);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".16.39.1");
        }
        [Theory(Skip = skip, DisplayName = ".16.39.2 Kodebeskrivelse.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(KodelisteTestData))]
        public void KodebeskrivelseGyldig(IList<EntityValidatorNode> tree, Enum codeListName, RegistryType registryType, Kodeliste kodeliste)
        {
            var codeListService = MockDataSource.CodeListService(true, false);
            var kodeListeValidator = new KodelisteValidatorV2(tree, 1, codeListName, registryType, codeListService, false);

            var result = kodeListeValidator.Validate(kodeliste);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".16.39.2");
        }
        //TODO ....

        [Theory(Skip = skip, DisplayName = "KodelisteValidatorV2 - Byggesoknad.OK")]
        [Trait("Category", "ExternalApiCall")]
        [MemberData(nameof(TestDataByggesoknad))]
        public void Kodelistnoko(KodelisteValidatorV2 kodelisteValidatorV2, Kodeliste kodeliste)
        {
            var result = kodelisteValidatorV2.Validate(kodeliste);
            result.ValidationMessages.Count.Should().Be(0);
        }

        public static IEnumerable<object[]> KodelisteTestData()
        {
            var entityValidatorNodes = new List<EntityValidatorNode>()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = null, }
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);
            var kodeliste = new Kodeliste()
            { Kodeverdi = "Privatperson", Kodebeskrivelse = "Privatperson" };
            yield return new object[] { entityValidationTree, FtbKodeListeEnum.Partstype, RegistryType.Byggesoknad, kodeliste };
        }
        public static IEnumerable<object[]> TestDataByggesoknad()
        {

            var memoryCache = TestHelper.GetMemoryCache();
            var options = TestHelper.GetCodeListApiSettins();

            var services = new ServiceCollection();
            services.AddMemoryCache();
            services.AddHttpClient();
            var serviceProvider = services.BuildServiceProvider();
            var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
            var mockLogger = new Moq.Mock<ILogger<CodeListApiService>>();

            ICodeListService codeListService = new CodeListService(memoryCache, options, new CodeListApiService(mockLogger.Object,options, httpClientFactory));

            var entityValidatorNodes = new List<EntityValidatorNode>() {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = null, }
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            yield return new object[] { new PartstypeValidator(entityValidationTree, 1, codeListService), new Kodeliste() { Kodeverdi = "Privatperson", Kodebeskrivelse = "Privatperson" } };
            yield return new object[] { new AnleggstypeValidator(entityValidationTree, codeListService), new Kodeliste() { Kodeverdi = "brygger", Kodebeskrivelse = "Brygger" } };
            yield return new object[] { new AvfalldisponeringsmaateValidator(entityValidationTree, 1, codeListService), new Kodeliste() { Kodeverdi = "direkteFormaal", Kodebeskrivelse = "Levert direkte til gjenvinning til utfyllingsformål/teknisk formål, eks. betong/tegl" } };
            yield return new object[] { new AvfallfraksjonValidator(entityValidationTree, 1, codeListService), new Kodeliste() { Kodeverdi = "1619", Kodebeskrivelse = "Asfalt" } };
        }
    }
}
