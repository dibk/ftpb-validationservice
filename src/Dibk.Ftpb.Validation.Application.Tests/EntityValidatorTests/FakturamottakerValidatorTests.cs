﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.IO;
using Xunit;
using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class FakturamottakerValidatorTests
    {

        private readonly IPostalCodeService _postalCodeService;

        private IFakturamottakerValidator _fakturamottakerValidator;
        private Fakturamottaker _fakturamottaker;
        private ArbeidstilsynetsSamtykkeV2 _form;
        private IEnkelAdresseValidator _fakturamottakerEnkelAdresseValidator;

        public FakturamottakerValidatorTests()
        {
            _postalCodeService = MockDataSource.ValidatePostnr(true, "Bø i telemark", "true");

            var xmlData = File.ReadAllText(@"Data\ArbeidstilsynetsSamtykke_v2_dfv45957.xml");
            _form = SerializeUtil.DeserializeFromString<ArbeidstilsynetsSamtykkeV2>(xmlData);
            _fakturamottaker =_form.Fakturamottaker;

            //fakturamottake
            var fakturamottakeNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 1, EnumId = EntityValidatorEnum.FakturamottakerValidator, ParentID = null},
            };
            var tree = EntityValidatorTree.BuildTree(fakturamottakeNodeList);

            _fakturamottakerEnkelAdresseValidator = MockDataSource.enkelAdresseValidator();
            
            // ATIL Feature - Endres etter 20250217 - START
            _fakturamottakerValidator = new FakturamottakerValidator(tree, _fakturamottakerEnkelAdresseValidator, FeatureManagerMocks.AtilFeatureManager());

        }

        [Fact]
        public void FakturaTest()
        {
            _fakturamottaker.EhfFaktura = null;
            _fakturamottaker.FakturaPapir = null;
            _fakturamottakerValidator.ValidateEntityFields(_form, _fakturamottaker);
            var result = _fakturamottakerValidator.ValidationResult.ValidationMessages;
            result.Count.Should().Be(1);
        }
        [Fact]
        public void Faktura_organisasjonsnummer_Utfylt()
        {
            _fakturamottaker.Organisasjonsnummer = null;

            _fakturamottakerValidator.ValidateEntityFields(_form, _fakturamottaker);
            var result = _fakturamottakerValidator.ValidationResult.ValidationMessages;
            result.Count.Should().Be(1);
        }
        [Fact(Skip = "mock error")]
        public void Faktura_Adresse()
        {
            _fakturamottaker.EhfFaktura = null;
            _fakturamottaker.FakturaPapir = true;
            
            _fakturamottaker.Adresse.Adresselinje1 = null;

           var result =  _fakturamottakerValidator.Validate(_form, _fakturamottaker);

           result.Messages.Should().NotBeNull();
        }
    }
}
