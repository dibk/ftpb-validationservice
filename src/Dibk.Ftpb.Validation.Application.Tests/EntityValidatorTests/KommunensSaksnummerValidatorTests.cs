﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class KommunensSaksnummerValidatorTests
    {
        private const string skip = "test";

        [Theory (DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void SaksnummerTestOk(Saksnummer saksnummer, KommunensSaksnummerValidator kommunensSaksnummerValidator)
        {      
            var result = kommunensSaksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory (DisplayName = ".25.1 - kommunensSaksnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void KommunensSaksnummerUtfylt(Saksnummer saksnummer, KommunensSaksnummerValidator kommunensSaksnummerValidator)
        {
            saksnummer = null;
            var result = kommunensSaksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".25.1");
        }

        [Theory(DisplayName = ".25.108.1 - kommunensSaksnummer.Saksaar.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void KommunensSaksnummerSaksaarUtfylt(Saksnummer saksnummer, KommunensSaksnummerValidator kommunensSaksnummerValidator)
        {
            saksnummer.Saksaar = null;
            saksnummer.Sakssekvensnummer = 12345;
            var result = kommunensSaksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".25.108.1");
        }

        [Theory(DisplayName = ".25.108.2 - kommunensSaksnummer.Saksaar.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void KommunensSaksnummerSaksaarGyldig(Saksnummer saksnummer, KommunensSaksnummerValidator kommunensSaksnummerValidator)
        {
            saksnummer.Saksaar = -11;
            var result = kommunensSaksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".25.108.2");
        }

        [Theory(DisplayName = ".25.109.1 - kommunensSaksnummer.Saksekvensnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void KommunensSaksnummerSekvensnummerUtfylt(Saksnummer saksnummer, KommunensSaksnummerValidator kommunensSaksnummerValidator)
        {
            saksnummer.Sakssekvensnummer = null;
            saksnummer.Saksaar = 2022;
            var result = kommunensSaksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".25.109.1");
        }

        public static IEnumerable<object[]> SaksnummerTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.KommunensSaksnummerValidator, ParentID = null },              
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            Saksnummer saksnummer = new Saksnummer() { Saksaar = 2022, Sakssekvensnummer = 20221212 };

            var saksnummerValidator = new KommunensSaksnummerValidator(entityValidationTree);

            yield return new object[] { saksnummer, saksnummerValidator };

        }
    }
}
