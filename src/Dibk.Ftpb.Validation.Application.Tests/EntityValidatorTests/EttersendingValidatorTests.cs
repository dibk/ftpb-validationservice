﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class EttersendingValidatorTests
    {
        private const string skip = "test";

        [Theory (DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EttersendingTestData))]
        public void EttersendingTestOk(EttersendingValidator ettersendingValidator, Ettersending ettersending)
        {
            var result = ettersendingValidator.Validate(ettersending);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory(DisplayName = ".58.152.1 - tittel.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EttersendingTestData))]
        public void TittelUtfylt(EttersendingValidator ettersendingValidator, Ettersending ettersending)
        {
            ettersending.Tittel = null;
            var result = ettersendingValidator.Validate(ettersending);         
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".58.152.1");
        }

        [Theory(DisplayName = ".58.158.1 - vedlegg_underskjema_kommentar.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EttersendingTestData))]
        public void VedleggUnderskjemaKommentarUtfylt(EttersendingValidator ettersendingValidator, Ettersending ettersending)
        {
            ettersending.Vedlegg = null;
            ettersending.Underskjema = null;
            ettersending.Kommentar = null;
            var result = ettersendingValidator.Validate(ettersending);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".58.158.1");
        }

        public static IEnumerable<object[]> EttersendingTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.EttersendingValidator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.SuppleringVedleggValidator, ParentID = 1 },
                new() { NodeId = 3, EnumId = EntityValidatorEnum.TemaValidator, ParentID = 1 },         
                new() { NodeId = 5, EnumId = EntityValidatorEnum.ArbeidstilsynetVedleggstypeValidator, ParentID = 2 },                          
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            Ettersending ettersending = new Ettersending()
            {
                Vedlegg = new Vedlegg[] { new Vedlegg()
                { Filnavn = "filnavn", Vedleggstype = new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste() { Kodeverdi = "kodeverdi", Kodebeskrivelse = "kodebeskrivelse" }, Versjonsdato = DateTime.Now, Versjonsnummer = "1.1" } },
                Tema = new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste() { Kodeverdi = "kodeverdi", Kodebeskrivelse = "kodebeskrivelse" },
                Tittel = "tittel",
                Kommentar = "kommentar",     
                Underskjema = null,
            };

            var vedleggstypeValidator = MockDataSource.KodelisteValidator();
            var suppleringVedleggValidator = new SuppleringVedleggValidator(entityValidationTree, 2, vedleggstypeValidator);
            var temaValidator = MockDataSource.KodelisteValidator();
            var ettersendingValidator = new EttersendingValidator(entityValidationTree, suppleringVedleggValidator, temaValidator);

            yield return new object[] { ettersendingValidator, ettersending };

        }
    }
}
