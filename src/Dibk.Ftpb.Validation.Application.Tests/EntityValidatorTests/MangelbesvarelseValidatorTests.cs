﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class MangelbesvarelseValidatorTests
    {
        private const string skip = "test";

        [Theory (DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelbesvarelseTestData))]
        public void MangelTestOk(MangelbesvarelseValidator mangelbesvarelseValidator, Mangelbesvarelse mangelbesvarelse)
        {
            var result = mangelbesvarelseValidator.Validate(mangelbesvarelse);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory(DisplayName = ".62.152.1 - tittel.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelbesvarelseTestData))]
        public void TittelUtfylt(MangelbesvarelseValidator mangelbesvarelseValidator, Mangelbesvarelse mangelbesvarelse)
        {            
            mangelbesvarelse.Tittel = null;
            var result = mangelbesvarelseValidator.Validate(mangelbesvarelse);         
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".62.152.1");
        }

        [Theory(DisplayName = ".62.154.1 - beskrivelseFraKommunen.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelbesvarelseTestData))]
        public void BeskrivelseFraKommunenUtfylt(MangelbesvarelseValidator mangelbesvarelseValidator, Mangelbesvarelse mangelbesvarelse)
        {
            mangelbesvarelse.BeskrivelseFraKommunen = null;
            var result = mangelbesvarelseValidator.Validate(mangelbesvarelse);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".62.154.1");
        }

        [Theory(DisplayName = ".62.156.1 - erMangelBesvaresSenere.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelbesvarelseTestData))]
        public void ErMangelBesvaresSenereUtfylt(MangelbesvarelseValidator mangelbesvarelseValidator, Mangelbesvarelse mangelbesvarelse)
        {
            mangelbesvarelse.ErMangelBesvaresSenere = null;
            var result = mangelbesvarelseValidator.Validate(mangelbesvarelse);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".62.156.1");
        }

        [Theory(DisplayName = ".62.153.1 - kommentar.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelbesvarelseTestData))]
        public void KommentarUtfylt(MangelbesvarelseValidator mangelbesvarelseValidator, Mangelbesvarelse mangelbesvarelse)
        {
            mangelbesvarelse.ErMangelBesvaresSenere = true;
            mangelbesvarelse.Kommentar = null;
            var result = mangelbesvarelseValidator.Validate(mangelbesvarelse);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".62.153.1");
        }

        [Theory(DisplayName = ".62.158.1 - vedlegg_underskjema_kommentar.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelbesvarelseTestData))]
        public void VedleggUnderskjemaKommentarUtfylt(MangelbesvarelseValidator mangelbesvarelseValidator, Mangelbesvarelse mangelbesvarelse)
        {
            mangelbesvarelse.Vedlegg = null;
            mangelbesvarelse.Underskjema = null;
            mangelbesvarelse.Kommentar = null;
            var result = mangelbesvarelseValidator.Validate(mangelbesvarelse);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".62.158.1");
        }

        public static IEnumerable<object[]> MangelbesvarelseTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.MangelbesvarelseValidator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.SuppleringVedleggValidator, ParentID = 1 },
                new() { NodeId = 3, EnumId = EntityValidatorEnum.TemaValidator, ParentID = 1 },
                new() { NodeId = 4, EnumId = EntityValidatorEnum.MangelValidator, ParentID = 1 },           
                new() { NodeId = 5, EnumId = EntityValidatorEnum.ArbeidstilsynetVedleggstypeValidator, ParentID = 2 },               
                new() { NodeId = 6, EnumId = EntityValidatorEnum.MangeltekstValidator, ParentID = 4 },               
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            Mangelbesvarelse mangelbesvarelse = new Mangelbesvarelse()
            {
                Vedlegg = new Vedlegg[] { new Vedlegg()
                { Filnavn = "filnavn", Vedleggstype = new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste() { Kodeverdi = "kodeverdi", Kodebeskrivelse = "kodebeskrivelse" }, Versjonsdato = DateTime.Now, Versjonsnummer = "1.1" } },
                Tema = new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste() { Kodeverdi = "kodeverdi", Kodebeskrivelse = "kodebeskrivelse" },
                Mangel = new Mangel() { MangelId = "mangelId", Mangeltekst = new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste() { Kodeverdi = "kodeverdi", Kodebeskrivelse = "kodebeskrivelse" } },
                Tittel = "tittel",
                BeskrivelseFraKommunen = "beskrivelseFraKommunen",
                ErMangelBesvart = false,
                ErMangelBesvaresSenere = false,
                Kommentar = "kommentar",               
            };

            var vedleggstypeValidator = MockDataSource.KodelisteValidator();
            var suppleringVedleggValidator = new SuppleringVedleggValidator(entityValidationTree, 2, vedleggstypeValidator);
            var temaValidator = MockDataSource.KodelisteValidator();
            var mangeltekstValidator = MockDataSource.KodelisteValidator();
            var mangelValidator = new MangelValidator(entityValidationTree, 4, mangeltekstValidator);
            var mangelbesvarelseValidator = new MangelbesvarelseValidator(entityValidationTree, suppleringVedleggValidator, temaValidator, mangelValidator);

            yield return new object[] { mangelbesvarelseValidator, mangelbesvarelse };

        }
    }
}
