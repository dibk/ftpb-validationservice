﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class SuppleringVedleggValidatorTests
    {
        private const string skip = "test";

        [Theory (DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SuppleringVedleggTestData))]
        public void VedleggTestOk(SuppleringVedleggValidator suppleringVedleggValidator, Vedlegg suppleringVedlegg)
        {      
            var result = suppleringVedleggValidator.Validate(suppleringVedlegg);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory(DisplayName = ".59.149.1 - vedlegg.versjonsnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SuppleringVedleggTestData))]
        public void VedleggVersjonsnummerUtfylt(SuppleringVedleggValidator suppleringVedleggValidator, Vedlegg suppleringVedlegg)
        {
            suppleringVedlegg.Versjonsnummer = null;
            var result = suppleringVedleggValidator.Validate(suppleringVedlegg);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".59.149.1");
        }

        [Theory (DisplayName = ".59.150.1 - vedlegg.versjonsdato.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SuppleringVedleggTestData))]
        public void VedleggVersjonsdatoUtfylt(SuppleringVedleggValidator suppleringVedleggValidator, Vedlegg suppleringVedlegg)
        {
            suppleringVedlegg.Versjonsdato = null;
            var result = suppleringVedleggValidator.Validate(suppleringVedlegg);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".59.150.1");
        }

        [Theory(DisplayName = ".59.151.1 - vedlegg.filnavn.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SuppleringVedleggTestData))]
        public void VedleggFilnavnUtfylt(SuppleringVedleggValidator suppleringVedleggValidator, Vedlegg suppleringVedlegg)
        {
            suppleringVedlegg.Filnavn = null;
            var result = suppleringVedleggValidator.Validate(suppleringVedlegg);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".59.151.1");
        }             

        public static IEnumerable<object[]> SuppleringVedleggTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.SuppleringVedleggValidator, ParentID = null },              
                new() { NodeId = 2, EnumId = EntityValidatorEnum.ArbeidstilsynetVedleggstypeValidator, ParentID = 1 },              
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            Vedlegg suppleringVedlegg = new Vedlegg() 
            { 
                Filnavn = "filnavn", 
                Vedleggstype = new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste() { Kodeverdi = "kodeverdi", Kodebeskrivelse = "kodebeskrivelse" }, 
                Versjonsdato = DateTime.Now, 
                Versjonsnummer = "1.1" 
            };

            var codeListService = MockDataSource.CodeListService();
            var vedleggstypeValidator = new ArbeidstilsynetVedleggstypeValidator(entityValidationTree, 2, codeListService);
            var suppleringVedleggValidator = new SuppleringVedleggValidator(entityValidationTree, 1, vedleggstypeValidator);

            yield return new object[] { suppleringVedleggValidator, suppleringVedlegg };

        }
    }
}
