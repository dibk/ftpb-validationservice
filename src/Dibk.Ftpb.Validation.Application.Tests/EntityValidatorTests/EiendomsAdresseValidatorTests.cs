﻿using Dibk.Ftpb.Validation.Application.DataSources;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;
using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class EiendomsAdresseValidatorTests
    {
        private readonly Eiendom[] _eiendomValidationEntities;

        private FormValidatorConfiguration _formValidatorConfiguration;
        private EiendomsAdresseValidator _validator;

        public EiendomsAdresseValidatorTests()
        {
            var xmlData = File.ReadAllText(@"Data\ArbeidstilsynetsSamtykke_v2_dfv45957.xml");
            var form = SerializeUtil.DeserializeFromString<ArbeidstilsynetsSamtykkeV2>(xmlData);

            _eiendomValidationEntities = form.EiendomByggested;

            _formValidatorConfiguration = new FormValidatorConfiguration();
            _formValidatorConfiguration.ValidatorFormName = "ArbeidstilsynetsSamtykke2_45957_Validator";
            _formValidatorConfiguration.FormXPathRoot = "ArbeidstilsynetsSamtykke";

            var validatorNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 1, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = null},
            };

            _validator = new EiendomsAdresseValidator(EntityValidatorTree.BuildTree(validatorNodeList), 1);
        }

        [Fact]
        public void testMatrikkel()
        {
            var matrikkelInfo = _eiendomValidationEntities.ToArray();
            var result = _validator.Validate(matrikkelInfo[0].Adresse);

            result.Should().NotBeNull();
        }

    }
}
