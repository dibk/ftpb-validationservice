﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.GeneralValidations;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class NorskStandardValidatorTests
    {
        [Fact]
        public void ValidateFoedselsnummer_validInput_returnsOk()
        {
            // Arrange
            var fnr = "25125401530";
            var decryptionFactory = MockDataSource.DecryptText(fnr);
            // Act
            var result = NorskStandardValidator.ValidateFoedselsnummer(fnr, decryptionFactory);
            // Assert
            Assert.Equal(FoedselnumerValidation.Ok, result);
        }

        [Fact]
        public void ValidateFoedselsnummer_invalidInputCharacters_returnsInvalid()
        {
            // Arrange
            var fnr = "EgErFnr";
            var decryptionFactory = MockDataSource.DecryptText(fnr);
            // Act
            var result = NorskStandardValidator.ValidateFoedselsnummer(fnr, decryptionFactory);
            // Assert
            Assert.Equal(FoedselnumerValidation.Invalid, result);
        }

        [Fact]
        public void ValidateFoedselsnummer_invalidInputTooLong_returnsInvalid()
        {
            // Arrange
            var fnr = "123456789987654";
            var decryptionFactory = MockDataSource.DecryptText(fnr);
            // Act
            var result = NorskStandardValidator.ValidateFoedselsnummer(fnr, decryptionFactory);
            // Assert
            Assert.Equal(FoedselnumerValidation.Invalid, result);
        }

        [Fact]
        public void ValidateFoedselsnummer_invalidInputTooShort_returnsInvalid()
        {
            // Arrange
            var fnr = "1234";
            var decryptionFactory = MockDataSource.DecryptText(fnr);
            // Act
            var result = NorskStandardValidator.ValidateFoedselsnummer(fnr, decryptionFactory);
            // Assert
            Assert.Equal(FoedselnumerValidation.Invalid, result);
        }

        [Fact]
        public void ValidateFoedselsnummer_invalidInputWrongControlDigit_returnsInvalidDigitsControl()
        {
            // Arrange
            var fnr = "25125401531";
            var decryptionFactory = MockDataSource.DecryptText(fnr);
            // Act
            var result = NorskStandardValidator.ValidateFoedselsnummer(fnr, decryptionFactory);
            // Assert
            Assert.Equal(FoedselnumerValidation.InvalidDigitsControl, result);
        }

        [Fact]
        public void ValidateOrgnr_validInput_returnsOk()
        {
            // Arrange
            var orgnr = "911455307";
            // Act
            var result = NorskStandardValidator.ValidateOrgnummer(orgnr);
            // Assert
            Assert.Equal(OrganisasjonsnummerValidation.Ok, result);
        }

        [Fact]
        public void ValidateOrgnr_invalidInputCharacters_returnsInvalid()
        {
            // Arrange
            var orgnr = "EgErOrgnr";
            // Act
            var result = NorskStandardValidator.ValidateOrgnummer(orgnr);
            // Assert
            Assert.Equal(OrganisasjonsnummerValidation.Invalid, result);
        }

        [Fact]
        public void ValidateOrgnr_invalidInputTooLong_returnsInvalid()
        {
            // Arrange
            var orgnr = "123456789321";
            // Act
            var result = NorskStandardValidator.ValidateOrgnummer(orgnr);
            // Assert
            Assert.Equal(OrganisasjonsnummerValidation.Invalid, result);
        }

        [Fact]
        public void ValidateOrgnr_invalidInputControlDigit_returnsInvalid()
        {
            // Arrange
            var orgnr = "911455305";
            // Act
            var result = NorskStandardValidator.ValidateOrgnummer(orgnr);
            // Assert
            Assert.Equal(OrganisasjonsnummerValidation.InvalidDigitsControl, result);
        }
    }
}