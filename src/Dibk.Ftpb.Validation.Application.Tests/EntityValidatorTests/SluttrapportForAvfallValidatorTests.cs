﻿using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class SluttrapportForAvfallValidatorTests
    {
        private SluttrapportForBygningsAvfall _form;
        private IList<EntityValidatorNode> _sluttrapportTree;
        //private IKodelisteValidator _avfallplanAvfallklasseValidator;
        //private IKodelisteValidator _avfallplanAvfallfraksjonValidator;
        //private AvfallplanValidator _avfallplanValidator;
        //private IKodelisteValidator _avfallplanAvfallklassedelsumAvfallklasseValidator;
        //private AvfallklasseDelsumValidator _avfallplanAvfallklasseDelsumValidator;
        //private AvfallsorteringValidator _avfallplanSorteringValidator;
        //private PlanForAvfallValidator _planForAvfallValidator;

        //**
        private ISluttrapportForAvfallValidator _sluttrapportForAvfallValidator;
        private IKodelisteValidator _avfallrapportAvfallklasseValidator;
        private IKodelisteValidator _avfallrapportAvfallfraksjonValidator;
        private IKodelisteValidator _avfallrapportAvfallklassedelsumAvfallklasseValidator;
        private IAvfallsorteringValidator _avfallsluttrapportSorteringValidator;
        private IKodelisteValidator _disponeringDisponeringsmaateValidator;
        private IKodelisteValidator _disponeringsmaatedelsumDisponeringsmaateValidator;
        private ILeveringsstedValidator _leveringsstedValidator;
        private IAvfallrapportValidator _avfallrapportValidator;
        private IAvfallklasseDelsumValidator _avfallrapportAvfallklasseDelsumValidator;

        private IDisponeringsmaateDelsumValidator _disponeringsmaateDelSumValidator;

        private readonly IAvfalldisponeringValidator _avfalldisponeringValidator;

        //**
        public SluttrapportForAvfallValidatorTests()
        {
            var xmlData = File.ReadAllText(@"Data\Avfallsplan TM 20220405 korrigert.xml");
            _form = SerializeUtil.DeserializeFromString<SluttrapportForBygningsAvfall>(xmlData);
            var sluttrapportForAvfallNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 25, EnumId = EntityValidatorEnum.SluttrapportForAvfallValidator, ParentID = null},

                new () {NodeId = 26, EnumId = EntityValidatorEnum.AvfallrapportValidator, ParentID = 25},

                new () {NodeId = 27, EnumId = EntityValidatorEnum.AvfallklasseValidator, ParentID = 26},
                new () {NodeId = 28, EnumId = EntityValidatorEnum.AvfallfraksjonValidator, ParentID = 26},
                new () {NodeId = 29, EnumId = EntityValidatorEnum.AvfalldisponeringValidator, ParentID = 26},

                new () {NodeId = 30, EnumId = EntityValidatorEnum.LeveringsstedValidator, ParentID = 29},
                new () {NodeId = 31, EnumId = EntityValidatorEnum.DisponeringsmaateValidator, ParentID = 29},

                new () {NodeId = 32, EnumId = EntityValidatorEnum.AvfallsorteringValidator, ParentID = 25},

                new () {NodeId = 33, EnumId = EntityValidatorEnum.DisponeringsmaateDelsumValidator, ParentID = 25},
                new () {NodeId = 34, EnumId = EntityValidatorEnum.DisponeringsmaateValidator, ParentID = 33},

                new () {NodeId = 35, EnumId = EntityValidatorEnum.AvfallsklasseDelsumValidator, ParentID = 25},
                new () {NodeId = 36, EnumId = EntityValidatorEnum.AvfallklasseValidator, ParentID = 35},
            };
            _sluttrapportTree = EntityValidatorTree.BuildTree(sluttrapportForAvfallNodeList);


            var _avfallsklasseDelsumAvfallsklasseCodeListService = MockDataSource.CodeListService( true);

            var tree = _sluttrapportTree;
            ////PlanForAvfall
            ////_avfallplanAvfallklasseValidator = new AvfallklasseValidator(tree, 20, _planAvfallklasseCodeListService);
            //_avfallplanAvfallklasseValidator = MockDataSource.KodelisteValidator();
            ////_avfallplanAvfallfraksjonValidator = new AvfallfraksjonValidator(tree, 21, _planFraksjonCodeListService);
            //_avfallplanAvfallfraksjonValidator = MockDataSource.KodelisteValidator();
            //_avfallplanValidator = new AvfallplanValidator(tree, _avfallplanAvfallklasseValidator, _avfallplanAvfallfraksjonValidator);


            // avfall/avfallrapport

            //var avfallklasseCodeListService = MockDataSource.CodeListService(FtbKodeListeEnum.Avfallklasse, true);
            //var fraksjonCodeListService = MockDataSource.CodeListService(FtbKodeListeEnum.Avfallfraksjoner_NS, true);
            //_avfallrapportAvfallfraksjonValidator = new AvfallfraksjonValidator(tree, 28, fraksjonCodeListService);
            //_avfallrapportAvfallklasseValidator = new AvfallklasseValidator(tree, 27, avfallklasseCodeListService);

            _avfallrapportAvfallfraksjonValidator = MockDataSource.KodelisteValidator();
            _avfallrapportAvfallklasseValidator = MockDataSource.KodelisteValidator();

            ///avfall/avfallrapport/disponering
            _leveringsstedValidator = new LeveringsstedValidator(tree);
            //var disponeringsmaateCodeListService = MockDataSource.CodeListService(FtbKodeListeEnum.Avfalldisponeringsmåte, true);
            //_disponeringDisponeringsmaateValidator = new DisponeringsmaateValidator(tree, 31, disponeringsmaateCodeListService);
            
            _disponeringDisponeringsmaateValidator = MockDataSource.KodelisteValidator();
            _avfalldisponeringValidator = new AvfalldisponeringValidator(tree, _leveringsstedValidator, _disponeringDisponeringsmaateValidator);

            _avfallrapportValidator = new AvfallrapportValidator(tree, _avfallrapportAvfallklasseValidator, _avfallrapportAvfallfraksjonValidator, _avfalldisponeringValidator);

            //disponeringsmaateDelsum/disponeringsmaatedelsum
            //var avfallDisponeringsmåteCodeListService = MockDataSource.CodeListService(FtbKodeListeEnum.Avfalldisponeringsmåte, true);
            //_disponeringsmaatedelsumDisponeringsmaateValidator = new DisponeringsmaateValidator(tree, 34, avfallDisponeringsmåteCodeListService);
            
            _disponeringsmaatedelsumDisponeringsmaateValidator = MockDataSource.KodelisteValidator();
            _disponeringsmaateDelSumValidator = new DisponeringsmaateDelsumValidator(tree, _disponeringsmaatedelsumDisponeringsmaateValidator);

            // avfallsklasseDelsum/avfallsklassedelsum
            //var avfallklasseDelsumAvfallklasseCodeListService = MockDataSource.CodeListService(FtbKodeListeEnum.Avfallklasse, true);
            //_avfallrapportAvfallklassedelsumAvfallklasseValidator = new AvfallklasseValidator(tree, 36, avfallklasseDelsumAvfallklasseCodeListService);
            
            _avfallrapportAvfallklassedelsumAvfallklasseValidator = MockDataSource.KodelisteValidator();

            _avfallrapportAvfallklasseDelsumValidator = new AvfallklasseDelsumValidator(tree, 35, _avfallrapportAvfallklassedelsumAvfallklasseValidator);
            
            //sluttrapport/sortering
            _avfallsluttrapportSorteringValidator = new AvfallsorteringValidator(tree, 32);

            _sluttrapportForAvfallValidator = new SluttrapportForAvfallValidator(tree, _avfallrapportValidator, _disponeringsmaateDelSumValidator, _avfallrapportAvfallklasseDelsumValidator, _avfallsluttrapportSorteringValidator);


        }
        [Fact]
        public void TestPartstype()
        {
            var result = _sluttrapportForAvfallValidator.Validate(_form.SluttrapportForAvfall);
            result.Should().NotBeNull();
        }
    }
}
