﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Atil;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Atil;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class MetadataSuppleringValidatorTests
    {
        private const string skip = "test";

        [Theory (DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MetadataTestData))]
        public void MetadataTestOk(MetadataSuppleringValidator metadataValidator, MetadataAtilSupplering metadata)
        {      
            var result = metadataValidator.Validate(metadata);
            result.ValidationMessages.Count.Should().Be(0);
        }




        [Theory (DisplayName = "69.1 - metadata.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MetadataTestData))]
        public void MetadataUtfylt(MetadataSuppleringValidator metadataValidator, MetadataAtilSupplering metadata)
        {
            metadata = null;
            var result = metadataValidator.Validate(metadata);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".69.1");
        }

        [Theory(DisplayName = "69.90.1 - metadata.fraSluttrbukersystem.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MetadataTestData))]
        public void MetadataFraSluttbrukersystemUtfylt(MetadataSuppleringValidator metadataValidator, MetadataAtilSupplering metadata)
        {
            metadata.FraSluttbrukersystem = null;
            var result = metadataValidator.Validate(metadata);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".69.90.1");
        }

        public static IEnumerable<object[]> MetadataTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.MetadataSuppleringValidator, ParentID = null },              
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            MetadataAtilSupplering metadata = new MetadataAtilSupplering() { Prosjektnavn = "Prosjektnavn", FraSluttbrukersystem = "fraSluttrbukersystem" };

            var metadataValidator = new MetadataSuppleringValidator(entityValidationTree);

            yield return new object[] { metadataValidator, metadata };

        }
    }
}
