﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class KommunensSaksnummerArrayValidatorTests
    {
        private const string skip = "test";

        [Theory (DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void SaksnummerTestOk(Saksnummer saksnummer, KommunensSaksnummerArrayValidator kommunensSaksnummerValidator)
        {      
            var result = kommunensSaksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory (DisplayName = ".55.1 - kommunensSaksnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void KommunensSaksnummerUtfylt(Saksnummer saksnummer, KommunensSaksnummerArrayValidator kommunensSaksnummerValidator)
        {
            saksnummer = null;
            var result = kommunensSaksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".55.1");
        }

        [Theory(DisplayName = ".55.108.1 - kommunensSaksnummer.Saksaar.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void KommunensSaksnummerSaksaarUtfylt(Saksnummer saksnummer, KommunensSaksnummerArrayValidator kommunensSaksnummerValidator)
        {
            saksnummer.Saksaar = null;
            saksnummer.Sakssekvensnummer = 12345;
            var result = kommunensSaksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".55.108.1");
        }

        [Theory(DisplayName = ".55.108.2 - kommunensSaksnummer.Saksaar.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void KommunensSaksnummerSaksaarGyldig(Saksnummer saksnummer, KommunensSaksnummerArrayValidator kommunensSaksnummerValidator)
        {
            saksnummer.Saksaar = -11;
            var result = kommunensSaksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".55.108.2");
        }

        [Theory(DisplayName = ".55.109.1 - kommunensSaksnummer.Saksekvensnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void KommunensSaksnummerSekvensnummerUtfylt(Saksnummer saksnummer, KommunensSaksnummerArrayValidator kommunensSaksnummerValidator)
        {
            saksnummer.Sakssekvensnummer = null;
            saksnummer.Saksaar = 2022;
            var result = kommunensSaksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".55.109.1");
        }

        public static IEnumerable<object[]> SaksnummerTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.KommunensSaksnummerArrayValidator, ParentID = null },              
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            Saksnummer saksnummer = new Saksnummer() { Saksaar = 2022, Sakssekvensnummer = 20221212 };

            var saksnummerValidator = new KommunensSaksnummerArrayValidator(entityValidationTree);

            yield return new object[] { saksnummer, saksnummerValidator };

        }
    }
}
