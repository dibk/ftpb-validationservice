﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Utils;
using System.Collections.Generic;
using System.IO;
using Xunit;
using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class ArbeidsplasserValidatorTests
    {
        private Arbeidsplasser _arbeidsplasser;
        private List<string> _attachemntList;
        private ArbeidsplasserValidator _arbeidsplasserValidator;


        public ArbeidsplasserValidatorTests()
        {

            var xmlData = File.ReadAllText(@"Data\ArbeidstilsynetsSamtykke_v2_dfv45957.xml");
            var form = SerializeUtil.DeserializeFromString<ArbeidstilsynetsSamtykkeV2>(xmlData);
            _arbeidsplasser =form.Arbeidsplasser;
            var arbeidsplasserValidatorNodeList = new List<EntityValidatorNode>()
            {
                new() {NodeId = 17, EnumId = EntityValidatorEnum.ArbeidsplasserValidator, ParentID = null}
            };
            var tree = EntityValidatorTree.BuildTree(arbeidsplasserValidatorNodeList);
            _arbeidsplasserValidator = new ArbeidsplasserValidator(tree);
            _attachemntList = new List<string>() { "BeskrivelseTypeArbeidProsess" };


            var arbeidsplasser = new Arbeidsplasser()
            {
                AntallAnsatte = "3",
                AntallVirksomheter = "2",
                Beskrivelse = "noko rar har",
                Eksisterende = true,
                Faste = true,
                Framtidige = true,
                Midlertidige = false,
                UtleieBygg = null
            };
        }

        [Fact]
        public void ArbeidsplasserTest()
        {
            
        }
    }
}
