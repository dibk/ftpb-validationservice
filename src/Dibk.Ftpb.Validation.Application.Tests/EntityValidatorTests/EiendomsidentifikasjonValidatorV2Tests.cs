﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.IO;
using Xunit;
using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class EiendomsidentifikasjonValidatorV2Tests
    {
        private EiendomsidentifikasjonValidator _validator;
        private Eiendomsidentifikasjon _matrikkel;

        private ICodeListService _codeListService;
        private readonly IList<EntityValidatorNode> _tree;


        public EiendomsidentifikasjonValidatorV2Tests()
        {
            var xmlData = File.ReadAllText(@"Data\ArbeidstilsynetsSamtykke_v2_dfv45957.xml");
            var form = SerializeUtil.DeserializeFromString<ArbeidstilsynetsSamtykkeV2>(xmlData);
            _matrikkel = form.EiendomByggested[0].Eiendomsidentifikasjon;


          var matrikkelNodeList = new List<EntityValidatorNode>()
            {
                new () {NodeId = 1, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = null},
            };

            _tree = EntityValidatorTree.BuildTree(matrikkelNodeList);
            _codeListService = MockDataSource.CodeListService();
            var matrikkelService = MockDataSource.AnyByggWithEiendomsidentifikasjonInMatrikkel(true);

            _validator = new EiendomsidentifikasjonValidator(_tree, _codeListService, matrikkelService);
        }

        [Fact]
        public void testMatrikkel()
        {
            _codeListService = MockDataSource.CodeListService();
            var matrikkelService = MockDataSource.AnyByggWithEiendomsidentifikasjonInMatrikkel(true);

            _validator = new EiendomsidentifikasjonValidator(_tree, _codeListService, matrikkelService);
            var result = _validator.Validate(_matrikkel);
            result.Should().NotBeNull();
        }
    }
}
