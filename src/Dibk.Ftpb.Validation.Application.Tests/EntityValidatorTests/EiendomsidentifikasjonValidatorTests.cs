﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class EiendomsidentifikasjonValidatorTests
    {
        private const string skip = null;

        [Theory(DisplayName = "Eiendomsidentifikasjon xml.DeserializeFromString - OK ", Skip = skip)]
        [Trait("Category", "DeserializeFromString")]
        [MemberData(nameof(EiendomsidentifikasjonFormsTestData))]
        public void Eiendomsidentifikasjon(Eiendomsidentifikasjon eiendomsidentifikasjon, IList<EntityValidatorNode> tree)
        {
            eiendomsidentifikasjon.Festenummer.Should().NotBeEmpty();
            eiendomsidentifikasjon.Seksjonsnummer.Should().NotBeEmpty();
            eiendomsidentifikasjon.Bruksnummer.Should().NotBeEmpty();
            eiendomsidentifikasjon.Gaardsnummer.Should().NotBeEmpty();
            eiendomsidentifikasjon.Kommunenummer.Should().NotBeEmpty();
        }


        [Theory(DisplayName = ".7.14.1 - Eiendomsidentifikasjon.utfylt", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomsidentifikasjonTestData))]
        public void Eiendomsidentifikasjon_utfylt(Eiendomsidentifikasjon eiendomsidentifikasjon, IList<EntityValidatorNode> tree)
        {
            var kommunnenummerCodeList = MockDataSource.CodeListService();
            var matrikkelService = MockDataSource.ImatrikkelServices(false, true, true);

            var eiendomsidentifikasjonValidator = new EiendomsidentifikasjonValidator(tree, kommunnenummerCodeList, matrikkelService);

            eiendomsidentifikasjon = null;
            var result = eiendomsidentifikasjonValidator.Validate(eiendomsidentifikasjon);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.14.1");
        }
        [Theory(DisplayName = ".7.14.10.1 - kommunenummer.utfylt", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomsidentifikasjonTestData))]
        public void kommunenummer_utfylt( Eiendomsidentifikasjon eiendomsidentifikasjon, IList<EntityValidatorNode> tree)
        {
            var kommunnenummerCodeList = MockDataSource.CodeListService(true, true, "utfylt");
            var matrikkelService = MockDataSource.ImatrikkelServices(false, true, true);

            var eiendomByggestedValidator = new EiendomsidentifikasjonValidator(tree, kommunnenummerCodeList, matrikkelService);

            var result = eiendomByggestedValidator.Validate(eiendomsidentifikasjon);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.14.10.1");
        }
        [Theory(DisplayName = ".7.14.10.2 - kommunenummer.gyldig", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomsidentifikasjonTestData))]
        public void kommunenummer_gyldig(Eiendomsidentifikasjon eiendomsidentifikasjon, IList<EntityValidatorNode> tree)
        {
            var kommunnenummerCodeList = MockDataSource.CodeListService(true, true, "ugyldig");
            var matrikkelService = MockDataSource.ImatrikkelServices(false, true, true);

            var eiendomByggestedValidator = new EiendomsidentifikasjonValidator(tree, kommunnenummerCodeList, matrikkelService);

            var result = eiendomByggestedValidator.Validate(eiendomsidentifikasjon);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.14.10.2");
        }
        [Theory(DisplayName = ".7.14.10.3 - kommunenummer.validert", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(EiendomsidentifikasjonTestData))]
        public void kommunenummer_validert(Eiendomsidentifikasjon eiendomsidentifikasjon, IList<EntityValidatorNode> tree)
        {
            var kommunnenummerCodeList = MockDataSource.CodeListService(true, true, "ikkevalidert");
            var matrikkelService = MockDataSource.ImatrikkelServices(false, true, true);

            var eiendomByggestedValidator = new EiendomsidentifikasjonValidator(tree, kommunnenummerCodeList, matrikkelService);

            var result = eiendomByggestedValidator.Validate(eiendomsidentifikasjon);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault()?.Reference.Should().Be(".7.14.10.3");
        }
        // TODO ... to be continue ...

       // MemberData for unit test
        public static IEnumerable<object[]> EiendomsidentifikasjonTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 1 },
                new() { NodeId = 3, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 1 },
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            var eiendom = EiendomByggestedTestData.GetEiendom();

            yield return new object[] { eiendom.Eiendomsidentifikasjon, entityValidationTree };
        }
        public static IEnumerable<object[]> EiendomsidentifikasjonFormsTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.EiendomByggestedValidator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.EiendomsAdresseValidator, ParentID = 1 },
                new() { NodeId = 3, EnumId = EntityValidatorEnum.EiendomsidentifikasjonValidator, ParentID = 1 },
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            var avfallsplanEiendom = EiendomByggestedTestData.GetAvfallsplanEiendom();
            var arbeidstilsynetsSamtykkeEiendom = EiendomByggestedTestData.GetArbeidstilsynetsSamtykkeEiendom();

            yield return new object[] { avfallsplanEiendom?.Eiendomsidentifikasjon, entityValidationTree };
            yield return new object[] { arbeidstilsynetsSamtykkeEiendom?.Eiendomsidentifikasjon, entityValidationTree };
        }

    }
}
