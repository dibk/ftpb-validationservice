﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.IO;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Xunit;
using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests.AktoerValidtorTests
{
    public class ForetakValidatorTests
    {
        private ICodeListService _codeListService;
        private IPostalCodeService _postalCodeService;
        private IDecryptionFactory _decryptionFactory;

        //**foretak
        private ForetakValidator _foretakValidator;
        private IKontaktpersonValidator _foretakKontaktpersonValidator;
        private IKodelisteValidator _foretakPartstypeValidator;
        private IEnkelAdresseValidator _foretakEnkelAdresseValidator;

        private AnsakoAnsvarsrett _form;

        public ForetakValidatorTests()
        {
            var xmlData = File.ReadAllText(@"Data\Ansako\ErklaeringAnsvarsrett_1.xml");
            _form = SerializeUtil.DeserializeFromString<AnsakoAnsvarsrett>(xmlData);

            _codeListService = MockDataSource.CodeListService( true);
            _postalCodeService = MockDataSource.ValidatePostnr(true, "Bø i Telemark", "true");
            _decryptionFactory = MockDataSource.DecryptText("09107300292");

            var foretakNodeList = new List<EntityValidatorNode>()
            {
                //foretak
                new () {NodeId = 01, EnumId = EntityValidatorEnum.ForetakValidator, ParentID = null},
                new () {NodeId = 02, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 01},
                new () {NodeId = 03, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 01},
                new () {NodeId = 04, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 01},
            };
            var tree = EntityValidatorTree.BuildTree(foretakNodeList);

            //*Ansvarsrett
            //**foretak
            _foretakPartstypeValidator = MockDataSource.KodelisteValidator(null, null, "/foretak/partstype/kodeverdi");
            _foretakEnkelAdresseValidator = MockDataSource.enkelAdresseValidator();
            _foretakKontaktpersonValidator = MockDataSource.KontaktpersonValidator();

            //_foretakPartstypeValidator = new PartstypeValidator(tree, 02, _codeListService);
            //_foretakEnkelAdresseValidator = new EnkelAdresseValidator(tree, 03, _postalCodeService);
            //_foretakKontaktpersonValidator = new KontaktpersonValidatorV2(tree, 04);
            var foretakPartstypes = new[] { "Foretak" };
            _foretakValidator = new ForetakValidator(tree, _foretakEnkelAdresseValidator, _foretakKontaktpersonValidator, _foretakPartstypeValidator, _codeListService, _decryptionFactory, foretakPartstypes);

        }

        [Fact]
        public void ForetakTest()
        {
            var result = _foretakValidator.Validate(_form.Ansvarsretts.Foretak);
            var errorMessages = result?.ValidationMessages;
            errorMessages.Should().BeNullOrEmpty();
        }
        [Fact]
        public void Foretak_Organisasjonsnummer_Utfylt()
        {
            //_form.Ansvarsretts.Foretak.Organisasjonsnummer  = "910297937" //Gyldig
            _form.Ansvarsretts.Foretak.Organisasjonsnummer = null;
            _foretakValidator.ValidateDataRelations(_form.Ansvarsretts.Foretak);
            var errorMessages = _foretakValidator?.ValidationResult?.ValidationMessages;
            errorMessages.Should().NotBeNullOrEmpty();
        }
        [Fact]
        public void Foretak_Organisasjonsnummer_kontrollsiffer()
        {
            _form.Ansvarsretts.Foretak.Organisasjonsnummer = "123456789321654";
            _foretakValidator.ValidateDataRelations(_form.Ansvarsretts.Foretak);
            var errorMessages = _foretakValidator?.ValidationResult?.ValidationMessages;
            errorMessages.Should().NotBeNullOrEmpty();
        }
        [Fact]
        public void Foretak_Organisasjonsnummer_gyldig()
        {
            _form.Ansvarsretts.Foretak.Organisasjonsnummer = "123456789";
            _foretakValidator.ValidateDataRelations(_form.Ansvarsretts.Foretak);
            var errorMessages = _foretakValidator?.ValidationResult?.ValidationMessages;
            errorMessages.Should().NotBeNullOrEmpty();
        }
        [Fact]
        public void Foretak_Navn_Utfylt()
        {
            _form.Ansvarsretts.Foretak.Navn = null;
            _foretakValidator.ValidateDataRelations(_form.Ansvarsretts.Foretak);
            var errorMessages = _foretakValidator?.ValidationResult?.ValidationMessages;
            errorMessages.Should().NotBeNullOrEmpty();
        }
        [Fact]
        public void Foretak_Epost_Utfylt()
        {
            _form.Ansvarsretts.Foretak.Epost = null;
            var result =_foretakValidator.ValidateDataRelations(_form.Ansvarsretts.Foretak);
            var errorMessages = result.ValidationMessages;
            errorMessages.Should().NotBeNullOrEmpty();
        }
        [Fact]
        public void Foretak_telmob_utfylt_Utfylt()
        {
            _form.Ansvarsretts.Foretak.Mobilnummer = null;
            _form.Ansvarsretts.Foretak.Telefonnummer = null;
            _foretakValidator.ValidateDataRelations(_form.Ansvarsretts.Foretak);
            var errorMessages = _foretakValidator?.ValidationResult?.ValidationMessages;
            errorMessages.Should().NotBeNullOrEmpty();
        }
        [Fact]
        public void Foretak_telefonnummer_gyldig()
        {
            _form.Ansvarsretts.Foretak.Telefonnummer = "-4445sdf";
            _foretakValidator.ValidateDataRelations(_form.Ansvarsretts.Foretak);
            var errorMessages = _foretakValidator?.ValidationResult?.ValidationMessages;
            errorMessages.Should().NotBeNullOrEmpty();
        }
        [Fact]
        public void Foretak_mobilnummer_gyldig()
        {
            _form.Ansvarsretts.Foretak.Mobilnummer = "-4445sdf";
            _foretakValidator.ValidateDataRelations(_form.Ansvarsretts.Foretak);
            var errorMessages = _foretakValidator?.ValidationResult?.ValidationMessages;
            errorMessages.Should().NotBeNullOrEmpty();
        }

    }
}
