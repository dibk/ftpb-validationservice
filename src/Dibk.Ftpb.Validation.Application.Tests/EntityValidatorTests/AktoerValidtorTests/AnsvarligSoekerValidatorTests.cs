using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests.AktoerValidtorTests
{
    public class AnsvarligSoekerValidatorTests
    {       
        [Theory(DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void AnsvarligsoekerTestOk(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {        
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory(DisplayName = "2.1 - ansvarligsoeker.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void AnsvarligsoekerUtfylt(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {
            aktoer = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".2.1");
        }

        [Theory(DisplayName = "2.18.1 - foedselsnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void FoedselsnummerUtfylt(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {           
            aktoer.Foedselsnummer = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".2.18.1");
        }

        [Theory(DisplayName = "2.18.2 - foedselsnummer.kontrollsiffer")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void FoedselsnummerGyldig(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {
            aktoer.Foedselsnummer = "11111111111";
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".2.18.17");
        }

        [Theory(DisplayName = "2.18.17 - foedselsnummer.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void FoedselsnummerKontrollsiffer(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {
            aktoer.Foedselsnummer = "12345";
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".2.18.2");
        }

        [Theory(DisplayName = "2.19.1 - organisasjonsnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void OrganisasjonsnummerUtfylt(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {
            aktoer.Partstype.Kodeverdi = "Organisasjon";
            aktoer.Organisasjonsnummer = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".2.19.1");
        }

        [Theory(DisplayName = "2.19.2 - organisasjonsnummer.kontrollsiffer")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void OrganisasjonsnummerKontrollsiffer(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {
            aktoer.Partstype.Kodeverdi = "Organisasjon";
            aktoer.Organisasjonsnummer = "999888777";
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".2.19.17");
        }

        [Theory(DisplayName = "2.19.17 - organisasjonsnummer.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void OrganisasjonsnummerGyldig(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {
            aktoer.Partstype.Kodeverdi = "Organisasjon";
            aktoer.Organisasjonsnummer = "12345";
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".2.19.2");
        }

        [Theory(DisplayName = "2.20.1 - navn.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void NavnUtfylt(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {
            aktoer.Navn = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".2.20.1");
        }

        [Theory(DisplayName = "2.21.1 - telefonnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void TelefonnummerUtfylt(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {
            aktoer.Telefonnummer = null;
            aktoer.Mobilnummer = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".2.21.1");
        }

        [Theory(DisplayName = "2.21.2 - telefonnummer.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void TelefonnummerGyldig(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {
            aktoer.Telefonnummer = "-4711223366";
            aktoer.Mobilnummer = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".2.21.2");
        }

        [Theory(DisplayName = "2.22.2 - mobilnummer.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void MobilnummerGyldig(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {
            aktoer.Telefonnummer = null;
            aktoer.Mobilnummer = "-4711223366";
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".2.22.2");
        }

        [Theory(DisplayName = "2.23.1 - epost.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AnsvarligsoekerTestData))]
        public void EpostUtfylt(Aktoer aktoer, AnsvarligSoekerValidator aktoerValidator)
        {
            aktoer.Epost = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".2.23.1");
        }

        public static IEnumerable<object[]> AnsvarligsoekerTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new () {NodeId = 1, EnumId = EntityValidatorEnum.AnsvarligSoekerValidator, ParentID = null},
                new () {NodeId = 2, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 1},
                new () {NodeId = 3, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 1},
                new () {NodeId = 4, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 1}
            };
            var tree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            Aktoer aktoer = new Aktoer()
            {
                Partstype = new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste() { Kodeverdi = "Privatperson", Kodebeskrivelse = "Privatperson" },
                Foedselsnummer = "28044102120",
                Organisasjonsnummer = "894689382",
                Navn = "Oslo kommune",
                Adresse = new Dibk.Ftpb.Common.Datamodels.Parts.EnkelAdresse() { Adresselinje1 = "Ola Narr 2", Landkode = "NO", Postnr = "3674", Poststed = "NOTODDEN" },
                Telefonnummer = "44558899",
                Mobilnummer = "44556633",
                Epost = "noko@noko.no",
                Kontaktperson = new Kontaktperson() { Epost = "nokoana@noko.no", Mobilnummer = "55887799", Telefonnummer = "33669988", Navn = "Filip Mohammed" },
            };

            IPostalCodeService postalCodeService = MockDataSource.ValidatePostnr(true, "NOTODDEN", "true");
            ICodeListService codeListService = MockDataSource.CodeListService();
            IDecryptionFactory decryptionFactory = MockDataSource.DecryptText("09107300292");

            IKontaktpersonValidator ansvarligsoekerKontaktpersonValidator = new KontaktpersonValidator(tree, 2);
            IKodelisteValidator ansvarligsoekerPartstypeValidator = new PartstypeValidator(tree, 3, codeListService);
            var ansvarligsoekerEnkelAdresseValidator = new EnkelAdresseValidator(tree, 4, postalCodeService);
            IAktoerValidator aktoerValidator = new AnsvarligSoekerValidator(tree, ansvarligsoekerEnkelAdresseValidator, ansvarligsoekerKontaktpersonValidator, ansvarligsoekerPartstypeValidator, codeListService, decryptionFactory);

            yield return new object[] { aktoer, aktoerValidator };

        }
    }
}
