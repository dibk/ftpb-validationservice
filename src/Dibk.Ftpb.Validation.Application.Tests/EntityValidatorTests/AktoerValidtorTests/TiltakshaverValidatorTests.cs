using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Validation.Application.Logic.Interfaces;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.AktoerValidators;
using Xunit;
using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests.AktoerValidtorTests
{
    public class TiltakshaverValidatorTests
    {       
        [Theory(DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void TiltakshaverTestOk(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {        
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory(DisplayName = "20.1 - tiltakshaver.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void TiltakshaverUtfylt(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {
            aktoer = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".20.1");
        }

        [Theory(DisplayName = "20.18.1 - foedselsnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void FoedselsnummerUtfylt(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {           
            aktoer.Foedselsnummer = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".20.18.1");
        }

        [Theory(DisplayName = "20.18.2 - foedselsnummer.kontrollsiffer")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void FoedselsnummerKontrollsiffer(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {
            aktoer.Foedselsnummer = "11111111111";
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".20.18.17");
        }

        [Theory(DisplayName = "20.18.17 - foedselsnummer.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void FoedselsnummerGyldig(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {
            aktoer.Foedselsnummer = "12345";
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".20.18.2");
        }

        [Theory(DisplayName = "20.19.1 - organisasjonsnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void OrganisasjonsnummerUtfylt(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {
            aktoer.Partstype.Kodeverdi = "Organisasjon";
            aktoer.Organisasjonsnummer = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".20.19.1");
        }

        [Theory(DisplayName = "20.19.2 - organisasjonsnummer.kontrollsiffer")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void OrganisasjonsnummerKontrollsiffer(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {
            aktoer.Partstype.Kodeverdi = "Organisasjon";
            aktoer.Organisasjonsnummer = "999888777";
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".20.19.17");
        }

        [Theory(DisplayName = "20.19.17 - organisasjonsnummer.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void OrganisasjonsnummerGyldig(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {
            aktoer.Partstype.Kodeverdi = "Organisasjon";
            aktoer.Organisasjonsnummer = "12345";
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".20.19.2");
        }

        [Theory(DisplayName = "20.20.1 - navn.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void NavnUtfylt(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {
            aktoer.Navn = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".20.20.1");
        }

        [Theory(DisplayName = "20.21.1 - telefonnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void TelefonnummerUtfylt(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {
            aktoer.Telefonnummer = null;
            aktoer.Mobilnummer = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".20.21.1");
        }

        [Theory(DisplayName = "20.21.2 - telefonnummer.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void TelefonnummerGyldig(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {
            aktoer.Telefonnummer = "-4711223366";
            aktoer.Mobilnummer = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".20.21.2");
        }

        [Theory(DisplayName = "20.22.2 - mobilnummer.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void MobilnummerGyldig(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {
            aktoer.Telefonnummer = null;
            aktoer.Mobilnummer = "-4711223366";
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".20.22.2");
        }

        [Theory(DisplayName = "20.23.1 - epost.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(TiltakshaverTestData))]
        public void EpostUtfylt(Aktoer aktoer, TiltakshaverValidator aktoerValidator)
        {
            aktoer.Epost = null;
            var result = aktoerValidator.Validate(aktoer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".20.23.1");
        }

        public static IEnumerable<object[]> TiltakshaverTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new () {NodeId = 1, EnumId = EntityValidatorEnum.TiltakshaverValidator, ParentID = null},
                new () {NodeId = 2, EnumId = EntityValidatorEnum.KontaktpersonValidator, ParentID = 1},
                new () {NodeId = 3, EnumId = EntityValidatorEnum.PartstypeValidator, ParentID = 1},
                new () {NodeId = 4, EnumId = EntityValidatorEnum.EnkelAdresseValidator, ParentID = 1}
            };
            var tree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            Aktoer aktoer = new Aktoer()
            {
                Partstype = new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste() { Kodeverdi = "Privatperson", Kodebeskrivelse = "Privatperson" },
                Foedselsnummer = "28044102120",
                Organisasjonsnummer = "894689382",
                Navn = "Oslo kommune",
                Adresse = new Dibk.Ftpb.Common.Datamodels.Parts.EnkelAdresse() { Adresselinje1 = "Ola Narr 2", Landkode = "NO", Postnr = "3674", Poststed = "NOTODDEN" },
                Telefonnummer = "44558899",
                Mobilnummer = "44556633",
                Epost = "noko@noko.no",
                Kontaktperson = new Kontaktperson() { Epost = "nokoana@noko.no", Mobilnummer = "55887799", Telefonnummer = "33669988", Navn = "Filip Mohammed" },
            };

            IPostalCodeService postalCodeService = MockDataSource.ValidatePostnr(true, "NOTODDEN", "true");
            ICodeListService codeListService = MockDataSource.CodeListService();
            IDecryptionFactory decryptionFactory = MockDataSource.DecryptText("09107300292");

            var xmlData = File.ReadAllText(@"Data\ArbeidstilsynetsSamtykke_dfv41999.xml");
            var nn = TestHelper.GetJsonForPostman(xmlData);

            ArbeidstilsynetsSamtykkeV2 form = SerializeUtil.DeserializeFromString<ArbeidstilsynetsSamtykkeV2>(xmlData);

            //Tiltakshaver
            IKontaktpersonValidator tiltakshaverKontaktpersonValidator = new KontaktpersonValidator(tree, 2);
            IKodelisteValidator tiltakshaverPartstypeValidator = new PartstypeValidator(tree, 3, codeListService);
            var tiltakshaverEnkelAdresseValidator = new EnkelAdresseValidator(tree, 4, postalCodeService);
            IAktoerValidator aktoerValidator = new TiltakshaverValidator(tree, tiltakshaverEnkelAdresseValidator, tiltakshaverKontaktpersonValidator, tiltakshaverPartstypeValidator, codeListService, decryptionFactory);

            yield return new object[] { aktoer, aktoerValidator };

        }
    }
}
