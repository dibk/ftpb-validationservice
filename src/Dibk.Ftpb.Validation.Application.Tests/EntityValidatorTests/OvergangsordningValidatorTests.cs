﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using FluentAssertions;
using System;
using System.Collections.Generic;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class OvergangsordningValidatorTests
    {
        private const string skip = null;


        [Theory(DisplayName = ".68.165.1 - overgangsbestemmelse.utfylt", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(overgangsordningTestData))]
        public void OvergangsbestemmelseUtfylt(IList<EntityValidatorNode> tree, Overgangsordning overgangsordning)
        {
            var validator =new OvergangsordningValidator(tree);
            overgangsordning.DatoForHovedsoeknad = new DateTime(2023, 6, 30);
            overgangsordning.Overgangsbestemmelse = null;
            var result = validator.Validate(overgangsordning);
            var messages = result.ValidationMessages;
            messages.Count.Should().Be(1);
        }

        [Theory(DisplayName = ".68.166.1 - datoForHovedsoeknad.utfylt", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(overgangsordningTestData))]
        public void DatoForHovedsoeknadUtfylt(IList<EntityValidatorNode> tree, Overgangsordning overgangsordning)
        {
            var validator = new OvergangsordningValidator(tree);
            overgangsordning.DatoForHovedsoeknad = null;
            overgangsordning.Overgangsbestemmelse = true;
            var result = validator.Validate(overgangsordning);
            var messages = result.ValidationMessages;
            messages.Count.Should().Be(1);
        }

        public static IEnumerable<object[]> overgangsordningTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.OvergangsordningValidator, ParentID = null },
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            Overgangsordning overgangsordning = new Overgangsordning()
            {
                Overgangsbestemmelse = true,
                DatoForHovedsoeknad = DateTime.Now
            };

             yield return new object[] { entityValidationTree, overgangsordning };
        }
    }
}
