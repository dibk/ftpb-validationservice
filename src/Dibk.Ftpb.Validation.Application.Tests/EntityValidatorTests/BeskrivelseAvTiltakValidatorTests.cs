﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.AtilFee;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class BeskrivelseAvTiltakValidatorTests
    {    

        [Theory(DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(BeskrivelseAvTiltakForAtilV2TestData))]
        public void BeskrivelseAvTiltakForAtilV2TestOk(BeskrivelseAvTiltakForAtilV2Validator beskrivelseAvTiltakForAtilV2Validator, BeskrivelseAvTiltak beskrivelseAvTiltak)
        {
            var result = beskrivelseAvTiltakForAtilV2Validator.Validate(new List<ValidationRule>(), beskrivelseAvTiltak);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory(DisplayName = ".40.1 - beskrivelseAvTiltak.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(BeskrivelseAvTiltakForAtilV2TestData))]
        public void BeskrivelseAvTiltakForAtilV2Utfylt(BeskrivelseAvTiltakForAtilV2Validator beskrivelseAvTiltakForAtilV2Validator, BeskrivelseAvTiltak beskrivelseAvTiltak)
        {
            beskrivelseAvTiltak = null;
            var result = beskrivelseAvTiltakForAtilV2Validator.Validate(new List<ValidationRule>(), beskrivelseAvTiltak);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".40.1");
        }

        [Theory(DisplayName = ".40.107.1 - BRA.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(BeskrivelseAvTiltakForAtilV2TestData))]
        public void BRAUtfylt(BeskrivelseAvTiltakForAtilV2Validator beskrivelseAvTiltakForAtilV2Validator, BeskrivelseAvTiltak beskrivelseAvTiltak)
        {
            beskrivelseAvTiltak.BRA = null;
            var result = beskrivelseAvTiltakForAtilV2Validator.Validate(new List<ValidationRule>(), beskrivelseAvTiltak);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".40.107.1");
        }

        [Theory(DisplayName = ".40.107.2 - BRA.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(BeskrivelseAvTiltakForAtilV2TestData))]
        public void BRAGyldig(BeskrivelseAvTiltakForAtilV2Validator beskrivelseAvTiltakForAtilV2Validator, BeskrivelseAvTiltak beskrivelseAvTiltak)
        {
            beskrivelseAvTiltak.BRA = "fff";
            var result = beskrivelseAvTiltakForAtilV2Validator.Validate(new List<ValidationRule>(), beskrivelseAvTiltak);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".40.107.2");
        }


        public static IEnumerable<object[]> BeskrivelseAvTiltakForAtilV2TestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.BeskrivelseAvTiltakForAtilV2Validator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.TiltakstypeAtilValidator, ParentID = 1 },
                new() { NodeId = 3, EnumId = EntityValidatorEnum.FormaaltypeValidator, ParentID = 1 },
            
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            BeskrivelseAvTiltak beskrivelseAvTiltak = new BeskrivelseAvTiltak()
            {
                BRA = "123",
                Formaaltype = new Formaaltype() { Bygningstype = new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste[] { new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste() { Kodeverdi = "noko", Kodebeskrivelse = "nokorart" } } },
                Tiltakstype = new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste[] { new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste() { Kodeverdi = "noko", Kodebeskrivelse = "nokoana" } },
                Foelgebrev = null,
            };

            var tilakstypeValidator = MockDataSource.KodelisteValidator();
            var bygningstypeValidator = MockDataSource.KodelisteValidator();
            var formaaltypeValidator = new FormaaltypeValidator(entityValidationTree, bygningstypeValidator);
            IAtilFeeCalculatorService atilFeeCalculatorService = MockDataSource.GetAtilTiltakstyperAsync();

            var beskrivelseAvTiltakForAtilV2Validator = new BeskrivelseAvTiltakForAtilV2Validator(entityValidationTree, formaaltypeValidator, tilakstypeValidator, atilFeeCalculatorService);

            yield return new object[] { beskrivelseAvTiltakForAtilV2Validator, beskrivelseAvTiltak };
        }
    }
}
