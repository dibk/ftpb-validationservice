﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using Dibk.Ftpb.Common.Datamodels.Parts.Bygg;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class MangelValidatorTests
    {
        private const string skip = "test";

        [Theory (DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelTestData))]
        public void MangelTestOk(IList<EntityValidatorNode> tree, Mangel mangel)
        {
            var mangeltekstValidator = MockDataSource.KodelisteValidator();
            var mangelValidator = new MangelValidator(tree, 1, mangeltekstValidator);
            var result = mangelValidator.Validate(mangel);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory(DisplayName = ".63.1 - mangel.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelTestData))]
        public void MangelUtfylt(IList<EntityValidatorNode> tree, Mangel mangel)
        {
            var mangeltekstValidator = MockDataSource.KodelisteValidator();
            var mangelValidator = new MangelValidator(tree, 1, mangeltekstValidator);

            mangel = null;
            var result = mangelValidator.Validate(mangel);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".63.1");
        }

        [Theory (DisplayName = ".63.159.1 - mangelId.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelTestData))]
        public void MangelIdUtfylt(IList<EntityValidatorNode> tree, Mangel mangel)
        {
            var mangeltekstValidator = MockDataSource.KodelisteValidator();
            var mangelValidator = new MangelValidator(tree, 1, mangeltekstValidator);

            mangel.MangelId = null;
            var result = mangelValidator.Validate(mangel);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".63.159.1");
        }
        
        [Theory (DisplayName = ".63.64.1 - mangeltekst.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelTestData))]
        public void MangeltekstUtfylt(IList<EntityValidatorNode> tree, Mangel mangel)
        {
            var codeListService = MockDataSource.CodeListService();
            var mangeltekstValidator = new MangeltekstValidator(tree, 2, codeListService);

            mangel.Mangeltekst = null;
            var result = mangeltekstValidator.Validate(mangel.Mangeltekst);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".63.64.1");
        }

        [Theory(DisplayName = ".63.64.38.1 - mangeltekst.kodeverdi.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelTestData))]
        public void MangeltekstKodeverdiUtfylt(IList<EntityValidatorNode> tree, Mangel mangel)
        {
            var codeListService = MockDataSource.CodeListService();
            var mangeltekstValidator = new MangeltekstValidator(tree, 2, codeListService);
                        
            mangel.Mangeltekst.Kodeverdi = null;
            var result = mangeltekstValidator.Validate(mangel.Mangeltekst);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".63.64.38.1");
        }

        [Theory(DisplayName = ".63.64.38.2 - mangeltekst.kodeverdi.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelTestData))]
        public void MangeltekstKodeverdiGyldig(IList<EntityValidatorNode> tree, Mangel mangel)
        {
            var codeListService = MockDataSource.CodeListService(false);
            var mangeltekstValidator = new MangeltekstValidator(tree, 2, codeListService);

            var result = mangeltekstValidator.Validate(mangel.Mangeltekst);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".63.64.38.2");
        }

        [Theory(DisplayName = ".63.64.39.1 - mangeltekst.kodebeskrivelse.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelTestData))]
        public void MangeltekstKodebeskrivelseUtfylt(IList<EntityValidatorNode> tree, Mangel mangel)
        {
            var codeListService = MockDataSource.CodeListService();
            var mangeltekstValidator = new MangeltekstValidator(tree, 2, codeListService);
            
            mangel.Mangeltekst.Kodebeskrivelse = null;
            var result = mangeltekstValidator.Validate(mangel.Mangeltekst);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".63.64.39.1");
        }

        [Theory(DisplayName = ".63.64.39.2 - mangeltekst.kodebeskrivelse.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(MangelTestData))]
        public void MangeltekstKodebeskrivelseGyldig(IList<EntityValidatorNode> tree, Mangel mangel)
        {
            var codeListService = MockDataSource.CodeListService(true, false);
            var mangeltekstValidator = new MangeltekstValidator(tree, 2, codeListService);
            
            var result = mangeltekstValidator.Validate(mangel.Mangeltekst);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".63.64.39.2");
        }

        public static IEnumerable<object[]> MangelTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.MangelValidator, ParentID = null },
                new() { NodeId = 2, EnumId = EntityValidatorEnum.MangeltekstValidator, ParentID = 1 },
               
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            Mangel mangel = new Mangel() { MangelId = "nokoId", Mangeltekst = new Dibk.Ftpb.Common.Datamodels.Parts.Kodeliste { Kodeverdi = "kodeverdi", Kodebeskrivelse = "kodebeskrivelse"} };           

            yield return new object[] { entityValidationTree, mangel };

        }
    }
}
