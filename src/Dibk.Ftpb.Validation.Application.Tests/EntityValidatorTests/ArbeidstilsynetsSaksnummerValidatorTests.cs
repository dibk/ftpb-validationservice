﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators.Common;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Common.Datamodels.Parts;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.EntityValidatorTests
{
    public class ArbeidstilsynetsSaksnummerValidatorTests
    {
        private const string skip = "test";

        [Theory (DisplayName = "test OK")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void ArbeidstilsynetsSaksnummerTestOk(Saksnummer saksnummer, ArbeidstilsynetsSaksnummerValidator saksnummerValidator)
        {      
            var result = saksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(0);
        }

        [Theory (DisplayName = ".24.1 - arbeidstilsynetsSaksnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void ArbeidstilsynetsSaksnummerUtfylt(Saksnummer saksnummer, ArbeidstilsynetsSaksnummerValidator saksnummerValidator)
        {
            saksnummer = null;
            var result = saksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".24.1");
        }

        [Theory(DisplayName = ".24.108.1 - arbeidstilsynetsSaksnummer.Saksaar.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void ArbeidstilsynetsSaksnummerSaksaarUtfylt(Saksnummer saksnummer, ArbeidstilsynetsSaksnummerValidator saksnummerValidator)
        {
            saksnummer.Saksaar = null;
            saksnummer.Sakssekvensnummer = 12345;
            var result = saksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".24.108.1");
        }

        [Theory(DisplayName = ".24.108.2 - arbeidstilsynetsSaksnummer.Saksaar.gyldig")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void ArbeidstilsynetsSaksnummerSaksaarGyldig(Saksnummer saksnummer, ArbeidstilsynetsSaksnummerValidator saksnummerValidator)
        {
            saksnummer.Saksaar = -11;
            var result = saksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".24.108.2");
        }

        [Theory(DisplayName = ".24.109.1 - arbeidstilsynetsSaksnummer.Saksekvensnummer.utfylt")]
        [Trait("Category", "Validator")]
        [MemberData(nameof(SaksnummerTestData))]
        public void ArbeidstilsynetsSaksnummerSekvensnummerUtfylt(Saksnummer saksnummer, ArbeidstilsynetsSaksnummerValidator saksnummerValidator)
        {
            saksnummer.Sakssekvensnummer = null;
            saksnummer.Saksaar = 2022;
            var result = saksnummerValidator.Validate(saksnummer);
            result.ValidationMessages.Count.Should().Be(1);
            result.ValidationMessages.FirstOrDefault().Reference.Should().Be(".24.109.1");
        }

        public static IEnumerable<object[]> SaksnummerTestData()
        {
            List<EntityValidatorNode> entityValidatorNodes = new()
            {
                new() { NodeId = 1, EnumId = EntityValidatorEnum.ArbeidstilsynetsSaksnummerValidator, ParentID = null },              
            };
            var entityValidationTree = EntityValidatorTree.BuildTree(entityValidatorNodes);

            Saksnummer saksnummer = new Saksnummer() { Saksaar = 2022, Sakssekvensnummer = 20221212 };

            var saksnummerValidator = new ArbeidstilsynetsSaksnummerValidator(entityValidationTree);

            yield return new object[] { saksnummer, saksnummerValidator };

        }
    }
}
