﻿using Dibk.Ftpb.Validation.Application.Utils;
using FluentAssertions;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests
{
    public class DebugingTests
    {
        [Fact]
        public void GetValidationEnumXpath()
        {

            var reference = "45957.4.12.15.2";
            var reference1 = "10000.1.35.21.2";
            var reference2 = "10000.1.27.29.126.1";
            var reference3 = "6821.45957.11.32.1";
            var validatorEnums2 = Debugging.DebugValidatorFormReference(reference2);
            var validatorEnums3 = Debugging.DebugValidatorFormReference(reference3);

            var validatorEnums = Debugging.DebugValidatorFormReference(reference);
            var validatorEnums1 = Debugging.DebugValidatorFormReference(reference1);

            validatorEnums.Should().NotBeNullOrEmpty();
        }
    }
}
