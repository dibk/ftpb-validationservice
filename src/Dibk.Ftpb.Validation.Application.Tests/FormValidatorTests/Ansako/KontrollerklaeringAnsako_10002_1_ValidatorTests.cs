﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators.Ansako;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.FormValidatorTests.Ansako
{
    public class KontrollerklaeringAnsako_10002_1_ValidatorTests
    {
        private KontrollerklaeringAnsako_10002_1_Validator _formValidator;
        private readonly IValidationMessageComposer _validationMessageComposer;
        private readonly IChecklistService _checklistService;
        private IDecryptionFactory _decryptionFactory;

        private ICodeListService _codeListService;
        private IPostalCodeService _postalCodeService;
        private readonly ValidationInput _validationInput;
        private const string skip = "test";

        public KontrollerklaeringAnsako_10002_1_ValidatorTests()
        {
            _validationMessageComposer = MockDataSource.GetValidationMessageComposer(); 

            _checklistService = MockDataSource.GetCheckpoints("RS");

            _codeListService = MockDataSource.CodeListService( true);
            _postalCodeService = MockDataSource.ValidatePostnr(true, "Bø i Telemark", "true");
            var matrikkelService = MockDataSource.AnyByggWithEiendomsidentifikasjonInMatrikkel(true);
            _decryptionFactory = MockDataSource.DecryptText("09107300292");

            _formValidator = new KontrollerklaeringAnsako_10002_1_Validator(_validationMessageComposer, _codeListService, _postalCodeService, _checklistService,matrikkelService, _decryptionFactory);

            var xmlData = File.ReadAllText(@"Data\Ansako\Kontrollerklaering_1.xml");
            _validationInput = new()
            {
                FormData = xmlData
            };
        }
        [Fact(Skip = skip)]

        public void ErklaeringAnsvarsrett_IntegrationTest()
        {
            var postmanJson = TestHelper.GetJsonForPostman(_validationInput.FormData);

            var report = _formValidator.StartValidation(_validationInput);


            //Get Text not found
            var textNotFound = report.ValidationRules.Where(r => r.Message.Contains("Could not find")).ToList();
            var resultRulesText = TestHelper.DebugValidatorFormReference(textNotFound);
            var rulesWithoutTextJson = JObject.FromObject(resultRulesText);

            var validatorRules = report.ValidationRules;
            var validatorRulesJson = JArray.FromObject(validatorRules);

            var rulesForDocument = RuleDocumentationComposer.GetRuleDocumentationModel(validatorRules);
            var noko = rulesForDocument.OrderBy(x => x.RuleId).ThenByDescending(y => y.Xpath).ToArray();
            var ruleJson1 = JArray.FromObject(noko);

            var validatorMessages = report.ValidationMessages;
            var validatorMessagesJson = JArray.FromObject(validatorMessages.Where(m => m.XpathField.Contains("ansvarligSoeker")));

            report.Should().NotBeNull();
        }
    }
}
