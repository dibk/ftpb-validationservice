using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators.Ansako;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using MatrikkelWSClient.Services;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Linq;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.FormValidatorTests.Ansako
{
    public class AnsvarsrettAnsako_10000_ValidatorTests
    {
        private AnsvarsrettAnsako_10000_Validator _formValidator;
        private readonly IValidationMessageComposer _validationMessageComposer;
        private readonly IChecklistService _checklistService;
        private IDecryptionFactory _decryptionFactory;

        private ICodeListService _codeListService;
        private IPostalCodeService _postalCodeService;
        private readonly IMatrikkelProvider _matrikkelProvider;
        private const string skip = "test";


        public AnsvarsrettAnsako_10000_ValidatorTests()
        {
            _validationMessageComposer = MockDataSource.GetValidationMessageComposer();

            _checklistService = MockDataSource.GetCheckpoints("AT");
            _decryptionFactory = MockDataSource.DecryptText("09107300292");

            //_codeListService = MockDataSource.CodeListService(FtbKodeListeEnum.Partstype, true);
            _codeListService = MockDataSource.CodeListService(true, true,"gyldig");
            _postalCodeService = MockDataSource.ValidatePostnr(true, "Bø i Telemark", "true");

            _matrikkelProvider = MockDataSource.AnyByggWithEiendomsidentifikasjonInMatrikkel(true);

            _formValidator = new AnsvarsrettAnsako_10000_Validator(_validationMessageComposer, _codeListService, _postalCodeService, _checklistService, _matrikkelProvider,_decryptionFactory);
        }
        [Fact(Skip = skip)]
        public void ErklaeringAnsvarsrett_IntegrationTest()
        {
            var xmlData = File.ReadAllText(@"Data\Ansako\ErklaeringAnsvarsrett_1.xml");
            ValidationInput validationInput = new();
            validationInput.FormData = xmlData;
            var newValidationReport = _formValidator.StartValidation(validationInput);
            newValidationReport.Should().NotBeNull();
        }
        [Fact(Skip = skip)]
        public void ErklaeringAnsvarsrett_RuleTest()
        {
            var xmlData = File.ReadAllText(@"Data\Ansako\ErklaeringAnsvarsrett_1.xml");
            var jsonPostman = TestHelper.GetJsonForPostman(xmlData);

            ValidationInput validationInput = new();
            validationInput.FormData = xmlData;
            var nn = _formValidator.FormValidationRules;
            var report = _formValidator.StartValidation(validationInput);

            //Get Text not found
            var textNotFound = report.ValidationRules.Where(r => r.Message.Contains("Could not find")).ToList();
            var resultRulesText = TestHelper.DebugValidatorFormReference(textNotFound);
            var rulesWithoutTextJson = JObject.FromObject(resultRulesText);

            var validatorRules = report.ValidationRules;
            var validatorRulesJson = JArray.FromObject(validatorRules);

            var rulesForDocument = RuleDocumentationComposer.GetRuleDocumentationModel(validatorRules);
            var noko = rulesForDocument.OrderBy(x => x.RuleId).ThenByDescending(y => y.Xpath).ToArray();
            var ruleJson1 = JArray.FromObject(noko);

            var validatorMessages = report.ValidationMessages;
            var validatorMessagesJson = JArray.FromObject(validatorMessages.Where(m => m.XpathField.Contains("ansvarligSoeker")));


            report.Should().NotBeNull();
        }

        [Fact(Skip = skip)]
        public void testInternal()
        {
            var xmlData = File.ReadAllText(@"Data\Ansako\ErklaeringAnsvarsrett_1.xml");

            var noko = TestHelper.GetJsonForPostman(xmlData);

            noko.Should().NotBeEmpty();
        }
    }
}
