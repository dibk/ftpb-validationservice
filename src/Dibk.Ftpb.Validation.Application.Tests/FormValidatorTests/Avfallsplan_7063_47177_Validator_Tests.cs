﻿using Dibk.Ftpb.Common.Datamodels.FellestjenesterBygg.Classes;
using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using Dibk.Ftpb.Validation.Application.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.FormValidatorTests
{
    public class SuppleringArbeidstilAvfallsplan_7063_47177_Validator_Tests
    {
        private const string skip = null;

        [Theory(DisplayName = ".66.160.1 - attachmentTypeName.Gyldig", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AttachementTestData))]
        public void noko(Avfallsplan_7063_47177_Validator validator, SluttrapportForBygningsAvfall form, List<AttachmentInfo> attachmentInfos)
        {
            form.Overgangsordning.DatoForHovedsoeknad = new DateTime(2022, 05, 15);
            var nokoJson = TestHelper.GetJsonForPostman(null, form,new (){ "Situasjonsplan", "AndreRedegjoerelserttt", "Annet", "TegningNyFasade" });


            //var result = validator.StartValidation(form.PlanForAvfall.Avfallsortering, form.Overgangsordning, attachmentInfos);

            //result.Messages.Should().NotBeEmpty();
        }

        public static IEnumerable<object[]> AttachementTestData()
        {
            var validationMessageComposer = MockDataSource.GetValidationMessageComposer();
            var municipalityValidator = MockDataSource.MunicipalityValidatorResult(MunicipalityValidationEnum.Ok);
            var codlistservice = MockDataSource.CodeListService();
            var postalCodeService = MockDataSource.ValidatePostnr(true, "Bø i Telemark", "true");
            var checkList = MockDataSource.GetCheckpoints("");           
            var decryptionFactory = MockDataSource.DecryptText("09107300292");
            var matrikkel = MockDataSource.AnyByggWithEiendomsidentifikasjonInMatrikkel(true);          


            var validator = new Avfallsplan_7063_47177_Validator(
                validationMessageComposer,
                codlistservice,
                postalCodeService,
                checkList,
                matrikkel,
                decryptionFactory);

            var attachmentInfos = new List<AttachmentInfo>
            {
               new () { AttachmentTypeName = "Situasjonsplan", Filename = "Situasjonsplan.pdf", FileSize = 30 },
               new () { AttachmentTypeName = "AndreRedegjoerelserttt", Filename = "AndreRedegjoerelserttt.pdf", FileSize = 20 },
               new () { AttachmentTypeName = "Annetgresrt", Filename = "nokorar.jpg", FileSize = 20 },
               new () { AttachmentTypeName = "Annetsdfgdsfg", Filename = "nokorar.jpg", FileSize = 20 },
               new () { AttachmentTypeName = "Annet", Filename = "nokorar.jpg", FileSize = 20 },
               new () { AttachmentTypeName = "TegningNyFasade", Filename = "TegningNyFasadeqwqwqw.pdf", FileSize = 30 },
        };

         var form = SerializeUtil.DeserializeFromString<SluttrapportForBygningsAvfall>(GetAvfallsplan());
            
            //ValidationInput validationInput = new ValidationInput()
            //{
            //    Attachments = attachmentInfos,
            //    AuthenticatedSubmitter = "",
            //    FormData = ,
            //    SubForms = new SubFormInfo[] { }
            //};
            yield return new object[] { validator, form, attachmentInfos };

        }
        internal static string GetAvfallsplan()
        {
            var xmlData = File.ReadAllText(@"Data\Avfallsplan TM 20220405 korrigert.xml");
            return xmlData;
        }

    }
}
