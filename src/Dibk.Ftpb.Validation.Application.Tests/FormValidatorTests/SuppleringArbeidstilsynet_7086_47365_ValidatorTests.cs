﻿using Dibk.Ftpb.Validation.Application.Enums;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators.Atil;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using FluentAssertions;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace Dibk.Ftpb.Validation.Application.Tests.FormValidatorTests
{
    public class SuppleringArbeidstilsynet_7086_47365_ValidatorTests
    {
        private const string skip = null;

        [Theory(DisplayName = ".66.160.1 - attachmentTypeName.Gyldig", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AttachementTestData))]
        public void noko(SuppleringArbeidstilsynet_7086_47365_Validator form, ValidationInput validationInput)
        {
            List<AttachmentRule> altinnFormAttachmentRules= new List<AttachmentRule>();
            var resut = form.ValidateAttachment(validationInput.Attachments, altinnFormAttachmentRules);
            resut.Should().NotBeNull();
        }
        [Theory(DisplayName = ".66.160.1 - attachmentTypeName.Gyldig", Skip = skip)]
        [Trait("Category", "Validator")]
        [MemberData(nameof(AttachementTestData))]
        public void nokoana(SuppleringArbeidstilsynet_7086_47365_Validator form, ValidationInput validationInput)
        {

            var resut = form.StartValidation(validationInput);
            resut.Should().NotBeNull();

        }

        public static IEnumerable<object[]> AttachementTestData()
        {
            var validationMessageComposer = MockDataSource.GetValidationMessageComposer();
            var municipalityValidator = MockDataSource.MunicipalityValidatorResult(MunicipalityValidationEnum.Ok);
            var codlistservice = MockDataSource.CodeListService();
            var postalCodeService = MockDataSource.ValidatePostnr(true, "Bø i Telemark", "true");
            var checkList = MockDataSource.GetCheckpoints("");
            var atilFeeCalculatorApiHttpClient = MockDataSource.GetIFeeInformation(2240, "3a");
            var decryptionFactory = MockDataSource.DecryptText("09107300292");
            var matrikkel = MockDataSource.AnyByggWithEiendomsidentifikasjonInMatrikkel(true);
            var altinnMetadataServices = MockDataSource.AttachmentValidatorSuppleringATIL();


            var form = new SuppleringArbeidstilsynet_7086_47365_Validator(validationMessageComposer,
                codlistservice, 
                postalCodeService,
                checkList,
                atilFeeCalculatorApiHttpClient,
                decryptionFactory,
                matrikkel,
                altinnMetadataServices);

            var attachmentInfos = new AttachmentInfo[]
            {
               new () { AttachmentTypeName = "Situasjonsplan", Filename = "Situasjonsplan.pdf", FileSize = 30 },
               new () { AttachmentTypeName = "Situasjonsplan", Filename = "Situasjonsplan.pdf", FileSize = 30 },
               new () { AttachmentTypeName = "Annetdfg", Filename = "nokorar.jpg", FileSize = 20 },
               new () { AttachmentTypeName = "Annetgresrt", Filename = "nokorar.jpg", FileSize = 20 },
               new () { AttachmentTypeName = "Annetsdfgdsfg", Filename = "nokorar.jpg", FileSize = 20 },
               new () { AttachmentTypeName = "Annet", Filename = "nokorar.jpg", FileSize = 20 },
               new () { AttachmentTypeName = "Annet", Filename = "nokorar.jpg", FileSize = 20 },
               new () { AttachmentTypeName = "TegningNyFasade", Filename = "TegningNyFasadeqwqwqw.pdf", FileSize = 30 },
               new () { AttachmentTypeName = "TegningNyFasade", Filename = "TegningNyFasadeqwqwqw.pdf", FileSize = 30 },
               new () { AttachmentTypeName = "TegningNyFasade", Filename = "TegningNyFasadeqwqwqw.pdf", FileSize = 30 },
               new () { AttachmentTypeName = "TegningNyFasade", Filename = "TegningNyFasadeqwqwqw.pdf", FileSize = 30 },

        };

            ValidationInput validationInput = new ValidationInput()
            {
                Attachments = attachmentInfos,
                AuthenticatedSubmitter = "",
                FormData = GetSuppleringATL(),
                SubForms = new SubFormInfo[] { }
            };
            yield return new object[] { form, validationInput };

        }
        internal static string GetSuppleringATL()
        {
            var xmlData = File.ReadAllText(@"Data\suppleringForArbeidstilsynetMangelbesvarelse.xml");
            return xmlData;
        }

    }
}
