﻿using Dibk.Ftpb.Validation.Application.Logic.FormValidators.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Tests.Utils;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;
using static Dibk.Ftpb.Validation.Application.Enums.ValidationResultSeverityEnum;

namespace Dibk.Ftpb.Validation.Application.Tests.FormValidatorTests
{
    public class MidlertidigBrukstillatelse_10004_4_ValidatorTests
    {
        private static readonly Dictionary<string, ValidationRule> ValidationSpec;
        private const string XmlRootName = "midlertidigbrukstillatelse";
        private const string FormName = "midlertidig brukstillatelse";

        static MidlertidigBrukstillatelse_10004_4_ValidatorTests()
        {
            ValidationSpec = CreateValidationSpec();
        }

        private enum SpecMismatchDiscrepancy
        {
            Message,
            MessageType,
            XpathField,
            PreCondition,
            ChecklistReference,
        }

        [Fact]
        [Trait("Category", "ValidationRule Control")]
        public void ImplementedTestsFollowsSpec()
        {
            var implementedRules = GetValidationRules();

            var validationRuleIdsNotIncludedInSpec = new List<ValidationRule>();

            var implementedRulesThatDoesNotFollowSpec = new Dictionary<SpecMismatchDiscrepancy, List<ValidationRule>>();

            foreach (var implementedRule in implementedRules)
            {
                if (!ValidationSpec.TryGetValue(implementedRule.Id, out var specifiedRule))
                {
                    validationRuleIdsNotIncludedInSpec.Add(implementedRule);
                    continue;
                }

                if ((implementedRule.ChecklistReference??"") != specifiedRule.ChecklistReference)
                {
                    if (!implementedRulesThatDoesNotFollowSpec.ContainsKey(SpecMismatchDiscrepancy.ChecklistReference))
                        implementedRulesThatDoesNotFollowSpec.Add(SpecMismatchDiscrepancy.ChecklistReference, new List<ValidationRule>());

                    implementedRulesThatDoesNotFollowSpec[SpecMismatchDiscrepancy.ChecklistReference].Add(implementedRule);
                }


                if (implementedRule.Message.Replace(" om {0}", $" om {FormName}") != specifiedRule.Message)
                {
                    if (!implementedRulesThatDoesNotFollowSpec.ContainsKey(SpecMismatchDiscrepancy.Message))
                        implementedRulesThatDoesNotFollowSpec.Add(SpecMismatchDiscrepancy.Message, new List<ValidationRule>());

                    implementedRulesThatDoesNotFollowSpec[SpecMismatchDiscrepancy.Message].Add(implementedRule);
                }

                if (implementedRule.Messagetype != specifiedRule.Messagetype)
                {
                    if (!implementedRulesThatDoesNotFollowSpec.ContainsKey(SpecMismatchDiscrepancy.MessageType))
                        implementedRulesThatDoesNotFollowSpec.Add(SpecMismatchDiscrepancy.MessageType, new List<ValidationRule>());

                    implementedRulesThatDoesNotFollowSpec[SpecMismatchDiscrepancy.MessageType].Add(implementedRule);
                }

                if (implementedRule.XpathField != null && specifiedRule.XpathField != null &&
                    implementedRule.XpathField.Replace(XmlRootName, string.Empty) != specifiedRule.XpathField)
                {
                    if (!implementedRulesThatDoesNotFollowSpec.ContainsKey(SpecMismatchDiscrepancy.XpathField))
                        implementedRulesThatDoesNotFollowSpec.Add(SpecMismatchDiscrepancy.XpathField, new List<ValidationRule>());

                    implementedRulesThatDoesNotFollowSpec[SpecMismatchDiscrepancy.XpathField].Add(implementedRule);
                }

                if (implementedRule.PreCondition != null && specifiedRule.PreCondition != null &&
                    implementedRule.PreCondition.Replace(XmlRootName, string.Empty) != specifiedRule.PreCondition)
                {
                    if (!implementedRulesThatDoesNotFollowSpec.ContainsKey(SpecMismatchDiscrepancy.PreCondition))
                        implementedRulesThatDoesNotFollowSpec.Add(SpecMismatchDiscrepancy.PreCondition, new List<ValidationRule>());

                    implementedRulesThatDoesNotFollowSpec[SpecMismatchDiscrepancy.PreCondition].Add(implementedRule);
                }
            }

            //All implemented rules should be in the spec
            validationRuleIdsNotIncludedInSpec.Should().HaveCount(0);

            //Implemented rules should be distinct with regards to rule id
            var distinctRuleIds = implementedRules.DistinctBy(r => r.Id);
            Assert.Equal(implementedRules.Length, distinctRuleIds.Count());

            //All spec rules should be implemented
            var specRuleIdsNotIncludedInImplemented = (from validationRule in ValidationSpec
                where implementedRules.All(r => r.Id != validationRule.Key)
                select validationRule.Key).ToList();
            Assert.Empty(specRuleIdsNotIncludedInImplemented);

            //All implemented rules should follow the spec
            implementedRulesThatDoesNotFollowSpec.Should().BeEmpty();
        }

        public static ValidationRule[] GetValidationRules()
        {
            var validationMessageComposer = MockDataSource.GetValidationMessageComposer();
            var codlistservice = MockDataSource.CodeListService();
            var postalCodeService = MockDataSource.ValidatePostnr(true, "Bø i Telemark", "true");
            var matrikkel = MockDataSource.AnyByggWithEiendomsidentifikasjonInMatrikkel(true);
            var decryptionFactory = MockDataSource.DecryptText("09107300292");

            var validator = new Midlertidigbrukstillatelse_10004_4_Validator(
                validationMessageComposer,
                null,
                codlistservice, 
                matrikkel,
                decryptionFactory,
                postalCodeService);

            return validator.FormValidationRules;
        }

        private static Dictionary<string, ValidationRule> CreateValidationSpec() => new()
        {
            //Skjemavalidering
            {"10004.4.122.2", new ValidationRule{ ChecklistReference = "", Message = "Innhold må være i henhold til informasjonsmodell/XSD for skjema. Ta kontakt med søknadssystemet du bruker. XSD errors: {0}", Messagetype = ERROR, XpathField = "/xml"}},
            // EiendomByggested
            {"10004.4.7.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi hvilken eiendom/hvilke eiendommer byggesøknaden gjelder.", Messagetype = ERROR, XpathField = "/eiendomByggested/eiendom{0}"}},
            {"10004.4.7.14.1", new ValidationRule{ ChecklistReference = "1.72", Message = "Du må oppgi eiendomsidentifikasjon for eiendom/byggested.", Messagetype = ERROR, XpathField = "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon"}},
            {"10004.4.7.14.7", new ValidationRule{ ChecklistReference = "1.72", Message = "Når eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] er oppgitt for eiendom/byggested, bør den være gyldig i matrikkelen. Du kan sjekke riktig informasjon på https://seeiendom.no", Messagetype = WARNING, XpathField = "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon"}},
            {"10004.4.7.14.3", new ValidationRule{ ChecklistReference = "1.72", Message = "Eiendomsidentifikasjon [{0}-{1}/{2}/{3}/{4}] for eiendom/byggested ble ikke validert mot matrikkelen. Identifikasjonen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Messagetype = WARNING, XpathField = "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon"}},
            {"10004.4.7.14.10.1", new ValidationRule{ ChecklistReference = "1.2", Message = "Kommunenummer må fylles ut for eiendom/byggested.", Messagetype = ERROR, XpathField = "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/kommunenummer"}},
            {"10004.4.7.14.10.2", new ValidationRule{ ChecklistReference = "1.2", Message = "Kommunenummeret '{0}' for eiendom/byggested finnes ikke i kodelisten. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Messagetype = ERROR, XpathField = "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/kommunenummer"}},
            {"10004.4.7.14.10.3", new ValidationRule{ ChecklistReference = "1.2", Message = "En teknisk feil gjør at vi ikke kan bekrefte om kommunenummeret du har oppgitt '{0}' er riktig. Du kan sjekke riktig kommunenummer på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer", Messagetype = WARNING, XpathField = "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/kommunenummer"}},
            {"10004.4.7.14.10.5", new ValidationRule{ ChecklistReference = "1.2", Message = "Kommunenummeret '{0}' for eiendom/byggested har ugyldig status ({1}). Du kan sjekke status på https://register.geonorge.no/sosi-kodelister/inndelinger/inndelingsbase/kommunenummer ", Messagetype = ERROR, XpathField = "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/kommunenummer"}},
            {"10004.4.7.14.11.1", new ValidationRule{ ChecklistReference = "1.3", Message = "Gårdsnummer må fylles ut for eiendom/byggested.", Messagetype = ERROR, XpathField = "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/gaardsnummer"}},
            {"10004.4.7.14.11.2", new ValidationRule{ ChecklistReference = "1.3", Message = "Gårdsnummer '{0}' for eiendom/byggested må være større enn '0'.", Messagetype = ERROR, XpathField = "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/gaardsnummer"}},
            {"10004.4.7.14.12.1", new ValidationRule{ ChecklistReference = "1.4", Message = "Bruksnummer må fylles ut for eiendom/byggested.", Messagetype = ERROR, XpathField = "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/bruksnummer"}},
            {"10004.4.7.14.12.2", new ValidationRule{ ChecklistReference = "1.4", Message = "Bruksnummer '{0}' for eiendom/byggested må være større enn '0'.", Messagetype = ERROR, XpathField = "/eiendomByggested/eiendom{0}/eiendomsidentifikasjon/bruksnummer"}},
            {"10004.4.7.15.2", new ValidationRule{ ChecklistReference = "1.6", Message = "Bygningsnummer for eiendom/byggested må være større enn '0'.", Messagetype = ERROR, XpathField = "/eiendomByggested/eiendom{0}/bygningsnummer"}},
            {"10004.4.7.15.3", new ValidationRule{ ChecklistReference = "1.6", Message = "Bygningsnummer for eiendom/byggested ble ikke validert mot matrikkelen. Bygningsnummeret kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det. Du kan sjekke adressen på https://seeiendom.no", Messagetype = WARNING, XpathField = "/eiendomByggested/eiendom{0}/bygningsnummer"}},
            {"10004.4.7.15.24", new ValidationRule{ ChecklistReference = "1.6", Message = "Du har oppgitt følgende bygningsnummer for eiendom/byggested: '{0}'. Bygningsnummeret må være et tall.", Messagetype = ERROR, XpathField = "/eiendomByggested/eiendom{0}/bygningsnummer"}},
            {"10004.4.7.15.7", new ValidationRule{ ChecklistReference = "1.6", Message = "Når bygningsnummer [{0}] er oppgitt for eiendom/byggested, bør det være gyldig i matrikkelen på aktuelt matrikkelnummer. Du kan sjekke riktig bygningsnummer på https://seeiendom.no", Messagetype = WARNING, XpathField = "/eiendomByggested/eiendom{0}/bygningsnummer"}},
            {"10004.4.7.16.2", new ValidationRule{ ChecklistReference = "", Message = "Når bruksenhetsnummer/bolignummer er fylt ut for eiendom/byggested, må det følge riktig format (for eksempel H0101). Se https://www.kartverket.no/eiendom/adressering/bruksenhetsnummer/ ", Messagetype = ERROR, XpathField = "/eiendomByggested/eiendom{0}/bolignummer"}},
            {"10004.4.7.17.1", new ValidationRule{ ChecklistReference = "", Message = "Navnet på kommunen bør fylles ut for eiendom/byggested.", Messagetype = WARNING, XpathField = "/eiendomByggested/eiendom{0}/kommunenavn"}},
            {"10004.4.7.8.1", new ValidationRule{ ChecklistReference = "1.8", Message = "Postadresse for eiendom/byggested bør fylles ut.", Messagetype = WARNING, XpathField = "/eiendomByggested/eiendom{0}/adresse"}},
            {"10004.4.7.8.1.1", new ValidationRule{ ChecklistReference = "1.8", Message = "Adresselinje 1 for eiendom/byggested bør fylles ut.", Messagetype = WARNING, XpathField = "/eiendomByggested/eiendom{0}/adresse/adresselinje1"}},
            {"10004.4.7.8.7.1", new ValidationRule{ ChecklistReference = "1.8", Message = "Du bør oppgi gatenavn for eiendom/byggested slik at adressen kan valideres mot matrikkelen. Du kan sjekke riktig adresse på https://seeiendom.no ", Messagetype = WARNING, XpathField = "/eiendomByggested/eiendom{0}/adresse/gatenavn"}},
            {"10004.4.7.8.8.1", new ValidationRule{ ChecklistReference = "1.8", Message = "Du bør oppgi husnummer og eventuell bokstav for eiendom/byggested slik at adressen kan valideres mot matrikkelen. Du kan sjekke riktig adresse på https://seeiendom.no", Messagetype = WARNING, XpathField = "/eiendomByggested/eiendom{0}/adresse/husnr"}},
            {"10004.4.7.195.7", new ValidationRule{ ChecklistReference = "1.8", Message = "Du har oppgitt feil gateadresse for eiendom/byggested. Du kan sjekke riktig adresse på https://seeiendom.no", Messagetype = WARNING, XpathField = "/eiendomByggested/eiendom{0}/adresse"}},
            {"10004.4.7.195.3", new ValidationRule{ ChecklistReference = "1.8", Message = "Gateadressen for eiendom/byggested ble ikke validert mot matrikkelen. Gateadressen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det. Du kan sjekke adressen på https://seeiendom.no", Messagetype = WARNING, XpathField = "/eiendomByggested/eiendom{0}/adresse"}},
            //Kommunens saksnummer
            {"10004.4.25.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi kommunens saksnummer med saksår og sekvensnummer.", Messagetype = ERROR, XpathField = "/kommunensSaksnummer"}},
            {"10004.4.25.108.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi kommunens saksnummer med saksår.", Messagetype = ERROR, XpathField = "/kommunensSaksnummer/saksaar", PreCondition = "/kommunensSaksnummer/sakssekvensnummer"}},
            {"10004.4.25.109.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi kommunens saksnummer med sekvensnummer.", Messagetype = ERROR, XpathField = "/kommunensSaksnummer/sakssekvensnummer", PreCondition = "/kommunensSaksnummer/saksaar"}},
            {"10004.4.25.108.2", new ValidationRule{ ChecklistReference = "", Message = "Saksår ({0}) for kommunens saksnummer er ikke gyldig. Saksåret må inneholde fire siffer, og ikke være eldre enn 30 år.", Messagetype = ERROR, XpathField = "/kommunensSaksnummer/saksaar"}},
            //Metadata
            {"10004.4.92.1", new ValidationRule{ ChecklistReference = "", Message = "Søknadens metadata må fylles ut.", Messagetype = ERROR, XpathField = "/metadata"}},
            {"10004.4.92.90.1", new ValidationRule{ ChecklistReference = "", Message = "Systemet må fylle ut samme navn som er brukt i registrering for Altinn API i 'fraSluttbrukersystem'.", Messagetype = ERROR, XpathField = "/metadata/fraSluttbrukersystem"}},
            {"10004.4.92.93.38.2", new ValidationRule{ ChecklistReference = "", Message = "'{0}' er en ugyldig kodeverdi for målform. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/foretrukketspraak", Messagetype = WARNING, XpathField = "/metadata/foretrukketSpraak/kodeverdi"}},
            {"10004.4.92.93.38.3", new ValidationRule{ ChecklistReference = "", Message = "Kodeverdien '{0}' ble ikke validert. Målformen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Messagetype = WARNING, XpathField = "/metadata/foretrukketSpraak/kodeverdi"}},
            {"10004.4.92.93.39.1", new ValidationRule{ ChecklistReference = "", Message = "Når målform er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/foretrukketspraak", Messagetype = ERROR, XpathField = "/metadata/foretrukketSpraak/kodebeskrivelse", PreCondition = "/metadata/foretrukketSpraak/kodeverdi"}},
            {"10004.4.92.93.39.2", new ValidationRule{ ChecklistReference = "", Message = "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for målformen. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/foretrukketspraak", Messagetype = ERROR, XpathField = "/metadata/foretrukketSpraak/kodebeskrivelse"}},
            //GenerelleVilkaar
            {"10004.4.102.1", new ValidationRule{ ChecklistReference = "", Message = "Du må svare på de generelle vilkårene.", Messagetype = ERROR, XpathField = "/generelleVilkaar"}},
            {"10004.4.102.57.1", new ValidationRule{ ChecklistReference = "1.1", Message = "Du må svare på om søknaden og all relevant dokumentasjon er skrevet på eller oversatt til norsk, svensk eller dansk.", Messagetype = ERROR, XpathField = "/generelleVilkaar/norskSvenskDansk"}},
            //Ansvar for byggesaken
            {"10004.4.95.1", new ValidationRule{ ChecklistReference = "", Message = "Du må velge hvilket ansvarsforhold som gjelder for byggesaken. Du kan sjekke riktig ansvarsforhold på https://register.geonorge.no/kodelister/byggesoknad/ansvar-for-byggesaken", Messagetype = ERROR, XpathField = "/ansvarForByggesaken"}},
            {"10004.4.95.38.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi en kodeverdi for ansvarsforhold. Du kan sjekke riktig ansvarsforhold på https://register.geonorge.no/kodelister/byggesoknad/ansvar-for-byggesaken", Messagetype = ERROR, XpathField = "/ansvarForByggesaken/kodeverdi"}},
            {"10004.4.95.38.2", new ValidationRule{ ChecklistReference = "", Message = "'{0}' er en ugyldig kodeverdi for ansvar for byggesaken. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/byggesoknad/ansvar-for-byggesaken", Messagetype = ERROR, XpathField = "/ansvarForByggesaken/kodeverdi"}},
            {"10004.4.95.38.3", new ValidationRule{ ChecklistReference = "", Message = "Kodeverdien '{0}' ble ikke validert. Ansvar for byggesaken kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Messagetype = ERROR, XpathField = "/ansvarForByggesaken/kodeverdi"}},
            {"10004.4.95.39.1", new ValidationRule{ ChecklistReference = "", Message = "Når ansvar for byggesaken er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/ansvar-for-byggesaken", Messagetype = ERROR, XpathField = "/ansvarForByggesaken/kodebeskrivelse", PreCondition = "/ansvarForByggesaken/kodeverdi"}},
            {"10004.4.95.39.2", new ValidationRule{ ChecklistReference = "", Message = "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for ansvar for byggesaken. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/byggesoknad/ansvar-for-byggesaken", Messagetype = WARNING, XpathField = "/ansvarForByggesaken/kodebeskrivelse", PreCondition = "/ansvarForByggesaken/kodeverdi"}},
            //Ansvarlig søker
            {"10004.4.2.1", new ValidationRule{ ChecklistReference = "", Message = "Du har valgt '{0}'. Da må du fylle ut informasjon om ansvarlig søker.", Messagetype = ERROR, XpathField = "/ansvarligSoeker", PreCondition = "/ansvarForByggesaken/kodeverdi = 'medAnsvar' || 'selvbygger'"}},
            {"10004.4.2.2", new ValidationRule{ ChecklistReference = "", Message = "Du har valgt '{0}'. Da skal ikke søknaden ha en ansvarlig søker.", Messagetype = ERROR, XpathField = "/ansvarligSoeker", PreCondition = "/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll'"}},
            {"10004.4.2.3", new ValidationRule{ ChecklistReference = "", Message = "Ansvarlig søker for byggetillatelsen ble ikke validert. For å validere ansvarlig søker må du først fylle ut en gyldig verdi for 'ansvarForByggesaken'.", Messagetype = ERROR, XpathField = "/ansvarligSoeker", PreCondition = "/ansvarForByggesaken/kodeverdi ugyldig"}},
            {"10004.4.2.13.1", new ValidationRule{ ChecklistReference = "", Message = "Kontaktpersonen til ansvarlig søker bør fylles ut.", Messagetype = WARNING, XpathField = "/ansvarligSoeker/kontaktperson"}},
            {"10004.4.2.13.20.1", new ValidationRule{ ChecklistReference = "", Message = "Navnet til kontaktpersonen for ansvarlig søker bør fylles ut.", Messagetype = WARNING, XpathField = "/ansvarligSoeker/kontaktperson/navn", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.13.21.1", new ValidationRule{ ChecklistReference = "", Message = "Telefonnummeret eller mobilnummeret til ansvarlig søkers kontaktperson bør fylles ut.", Messagetype = WARNING, XpathField = "/ansvarligSoeker/kontaktperson/telefonnummer", PreCondition = "/ansvarligSoeker/kontaktperson/mobilnummer"}},
            {"10004.4.2.13.21.2", new ValidationRule{ ChecklistReference = "", Message = "Telefonnummeret til ansvarlig søkers kontaktperson må kun inneholde tall og '+'.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/kontaktperson/telefonnummer"}},
            {"10004.4.2.13.22.2", new ValidationRule{ ChecklistReference = "", Message = "Mobilnummeret til ansvarlig søkers kontaktperson må kun inneholde tall og '+'.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/kontaktperson/mobilnummer"}},
            {"10004.4.2.13.23.1", new ValidationRule{ ChecklistReference = "", Message = "E-postadressen til ansvarlig søkers kontaktperson bør fylles ut.", Messagetype = WARNING, XpathField = "/ansvarligSoeker/kontaktperson/epost", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.16.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi partstypen for ansvarlig søker. Du kan sjekke gyldige partstyper på https://register.geonorge.no/byggesoknad/partstype", Messagetype = ERROR, XpathField = "/ansvarligSoeker/partstype"}},
            {"10004.4.2.16.38.1", new ValidationRule{ ChecklistReference = "", Message = "Kodeverdien for 'partstype' til ansvarlig søker må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Messagetype = ERROR, XpathField = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.16.38.2", new ValidationRule{ ChecklistReference = "", Message = "'{0}' er en ugyldig kodeverdi for partstypen til ansvarlig søker. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.16.38.3", new ValidationRule{ ChecklistReference = "", Message = "Kodeverdien '{0}' ble ikke validert. Partstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Messagetype = WARNING, XpathField = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.16.39.1", new ValidationRule{ ChecklistReference = "", Message = "Når partstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", Messagetype = ERROR, XpathField = "/ansvarligSoeker/partstype/kodebeskrivelse", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.16.39.2", new ValidationRule{ ChecklistReference = "", Message = "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for partstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", Messagetype = WARNING, XpathField = "/ansvarligSoeker/partstype/kodebeskrivelse", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.18.1", new ValidationRule{ ChecklistReference = "", Message = "Fødselsnummer må fylles ut når ansvarlig søker er en privatperson.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/foedselsnummer", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.18.17", new ValidationRule{ ChecklistReference = "", Message = "Fødselsnummeret til ansvarlig søker har ikke gyldig kontrollsiffer.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/foedselsnummer", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.18.18", new ValidationRule{ ChecklistReference = "", Message = "Fødselsnummeret til ansvarlig søker kan ikke dekrypteres.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/foedselsnummer", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.18.2", new ValidationRule{ ChecklistReference = "", Message = "Fødselsnummeret til ansvarlig søker er ikke gyldig.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/foedselsnummer", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.19.1", new ValidationRule{ ChecklistReference = "", Message = "Organisasjonsnummeret til ansvarlig søker må fylles ut.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/organisasjonsnummer", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.19.17", new ValidationRule{ ChecklistReference = "", Message = "Organisasjonsnummeret ('{0}') til ansvarlig søker har ikke gyldig kontrollsiffer.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/organisasjonsnummer", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.19.2", new ValidationRule{ ChecklistReference = "", Message = "Organisasjonsnummeret ('{0}') til ansvarlig søker er ikke gyldig.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/organisasjonsnummer", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.20.1", new ValidationRule{ ChecklistReference = "", Message = "Navnet til ansvarlig søker må fylles ut.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/navn", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.21.1", new ValidationRule{ ChecklistReference = "", Message = "Telefonnummeret eller mobilnummeret til ansvarlig søker bør fylles ut.", Messagetype = WARNING, XpathField = "/ansvarligSoeker/telefonnummer", PreCondition = "/ansvarligSoeker/mobilnummer"}},
            {"10004.4.2.21.2", new ValidationRule{ ChecklistReference = "", Message = "Telefonnummeret til ansvarlig søker må kun inneholde tall og '+'.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/telefonnummer"}},
            {"10004.4.2.22.2", new ValidationRule{ ChecklistReference = "", Message = "Mobilnummeret til ansvarlig søker må kun inneholde tall og '+'.", Messagetype = ERROR, XpathField = "/ansvarligSoeker/mobilnummer"}},
            {"10004.4.2.23.1", new ValidationRule{ ChecklistReference = "", Message = "E-postadressen til ansvarlig søker bør fylles ut.", Messagetype = WARNING, XpathField = "/ansvarligSoeker/epost", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
            {"10004.4.2.9.1", new ValidationRule{ ChecklistReference = "", Message = "Adressen til ansvarlig søker bør fylles ut.", Messagetype = WARNING, XpathField = "/ansvarligSoeker/adresse"}},
            {"10004.4.2.9.1.1", new ValidationRule{ ChecklistReference = "", Message = "Adresselinje 1 bør fylles ut for ansvarlig søker.", Messagetype = WARNING, XpathField = "/ansvarligSoeker/adresse/adresselinje1"}},
            {"10004.4.2.9.4.1", new ValidationRule{ ChecklistReference = "", Message = "Du bør fylle ut postnummeret til ansvarlig søker.", Messagetype = WARNING, XpathField = "/ansvarligSoeker/adresse/postnr"}},
            {"10004.4.2.9.4.2", new ValidationRule{ ChecklistReference = "", Message = "Postnummeret '{0}' til ansvarlig søker er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Messagetype = ERROR, XpathField = "/ansvarligSoeker/adresse/postnr"}},
            {"10004.4.2.9.5.2", new ValidationRule{ ChecklistReference = "", Message = "Postnummeret '{0}' til ansvarlig søker stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/", Messagetype = ERROR, XpathField = "/ansvarligSoeker/adresse/poststed", PreCondition = "/ansvarligSoeker/adresse/postnr"}},
            {"10004.4.2.9.6.2", new ValidationRule{ ChecklistReference = "", Message = "Landkoden for ansvarlig søkers adresse er ikke gyldig.", Messagetype = WARNING, XpathField = "/ansvarligSoeker/adresse/landkode"}},
            //Tiltakshaver
            {"10004.4.20.1", new ValidationRule{ ChecklistReference = "", Message = "Du har valgt '{0}'. Da må du fylle ut informasjon om tiltakshaver.", Messagetype = ERROR, XpathField = "/tiltakshaver", PreCondition = "/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll'"}},
            {"10004.4.20.13.1", new ValidationRule{ ChecklistReference = "", Message = "Kontaktpersonen til tiltakshaver bør fylles ut.", Messagetype = WARNING, XpathField = "/tiltakshaver/kontaktperson"}},
            {"10004.4.20.13.20.1", new ValidationRule{ ChecklistReference = "", Message = "Navnet til kontaktpersonen for tiltakshaver bør fylles ut.", Messagetype = WARNING, XpathField = "/tiltakshaver/kontaktperson/navn"}},
            {"10004.4.20.13.21.1", new ValidationRule{ ChecklistReference = "", Message = "Telefonnummeret eller mobilnummeret til tiltakshaver kontaktperson bør fylles ut.", Messagetype = WARNING, XpathField = "/tiltakshaver/kontaktperson/telefonnummer", PreCondition = "/tiltakshaver/kontaktperson/mobilnummer"}},
            {"10004.4.20.13.21.2", new ValidationRule{ ChecklistReference = "", Message = "Telefonnummeret til tiltakshaver kontaktperson må kun inneholde tall og '+'.", Messagetype = WARNING, XpathField = "/tiltakshaver/kontaktperson/telefonnummer"}},
            {"10004.4.20.13.22.2", new ValidationRule{ ChecklistReference = "", Message = "Mobilnummeret til tiltakshaver kontaktperson må kun inneholde tall og '+'.", Messagetype = WARNING, XpathField = "/tiltakshaver/kontaktperson/mobilnummer"}},
            {"10004.4.20.13.23.1", new ValidationRule{ ChecklistReference = "", Message = "E-postadressen til tiltakshaver kontaktperson bør fylles ut.", Messagetype = WARNING, XpathField = "/tiltakshaver/kontaktperson/epost"}},
            {"10004.4.20.16.1", new ValidationRule{ ChecklistReference = "", Message = "Du må fylle ut partstypen for tiltakshaver.", Messagetype = ERROR, XpathField = "/tiltakshaver/partstype"}},
            {"10004.4.20.16.38.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi partstypen for tiltakshaver. Du kan sjekke gyldige partstyper på https://register.geonorge.no/byggesoknad/partstype", Messagetype = ERROR, XpathField = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.16.38.2", new ValidationRule{ ChecklistReference = "", Message = "'{0}' er en ugyldig kodeverdi for partstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/byggesoknad/partstype", Messagetype = ERROR, XpathField = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.16.38.3", new ValidationRule{ ChecklistReference = "", Message = "Kodeverdien '{0}' ble ikke validert. Partstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Messagetype = WARNING, XpathField = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.16.39.1", new ValidationRule{ ChecklistReference = "", Message = "Når partstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", Messagetype = ERROR, XpathField = "/tiltakshaver/partstype/kodebeskrivelse", PreCondition = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.16.39.2", new ValidationRule{ ChecklistReference = "", Message = "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for partstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/byggesoknad/partstype", Messagetype = WARNING, XpathField = "/tiltakshaver/partstype/kodebeskrivelse", PreCondition = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.18.1", new ValidationRule{ ChecklistReference = "", Message = "Fødselsnummer må angis når tiltakshaver er privatperson.", Messagetype = ERROR, XpathField = "/tiltakshaver/foedselsnummer", PreCondition = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.18.17", new ValidationRule{ ChecklistReference = "", Message = "Fødselsnummeret til tiltakshaver har ikke gyldig kontrollsiffer.", Messagetype = ERROR, XpathField = "/tiltakshaver/foedselsnummer", PreCondition = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.18.18", new ValidationRule{ ChecklistReference = "", Message = "Fødselsnummeret til tiltakshaver kan ikke dekrypteres.", Messagetype = ERROR, XpathField = "/tiltakshaver/foedselsnummer", PreCondition = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.18.2", new ValidationRule{ ChecklistReference = "", Message = "Tiltakshavers fødselsnummer er ikke gyldig.", Messagetype = ERROR, XpathField = "/tiltakshaver/foedselsnummer", PreCondition = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.19.1", new ValidationRule{ ChecklistReference = "", Message = "Organisasjonsnummeret til tiltakshaver må fylles ut når tiltakshaver er en organisasjon.", Messagetype = ERROR, XpathField = "/tiltakshaver/organisasjonsnummer", PreCondition = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.19.17", new ValidationRule{ ChecklistReference = "", Message = "Organisasjonsnummeret ('{0}') til tiltakshaver har ikke gyldig kontrollsiffer.", Messagetype = ERROR, XpathField = "/tiltakshaver/organisasjonsnummer", PreCondition = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.19.2", new ValidationRule{ ChecklistReference = "", Message = "Organisasjonsnummeret ('{0}') til tiltakshaver er ikke gyldig.", Messagetype = ERROR, XpathField = "/tiltakshaver/organisasjonsnummer", PreCondition = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.20.1", new ValidationRule{ ChecklistReference = "", Message = "Navnet til tiltakshaver må fylles ut.", Messagetype = ERROR, XpathField = "/tiltakshaver/navn"}},
            {"10004.4.20.21.1", new ValidationRule{ ChecklistReference = "", Message = "Telefonnummeret eller mobilnummeret til tiltakshaver må fylles ut.", Messagetype = ERROR, XpathField = "/tiltakshaver/telefonnummer", PreCondition = "/tiltakshaver/mobilnummer"}},
            {"10004.4.20.21.2", new ValidationRule{ ChecklistReference = "", Message = "Telefonnummeret til tiltakshaver må kun inneholde tall og '+'.", Messagetype = ERROR, XpathField = "/tiltakshaver/telefonnummer"}},
            {"10004.4.20.22.2", new ValidationRule{ ChecklistReference = "", Message = "Mobilnummeret til tiltakshaver må kun inneholde tall og '+'.", Messagetype = ERROR, XpathField = "/tiltakshaver/mobilnummer"}},
            {"10004.4.20.23.1", new ValidationRule{ ChecklistReference = "", Message = "E-postadressen til tiltakshaver bør fylles ut.", Messagetype = WARNING, XpathField = "/tiltakshaver/epost", PreCondition = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.20.9.1", new ValidationRule{ ChecklistReference = "", Message = "Adressen til tiltakshaver bør fylles ut.", Messagetype = WARNING, XpathField = "/tiltakshaver/adresse"}},
            {"10004.4.20.9.1.1", new ValidationRule{ ChecklistReference = "", Message = "Adresselinje 1 bør fylles ut for tiltakshaver.", Messagetype = WARNING, XpathField = "/tiltakshaver/adresse/adresselinje1"}},
            {"10004.4.20.9.4.1", new ValidationRule{ ChecklistReference = "", Message = "Postnummeret til tiltakshaver bør angis.", Messagetype = WARNING, XpathField = "/tiltakshaver/adresse/postnr"}},
            {"10004.4.20.9.4.2", new ValidationRule{ ChecklistReference = "", Message = "Postnummeret '{0}' til tiltakshaver er ugyldig. Du kan sjekke riktig postnummer på http://adressesok.bring.no/", Messagetype = ERROR, XpathField = "/tiltakshaver/adresse/postnr"}},
            {"10004.4.20.9.5.2", new ValidationRule{ ChecklistReference = "", Message = "Postnummeret '{0}' til tiltakshaver stemmer ikke overens med poststedet '{1}'. Postnummeret er fra '{2}'. Du kan sjekke riktig postnummer/poststed på http://adressesok.bring.no/", Messagetype = WARNING, XpathField = "/tiltakshaver/adresse/poststed", PreCondition = "/tiltakshaver/adresse/postnr"}},
            {"10004.4.20.9.6.2", new ValidationRule{ ChecklistReference = "", Message = "Landkoden for tiltakshavers adresse er ikke gyldig.", Messagetype = WARNING, XpathField = "/tiltakshaver/adresse/landkode"}},
            //Søknad gjelder
            {"10004.4.83.1", new ValidationRule{ ChecklistReference = "", Message = "Du må beskrive hva denne søknaden gjelder.", Messagetype = ERROR, XpathField = "/soeknadGjelder"}},
            {"10004.4.83.178.1", new ValidationRule{ ChecklistReference = "", Message = "Du må svare på om denne søknaden gjelder for hele eller deler av tiltaket som byggetillatelsen gjelder.", Messagetype = ERROR, XpathField = "/soeknadGjelder/gjelderHeleTiltaket"}},
            {"10004.4.83.179.1", new ValidationRule{ ChecklistReference = "", Message = "Du har svart at denne søknaden gjelder for en del av tiltaket. Da må du beskrive hvilken del av tiltaket denne søknaden gjelder for.", Messagetype = ERROR, XpathField = "/soeknadGjelder/delAvTiltaket", PreCondition = "/soeknadGjelder/gjelderHeleTiltaket = 'false'"}},
            {"10004.4.83.114.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi tiltakstypen eller tiltakstypene som gjelder for byggetillatelsen.", Messagetype = ERROR, XpathField = "/soeknadGjelder/type"}},
            {"10004.4.83.114.3", new ValidationRule{ ChecklistReference = "", Message = "Tiltakstypen(e) for byggetillatelsen ble ikke validert. For å validere tiltakstypen(e), må du først fylle ut en gyldig verdi for 'ansvarForByggesaken'.", Messagetype = ERROR, XpathField = "/soeknadGjelder/type", PreCondition = "/ansvarForByggesaken/kodeverdi ugyldig"}},
            {"10004.4.83.84.38.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi en tiltakstype. Du kan sjekke riktig kodeverdi for tiltak med ansvarsrett på https://register.geonorge.no/byggesoknad/tiltaktype", Messagetype = ERROR, XpathField = "/soeknadGjelder/type/kode{0}/kodeverdi", PreCondition = "/ansvarForByggesaken/kodeverdi"}},
            {"10004.4.83.84.38.2", new ValidationRule{ ChecklistReference = "", Message = "'{0}' er en ugyldig kodeverdi for tiltakstype. Du kan sjekke riktig kodeverdi for tiltak med ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltaktype", Messagetype = ERROR, XpathField = "/soeknadGjelder/type/kode{0}/kodeverdi", PreCondition = "/ansvarForByggesaken/kodeverdi"}},
            {"10004.4.83.84.38.3", new ValidationRule{ ChecklistReference = "", Message = "Kodeverdien '{0}' ble ikke validert. Tiltakstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det", Messagetype = ERROR, XpathField = "/soeknadGjelder/type/kode{0}/kodeverdi", PreCondition = "/soeknadGjelder/type"}},
            {"10004.4.83.84.39.1", new ValidationRule{ ChecklistReference = "", Message = "Når tiltakstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse for tiltak med ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltaktype", Messagetype = ERROR, XpathField = "/soeknadGjelder/type/kode{0}/kodebeskrivelse", PreCondition = "/soeknadGjelder/type/kode{0}/kodeverdi"}},
            {"10004.4.83.84.39.2", new ValidationRule{ ChecklistReference = "", Message = "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tiltakstypen. Du kan sjekke riktig kodebeskrivelse for tiltak med ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltaktype", Messagetype = WARNING, XpathField = "/soeknadGjelder/type/kode{0}/kodebeskrivelse", PreCondition = "/soeknadGjelder/type/kode{0}/kodeverdi"}},
            {"10004.4.83.101.38.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi en tiltakstype. Du kan sjekke riktig kodeverdi for tiltak uten ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltakstyperutenansvarsrett", Messagetype = ERROR, XpathField = "/soeknadGjelder/type/kode{0}/kodeverdi", PreCondition = "/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll'"}},
            {"10004.4.83.101.38.2", new ValidationRule{ ChecklistReference = "", Message = "'{0}' er en ugyldig kodeverdi for tiltakstype. Du kan sjekke riktig kodeverdi for tiltak uten ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltakstyperutenansvarsrett", Messagetype = ERROR, XpathField = "/soeknadGjelder/type/kode{0}/kodeverdi", PreCondition = "/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll'"}},
            {"10004.4.83.101.39.1", new ValidationRule{ ChecklistReference = "", Message = "Når tiltakstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse for tiltak uten ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltakstyperutenansvarsrett", Messagetype = ERROR, XpathField = "/soeknadGjelder/type/kode{0}/kodebeskrivelse", PreCondition = "/soeknadGjelder/type/kode{0}/kodeverdi & (/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll')"}},
            {"10004.4.83.101.39.2", new ValidationRule{ ChecklistReference = "", Message = "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tiltakstypen. Du kan sjekke riktig kodebeskrivelse for tiltak uten ansvarsrett på https://register.geonorge.no/kodelister/byggesoknad/tiltakstyperutenansvarsrett", Messagetype = WARNING, XpathField = "/soeknadGjelder/type/kode{0}/kodebeskrivelse", PreCondition = "/soeknadGjelder/type/kode{0}/kodeverdi & (/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll')"}},
            {"10004.4.83.101.38.3", new ValidationRule{ ChecklistReference = "", Message = "Kodeverdien '{0}' ble ikke validert. Tiltakstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det", Messagetype = ERROR, XpathField = "/soeknadGjelder/type/kode{0}/kodeverdi", PreCondition = "/soeknadGjelder/type & (/ansvarForByggesaken/kodeverdi = 'utenAnsvar' || 'utenAnsvarMedKontroll')"}},
            {"10004.4.83.180.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi hvilket nummer denne søknaden om midlertidig brukstillatelse er.", Messagetype = ERROR, XpathField = "/soeknadGjelder/delsoeknadsnummer"}},
            {"10004.4.83.180.24", new ValidationRule{ ChecklistReference = "", Message = "Nummeret til søknaden om midlertidig brukstillatelse må være et tall.", Messagetype = ERROR, XpathField = "/soeknadGjelder/delsoeknadsnummer"}},
            //Delsøknader
            {"10004.4.187.1", new ValidationRule{ ChecklistReference = "", Message = "Du bør oppgi informasjon om de tidligere innsendte søknadene om midlertidig brukstillatelse.", Messagetype = WARNING, XpathField = "/delsoeknader", PreCondition = "/soeknadGjelder/delsoeknadsnummer > 1"}},
            {"10004.4.94.1", new ValidationRule{ ChecklistReference = "", Message = "Du bør oppgi informasjon om den tidligere innsendte delsøknaden.", Messagetype = WARNING, XpathField = "/delsoeknader/delsoeknad{0}", PreCondition = "/soeknadGjelder/delsoeknadsnummer > 1"}},
            {"10004.4.94.179.1", new ValidationRule{ ChecklistReference = "", Message = "Du må beskrive hvilke deler av tiltaket som gjaldt for den tidligere innsendte delsøknaden.", Messagetype = ERROR, XpathField = "/delsoeknader/delsoeknad{0}/delAvTiltaket", PreCondition = "/soeknadGjelder/delsoeknadsnummer > 1"}},
            {"10004.4.94.186.1", new ValidationRule{ ChecklistReference = "", Message = "Dersom det er gitt tillatelse for den tidligere søknaden, bør du oppgi datoen den ble gitt.", Messagetype = WARNING, XpathField = "/delsoeknader/delsoeknad{0}/tillatelsedato", PreCondition = "/soeknadGjelder/delsoeknadsnummer > 1"}},
            {"10004.4.94.180.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi hvilket nummer den tidligere innsendte delsøknaden er.", Messagetype = ERROR, XpathField = "/delsoeknader/delsoeknad{0}/delsoeknadsnummer", PreCondition = "/delsoeknader/delsoeknad{0}"}},
            {"10004.4.94.180.24", new ValidationRule{ ChecklistReference = "", Message = "Nummeret til den tidligere innsendte delsøknaden må være et tall.", Messagetype = ERROR, XpathField = "/delsoeknader/delsoeknad{0}/delsoeknadsnummer"}},
            //UtfallBesvarelse
            {"10004.4.185.1", new ValidationRule{ ChecklistReference = "", Message = "Dersom det er stilt vilkår eller oppfølgingspunkt for midlertidig brukstillatelse i byggetillatelsen, må du opplyse om status for disse.", Messagetype = WARNING, XpathField = "/utfallBesvarelse"}},
            {"10004.4.85.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi informasjon om vilkåret/oppfølgingspunktet.", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}"}},
            {"10004.4.85.86.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi hvilken type utfall du besvarer. Hvilken utfallstype som er riktig bestemmes av det aktuelle sjekkpunktet. Du kan sjekke tilgjengelige utfallstyper på https://register.geonorge.no/kodelister/ebyggesak/utfall", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/utfallType"}},
            {"10004.4.85.86.38.1", new ValidationRule{ ChecklistReference = "", Message = "Kodeverdien for 'utfall' må fylles ut. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/ebyggesak/utfall", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi"}},
            {"10004.4.85.86.38.2", new ValidationRule{ ChecklistReference = "", Message = "‘{0}' er en ugyldig kodeverdi for utfallstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/kodelister/ebyggesak/utfall.", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi"}},
            {"10004.4.85.86.38.6", new ValidationRule{ ChecklistReference = "", Message = "Utfallstypen '{0}' for utfallSvar er ikke tillatt. Godkjente utfallstyper for søknader om midlertidig brukstillatelse er begrenset til ‘SVV’ og 'MKV’.", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi"}},
            {"10004.4.85.86.38.3", new ValidationRule{ ChecklistReference = "", Message = "Kodeverdien '{0}' ble ikke validert. Utfallstypen kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi"}},
            {"10004.4.85.86.39.1", new ValidationRule{ ChecklistReference = "", Message = "Når utfallstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/ebyggesak/utfall", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/utfallType/kodebeskrivelse", PreCondition = "/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi"}},
            {"10004.4.85.86.39.2", new ValidationRule{ ChecklistReference = "", Message = "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for utfallet. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/kodelister/ebyggesak/utfall", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/utfallType/kodebeskrivelse", PreCondition = "/utfallBesvarelse/utfallSvar{0}/utfallType/kodeverdi"}},
            {"10004.4.85.87.181.2", new ValidationRule{ ChecklistReference = "", Message = "SjekkpunktID-en '{0}' er på feil format. Sjekkpunktet skal bestå av siffer og punktum. Du kan finne riktig sjekkpunkt på https://ftb-checklist.dibk.no/", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/utloestFraSjekkpunkt/sjekkpunktId"}},
            {"10004.4.85.65.38.2", new ValidationRule{ ChecklistReference = "", Message = "'{0}' er en ugyldig kodeverdi for tema. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/ebyggesak/tema", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/tema/kodeverdi"}},
            {"10004.4.85.65.38.3", new ValidationRule{ ChecklistReference = "", Message = "Kodeverdien '{0}' ble ikke validert. Temaet kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Messagetype = WARNING, XpathField = "/utfallBesvarelse/utfallSvar{0}/tema/kodeverdi"}},
            {"10004.4.85.65.39.1", new ValidationRule{ ChecklistReference = "", Message = "Når tema er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/ebyggesak/tema", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/tema/kodebeskrivelse", PreCondition = "/utfallBesvarelse/utfallSvar{0}/tema/kodeverdi"}},
            {"10004.4.85.65.39.2", new ValidationRule{ ChecklistReference = "", Message = "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for tema. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/ebyggesak/tema", Messagetype = WARNING, XpathField = "/utfallBesvarelse/utfallSvar{0}/tema/kodebeskrivelse", PreCondition = "/utfallBesvarelse/utfallSvar{0}/tema/kodeverdi"}},
            {"10004.4.85.152.1", new ValidationRule{ ChecklistReference = "", Message = "Du har valgt å besvare '{0}' fra kommunen. Da må du lage en tittel som beskriver det du svarer på.", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/tittel"}},
            {"10004.4.85.183.1", new ValidationRule{ ChecklistReference = "", Message = "Du må svare på om '{0}' skal besvares senere.", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere"}},
            {"10004.4.85.184.1", new ValidationRule{ ChecklistReference = "", Message = "Du har oppgitt at '{0}' ikke skal besvares senere. Da må du svare på om '{0}' allerede er besvart.", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvart", PreCondition = "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere = false & /soeknadGjelder/delsoeknadsnummer > 1"}},
            {"10004.4.85.153.1", new ValidationRule{ ChecklistReference = "", Message = "Du har bedt om å få besvare '{0}' senere. Da må du skrive en kommentar til kommunen.", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/kommentar", PreCondition = "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere"}},
            {"10004.4.85.194.1", new ValidationRule{ ChecklistReference = "", Message = "Du har valgt å besvare '{0}' fra kommunen. Da må du enten skrive en kommentar eller fylle ut informasjon om hvilke vedlegg du skal sende inn.", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/kommentar || vedlegg", PreCondition = "/utfallBesvarelse/utfallSvar{0}/erUtfallBesvaresSenere = false & /utfallBesvarelse/utfallSvar{0}/erUtfallBesvart = false"}},
            {"10004.4.85.89.90.1", new ValidationRule{ ChecklistReference = "", Message = "Du har lagt ved vedlegget '{0}'. Da må du også velge vedleggstype.", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype", PreCondition = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/filnavn"}},
            {"10004.4.85.89.90.38.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi en kodeverdi for vedleggstype. Du kan sjekke gyldige vedlegg på https://register.geonorge.no/kodelister/byggesoknad/vedlegg-og-underskjema/vedlegg", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi", PreCondition = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/filnavn"}},
            {"10004.4.85.89.90.38.2", new ValidationRule{ ChecklistReference = "", Message = "'{0}' er en ugyldig kodeverdi for vedleggstype. Du kan sjekke riktig kodeverdi på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi", PreCondition = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/filnavn"}},
            {"10004.4.85.89.90.38.3", new ValidationRule{ ChecklistReference = "", Message = "Kodeverdien '{0}' ble ikke validert. Vedleggstype kan være riktig, men en teknisk feil gjør at vi ikke kan bekrefte det.", Messagetype = WARNING, XpathField = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi"}},
            {"10004.4.85.89.90.39.1", new ValidationRule{ ChecklistReference = "", Message = "Når vedleggstype er valgt, må kodebeskrivelse fylles ut. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodebeskrivelse", PreCondition = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi"}},
            {"10004.4.85.89.90.39.2", new ValidationRule{ ChecklistReference = "", Message = "Kodebeskrivelsen '{0}' stemmer ikke med den valgte kodeverdien for vedleggstype. Du kan sjekke riktig kodebeskrivelse på https://register.geonorge.no/vedlegg-og-underskjema/vedlegg", Messagetype = WARNING, XpathField = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodebeskrivelse", PreCondition = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi"}},
            {"10004.4.85.89.151.1", new ValidationRule{ ChecklistReference = "", Message = "Du har valgt å legge ved '{0}'. Da må du også fylle ut et filnavn.", Messagetype = ERROR, XpathField = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/filnavn", PreCondition = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype"}},
            {"10004.4.85.89.150.1", new ValidationRule{ ChecklistReference = "", Message = "Du har lagt ved vedlegget '{0}'. Da bør du også fylle ut vedleggets dato.", Messagetype = WARNING, XpathField = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/versjonsdato", PreCondition = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi"}},
            {"10004.4.85.89.149.1", new ValidationRule{ ChecklistReference = "", Message = "Du har lagt ved vedlegget '{0}'. Dersom dette er en ny versjon av et tidligere innsendt dokument, bør du fylle ut tegnings-/versjonsnummer.", Messagetype = WARNING, XpathField = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/versjonsnummer", PreCondition = "/utfallBesvarelse/utfallSvar{0}/vedleggsliste/vedlegg{0}/vedleggstype/kodeverdi"}},
            //DatoFerdigattest
            {"10004.4.190.1", new ValidationRule{ ChecklistReference = "18.23", Message = "Du må oppgi datoen for når du planlegger å søke om ferdigattest.", Messagetype = ERROR, XpathField = "/datoForFerdigattest"}},
            //Gjenstående arbeider
            {"10004.4.97.1", new ValidationRule{ ChecklistReference = "18.24", Message = "Du må oppgi hvilke arbeider som gjenstår.", Messagetype = ERROR, XpathField = "/gjenstaaendeArbeider"}},
            {"10004.4.97.191.1", new ValidationRule{ ChecklistReference = "18.24", Message = "Du må oppgi hvilke arbeider som gjenstår innenfor den delen av tiltaket det søkes midlertidig brukstillatelse for. Arbeidene skal være av mindre vesentlig betydning.", Messagetype = ERROR, XpathField = "/gjenstaaendeArbeider/gjenstaaendeInnenfor"}},
            {"10004.4.97.192.1", new ValidationRule{ ChecklistReference = "", Message = "Du må oppgi de resterende delene av tiltaket, som ikke er inkludert i denne søknaden om midlertidig brukstillatelse.", Messagetype = ERROR, XpathField = "/gjenstaaendeArbeider/gjenstaaendeUtenfor", PreCondition = "/soeknadGjelder/gjelderHeleTiltaket = false"}},
            //Sikkerhetsnivå
            {"10004.4.98.1", new ValidationRule{ ChecklistReference = "18.19", Message = "Du må fylle ut informasjon om sikkerhetsnivået for tiltaket.", Messagetype = ERROR, XpathField = "/sikkerhetsnivaa"}},
            {"10004.4.98.193.1", new ValidationRule{ ChecklistReference = "18.19", Message = "Du må svare på om tiltaket har tilstrekkelig sikkerhetsnivå til å tas i bruk i dag.", Messagetype = ERROR, XpathField = "/sikkerhetsnivaa/harTilstrekkeligSikkerhet"}},
            {"10004.4.98.131.1", new ValidationRule{ ChecklistReference = "18.21", Message = "Du har svart at tiltaket ikke har tilstrekkelig sikkerhetsnivå til å tas i bruk. Da må du oppgi datoen for når arbeidet for å tilfredsstille sikkerhetsnivået skal være ferdigstilt.", Messagetype = ERROR, XpathField = "/sikkerhetsnivaa/utfoertInnen", PreCondition = "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false"}},
            {"10004.4.98.131.2", new ValidationRule{ ChecklistReference = "18.21", Message = "Datoen for når arbeidet for å tilfredsstille sikkerhetsnivået skal være ferdigstilt, må være i løpet av de neste 14 dagene.", Messagetype = ERROR, XpathField = "/sikkerhetsnivaa/utfoertInnen", PreCondition = "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false"}},
            {"10004.4.98.132.1", new ValidationRule{ ChecklistReference = "18.20", Message = "Du må beskrive hva som gjenstår for at tiltaket skal ha tilstrekkelig sikkerhetsnivå til å kunne tas i bruk.", Messagetype = ERROR, XpathField = "/sikkerhetsnivaa/typeArbeider", PreCondition = "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false"}},
            {"10004.4.98.133.1", new ValidationRule{ ChecklistReference = "18.21", Message = "Du må oppgi en dato for når bekreftelsen på at tiltaket har tilfredsstillende sikkerhetsnivå, skal være sendt til kommunen.", Messagetype = ERROR, XpathField = "/sikkerhetsnivaa/bekreftelseInnen", PreCondition = "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false"}},
            {"10004.4.98.133.2", new ValidationRule{ ChecklistReference = "18.21", Message = "Bekreftelsen på at tiltaket har tilfredsstillende sikkerhetsnivå må være sendt til kommunen i løpet av de neste 14 dagene.", Messagetype = ERROR, XpathField = "/sikkerhetsnivaa/bekreftelseInnen", PreCondition = "/sikkerhetsnivaa/harTilstrekkeligSikkerhet = false"}},
            //Vedlegg
            {"10004.4.99.12", new ValidationRule{ ChecklistReference = "", Message = "Du har valgt å besvare '{0}' med vedlegg. Da må du legge ved minst én fil.", Messagetype = ERROR, XpathField = "/vedlegg", PreCondition = "/utfallBesvarelse/utfallSvar{0}/vedlegg"}},
            {"10004.4.99.56.12", new ValidationRule{ ChecklistReference = "17.11", Message = "‘Gjennomføringsplan’ skal følge med søknaden.", Messagetype = ERROR, XpathField = "/vedlegg/Gjennomføringsplan", PreCondition = "/ansvarForByggesaken/kodeverdi = 'medAnsvar' || 'utenAnsvarMedKontroll'"}},
            {"10004.4.99.17.12", new ValidationRule{ ChecklistReference = "17.40", Message = "Du har valgt '{0}'. Da må du legge ved ‘Egenerklæring for selvbygger’ (blankett 5187).", Messagetype = ERROR, XpathField = "/vedlegg/ErklaeringSelvbygger", PreCondition = "/ansvarForByggesaken/kodeverdi = 'selvbygger' && /soeknadGjelder/gjelderHeleTiltaket = 'true'"}},
            //Avsender
            {"10004.4.96.1.2", new ValidationRule{ ChecklistReference = "", Message = "Identiteten (organisasjonsnummeret eller fødselsnummeret) til avsender må være lik identiteten som er oppgitt for tiltakshaver.", Messagetype = ERROR, XpathField = "/avsender/tiltakshaver", PreCondition = "/tiltakshaver/partstype/kodeverdi"}},
            {"10004.4.96.2.2", new ValidationRule{ ChecklistReference = "", Message = "Identiteten (organisasjonsnummeret eller fødselsnummeret) til avsender må være lik identiteten som er oppgitt for ansvarlig søker.", Messagetype = ERROR, XpathField = "/avsender/ansvarligSoeker", PreCondition = "/ansvarligSoeker/partstype/kodeverdi"}},
        };
    }
}
