﻿using Dibk.Ftpb.Validation.Client.HttpClients;
using Dibk.Ftpb.Validation.Client.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Dibk.Ftpb.Validation.Client
{
    public static class ValidationConfig
    {
        /// <summary>
        /// Adds <see cref="ValidationApiService"/> to the service collection
        /// </summary>
        /// <param name="services"></param>
        /// <param name="config"></param>
        public static void AddValidation(this IServiceCollection services, IConfiguration config)
        {
            var validationUri = GetValidationUrl(config);
            services.AddHttpClient<ValidateFormHttpClient>((c) => { c.BaseAddress = validationUri; });
            services.AddHttpClient<ValidateFormReportHttpClient>((c) => { c.BaseAddress = validationUri; });
            services.AddHttpClient<ValidatePlanomraadeHttpClient>((c) => { c.BaseAddress = validationUri; });
            services.AddScoped<IValidationApiService, ValidationApiService>();            
        }

        private static Uri GetValidationUrl(IConfiguration config)
        {
            var validationUrl = config.GetSection("ValidationUrl").Value;

            if (validationUrl == null)
            {
                throw new InvalidOperationException("ValidationUrl is missing in the configuration.");
            }
            return new Uri(validationUrl);
        }
    }
}