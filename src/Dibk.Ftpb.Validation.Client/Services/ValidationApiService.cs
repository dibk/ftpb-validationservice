using Dibk.Ftpb.Validation.Client.HttpClients;
using Dibk.Ftpb.Validation.Client.Models.Request;
using Dibk.Ftpb.Validation.Client.Models.Response;
using Microsoft.AspNetCore.Http;

namespace Dibk.Ftpb.Validation.Client.Services
{
    /// <summary>
    /// Service for validation of form data
    /// </summary>
    public class ValidationApiService : IValidationApiService
    {
        private readonly ValidateFormHttpClient _validateFormHttpClient;
        private readonly ValidateFormReportHttpClient _validateFormReportHttpClient;
        private readonly ValidatePlanomraadeHttpClient _validatePlanomraadeHttpClient;

        /// <summary>
        /// Creates a new instance of the <see cref="ValidationApiService"/>
        /// </summary>
        /// <param name="validateFormHttpClient"></param>
        /// <param name="validateFormReportHttpClient"></param>
        /// <param name="validatePlanomraadeHttpClient"></param>
        public ValidationApiService(ValidateFormHttpClient validateFormHttpClient,
            ValidateFormReportHttpClient validateFormReportHttpClient,
            ValidatePlanomraadeHttpClient validatePlanomraadeHttpClient)
        {
            _validateFormHttpClient = validateFormHttpClient;
            _validateFormReportHttpClient = validateFormReportHttpClient;
            _validatePlanomraadeHttpClient = validatePlanomraadeHttpClient;
        }

        /// <inheritdoc/>
        public async Task<ValidationResponse?> ValidateAsync(ValidationRequest inputData)
        {
            return await ValidateAsync(inputData, CancellationToken.None);
        }

        /// <inheritdoc/>
        public async Task<ValidationResponse?> ValidateAsync(ValidationRequest inputData, CancellationToken cancellationToken)
        {
            return await _validateFormHttpClient.ValidateAsync(inputData, cancellationToken);
        }

        /// <inheritdoc/>
        public async Task<ValidationReportResponse?> ValidateReportAsync(ValidationRequest inputData)
        {
            return await ValidateReportAsync(inputData, CancellationToken.None);
        }

        /// <inheritdoc/>
        public async Task<ValidationReportResponse?> ValidateReportAsync(ValidationRequest inputData, CancellationToken cancellationToken)
        {
            return await _validateFormReportHttpClient.ValidateAsync(inputData, CancellationToken.None);
        }

        /// <inheritdoc/>
        public async Task<ValidationResponse?> ValidatePlanomraade(IFormFile file)
        {
            return await ValidatePlanomraade(file, CancellationToken.None);
        }

        /// <inheritdoc/>
        public async Task<ValidationResponse?> ValidatePlanomraade(IFormFile file, CancellationToken cancellationToken)
        {
            return await _validatePlanomraadeHttpClient.ValidateAsync(file, CancellationToken.None);
        }
    }
}