﻿using Dibk.Ftpb.Validation.Client.Models.Request;
using Dibk.Ftpb.Validation.Client.Models.Response;
using Microsoft.AspNetCore.Http;

namespace Dibk.Ftpb.Validation.Client.Services
{
    public interface IValidationApiService
    {
        /// <summary>
        /// Validerer innsendt skjema
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        Task<ValidationResponse?> ValidateAsync(ValidationRequest inputData);

        /// <summary>
        /// Validerer innsendt skjema
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ValidationResponse?> ValidateAsync(ValidationRequest inputData, CancellationToken cancellationToken);

        /// <summary>
        /// Validerer innsendt skjema og fyller ut preutfylt sjekkliste
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        Task<ValidationReportResponse?> ValidateReportAsync(ValidationRequest inputData);

        /// <summary>
        /// Validerer innsendt skjema og fyller ut preutfylt sjekkliste
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ValidationReportResponse?> ValidateReportAsync(ValidationRequest inputData, CancellationToken cancellationToken);

        /// <summary>
        /// GML validering av planområde
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        Task<ValidationResponse?> ValidatePlanomraade(IFormFile file);

        /// <summary>
        /// GML validering av planområde
        /// </summary>
        /// <param name="file"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ValidationResponse?> ValidatePlanomraade(IFormFile file, CancellationToken cancellationToken);
    }
}