﻿namespace Dibk.Ftpb.Validation.Client.Models
{
    /// <summary>
    /// Alvorlighetsgrad for valideringsfeil
    /// </summary>
    public enum ValidationResponseSeverityEnum
    {
        /// <summary>
        /// Feil
        /// </summary>
        ERROR,
        /// <summary>
        /// Advarsel
        /// </summary>
        WARNING
    }
}