﻿using System.ComponentModel.DataAnnotations;

namespace Dibk.Ftpb.Validation.Client.Models.Request
{
    public class SubFormValidationInfo
    {
        /// <summary>
        /// Subform name
        /// </summary>
        [Required(ErrorMessage = "Underskjemaet må ha et navn")]
        public string FormName { get; set; }

        /// <summary>
        /// Subform data in XML format
        /// </summary>
        [Required(ErrorMessage = "Underskjemaet må inneholde data i XML-format")]
        public string SubFormData { get; set; }
    }
}