using System.ComponentModel.DataAnnotations;

namespace Dibk.Ftpb.Validation.Client.Models.Request
{
    public class ValidationRequest
    {
        /// <summary>
        /// Submitter of the form. This is the person that is authenticated and submitting the form. This is used to validate that the authenticated user is the same as the submitter of the form.
        /// </summary>
        public string? AuthenticatedSubmitter { get; set; }

        /// <summary>
        /// Form data in XML format
        /// </summary>
        [Required(ErrorMessage = "FormData må inneholde XML skjema data")]
        public string FormData { get; set; }

        /// <summary>
        /// List of all subforms name used to validate required documentation. V2 enables more detailed validation of altinn rules for each attachement. Size, file extension and count of attachmenttypes.
        /// </summary>
        public SubFormValidationInfo[]? SubForms { get; set; }

        /// <summary>
        /// List of all attachment types and forms/subforms name used to validate required documentation. V2 enables more detailed validation of altinn rules for each attachement. Size, file extension and count of attachmenttypes.
        /// </summary>
        public AttachmentValidationInfo[]? Attachments { get; set; }
    }
}