﻿using System.ComponentModel.DataAnnotations;

namespace Dibk.Ftpb.Validation.Client.Models.Request
{
    public class AttachmentValidationInfo
    {
        /// <summary>
        /// Name is attachmentType for attachment and form name for form and subforms
        /// </summary>
        [Required(ErrorMessage = "Vedlegget må ha et navn")]
        public string AttachmentTypeName { get; set; }

        /// <summary>
        /// Filename with extension
        /// </summary>
        public string? Filename { get; set; } 

        /// <summary>
        /// Filesize in bytes
        /// </summary>
        public int? FileSize { get; set; }
    }
}