﻿using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Validation.Client.Models.Response
{
    [XmlRoot("ValidationResult")]
    public class ValidationResponse
    {
        [JsonPropertyOrder(1)]
        public int Errors { get; set; }

        [JsonPropertyOrder(2)]
        public int Warnings { get; set; }

        [JsonPropertyOrder(3)]
        public List<string> TiltakstyperISoeknad { get; set; }

        [JsonPropertyOrder(4)]
        public string Soknadtype { get; set; }

        [XmlArray("messages", IsNullable = false)]
        [XmlArrayItem("ValidationMessage")]
        [JsonPropertyOrder(5)]
        public List<ValidationResultMessage> Messages { get; set; }

        [XmlArray("rulesChecked", IsNullable = false)]
        [XmlArrayItem("ValidationRule")]
        [JsonPropertyOrder(6)]
        public List<ValidationRuleResult> RulesChecked { get; set; }

        public string GetValidationMessages()
        {
            if (Messages != null)
                return string.Join(" ", Messages.Select(x => x.Messagetype.ToString() + ": " + x.Message + ": " + x.Reference).ToList());
            else
                return string.Empty;
        }
    }
}