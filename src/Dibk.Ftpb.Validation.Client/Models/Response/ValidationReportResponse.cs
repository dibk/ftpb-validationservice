﻿using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Validation.Client.Models.Response
{
    public class ValidationReportResponse : ValidationResponse
    {
        [XmlArray("prefillChecklist", IsNullable = false)]
        [XmlArrayItem("ChecklistAnswer")]
        [JsonPropertyOrder(7)]
        public List<ChecklistAnswerResult> PrefillChecklist { get; set; }
    }
}