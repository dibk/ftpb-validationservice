﻿using System.Xml.Serialization;

namespace Dibk.Ftpb.Validation.Client.Models.Response
{
    public class ChecklistAnswerResult
    {
        [XmlElement("checklistReference")]
        public string ChecklistReference { get; set; }

        [XmlElement("checklistQuestion")]
        public string ChecklistQuestion { get; set; }

        [XmlElement("yesNo")]
        public bool YesNo { get; set; }

        [XmlArrayItem("validationRuleId")]
        [XmlArray("supportingDataValidationRuleId", IsNullable = false)]
        public List<string> SupportingDataValidationRuleId { get; set; }

        [XmlArrayItem("xpathField")]
        [XmlArray("supportingDataXpathField", IsNullable = false)]
        public List<string> SupportingDataXpathField { get; set; }

        [XmlElement("documentation")]
        public string Documentation { get; set; }
    }
}