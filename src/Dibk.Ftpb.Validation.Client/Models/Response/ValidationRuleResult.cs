using System.Xml.Serialization;

namespace Dibk.Ftpb.Validation.Client.Models.Response
{
    public class ValidationRuleResult
    {
        [XmlElement("id")]
        public string Id { get; set; }

        [XmlElement("rule")]
        public string Rule { get; set; }

        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// The type of message, e.g. Error, Warning, Info
        /// </summary>
        [XmlElement("messagetype")]
        public ValidationResponseSeverityEnum Messagetype { get; set; }

        [XmlElement("xpathField")]
        public string XpathField { get; set; }

        [XmlElement("preCondition")]
        public string PreCondition { get; set; }

        public string ChecklistReference { get; set; }
    }
}