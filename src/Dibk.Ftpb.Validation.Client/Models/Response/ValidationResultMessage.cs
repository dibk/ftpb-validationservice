using System.Xml.Serialization;

namespace Dibk.Ftpb.Validation.Client.Models.Response
{
    public class ValidationResultMessage
    {
        [XmlElement("rule")]
        public string Rule { get; set; }

        [XmlElement("reference")]
        public string Reference { get; set; }

        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// The type of message, e.g. Error, Warning, Info
        /// </summary>
        [XmlElement("messagetype")]
        public ValidationResponseSeverityEnum Messagetype { get; set; }

        [XmlElement("xpathField")]
        public string XpathField { get; set; }

        [XmlElement("preCondition")]
        public string PreCondition { get; set; }

        [XmlElement("checklistReference")]
        public string ChecklistReference { get; set; }
    }
}