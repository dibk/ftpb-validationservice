﻿using System.Security.Cryptography;
using System.Text;

namespace Dibk.Ftpb.Validation.Client.Utils
{
    /// <summary>
    /// Encrypts text using RSA
    /// </summary>
    public class RsaEncryptor
    {
        /// <summary>
        /// Encrypts text using RSA
        /// </summary>
        /// <param name="privateKey"></param>
        /// <param name="plainText"></param>
        /// <param name="encrypted"></param>
        /// <returns></returns>
        public static bool TryEncryptText(string privateKey, string plainText, out string encrypted)
        {
            var rsa = RSA.Create();

            try
            {
                // Konverterer privat nøkkel fra Base64 til byte-array
                var privateKeyBytes = Convert.FromBase64String(privateKey);

                // Importerer privat nøkkel
                rsa.ImportPkcs8PrivateKey(privateKeyBytes, out _);

                var byteConverter = new UnicodeEncoding();
                var clearTextBytes = byteConverter.GetBytes(plainText);
                var cipherBytes = rsa.Encrypt(clearTextBytes, RSAEncryptionPadding.Pkcs1);
                encrypted = Convert.ToBase64String(cipherBytes);
                rsa.Dispose();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                encrypted = null;
                rsa.Dispose();

                return false;
            }
        }
    }
}