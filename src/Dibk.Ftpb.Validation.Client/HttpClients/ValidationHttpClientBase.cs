﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;

namespace Dibk.Ftpb.Validation.Client.HttpClients
{
    public class ValidationHttpClientBase<TInput, TOutput>
            where TInput : class
            where TOutput : class
    {
        private readonly HttpClient _httpClient;
        private readonly string _requestUri;
        private readonly ILogger _logger;

        public ValidationHttpClientBase(HttpClient httpClient, string requestUri, ILogger logger)
        {
            _httpClient = httpClient;
            _requestUri = requestUri;
            _logger = logger;
        }

        public async Task<TOutput?> ValidateAsync(TInput input, CancellationToken cancellationToken)
        {
            var content = JObject.FromObject(input).ToString();

            var request = new HttpRequestMessage(HttpMethod.Post, _requestUri)
            { Content = new StringContent(content, Encoding.UTF8, "application/json") };

            var response = await _httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);
            try
            {
                response.EnsureSuccessStatusCode();
                var jsonContent = await response.Content.ReadAsStringAsync(cancellationToken).ConfigureAwait(false);
                return JsonConvert.DeserializeObject<TOutput>(jsonContent);
            }
            catch (Exception ex)
            {
                string contentAsString = await response.Content.ReadAsStringAsync(cancellationToken);
                _logger.LogError(ex, "Error occured when validating. Http response content:\n" + contentAsString);
                return null;
            }
        }

        public async Task<TOutput?> ValidateAsync(IFormFile file, CancellationToken cancellationToken)
        {
            // Send file med i request
            var content = new MultipartFormDataContent
            {
                { new StreamContent(file.OpenReadStream()), "file", file.FileName }
            };

            var request = new HttpRequestMessage(HttpMethod.Post, _requestUri)
            {
                Content = content
            };

            var response = await _httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);
            try
            {
                response.EnsureSuccessStatusCode();
                var jsonContent = await response.Content.ReadAsStringAsync(cancellationToken).ConfigureAwait(false);
                return JsonConvert.DeserializeObject<TOutput>(jsonContent);
            }
            catch (Exception ex)
            {
                string contentAsString = await response.Content.ReadAsStringAsync(cancellationToken);
                _logger.LogError(ex, "Error occured when validating. Http response content:\n" + contentAsString);
                return null;
            }
        }
    }
}