using Dibk.Ftpb.Validation.Client.Models.Request;
using Dibk.Ftpb.Validation.Client.Models.Response;
using Microsoft.Extensions.Logging;

namespace Dibk.Ftpb.Validation.Client.HttpClients
{
    public class ValidateFormHttpClient : ValidationHttpClientBase<ValidationRequest, ValidationResponse>
    {
        public ValidateFormHttpClient(HttpClient httpClient,
            ILogger<ValidateFormHttpClient> logger) : base(
            httpClient,
            "api/validation",
            logger)
        { }
    }

    public class ValidateFormReportHttpClient : ValidationHttpClientBase<ValidationRequest, ValidationReportResponse>
    {
        public ValidateFormReportHttpClient(HttpClient httpClient,
            ILogger<ValidateFormHttpClient> logger) : base(
            httpClient,
            "api/validationReport",
            logger)
        { }
    }

    public class ValidatePlanomraadeHttpClient : ValidationHttpClientBase<ValidationRequest, ValidationResponse>
    {
        public ValidatePlanomraadeHttpClient(HttpClient httpClient,
            ILogger<ValidateFormHttpClient> logger) : base(
            httpClient,
            "api/validation/file/planomraade",
            logger)
        { }
    }
}