﻿# Valideringsklient for DiBK Fellestjenester Plan og Bygg

Denne klienter er et integrasjonsbibliotek for å sende valideringsforespørsler til DiBKs Valideringstjeneste. 
Api som støttes av klienten er:
* api/validation - normal validering
* api/validationReport - validering med komplett rapport og preutfylt sjekkliste
* api/validation/file/planomraade - validering av planområde for plantjenster

## Konfigurasjon
Tilgjengeliggjøring av valideringsklienten i en applikasjon krever at følgende konfigurasjon er satt opp:
I program.cs:
```csharp
services.AddValidation(config);
```

URL til valideringstjenesten settes i config. Feks: appsettings.json:
```json
"ValidationUrl": "https://validering.ft.dibk.no"
```