﻿using Elastic.Apm.SerilogEnricher;
using Elastic.CommonSchema.Serilog;
using Elastic.Serilog.Sinks;
using Elastic.Transport;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using System;

namespace Dibk.Ftpb.Validation
{
    internal static class SerilogConfiguration
    {
        public static LoggerConfiguration ConfigureLogging(IConfiguration configuration)
        {
            var elasticSearchUrl = configuration.GetValue<string>("Serilog:ConnectionUrl");
            var elasticUsername = configuration.GetValue<string>("Serilog:Username");
            var elasticPassword = configuration.GetValue<string>("Serilog:Password");
            var elasticIndexFormat = configuration.GetValue<string>("Serilog:IndexFormat");

            var loggerConfig = new LoggerConfiguration()
                .MinimumLevel.Is(LogEventLevel.Debug)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .MinimumLevel.Override("Elastic.Apm", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .Enrich.WithCorrelationIdHeader()
                .Enrich.WithEcsHttpContext(configuration.Get<HttpContextAccessor>())
                .Enrich.WithElasticApmCorrelationInfo()
                .WriteTo.Console(outputTemplate: "{Timestamp:HH:mm:ss.fff} [{Level}] {Scope} {SourceContext} {Message}{NewLine}{Exception}");

            if (!string.IsNullOrEmpty(elasticSearchUrl))
                loggerConfig.WriteTo.Elasticsearch(new Uri[] { new Uri(elasticSearchUrl) },
                                   opts => { opts.DataStream = new Elastic.Ingest.Elasticsearch.DataStreams.DataStreamName(elasticIndexFormat); },
                                   tr => { tr.Authentication(new BasicAuthentication(elasticUsername, elasticPassword)); });
            else
                Console.WriteLine("ERROR IN SERILOG CONFIGURATION - Unable to register elastic sink. URL is missing in config");

            return loggerConfig;
        }
    }
}