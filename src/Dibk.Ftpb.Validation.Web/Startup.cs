using Dibk.Ftpb.Validation.Application.DataSources;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Altinn;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.AtilFee;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Checklist;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Municipality;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Validation;
using Dibk.Ftpb.Validation.Application.DataSources.Models;
using Dibk.Ftpb.Validation.Application.Encryption;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators.Ansako;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Reporter.DataBase;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Services.Xsd;
using Dibk.Ftpb.Validation.Web;
using Dibk.Ftpb.Validation.Web.Configuration;
using Dibk.Ftpb.Validation.XmlSchemaValidator.Config;
using DiBK.RuleValidator.Config;
using Elastic.Apm.NetCoreAll;
using MatrikkelWSClient;
using MaxRev.Gdal.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.FeatureManagement;
using Microsoft.FeatureManagement.FeatureFilters;
using Microsoft.OpenApi.Models;
using OSGeo.OGR;
using Serilog;
using Serilog.Debugging;
using System;
using System.Text.Json.Serialization;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators.FellestjenesterBygg;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators.FellestjenesterPlan;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators.Atil;
using Dibk.Ftpb.Validation.Web.Mapping.Request;
using Dibk.Ftpb.Validation.Application.Logic.EntityValidators;

namespace Dibk.Ftpb.Validation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddFeatureManagement().AddFeatureFilter<TimeWindowFilter>();

            services.AddControllers();
            services.AddHttpContextAccessor();
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Dibk.Ftpb.Api.Validation", Version = "v1" });
            });

            services.AddMvc().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = System.Text.Json.JsonNamingPolicy.CamelCase;
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });

            services.AddHttpLogging(o => o = new Microsoft.AspNetCore.HttpLogging.HttpLoggingOptions());
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddSerilog(SerilogConfiguration.ConfigureLogging(Configuration).CreateLogger());
            });

            Log.Logger = SerilogConfiguration.ConfigureLogging(Configuration).CreateLogger();

            if (bool.TryParse(Configuration["Serilog:SelfLogEnabled"], out var selflogEnabled))
                if (selflogEnabled)
                {
                    SelfLog.Enable(msg => Console.WriteLine(msg));
                }

            services.AddMemoryCache();
            services.AddOptions();
            services.AddHealthChecks();
            services.AddAutoMapper(typeof(ValidationRequestProfile).Assembly);

            // Documentation
            services.Configure<ValidationSettings>(Configuration.GetSection("ValidationApi"));
            services.AddTransient<IRuleDocumentationProvider, RuleDocumentationProvider>();

            services.AddHttpClient<MunicipalityApiHttpClient>();
            services.AddTransient<IMunicipalityApiService, MunicipalityApiService>();
            services.Configure<MunicipalityApiSettings>(Configuration.GetSection("MunicipalityApi"));
            services.Configure<EncryptionSettings>(Configuration.GetSection("EncryptionSettings"));

            //Codelist
            services.AddTransient<CodeListApiService>();
            services.AddTransient<ICodeListService, CodeListService>();
            services.Configure<CodeListApiSettings>(Configuration.GetSection("CodeListApi"));
            services.AddHostedService<RefreshCodeListCacheBackgroundService>();

            //CheckList
            services.AddHttpClient(ChecklistConstants.DibkClientName, configureClient => { configureClient.BaseAddress = new Uri(Configuration["CheckListApi:DibkUrl"]); });
            services.AddHttpClient(ChecklistConstants.AtilClientName, configureClient => { configureClient.BaseAddress = new Uri(Configuration["CheckListApi:AtilUrl"]); });
            services.AddTransient<ChecklistApiService>();
            services.Configure<ChecklistSettings>(Configuration.GetSection("CheckListApi"));
            services.AddTransient<IChecklistService, ChecklistService>();
            services.AddHostedService<RefreshChecklistCacheBackgroundService>();

            //PostalCode cache
            services.AddHttpClient<PostalCodeHttpClient>(config => { config.BaseAddress = new Uri(Configuration["PostalCodeApi:PostalCodeFileUrl"]); });
            services.AddSingleton<IPostalCodeService, PostalCodeService>() ;
            services.AddHostedService<RefreshPostalCodeCacheBackgroundService>();

            //AtilFeeCalculator
            services.AddHttpClient<AtilFeeCalculatorApiHttpClient>();
            services.AddTransient<IAtilFeeCalculatorService, AtilFeeCalculatorService>();
            services.Configure<AtilFeeCalculatorSettings>(Configuration.GetSection("AtilFeeCalculatorApi"));

            services.AddTransient<IValidationService, ValidationService>();
            services.AddTransient<IInputDataService, InputDataService>();
            services.AddTransient<IValidationHandler, ValidationHandler>();
            services.AddTransient<IValidationMessageComposer, ValidationMessageComposer>();

            services.AddScoped<IValidationMessageRepository, ValidationMessageRepository>();
            services.AddScoped<IValidationMessageDb, ValidationMessageDb>();

            services.AddScoped<IDecryption, Decryption>();
            services.AddScoped<IDecryptionFactory, DecryptionFactory>();

            services.AddScoped<ArbeidstilsynetsSamtykke2_45957_Validator>();
            services.AddScoped<ArbeidstilsynetsSamtykke3_12000_3_Validator>();
            services.AddScoped<SuppleringArbeidstilsynet_7086_47365_Validator>();
            services.AddScoped<AnsvarsrettAnsako_10000_Validator>();
            services.AddScoped<SamsvarserklaeringAnsako_10001_1_Validator>();
            services.AddScoped<KontrollerklaeringAnsako_10002_1_Validator>();
            services.AddScoped<Avfallsplan_7063_47177_Validator>();
            services.AddScoped<Planvarsel_11000_2_Validator>();
            services.AddScoped<Igangsettingstillatelse_10003_4_Validator>();
            services.AddScoped<Midlertidigbrukstillatelse_10004_4_Validator>();
            services.AddScoped<Gjennomføringsplan_6146_44096_Validator>();
            services.AddScoped<Ferdigattest_10005_4_Validator>();

            //Matrikkel
            services.AddMatrikkelClient(Configuration);

            //Altinn
            services.AddHttpClient<AltinnHttpClient>();
            services.AddTransient<IAltinnMetadataServices, AltinnMetadataServices>();
            services.Configure<AltinnSettings>(Configuration.GetSection("Altinn"));

            //XSD
            services.AddXmlSchemaValidator();
            services.AddTransient<IXsdValidationService, XsdValidationService>();

            //GML
            services.AddRuleValidator(settings =>
            {
                settings.AddRuleAssembly("Dibk.Ftpb.Validation.Application");
                settings.AddRuleAssembly("DiBK.RuleValidator.Rules.Gml");
                settings.MaxMessageCount = 500;
            });

            services.AddValidators();
            services.AddGmlValidationServices();

            services.AddAllElasticApm();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            using(var scope = app.ApplicationServices.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;
                var featureManager = serviceProvider.GetRequiredService<IFeatureManager>();
                AtilFeature.Instance.LoadFeature(featureManager);
            }

            GdalBase.ConfigureAll();
            Ogr.UseExceptions();

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseXmlSchemaValidator();

            app.UserCorrelationIdMiddleware();
            app.UseSerilogRequestLogging(opts =>
            {
                opts.EnrichDiagnosticContext = (diagnosticContext, httpContext) =>
                {
                    diagnosticContext.Set("UrlQuery", httpContext.Request.QueryString.Value?.Trim('?'));
                };
            });

            app.UseHttpLogging();

            app.UseCors(c => { c.AllowAnyMethod(); c.AllowAnyOrigin(); c.AllowAnyHeader(); c.WithExposedHeaders("Content-Disposition"); });

            app.UseSwagger();
            app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v1/swagger.json", "Dibk.Ftpb.Api.Validation v1"));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
            });
        }
    }
}