﻿using Dibk.Ftpb.Validation.Application.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NCrontab;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Web
{
    public class RefreshChecklistCacheBackgroundService : BackgroundService
    {
        private readonly ILogger<RefreshChecklistCacheBackgroundService> _logger;
        private readonly IChecklistService _checklistService;
        private readonly IConfiguration _configuration;
        private CrontabSchedule _schedule;
        private DateTime _nextRun;
        private string _cronExpression = "*/30 * * * *"; //Default verdi hver halvtime

        public RefreshChecklistCacheBackgroundService(ILogger<RefreshChecklistCacheBackgroundService> logger, IChecklistService checklistService, IConfiguration configuration)
        {
            _logger = logger;
            _checklistService = checklistService;
            _configuration = configuration;
            ConfigureCronJob();
        }

        private void ConfigureCronJob()
        {
            _logger.LogInformation("Configures cron expression for RefreshCacheChecklistBackgroundService. Loads value from config 'CacheService:ScheduleCronExpression'..");
            var configuredCronExpression = _configuration["CacheService:ChecklistScheduleCronExpression"];
            if (!string.IsNullOrEmpty(configuredCronExpression))
            {
                try
                {
                    var schedule = CrontabSchedule.TryParse(configuredCronExpression);
                    _cronExpression = configuredCronExpression;
                    _schedule = schedule;
                }
                catch (CrontabException ce)
                {
                    _logger.LogError(ce, "Invalid cron expression configured in config 'CacheService:ScheduleCronExpression': {CronExpression}", configuredCronExpression);
                }
            }
            else
            {
                _logger.LogInformation("Cron expression wasn't configured, uses default {CronExpression}", _cronExpression);
                _schedule = CrontabSchedule.Parse(_cronExpression);
            }
        }

        private List<(Application.Enums.ServiceDomain domain, string processCategory)> processCategories = new List<(Application.Enums.ServiceDomain, string)>
        {
            {(Application.Enums.ServiceDomain.FTB, "IG") },
            {(Application.Enums.ServiceDomain.FTB, "MB") },
            {(Application.Enums.ServiceDomain.FTB, "FA") },
            {(Application.Enums.ServiceDomain.ATIL, "AT" )}

            //Disse treng ikkje cachast før valideringer blir implementert her
            //{(Application.Enums.ServiceDomain.FTB, "RS")},
            //{(Application.Enums.ServiceDomain.FTB, "ET") },
            //{(Application.Enums.ServiceDomain.FTB, "TA") },            
            //{(Application.Enums.ServiceDomain.FTB, "ES") }
            
        };

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("RefreshCacheChecklistBackgroundService starts..");

            //UpdateExecutionTime();

            _logger.LogInformation("Waits for next run..");

            do
            {
                if (DateTime.Now > _nextRun)
                {
                    foreach (var (domain, processCategory) in processCategories)
                    {
                        try
                        {
                            await _checklistService.RefreshCachedChecklist(processCategory, domain);
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "Error refreshing cached checklist for processCategory {ProcessCategory} and domain {Domain}", processCategory, domain);
                        }                        
                    }

                    await _checklistService.RefreshCachedChecklist("AT", Application.Enums.ServiceDomain.ATIL, "metadataid=1");

                    UpdateExecutionTime();
                }
                await Task.Delay(5000, stoppingToken); //5 seconds delay
            }
            while (!stoppingToken.IsCancellationRequested);
        }

        private void UpdateExecutionTime()
        {
            _nextRun = _schedule.GetNextOccurrence(DateTime.Now);
            _logger.LogInformation("Next run is scheduled at {NextRun}", _nextRun.ToString());
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("RefreshCacheChecklistBackgroundService is stopping..");
            return base.StopAsync(cancellationToken);
        }
    }
}