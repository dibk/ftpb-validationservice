﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.CodeList;
using Dibk.Ftpb.Validation.Application.Enums;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NCrontab;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Web
{
    public class RefreshCodeListCacheBackgroundService : BackgroundService
    {
        private readonly ILogger<RefreshChecklistCacheBackgroundService> _logger;
        private readonly ICodeListService _codeListService;
        private readonly IConfiguration _configuration;
        private CrontabSchedule _schedule;
        private DateTime _nextRun;
        private string _cronExpression = "*/30 * * * *"; //Default verdi hver halvtime

        public RefreshCodeListCacheBackgroundService(ILogger<RefreshChecklistCacheBackgroundService> logger, ICodeListService codeListService, IConfiguration configuration)
        {
            _logger = logger;
            _codeListService = codeListService;
            _configuration = configuration;
            ConfigureCronJob();
        }

        private void ConfigureCronJob()
        {
            _logger.LogInformation("Configures cron expression for RefreshCodeListCacheBackgroundService. Loads value from config 'CacheService:CodeListScheduleCronExpression'..");
            var configuredCronExpression = _configuration["CacheService:CodeListScheduleCronExpression"];
            if (!string.IsNullOrEmpty(configuredCronExpression))
            {
                try
                {
                    var schedule = CrontabSchedule.TryParse(configuredCronExpression);
                    _cronExpression = configuredCronExpression;
                    _schedule = schedule;
                }
                catch (CrontabException ce)
                {
                    _logger.LogError(ce, "Invalid cron expression configured in config 'CacheService:CodeListScheduleCronExpression': {CronExpression}", configuredCronExpression);
                }
            }
            else
            {
                _logger.LogInformation("Cron expression wasn't configured, uses default {CronExpression}", _cronExpression);
                _schedule = CrontabSchedule.Parse(_cronExpression);
            }
        }

        private List<(Enum codeList, RegistryType registryType)> codeLists = new List<(Enum, RegistryType)>
        {
            {(FtbKodeListeEnum.Anleggstype, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.AnsvarForByggesaken, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.Avfalldisponeringsmåte, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.Avfallfraksjoner_NS, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.Avfallklasse, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.Bygningstype, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.foretrukketspraak, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.funksjon, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.Naeringsgruppe, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.Partstype, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.tiltaksformal, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.tiltaksklasse, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.tiltaktype, RegistryType.Byggesoknad) },
            {(FtbKodeListeEnum.tiltakstyperUtenAnsvarsrett, RegistryType.Byggesoknad) },
            
            {(FtbKodeListeEnum.Vedlegg, RegistryType.Byggesoknad) },

            {(FtbKodeListeEnum.Mangeltekst, RegistryType.EByggesak) },
            {(FtbKodeListeEnum.Tema, RegistryType.EByggesak) },
            {(FtbKodeListeEnum.utfall, RegistryType.EByggesak) },

            {(FtbKodeListeEnum.ArbeidstilsynetVedlegg, RegistryType.Arbeidstilsynet) },
            {(FtbKodeListeEnum.tiltakstyper_for_arbeidstilsynet, RegistryType.Arbeidstilsynet) },
            {(ArbeidstilsynetKodeListeEnum.Arbeidstilsynets_krav, RegistryType.Arbeidstilsynet) },

            {(PlansakKodelisteEnum.plantype , RegistryType.plansak) },

            {(SosiKodelisterEnum.kommunenummer, RegistryType.SosiKodelister) },
        };

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("RefreshCodeListCacheBackgroundService starts..");

            //UpdateExecutionTime();

            _logger.LogInformation("Waits for next run..");

            do
            {
                if (DateTime.Now > _nextRun)
                {
                    foreach (var (codeList, registryType) in codeLists)
                    {
                        try
                        {
                            await _codeListService.RefreshCodeListCache(codeList, registryType);
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "Error refreshing cached codeList for codeList {CodeList} and registryType {RegistryType}", codeList, registryType);
                        }
                    }

                    UpdateExecutionTime();
                }
                await Task.Delay(5000, stoppingToken); //5 seconds delay
            }
            while (!stoppingToken.IsCancellationRequested);
        }

        private void UpdateExecutionTime()
        {
            _nextRun = _schedule.GetNextOccurrence(DateTime.Now);
            _logger.LogInformation("Next run is scheduled at {NextRun}", _nextRun.ToString());
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("RefreshCodeListCacheBackgroundService is stopping..");
            return base.StopAsync(cancellationToken);
        }
    }
}