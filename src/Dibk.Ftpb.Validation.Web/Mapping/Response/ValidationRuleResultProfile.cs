﻿using AutoMapper;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Client.Models.Response;

namespace Dibk.Ftpb.Validation.Web.Mapping.Response
{
    public class ValidationRuleResultProfile : Profile
    {
        public ValidationRuleResultProfile()
        {
            CreateMap<ValidationRule, ValidationRuleResult>();
        }
    }
}