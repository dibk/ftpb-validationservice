﻿using AutoMapper;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Client.Models.Response;

namespace Dibk.Ftpb.Validation.Web.Mapping.Response
{
    public class ChecklistAnswerResultProfile : Profile
    {
        public ChecklistAnswerResultProfile()
        {
            CreateMap<ChecklistAnswer, ChecklistAnswerResult>();
        }
    }
}
