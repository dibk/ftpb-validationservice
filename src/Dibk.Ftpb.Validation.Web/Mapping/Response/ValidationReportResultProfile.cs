﻿using AutoMapper;
using Dibk.Ftpb.Validation.Client.Models.Response;

namespace Dibk.Ftpb.Validation.Web.Mapping.Response
{
    public class ValidationReportResultProfile : Profile
    {
        public ValidationReportResultProfile()
        {
            CreateMap<Application.Reporter.ValidationResult, ValidationReportResponse>();
        }
    }
}