﻿using AutoMapper;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Client.Models.Request;

namespace Dibk.Ftpb.Validation.Web.Mapping.Request
{
    public class SubFormValidationInfoProfile : Profile
    {
        public SubFormValidationInfoProfile()
        {
            CreateMap<SubFormValidationInfo, SubFormInfo>();
        }
    }
}
