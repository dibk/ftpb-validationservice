﻿using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.PostalCode;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NCrontab;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Validation.Web
{
    public class RefreshPostalCodeCacheBackgroundService : BackgroundService
    {
        private readonly ILogger<RefreshPostalCodeCacheBackgroundService> _logger;
        private readonly IPostalCodeService _postalCodeService;
        private readonly IConfiguration _configuration;
        private CrontabSchedule _schedule;
        private DateTime _nextRun;
        private string _cronExpression = "0 1 * * *"; //Default verdi 01:00:00 hver natt

        public RefreshPostalCodeCacheBackgroundService(ILogger<RefreshPostalCodeCacheBackgroundService> logger, IPostalCodeService postalCodeService, IConfiguration configuration)
        {
            _logger = logger;
            _postalCodeService = postalCodeService;
            _configuration = configuration;
            ConfigureCronJob();
        }

        private void ConfigureCronJob()
        {
            _logger.LogInformation("Configures cron expression for RefreshCacheBackgroundService. Loads value from config 'CacheService:ScheduleCronExpression'..");
            var configuredCronExpression = _configuration["CacheService:PostalCodeScheduleCronExpression"];
            if (!string.IsNullOrEmpty(configuredCronExpression))
            {
                try
                {
                    var schedule = CrontabSchedule.TryParse(configuredCronExpression);
                    _cronExpression = configuredCronExpression;
                    _schedule = schedule;
                }
                catch (CrontabException ce)
                {
                    _logger.LogError(ce, "Invalid cron expression configured in config 'CacheService:ScheduleCronExpression': {CronExpression}", configuredCronExpression);
                }
            }
            else
            {
                _logger.LogInformation("Cron expression wasn't configured, uses default {CronExpression}", _cronExpression);
                _schedule = CrontabSchedule.Parse(_cronExpression);
            }
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("RefreshCacheBackgroundService starts..");

            //UpdateExecutionTime();

            _logger.LogInformation("Waits for next run..");

            do
            {
                if (DateTime.Now > _nextRun)
                {
                    await _postalCodeService.LoadPostalCodesIntoCacheAsync();

                    UpdateExecutionTime();
                }
                await Task.Delay(5000, stoppingToken); //5 seconds delay
            }
            while (!stoppingToken.IsCancellationRequested);
        }

        private void UpdateExecutionTime()
        {
            _nextRun = _schedule.GetNextOccurrence(DateTime.Now);
            _logger.LogInformation("Next run is scheduled at {NextRun}", _nextRun.ToString());
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("RefreshCacheBackgroundService is stopping..");
            return base.StopAsync(cancellationToken);
        }
    }
}