﻿using Dibk.Ftpb.Validation.Application.Services.Gml.Generic;
using Dibk.Ftpb.Validation.Application.Services.Gml.Planområde;
using Microsoft.Extensions.DependencyInjection;

namespace Dibk.Ftpb.Validation.Web.Configuration
{
    public static class GmlValidationServiceConfig
    {
        public static void AddGmlValidationServices(this IServiceCollection services)
        {
            services.AddTransient<IGmlValidationService, GmlValidationService>();
            services.AddTransient<IPlanområdeValidationService, PlanområdeValidationService>();
        }
    }
}
