﻿using Dibk.Ftpb.Validation.Web.Configuration;
using Dibk.Ftpb.Validation.XmlSchemaValidator.Config;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Dibk.Ftpb.Validation.Web.Configuration;

public static class XsdValidatorConfig
{
    private static readonly Assembly _schemaRuleAssembly = Assembly.Load("Dibk.Ftpb.Validation.Application");

    public static void AddXmlSchemaValidator(this IServiceCollection services)
    {
        services.AddXmlSchemaValidator(options =>
        {
            options.AddSchema("Planområde", GetResourceStream("planleggingIgangsatt.xsd"));
            options.CacheDurationDays = 30;
        });
    }

    private static Stream GetResourceStream(string fileName)
    {
        var name = _schemaRuleAssembly.GetManifestResourceNames()
            .SingleOrDefault(name => name.EndsWith(fileName));

        return name != null ? 
            _schemaRuleAssembly.GetManifestResourceStream(name) : 
            null;
    }
}
