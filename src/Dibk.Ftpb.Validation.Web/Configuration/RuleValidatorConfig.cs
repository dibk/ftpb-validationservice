﻿using Dibk.Ftpb.Validation.Application.Config;
using Dibk.Ftpb.Validation.Application.Rules.Planområde;
using Dibk.Ftpb.Validation.Application.Rules.Schema.Planområde;
using Dibk.Ftpb.Validation.Web.Configuration;
using DiBK.RuleValidator.Rules.Gml;
using Microsoft.Extensions.DependencyInjection;

namespace Dibk.Ftpb.Validation.Web.Configuration;

public static class RuleValidatorConfig
{
    public static void AddValidators(this IServiceCollection services)
    {
        services.AddValidators(options =>
        {
            options.AddValidator(
                "Planområde",
                typeof(SkjemavalideringForGmlPlanområde),
                new[] { typeof(IGmlValidationInputV1), typeof(IPlanområdeValidationInput) }
            );
        });
    }
}
