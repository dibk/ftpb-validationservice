using System;
using System.Collections.Generic;
using System.Linq;
using Dibk.Ftpb.Validation.Application.DataSources.ApiServices.Validation;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;


namespace Dibk.Ftpb.Validation.Web.Controllers
{
    public class InternalController : Controller
    {
        private readonly IRuleDocumentationProvider _ruleDocumentation;

        public InternalController(ILogger<ValidationController> logger, IRuleDocumentationProvider ruleDocumentation)
        {
            _ruleDocumentation = ruleDocumentation;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("api/comparevalidationruleenvironments")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status207MultiStatus)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Dictionary<string, Dictionary<string, List<RuleModelDocumentation>>>> CompareValidationRuleEnvironments([FromBody] RuleComparison ruleComparison)
        {
            if (ruleComparison == null)
            {
                return BadRequest("RuleComparison object is null");
            }

            var ruleSummaryDictionary = new Dictionary<string, Dictionary<string, List<RuleModelDocumentation>>>();

            var allBranches = new[] { "Dev", "Test", "Prod" };
            var brancheDev = "Dev";
            var brancheTest = "Test";
            var brancheProd = "Prod";
            var localHost = "localhost";

            var dataFormatId = ruleComparison.DataFormatId;
            var dataFormatVersion = ruleComparison.DataFormatVersion;
            //if (!string.IsNullOrEmpty(dataFormatVersion) && dataFormatVersion.Contains("."))
            //    dataFormatVersion = dataFormatVersion.Replace(".", "");
            var branch = ruleComparison.Branch;
            var compareBranch = ruleComparison.CompareBranch;

            if (string.IsNullOrEmpty(branch) && string.IsNullOrEmpty(compareBranch))
            {
                ProcessRuleSummary(brancheDev, brancheTest, "Dev Og Test");
                ProcessRuleSummary(brancheTest, brancheProd, "Test Og Prod");
            }
            else
            {
                branch = GetBranch(branch);
                compareBranch = GetBranch(compareBranch);
                allBranches = new[] { branch, compareBranch };

                ProcessRuleSummary(branch, compareBranch, $"{branch} Og {compareBranch}");
            }
 
            void ProcessRuleSummary(string firstBranch, string secondBranch, string summaryName)
            {
                // Amount
                var rulesNotImplemented = _ruleDocumentation.GetRulesNotImplemented(dataFormatId, dataFormatVersion, firstBranch, secondBranch);
                ruleSummaryDictionary.Add($"Rules Not Implemented In {summaryName}", rulesNotImplemented);

                // Type
                var rulesWithDifferenceSeverityLevel = _ruleDocumentation.GetRulesWithDifferenceSeverityLevel(dataFormatId, dataFormatVersion, firstBranch, secondBranch);
                ruleSummaryDictionary.Add($"Rules With Difference Severity Level In {summaryName}", rulesWithDifferenceSeverityLevel);

                // Description
                var rulesWithDifferentMessageText = _ruleDocumentation.GetRulesWithDifferentMessageText(dataFormatId, dataFormatVersion, firstBranch, secondBranch);
                ruleSummaryDictionary.Add($"Rules With Different Message Text In {summaryName}", rulesWithDifferentMessageText);

                // Different RuleId
                var rulesWithDifferentRuleId = _ruleDocumentation.GetDifferentRuleIdInRule(dataFormatId, dataFormatVersion, firstBranch, secondBranch);
                ruleSummaryDictionary.Add($"Rules With Different ruleId In {summaryName}", rulesWithDifferentRuleId);
            }

            bool ruleDeviationFound = false;


            var rulesLists = ruleSummaryDictionary.Values.ToList();

            if (rulesLists.All(l => l == null))
                return BadRequest($"No rules found for dataFormatId: '{dataFormatId}', dataFormatVersion: '{dataFormatVersion}', in branches: '{string.Join(", ", allBranches)}'");

            //WithoutText
            var rulesWithoutText = _ruleDocumentation.GetRuleWithoutMessageText(dataFormatId, dataFormatVersion, allBranches);
            ruleSummaryDictionary.Add("Rules Without Text", rulesWithoutText);

            foreach (var ruleSummary in ruleSummaryDictionary)
            {
                if (ruleSummary.Value == null)
                    continue;
                if (ruleSummary.Value.Count > 0)
                {
                    ruleDeviationFound = true;
                    break;
                };
            }

            // Sort the dictionary by keys
            var sortedDictionary = ruleSummaryDictionary.OrderBy(kv => kv.Key).ToDictionary(kv => kv.Key, kv => kv.Value);

            return ruleDeviationFound ? StatusCode(207, sortedDictionary) : Ok(sortedDictionary);
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("api/getxmlnodedocumentation")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Dictionary<string, List<RuleModelDocumentation>>> GetXmlNodeRuleDokumentation([FromBody] DocumentationInputModel inputModel)
        {

            var ruleSummaryDictionary = new Dictionary<string, List<RuleModelDocumentation>>();

            if (Helpers.ObjectIsNullOrEmpty(inputModel.FormsMetadataList) || string.IsNullOrEmpty(inputModel?.Branch))
                return NoContent();

            var formRuleForXmlNode = _ruleDocumentation.GetRuleForXmlNodeDocumentation(inputModel.FormsMetadataList, inputModel.XmlNode, inputModel.Branch, inputModel.isXmlNodeStartElement);

            var ruleSummaryForXmlNode = _ruleDocumentation.RuleInfoUse(formRuleForXmlNode, inputModel.IncludeChecklist);

            var ruleSummaryForXmlNodeWithFormName = _ruleDocumentation.UpdateRuleModelDocumentationWithFormName(ruleSummaryForXmlNode);

            if (Helpers.ObjectIsNullOrEmpty(ruleSummaryForXmlNode))
                return NotFound($"Fant ingen regler som starter med dette xmlNode:'{inputModel.XmlNode}' i disse skjemaene: '{string.Join(", ", inputModel.FormsMetadataList)}'");

            ruleSummaryDictionary.Add(inputModel.Branch, ruleSummaryForXmlNodeWithFormName);

            return Ok(ruleSummaryDictionary);
        }

        private static string GetBranch(string branch)
        {

            switch (branch.ToLower())
            {
                case "dev":
                    return "Dev";
                case "test":
                    return "Test";
                case "prod":
                    return "Prod";
                case "localhost":
                    return "localhost";
                default:
                    return branch;
            }
        }
    }
}
