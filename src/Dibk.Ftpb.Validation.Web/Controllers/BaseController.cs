﻿using Dibk.Ftpb.Validation.Application.Logic.FormValidators;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace Dibk.Ftpb.Validation.Web
{
    public abstract class BaseController : ControllerBase
    {
        protected readonly ILogger _logger;
        private readonly IWebHostEnvironment _env;

        protected BaseController(
            ILogger logger, IWebHostEnvironment env)
        {
            _logger = logger;
            _env = env;
        }

        protected IActionResult HandleException(Exception exception)
        {
            _logger.LogError(exception, "Unexpected error occured");

            return exception switch
            {
                ArgumentException _ or InvalidDataException _ or FormatException _ => BadRequest(),
                FormValidatorException _ => BadRequest(exception.Message),
                Exception _ => InternalServerError(exception),
                _ => null,
            };
        }

        private IActionResult InternalServerError(Exception ex)
        {
            if (_env.IsDevelopment())
                return StatusCode(StatusCodes.Status500InternalServerError,
                                  new ProblemDetails()
                                  {
                                      Detail = ex.StackTrace,
                                      Title = ex.Message
                                  });
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
