using AutoMapper;
using Dibk.Ftpb.Validation.Application.Logic.FormValidators;
using Dibk.Ftpb.Validation.Application.Models.Standard;
using Dibk.Ftpb.Validation.Application.Models.Web;
using Dibk.Ftpb.Validation.Application.Reporter;
using Dibk.Ftpb.Validation.Application.Services;
using Dibk.Ftpb.Validation.Application.Services.Gml.Generic;
using Dibk.Ftpb.Validation.Application.Services.Gml.Planområde;
using Dibk.Ftpb.Validation.Client.Models.Request;
using Dibk.Ftpb.Validation.Client.Models.Response;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Debugging = Dibk.Ftpb.Validation.Application.Models.Web.Debugging;

namespace Dibk.Ftpb.Validation.Web.Controllers
{
    [ApiController]
    public class ValidationController : BaseController
    {
        private readonly IValidationService _validationService;
        private readonly IPlanområdeValidationService _planområdeValidationService;
        private readonly IMapper _mapper;

        public ValidationController(
            IValidationService validationService,
            IPlanområdeValidationService planområdeValidationService,
            IMapper mapper,
            ILogger<ValidationController> logger,
            IWebHostEnvironment env) : base(logger, env)
        {
            _validationService = validationService;
            _planområdeValidationService = planområdeValidationService;
            _mapper = mapper;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("api/validate")]
        [HttpPost]
        [ProducesResponseType(typeof(ValidationResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult ValidateForm([FromBody] ValidationRequest input)
        {
            return FormValidation(input);
        }

        [Route("api/validation")]
        [HttpPost]
        [ProducesResponseType(typeof(ValidationResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult FormValidation([FromBody] ValidationRequest input)
        {
            //// Authentication?
            if (!ValidInput(input))
                return BadRequest();

            try
            {
                var validationInput = _mapper.Map<ValidationInput>(input);

                var messages = _validationService.GetValidationResult(validationInput);

                var response = _mapper.Map<ValidationResponse>(messages);

                return Ok(response);
            }
            catch (Exception exception)
            {
                _logger.LogDebug("Validation failed for xml: {FormXml}", input.FormData);
                return HandleException(exception);
            }
        }

        [Route("api/validationReport")]
        [HttpPost]
        [ProducesResponseType(typeof(ValidationReportResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult ValidationReport([FromBody] ValidationRequest input)
        {
            //// Authentication?
            if (!ValidInput(input))
                return BadRequest();

            try
            {
                var validationInput = _mapper.Map<ValidationInput>(input);

                var validationResult = _validationService.GetValidationReportIncludingChecklistAnswers(validationInput);

                var response = _mapper.Map<ValidationReportResponse>(validationResult);

                return Ok(response);
            }
            catch (Exception exception)
            {
                return HandleException(exception);
            }
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("api/validate/file")]
        [HttpPost]
        [ProducesResponseType(typeof(ValidationResponse), StatusCodes.Status200OK)]
        public IActionResult ValidateFile(IFormFile file)
        {
            return FileValidation(file);
        }

        [Route("api/validation/file")]
        [HttpPost]
        [ProducesResponseType(typeof(ValidationResponse), StatusCodes.Status200OK)]
        public IActionResult FileValidation(IFormFile file)
        {
            if (file == null)
                return BadRequest();

            try
            {
                var messages = _validationService.ValidateXmlFile(file);

                var response = _mapper.Map<ValidationResponse>(messages);

                return Ok(response);
            }
            catch (Exception exception)
            {
                return HandleException(exception);
            }
        }

        [Route("api/validation/file/planomraade")]
        [HttpPost]
        [ProducesResponseType(typeof(ValidationResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ValidateGml(IFormFile file)
        {
            if (file == null)
                return BadRequest();

            try
            {
                var messages = await _planområdeValidationService.ValidateAsync(file);

                var response = _mapper.Map<ValidationResponse>(messages);

                return Ok(response);
            }
            catch (Exception exception)
            {
                return HandleException(exception);
            }
        }

        [Route("api/validation/file-and-attachments")]
        [HttpPost]
        [ProducesResponseType(typeof(ValidationResponse), StatusCodes.Status200OK)]
        public IActionResult FileAndAttachmentsValidation(IFormFile file, [FromForm] List<string> attachment)
        {
            if (file == null)
                return BadRequest();

            try
            {
                var messages = _validationService.ValidateXmlFileAndAttachments(file, attachment);

                var response = _mapper.Map<ValidationResponse>(messages);

                return Ok(response);
            }
            catch (Exception exception)
            {
                return HandleException(exception);
            }
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("api/validation/xmlfile-and-attachmentsjsonfile")]
        [HttpPost]
        [ProducesResponseType(typeof(ValidationResponse), StatusCodes.Status200OK)]
        public IActionResult FileAndAttachmentsValidationFile(IFormFile xmlFile, IFormFile attachmentsJsonFile, IFormFile subFormsJsonFile, [FromForm] string? authenticatedSubmitter)
        {
            if (xmlFile == null)
                return BadRequest();

            try
            {
                var messages = _validationService.ValidateXmlFileAndAttachments(xmlFile, attachmentsJsonFile, subFormsJsonFile, authenticatedSubmitter);

                var response = _mapper.Map<ValidationResponse>(messages);

                return Ok(response);
            }
            catch (Exception exception)
            {
                return HandleException(exception);
            }
        }

        [Route("api/formrules/{dataFormatId}/{dataFormatVersion}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<RuleModelDocumentation[]> FormDocumentation(string dataFormatId, string dataFormatVersion)
        {
            if (string.IsNullOrEmpty(dataFormatId) || string.IsNullOrEmpty(dataFormatVersion))
            {
                return BadRequest();
            }

            var formValidationRules = _validationService.GetFormValidationRules(dataFormatId, dataFormatVersion);
            if (formValidationRules == null || !formValidationRules.Any())
            {
                return BadRequest($"No rules found for dataFormatId: {dataFormatId}, dataFormatVersion: {dataFormatVersion}");
            }
            var documentationModel = RuleDocumentationComposer.GetRuleDocumentationModel(formValidationRules.ToList());

            var OrderdRules = documentationModel.OrderBy(r => r.Xpath).ThenBy(r => r.RuleId);

            return Ok(OrderdRules);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("api/debuggrulenumber")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Dictionary<string, string>> DebuggRuleNumber([FromBody] Debugging[] body)
        {
            if (body == null || !body.Any())
                return BadRequest();


            var validatorXpathList = new Dictionary<string, string>();
            var rulesDictionary = body.GroupBy(data => data.RuleId).ToDictionary(gdc => gdc.Key, gdc => gdc.ToList());

            foreach (var rules in rulesDictionary)
            {
                for (int index = 0; index < rules.Value.Count(); index++)
                {
                    var validtorXpath = Application.Utils.Debugging.DebugValidatorFormReference(rules.Key);

                    string ruleId = rules.Key;
                    if (validatorXpathList.ContainsKey(rules.Key))
                        ruleId = ruleId + $" ({index})";

                    validatorXpathList.Add(ruleId, $"{validtorXpath}");
                }
            }


            return Ok(validatorXpathList);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("api/formproperties")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<FormDataAttribute> ChecklistUrlInfo([FromBody] string dataFormatId, string dataFormatVersion)
        {
            if (string.IsNullOrEmpty(dataFormatVersion) && string.IsNullOrEmpty(dataFormatVersion))
            {
                return BadRequest();
            }

            var props = _validationService.GetFormDataAttribute(dataFormatId, dataFormatVersion);

            return Ok(props);
        }

        private bool ValidInput(ValidationRequest input)
        {
            return !string.IsNullOrWhiteSpace(input.FormData);
        }
    }
}
